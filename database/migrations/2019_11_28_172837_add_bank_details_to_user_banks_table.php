<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankDetailsToUserBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_banks', function (Blueprint $table) {
            $table->string('customer')->nullable();
            $table->string('legal_address')->nullable();
            $table->string('physical_address')->nullable();
            $table->string('signing_person_1')->nullable();
            $table->string('position_1')->nullable();
            $table->string('signing_person_2')->nullable();
            $table->string('position_2')->nullable();
            $table->string('passport')->nullable();
            $table->integer('individual')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_banks', function (Blueprint $table) {
            //
        });
    }
}
