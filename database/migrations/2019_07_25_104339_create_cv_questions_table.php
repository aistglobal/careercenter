<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCvQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cv_questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('cv_id')->nullable();
            $table->enum('present_employer',['yes','no']);
            $table->enum('previous_employer',['yes','no']);
            $table->string('government')->nullable();
            $table->enum('arrested',['yes','no']);
            $table->text('arrested_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cv_questions');
    }
}
