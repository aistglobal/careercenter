<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldsInCvVolunteering extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cv_volunteering', function (Blueprint $table) {
            $table->dropColumn('country_id');
            $table->text('countries')->after('work_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cv_volunteering', function (Blueprint $table) {
            $table->dropColumn('countries');
            $table->smallInteger('country_id')->nullable();
        });
    }
}
