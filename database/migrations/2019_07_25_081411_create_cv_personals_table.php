<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCvPersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cv_personals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('cv_id')->nullable();
            $table->date('birthday')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('sex')->nullable();
            $table->enum('disabled',['yes','no'])->default('no');
            $table->string('disabled_description')->nullable();
            $table->string('height')->nullable();
            $table->string('weight')->nullable();
            $table->smallInteger('citizenship_id')->unsigned()->nullable();
            $table->smallInteger('nationality_id')->unsigned()->nullable();
            $table->string('image')->nullable();
            $table->string('official_service')->nullable();
            $table->enum('dependants',['yes','no'])->default('no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cv_personals');
    }
}
