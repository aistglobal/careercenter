<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCvExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cv_experiences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('cv_id')->nullable();
            $table->string('name')->nullable();
            $table->string('city')->nullable();
            $table->smallInteger('country_id')->nullable();
            $table->string('position')->nullable();
            $table->string('position_field')->nullable();
            $table->string('salary')->nullable();
            $table->string('currency')->nullable();
            $table->string('period')->nullable();
            $table->date('period_from')->nullable();
            $table->date('period_to')->nullable();
            $table->text('job_description')->nullable();
            $table->text('main_duties')->nullable();
            $table->string('supervisor_name')->nullable();
            $table->string('employer_numbers')->nullable();
            $table->text('reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cv_experiences');
    }
}
