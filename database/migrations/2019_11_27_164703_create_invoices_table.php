<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->string('performer')->nullable();
            $table->string('performer_address')->nullable();
            $table->string('performer_bank')->nullable();
            $table->string('performer_tax')->nullable();
            $table->string('performer_account')->nullable();
            $table->string('client')->nullable();
            $table->string('client_address')->nullable();
            $table->string('client_bank')->nullable();
            $table->string('client_tax')->nullable();
            $table->string('client_account')->nullable();
            $table->string('booking_number')->nullable();
            $table->string('account_product')->nullable();
            $table->datetime('date')->nullable();
            $table->integer('amount')->nullable();
            $table->string('currency')->nullable();
            $table->string('director')->nullable();
            $table->string('position')->nullable();
            $table->text('posted_announcements')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
