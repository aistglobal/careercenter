<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCvAvailabilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cv_availabilities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('cv_id')->nullable();
            $table->date('period_from')->nullable();
            $table->date('period_to')->nullable();
            $table->string('days')->nullable();
            $table->string('hours_from')->nullable();
            $table->string('hours_to')->nullable();
            $table->enum('travel',['yes','no']);
            $table->string('term')->nullable();
            $table->text('travel_reason');
            $table->string('show_resume')->nullable();
            $table->string('short_salary')->nullable();
            $table->string('short_currency')->nullable();
            $table->string('short_period')->nullable();
            $table->string('long_salary')->nullable();
            $table->string('long_currency')->nullable();
            $table->string('long_period')->nullable();
            $table->string('time')->nullable();
            $table->text('interest_area')->nullable();
            $table->text('position_type')->nullable();
            $table->text('org_type')->nullable();
            $table->string('undesired_org')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cv_availabilities');
    }
}
