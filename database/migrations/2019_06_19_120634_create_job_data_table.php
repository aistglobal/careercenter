<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('job_id')->unsigned();
            $table->string('lang', 3)->nullable();
            $table->string('organization')->nullable();
            $table->string('title')->nullable();
            $table->string('menu')->nullable();
            $table->string('term')->nullable();
            $table->text('open')->nullable();
            $table->string('audience')->nullable();
            $table->string('start_date', 30)->nullable();
            $table->string('duration')->nullable();
            $table->string('location')->nullable();
            $table->text('description')->nullable();
            $table->text('responsibilities')->nullable();
            $table->text('requirements')->nullable();
            $table->string('salary', 50)->nullable();
            $table->text('procedures')->nullable();
            $table->string('opening_date', 50)->nullable();
            $table->string('deadline')->nullable();
            $table->text('about_company')->nullable();
            $table->text('about')->nullable();
            $table->text('notes')->nullable();
            $table->string('author')->nullable();
            $table->string('language')->nullable();
            $table->string('pages')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_data');
    }
}
