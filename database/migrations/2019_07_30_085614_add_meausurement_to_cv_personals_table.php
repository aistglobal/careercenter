<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMeausurementToCvPersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cv_personals', function (Blueprint $table) {
            $table->enum('height_measurement',['centimeters','meters','inches','feet']);
            $table->enum('weight_measurement',['kilograms','pounds']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cv_personals', function (Blueprint $table) {
            //
        });
    }
}
