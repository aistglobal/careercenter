<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = array(
            array('name' => 'user', 'display_name' => 'User'),
            array('name' => 'organization', 'display_name' => 'Organization'),
            array('name' => 'pro_organization', 'display_name' => 'Pro organization'),
            array('name' => 'moderator', 'display_name' => 'Moderator'),
            array('name' => 'head_moderator', 'display_name' => 'Head moderator'),
            array('name' => 'admin', 'display_name' => 'Admin'),
        );
        \App\Role::insert($roles);

    }
}
