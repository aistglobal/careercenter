<?php

use Illuminate\Database\Seeder;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $langs =LaravelLocalization::getSupportedLocales();
        $languages=[];
        foreach($langs as $localeCode => $properties){
            $languages[]=['code'=>$localeCode];

        }
        \App\Languages::insert($languages);
    }
}
