<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = array(
            array('key' => 'volunteering'),
            array('key' => 'internship'),
            array('key' => 'education'),
            array('key' => 'training'),
            array('key' => 'fellowship'),
            array('key' => 'scholarship'),
            array('key' => 'event'),
            array('key' => 'news'),
            array('key' => 'publication'),
            array('key' => 'competition'),
            );
        \App\Type::insert($types);

    }
}
