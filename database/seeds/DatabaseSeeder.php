<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $this->call( CountriesTableSeeder::class);
          $this->call( PermissionsTableSeeder::class);
//        $this->call( TypesTableSeeder::class);
//       $this->call( LanguagesTableSeeder::class);
//        $this->call(CategorysTableSeeder::class);
//        $this->call(SubCategoryTableSeeder::class);

//         $this->call(RolesTableSeeder::class);
    }
}
