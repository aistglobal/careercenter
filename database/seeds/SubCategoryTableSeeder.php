<?php

use Illuminate\Database\Seeder;

class SubCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $categories = \App\Category::all();
      $sub=[];
      foreach ($categories as $item)
      {
          if($item->name!='all_fields')
          {
              foreach (config('enums.sub_category_'.$item->name) as $subcategory)
              {
                  $sub[] = ['name'=>$subcategory,'category_id'=>$item->id];
              }
          }

      }
        \App\Category::insert($sub);
    }
}
