<?php


use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        Illuminate\Support\Facades\DB::table('permissions')->delete();
//
//        $give_permissions = new \App\Permission();
//        $give_permissions->name = 'give_permissions';
//        $give_permissions->display_name = 'Give permissions to roles';
//        $give_permissions->save();
//
//
//        $profile = new \App\Permission();
//        $profile->name = 'profile';
//        $profile->display_name = 'View My Profile Page';
//        $profile->save();
//
//        $create = new \App\Permission();
//        $create->name = 'create_announcement';
//        $create->display_name = 'Create an announcement';
//        $create->save();
//
//        $edit = new \App\Permission();
//        $edit->name = 'edit_announcement';
//        $edit->display_name = 'Edit an announcement';
//        $edit->save();
//
//        $preview = new \App\Permission();
//        $preview->name = 'preview_announcement';
//        $preview->display_name = 'Preview an announcement';
//        $preview->save();
//
//        $delete = new \App\Permission();
//        $delete->name = 'delete_announcement';
//        $delete->display_name = 'Delete an announcement';
//        $delete->save();
//
//        $submit = new \App\Permission();
//        $submit->name = 'submit_announcement';
//        $submit->display_name = 'Submit an announcement';
//        $submit->save();
//
//        $post = new \App\Permission();
//        $post->name = 'post_announcement';
//        $post->display_name = 'Post an announcement';
//        $post->save();
//
//        $post_email = new \App\Permission();
//        $post_email->name = 'post_email_announcement';
//        $post_email->display_name = 'Post and Email an announcement';
//        $post_email->save();
//
//        $reject = new \App\Permission();
//        $reject->name = 'reject_announcement';
//        $reject->display_name = 'Reject an announcement';
//        $reject->save();
//
//        $expired = new \App\Permission();
//        $expired->name = 'expired_announcement';
//        $expired->display_name = 'Expired an announcement';
//        $expired->save();
//
//        $clone = new \App\Permission();
//        $clone->name = 'clone_announcement';
//        $clone->display_name = 'Copy to new an announcement';
//        $clone->save();
//
//        $draft = new \App\Permission();
//        $draft->name = 'save_as_draft';
//        $draft->display_name = 'Save as draft an announcement';
//        $draft->save();
//
//        $job_all = new \App\Permission();
//        $job_all->name = 'all_jobs';
//        $job_all->display_name = 'Access to all jobs';
//        $job_all->save();
//
//        $create_pages = new \App\Permission();
//        $create_pages->name = 'create_pages';
//        $create_pages->display_name = 'Create pages';
//        $create_pages->save();
//
//        $edit_pages = new \App\Permission();
//        $edit_pages->name = 'edit_pages';
//        $edit_pages->display_name = 'Edit pages';
//        $edit_pages->save();
//
//        $delete_pages = new \App\Permission();
//        $delete_pages->name = 'delete_pages';
//        $delete_pages->display_name = 'Delete pages';
//        $delete_pages->save();
//
//
//        $create_article = new \App\Permission();
//        $create_article->name = 'create_article';
//        $create_article->display_name = 'Create article';
//        $create_article->save();
//
//        $edit_article = new \App\Permission();
//        $edit_article->name = 'edit_article';
//        $edit_article->display_name = 'Edit article';
//        $edit_article->save();
//
//        $delete_article = new \App\Permission();
//        $delete_article->name = 'delete_article';
//        $delete_article->display_name = 'Delete article';
//        $delete_article->save();
//
//        $create_review = new \App\Permission();
//        $create_review->name = 'create_review';
//        $create_review->display_name = 'Create review';
//        $create_review->save();
//
//        $edit_review = new \App\Permission();
//        $edit_review->name = 'edit_review';
//        $edit_review->display_name = 'Edit review';
//        $edit_review->save();
//
//        $delete_review = new \App\Permission();
//        $delete_review->name = 'delete_review';
//        $delete_review->display_name = 'Delete review';
//        $delete_review->save();
//
//        $create_resume = new \App\Permission();
//        $create_resume->name = 'create_resume';
//        $create_resume->display_name = 'Create resume';
//        $create_resume->save();
//
//        $edit_resume = new \App\Permission();
//        $edit_resume->name = 'edit_resume';
//        $edit_resume->display_name = 'Edit resume';
//        $edit_resume->save();
//
//        $delete_resume = new \App\Permission();
//        $delete_resume->name = 'delete_resume';
//        $delete_resume->display_name = 'Delete resume';
//        $delete_resume->save();
//
//        $create_user = new \App\Permission();
//        $create_user->name = 'create_user';
//        $create_user->display_name = 'Create user';
//        $create_user->save();
//
//        $edit_user = new \App\Permission();
//        $edit_user->name = 'edit_user';
//        $edit_user->display_name = 'Edit user';
//        $edit_user->save();
//
//        $delete_user = new \App\Permission();
//        $delete_user->name = 'delete_user';
//        $delete_user->display_name = 'Delete user';
//        $delete_user->save();
//
//        $close_user = new \App\Permission();
//        $close_user->name = 'close_user';
//        $close_user->display_name = 'Close user';
//        $close_user->save();
//
//        $translate = new \App\Permission();
//        $translate->name = 'translate';
//        $translate->display_name = 'translate';
//        $translate->save();
//
//        $change_password = new \App\Permission();
//        $change_password->name = 'change_password';
//        $change_password->display_name = 'Change Password';
//        $change_password->save();


//        $subscribtion_all = new \App\Permission();
//        $subscribtion_all->name = 'subscribtions';
//        $subscribtion_all->display_name = 'Access to all subscribers';
//        $subscribtion_all->save();
//
//        $resources_all = new \App\Permission();
//        $resources_all->name = 'resources';
//        $resources_all->display_name = 'Access to all resources';
//        $resources_all->save();
//
//        $resources_category = new \App\Permission();
//        $resources_category->name = 'resource_category';
//        $resources_category->display_name = 'Modify Resource Categories';
//        $resources_category->save();
//
//        $resources_category = new \App\Permission();
//        $resources_category->name = 'all_resume';
//        $resources_category->display_name = 'Access to all Resumes';
//        $resources_category->save();

//        $resources = new \App\Permission();
//        $resources->name = 'all_resource';
//        $resources->display_name = 'Access to all Resources';
//        $resources->save();
//
//        $create_resource = new \App\Permission();
//        $create_resource->name = 'create_resource';
//        $create_resource->display_name = 'Create resource';
//        $create_resource->save();
//
//        $edit_resource = new \App\Permission();
//        $edit_resource->name = 'edit_resource';
//        $edit_resource->display_name = 'Edit resource';
//        $edit_resource->save();
//
//        $delete_resource = new \App\Permission();
//        $delete_resource->name = 'delete_resource';
//        $delete_resource->display_name = 'Delete resource';
//        $delete_resource->save();
//
//
//        $attach_company = new \App\Permission();
//        $attach_company->name = 'attachCompany_user';
//        $attach_company->display_name = 'Attach Company to user';
//        $attach_company->save();


//        $change_username = new \App\Permission();
//        $change_username->name = 'changeUsername_user';
//        $change_username->display_name = 'Permission to Change Own Username';
//        $change_username->save();
//
//
//        $change_username_all = new \App\Permission();
//        $change_username_all->name = 'changeUsernameAll_user';
//        $change_username_all->display_name = 'Permission to Change all Users Username';
//        $change_username_all->save();

//        $articles_all = new \App\Permission();
//        $articles_all->name = 'all_article';
//        $articles_all->display_name = 'Access to all articles';
//        $articles_all->save();

//
//        $service = new \App\Permission();
//        $service->name = 'all_service';
//        $service->display_name = 'Access to all Services';
//        $service->save();
//
//
//        $create_service = new \App\Permission();
//        $create_service->name = 'create_service';
//        $create_service->display_name = 'Create service';
//        $create_service->save();
//
//        $edit_service = new \App\Permission();
//        $edit_service->name = 'edit_service';
//        $edit_service->display_name = 'Edit service';
//        $edit_service->save();
//
//        $delete_service = new \App\Permission();
//        $delete_service->name = 'delete_service';
//        $delete_service->display_name = 'Delete service';
//        $delete_service->save();
//
//        $service_category = new \App\Permission();
//        $service_category->name = 'serviceCategory_service';
//        $service_category->display_name = 'Modify service Categories';
//        $service_category->save();
//
//        $change_bannkDetails = new \App\Permission();
//        $change_bannkDetails->name = 'changeBankDetails_user';
//        $change_bannkDetails->display_name = 'Change BankDetails';
//        $change_bannkDetails->save();
//
//
//        $create_invoice = new \App\Permission();
//        $create_invoice->name = 'create_invoice';
//        $create_invoice->display_name = 'Create invoice';
//        $create_invoice->save();
//
//        $edit_invoice = new \App\Permission();
//        $edit_invoice->name = 'edit_invoice';
//        $edit_invoice->display_name = 'Edit invoice';
//        $edit_invoice->save();
//
//        $delete_invoice = new \App\Permission();
//        $delete_invoice->name = 'delete_invoice';
//        $delete_invoice->display_name = 'Delete invoice';
//        $delete_invoice->save();


        $invoice = new \App\Permission();
        $invoice->name = 'all_invoice';
        $invoice->display_name = 'Access to all Invoices';
        $invoice->save();
    }
}
