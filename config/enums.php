<?php
return [
    'title' =>
        [
            'mr', 'ms', 'mrs'
        ],
    'legal' => ['local_ngo', 'llc', 'ltd', 'cjsc', 'ojsc', 'government', 'private', 'international_ngo', 'fund', 'foundation',
        'charity', 'benevolent_union', 'general_benevolent_union', 'humanitar_organization', 'brand', 'other'],
    'org_type' => [
        'library', 'direct_employer', 'staffing_agency', 'educational_institution', 'other'],
    'fields_of_activity' => ['administration', 'advertising/pr', 'agriculture', 'art/publishing', 'banking', 'construction',
        'customer_service', 'manufacturing_operations', 'education/training', 'engineering/architecture', 'entrepreneurial/startup', 'entry_level',
        'finance/insurance', 'government', 'health_care', 'hospitality/travel', 'human_resources', 'information_technology',
        'insurance', 'internet/new_media', 'law_enforcement/security', 'legal', 'management_consulting', 'marketing',
        'non_profit/volunteer', 'pharmaceutical/biotech', 'real_estate', 'restaurant/food_service', 'retail', 'sales',
        'social', 'teaching', 'tech_contract', 'technology', 'telecommunications', 'temp_jobs', 'translation/interpretation',
        'transportation/logistics'
    ],
    'number_of_employees' => ['1-9', '10-49', '50-90', '90-99', '100+'],
    'category' =>
        ['all_fields', 'advertising/pr', 'arts/entertainment', 'diplomacy', 'school_education',
            'engineering/real_estate', 'finance/insurance', 'health_care/chemistry', 'human_resources', 'it',
            'journalism/publishing', 'law/legal', 'linguistics', 'nature/agriculture', 'tourism/transportation'],
    'resource_category' => ['employment', 'for_organizations', 'educational', 'other'],
    'sub_category_advertising/pr' =>
        [
            'advertising',
            'marketing',
            'sales',
            'pr_communication',
            'public-relations'
        ],

    'sub_category_arts/entertainment' => [
        'styling',
        'interior_and_exterior_decoration',
        'fashion_design',
        'design',
        'painting',
        'music',
        'acting',
        'make-up',
        'manicure',
        'hair_styling',
        'production',
        'casting',
        'operating',
        'cosmetology'
    ],
    'sub_category_diplomacy' =>
        [
            'international_affairs',
            'politics',
            'foreign/home_affairs'
        ],
    'sub_category_school_education' =>
        [
            'teaching/training',
            'scientific_studies'
        ],
    'sub_category_engineering/real_estate' =>
        ['engineering',
            'architecture',
            'construction',
            'manufacturing',
            'engineering',
            'real_estate_brokerage',
            'agent'],
    'sub_category_finance/insurance' =>
        [
            'finance',
            'business',
            'auditing',
            'economics',
            'accounting',
            'administration',
            'management',
            'insurance_brokerage'
        ],
    'sub_category_health_care/chemistry' => [
        'pharmacy',
        'medical_consultancy',
        'lab_supervising',
        'social_science',
        'surgery',
        'dentistry',
        'therapeutics',
        'doctor',
        'doctor_cosmetologist',
        'gynecology',
        'pediatrics',
        'cardiology',
        'nursing'
    ],
    'sub_category_human_resources' => [
        'hr_management',
        'philosophy',
        'sociology'
    ],
    'sub_category_it' => [
        'it_management',
        'software_engineering',
        'software_development',
        'general_pc_usage',
        'wireless_engineering',
        'software_engineering',
        'software_development',
        'C/C++/C#_development',
        'java_development',
        'ASP.NET_development',
        'PHP/MySQL_development',
        'perl_programming',
        'computer_graphics_and_animation',
        'system_administration',
        'web_design',
        'web_site_moderator',
        'system_analysis'
    ],
    'sub_category_journalism/publishing' =>
        ['journalism',
            'TV_Reporting',
            'correspondence',
            'editing',
            'photography',
            'graphics_editing'],
    'sub_category_law/legal' =>
        ['jurisprudence',
            'procurator', 'law',
            'advocacy',
            'investigation',
            'police_work',
            'detective',
            'military_officer',
            'security_service',
            'body_guard'
        ],
    'sub_category_linguistics' =>
        ['  translation',
            'interpretation',
            'linguistics_and_cross_cultural_communication'
        ],
    'sub_category_nature/agriculture' => [
        'geography',
        'agriculture'
    ],
    'sub_category_tourism/transportation' =>
        ['cooking',
            'barmen',
            'driving'],
    'sub_category_all_fields' => [''],
    'job_status' => ['expired', 'posted', 'rejected', 'draft', 'pending'],
    'marital_status' => ['single', 'married', 'separated', 'widow(er)', 'divorced'],
    'height_measurement' => ['centimeters', 'meters', 'inches', 'feet'],
    'weight_measurement' => ['kilograms', 'pounds'],
    'official_service' => ['not_served_yet', 'in_service', 'served', 'exempted', 'no_intensions'],
    'phone_type' => ['mobile', 'landline'],
    'computer_level' => ['familiar', 'good', 'advanced', 'professional'],
    'work_type' => ['campaign', 'tree_planting', 'organization_of_festival'],
    'relationship' => ['sister', 'brother', 'mother', 'father', 'husband', 'wife', 'son', 'daughter'],
    'currency' => ['amd', 'euro', 'rur', 'usd'],
    'period' => ['hourly', 'weekly', 'daily', 'monthly', 'yearly'],
    'education_type' => ['kindergarten', 'primary_school', 'secondary_school', 'high_school', 'college', 'university', 'academy', 'training_center', 'institute', 'aspirantura',
        'seminary', 'specialized_educational_institution'],
    'degree' => ['attestat', 'k-12_certificate', 'graduate_diploma', 'BA', '5_year_graduate_diploma', 'MA', 'MBA', 'PHD', 'candidate_of_sciences', 'professor', 'incomplete'],
    'training_type' => ['computer_center', 'technical_center', 'other'],
    'certificate_type' => ['no_certificate', 'participation', 'technical_center', 'accomplishment', 'successful_accomplishment', 'other'],
    'reading' => ['lsq_Fluent', 'lsr_Good', 'lsq_Fair', 'lsq_Poor'],
    'total_study' => ['1-3_months', '4-6_months', '7-12_months', '1-2_years', '2-3_years', '3-5_years', 'over_5_years', 'over_10_years'],
    'types_of_positions' => ['advertising_agent', 'market_developer', 'sales_manager', 'distributor', 'stylist', 'fashion_designer', 'painter', 'musician'],
    'days' => ['mon', 'tus', 'wed', 'thu', 'fri', 'sat', 'sun'],
    'terms' => ['full_time', 'part_time', 'permanent', 'contract', 'temporary'],
    'show_resume' => ['career_house', 'all'],
    'reference_relationship' => ['my_colleague', 'my_direct_supervisor', 'my_department_director', 'my_company_head', 'my_teacher/instructor'],
    'duration_type' => ['hours', 'days', 'weeks', 'months', 'years'],
    'applier_status' => [
        "rpending",
        "rnm",
        "rpospective",
        "rsl",
        "rgroup1",
        "rgroup2",
        "rgroup3",
        "rstage1",
        "rstage2",
        "rstage3",
        "rselected",
        "rrefused",
        "rmi",
        "rjo",
        "rhired",
        "rfired",
        "roverqualified",
        "rstat_Inappropriate",
        "rstat_Incomplete",
        "rst_Reserve",
        "rstat_LessMatching",
        "rst_no",
        "rs_Resigned",
        "rs_Underqualified",
        "rs_InvitedforTest",
        "rs_prelimineryinterview",
        "rs_prefinalinterview",
        "rs_finalinterview",
        "rsl2",
        "rsl3",
        "rst_other",
        "rst_re_applied",
        "rs_InvitedforTraining"],
    'months' => ['jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'august', 'sept', 'oct', 'nov', 'dec']


];


