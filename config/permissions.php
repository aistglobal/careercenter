<?php
return [
    'user' =>
        [
            'profile' => '1'
        ],
    'organization' =>
        [
            'profile' => '1',
            'job' => '1'
        ],
    'pro_organization' =>
        [
            'profile' => '1',
            'job' => '1',
        ],
    'moderator' =>
        [
            'profile' => '1',
            'job' => '1',
            'blog' => '1',
            'review' => '1',
            'translation' => '1',
            'all_jobs' => '1',
            'all_articles' => '1',
            'all_reviews' => '1',
            'pages' => '1',
            'subscribtions' => '1'
        ],
    'head_moderator' =>
        [
            'profile' => '1',
            'job' => '1',
            'blog' => '1',
            'review' => '1',
            'translation' => '1',
            'user' => '1',
            'all_jobs' => '1',
            'all_articles' => '1',
            'all_reviews' => '1',
            'pages' => '1',
            'subscribtions' => '1'
        ],
    'admin' =>
        [
            'profile' => '1',
            'job' => '1',
            'blog' => '1',
            'review' => '1',
            'translation' => '1',
            'user' => '1',
            'edit_admin' => '1',
            'all_jobs' => '1',
            'all_articles' => '1',
            'all_reviews' => '1',
            'pages' => '1',
            'subscribtions' => '1',
            'resumes' => '1',
            'give_permissions' => '1'
        ],


];