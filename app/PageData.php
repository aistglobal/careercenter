<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageData extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'page_data';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'page_id';
    protected $fillable =
        [
            'page_id', 'lang', 'title', 'content', 'meta_title','meta_description','meta_keywords'
        ];
}
