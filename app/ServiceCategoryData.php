<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCategoryData extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'service_category_data';
    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'service_category_id';
    protected $fillable = ['service_category_id','lang','name'];
}
