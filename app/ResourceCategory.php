<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ResourceCategory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'resource_category';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    protected $fillable =
        [
            'parent_id','slug'
        ];
    public function child()
    {
        return $this->hasMany(ResourceCategory::class, 'parent_id');
    }
    public function parent()
    {
        return $this->belongsTo(ResourceCategory::class, 'parent_id');
    }
    public function data()
    {
        $lang = LaravelLocalization::getCurrentLocale();
        return $this->hasOne(ResourceCategoryData::class, 'resource_id')->where('lang', $lang);
    }
    public static function slugify($text)
    {

        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
