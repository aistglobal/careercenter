<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cv extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cv';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'user_id';
    protected $fillable = [
        'user_id',
        'lang',
        'objective',
        'special_skills',
        'status'
    ];
    public function  user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function contacts()
    {
        return $this->hasOne(CvContact::class, 'cv_id');
    }
    public function personal()
    {
        return $this->hasOne(CvPersonal::class, 'cv_id');
    }
    public function address()
    {
        return $this->morphMany('App\UserAddress', 'addressable');
    }
    public function computer_skills()
    {
        return $this->hasOne(CvComputerSkills::class, 'cv_id');
    }
    public function volunteering()
    {
        return $this->hasOne(CvVolunteering::class, 'cv_id');
    }
    public function dependants()
    {
        return $this->hasMany(CvDependant::class, 'cv_id');
    }
    public function experience()
    {
        return $this->hasMany(CvExperience::class, 'cv_id');
    }
    public function education()
    {
        return $this->hasMany(CvEducation::class, 'cv_id');
    }
    public function training()
    {
        return $this->hasMany(CvTraining::class, 'cv_id');
    }
    public function language()
    {
        return $this->hasMany(CvLanguage::class, 'cv_id');
    }

    public function visa()
    {
        return $this->hasMany(CvVisa::class, 'cv_id');
    }
    public function availability()
    {
        return $this->hasOne(CvAvailability::class, 'cv_id');
    }
    public function reference()
    {
        return $this->hasMany(CvReference::class, 'cv_id');
    }
    public function questions()
    {
        return $this->hasOne(CvQuestion::class, 'cv_id');
    }
    public function attachment()
    {
        return $this->hasMany(CvAttachment::class, 'cv_id');
    }
    public static function  resumePdf($id)
    {
        $cv = Cv::findorfail($id);
        $phone = null;
        $personal_citizenship = null;
        $countries = Country::all();
        $work_type = null;
        $volunteering_countries = null;
        $availability_days = null;
        $availability_terms = null;

        if (isset($cv->contacts)) {
            $phone = json_decode($cv->contacts->phone, true);
        }
        if (isset($cv->personal->citizenship_id)) {
            $personal_citizenship = json_decode($cv->personal->citizenship_id);
        }
        if (isset($cv->volunteering)) {
            $work_type = json_decode($cv->volunteering->work_type);
            $volunteering_countries = json_decode($cv->volunteering->countries);
        }
        if (isset($cv->availability)) {
            $availability_days = json_decode($cv->availability->days);
            $availability_terms = json_decode($cv->availability->term);
        }
        $pdf = \PDF::loadView('resumePdf', [
            'cv' => $cv,
            'phone' => $phone,
            'personal_citizenship' => $personal_citizenship,
            'countries' => $countries,
            'work_type' => $work_type,
            'volunteering_countries' => $volunteering_countries,
            'availability_days' => $availability_days,
            'availability_terms' => $availability_terms
        ]);
        $pdf = $pdf->save(storage_path().'/app/public/resume-'.$id.'.pdf');
        return 'resume-'.$id.'.pdf';
    }

    public static function  educationName($name,$type)
    {
        $l_name = strtolower($name);
        $l_type =  strtolower($type);
        if (strpos($l_name, $l_type) !== false) {
            return $name;
        }
        return $name.' '.$type;
    }

}



