<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CvQuestion extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cv_questions';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'cv_id';
    protected $fillable =
        [
        'cv_id',
        'present_employer',
        'previous_employer',
        'government',
        'arrested',
        'arrested_details',
    ];
}
