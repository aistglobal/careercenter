<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Blog extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blogs';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'user_id';
    protected $fillable = ['user_id', 'image','slug','edited_id','status'];

    public static function slugify($text)
    {

        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }
        return $text;
    }
    public function data()
    {
        $lang = LaravelLocalization::getCurrentLocale();
        return $this->hasOne(BlogData::class, 'blog_id')->where('lang', $lang);
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function editor()
    {
        return $this->belongsTo(User::class, 'edited_id');
    }
}
