<?php

namespace App\Notifications;

use App\Channels\PushChannel;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AcceptJobNotification extends Notification
{
  use Queueable;
  protected $user;
  protected $data;
  protected $pushData = [];

  /**
   * AcceptJobNotification constructor.
   * @param $data
   * @param User $user
   * @param $message
   */
  public function __construct($data, User $user, $push_notification)
  {
    $this->$user = $user;
    $this->data = $data;
    $this->pushData['data'] = $data;
    $this->pushData['title'] = $push_notification['title'];
    $this->pushData['message'] = $push_notification['message'];
  }

  /**
   * Get the notification's delivery channels.
   *
   * @param  mixed $notifiable
   * @return array
   */
  public function via($notifiable)
  {
    return ['database', PushChannel::class];
  }

  /**
   * Get the mail representation of the notification.
   *
   * @param  mixed $notifiable
   * @return \Illuminate\Notifications\Messages\MailMessage
   */
  public function toMail($notifiable)
  {
    return (new MailMessage)->view(
        'emails.accepted_job_contractor', ['user' => $this->user, 'contractor' => $notifiable, 'data' => $this->data]
    )->subject($this->pushData['title']);
  }

  /**
   * Get the array representation of the notification.
   *
   * @param  mixed $notifiable
   * @return array
   */
  public function toArray($notifiable)
  {
    $this->data['title'] = $this->pushData['title'];
    $this->data['message'] = $this->pushData['message'];
    return $this->data;
  }

  /**
   * @return array
   */
  public function toPush()
  {
    return $this->pushData;
  }
}

