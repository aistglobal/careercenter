<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;

class MailResetPasswordNotification extends ResetPassword
{
  use Queueable;

  protected $is_app;

  /**
   * Create a new notification instance.
   *
   * @return void
   */
  public function __construct($token, $is_app = false)
  {
    $this->is_app = $is_app;
    parent::__construct($token);
  }

  /**
   * Get the notification's delivery channels.
   *
   * @param  mixed $notifiable
   * @return array
   */
  public function via($notifiable)
  {
    return ['mail'];
  }

  /**
   * Get the mail representation of the notification.
   *
   * @param  mixed $notifiable
   * @return \Illuminate\Notifications\Messages\MailMessage
   */
  public function toMail($notifiable)
  {
    $reset_token = $this->token;
    if ($this->is_app) {
      return (new MailMessage)
          ->view('auth.passwords.notify',compact('reset_token'))
          ->subject('Reset Password Notification')
          ->line("Hello! You are receiving this email because we received a password reset request for your account.")
          ->action('Reset Password', $this->token)
          ->line("This password reset token will expire in " . config('auth.passwords.users.expire') . " minutes")
          ->line("If you did not request a password reset, no further action is required.");
    }
    $link = url("/reset-password/" . $this->token);
    return (new MailMessage)
        ->subject('Reset Password Notification')
        ->line("Hello! You are receiving this email because we received a password reset request for your account.")
        ->action('Reset Password', $link)
        ->line("This password reset token will expire in " . config('auth.passwords.users.expire') . " minutes")
        ->line("If you did not request a password reset, no further action is required.");
  }

  /**
   * Get the array representation of the notification.
   *
   * @param  mixed $notifiable
   * @return array
   */
  public function toArray($notifiable)
  {
    return [
      //
    ];
  }
}