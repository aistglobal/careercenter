<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CvLanguage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cv_languages';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'cv_id';
    protected $fillable = [
        'cv_id',
        'language_id',
        'language_type',
        'reading',
        'writing',
        'communication',
        'total_study',
        'typing',
        'shorthand'
    ];
    public function language()
    {
        return $this->belongsTo(Languages::class, 'language_id');
    }
}
