<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CvTraining extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cv_trainings';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'cv_id';
    protected $fillable = [
        'cv_id',
        'name',
        'city',
        'country_id',
        'type',
        'study_area',
        'course_name',
        'certificate_type',
        'period_from',
        'period_to',

    ];
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
}
