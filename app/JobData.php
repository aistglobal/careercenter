<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class JobData extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'job_data';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    protected $dates = ['expiration_date'];

    public $foreignKey = 'job_id';
    protected $fillable = [
        'job_id','lang','organization','title','menu','term','open','audience','start_date',
        'duration','description','responsibilities','requirements','salary','procedures','opening_date',
        'deadline','about_company','about','notes','author','language','pages','contact_person',
        'contact_title','organization_notes','location_city'
    ];
  /**
   * The "booting" method of the model.
   *
   * @return void
   */

//  protected static function boot()
//  {
//    parent::boot();
//    $lang = LaravelLocalization::getCurrentLocale();
//    static::addGlobalScope('lang', function (Builder $builder) use ($lang) {
//      $builder->where('lang', $lang);
//    });
//
//  }

}
