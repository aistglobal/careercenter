<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Resource extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'resources';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    protected $fillable = ['url','category_id','subcategory_id','status','user_id','edited_id'];
    public function data()
    {
        $lang = LaravelLocalization::getCurrentLocale();
        return $this->hasOne(ResourceData::class, 'resource_id')->where('lang', $lang);
    }
    public function category()
    {
        return $this->belongsTo(ResourceCategory::class, 'category_id', 'id');
    }
    public function subCategory()
    {
        return $this->belongsTo(ResourceCategory::class, 'subcategory_id', 'id');
    }
    public function editor()
    {
        return $this->belongsTo(User::class, 'edited_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public static $statuses = ['pending'=>'Pending','rejected'=>'Rejected','active'=>"Active",'inactive'=>'Inactive'];
}
