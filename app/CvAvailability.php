<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CvAvailability extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cv_availabilities';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'cv_id';
    protected $fillable = [
        'cv_id',
        'period_from',
        'period_to',
        'days',
        'hours_from',
        'hours_to',
        'travel',
        'term',
        'travel_reason',
        'show_resume',
        'short_salary',
        'short_currency',
        'short_period',
        'long_salary',
        'long_currency',
        'long_period',
        'time',
        'interest_area',
        'position_type',
        'org_type',
        'undesired_org',
    ];
}
