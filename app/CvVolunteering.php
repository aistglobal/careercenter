<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CvVolunteering extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cv_volunteering';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'cv_id';
    protected $fillable = [
        'cv_id',
        'willing',
        'public_work',
        'work_type',
        'countries'
    ];
}
