<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CvComputerSkills extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cv_computer_skills';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'cv_id';
    protected $fillable = [
        'cv_id',
        'level',
        'software',
        'hardware',
    ];
}
