<?php

namespace App\Console\Commands;

use App\Job;
use App\Organization;
use App\Services\Helper;
use Illuminate\Console\Command;

class GenerateSlugs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate_slugs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       /* $organizations = Organization::all();
        foreach ($organizations as $item)
        {
            $item->slug  = Helper::getSlug($item->name, 'App\Organization',$item);
            $item->save();
        }*/

        $jobs = Job::whereNull('slug')->get();
        foreach ($jobs as $item)
        {
            if(isset($item->data))
            {
                $item->slug = Helper::getSlug($item->data->title, 'App\Job',$item);
                $item->save();
            }
        }

        dd('success');
    }
}
