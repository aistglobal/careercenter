<?php

namespace App\Console\Commands;

use App\Permission;
use App\PermissionRole;
use App\Role;
use Illuminate\Console\Command;

class FillPermissionRole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fillPermissionRole';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $roles = Role::all();
        foreach ($roles as $item)
        {
            foreach (config('permissions.' . $item->name) as $key => $value) {
                $permission = Permission::where('name',$key)->first();
               PermissionRole::create(['role_id'=>$item->id,'permission_id'=>$permission->id]);
            }
        }
        dd('permission_role');

    }
}
