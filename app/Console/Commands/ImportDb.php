<?php

namespace App\Console\Commands;

use App\Attachment;
use App\Country;
use App\Cv;
use App\CvAttachment;
use App\CvAvailability;
use App\CvComputerSkills;
use App\CvContact;
use App\CvDependant;
use App\CvEducation;
use App\CvExperience;
use App\CvLanguage;
use App\CvPersonal;
use App\CvQuestion;
use App\CvReference;
use App\CvTraining;
use App\CvVisa;
use App\CvVolunteering;
use App\Job;
use App\JobData;
use App\Organization;
use App\User;
use App\UserAddress;
use Dompdf\Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ImportDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import_db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //$this->moveCvAttachments();
        //$this->fixPeriods();
        //$this->givePermissions();
        //$this->users();
        //$this->companies();
        //$this->resumes();
        //$this->careerUsers();

        //$this->careerJobs();

        //$this->changePhoneStructure();

        //$this->fixAttachments();

        //$this->updateRegisteredDate();

        //$this->replaceEnters();

        $this->fixStatus();

    }

    public function fixStatus()
    {
        $oldJobs = DB::connection('mysql2')->table('annids')->get()->toArray();

        foreach ($oldJobs as $oldJob) {
            $job = Job::with('data')->whereHas('data', function ($q) use ($oldJob) {
                return $q->where('menu', $oldJob->linktext)->where('contact_person', $oldJob->contactperson)->where('contact_title', $oldJob->contacttitle);
            })->whereDate('expiration_date', $oldJob->expires)->where('email', $oldJob->contactemail)->where('phone', $oldJob->contactphone)->first();


            if($job){

                $job->views = $oldJob->retrieved;
                $job->save();
               /* $editedUser = User::where('username', $oldJob->lasteditor)->first();

                $editedUserId = null;

                if ($editedUser) {
                    $editedUserId = $editedUser->id;
                }


                $addedIP = null;
                $addedUserId = null;

                if (filter_var($oldJob->addedby, FILTER_VALIDATE_IP)) {
                    $addedIP = $oldJob->addedby;
                } else {
                    $addedUser = User::where('username', $oldJob->addedby)->first();

                    if ($addedUser)
                        $addedUserId = $addedUser->id;
                }


                $job->added_user_id = $addedUserId;
                $job->edited_user_id = $editedUserId;
                $job->added_IP = $addedIP;
                $job->edited_IP = long2ip($oldJob->lastip);
                $job->save();*/
            }


        }

            dd("bareeev");

    }

    public function replaceEnters()
    {
        $jobs = JobData::all();

        foreach ($jobs as $job) {
            if (isset($job->description)) {
                $job->description = str_replace("\r\n", "<br />", $job->description);
            }
            if (isset($job->responsibilities)) {
                $job->responsibilities = str_replace("\r\n", "<br />", $job->responsibilities);
            }
            if (isset($job->requirements)) {
                $job->requirements = str_replace("\r\n", "<br />", $job->requirements);
            }
            if (isset($job->about)) {
                $job->about = str_replace("\r\n", "<br />", $job->about);
            }
            if (isset($job->about_company)) {
                $job->about_company = str_replace("\r\n", "<br />", $job->about_company);
            }
            if (isset($job->procedures)) {
                $job->procedures = str_replace("\r\n", "<br />", $job->procedures);
            }

            $job->save();
        }


        dd("barev");
    }


    public function updateRegisteredDate()
    {
        $oldUsers = DB::connection('mysql2')->table('ccusers')->get()->toArray();

        foreach ($oldUsers as $oldUser) {
            $currentUser = User::where('username', $oldUser->userid)->first();

            if ($currentUser) {
                $currentUser->created_at = $oldUser->lastupdate;
                $currentUser->save();
            }

        }

    }

    public function moveCvAttachments()
    {
        $attachments = CvAttachment::all();

        foreach ($attachments as $attachment) {
            $file = explode('/', $attachment->attachment);

            $filename = $file[1];

            mkdir('/' . storage_path('app/public/cv_attachments/' . $attachment->cv_id));

            Storage::move('/' . storage_path('app/public/cv_attachments/' . $filename), '/' . storage_path('app/public/cv_attachments/' . $attachment->attachment));


        }
    }

    public function fixPeriods()
    {
        $cvExperiences = CvExperience::all();
        foreach ($cvExperiences as $cvExperience) {
            $from = explode('-', $cvExperience->period_from);
            $to = explode('-', $cvExperience->period_to);
            if (count($from) > 1) {

                if (strlen($from[0]) <= 2) {
                    $cvExperience->period_from = $from[1] . '-' . $from[0];
                }

            }

            if (count($to) > 1) {
                if (strlen($to[0]) <= 2) {
                    $cvExperience->period_to = $to[1] . '-' . $to[0];
                }

            }

            $cvExperience->save();
        }


        /*
                $cvEducations = CvEducation::all();
                foreach ($cvEducations as $cvEducation){
                    $from = explode('-', $cvEducation->period_from);
                    $to = explode('-', $cvEducation->period_to);
                    if(count($from) > 1 && count($to) > 1) {

                        $cvEducation->period_from = $from[1] . '-' . $from[0];
                        $cvEducation->period_to = $to[1] . '-' . $to[0];
                        $cvEducation->save();
                    }
                }

                $cvTrainings = CvTraining::all();
                foreach ($cvTrainings as $cvTraining){
                        $from = explode('-', $cvTraining->period_from);
                        $to = explode('-', $cvTraining->period_to);
                        if(count($from) > 1 && count($to) > 1){

                            $cvTraining->period_from = $from[1].'-'.$from[0];
                            $cvTraining->period_to = $to[1].'-'.$to[0];
                            $cvTraining->save();
                        }

                }

                $cvVisas = CvVisa::all();
                foreach ($cvVisas as $cvVisa){
                    $from = explode('-', $cvVisa->period_from);
                    $to = explode('-', $cvVisa->period_to);
                    if(count($from) > 1 && count($to) > 1) {
                        $cvVisa->period_from = $from[1] . '-' . $from[0];
                        $cvVisa->period_to = $to[1] . '-' . $to[0];
                        $cvVisa->save();
                    }
                }*/
    }

    public function givePermissions()
    {
        $users = User::all();
        foreach ($users as $user) {
            foreach (config('permissions.' . $user->role) as $key => $value) {
                $user->givePermissionsTo($key);
            }
        }
    }

    public function changePhoneStructure()
    {
        $users = User::where('phone', 'not like', "%{%")->where('phone', '<>', '')->get();

        foreach ($users as $user) {
            $phone = json_decode($user->phone);

            $user->phone = json_encode([array('phone' => $phone[0], 'type' => 'mobile')]);
            $user->save();

        }

    }

    public function fixAttachments()
    {
        $attachments = Attachment::all();

        foreach ($attachments as $attachment) {
            $fileInfo = explode('/', $attachment->file);
            $fileName = $fileInfo[1];

            $oldAttachment = DB::connection('mysql2')->table('annattachments')->where('outname', $fileName)->where('descr', $attachment->name)->first();

            try {
                if ($oldAttachment && file_exists('/' . storage_path('app/public/attachments/' . $oldAttachment->inname)))

                    Storage::move('/' . storage_path('app/public/attachments/' . $oldAttachment->inname), '/' . storage_path('app/public/attachments/' . $attachment->job_id . '/' . $oldAttachment->outname));
            } catch (\Exception $e) {
                dd($e->getMessage());
            }

        }
    }

    public function careerUsers()
    {
        $oldUsers = DB::connection('mysql2')->table('ccusers')->get()->toArray();

        foreach ($oldUsers as $oldUser) {

            $currentUser = User::where('username', $oldUser->userid)->first();

            if (!$currentUser) {
                $phone = json_encode([array('phone' => $oldUser->phone, 'type' => 'mobile')]);


                $name = explode(' ', $oldUser->contact_name);

                $firstName = $name[0];
                $lastName = '';

                if (count($name) > 1) {
                    $lastName = $name[1];
                }

                $logo = time() . rand(0, 9999999) . '.jpg';

                file_put_contents(storage_path('app/public/logos/' . $logo), $oldUser->logo);

                $userData = [
                    'username' => $oldUser->userid,
                    'first_name' => $firstName,
                    'last_name' => $lastName,
                    'email' => $oldUser->email,
                    'password' => $oldUser->password,
                    'title' => $oldUser->contact_title,
                    'phone' => $phone,
                    'is_organization' => 1,
                    'status' => 0,
                    'role' => $oldUser->userpriv,
                    'image' => $logo
                ];

                $user = User::create($userData);

                Organization::create(
                    [
                        'user_id' => $user->id,
                        'name' => $oldUser->organization,
                        'legal_structure' => 'llc',
                        'type' => 'other'
                    ]
                );
                UserAddress::create([
                    'addressable_type' => 'App\User',
                    'addressable_id' => $user->id,
                    'country_id' => 13,
                    'address' => $oldUser->address,
                    'type' => 'business'
                ]);
            } else {

                Organization::create(
                    [
                        'user_id' => $currentUser->id,
                        'name' => $oldUser->organization,
                        'legal_structure' => 'llc',
                        'type' => 'other'
                    ]
                );
                UserAddress::create([
                    'addressable_type' => 'App\User',
                    'addressable_id' => $currentUser->id,
                    'country_id' => 13,
                    'address' => $oldUser->address,
                    'type' => 'business'
                ]);
                $currentUser->is_organization = 1;
                $currentUser->save();
            }


        }

    }

    public function careerJobs()
    {
        $oldjobs = DB::connection('mysql2')->table('annids')->where('id', '>', 28322)->get()->toArray();


        foreach ($oldjobs as $oldjob) {

            $infos = DB::connection('mysql2')->table('anncontents')->where('annid', $oldjob->id)->get()->toArray();

            $open = '';
            $audience = '';
            $starDate = '';
            $duration = '';
            $location = '';
            $description = '';
            $responsibilities = '';
            $qualifications = '';
            $salary = '';
            $procedures = '';
            $term = '';
            $code = '';
            $openingDate = '';
            $deadline = '';
            $aboutCompany = '';
            $about = '';
            $notes = '';
            $author = '';
            $isbn = '';
            $language = '';
            $pages = '';

            foreach ($infos as $info) {

                if ($info->hdrcode == 'OT') $open = $info->text;

                if ($info->hdrcode == 'IA') $audience = $info->text;

                if ($info->hdrcode == 'SD') $starDate = $info->text;

                if ($info->hdrcode == 'D') $duration = $info->text;

                if ($info->hdrcode == 'LN') $location = $info->text;

                if ($info->hdrcode == 'JD') $description = $info->text;

                if ($info->hdrcode == 'JR') $responsibilities = $info->text;

                if ($info->hdrcode == 'RQ') $qualifications = $info->text;

                if ($info->hdrcode == 'RS') $salary = $info->text;

                if ($info->hdrcode == 'AP') $procedures = $info->text;

                if ($info->hdrcode == 'T') $term = $info->text;

                if ($info->hdrcode == 'AC') $code = $info->text;

                if ($info->hdrcode == 'OD') $openingDate = $info->text;

                if ($info->hdrcode == 'AD') $deadline = $info->text;

                if ($info->hdrcode == 'C') $aboutCompany = $info->text;

                if ($info->hdrcode == 'AB') $about = $info->text;

                if ($info->hdrcode == 'AN') $notes = $info->text;

                if ($info->hdrcode == 'PI') $isbn = $info->text;

                if ($info->hdrcode == 'A') $author = $info->text;

                if ($info->hdrcode == 'PT') $term = $info->text;

                if ($info->hdrcode == 'LG') $language = $info->text;

                if ($info->hdrcode == 'PG') $pages = $info->text;

                if ($info->hdrcode == 'EL') $responsibilities = $info->text;

                if ($info->hdrcode == 'CT') $term = $info->text;

                if ($info->hdrcode == 'DD') $description = $info->text;

                if ($info->hdrcode == 'ET') $term = $info->text;

                if ($info->hdrcode == 'E') $term = $info->text;

                if ($info->hdrcode == 'F') $term = $info->text;

                if ($info->hdrcode == 'IA') $audience = $info->text;

                if ($info->hdrcode == 'N') $term = $info->text;

                if ($info->hdrcode == 'ND') $description = $info->text;

                if ($info->hdrcode == 'PD') $starDate = $info->text;

                if ($info->hdrcode == 'R') $qualifications = $info->text;

                if ($info->hdrcode == 'S') $term = $info->text;

                if ($info->hdrcode == 'TT') $term = $info->text;

            }

            $user = User::where('username', $oldjob->addedby)->first();
            $userId = 16057;
            if ($user) {
                $userId = $user->id;
            }
            $status = $oldjob->status;
            if ($status == 'final') $status = 'posted';

            if ($status == 'drafts') $status = 'draft';

            $job = Job::create([
                'code' => $code,
                'type_id' => $this->convertAnnouncementType($oldjob->anntype),
                'status' => $status,
                'expiration_date' => $oldjob->expires,
                'phone' => $oldjob->contactphone,
                'email' => $oldjob->contactemail,
                'category' => NULL,
                'online_submission' => 0,
                'user_id' => $userId,
                'sub_category' => NULL,
                'isbn' => $isbn,
            ]);


            JobData::create(
                [
                    'job_id' => $job->id,
                    'lang' => 'en',
                    'organization' => $oldjob->organization,
                    'title' => $oldjob->title,
                    'menu' => $oldjob->linktext,
                    'term' => $term,
                    'open' => $open,
                    'audience' => $audience,
                    'start_date' => $starDate,
                    'duration' => $duration,
                    'description' => $description,
                    'responsibilities' => $responsibilities,
                    'requirements' => $qualifications,
                    'salary' => $salary,
                    'procedures' => $procedures,
                    'opening_date' => $openingDate,
                    'deadline' => $deadline,
                    'about_company' => $aboutCompany,
                    'about' => $about,
                    'notes' => $notes,
                    'author' => $author,
                    'language' => $language,
                    'pages' => $pages,
                    'contact_person' => $oldjob->contactperson,
                    'contact_title' => $oldjob->contacttitle,
                    'organization_notes' => $oldjob->contactorg,
                    'location_city' => $location
                ]
            );

            $attachments = DB::connection('mysql2')->table('annattachments')->where('annid', $oldjob->id)->get()->toArray();

            if ($attachments) {
                foreach ($attachments as $attachment) {
                    Attachment::create([
                        'file' => $job->id . '/' . $attachment->outname,
                        'job_id' => $job->id,
                        'name' => $attachment->descr
                    ]);
                }
            }

        }
    }

    public function users()
    {
        $oldUsers = DB::connection('mysql2')->table('reg_users')->get()->toArray();

        foreach ($oldUsers as $oldUser) {
            $phone = $this->getPhoneNumber($oldUser);

            $data['phone'] = $phone;

            $userData = [
                'username' => $oldUser->user,
                'first_name' => $oldUser->fname,
                'last_name' => $oldUser->lname,
                'email' => $oldUser->email,
                'password' => $oldUser->passwd,
                'sid' => $oldUser->sid,
                'middle_name' => $oldUser->mname,
                'title' => $oldUser->title,
                'website' => $oldUser->web_url,
                'phone' => $phone,
                'is_organization' => 0,
                'birthday' => date('Y-m-d', $oldUser->bdata),
                'citizenship_id' => $this->convertCountry($oldUser->citizenship),
                'residence_country_id' => $this->convertCountry($oldUser->res_country),
                'status' => 0,
                'role' => 'user',
                'image' => ''
            ];

            $user = User::create($userData);

            UserAddress::create([
                'addressable_type' => 'App\User',
                'addressable_id' => $user->id,
                'country_id' => $this->convertCountry($oldUser->prim_country),
                'city' => $oldUser->prim_city,
                'zip' => $oldUser->prim_zipcode,
                'address' => $oldUser->prim_address,
                'type' => $oldUser->prim_addresstype
            ]);

            if (strlen($oldUser->sec_address) > 2) {
                UserAddress::create([
                    'addressable_type' => 'App\User',
                    'addressable_id' => $user->id,
                    'country_id' => $this->convertCountry($oldUser->sec_country),
                    'zip' => $oldUser->sec_zipcode,
                    'address' => $oldUser->sec_address,
                    'type' => $oldUser->sec_addresstype
                ]);
            }


        }


    }

    public function companies()
    {
        $oldUsers = DB::connection('mysql2')->table('org_user')->get()->toArray();

        foreach ($oldUsers as $oldUser) {

            $phone = $this->getPhoneNumberCompanies($oldUser);

            $data['phone'] = $phone;


            $userData = [
                'username' => $oldUser->user,
                'first_name' => $oldUser->fname,
                'last_name' => $oldUser->lname,
                'email' => $oldUser->email,
                'password' => $oldUser->passwd,
                'sid' => $oldUser->sid,
                'middle_name' => $oldUser->mname,
                'title' => $oldUser->title,
                'website' => $oldUser->web_url,
                'phone' => $phone,
                'is_organization' => 1,
                'birthday' => date('Y-m-d', $oldUser->reg_date),
                'status' => 0,
                'role' => $oldUser->userlevel,
                'image' => $oldUser->logo
            ];

            $user = User::create($userData);

            Organization::create(
                [
                    'user_id' => $user->id,
                    'name' => $oldUser->org_name,
                    'short_name' => $oldUser->org_shortname,
                    'legal_structure' => $this->convertLegalType($oldUser->leg_structure),
                    'type' => $this->convertOrganizationType($oldUser->org_type),
                    'field_of_activities' => $this->convertActivity($oldUser->activity),
                    'number_of_employees' => $oldUser->emp_number,
                    'about_company' => $oldUser->about,
                    'position' => $oldUser->position
                ]
            );
            UserAddress::create([
                'addressable_type' => 'App\User',
                'addressable_id' => $user->id,
                'country_id' => $this->convertCountry($oldUser->country),
                'city' => $oldUser->city,
                'zip' => $oldUser->Zip_Code,
                'address' => $oldUser->address,
                'type' => 'business'
            ]);

        }


    }

    public function jobs()
    {
        $oldjobs = DB::connection('mysql2')->table('pann_job')->get()->toArray();


        foreach ($oldjobs as $oldjob) {
            $user = User::where('username', $oldjob->user)->first();
            $userId = 16057;
            if ($user) {
                $userId = $user->id;
            }

            $job = Job::create([
                'code' => $oldjob->app_ac,
                'type_id' => 1,
                'status' => $oldjob->status,
                'expiration_date' => date('Y-m-d', $oldjob->app_expd),
                'phone' => $oldjob->app_tel,
                'email' => $oldjob->app_emadd,
                'category' => NULL,
                'online_submission' => 1,
                'user_id' => $userId,
                'sub_category' => NULL,
                'location_country_id' => $this->convertCountry($oldjob->app_country)
            ]);

            JobData::create(
                [
                    'job_id' => $job->id,
                    'lang' => $this->convertLang($oldjob->lang),
                    'organization' => $oldjob->app_org,
                    'title' => $oldjob->app_title,
                    'menu' => $oldjob->app_mlt,
                    'term' => $oldjob->app_term,
                    'open' => $oldjob->app_oec,
                    'audience' => $oldjob->app_IA,
                    'start_date' => $oldjob->app_st,
                    'duration' => $oldjob->app_dur,
                    'description' => $oldjob->app_jd,
                    'responsibilities' => $oldjob->app_jr,
                    'requirements' => $oldjob->app_rq,
                    'salary' => $oldjob->app_rs,
                    'procedures' => $oldjob->app_ap,
                    'opening_date' => $oldjob->app_od,
                    'deadline' => $oldjob->app_ad,
                    'about_company' => $oldjob->app_abc,
                    'about' => $oldjob->app_about,
                    //'notes'=> $oldjob->app,
                    //'author'=> $oldjob->app,
                    //'language'=> $oldjob->app,
                    //'pages'=> $oldjob->app,
                    'contact_person' => $oldjob->app_cotpe,
                    'contact_title' => $oldjob->app_cotitle,
                    'organization_notes' => NULL,
                    'location_city' => $oldjob->app_city
                ]
            );
        }
    }

    public function resumes()
    {
        $oldResumes = DB::connection('mysql2')->table('total_resume')->get()->toArray();

        foreach ($oldResumes as $oldResume) {
            $user = User::where('username', $oldResume->userName)->first();
            $userId = 16422;
            if ($user) {
                $userId = $user->id;
            }
            $objText = '';
            $objective = DB::connection('mysql2')->table('ResumeCC_o')->where('username', $oldResume->userName)->where('lang', $oldResume->lang)->first();

            if ($objective) {
                $objText = $objective->o_objective;
            }

            $cv = Cv::create([
                'user_id' => $userId,
                'lang' => $this->convertLang($oldResume->lang),
                'objective' => $oldResume->SpecialSkills,
                'special_skills' => $objText
            ]);

            $contact = DB::connection('mysql2')->table('ResumeCC_Ci')->where('username', $oldResume->userName)->where('lang', $oldResume->lang)->first();

            if ($contact) {
                CvContact::create([
                    'cv_id' => $cv->id,
                    'first_name' => $contact->firstName,
                    'last_name' => $contact->lastName,
                    'middle_name' => $contact->middleName,
                    'email' => $contact->email,
                    'title' => $this->convertTitle($contact->title),
                    'website' => $contact->webPage,
                    'phone' => $this->convertCvPhone($contact),
                    'residence_country_id' => $this->convertCountry($contact->residence)
                ]);

                UserAddress::create([
                    'addressable_type' => 'App\Cv',
                    'addressable_id' => $cv->id,
                    'country_id' => $this->convertCountry($contact->country),
                    'city' => $contact->prim_city,
                    'zip' => $contact->zipCode,
                    'address' => $contact->primaryAddress,
                    'type' => $contact->addressType1
                ]);

                if (strlen($contact->secondaryAddress) > 4) {
                    UserAddress::create([
                        'addressable_type' => 'App\Cv',
                        'addressable_id' => $cv->id,
                        'country_id' => $this->convertCountry($contact->country2),
                        'city' => '',
                        'zip' => $contact->zipCode2,
                        'address' => $contact->secondaryAddress,
                        'type' => $contact->addressType2
                    ]);
                }

            }

            $personal = DB::connection('mysql2')->table('ResumeCC_Pi')->where('username', $oldResume->userName)->where('lang', $oldResume->lang)->first();
            if ($personal) {
                $disabled = 'yes';
                $depend = 'yes';

                if ($personal->Disabled_stat == 'pe_no') $disabled = 'no';

                if ($personal->havedepend == 'pe_no') $depend = 'no';

                $birthday = null;
                if ($personal->pi_byear > 0 && $personal->pi_bmonth > 0 && $personal->pi_bday > 0) {
                    if ($personal->pi_bmonth == 2 && $personal->pi_bday < 29) {
                        $birthday = $personal->pi_byear . '-' . $personal->pi_bmonth . '-' . $personal->pi_bday;
                    }
                }

                CvPersonal::create([
                    'cv_id' => $cv->id,
                    'birthday' => $birthday,
                    'marital_status' => $personal->pi_ms,
                    'sex' => $personal->pi_sex,
                    'disabled' => $disabled,
                    'disabled_description' => $personal->Disabled_detail,
                    'height' => $personal->Realheight,
                    'weight' => $personal->RealWeight,
                    'citizenship_id' => json_encode([$this->convertCountry($personal->pi_citizen)]),
                    'nationality_id' => $this->convertCountry($personal->pi_national),
                    'image' => $personal->RealPictureName,
                    'official_service' => $this->convertServe($personal->pe_stype),
                    'dependants' => $depend,
                    'height_measurement' => $this->convertHeight($personal->pi_hg),
                    'weight_measurement' => $this->convertWeight($personal->pi_wt),

                ]);


                if ($personal->havedepend) {
                    if (strlen($personal->depName) > 2) $this->createCVDependant($cv->id, $personal->depName, date('Y-m-d', is_int($personal->dep_date) ? $personal->dep_date : null), $this->convertRelationship($personal->pe_relationship));
                    if (strlen($personal->depName1) > 2) $this->createCVDependant($cv->id, $personal->depName1, date('Y-m-d', is_int($personal->dep_date1) ? $personal->dep_date1 : null), $this->convertRelationship($personal->pe_relationship1));
                    if (strlen($personal->depName2) > 2) $this->createCVDependant($cv->id, $personal->depName2, date('Y-m-d', is_int($personal->dep_date2) ? $personal->dep_date2 : null), $this->convertRelationship($personal->pe_relationship2));
                    if (strlen($personal->depName3) > 2) $this->createCVDependant($cv->id, $personal->depName3, date('Y-m-d', is_int($personal->dep_date3) ? $personal->dep_date3 : null), $this->convertRelationship($personal->pe_relationship3));

                }


            }

            $experiences = DB::connection('mysql2')->table('ResumeCC_pe')->where('username', $oldResume->userName)->where('lang', $oldResume->lang)->get();

            if ($experiences) {
                foreach ($experiences as $experience) {
                    CvExperience::create([
                        'cv_id' => $cv->id,
                        'name' => $experience->pe_name,
                        'city' => $experience->pe_city,
                        'country_id' => $this->convertCountry($experience->pe_country),
                        'position' => $experience->pe_position,
                        'position_field' => $experience->pe_field,
                        'salary' => $experience->pe_salery_f,
                        'currency' => $this->convertSalaryCurrency($experience->pe_salery_t),
                        'period' => $this->convertSalaryPeriod($experience->pe_times),
                        'period_from' => $experience->fmonths . '-' . $experience->fyears,
                        'period_to' => $experience->tmonths . '-' . $experience->tyears,
                        'job_description' => $experience->pe_jd,
                        'main_duties' => $experience->pe_mainduties,
                        'supervisor_name' => $experience->pe_name_super,
                        'employer_numbers' => $experience->pe_nokind,
                        'reason' => $experience->pe_reason
                    ]);

                }

            }

            $educations = DB::connection('mysql2')->table('ResumeCC_be')->where('username', $oldResume->userName)->where('lang', $oldResume->lang)->get();

            if ($educations) {

                foreach ($educations as $education) {

                    CvEducation::create(
                        [
                            'cv_id' => $cv->id,
                            'name' => $education->pe_name,
                            'city' => $education->pe_city,
                            'country_id' => $this->convertCountry($education->pe_country),
                            'type' => $this->convertEducationType($education->pe_type),
                            'department' => $education->pe_department,
                            'study_area' => $this->convertStudyArea($education->pe_stady_area),
                            'major_field' => $education->pe_major_field,
                            'period_from' => $education->fmonths . '-' . $education->fyears,
                            'period_to' => $education->tmonths . '-' . $education->tyears,
                            'degree' => $this->convertEducationDegree($education->pe_digree),
                        ]
                    );
                }
            }

            $languages = DB::connection('mysql2')->table('ResumeCC_ls')->where('username', $oldResume->userName)->where('lang', $oldResume->lang)->get();

            if ($languages) {
                foreach ($languages as $language) {
                    $languageType = 'native';
                    if ($language->pe_langtype == 'pe_not_native') $languageType = 'foreign';

                    CvLanguage::create([
                        'cv_id' => $cv->id,
                        'language_id' => $language->pe_lang,
                        'language_type' => $languageType,
                        'reading' => $this->convertLevel($language->pe_reading),
                        'writing' => $this->convertLevel($language->pe_writing),
                        'communication' => $this->convertLevel($language->pe_comunication),
                        'total_study' => $this->convertTotalStudy($language->pe_totalstudy),
                        'typing' => $language->pe_typeing,
                        'shorthand' => $language->pe_shorthand
                    ]);
                }
            }

            $computer = DB::connection('mysql2')->table('ResumeCC_ces')->where('username', $oldResume->userName)->where('lang', $oldResume->lang)->first();

            if ($computer) {
                CvComputerSkills::create(
                    [
                        'cv_id' => $cv->id,
                        'level' => $this->convertComputerLevel($computer->pe_level),
                        'software' => $computer->pe_software,
                        'hardware' => $computer->pe_he,
                    ]
                );
            }

            $availability = DB::connection('mysql2')->table('ResumeCC_a')->where('username', $oldResume->userName)->where('lang', $oldResume->lang)->first();

            if ($availability) {

                $travel = 'no';

                if ($availability->pe_avtrevel == 'pe_yes') $travel = 'yes';

                $term = [];

                if ($availability->pe_full_time == 1) $term[] = 'full_time';
                if ($availability->pe_part_time == 1) $term[] = 'part_time';
                if ($availability->pe_Permanent == 1) $term[] = 'permanent';
                if ($availability->pe_Temporary == 1) $term[] = 'temporary';
                if ($availability->pe_Contract == 1) $term[] = 'contract';


                $periodFrom = null;
                if ($availability->fyears > 0 && $availability->fmonths > 0 && $availability->fbday > 0) {
                    if ($availability->fmonths == 2 && $availability->fbday < 29) {
                        $periodFrom = $availability->fyears . '-' . $availability->fmonths . '-' . $availability->fbday;
                    }
                }

                $periodTo = null;
                if ($availability->tyears > 0 && $availability->tmonths > 0 && $availability->tbday > 0) {
                    if ($availability->tmonths == 2 && $availability->tbday < 29) {
                        $periodTo = $availability->tyears . '-' . $availability->tmonths . '-' . $availability->tbday;
                    }
                }

                CvAvailability::create(
                    [
                        'cv_id' => $cv->id,
                        'period_from' => $periodFrom,
                        'period_to' => $periodTo,
                        'days' => $this->convertCvDates($availability),
                        'hours_from' => $availability->start_hh . ':' . $availability->start_mm,
                        'hours_to' => $availability->end_hh . ':' . $availability->end_mm,
                        'travel' => $travel,
                        'term' => json_encode($term),
                        'travel_reason' => $availability->pe_ifnot,
                        'show_resume' => $availability->resume_av,
                        'short_salary' => $availability->a_salery_t_small,
                        'short_currency' => $this->convertSalaryCurrency($availability->a_currency_small),
                        'short_period' => $this->convertSalaryPeriod($availability->a_times_small),
                        'long_salary' => $availability->a_salery_t,
                        'long_currency' => $this->convertSalaryCurrency($availability->a_currency),
                        'long_period' => $this->convertSalaryPeriod($availability->a_times),
                        'time' => $availability->pe_minleng,
                        'interest_area' => $availability->pe_aoi,
                        'position_type' => $availability->pe_top,
                        'org_type' => $availability->pe_too,
                        'undesired_org' => $availability->Und_Org,
                    ]
                );
            }

            $volunteer = DB::connection('mysql2')->table('ResumeCC_vpw')->where('username', $oldResume->userName)->where('lang', $oldResume->lang)->first();

            if ($volunteer) {

                $willing = 'yes';
                if ($volunteer->wpv == 'wp_no') $willing = 'no';

                $publicWork = 'yes';
                if ($volunteer->wpp == 'wp_no') $publicWork = 'no';

                CvVolunteering::create([
                    'cv_id' => $cv->id,
                    'willing' => $willing,
                    'public_work' => $publicWork,
                    'work_type' => $this->convertWorkType($volunteer->wpp_content),
                    'countries' => $this->convertVolunteerCountries($volunteer->validcountry)
                ]);
            }

            $visas = DB::connection('mysql2')->table('ResumeCC_vrs')->where('username', $oldResume->userName)->where('lang', $oldResume->lang)->get();

            if ($visas) {
                foreach ($visas as $visa) {

                    $periodFrom = null;
                    if (is_int($visa->vrs_fdate)) $periodFrom = date('Y-m-d', $visa->vrs_fdate);

                    $periodTo = null;
                    if (is_int($visa->vrs_tdate)) $periodTo = date('Y-m-d', $visa->vrs_tdate);

                    CvVisa::create([
                        'cv_id' => $cv->id,
                        'type' => $visa->validtype,
                        'country_id' => $this->convertCountry($visa->validcountry),
                        'period_from' => $periodFrom,
                        'period_to' => $periodTo
                    ]);
                }
            }

            $questions = DB::connection('mysql2')->table('ResumeCC_aq')->where('username', $oldResume->userName)->where('lang', $oldResume->lang)->get();

            if ($questions) {
                foreach ($questions as $question) {

                    $presentEmployer = 'yes';

                    if ($question->pe_canapros == 'pe_yes') $presentEmployer = 'no';

                    $previousEmployer = 'yes';

                    if ($question->pe_previos == 'pe_yes') $previousEmployer = 'no';

                    $arrested = 'no';

                    if ($question->aq == 'pe_yes') $arrested = 'yes';

                    CvQuestion::create(
                        [
                            'cv_id' => $cv->id,
                            'present_employer' => $presentEmployer,
                            'previous_employer' => $previousEmployer,
                            'government' => $question->pe_ifyes,
                            'arrested' => $arrested,
                            'arrested_details' => $question->aq_content,
                        ]
                    );
                }
            }

            $references = DB::connection('mysql2')->table('ResumeCC_re')->where('username', $oldResume->userName)->where('lang', $oldResume->lang)->get();

            if ($references) {
                foreach ($references as $reference) {

                    $duration = null;
                    if (is_int($reference->redu_date)) $duration = $reference->redu_date;

                    CvReference::create(
                        [
                            'cv_id' => $cv->id,
                            'title' => $this->convertTitle($reference->retitle_title),
                            'full_name' => $reference->refname_title,
                            'position' => $reference->repo_title,
                            'relationship' => $reference->rere_title,
                            'duration' => $duration,
                            'duration_type' => $reference->redu_title,
                            'current_position' => $reference->recpo_title,
                            'email' => $reference->reem_title,
                            'phone' => $reference->repn_title,
                        ]
                    );
                }
            }

            $attachments = DB::connection('mysql2')->table('ResumeCC_ua')->where('username', $oldResume->userName)->where('lang', $oldResume->lang)->get();

            if ($attachments) {
                foreach ($attachments as $attachment) {
                    CvAttachment::create(
                        [
                            'cv_id' => $cv->id,
                            'attachment' => $cv->id . '/' . $attachment->newfile,
                            'description' => $attachment->comment,
                        ]
                    );
                }
            }

            $trainings = DB::connection('mysql2')->table('ResumeCC_stt')->where('username', $oldResume->userName)->where('lang', $oldResume->lang)->get();

            if ($trainings) {
                foreach ($trainings as $training) {

                    $periodFrom = null;
                    if (is_int($training->fdate)) $periodFrom = date('Y-m-d', $training->fdate);

                    $periodTo = null;
                    if (is_int($training->tdate)) $periodTo = date('Y-m-d', $training->tdate);

                    CvTraining::create(
                        [
                            'cv_id' => $cv->id,
                            'name' => $training->pe_name,
                            'city' => $training->pe_city,
                            'country_id' => $this->convertCountry($training->pe_country),
                            'type' => $this->convertTrainingType($training->pe_type),
                            'study_area' => $this->convertStudyArea($training->pe_fieldstudy),
                            'course_name' => $training->pe_coursename,
                            'certificate_type' => $this->convertTrainingCertificate($training->pe_certtype),
                            'period_from' => $periodFrom,
                            'period_to' => $periodTo,
                        ]
                    );
                }
            }


        }

    }

    public function convertCvDates($availability)
    {
        $dates = [];
        if ($availability->WeekDays_1 == 1) $dates[] = 'mon';
        if ($availability->WeekDays_2 == 1) $dates[] = 'tus';
        if ($availability->WeekDays_3 == 1) $dates[] = 'wed';
        if ($availability->WeekDays_4 == 1) $dates[] = 'thu';
        if ($availability->WeekDays_5 == 1) $dates[] = 'fri';
        if ($availability->WeekDays_6 == 1) $dates[] = 'sat';
        if ($availability->WeekDays_7 == 1) $dates[] = 'sun';

        return json_encode($dates);
    }

    public function convertTrainingCertificate($certificate)
    {

        switch ($certificate) {
            case 0:
                $textType = "no_certificate";
                break;
            case 1:
                $textType = "participation";
                break;
            case 2:
                $textType = "technical_center";
                break;
            case 3:
                $textType = "accomplishment";
                break;
            case 4:
                $textType = "successful_accomplishment";
                break;
            case 5:
                $textType = "other";
                break;
            default:
                $textType = "";
                break;
        }

        return $textType;
    }


    public function convertVolunteerCountries($countries)
    {
        if ($countries) {
            if (strlen($countries) == 3)
                return json_encode($this->convertCountry($countries));

            $oldCountries = explode(',', $countries);

            $countriesJson = [];
            foreach ($oldCountries as $country) {
                $countriesJson[] = $this->convertCountry($country);
            }

            return json_encode($countriesJson);
        }

        return null;
    }

    public function convertWorkType($workTypes)
    {
        $types = [];

        if (strpos($workTypes, '1') !== false) {
            $types[] = 'organization_of_festivals';
        }

        if (strpos($workTypes, '2') !== false) {
            $types[] = 'tree_planting';
        }

        if (strpos($workTypes, '3') !== false) {
            $types[] = 'campaign';
        }

        return json_encode($types);

    }

    public function convertLevel($level)
    {
        switch ($level) {
            case 'lsq_Fluent':
                $textType = "fluent";
                break;
            case 'lsr_Good':
                $textType = "good";
                break;
            case 'lsq_Fair':
                $textType = "fair";
                break;
            case 'lsq_Poor':
                $textType = "poor";
                break;
            default:
                $textType = "";
                break;
        }

        return $textType;
    }

    public function convertTrainingType($type)
    {
        switch ($type) {
            case 0:
                $textType = "language_center";
                break;
            case 1:
                $textType = "computer_center";
                break;
            case 2:
                $textType = "technical_center";
                break;
            case 3:
                $textType = "other";
                break;
            default:
                $textType = "";
                break;
        }

        return $textType;
    }

    public function convertComputerLevel($level)
    {
        switch ($level) {
            case 1:
                $textType = "familiar";
                break;
            case 2:
                $textType = "good";
                break;
            case 3:
                $textType = "advanced";
                break;
            case 4:
                $textType = "professional";
                break;
            default:
                $textType = "";
                break;
        }

        return $textType;
    }

    public function convertTotalStudy($study)
    {
        switch ($study) {
            case 'lst_13months':
                $textType = "1_3_months";
                break;
            case 'lst_46months':
                $textType = "4_6_months";
                break;
            case 'lst_712months':
                $textType = "7_12_months";
                break;
            case 'lst_12years':
                $textType = "1_2_years";
                break;
            case 'lst_23years':
                $textType = "2_3_years";
                break;
            case 'lst_35years':
                $textType = "3_5_years";
                break;
            case 'lst_5years':
                $textType = "over_5_years";
                break;
            case 'lst_10years':
                $textType = "over_10_years";
                break;
            default:
                $textType = "";
                break;
        }

        return $textType;
    }

    public function convertEducationType($type)
    {
        switch ($type) {
            case 1:
                $textType = "kindergarten";
                break;
            case 2:
                $textType = "primary_school";
                break;
            case 3:
                $textType = "secondary_school";
                break;
            case 4:
                $textType = "high_school";
                break;
            case 5:
                $textType = "college";
                break;
            case 6:
                $textType = "university";
                break;
            case 7:
                $textType = "academy";
                break;
            case 8:
                $textType = "training_center";
                break;
            case 9:
                $textType = "institute";
                break;
            case 10:
                $textType = "aspirantura";
                break;
            case 11:
                $textType = "seminary";
                break;
            case 12:
                $textType = "specialized_educational_institution";
                break;
            default:
                $textType = "";
                break;
        }

        return $textType;
    }

    public function convertEducationDegree($degree)
    {
        switch ($degree) {
            case 2:
                $textType = "attestat";
                break;
            case 3:
                $textType = "k-12_certificate";
                break;
            case 4:
                $textType = "graduate_diploma";
                break;
            case 5:
                $textType = "BA";
                break;
            case 6:
                $textType = "5_year_graduate_diploma";
                break;
            case 7:
                $textType = "MA";
                break;
            case 8:
                $textType = "MBA";
                break;
            case 9:
                $textType = "PHD";
                break;
            case 10:
                $textType = "candidate_of_sciences";
                break;
            case 11:
                $textType = "professor";
                break;
            case 12:
                $textType = "incomplete";
                break;
            default:
                $textType = "";
                break;
        }

        return $textType;
    }

    public function convertStudyArea($area)
    {
        switch ($area) {
            case 1:
                $textType = "advertizing_pr";
                break;
            case 2:
                $textType = "arts_entertainment";
                break;
            case 3:
                $textType = "diplomacy";
                break;
            case 4:
                $textType = "school_education";
                break;
            case 5:
                $textType = "engineering_real_estate";
                break;
            case 6:
                $textType = "finance_insurance";
                break;
            case 7:
                $textType = "health_care_chemistry";
                break;
            case 8:
                $textType = "human_resources";
                break;
            case 9:
                $textType = "IT";
                break;
            case 10:
                $textType = "journalism_publishing";
                break;
            case 11:
                $textType = "law_legal";
                break;
            case 12:
                $textType = "linguistics";
                break;
            case 13:
                $textType = "nature_agriculture";
                break;
            case 14:
                $textType = "tourism_transportation";
                break;
            default:
                $textType = "";
                break;
        }

        return $textType;
    }

    public function convertSalaryCurrency($currency)
    {
        switch ($currency) {
            case 1:
                $textType = "amd";
                break;
            case 2:
                $textType = "rur";
                break;
            case 3:
                $textType = "eur";
                break;
            case 4:
                $textType = "usd";
                break;
            default:
                $textType = "";
                break;
        }

        return $textType;
    }

    public function convertSalaryPeriod($period)
    {
        switch ($period) {
            case 1:
                $textType = "hourly";
                break;
            case 2:
                $textType = "daily";
                break;
            case 3:
                $textType = "weekly";
                break;
            case 4:
                $textType = "monthly";
                break;
            case 5:
                $textType = "yearly";
                break;
            default:
                $textType = "";
                break;
        }

        return $textType;
    }

    public function createCVDependant($cvId, $name, $birthday, $relationship)
    {
        CvDependant::create(
            [
                'cv_id' => $cvId,
                'name' => $name,
                'birthday' => $birthday,
                'relationship' => $relationship
            ]
        );
    }

    public function convertHeight($height)
    {
        switch ($height) {
            case 1:
                $textType = "centimeters";
                break;
            case 2:
                $textType = "meters";
                break;
            case 3:
                $textType = "inches";
                break;
            case 4:
                $textType = "feet";
                break;
            default:
                $textType = "";
                break;
        }

        return $textType;
    }

    public function convertWeight($weight)
    {
        switch ($weight) {
            case 1:
                $textType = "kilograms";
                break;
            case 2:
                $textType = "pounds";
                break;
            default:
                $textType = "";
                break;
        }

        return $textType;
    }

    public function convertServe($serve)
    {
        switch ($serve) {
            case 1:
                $textType = "not_served";
                break;
            case 2:
                $textType = "in_service";
                break;
            case 3:
                $textType = "served";
                break;
            case 4:
                $textType = "exempted";
                break;
            case 5:
                $textType = "no_intensions";
                break;
            default:
                $textType = "";
                break;
        }

        return $textType;
    }

    public function convertRelationship($relationship)
    {
        switch ($relationship) {
            case 1:
                $textType = "wife";
                break;
            case 2:
                $textType = "husband";
                break;
            case 3:
                $textType = "son";
                break;
            case 4:
                $textType = "daughter";
                break;
            case 5:
                $textType = "father";
                break;
            case 6:
                $textType = "mother";
                break;
            case 7:
                $textType = "brother";
                break;
            case 8:
                $textType = "sister";
                break;
            default:
                $textType = "";
                break;
        }

        return $textType;
    }


    public function convertCountry($oldCountryId)
    {
        $countryId = 13; //armenia

        if ($oldCountryId > 0) {
            if (Country::where('old_country_id', $oldCountryId)->first()) {
                $countryId = Country::where('old_country_id', $oldCountryId)->first()->id;
            }
        }

        return $countryId;
    }

    public function convertCvPhone($cv)
    {
        $phones = [];

        if (strlen($cv->phone1) > 5) {
            $phones[] = array('phone' => $cv->phone1, 'type' => $this->getPhoneType($cv->phoneType1));
        }
        if (strlen($cv->phone2) > 5) {
            $phones[] = array('phone' => $cv->phone2, 'type' => $this->getPhoneType($cv->phoneType2));
        }
        if (strlen($cv->phone3) > 5) {
            $phones[] = array('phone' => $cv->phone3, 'type' => $this->getPhoneType($cv->phoneType3));
        }

        return json_encode($phones);
    }

    public function getPhoneNumber($user)
    {
        $phones = [];

        if (strlen($user->phone1number) > 5) {
            $phones[] = array('phone' => $user->phone1number, 'type' => $this->getPhoneType($user->phone1type));
        }
        if (strlen($user->phone2number) > 5) {
            $phones[] = array('phone' => $user->phone2number, 'type' => $this->getPhoneType($user->phone2type));
        }
        if (strlen($user->phone3number) > 5) {
            $phones[] = array('phone' => $user->phone3number, 'type' => $this->getPhoneType($user->phone3type));
        }

        return json_encode($phones);
    }

    public function getPhoneNumberCompanies($user)
    {
        $phones = [];

        if (strlen($user->phone1number) > 5) {
            $phones[] = array('phone' => $user->phone1number, 'type' => $this->getPhoneType($user->phon1type));
        }
        if (strlen($user->phone2number) > 5) {
            $phones[] = array('phone' => $user->phone2number, 'type' => $this->getPhoneType($user->phon2type));
        }
        if (strlen($user->phone3number) > 5) {
            $phones[] = array('phone' => $user->phone3number, 'type' => $this->getPhoneType($user->phon3type));
        }

        return json_encode($phones);
    }

    public function getPhoneType($type)
    {
        switch ($type) {
            case 1:
                $textType = "home";
                break;
            case 2:
                $textType = "home_2";
                break;
            case 3:
                $textType = "business";
                break;
            case 4:
                $textType = "business_2";
                break;
            case 5:
                $textType = "daytime";
                break;
            case 6:
                $textType = "evening";
                break;
            case 7:
                $textType = "primary";
                break;
            case 8:
                $textType = "mobile";
                break;
            case 9:
                $textType = "fax";
                break;
            default:
                $textType = "primary";
                break;
        }

        return $textType;
    }

    public function convertCountries()
    {
        $oldCountries = DB::connection('mysql2')->table('allcountry')->get()->toArray();

        foreach ($oldCountries as $oldCountry) {
            $country = Country::where('name', 'like', '%' . $oldCountry->short_country . '%')->first();
            if ($country) {
                $country->old_country_id = $oldCountry->country_id;
                $country->save();
            }
        }

    }

    public function convertLegalType($legal)
    {
        switch ($legal) {
            case 1:
                $textType = "local_ngo";
                break;
            case 9:
                $textType = "llc";
                break;
            case 10:
                $textType = "ltd";
                break;
            case 11:
                $textType = "cjsc";
                break;
            case 12:
                $textType = "ojsc";
                break;
            case 13:
                $textType = "government";
                break;
            case 14:
                $textType = "private";
                break;
            case 15:
                $textType = "international_ngo";
                break;
            case 16:
                $textType = "fund";
                break;
            case 17:
                $textType = "foundation";
                break;
            case 18:
                $textType = "charity";
                break;
            case 19:
                $textType = "benevolent_union";
                break;
            case 20:
                $textType = "general_benevolent_union";
                break;
            case 21:
                $textType = "humanitar_organization";
                break;
            case 22:
                $textType = "other";
                break;
            default:
                $textType = "other";
                break;
        }

        return $textType;
    }

    public function convertOrganizationType($type)
    {

        switch ($type) {
            case 1:
                $textType = "direct_employer";
                break;
            case 2:
                $textType = "staffing_agency";
                break;
            case 3:
                $textType = "educational_institution";
                break;
            case 4:
                $textType = "library";
                break;
            case 5:
                $textType = "other";
                break;
            default:
                $textType = "other";
                break;
        }

        return $textType;

    }

    public function convertActivity($activity)
    {
        switch ($activity) {
            case 1:
                $textType = "finance/insurance";
                break;
            case 2:
                $textType = "administration";
                break;
            case 3:
                $textType = "advertising/pr";
                break;
            case 4:
                $textType = "agriculture";
                break;
            case 5:
                $textType = "art/publishing";
                break;
            case 6:
                $textType = "banking";
                break;
            case 7:
                $textType = "construction";
                break;
            case 8:
                $textType = "customer_service";
                break;
            case 9:
                $textType = "education/training";
                break;
            case 10:
                $textType = "engineering/architecture";
                break;
            case 11:
                $textType = "entrepreneurial/startup";
                break;
            case 12:
                $textType = "entry_level";
                break;
            case 13:
                $textType = "government";
                break;
            case 14:
                $textType = "health_care";
                break;
            case 15:
                $textType = "hospitality/travel";
                break;
            case 16:
                $textType = "human_resources";
                break;
            case 17:
                $textType = "insurance";
                break;
            case 18:
                $textType = "internet/new_media";
                break;
            case 19:
                $textType = "law_enforcement/security";
                break;
            case 20:
                $textType = "legal";
                break;
            case 21:
                $textType = "management_consulting";
                break;
            case 22:
                $textType = "manufacturing_operations";
                break;
            case 23:
                $textType = "marketing";
                break;
            case 24:
                $textType = "non_profit/volunteer";
                break;
            case 25:
                $textType = "pharmaceutical/biotech";
                break;
            case 26:
                $textType = "real_estate";
                break;
            case 27:
                $textType = "restaurant/food_service";
                break;
            case 28:
                $textType = "retail";
                break;
            case 29:
                $textType = "sales";
                break;
            case 30:
                $textType = "social";
                break;
            case 31:
                $textType = "teaching";
                break;
            case 32:
                $textType = "tech_contract";
                break;
            case 33:
                $textType = "technology";
                break;
            case 34:
                $textType = "telecommunications";
                break;
            case 35:
                $textType = "temp_jobs";
                break;
            case 36:
                $textType = "translation/interpretation";
                break;
            case 37:
                $textType = "transportation/logistics";
                break;
            case 38:
                $textType = "information_technology";
                break;
            default:
                $textType = "information_technology";
                break;
        }

        return $textType;
    }

    public function convertLang($lang)
    {
        switch ($lang) {
            case '_eng':
                $textType = "en";
                break;
            case '_rus':
                $textType = "ru";
                break;
            case '_arm':
                $textType = "hy";
                break;
            default:
                $textType = "en";
                break;
        }

        return $textType;
    }

    public function convertTitle($title)
    {
        switch ($title) {
            case 1:
                $textType = "mr";
                break;
            case 2:
                $textType = "mrs";
                break;
            case 3:
                $textType = "dr";
                break;
            case 4:
                $textType = "ms";
                break;
            case "Ms":
                $textType = "ms";
                break;
            case "Mr":
                $textType = "mr";
                break;
            case "Mrs":
                $textType = "mrs";
                break;
            default:
                $textType = "mr";
                break;
        }

        return $textType;
    }

    public function convertAnnouncementType($type)
    {
        switch ($type) {
            case "JO":
                $textType = 1;
                break;
            case "VO":
                $textType = 2;
                break;
            case "IO":
                $textType = 3;
                break;
            case "EO":
                $textType = 4;
                break;
            case "TG":
                $textType = 5;
                break;
            case "FP":
                $textType = 6;
                break;
            case "SP":
                $textType = 7;
                break;
            case "ET":
                $textType = 8;
                break;
            case "NW":
                $textType = 9;
                break;
            case "PN":
                $textType = 10;
                break;
            case "CM":
                $textType = 11;
                break;
            default:
                $textType = 1;
                break;
        }

        return $textType;
    }
}
