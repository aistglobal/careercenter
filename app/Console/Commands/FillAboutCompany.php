<?php

namespace App\Console\Commands;

use App\Job;
use Illuminate\Console\Command;

class FillAboutCompany extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fillAboutCompany';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       foreach (Job::whereHas('data')->with('data')->cursor() as $item)
       {
           if(isset($item->data->about_company) && isset($item->user) && isset($item->user->organization))
           {
               if(!isset($item->user->organization->about_company) || $item->user->organization->about_company=='')
               {
                   $item->user->organization->about_company = $item->data->about_company;
                   $item->user->organization->save();
               }

           }
       }
       dd('yeeee');
    }
}
