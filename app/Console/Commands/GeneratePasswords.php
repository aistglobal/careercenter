<?php

namespace App\Console\Commands;

use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class GeneratePasswords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate-passwords';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $updated_at = '2019-11-05 09:00:00';
        $updated_at = Carbon::parse($updated_at)->format('Y-m-d H:i:s');
        $users = User::where('status', 1)->where('password', "not like", "$2y%")->where('updated_at', '<', $updated_at)
            ->whereHas('role', function ($a) {
                return $a->whereIn('name', ['organization', 'pro_organization']);
            })->get();

        $subject = 'CareerCenter - Գաղտնաբառի Փոփոխություն/ Password Reset';
        $subject_individual = 'CareerCenter - Նոր հնարավորություններ careercenter.am-ում';
        $data = [];
        foreach ($users as $item) {
            if ($item->email) {
                $arr = explode(',', $item->email);
                if ($item->is_organization == 1) {
                    $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
                    $password = substr($random, 0, 8);
                    $item->password = Hash::make($password);
                    $item->save();
                    $data['password'] = $password;
                    try {
                        Mail::send('email.reset-password', ['data' => $data], function ($message) use ($subject, $item, $arr) {
                            $message->to($arr[0])->subject($subject);
                        });
                    } catch (\Exception $exception) {
                    }
                }
                else {
                    try {
                        Mail::send('email.reset-password-individual', ['data' => $data], function ($message) use ($subject_individual, $item, $arr) {
                            $message->to($arr[0])->subject($subject_individual);
                        });
                    } catch (\Exception $exception) {
                    }
                }

            }
        }
        dd('end');
    }
}