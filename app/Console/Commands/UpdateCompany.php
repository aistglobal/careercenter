<?php

namespace App\Console\Commands;

use App\Organization;
use Illuminate\Console\Command;

class UpdateCompany extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateCompany';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $organizations = Organization::all();
        foreach ($organizations as $item)
        {
            $user = $item->user;
            if(isset($user))
            {
                $user->organization_id = $item->id;
                $user->save();
            }


        }
    }
}
