<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PublishTranslations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'publish_translations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $groups = [
            'auth', 'announcements', 'category', 'countries', 'fields_of_activity', 'home', 'jobs', 'legal_structure', 'login', 'menu',
            'org_type', 'pagination', 'passwords', 'profile_menu', 'profile_tables', 'register_step_1', 'register_step_2',
            'title', 'types', 'user_category', 'validation', 'blogs', 'reviews', 'resume', 'company', 'cv', 'cv_availability',
            'cv_computer_skills', 'cv_education', 'cv_language_skills', 'cv_personal', 'cv_questions', 'cv_reference', 'cv_training',
            'cv_volunteering', 'appliers', 'languages', 'language', 'currency', 'period', 'months', 'resource_category', 'users', 'permissions',
            'availability', 'contact', 'pages', 'resources', 'settings', 'sub_category', 'sub_category_advertising/pr', 'sub_category_arts/entertainment',
            'sub_category_diplomacy', 'sub_category¬engineering/real_estate', 'sub_category_finance/insurance', 'sub_category_health_care/chemistry',
            'sub_category_human_resources', 'sub_category_it', 'sub_category_journalism/publishing', 'sub_category_law/legal', 'sub_category_linguistics',
            'sub_category_nature/agriculture', 'sub_category_school_education', 'sub_category_tourism/transportation','about'
        ];
        foreach ($groups as $item) {
            $this->call('translations:export', ['group' => $item]);
        }
    }
}
