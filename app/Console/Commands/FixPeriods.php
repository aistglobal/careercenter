<?php
namespace App\Console\Commands;
use App\CvEducation;
use App\CvExperience;
use App\CvTraining;
use App\CvVisa;
use Illuminate\Console\Command;
class FixPeriods extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix-periods';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cvEducations = CvEducation::all();
        foreach ($cvEducations as $cvEducation) {
            $from = explode('-', $cvEducation->period_from);
            $to = explode('-', $cvEducation->period_to);
            if (count($from) > 1) {
                if($from[0]=='' || $from[1]=='' )
                {
                    $cvEducation->period_from = '-';
                }
                else
                {
                    if (strlen($from[0]) <= 2) {
                        $cvEducation->period_from = $from[1] . '-' . $from[0];
                    }
                }
            }

            if (count($to) > 1) {
                if($to[0]=='' ||$to[1]=='' )
                {
                    $cvEducation->period_to = '-';
                }
                else
                {
                    if (strlen($to[0]) <= 2) {
                        $cvEducation->period_to = $to[1] . '-' . $to[0];
                    }
                }
            }
            $cvEducation->save();
        }
        $cvExperiences = CvExperience::all();
        foreach ($cvExperiences as $cvExperience) {
            $from = explode('-', $cvExperience->period_from);
            $to = explode('-', $cvExperience->period_to);
            if (count($from) > 1) {

                if($from[0]=='' || $from[1]=='' )
                {
                    $cvExperience->period_from = '-';
                }
                else
                {
                    if (strlen($from[0]) <= 2) {
                        $cvExperience->period_from = $from[1] . '-' . $from[0];
                    }
                }

            }
            if (count($to) > 1) {
                if($from[0]=='' || $from[1]=='' )
                {
                    $cvExperience->period_to = '-';
                }
                else
                {
                    if (strlen($from[0]) <= 2) {
                        $cvExperience->period_to = $to[1] . '-' . $to[0];
                    }
                }
            }
            $cvExperience->save();
        }
        $cvTrainings = CvTraining::all();
        foreach ($cvTrainings as $cvTraining){
            $from = explode('-', $cvTraining->period_from);
            $to = explode('-', $cvTraining->period_to);
            if (count($from) > 1) {
                if($from[0]=='' || $from[1]=='' )
                {
                    $cvTraining->period_from = '-';
                }
                else
                {
                    if (strlen($from[0]) <= 2) {
                        $cvTraining->period_from = $from[1] . '-' . $from[0];
                    }
                }
            }
            if (count($to) > 1) {
                if($from[0]=='' || $from[1]=='' )
                {
                    $cvExperience->period_to = '-';
                }
                else
                {
                    if (strlen($from[0]) <= 2) {
                        $cvExperience->period_to = $to[1] . '-' . $to[0];
                    }
                }
            }
            $cvTraining->save();
        }
        $cvVisas = CvVisa::all();
        foreach ($cvVisas as $cvVisa){
            $from = explode('-', $cvVisa->period_from);
            $to = explode('-', $cvVisa->period_to);
            if (count($from) > 1) {
                if (strlen($from[0]) <= 2) {
                    $cvVisa->period_from = $from[1] . '-' . $from[0];
                }
            }
            if (count($to) > 1) {
                if (strlen($to[0]) <= 2) {
                    $cvVisa->period_to = $to[1] . '-' . $to[0];
                }
            }
            $cvVisa->save();
        }
        dd('ok');
    }
}