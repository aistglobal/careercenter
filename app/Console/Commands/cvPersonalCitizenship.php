<?php

namespace App\Console\Commands;

use App\CvPersonal;
use Illuminate\Console\Command;

class cvPersonalCitizenship extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cvPersonalCitizenship';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cvs = CvPersonal::all();
        foreach ($cvs as $item)
        {
            $citizenship=[];
            dump($item->id);
            $citizenship[] = $item->citizenship_id;
            $item->citizenship_id = json_encode($citizenship);
            $item->save();
        }
        dd('success');


    }
}
