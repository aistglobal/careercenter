<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class FillRoleUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fillRoleUser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = \App\User::all();
        foreach ($users as $item)
        {
            if(isset($item->role))
            {
                $role = \App\Role::where('name',$item->role)->first();
                if(isset($role))
                {
                    $item->attachRole($role->id);
                }

            }
        }
        dd('role_user');
    }
}
