<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CvReference extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cv_references';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'cv_id';
    protected $fillable = [
        'cv_id',
        'title',
        'full_name',
        'position',
        'relationship',
        'duration',
        'duration_type',
        'current_position',
        'email',
        'phone',

    ];
}
