<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CvExperience extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cv_experiences';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'cv_id';
    protected $fillable = [
        'cv_id',
        'name',
        'city',
        'country_id',
        'position',
        'position_field',
        'salary',
        'currency',
        'period',
        'period_from',
        'period_to',
        'job_description',
        'main_duties',
        'supervisor_name',
        'employer_numbers',
        'reason'
    ];
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
}
