<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ServiceCategory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'service_categories';
    public function data()
    {
        $lang = LaravelLocalization::getCurrentLocale();
        return $this->hasOne(ServiceCategoryData::class, 'service_category_id')->where('lang', $lang);
    }
    public function services()
    {
        return $this->hasMany(Service::class,'category_id');
    }
}
