<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResourceData extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'resource_data';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'resource_id';
    protected $fillable =
        [
            'resource_id','lang', 'name', 'note','link_to'
        ];
}
