<?php

namespace App\Channels;

use App\Services\PushNotificationService;
use Illuminate\Notifications\Notification;

class PushChannel
{
  /**
   * Send the given notification.
   *
   * @param  mixed $notifiable
   * @param  \Illuminate\Notifications\Notification $notification
   * @return void
   */
  public function send($notifiable, Notification $notification)
  {
    $data = $notification->toPush($notifiable);
    $pushData = $data['data'];
    $pushData['notification_id'] = $notification->id;
    $badge = count($notifiable->unreadNotifications);
    if (\count($notifiable->getTokens())) {
      (new PushNotificationService())->sendNotifications($data['title'], $data['message'], $pushData, $notifiable->getTokens(), $badge);
    }
  }
}
