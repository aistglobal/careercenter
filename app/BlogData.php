<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogData extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blogs_data';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'blog_id';
    protected $fillable = [
        'blog_id','lang','title','content','short_description','meta_keywords','meta_description','meta_title'
    ];
}
