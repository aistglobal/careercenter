<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CvVisa extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cv_visas';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'cv_id';
    protected $fillable = [
        'cv_id',
        'type',
        'country_id',
        'period_from',
        'period_to'
    ];
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
}
