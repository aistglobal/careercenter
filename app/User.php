<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Spatie\Activitylog\Traits\LogsActivity;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Zizaco\Entrust\Traits\EntrustUserTrait;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use EntrustUserTrait;
    use LogsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'username',
        'middle_name',
        'title',
        'website',
        'phone',
        'is_organization',
        'birthday',
        'citizenship_id',
        'residence_country_id',
        'status',
        'image',
        'sid',
        'reset_token',
        'organization_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static $logAttributes = ['role'];

    public function address()
    {
        return $this->morphMany('App\UserAddress', 'addressable');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id');
    }

    public function job()
    {
        return $this->hasMany(Job::class, 'user_id');
    }

    public function posted_job()
    {
        return $this->hasMany(Job::class, 'user_id')->where('status', 'posted')->whereDate('expiration_date', '>', now());
    }

    public function applied_job()
    {
        return $this->belongsToMany(Job::class, 'apply_jobs', 'user_id', 'job_id');
    }

    public function role()
    {
        return $this->belongsToMany('App\Role');
    }

    public function cv()
    {
        $lang = LaravelLocalization::getCurrentLocale();
        return $this->hasMany(Cv::class, 'user_id')->where('lang', $lang);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookmarks()
    {
        return $this->hasMany(Bookmark::class, 'user_id');
    }

    /**
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Override the mail body for reset password notification mail.
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\MailResetPasswordNotification($token));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function devices()
    {
        return $this->hasMany(UserDevice::class);
    }

    /**
     * @return array
     */
    public function getTokens()
    {
        return $this->devices()->pluck('token')->toArray();
    }
    public function bankDetails()
    {
        return $this->hasOne(UserBank::class, 'user_id');
    }
    public function products()
    {
        return $this->hasMany(UserProduct::class, 'user_id');
    }
    public function jobProducts()
    {
        return $this->hasMany(UserProduct::class, 'user_id')->where('type','announcement')->where('quantity','!=',0);
    }
}
