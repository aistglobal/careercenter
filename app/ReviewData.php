<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewData extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'review_data';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'review_id';
    protected $fillable =
        [
            'review_id', 'lang', 'name', 'review', 'profession'
        ];
}
