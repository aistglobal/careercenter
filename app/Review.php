<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Review extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reviews';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    protected $fillable = ['image'];
    public function data()
    {
        $lang = LaravelLocalization::getCurrentLocale();
        return $this->hasOne(ReviewData::class, 'review_id')->where('lang', $lang);
    }
}
