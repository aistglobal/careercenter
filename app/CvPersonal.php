<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CvPersonal extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cv_personals';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'cv_id';
    protected $fillable = [
        'cv_id',
        'birthday',
        'marital_status',
        'sex',
        'disabled',
        'disabled_description',
        'height',
        'weight',
        'citizenship_id',
        'nationality_id',
        'image',
        'official_service',
        'dependants',
        'height_measurement',
        'weight_measurement',
    ];

    public function nationality()
    {
        return $this->belongsTo(Country::class, 'nationality_id');
    }
}
