<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CvAttachment extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cv_attachments';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'cv_id';
    protected $fillable = [
        'cv_id',
        'attachment',
        'description',
        'is_image'
    ];
}
