<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'addresses';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    protected $fillable = ['addressable_type','addressable_id','country_id','city','zip','address','type','address_type'];
    public function addressable()
    {
        return $this->morphTo();
    }
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }



}
