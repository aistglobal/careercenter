<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * @var string
     */
    protected $table = 'categories';

    /**
     * @var array
     */
    protected $fillable = ['name', 'category_id'];
    public function child()
    {
        return $this->hasMany(Category::class, 'category_id');
    }
    public function parent()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
    public function job()
    {
        return $this->belongsToMany(Job::class, 'job_categories', 'job_id', 'category_id');
    }
}
