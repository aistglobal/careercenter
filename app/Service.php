<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Service extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'services';
    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    protected $fillable = ['category_id','co_id','product_id','amd','usd','euro'];
    public function data()
    {
        $lang = LaravelLocalization::getCurrentLocale();
        return $this->hasOne(ServiceData::class, 'service_id')->where('lang', $lang);
    }
    public function category()
    {
        return $this->belongsTo(ServiceCategory::class, 'category_id', 'id');
    }
}
