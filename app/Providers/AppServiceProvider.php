<?php

namespace App\Providers;

use App\Type;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Lang;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use View;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('phone', function($attribute, $value, $parameters, $validator) {
            return preg_match('%^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$%i', $value) && strlen($value) >= 5;
        });
        $types = [];
        Validator::extend('current_password', function ($attribute, $value, $parameters, $validator) {
            $user = auth()->user();
            return $user && Hash::check($value, $user->password);
        });
        Validator::extend('same_password', function ($attribute, $value, $parameters, $validator) {
            $user = auth()->user();
            return $user && !Hash::check($value, $user->password);
        });
        Validator::extend('recaptcha', function ($attribute, $value, $parameters, $validator) {
            $client = new Client();

            $response = $client->post(
                'https://www.google.com/recaptcha/api/siteverify',
                ['form_params'=>
                    [
                        'secret'=>env('GOOGLE_RECAPTCHA_SECRET'),
                        'response'=>$value
                    ]
                ]
            );

            $body = json_decode((string)$response->getBody());
            return $body->success;
        });
        if(  Schema::hasTable('types')){
            $types = Type::all();
        };

        View::composer('profile.menu', function ($view) use($types) {
            $view->with('types', $types);
        });
        View::composer('layouts.app', function ($view) use($types) {
            $view->with('types', $types);
        });
    }
}
