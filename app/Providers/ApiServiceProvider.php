<?php

namespace App\Providers;


use App\Contracts\ApiServiceInterface;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    $this->app->singleton(ApiServiceInterface::class,ApiServiceProvider::class);
  }
}