<?php

namespace App\Providers;


use App\Contracts\ImageServiceInterface;
use App\Contracts\JobServiceInterface;
use App\Contracts\ProfileServiceInterface;
use App\Contracts\SeoServiceInterface;
use App\Services\ImageService;
use App\Services\JobService;
use App\Services\ProfileService;
use App\Services\PushNotificationService;
use App\Services\SeoService;
use Carbon\Laravel\ServiceProvider;

class JobServiceProvider extends ServiceProvider
{
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    $this->app->singleton(JobServiceInterface::class, JobService::class);
    $this->app->singleton(ImageServiceInterface::class, ImageService::class);
    $this->app->singleton(SeoServiceInterface::class, SeoService::class);
    $this->app->singleton(ProfileServiceInterface::class, ProfileService::class);
    $this->app->singleton(PushNotificationService::class );
  }
}