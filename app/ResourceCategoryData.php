<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResourceCategoryData extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'resource_category_data';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'resource_id';
    protected $fillable =
        ['lang','name','resource_id'];
}
