<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceData extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'service_data';
    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'service_id';
    protected $fillable = ['service_id','lang','name'];
}
