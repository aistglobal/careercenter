<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CvDependant extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cv_dependants';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'cv_id';
    protected $fillable = [
        'cv_id',
        'name',
        'birthday',
        'relationship'
    ];
}
