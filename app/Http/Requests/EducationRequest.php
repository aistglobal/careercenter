<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EducationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'city' => 'required',
            'type' => 'required',
            'country_id' => 'required',
            'department' => 'required',
            'study_area' => 'required',
            'major_field' => 'required',
            'edu_year_from' => 'required',
            'edu_year_to' => 'required',
            'edu_month_from' => 'required',
            'edu_month_to' => 'required',
            'degree' => 'required'

        ];
    }
}
