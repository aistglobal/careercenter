<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReferenceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'full_name'=>'required',
            'position'=>'required',
            'relationship'=>'required',
            'duration'=>'required',
            'duration_type'=>'required',
            'current_position'=>'required',
            'email'=>'required|email',
            'phone'=>'required|phone',
        ];
    }
    public function messages()
    {
        return [
            'phone.phone' => 'Wrong phone format',
        ];
    }
}
