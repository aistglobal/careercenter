<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegisterStep1Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'password' => 'required|min:8|confirmed',
            'username' => 'required|min:2|max:100|unique:users,username',
            'agree_terms' => 'required',
            'role_id' => 'required'
        ];

        return $rules;
    }
}
