<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'performer' => 'required',
            'performer_address' => 'required',
            'performer_bank' => 'required',
            'performer_tax' => 'required',
            'performer_account' => 'required',
            'client' => 'required',
            'client_address' => 'required',
            'booking_number' => 'required',
            'account_product' => 'required',
            'date' => 'required',
            'currency' => 'required',
            'quantity' => 'required|array',
            'quantity.*' => 'required',
            'measure' => 'required|array',
            'measure.*' => 'required',
            'service_id' => 'required|array',
            'service_id.*' => 'required',
            'amount' => 'required|array',
            'amount.*' => 'required',
            'sum' => 'required|array',
            'sum.*' => 'required',

        ];

        if (isset($_REQUEST['individual'])) {
            $rules['passport'] = 'required';
        } else {
            $rules['client_bank'] = 'required';
            $rules['client_tax'] = 'required';
            $rules['client_account'] = 'required';
        }
        return $rules;

    }
}
