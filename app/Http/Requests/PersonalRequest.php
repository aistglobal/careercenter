<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'birthday' => 'required',
            'sex' => 'required',
            'citizenship_id' => 'required',
            'height' => 'nullable|numeric',
            'weight' => 'nullable|numeric',
        ];
    }
}
