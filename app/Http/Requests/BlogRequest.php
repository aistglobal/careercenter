<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $localeCode = Config::get('app.locale');
        $rules =
            [
                'title_'.$localeCode => 'required',
                'content_'.$localeCode => 'required',
                'short_description_'.$localeCode => 'required',
                'meta_description_'.$localeCode => 'required',
                'meta_keywords_'.$localeCode => 'required',
                'meta_title_'.$localeCode => 'required',

            ];
        if(\Illuminate\Http\Request::method() == 'POST')
        {
            $rules['image'] = 'required';
        }

        return $rules;


    }
}
