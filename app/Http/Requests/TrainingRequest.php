<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TrainingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'city' => 'required',
            'country_id' => 'required',
            'type' => 'required',
            'study_area' => 'required',
            'course_name' => 'required',
            'certificate_type' => 'required',
            'training_year_from' => 'required',
            'training_year_to' => 'required',
            'training_month_from' => 'required',
            'training_month_to' => 'required',
        ];
    }
}
