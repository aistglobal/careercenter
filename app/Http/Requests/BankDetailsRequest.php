<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BankDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (isset($_REQUEST['individual'])) {
            return [
                'customer' => 'required',
                'address' => 'required',
                'passport' => 'required',

            ];
        } else {

            return [
                'customer' => 'required',
                'bank' => 'required',
                'tax' => 'required',
                'account' => 'required',
                'legal_address' => 'required',
                'physical_address' => 'required',
                'signing_person_1' => 'required',
                'position_1' => 'required',
            ];
        }


    }
}
