<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Tymon\JWTAuth\Contracts\Providers\Auth;

class JobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $localeCode = Config::get('app.locale');
        $rules = [];
        if ($_REQUEST['action'] == 'preview') {
            $rules = [
                'salary_' . $localeCode => 'nullable',
                'organization_' . $localeCode => 'required',
                'title_' . $localeCode => 'required',
                'term_' . $localeCode => 'nullable|max:255',
                'location_city_' . $localeCode => 'required',
                'contact_person_' . $localeCode => 'required',
                'phone' => 'required',
                'email' => 'required',
                'location_country_id' => 'required',
                'contact_title_' . $localeCode => 'required',
                'category' => 'required',
                'expiration_date' => 'required',
            ];
            if(\Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement'))
            {
                $rules['menu_' . $localeCode] = 'required';
            }

            if ($_REQUEST['type'] != 'publication') {
                $rules['description_' . $localeCode] = 'required';
            }
            if ($_REQUEST['type'] == 'publication') {
                $rules['start_date_' . $localeCode] = 'required';
            }

            if ($_REQUEST['type'] == 'publication') {
                $rules['author_' . $localeCode] = 'required';
            }
            if ($_REQUEST['type'] == 'publication') {
                $rules['language_' . $localeCode] = 'required';
            }
            if ($_REQUEST['type'] == 'job_opp' || $_REQUEST['type'] == 'volunteering' || $_REQUEST['type'] == 'internship') {
                $rules['requirements_' . $localeCode] = 'required';
            }
 
            if ($_REQUEST['type'] != 'news' && $_REQUEST['type'] != 'event' && $_REQUEST['type'] != 'publication') {
                $rules['deadline_' . $localeCode] = 'required';
            }
            if ($_REQUEST['type'] == 'event' || $_REQUEST['type'] == 'publication' || $_REQUEST['type'] == 'competition') {
                $rules['term_' . $localeCode] = 'required|max:255';
            }
        }
        return $rules;

    }

}
