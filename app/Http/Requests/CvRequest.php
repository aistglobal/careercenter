<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CvRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'title' => 'required',
            'residence_country_id' => 'required',
            'phone' => 'required|array',
            'phone.*' => 'required|phone',
            'city' => 'required|array',
            'city.*' => 'required',
            'country' => 'required|array',
            'country.*' => 'required',
            'address' => 'required|array',
            'address.*' => 'required',
//            'zip' => 'required|array',
//            'zip.*' => 'required|min:4|max:10',
            'email' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'phone.*.phone' => 'Wrong phone format',
        ];
    }
}
