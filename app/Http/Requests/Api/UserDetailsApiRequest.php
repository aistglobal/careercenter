<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UserDetailsApiRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    $id = Auth::id();


      $rules = [
          'email' => 'required|email|max:255|unique:users,email,' . $id,
          'first_name' => 'required|max:255|alpha_dash',
          'last_name' => 'required|max:255|alpha_dash',
          'residence_country_id' => 'required',
          'phone' => 'required',
          'phone.*' => 'phone',
//          'zip' => 'nullable|array',
//          'zip.*' => 'nullable|min:4|max:10',
          'username' => 'required|min:3|max:100|alpha_dash|unique:users,username,' . $id
      ];

    return $rules;
  }
  public function messages()
  {
    return [
        'phone.*.phone' => 'Wrong phone format',
    ];
  }
}
