<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class PasswordApiRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
        'current_password' => ['required_with:password',"current_password"],
        'password' => 'required|confirmed|same_password|min:6',
        'password_confirmation' => 'required',
    ];
  }
  public function messages()
  {
    return [
        'password.required' => 'The password field is required',
        'password.confirmed' => 'The password confirmation does not match',
        'password.same_password' => 'The new password and the current password are same',
        'password.min' => 'The password must be at least 6 characters',
        'current_password.required' => 'The current password field is required',
        'password_confirmation.required' => 'The confirmation password field is required',
        'current_password.current_password' => 'The current password is wrong',
    ];
  }
}
