<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => 'required|max:255',
            'name_ru' => 'required|max:255',
            'name_hy' => 'required|max:255',
            'category_id' => 'required',
            'co_id' => 'required',
            'product_id' => 'required',
            'amd' => 'required',
            'usd' => 'required',
            'euro' => 'required',
        ];
    }
}
