<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class FinalRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($_REQUEST['is_organization'] == 1) {
            $rules =[
                'email' => 'required|max:255|unique:users,email',
                'first_name' => 'required|max:255|alpha_dash',
                'last_name' => 'required|max:255|alpha_dash',
                'name' => 'required',
                'phone.*' => 'required',
//                'phone.*' => 'phone',
//                'zip' => 'nullable|array',
//                'zip.*' => 'nullable|min:4|max:10',
                'image' => 'image|max:3000',
                'position' => 'required'
            ];
        }
        else {
            $rules =  [
                'email' => 'required|max:255|unique:users,email',
                'first_name' => 'required|max:255|alpha_dash',
                'last_name' => 'required|max:255|alpha_dash',
                'residence_country_id' => 'required',
                'phone.*' => 'required',
//                'phone.*' => 'phone',
//                'zip' => 'nullable|array',
//                'zip.*' => 'nullable|min:4|max:10',
                'image' => 'image|max:3000'
            ];

        }
        return $rules;


    }
    public function messages()
    {
        return [
            'phone.*.phone' => 'Wrong phone format',
        ];
    }
}
