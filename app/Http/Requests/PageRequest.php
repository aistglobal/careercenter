<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =
            [
                'title_en' => 'required',
                'content_en' => 'required',
                'meta_title_en' => 'required',
                'meta_description_en' => 'required',
                'meta_keywords_en' => 'required',

            ];
        if(\Illuminate\Http\Request::method() == 'POST')
        {
            $rules['image'] = 'required';
        }
        return $rules;

    }
}
