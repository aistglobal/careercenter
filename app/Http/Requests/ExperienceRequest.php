<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExperienceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'city' => 'required',
            'country_id' => 'required',
            'position' => 'required',
            'position_field' => 'required',
            'salary' => 'nullable|numeric',
            'main_duties' => 'required',
            'exp_year_from' => 'required',

            'exp_month_from' => 'required',


        ];
    }
}
