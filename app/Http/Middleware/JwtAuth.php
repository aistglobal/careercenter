<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Tymon\JWTAuth\Facades\JWTAuth as JwtCoreaAuth;
class JwtAuth extends Middleware
{
  // Override handle method
  public function handle($request, Closure $next, ...$guards)
  {
    try {
      JwtCoreaAuth::parseToken()->authenticate();
    } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
      return response()->json(['message'=> $e->getMessage()],463);
      // do whatever you want to do if a token is expired
    } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
      return response()->json(['message'=> $e->getMessage()],401);
      // do whatever you want to do if a token is invalid
    } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
      return response()->json(['message'=> $e->getMessage()],401);
      // do whatever you want to do if a token is not present
    }
    return $next($request);
  }
}