<?php

namespace App\Http\Middleware;

use \Mcamara\LaravelLocalization\Middleware\LaravelLocalizationRedirectFilter as McamaraLaravelLocalizationRedirectFilter;

class LaravelLocalizationRedirectFilter extends McamaraLaravelLocalizationRedirectFilter
{
    /**
     * The URIs that should not be redirected to their localized versions.
     *
     * @var array
     */
    protected $except = [
        'phpmyadmin',
        'webmail'
    ];
}