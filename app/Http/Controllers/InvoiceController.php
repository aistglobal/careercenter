<?php

namespace App\Http\Controllers;

use App\Http\Requests\InvoiceRequest;
use App\Invoice;
use App\Service;
use App\ServiceCategory;
use App\User;
use App\UserBank;
use App\UserProduct;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rules\In;
use Barryvdh\DomPDF\Facade as PDF;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $search = $request->search;
        $order = 20;
        $status = $request->status;
        if (isset($request->order_by)) {
            $order = $request->order_by;
        }
        $invoices = Invoice::when($search, function ($query) use ($search) {
            $query->whereHas('user', function ($query) use ($search) {
                return $query->where('username', 'LIKE', "%$search%");
            });
        })->when($status, function ($query) use ($status) {
            return $query->where('status', $status);
        });
        if (!Auth::user()->can('all_invoice')) {
            $invoices = $invoices->where('user_id', Auth::id());
        }

        $invoices = $invoices->orderby('id', 'desc')->paginate($order);

        return view('profile.invoice.index', compact('invoices', 'search', 'order', 'status'));
    }

    public function changeStatus($id, Request $request)
    {
        $invoice = Invoice::find($id);
        $invoice->status = $request->status;
        $invoice->save();

        if ($invoice->status == 'posted') {
            foreach ($invoice->invoiceItems as $item) {
                $userProduct = UserProduct::where('user_id', $invoice->user_id)->where('product_id', $item->service_id)->first();
                if (isset($userProduct)) {
                    $userProduct->quantity++;
                    $userProduct->save();
                } else {
                    UserProduct::create(
                        [
                            'user_id' => $invoice->user_id,
                            'product_id' => $item->service_id,
                            'quantity' => $item->quantity,
                            'type' => 'announcement'
                        ]
                    );
                }
            }
        }
        return back();
    }

    public function saveInvoice($id, Request $request)
    {
        $invoice = Invoice::findorfail($id);
        $invoice->status = 'pending';
        $invoice->save();
        return redirect()->route('invoice.index');

    }

    public function createInvoiceStep1(Request $request)
    {
        $search = $request->search;
        $order = 20;
        $currency = 'amd';
        if (isset($request->order_by)) {
            $order = $request->order_by;
        }

        $categories = ServiceCategory::whereHas("services")->orderby('id', 'desc')->paginate($order);

        return view('profile.invoice.create-step-1', compact('categories', 'search', 'order', 'currency'));
    }

    public function createInvoice(Request $request)
    {
        $user = Auth::user();
        $bankDetails = $user->bankDetails;
        if (!isset($bankDetails)) {
            Session::put('fail-invoice', true);
            return back();
        }
        $quantity = explode(",", $request->quant);
        $service_Id = explode(",", $request->serv);
        $data = [];
        $data['booking_number'] = Invoice::orderby('id', 'DESC')->pluck('id')->first() + 1;
        $data['account_product'] = $data['booking_number'] + 1;
        $data['performer'] = config('bank-details.performer');
        $data['performer_address'] = config('bank-details.performer_address');
        $data['performer_bank'] = config('bank-details.performer_bank');
        $data['performer_tax'] = config('bank-details.performer_tax');
        $data['performer_account'] = config('bank-details.performer_account');
        $data['date'] = Carbon::now()->format('Y-m-d');
        $data['user_id'] = Auth::id();
        $data['status'] = 'draft';
        if (isset($bankDetails)) {
            $data['client'] = $bankDetails->customer;
            $data['individual'] = $bankDetails->individual;
            if ($bankDetails->individual == 1) {

                $data['client_address'] = $bankDetails->address;
                $data['passport'] = $bankDetails->passport;
            } else {
                $data['client_address'] = $bankDetails->physical_address;
                $data['client_bank'] = $bankDetails->bank;
                $data['client_tax'] = $bankDetails->tax;
                $data['client_account'] = $bankDetails->account;
                $data['director'] = $bankDetails->signing_person_1;
                $data['position'] = $bankDetails->position_1;
            }
        }

        $invoice = Invoice::create($data);
        $total = 0;
        $currency = $request->cur;
        for ($i = 0; $i < count($quantity); $i++) {
            $service = Service::find($service_Id[$i]);
            $qty = $quantity[$i];
            $sum = $qty * $service->$currency;
            $amount = $service->$currency;
            $total += $sum;
            $invoice->invoiceItems()->create([
                'service_id' => $service->id,
                'name' => $service->data->name,
                'quantity' => $qty,
                'amount' => $amount,
                'sum' => $sum,
                'measure' => 'hat'
            ]);

        }
        $invoice->money = $total;
        $invoice->currency = $currency;
        $invoice->save();

        return redirect()->route('invoice.show', $invoice->id);


    }

    public function create()
    {
        $users = User::whereHas('organization')->whereHas('bankDetails')->get();
        $services = Service::wherehas('data')->get();
        return view('profile.invoice.create', compact('users', 'services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(InvoiceRequest $request)
    {
        $data = $request->all();
        $data['individual'] = 0;
        if (isset($request->individual)) {
            $data['individual'] = 1;
        }
        if (!isset($data['user_id']))
        {
            $data['user_id'] = Auth::id();
        }
        $data['date'] = Carbon::parse($request->date)->format('Y-m-d');
        $data['status'] = 'draft';
        if(!isset($request->currency))
        {
            $data['currency'] ='amd';

        }
        $invoice = Invoice::create($data);
        $currency = $invoice->currency;
        $total = 0;
        if(isset($currency))
        {
            for ($i = 0; $i < $request->counter; $i++) {
                $service = Service::findorfail($request->service_id[$i]);
                $amount = $service->$currency;
                $sum = $request->quantity[$i] * $amount;
                $total += $sum;
                $invoice->invoiceItems()->create([
                    'service_id' => $request->service_id[$i],
                    'name' => $service->data->name,
                    'quantity' => $request->quantity[$i],
                    'measure' => $request->measure[$i],
                    'amount' => $amount,
                    'sum' => $sum
                ]);

            }

        }
        $invoice->money = $total;
        $invoice->save();
        return redirect()->route('invoice.show', $invoice->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = Invoice::find($id);
        return view('profile.invoice.show', compact('invoice'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $invoice = Invoice::findorfail($id);
        $users = User::whereHas('organization')->whereHas('bankDetails')->get();
        $services = Service::wherehas('data')->get();
        return view('profile.invoice.edit', compact('users', 'invoice', 'services'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(InvoiceRequest $request, $id)
    {
        $invoice = Invoice::findorfail($id);
        $data = $request->all();
        $data['date'] = Carbon::parse($request->date)->format('Y-m-d');
        $data['individual'] = 0;
        $data['status'] = 'pending';
        if (isset($request->individual)) {
            $data['individual'] = 1;
        }
        if (!isset($data['user_id']))
        {
            $data['user_id'] = Auth::id();
        }
        if(!isset($request->currency))
        {
            $data['currency'] ='amd';

        }
        $invoice->update($data);
        $currency = $invoice->currency;
        $total = 0;
        $invoice->invoiceItems()->delete();
        if(isset($currency))
        {
            for ($i = 0; $i < $request->counter; $i++) {
                $service = Service::findorfail($request->service_id[$i]);
                $amount = $service->$currency;
                $sum = $request->quantity[$i] * $amount;
                $total += $sum;
                $invoice->invoiceItems()->create([
                    'service_id' => $request->service_id[$i],
                    'name' => $service->data->name,
                    'quantity' => $request->quantity[$i],
                    'measure' => $request->measure[$i],
                    'amount' => $amount,
                    'sum' => $sum
                ]);

            }

        }
        $invoice->money = $total;
        $invoice->save();
        return redirect()->route('invoice.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $invoice = Invoice::findorfail($id);
        $invoice->invoiceItems()->delete();
        $invoice->delete();
        return back();
    }

    public function getBankDetails(Request $request)
    {
        $user = User::findorfail($request->id);
        $bankDetails = $user->bankDetails;
        $bankDetails['company'] = $user->organization->name;
        return response()->json(['result' => $bankDetails]);
    }

    public function getServiceAmount(Request $request)
    {
        $service = Service::findorfail($request->id);
        return response()->json(['result' => $service]);
    }

    public function invoicePdf($id)
    {
        $invoice = Invoice::find($id);
        $pdf = PDF::loadView('invoicePDF', [
            'invoice' => $invoice,
        ]);

        return $pdf->download('invoice-' . $invoice->id . '.pdf');
    }
}
