<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\ServiceRequest;
use App\Service;
use App\ServiceCategory;
use App\ServiceData;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ServiceController extends Controller
{
    public function updateData($requestData, $object, $dataObject)
    {
        $fillableArray = array_flip($dataObject->getFillable());
        $langs = LaravelLocalization::getSupportedLocales();
        foreach ($langs as $localeCode => $properties) {
            $create = false;
            $dataToFind = [];
            $data = [];

            foreach ($fillableArray as $key => $value) {
                if ($key == $dataObject->foreignKey) {
                    $dataToFind[$key] = $object->id;
                } elseif ($key == 'lang') {
                    $dataToFind[$key] = $localeCode;
                } else {
                    if (isset($requestData[$key . '_' . $localeCode])) {
                        $create = true;
                        $requestDataKey = $key . '_' . $localeCode;
                        $data[$key] = $requestData[$requestDataKey];
                    }
                }
            }
            if ($create == true) {
                $dataObject::updateOrCreate($dataToFind, $data);
            }

        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lang = LaravelLocalization::getCurrentLocale();
        $search = $request->search;
        $order = 20;
        if (isset($request->order_by))
        {
            $order = $request->order_by;
        }
        $categories  = ServiceCategory::whereHas("services")->orderby('id', 'desc')->paginate($order);
//        $services = Service::when($search, function ($query) use ($search)
//        {
//            $query->whereHas('data', function ($query) use ($search)
//            {
//                return $query->where('name', 'LIKE', "%$search%");
//            });
//        })->orderby('id', 'desc')->paginate($order);

        return view('profile.service.index', compact( 'search', 'order', 'lang','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $langs = LaravelLocalization::getSupportedLocales();
        $data = [];
        foreach ($langs as $localeCode => $properties)
        {
            $data[$localeCode] = new ServiceData();
        }
        $lang = LaravelLocalization::getCurrentLocale();
        $category = ServiceCategory::whereHas('data')->get();
        return view('profile.service.create', compact('langs', 'data', 'lang', 'category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceRequest $request)
    {
        $data = $request->all();

        $resource = Service::create($data);
        $this->updateData($request->all(), $resource, new ServiceData());

        return  redirect()->route('services.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $langs = LaravelLocalization::getSupportedLocales();
        $data = [];
        foreach ($langs as $localeCode => $properties) {
            $data[$localeCode] = ServiceData::where('service_id', $id)->where('lang', $localeCode)->first();
        }
        $service = Service::findorfail($id);
        $lang = LaravelLocalization::getCurrentLocale();
        $category = ServiceCategory::whereHas('data')->get();
        return view('profile.service.edit', compact('service', 'langs', 'data', 'lang', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceRequest $request, $id)
    {
        $service = Service::findorfail($id);
        $data = $request->all();
        $service->update($data);
        $this->updateData($request->all(), $service, new ServiceData());
        return redirect()->route('services.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Service::destroy($id);
        ServiceData::where('service_id', $id)->delete();
        return redirect()->route('services.index');
    }
}
