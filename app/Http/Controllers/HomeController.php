<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Counter;
use App\Http\Requests\ContactRequest;
use App\Job;
use App\Organization;
use App\Permission;
use App\Review;
use App\Services\SeoService;
use App\Subscribtion;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{

    protected $seoService;
    protected $counter;

    /**
     * CourseController constructor.
     */
    public function __construct(SeoService $seoService, Counter $counter)
    {
        $this->seoService = $seoService;
        $this->counter = $counter;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->counter->count();
        $localeCode = Config::get('app.locale');
        $blogs = Blog::whereHas('data')->get();
        $reviews = Review::whereHas('data')->orderby('id', 'DESC')->get();
        return view('index', compact('blogs', 'reviews', 'localeCode'));
    }

    public function about()
    {
        $this->counter->count();
        $this->seoService->setPageMeta('about-us');
        return view('about');
    }

    public function contact()
    {
        $this->counter->count();
        $this->seoService->setPageMeta('contact-us');
        return view('contact');
    }

    public function companySingle($slug)
    {
        $this->counter->count();
        $company = Organization::where('slug', $slug)->first();
        $user = User::with('posted_job.data')->find($company->user_id);
        $phone = null;
        if (isset($user->phone)) {
            $phone = json_decode($user->phone, true);
        }
        return view('companySingle', compact('company', 'user', 'phone'));
    }

    public function contactUs(ContactRequest $request)
    {
        $data = $request->all();
        $subject = 'CareerCenter - Contact Us';

        Mail::send('email.contact', ['data' => $data], function ($message) use ($data, $subject) {
            $message->to('mailbox@careercenter.am')->subject($subject);
            $message->from($data['email']);
        });

        Session::put('contact-us', true);
        return back();
    }

    public function subscribe(Request $request)
    {
        Subscribtion::create($request->all());
        $subject = 'CareerCenter - Subscribers';
        Mail::send('email.subscribers', ['data' => $request], function ($message) use ($request, $subject) {
            $message->to('careercenter-am-subscribe@yahoogroups.com')->subject($subject);
            $message->from('mailinglist@careercenter.am', 'CareerCenter');
        });
        Session::put('subscribe', true);
        return back();
    }

}
