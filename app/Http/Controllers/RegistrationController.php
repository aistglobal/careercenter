<?php

namespace App\Http\Controllers;

use App\Country;
use App\Http\Requests\FinalRegistrationRequest;
use App\Http\Requests\RegisterRequest;
use App\Organization;
use App\Permission;
use App\PermissionUser;
use App\Role;
use App\Services\Helper;
use App\Services\ImageService;
use App\Services\SeoService;
use App\User;
use App\UserAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegistrationController extends Controller
{
    protected $imageService;
    protected $seoService;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
        $this->seoService = new SeoService();
    }

    public function registerForm()
    {
        if (Auth::check()) {
            return redirect()->route('home');
        }
        $this->seoService->setPageMeta('register');
        return view('auth.register');
    }

    public function register(RegisterRequest $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt($request->get('password'));
       $role = Role::where('name','user')->first();
        if ($request->is_organization == 1) {
            $role = Role::where('name','organization')->first();
        }
        $user = User::create($data);
        $user->attachRole($role);
//        foreach (config('permissions.' . $data['role']) as $key => $value) {
//            $user->givePermissionsTo($key);
//        }
        Auth::login($user);
        if ($user->is_organization == '0') {

            return redirect()->route('user.register_form_step_2_individual');
        } else {
            $organization = Organization::create(['user_id'=>$user->id]);
            $user->organization_id = $organization->id;
            $user->save();
            return redirect()->route('user.register_form_step_2_organization');
        }
    }

    public function registerOrganizationForm()
    {
        $user = Auth::user();
        if (isset($user->email)) {
            return redirect()->route('home');
        }
        if ($user->is_organization == '0') {
            return redirect()->route('user.register_form_step_2_individual');
        }
        $countries = Country::all();
        $this->seoService->setPageMeta('register_step_2');
        return view('organization.register', compact('user', 'countries'));
    }

    public function registerIndividualForm()
    {
        $user = Auth::user();
        if (isset($user->email)) {
            return redirect()->route('home');
        }
        if ($user->is_organization == '1') {
            return redirect()->route('user.register_form_step_2_organization');
        }
        $countries = Country::all();
        $this->seoService->setPageMeta('register_step_2');
        return view('individual.register', compact('user', 'countries'));
    }

    public function register_final(FinalRegistrationRequest $request, $id)
    {
        $user = User::find($id);
        $count = count($request->address);

            for ($i = 0; $i < $count; $i++) {
                if($request->address[$i]!=null || $request->zip[$i]!=null || $request->country[$i]!=null || $request->city[$i]!=null )
                {
                    $user->address()->create([
                        'address' => $request->address[$i],
                        'zip' => $request->zip[$i],
                        'country_id' => $request->country[$i],
                        'city' => $request->city[$i],
                        'address_type' => $request->address_type[$i]
                    ]);
                }
            }

        $data = $request->all();
        $data['phone'] = null;
        if (count($request->phone) != 0)
        {
            $comb = array_combine($request->phone, $request->phone_type);
            $b=[];
            foreach ($comb as $key => $value )
            {
                $b[]=array('phone'=>$key, 'type'=>$value);
            }

            $phone = json_encode($b);
            $data['phone'] = $phone;
        }

        $user->update($data);
        if ($request->hasFile('image')) {
            $this->imageService->saveProfilePhoto($request['image'], 'public', $user, $key = 'image');
        }
//        $data['user_id'] = $user->id;
        if ($user->is_organization == 1) {
            $organization = $user->organization;
            if(isset($request->about_company))
            {
                $data['about_company'] = str_replace("\r\n", "<br />", $request->about_company);
            }
            if(isset($request->legal_structure) && $request->legal_structure == 'other')
            {
                if(isset($request->legal_other))
                {
                    $data['legal_structure'] =  $data['legal_other'];
                }
            }
            $organization->update($data);
            $slug = Helper::getSlug($data['name'], 'App\Organization',$organization);
            $organization->slug = $slug;
            $organization->save();
        }
        return redirect()->route('profile');
    }
}
