<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TranslationController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchKey(Request $request)
    {
        $keys = DB::table('ltm_translations')
            ->whereRaw(   'LOWER(`value`) like ?', ['%' . mb_strtolower($request->get('search')) . '%'])
            ->orWhere('key','LIKE','%' . $request->get('search') . '%')
            ->take('100')
            ->get();

        return view('translations._search', compact('keys'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addDescription(Request $request)
    {
        if($request->get('id') != 0){
            DB::table('ltm_translations')->where('id', $request->get('id'))->update(['description' => $request->get('value')]);
            $id = $request->get('id');
        }else{
            $id =  DB::table('ltm_translations')->insertGetId([
               'status' => 0,
               'group' => $request->get('group'),
               'locale' => $request->get('lang'),
               'description' => $request->get('value'),
               'value' =>  '',
               'key' =>  $request->get('key')
           ]);
        }
        return response()->json(['status' => 'success', 'id' => $id], 200);
    }
}
