<?php

namespace App\Http\Controllers;

use App\Counter;
use App\Country;
use App\Cv;
use App\CvAttachment;
use App\CvDependant;
use App\CvExperience;
use App\CvEducation;
use App\CvLanguage;
use App\CvReference;
use App\CvTraining;
use App\CvVisa;
use App\Http\Requests\AvailabilityRequest;
use App\Http\Requests\CvImageRequest;
use App\Http\Requests\CvRequest;
use App\Http\Requests\EducationRequest;
use App\Http\Requests\ExperienceRequest;
use App\Http\Requests\LanguageRequest;
use App\Http\Requests\PersonalRequest;
use App\Http\Requests\ReferenceRequest;
use App\Http\Requests\TrainingRequest;
use App\Http\Requests\UserDetailsRequest;
use App\Http\Requests\VisaRequest;
use App\Languages;
use Carbon\CarbonInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Support\Facades\Storage;
use App\Services\SeoService;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade as PDF;


class CvController extends Controller
{
    protected $seoService;
    protected $counter;

    /**
     * CourseController constructor.
     */
    public function __construct(SeoService $seoService, Counter $counter)
    {
        $this->seoService = $seoService;
        $this->counter = $counter;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->counter->count();
        $user = Auth::user();
        if (!isset($user->email)) {
            if ($user->is_organization == '0') {
                return redirect()->route('user.register_form_step_2_individual');
            } else {
                return redirect()->route('user.register_form_step_2_organization');
            }
        }
        $order_name = $request->get('order_name', 'id');
        $order_by = $request->get('order_by', 'desc');

        $lang = LaravelLocalization::getCurrentLocale();
        $search = $request->search;
        $order = $request->get('order', 20);
        $resumes = Cv::when($search, function ($query) use ($search) {
            $query->whereHas('user', function ($query) use ($search) {
                return $query->where('username', 'LIKE', "%$search%")->orWhere('first_name', 'LIKE', "%$search%")->orWhere('last_name', 'LIKE', "%$search%");
            });
        })->where('lang', $lang);
        if (!Auth::user()->can('all_resume')) {
            $resumes = $resumes->where('user_id', Auth::id());
        }
        $resumes = $resumes->orderBy($order_name, $order_by)->paginate($order);
        $this->seoService->setPageMeta('resume');
        return view('profile.resume.index', compact('resumes', 'search', 'order', 'order_name', 'order_by'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id = null)
    {
        $this->seoService->setPageMeta('resume');
        $lang = LaravelLocalization::getCurrentLocale();
        $user = Auth::user();


        $citizenship = [];
        if (isset($id)) {
            $resume = Cv::findorfail($id);

        } else {

            $resume = $user->cv()->create(['lang' => $lang, 'status' => '0']);
            $resume->contacts()->create([
                'first_name' => $user->first_name,
                'middle_name' => $user->middle_name,
                'last_name' => $user->last_name,
                'title' => $user->title,
                'website' => $user->website,
                'email' => $user->email,
                'phone' => $user->phone,
                'residence_country_id' => $user->residence_country_id,
            ]);
            if (isset($user->address)) {
                foreach ($user->address as $item) {
                    $resume->address()->create([
                        'country_id' => $item->country_id,
                        'city' => $item->city,
                        'zip' => $item->zip,
                        'address' => $item->address,
                        'address_type' => $item->address_type
                    ]);
                }
            }
            if (isset($user->citizenship)) {
                $citizenship[] = json_encode($citizenship);
            } else {
                $citizenship = "[]";
            }


            $resume->personal()->create([
                'birthday' => $user->birthday,
                'citizenship_id' => $citizenship,
                'image' => $user->image
            ]);

            return redirect()->route('resume.create', $resume->id);
        }
        $phone = null;
        if(isset($resume->contacts->phone) && $resume->contacts->phone)
        {
            $phone = json_decode($resume->contacts->phone, true);
        }
       if(isset($resume->personal->citizenship_id) && $resume->personal->citizenship_id )
       {
           $citizenship = json_decode($resume->personal->citizenship_id);
       }

        $v_countries = [];
        $work_type = [];
        if (isset($resume->volunteering)) {
            $v_countries = json_decode($resume->volunteering->countries);
            $work_type = json_decode($resume->volunteering->work_type);
        }
        $experience = null;
        if (count($resume->experience) != 0) {
            $experience = $resume->experience[0];
        }

        $education = null;
        if (count($resume->education) != 0) {
            $education = $resume->education[0];
        }
        $training = null;
        if (count($resume->training) != 0) {
            $training = $resume->training[0];
        }
        $language_skill = null;
        if (count($resume->language) != 0) {
            $language_skill = $resume->language[0];
        }
        $visa = null;
        if (count($resume->visa) != 0) {
            $visa = $resume->visa[0];
        }
        $reference = null;
        if (count($resume->reference) != 0) {
            $reference = $resume->reference[0];
        }

        $countries = Country::all();
        $languages = Languages::all();
        $term = [];
        $days = [];
        $org_type = [];
        $position_type = [];
        $interest_area = [];
        if (isset($resume->availability)) {

            if (isset($resume->availability->term) && $resume->availability->term) {
                $term = json_decode($resume->availability->term);
            }
            if (isset($resume->availability->days) && $resume->availability->days) {
                $days = json_decode($resume->availability->days);
            }
            if (isset($resume->availability->org_type) && $resume->availability->org_type) {
                $org_type = json_decode($resume->availability->org_type);
                if(!is_array($org_type))
                {
                    $org_type = [];
                }
            }
            if (isset($resume->availability->position_type) && $resume->availability->position_type) {
                $position_type = json_decode($resume->availability->position_type);
                if(!is_array($position_type))
                {
                    $position_type = [];
                }
            }
            if (isset($resume->availability->interest_area) && $resume->availability->interest_area) {
                $interest_area = json_decode($resume->availability->interest_area);
                if(!is_array($interest_area))
                {
                    $interest_area = [];
                }
            }
        }
        return view('profile.resume.create', compact('user', 'countries', 'phone', 'resume',
            'citizenship', 'v_countries', 'work_type', 'experience', 'education', 'training', 'language_skill', 'visa',
            'reference', 'languages', 'term', 'days',
            'org_type', 'position_type', 'interest_area'));
    }

    public function contacts(CvRequest $request)
    {
        $cv = Cv::find($request->cv_id);
        $cv->status = '1';
        $cv->updated_at = now();
        $cv->save();
        $contacts = $cv->contacts;
        $cv->address()->delete();
        $count = count($request->address);
        for ($i = 0; $i < $count; $i++) {
            $cv->address()->create([
                'address' => $request->address[$i],
                'zip' => $request->zip[$i],
                'country_id' => $request->country[$i],
                'city' => $request->city[$i],
                'address_type' => $request->address_type[$i]
            ]);
        }
        $data = $request->all();
        $data['phone'] = null;
        if (count($request->phone) != 0) {
            $comb = array_combine($request->phone, $request->phone_type);
            $b = [];
            foreach ($comb as $key => $value) {
                $b[] = array('phone' => $key, 'type' => $value);
            }

            $phone = json_encode($b);
            $data['phone'] = $phone;
        }
        $contacts->update($data);
        return 'success';

    }

    public function personal(PersonalRequest $request)
    {

        $cv = Cv::find($request->cv_id);
        $cv->status = '1';
        $cv->updated_at = now();
        $cv->save();
        $data = $request->except(['image']);
        $personal = $cv->personal;
        $data['citizenship_id'] = null;
        $citizenship = json_encode($request->citizenship_id);
        $data['citizenship_id'] = $citizenship;
        $personal->update($data);
        $cv->dependants()->delete();
        if ($request->key_count != 0 && $request->dependants != 'no') {
            for ($i = 0; $i < $request->key_count; $i++) {
                $cv->dependants()->create([
                    'name' => $request->name[$i],
                    'birthday' => $request->birth_day[$i],
                    'relationship' => $request->relationship[$i],

                ]);
            }
        }
        return 'success';
    }

    public function image(Request $request)
    {

        $cv = Cv::find($request->cv_id);
        $personal = $cv->personal;
        if (isset($request['image']) && $request['image'] != 'null') {
            $cv->updated_at = now();
            $cv->save();
            if ($request['image'] == 'no_image') {
                $personal->image = null;
                $personal->save();
                return 'success';
            } else {
                $imageFileName = time() . rand(1, 999999999) . '.jpg';
                $dataImage = $request['image'];
                list($type, $dataImage) = explode(';', $dataImage);
                list(, $dataImage) = explode(',', $dataImage);
                $dataImage = base64_decode($dataImage);
                $s3 = Storage::disk('public');
                $filePath = '/images/resumes/' . $imageFileName;
                $s3->put($filePath, $dataImage, 'public');
                $personal->image = 'images/resumes/' . $imageFileName;
                $personal->save();
                return 'Success';
            }

        }
        if (isset($request->delete)) {
            $personal->image = null;
            $personal->save();
            return 'success';
        }


    }

    public function objective(Request $request)
    {
        $cv = Cv::find($request->cv_id);
        $cv->update($request->all());
        $cv->status = '1';
        $cv->updated_at = now();
        $cv->save();
        return 'success';

    }

    public function computer_skills(Request $request)
    {
        $cv = Cv::find($request->cv_id);
        $cv->status = '1';
        $cv->updated_at = now();
        $cv->save();
        $computer = $cv->computer_skills;
        if (isset($computer)) {
            $computer->update($request->all());
        } else {
            $cv->computer_skills()->create($request->all());
        }

        return 'success';
    }

    public function skills(Request $request)
    {
        $cv = Cv::find($request->cv_id);
        $cv->status = '1';
        $cv->updated_at = now();
        $cv->save();
        $cv->update($request->all());
        return 'success';
    }

    public function volunteering(Request $request)
    {
        $cv = Cv::find($request->cv_id);
        $cv->status = '1';
        $cv->updated_at = now();
        $cv->save();
        $data = $request->all();
        $volunteering = $cv->volunteering;
        $data['countries'] = null;
        $countries = json_encode($request->countries);
        $data['countries'] = $countries;
        $data['work_type'] = null;
        $work_type = json_encode($request->work_type);
        $data['work_type'] = $work_type;
        if (isset($volunteering)) {
            $volunteering->update($data);
        } else {
            $cv->volunteering()->create($data);
        }
        return 'success';

    }

    public function experience(ExperienceRequest $request)
    {
        $cv = Cv::find($request->cv_id);
        $cv->status = '1';
        $cv->updated_at = now();
        $cv->save();
        $data = $request->all();
        $data['period_from'] = $request->exp_year_from . '-' . $request->exp_month_from;
        $data['period_to'] = $request->exp_year_to . '-' . $request->exp_month_to;
        if (isset($request->exp)) {
            $a = CvExperience::find($request->exp);
            $a->update($data);
        } else {
            $a = $cv->experience()->create($data);
        }
        return $a;
    }

    public function getExperience(Request $request)
    {
        $experience = CvExperience::find($request->id);
        return $experience;
    }

    public function deleteExperience(Request $request)
    {
        CvExperience::destroy($request->id);
        return 'success';
    }

    public function education(EducationRequest $request)
    {
        $cv = Cv::find($request->cv_id);
        $cv->status = '1';
        $cv->updated_at = now();
        $cv->save();
        $data = $request->all();
        $data['period_from'] = $request->edu_year_from . '-' . $request->edu_month_from;
        $data['period_to'] = $request->edu_year_to . '-' . $request->edu_month_to;
        if (isset($request->edu)) {
            $a = CvEducation::find($request->edu);
            $a->update($data);
        } else {
            $a = $cv->education()->create($data);
        }

        return $a;
    }

    public function getEducation(Request $request)
    {
        $education = CvEducation::find($request->id);
        return $education;
    }

    public function deleteEducation(Request $request)
    {
        CvEducation::destroy($request->id);
        return 'success';
    }

    public function training(TrainingRequest $request)
    {
        $cv = Cv::find($request->cv_id);
        $cv->status = '1';
        $cv->updated_at = now();
        $cv->save();
        $data = $request->all();
        $data['period_from'] = $request->training_year_from . '-' . $request->training_month_from;
        $data['period_to'] = $request->training_year_to . '-' . $request->training_month_to;
        if (isset($request->training)) {
            $a = CvTraining::find($request->training);
            $a->update($data);
        } else {
            $a = $cv->training()->create($data);
        }

        return $a;
    }

    public function getTraining(Request $request)
    {
        $training = CvTraining::find($request->id);
        return $training;
    }

    public function deleteTraining(Request $request)
    {
        CvTraining::destroy($request->id);
        return 'success';
    }

    public function language(LanguageRequest $request)
    {
        $cv = Cv::find($request->cv_id);
        $cv->status = '1';
        $cv->updated_at = now();
        $cv->save();
        $data = $request->all();
        if (isset($request->language)) {
            $a = CvLanguage::find($request->language);
            $a->update($data);
        } else {
            $a = $cv->language()->create($data);
        }

        $code = Languages::where('id', $a->language_id)->first();
        $a['code'] = trans('languages.' . $code->name);
        return $a;
    }

    public function getLanguage(Request $request)
    {
        $language = CvLanguage::find($request->id);
        $code = Languages::where('id', $language->language_id)->first();
        $language['code'] = trans('languages.' . $code->name);
        return $language;
    }

    public function deleteLanguage(Request $request)
    {
        CvLanguage::destroy($request->id);
        return 'success';
    }

    public function visa(VisaRequest $request)
    {
        $cv = Cv::find($request->cv_id);
        $cv->status = '1';
        $cv->updated_at = now();
        $cv->save();
        $data = $request->all();
        if (isset($request->visa)) {
            $a = CvVisa::find($request->visa);
            $a->update($data);
        } else {
            $a = $cv->visa()->create($data);
        }
        $country = Country::where('id', $a->country_id)->first();
        $a['country_name'] = trans(('countries.' . $country->code));

        return $a;
    }

    public function getVisa(Request $request)
    {
        $visa = CvVisa::find($request->id);

        $country = Country::where('id', $visa->country_id)->first();
        $visa['country_name'] = trans(('countries.' . $country->code));

        return $visa;
    }

    public function deleteVisa(Request $request)
    {
        CvVisa::destroy($request->id);
        return 'success';
    }

    public function availability(AvailabilityRequest $request)
    {
        $cv = Cv::find($request->cv_id);
        $cv->status = '1';
        $cv->updated_at = now();
        $cv->save();
        $availability = $cv->availability;
        $data = $request->all();
        if (isset($request->term)) {
            $data['term'] = json_encode($request->term);
        }
        if (isset($request->days)) {
            $data['days'] = json_encode($request->days);
        }
        if (isset($request->interest_area)) {
            $data['interest_area'] = json_encode($request->interest_area);
        }
        if (isset($request->position_type)) {
            $data['position_type'] = json_encode($request->position_type);

        }
        if (isset($request->org_type)) {
            $data['org_type'] = json_encode($request->org_type);
        }

        if (isset($availability)) {
            $availability->update($data);
        } else {
            $cv->availability()->create($data);
        }
        if (isset($request->finish)) {

            if (isset($cv->contacts) && isset($cv->availability) && count($cv->education) != 0 && count($cv->language) != 0) {
                return 'finish';
            } else {
                return 'fail';
            }

        }
        return 'success';
    }

    public function reference(ReferenceRequest $request)
    {
        $cv = Cv::find($request->cv_id);
        $cv->status = '1';
        $cv->updated_at = now();
        $cv->save();
        $data = $request->all();
        if (isset($request->ref)) {
            $a = CvReference::find($request->ref);
            $a->update($data);
        } else {
            $a = $cv->reference()->create($data);
        }

        return $a;
    }

    public function getReference(Request $request)
    {
        $reference = CvReference::find($request->id);
        return $reference;
    }

    public function deleteReference(Request $request)
    {
        CvReference::destroy($request->id);
        return 'success';
    }

    public function questions(Request $request)
    {
        $cv = Cv::find($request->cv_id);
        $cv->status = '1';
        $cv->updated_at = now();
        $cv->save();
        $questions = $cv->questions;
        if (isset($cv->questions)) {
            $questions->update($request->all());
        } else {
            $cv->questions()->create($request->all());
        }
    }

    public function attachments(Request $request)
    {
        $cv = Cv::find($request->cv_id);
        $cv->status = '1';
        $cv->updated_at = now();
        $cv->save();
        $cv->attachment()->delete();
        if (isset($request->filepond)) {

            foreach ($request->filepond as $item) {
                if (isset($item)) {
                    $arr = json_decode($item);
                    $type = substr($arr->type, 0, 5);
                    $attachments = new CvAttachment();
                    $cv->attachment()->save($attachments);
                    $dataImage = base64_decode($arr->data);
                    $imageName = $cv->id . '/' . $arr->name;
                    Storage::disk('public')->put($imageName, $dataImage, 'public');
                    $attachments->attachment = $imageName;
                    $attachments->description = $arr->name;
                    if ($type == 'image') {
                        $attachments->is_image = 1;
                    }
                    $attachments->save();
                }

            }
        }
        if (isset($cv->contacts) && isset($cv->availability) && count($cv->education) != 0 && count($cv->language) != 0) {
            return 'finish';
        } else {
            return 'fail';
        }
    }

    public function getAttachments(Request $request)
    {
        $cv = Cv::find($request->id);
        $attachments = $cv->attachment;
        return $attachments;
    }

    public function showCv($id)
    {

        $cv = Cv::findorfail($id);


        $phone = null;
        $personal_citizenship = null;
        $countries = Country::all();
        $work_type = null;
        $volunteering_countries = null;
        $availability_days = null;
        $availability_terms = null;


        if (isset($cv->contacts)) {
            $phone = json_decode($cv->contacts->phone, true);
        }
        if (isset($cv->personal->citizenship_id)) {
            $personal_citizenship = json_decode($cv->personal->citizenship_id);

        }
        if (isset($cv->volunteering)) {
            $work_type = json_decode($cv->volunteering->work_type);
            $volunteering_countries = json_decode($cv->volunteering->countries);
        }
        if (isset($cv->availability)) {
            $availability_days = json_decode($cv->availability->days);
            $availability_terms = json_decode($cv->availability->term);
        }
        return view('cv', compact('cv', 'phone', 'personal_citizenship', 'countries', 'work_type', 'volunteering_countries', 'availability_days', 'availability_terms'));

    }

    public function resumePdf($id)
    {
        $cv = Cv::findorfail($id);
        $phone = null;
        $personal_citizenship = null;
        $countries = Country::all();
        $work_type = null;
        $volunteering_countries = null;
        $availability_days = null;
        $availability_terms = null;

        if (isset($cv->contacts)) {
            $phone = json_decode($cv->contacts->phone, true);
        }
        if (isset($cv->personal->citizenship_id)) {
            $personal_citizenship = json_decode($cv->personal->citizenship_id);
        }
        if (isset($cv->volunteering)) {
            $work_type = json_decode($cv->volunteering->work_type);
            $volunteering_countries = json_decode($cv->volunteering->countries);
        }
        if (isset($cv->availability)) {
            $availability_days = json_decode($cv->availability->days);
            $availability_terms = json_decode($cv->availability->term);
        }
        $pdf = PDF::loadView('resumePdf', [
            'cv' => $cv,
            'phone' => $phone,
            'personal_citizenship' => $personal_citizenship,
            'countries' => $countries,
            'work_type' => $work_type,
            'volunteering_countries' => $volunteering_countries,
            'availability_days' => $availability_days,
            'availability_terms' => $availability_terms
        ]);

        return $pdf->download('resume-' . $id . '.pdf');

    }

    public function previewCv(Request $request)
    {
        $cv = Cv::find($request->id);
        if (isset($cv->contacts) && isset($cv->availability) && count($cv->education) != 0 && count($cv->language) != 0) {
            return 'success';
        } else {
            return 'fail';
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cv $cv
     * @return \Illuminate\Http\Response
     */
    public function show(Cv $cv)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cv $cv
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Cv $cv
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cv $cv)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cv $cv
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cv::destroy($id);
        return back();

    }
}
