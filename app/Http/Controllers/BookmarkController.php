<?php

namespace App\Http\Controllers;

use App\Services\JobService;
use Illuminate\Http\Request;

class BookmarkController extends Controller
{
  /**
   * @var JobService
   */
  protected $jobService;

  /**
   * BookmarkController constructor.
   * @param JobService $jobService
   */
  public function __construct(JobService $jobService)
  {
    $this->jobService = $jobService;
  }

  /**
   * @param Request $request
   * @return string
   */
  public function create(Request $request)
  {
    $job_id = $request->job_id;
    try {
      $bookmark = $this->jobService->addBookmark($job_id);
      if ($bookmark->count() > 0) {
        return 'success';
      }
    } catch (\Exception $exception) {
      dd($exception->getMessage());
    }
  }

  /**
   * @param Request $request
   * @return string
   */
  public function destroy(Request $request)
  {
    $job_id = $request->job_id;
    try {
      if ($this->jobService->removeBookmark($job_id)) {
        return 'success';
      }
    } catch (\Exception $exception) {
      dd($exception->getMessage());
    }
  }
}
