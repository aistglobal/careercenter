<?php

namespace App\Http\Controllers;

use App\Http\Requests\PageRequest;
use App\Page;
use App\PageData;
use App\Services\ImageService;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class PageController extends Controller
{
    protected $imageService;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(ImageService $imageService)
    {

        $this->imageService = $imageService;
    }
    public  function updateData($requestData, $object, $dataObject){
        $fillableArray = array_flip($dataObject->getFillable());
        $langs = LaravelLocalization::getSupportedLocales();
        foreach($langs as $localeCode => $properties){
            $dataToFind = [];
            $data = [];

            foreach($fillableArray as $key => $value){
                if($key == $dataObject->foreignKey) {
                    $dataToFind[$key] = $object->id;
                }elseif($key == 'lang') {
                    $dataToFind[$key] = $localeCode;
                }else {
                    if (isset($requestData[$key . '_' . $localeCode])) {
                        $requestDataKey = $key . '_' . $localeCode;
                        $data[$key] = $requestData[$requestDataKey];
                    }
                }
            }
            $dataObject::updateOrCreate($dataToFind, $data);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $order = 20;
        if (isset($request->order_by)) {
            $order = $request->order_by;
        }

        $pages = Page::whereHas('data')->when($search, function ($query) use ($search) {

            $query->whereHas('data', function ($query) use ($search) {
                return $query->where('title', 'LIKE', "%$search%");
            });

        })->paginate($order);

        return view('profile.page.index', compact('pages', 'search', 'order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $langs = LaravelLocalization::getSupportedLocales();
        $data = [];
        foreach ($langs as $localeCode => $properties) {
            $data[$localeCode] = new PageData();
        }
        return view('profile.page.create', compact('langs', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {
        $page = Page::create($request->all());
        $slug = $page->slugify($request->title_en);
        $page->slug = $slug;
        $page->save();
        $this->imageService->savePhoto($request->image, 'public', $page, $key = 'image');
        $this->updateData($request->all(), $page, new PageData());
        return redirect()->route('pages.index');


    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $langs = LaravelLocalization::getSupportedLocales();
        $data = [];
        foreach ($langs as $localeCode => $properties) {
            $data[$localeCode] = PageData::where('page_id', $id)->where('lang', $localeCode)->first();
        }
        $page = Page::findorfail($id);

        return view('profile.page.edit', compact('page',  'langs', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function update(PageRequest $request, $id)
    {
        $page = Page::findorfail($id);
        $slug = $page->slugify($request->title_en);
        $page->slug = $slug;
        $page->save();
        if ($request->hasFile('image')) {
            $this->imageService->savePhoto($request->image, 'public', $page, $key = 'image');
        }

        $this->updateData($request->all(), $page, new PageData());
        return redirect()->route('pages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Page::destroy($id);
        PageData::where('page_id', $id)->delete();
        return redirect()->route('pages.index');

    }
}
