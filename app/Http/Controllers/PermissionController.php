<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{
    public function index()
    {
        $permissionData = Permission::all();
        $roles = Role::with('permissions')->get();
        $permission_data = [];
        foreach($permissionData as $item)
        {
            $name_array = explode('_',$item['name']);
            $key = $name_array[1] ?? $name_array[0];
            $permission_data[$key][] = [$item['id'],$item['name'],$item['display_name'], $item['description']];
        }

        return view('profile.permissions.index', compact('permission_data', 'roles'));
    }

    public function permissionsUpdate(Request $request){

        $data = $request->all();
        if($data['check'] == 'false'){
            DB::table('permission_role')->insert(['permission_id' => $data['permission_id'], 'role_id' => $data['role_id']]);
        }else{
            DB::table('permission_role')->where('permission_id', $data['permission_id'])
                ->where('role_id', $data['role_id'])->delete();
        }
        $response = [];
        $response['permission_id'] = $data['permission_id'];
        $response['role_id'] = $data['role_id'];
        $response['check'] = $data['check'];
        return $response;
    }


}
