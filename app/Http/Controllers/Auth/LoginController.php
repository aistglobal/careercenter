<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\SeoService;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/jobs';

    /**
     * Login username to be used by the controller.
     *
     * @var string
     */
    protected $username;
    protected $seoService;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->seoService = new SeoService();

        $this->username = $this->findUsername();
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->changPassToBcrypt($request);

//        $validator = Validator::make($request->all(), [
//            'username' => 'required',
//            'password' => 'required',
//        ]);
//
//        if ($validator->fails()) {
//
//            return response()->json(array(
//                'success' => false,
//                'error' => $validator->messages()
//            ), 400);
//        }


        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)
        ) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function findUsername()
    {
        $login = request()->input('login');

        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        request()->merge([$fieldType => $login]);

        return $fieldType;
    }

    /**
     * Get username property.
     *
     * @return string
     */
    public function username()
    {
        return $this->username;
    }

    public function redirectTo()
    {
        if (Auth::user()->status == 0)
        {
            Auth::logout();
            return app()->getLocale() . '/login';
        }
        else
        {
            return app()->getLocale() . '/jobs';
        }

    }

    public function logout(Request $request)
    {
        $this->performLogout($request);
        Session::flush();
        return redirect(app()->getLocale() . '/');
    }

    public function changPassToBcrypt(Request $request)
    {
        $identity = $request->get('username');
        $pass = $request->get('password');
        $md5_pass = md5($request->get('password'));

        $user = app(User::class)->where('username', $identity)->first();
        if (isset($user)) {
            if ($user->password == $md5_pass) {
                $bCrypt_pass = bcrypt($pass);
                $user->update(['password' => $bCrypt_pass]);
            }
        }
    }

    public function showLoginForm()
    {
        $this->seoService->setPageMeta('login');
        return view('auth.login');
    }

}