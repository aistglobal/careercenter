<?php

namespace App\Http\Controllers;

use App\Country;
use App\Cv;
use App\Languages;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SearchResumeController extends Controller
{
    public function index(Request $request)
    {

        $updated_at = Cv::where('status',1)->groupBy('updated_at')->pluck('updated_at')->toArray();
        $country = Country::all();
        $languages = Languages::all();

        $order_name = $request->get('order_name', 'id');
        $order_by = $request->get('order_by', 'desc');
        $order = $request->get('order', 20);

       //personal info
        $s_updated = $request->updated_at;

        $s_sex = $request->sex;

        $s_citizenship = $request->citizenship_id;

        //contact info
        $s_location = $request->location;
        $s_country = $request->residence_country_id;

        //work experience
        $s_work_field = $request->position_field;

        //education
        $s_edu_area = $request->study_area;
        $s_degree = $request->degree;
        $s_major = $request->major_field;

        //availability
         $s_salary = $request->salary;
         $s_currency = $request->currrency;
         $s_period = $request->period;
         $s_term = $request->term;
         $s_interest = $request->interest_area;
         $s_positions = $request->positions;

         //volunteering
        $s_work_type = $request->work_type;
        $s_volunteering = $request->willing;

        //language_skills
        $s_lang = $request->language_id;

        //computer skills
        $s_software = $request->software;
        $s_hardware = $request->hardware;



        $resumes = Cv::when($s_updated, function ($query) use ($s_updated) {
            if (count($s_updated) != 0) {
                $query->where(function ($q) use ($s_updated) {
                    foreach ($s_updated as $item) {
                        $q->orWhereDate('updated_at', Carbon::parse($item)->format('Y-m-d'));
                    }
                });
                return $query;
            }
        })
            ->when($s_sex, function ($query) use ($s_sex) {
            $query->whereHas('personal', function ($query) use ($s_sex) {
                return $query->where('sex', $s_sex);
            });
        })
            ->when($s_country, function ($query) use ($s_country) {
            $query->whereHas('contacts', function ($query) use ($s_country) {
                return $query->where('residence_country_id', $s_country);
            });
        })
            ->when($s_work_field, function ($query) use ($s_work_field) {
                $query->whereHas('experience', function ($query) use ($s_work_field) {
                    return $query->where('position_field', $s_work_field);
                });
            })
            ->when($s_edu_area, function ($query) use ($s_edu_area) {
                $query->whereHas('education', function ($query) use ($s_edu_area) {
                    return $query->where('study_area', $s_edu_area);
                });
            })
            ->when($s_degree, function ($query) use ($s_degree) {
                $query->whereHas('education', function ($query) use ($s_degree) {
                    return $query->where('degree', $s_degree);
                });
            })
            ->when($s_major, function ($query) use ($s_major) {
                $query->whereHas('education', function ($query) use ($s_major) {
                    return $query->where('major_field', 'LIKE', "%$s_major%");
                });
            })
            ->when($s_term, function ($query) use ($s_major) {
                $query->whereHas('availability', function ($query) use ($s_major) {
                    return $query->where('term', $s_major);
                });
            })
            ->when($s_work_type, function ($query) use ($s_work_type) {
                if (count($s_work_type) != 0) {
                    $query->whereHas('volunteering',function ($q) use ($s_work_type) {
                        foreach ($s_work_type as $item) {
                            $q->orWhere('work_type', $item);
                        }
                    });
                    return $query;
                }
            })
            ->when($s_volunteering, function ($query) use ($s_volunteering) {
                $query->whereHas('volunteering', function ($query) use ($s_volunteering) {
                    return $query->where('willing', $s_volunteering);
                });
            })
            ->when($s_interest, function ($query) use ($s_interest) {
                if (count($s_interest) != 0) {
                    $query->whereHas('availability',function ($q) use ($s_interest) {
                        foreach ($s_interest as $item) {
                            $q->orWhere('interest_area','LIKE', "%$item%");
                        }
                    });
                    return $query;
                }
            })
            ->when($s_positions, function ($query) use ($s_positions) {
                if (count($s_positions) != 0) {
                    $query->whereHas('availability',function ($q) use ($s_positions) {
                        foreach ($s_positions as $item) {
                            $q->orWhere('position_type','LIKE', "%$item%");
                        }
                    });
                    return $query;
                }
            })
            ->when($s_citizenship, function ($query) use ($s_citizenship) {
                if (count($s_citizenship) != 0) {
                    $query->whereHas('personal',function ($q) use ($s_citizenship) {
                        foreach ($s_citizenship as $item) {
                            $q->orWhere('citizenship_id', $item);
                        }
                    });
                    return $query;
                }
            })
            ->when($s_lang, function ($query) use ($s_lang) {
                if (count($s_lang) != 0) {
                    $query->whereHas('language',function ($q) use ($s_lang) {
                        foreach ($s_lang as $item) {
                            $q->orWhere('language_id', $item);
                        }
                    });
                    return $query;
                }
            })
            ->when($s_software, function ($query) use ($s_software) {
                $query->whereHas('computer_skills', function ($query) use ($s_software) {
                    return $query->where('software','LIKE', "%$s_software%");
                });
            })
            ->when($s_hardware, function ($query) use ($s_hardware) {
                $query->whereHas('computer_skills', function ($query) use ($s_hardware) {
                    return $query->where('hardware','LIKE', "%$s_hardware%");
                });
            })
            ->where('status', 1)->orderBy($order_name, $order_by)->paginate($order);


        return view('profile.search-resume.search-form', compact('country', 'languages', 'updated_at', 'request', 'resumes', 'order', 'order_name', 'order_by'));
    }
}
