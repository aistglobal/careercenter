<?php

namespace App\Http\Controllers;

use App\Http\Requests\ServiceCategoryRequest;
use App\ResourceCategory;
use App\ServiceCategory;
use App\ServiceCategoryData;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ServiceCategoryController extends Controller
{
    public function updateData($requestData, $object, $dataObject)
    {
        $fillableArray = array_flip($dataObject->getFillable());
        $langs = LaravelLocalization::getSupportedLocales();
        foreach ($langs as $localeCode => $properties) {
            $dataToFind = [];
            $data = [];

            foreach ($fillableArray as $key => $value) {
                if ($key == $dataObject->foreignKey) {
                    $dataToFind[$key] = $object->id;
                } elseif ($key == 'lang') {
                    $dataToFind[$key] = $localeCode;
                } else {
                    if (isset($requestData[$key . '_' . $localeCode])) {
                        $requestDataKey = $key . '_' . $localeCode;
                        $data[$key] = $requestData[$requestDataKey];
                    }
                }
            }
            $dataObject::updateOrCreate($dataToFind, $data);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $order = 20;
        if (isset($request->order_by)) {
            $order = $request->order_by;
        }
        $categories = ServiceCategory::when($search, function ($query) use ($search) {

            $query->whereHas('data', function ($query) use ($search) {
                return $query->where('name', 'LIKE', "%$search%");
            });

        })->paginate(20);
        return view('profile.service-category.index', compact('categories', 'search', 'order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $langs = LaravelLocalization::getSupportedLocales();
        $data = [];
        foreach ($langs as $localeCode => $properties) {
            $data[$localeCode] = new ServiceCategoryData();
        }
        return view('profile.service-category.create', compact('langs', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceCategoryRequest $request)
    {
        $category = ServiceCategory::create([]);
        $this->updateData($request->all(), $category, new ServiceCategoryData());
        return redirect()->route('service_category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $langs = LaravelLocalization::getSupportedLocales();
        $data = [];
        foreach ($langs as $localeCode => $properties) {
            $data[$localeCode] = ServiceCategoryData::where('service_category_id', $id)->where('lang', $localeCode)->first();
        }
        $category = ServiceCategory::findorfail($id);

        return view('profile.service-category.edit', compact('category', 'langs', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceCategoryRequest $request, $id)
    {
        $category = ServiceCategory::find($id);
        $category->update($request->all());
        $this->updateData($request->all(), $category, new ServiceCategoryData());
        return redirect()->route('service-category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ServiceCategory::destroy($id);
        ServiceCategoryData::where('service_category_id', $id)->delete();
        return redirect()->route('service_category.index');
    }
}
