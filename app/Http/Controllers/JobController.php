<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\Category;
use App\Country;
use App\Http\Requests\JobRequest;
use App\Job;
use App\JobCategory;
use App\JobData;
use App\Organization;
use App\Services\Helper;
use App\Services\ImageService;
use App\SubCategory;
use App\Type;
use App\User;
use App\UserProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;


class JobController extends Controller
{
    protected $imageService;

    /**
     * JobController constructor.
     * @param ImageService $imageService
     */
    public function __construct(ImageService $imageService)
    {

        $this->imageService = $imageService;
    }

    public function updateData($requestData, $object, $dataObject)
    {
        $fillableArray = array_flip($dataObject->getFillable());
        $localeCode = Config::get('app.locale');

        $dataToFind = [];
        $data = [];

        foreach ($fillableArray as $key => $value) {

            if ($key == $dataObject->foreignKey) {
                $dataToFind[$key] = $object->id;
            } elseif ($key == 'lang') {
            } else {

                if (isset($requestData[$key . '_' . $localeCode])) {
                    $requestDataKey = $key . '_' . $localeCode;
                    if ($key == 'description' || $key == 'responsibilities' || $key == 'requirements' || $key == 'about'
                        || $key == 'about_company' || $key == 'procedures' || $key == 'notes'
                    ) {
                        $requestData[$requestDataKey] = str_replace("\r\n", "<br />", $requestData[$requestDataKey]);
                    }
                    if ($key == 'opening_date') {
                        $requestData[$requestDataKey] = Carbon::parse($requestData[$requestDataKey])->format('d F Y');
                    }
                    $data[$key] = $requestData[$requestDataKey];
                } else {

                    $data[$key] = null;
                }

            }
        }
        $dataObject::updateOrCreate($dataToFind, $data);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        return redirect()->route('jobs');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type, $job_id = null)
    {
        $user = Auth::user();
        $userProducts = $user->jobProducts;
        $previous_url = url()->previous();
        $about_company = null;
        if (isset($user) && isset($user->organization) && isset($user->organization->about_company)) {
            $about_company = str_replace("<br />", "\r\n", $user->organization->about_company);
        }
        $category = Category::all();


        if (Auth::user()->is_organization == 1 && !isset($user->email)) {
            return redirect()->route('user.register_form_step_2_organization');
        }
        $users = User::whereHas('organization')->with('organization')->where('status', 1)->get();

        $phone = json_decode($user->phone, true);
        $phone = $phone[0]["phone"];
        $localeCode = Config::get('app.locale');
        $data = [];
        $data[$localeCode] = new JobData();
        $type = Type::where('key', $type)->first();
        $translate = null;
        $countries = Country::all();
        $startDate = now();
        $start = date('d-m-Y', strtotime('+3 days', strtotime($startDate)));
        $end = date('d-m-Y', strtotime('+1 month', strtotime($startDate)));
        $start = Carbon::parse($start)->format('d M Y');
        $end = Carbon::parse($end)->format('d M Y');
        $start_opening_date = Carbon::parse($startDate)->format('d M Y');
        $loc_city = null;
        $loc_country = null;


        if (count($user->address) != 0) {
            $loc_city = $user->address[0]->city;
            $loc_country = $user->address[0]->country_id;
        }

        return view('profile.job.create', compact('localeCode', 'data', 'type', 'job_id', 'user',
            'phone', 'translate', 'countries', 'loc_city', 'loc_country', 'category', 'about_company', 'start',
            'end', 'users', 'start_opening_date', 'previous_url', 'userProducts'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobRequest $request)
    {
        $category = [];
        $data = $request->all();
        $data['user_id'] = Auth::id();
        $data['expiration_date'] = Carbon::parse($request->expiration_date)->format('Y-m-d');
        if (isset($request->org_id)) {
            $data['user_id'] = $request->org_id;
        }
        if (isset($request->online_submission)) {
            $data['online_submission'] = 1;
        } else {
            $data['online_submission'] = 0;
        }
        $localeCode = Config::get('app.locale');
        if (isset($request->announcement_id)) {
            $job = Job::findorfail($request->announcement_id);
            $job->update($data);
        } else {
            $job = Job::create($data);
        }

        $job->sort_by = $job->updated_at;
        $job->save();

        JobCategory::where('job_id', $job->id)->delete();
        Attachment::where('job_id', $job->id)->delete();
        if (isset($request->category) && $request->category != 'false') {
            $categories = explode(",", $request->category);
            if (isset($request->other_child)) {
                $cat = Category::where('name', 'other')->first();
                if (isset($request->new_cat_id)) {
                    $new_cat = Category::findorfail($request->new_cat_id);
                    $new_cat->name = $request->other_child;
                    $new_cat->save();
                } else {
                    $new_cat = Category::create(['name' => $request->other_child, 'category_id' => $cat->id]);
                }

                $categories[] = $new_cat->id;
            }

            foreach ($categories as $item) {
                $s = Category::find($item);

                if (!isset($s->parent)) {
                    if ($s->name == 'other') {
                        $category[] = $s->id;
                    } elseif (!in_array($s->id, $category)) {
                        $category[] = $s->id;
                    }
                } else {
                    if (!in_array($s->parent->id, $category)) {
                        $category[] = $s->parent->id;
                    }
                    JobCategory::create(['job_id' => $job->id, 'category_id' => $item]);
                }


            }

            foreach ($category as $item) {
                JobCategory::create(['job_id' => $job->id, 'category_id' => $item]);
            }
        }


        $slug = Helper::getSlug($data['title_' . $localeCode], 'App\Job', $job);
        $job->slug = $slug;
        $job->added_user_id = Auth::id();
        $job->added_IP = $request->ip();
        $job->save;

        if (!isset($data['menu_' . $localeCode])) {
            if (isset($data['organization_' . $localeCode]) && isset($data['title_' . $localeCode])) {
                $data['menu_' . $localeCode] = $data['organization_' . $localeCode] . '-' . $data['title_' . $localeCode];
            }

        }


        $this->updateData($data, $job, new JobData());
        if (isset($job->data->about_company) && isset($job->user) && !$job->user->organization->about_company) {
            $job->user->organization->about_company = $job->data->about_company;
            $job->user->organization->save();

        }

        if (isset($request->file_count) && $request->file_count != 0) {
            for ($i = 0; $i < $request->file_count; $i++) {
                $type = substr($request["file"][$i]->getMimeType(), 0, 5);
                $attachment = $job->attachment()->create(['name' => $request["file"][$i]->getClientOriginalName(), 'description' => $request->file_description[$i]]);
                $this->imageService->saveAttachments($job->id, $request["file"][$i], 'public', $attachment, $key = 'file');
                if ($type == 'image') {
                    $attachment->image = 1;
                    $attachment->save();
                }
            }

        }

        switch ($request->action) {
            case 'draft':
                $job->status = 'draft';
                $job->save();
                if (isset($new_cat)) {
                    return response()->json(['announcement_id' => $job->id, 'new_cat_id' => $new_cat->id]);
                }
                return response()->json(['announcement_id' => $job->id]);

                break;
            case 'save':
                if (Auth::user()->role == 'pro_organization' || Auth::user()->role == 'admin' || Auth::user()->role == 'moderator') {
                    $job->status = 'posted';
                } else {
                    $job->status = 'draft';
                }
                $job->save();
                break;
            case 'preview':
                $job->status = 'draft';
                $job->save();

                return response()->json(['redirect' => route('jobs', $job->slug)]);
                break;
        }

        return response()->json(['redirect' => route('jobs')]);


    }

    public function attachments(Request $request)
    {
        $job = Job::find($request->id);
        $attachments = $job->attachment;
        return $attachments;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Job $job
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Job $job
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $translate = null)
    {

        $user = Job::findorfail($id)->user;
        $previous_url = url()->previous();
        $about_company = null;
        if (isset($user) && isset($user->organization) && isset($user->organization->about_company)) {
            $about_company = str_replace("<br />", "\r\n", $user->organization->about_company);
        }


        $users = User::whereHas('organization')->with('organization')->where('status', 1)->get();
        if (isset($user) && isset($user->phone)) {
            $phone = json_decode($user->phone, true);
            $phone = $phone[0]["phone"];
        } else {
            $phone = null;
        }
        $countries = Country::all();
        $localeCode = Config::get('app.locale');
        $data = [];
        $data[$localeCode] = JobData::where('job_id', $id)->first();
        if (isset($data[$localeCode]->description)) {
            $data[$localeCode]->description = str_replace("<br />", "\r\n", $data[$localeCode]->description);
        }
        if (isset($data[$localeCode]->responsibilities)) {
            $data[$localeCode]->responsibilities = str_replace("<br />", "\r\n", $data[$localeCode]->responsibilities);
        }
        if (isset($data[$localeCode]->requirements)) {
            $data[$localeCode]->requirements = str_replace("<br />", "\r\n", $data[$localeCode]->requirements);
        }
        if (isset($data[$localeCode]->about)) {
            $data[$localeCode]->about = str_replace("<br />", "\r\n", $data[$localeCode]->about);
        }
        if (isset($data[$localeCode]->about_company)) {
            $data[$localeCode]->about_company = str_replace("<br />", "\r\n", $data[$localeCode]->about_company);
        }
        if (isset($data[$localeCode]->procedures)) {
            $data[$localeCode]->procedures = str_replace("<br />", "\r\n", $data[$localeCode]->procedures);
        }
        if (isset($data[$localeCode]->notes)) {
            $data[$localeCode]->notes = str_replace("<br />", "\r\n", $data[$localeCode]->notes);
        }


        $job = Job::whereHas('data')->with('data', 'category')->findorfail($id);


        $startDate = Carbon::parse($job->created_at)->format('Y-m-d');
        $start = date('d-m-Y', strtotime('+3 days', strtotime($startDate)));
        $end = date('d-m-Y', strtotime('+1 month', strtotime($startDate)));
        $start_opening_date = Carbon::parse($startDate)->format('d M Y');


        $start = Carbon::parse($start)->format('d M Y');
        $end = Carbon::parse($end)->format('d M Y');

        $sub_category = [];

        if (isset($job->category) && count($job->category)) {
            foreach ($job->category as $item) {
                $sub_category[] = $item->id;
            }
        }


        $type = $job->type;
        $langs = [];
        $loc_city = null;
        $loc_country = null;
        $category = Category::all();

        if (isset($user)) {
            if (count($user->address) != 0) {
                $loc_city = $user->address[0]->city;
                $loc_country = $user->address[0]->country_id;
            }
        }

        $job_id = null;


        return view('profile.job.edit', compact('job', 'localeCode', 'data', 'type', 'user', 'phone',
            'translate', 'job_id', 'langs', 'countries', 'loc_country', 'loc_city', 'category', 'sub_category',
            'about_company', 'start', 'end', 'users', 'start_opening_date', 'previous_url'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Job $job
     * @return \Illuminate\Http\Response
     */
    public function update(JobRequest $request, $id)
    {

        $job = Job::findorfail($id);
        if ($request->action == 'preview' && !isset($job->mirror_id)) {
            $job = $job->clone($id);
        }
        $last_updated_at = $job->sort_by;
        $category = [];
        $localeCode = Config::get('app.locale');
        JobCategory::where('job_id', $job->id)->delete();
        $job->edited_user_id = Auth::id();
        $job->edited_IP = $request->ip();
        $job->save;
        $data = $request->all();
        $data['expiration_date'] = Carbon::parse($request->expiration_date)->format('Y-m-d');

        if (isset($request->online_submission)) {
            $data['online_submission'] = 1;
        } else {
            $data['online_submission'] = 0;
        }
        if (isset($request->same_place)) {
            $data['same_place'] = 0;
        } else {
            $data['same_place'] = 1;

        }
        if (isset($request->org_id)) {
            $data['user_id'] = $request->org_id;
        }
        JobCategory::where('job_id', $job->id)->delete();
//        if (isset($request->category)) {
//            $categories = explode(",", $request->category);
//            foreach ($categories as $item) {
//                JobCategory::create(['job_id' => $job->id, 'category_id' => $item]);
//            }
//        }
        if (isset($request->category) && $request->category != 'false') {
            $categories = explode(",", $request->category);
            if (isset($request->other_child)) {
                $cat = Category::where('name', 'other')->first();
                $new_cat = Category::create(['name' => $request->other_child, 'category_id' => $cat->id]);
                $categories[] = $new_cat->id;
            }

            foreach ($categories as $item) {
                $s = Category::find($item);

                if (!isset($s->parent)) {
                    if ($s->name == 'other') {
                        $category[] = $s->id;
                    } elseif (!in_array($s->id, $category)) {
                        $category[] = $s->id;
                    }
                } else {
                    if (!in_array($s->parent->id, $category)) {
                        $category[] = $s->parent->id;
                    }
                    JobCategory::create(['job_id' => $job->id, 'category_id' => $item]);
                }


            }

            foreach ($category as $item) {
                JobCategory::create(['job_id' => $job->id, 'category_id' => $item]);
            }
        }

        $job->updated_at = Carbon::now();
        $job->update($data);
        if (!isset($request->same_place)) {
            $job->sort_by = $last_updated_at;
            $job->save();
        } else {
            $job->sort_by = $job->updated_at;
            $job->save();
        }
        if (!isset($data['menu_' . $localeCode])) {
            if (isset($data['organization_' . $localeCode]) && isset($data['title_' . $localeCode])) {
                $data['menu_' . $localeCode] = $data['organization_' . $localeCode] . '-' . $data['title_' . $localeCode];
            }

        }
        $this->updateData($data, $job, new JobData());
        if (isset($job->data->about_company) && isset($job->user) && !$job->user->organization->about_company) {
            $job->user->organization->about_company = $job->data->about_company;
            $job->user->organization->save();

        }
        if (isset($request->file_count) && $request->file_count != 0) {
            for ($i = 0; $i < $request->file_count; $i++) {
                if (isset($request->file_description[$i]) && isset($request["file"][$i])) {
                    $type = substr($request["file"][$i]->getMimeType(), 0, 5);
                    $attachment = $job->attachment()->create(['name' => $request["file"][$i]->getClientOriginalName(), 'description' => $request->file_description[$i]]);
                    $this->imageService->saveAttachments($job->id, $request["file"][$i], 'public', $attachment, $key = 'file');
                    if ($type == 'image') {
                        $attachment->image = 1;
                        $attachment->save();
                    }
                }
            }

        }

        if (isset($request->edited_description) && count($request->edited_description)) {
            foreach ($request->edited_description as $key => $value) {
                $file = Attachment::find($key);
                $file->update(['description' => $value]);
                $file->save();
            }
        }
        $status = 'my-draft';
        switch ($request->action) {

            case 'draft':
                if (isset($job->mirror)) {
                    $slug = $job->mirror->slug;
                    Job::destroy($job->mirror->id);
                    $job->slug = $slug;
                    $job->mirror_id = null;
                }
                $job->status = 'draft';
                $job->save();
                if (Auth::user()->can('accessAll_announcement') && $job->user_id != Auth::id()) {
                    $status = 'draft';
                }
                return response()->json(['redirect' => '/jobs?status=' . $status]);
                break;
            case 'save':
                if (Auth::user()->role == 'pro_organization' || Auth::user()->role == 'admin' || Auth::user()->role == 'moderator') {
                    $job->status = 'posted';
                } else {
                    $job->status = 'pending';
                }
                $job->save();
                break;
            case 'preview':
                $job->save();
                return response()->json(['redirect' => route('jobs', $job->slug)]);

                break;
            case 'cikl':
                if (isset($job->mirror)) {
                    $slug = $job->mirror->slug;
                    Job::destroy($job->mirror->id);
                    $job->slug = $slug;
                    $job->mirror_id = null;
                }
                $job->status = 'draft';
                $job->save();
                return response()->json(['cikl' => 'cikl']);


        }
        return response()->json(['redirect' => route('jobs')]);

    }

    public function changeStatus(Request $request, $id)
    {
        $job = Job::findorfail($id);
        $status = $request->status;
        if($job->status != 'pending')
        {
            if($status != 'postandemail')
            {
                if ($job->same_place == 0) {
                    $job->sort_by = Carbon::now();
                    $job->save();
                }
            }

        }
        else
        {
            $job->sort_by = Carbon::now();
            $job->save();
        }

        $subject = trans('types.' . $job->type->key) . ' - ' . $job->data->title . '/' . $job->data->organization;

        $link = route('jobs', ['slug' => $job->slug]);
        if ($status != 'postandemail') {
            $job->status = $request->status;
            if ($status == 'expired') {
                $job->expiration_date = Carbon::now();
                $job->save();
            }
            if ($status == 'posted') {
                if ($job->expiration_date < Carbon::now()) {
                    $job->status = 'expired';
                    $status = 'expired';
                }
            }
        } else {
            Mail::send('email.test', ['first' => $job], function ($message) use ($job, $subject) {
                $message->to('careercenter-am@yahoogroups.com')->subject($subject);
                $message->from('mailinglist@careercenter.am', 'CareerCenter');

            });
            $job->status = 'posted';
        }
        $job->edited_user_id = Auth::id();
        $job->edited_IP = $request->ip();
        $job->save();





        if($status != 'pending' && $status != 'postandemail')
        {
            if ($job->same_place == 0) {
                $job->sort_by = Carbon::now();
                $job->save();
            }

        }
        elseif($status == 'pending')
        {
                $job->sort_by = Carbon::now();
                $job->save();
        }


        if ($status != 'rejected') {
            if (Auth::user()->can('accessAll_announcement') && $job->user_id != Auth::id()) {
                $status = $job->status;
            } else {
                $status = 'my-' . $job->status;
            }
        }
        if ($status == 'expired') {
            return redirect()->route('jobs', $job->slug);
        }

        if ($status == 'rejected') {
            return back();
        }
        return redirect('/jobs/' . $job->slug . '?status=' . $status);

    }

    public function afterPreview(Request $request, $id)
    {
        $job = Job::findorfail($id);
        $job->updated_at = Carbon::now();
        $job->save();
        if ($job->same_place == 0) {
            $job->sort_by = Carbon::now();
            $job->save();
        }

        $mirror = $job->mirror;
        $subject = trans('types.' . $mirror->type->key) . ' - ' . $mirror->data->title . '/' . $mirror->data->organization;
        $status = 'pending';
        $link = route('jobs', ['slug' => $mirror->slug]);
        if ($mirror->status == 'pending') {
            $job->sort_by = Carbon::now();
            $job->save();
        }

        if ($request->action == 'save_changes') {

            $mirror = $job->mirror;
            $status = $mirror->status;
        } elseif ($request->action == 'post_and_email' || $request->action == 'post') {
            $status = 'posted';
            $mirror->save();
        } elseif ($request->action == 'save_as_draft') {
            $status = 'draft';
        } elseif ($request->action == 'submit') {
            Session::put('submit', true);
        }


        if ($request->action == 'save_changes' || $request->action == 'submit' || $request->action == 'post_and_email'
            || $request->action == 'post' || $request->action == 'save_as_draft'
        ) {

            JobCategory::where('job_id', $mirror->id)->delete();
            Attachment::where('job_id', $mirror->id)->delete();
            $slug = $mirror->slug;
            $mirror->update($job->toArray());
            $mirror->edited_user_id = Auth::id();
            $mirror->edited_IP = $request->ip();
            $mirror->mirror_id = null;
            $mirror->sort_by = $job->sort_by;
            $mirror->save();
            $mirror->slug = $slug;
            $mirror->status = $status;
            $mirror->save();
            $data = $mirror->data;
            $mirror->data->update($job->data->toArray());
            $data->job_id = $mirror->id;
            $data->save();
            $attachments = $job->attachment;
            if (count($attachments) != 0) {
                foreach ($attachments as $item) {
                    $item->job_id = $mirror->id;
                    $item->save();
                }
            }
            $categories = JobCategory::where('job_id', $job->id)->get();
            if (count($categories) != 0) {
                foreach ($categories as $item) {
                    $item->job_id = $mirror->id;
                    $item->save();
                }
            }

            Job::destroy($id);
            JobData::where('job_id', $id)->delete();
            if ($request->action == 'post_and_email') {
                Mail::send('email.test', ['first' => $mirror], function ($message) use ($subject) {
                    $message->to('careercenter-am@yahoogroups.com')->subject($subject);
                    $message->from('mailinglist@careercenter.am', 'CareerCenter');

                });
            }


            if ($status != 'rejected') {
                if (Auth::user()->can('accessAll_announcement') && $mirror->user_id != Auth::id()) {
                    $status = $mirror->status;
                } else {
                    $status = 'my-' . $mirror->status;
                }
            }
            if ($status == 'expired') {
                return redirect()->route('jobs', $mirror->slug);
            }
            if ($status == 'rejected') {
                return redirect('/jobs/?status=pending');
            }
            return redirect('/jobs/' . $mirror->slug . '?status=' . $status);

        } elseif ($request->action == 'cancel') {
            JobCategory::where('job_id', $id)->delete();
            Attachment::where('job_id', $id)->delete();
            Job::destroy($id);
            JobData::where('job_id', $id)->delete();
            return redirect()->route('announcements-edit', $mirror->id);
        }
    }

    public function jobDestroy($id, Request $request)
    {
        $job = Job::findorfail($id);
        $job->attachment()->delete();
        $job->delete();
        JobData::where('job_id', $id)->delete();
        return redirect('/jobs?status=' . $request->status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Job $job
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job = Job::findorfail($id);
        $job->attachment()->delete();
        $job->delete();
        JobData::where('job_id', $id)->delete();
        return back();
    }

    public function removeAttachment(Request $request)
    {
        $id = $request->id;
        Attachment::destroy($id);
        return 'success';
    }

    public function clone($id)
    {
        $job = Job::with('data')->find($id);
        $new_job = $job->replicate();
        $new_job->status = 'draft';
        $new_job->views = 0;
        $new_job->sort_by = now();
        $new_job->save();


        $job_data = $job->data;
        $new_data = $job_data->replicate();
        $new_data->job_id = $new_job->id;
        $new_data->save();

        $new_job_slug = Helper::getSlug($new_job->data->title, 'App\Job', $new_job);
        $new_job->slug = $new_job_slug;
        $new_job->save();
        $attachments = $job->attachment;
        if (count($attachments) != 0) {
            foreach ($attachments as $item) {
                $new_attachment = $item->replicate();
                $new_attachment->job_id = $new_job->id;
                $new_attachment->save();
            }
        }
        $categories = JobCategory::where('job_id', $job->id)->get();


        if (count($categories) != 0) {
            foreach ($categories as $item) {
                $new_category = $item->replicate();
                $new_category->job_id = $new_job->id;
                $new_category->save();
            }
        }

        return redirect()->route('announcements-edit', $new_job->id);

    }

    public function submit($id)
    {
        $job = Job::findorfail($id);
        $job->status = 'pending';
        $job->save();
        $last_updated_at = $job->sort_by;
        if ($job->same_place == 1) {
            $job->sort_by = $last_updated_at;
            $job->save();
        } else {
            $job->sort_by = $job->updated_at;
            $job->save();
        }
//        $userproduct = UserProduct::findorfail($job->product_id);
//        $userproduct->quantity--;
//        $userproduct->save();
        Session::put('submit', true);
        if (Auth::user()->can('accessAll_announcement') && $job->user_id != Auth::id()) {
            $status = 'pending';
        } else {
            $status = 'my-pending';
        }
        return redirect('/jobs?status=' . $status);
    }

    public function download($id)
    {
        $file = Attachment::findorfail($id);
        $headers = [
            'Content-Type' => 'application/pdf',
        ];
        $path = '/storage/' . $file->file;
        return response()->download($path, 'file.pdf', $headers);
    }

    public function getCompany(Request $request)
    {
        $user = User::find($request->id);
        $company = $user->organization;
        $info = [];
        $phone = null;
        if (isset($user->phone)) {
            $phone = json_decode($user->phone, true);
            $phone = $phone[0]["phone"];
        }
        $info['contact_title'] = $company->position;
        $info['contact_person'] = $user->first_name . ' ' . $user->last_name;
        $info['about_company'] = str_replace("<br />", "\r\n", $company->about_company);
        $info['email'] = $user->email;
        $info['phone'] = $phone;
        return $info;

    }
}
