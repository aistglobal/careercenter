<?php

namespace App\Http\Controllers;

use App\Country;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\UserDetailsRequest;
use App\Services\ImageService;
use App\Services\ProfileService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    protected $imageService;

    /**
     * ProfileController constructor.
     * @param ImageService $imageService
     */
    public function __construct(ImageService $imageService)
    {


        $this->imageService = $imageService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $about_company = null;
        $phone = json_decode($user->phone, true);

        if (!isset($user->email)) {
            if ($user->is_organization == '0') {
                return redirect()->route('user.register_form_step_2_individual');
            } else {
                return redirect()->route('user.register_form_step_2_organization');
            }
        }
        $countries = Country::all();
        if ($user->is_organization == 1) {
            if (isset($user->organization) && isset($user->organization->about_company)) {
                $about_company = str_replace("<br />", "\r\n", $user->organization->about_company);
            }
            return view('profile.user-info.organization', compact('user', 'countries', 'phone', 'about_company'));
        } else {
            return view('profile.user-info.individual', compact('user', 'countries', 'phone'));
        }
    }

    /**
     * @param Request $request
     * @param ProfileService $profileService
     * @return string
     */
    public function profileImage(Request $request, ProfileService $profileService)
    {
    $data = $request->all();
    try {
      $profileService->addProfileImage($data);
      return 'Success';
    } catch (\Exception $exception) {
      dd($exception->getMessage());
    }

//        $user = Auth::user();
//        if (isset($request['image']) && $request['image'] != 'null') {
//            $imageFileName = time() . rand(1, 999999999) . '.jpg';
//            $dataImage = $request['image'];
//            list($type, $dataImage) = explode(';', $dataImage);
//            list(, $dataImage) = explode(',', $dataImage);
//            $dataImage = base64_decode($dataImage);
//            $s3 = Storage::disk('public');
//            $filePath = 'logos/' . $imageFileName;
//            $s3->put($filePath, $dataImage, 'public');
//            $user->image = 'logos/' . $imageFileName;
//            $user->save();
//
//            return 'Success';
//        }
    }


  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * @param UserDetailsRequest $request
   * @param $id
   * @param ProfileService $profileService
   * @return \Illuminate\Http\RedirectResponse
   */
  public function update(UserDetailsRequest $request, $id, ProfileService $profileService)
  {
//        $user = User::find($id);
//        $user->address()->delete();
//        if (isset($request->address)) {
//            $count = count($request->address);
//            for ($i = 0; $i < $count; $i++) {
//                $user->address()->create([
//                    'address' => $request->address[$i],
//                    'zip' => $request->zip[$i],
//                    'country_id' => $request->country[$i],
//                    'city' => $request->city[$i],
//                    'address_type' => $request->address_type[$i]
//
//                ]);
//            }
//        }
//
//        $data = $request->all();
//        $data['phone'] = null;
//
//        if (count($request->phone) != 0) {
//            $comb = array_combine($request->phone, $request->phone_type);
//
//            $b = [];
//            foreach ($comb as $key => $value) {
//                $b[] = array('phone' => $key, 'type' => $value);
//            }
//
//            $phone = json_encode($b);
//            $data['phone'] = $phone;
//        }
//
//        $user->update($data);
//        $data['user_id'] = $user->id;
//        if ($user->is_organization == 1) {
//            $organization = Organization::where('user_id', $user->id)->first();
//            $organization->update($data);
//            if ($request->hasFile('image')) {
//                $this->imageService->savePhoto($request['image'], 'public', $user, $key = 'image');
//            }
//        }
    try {
      $profileService->update($request, $id);

        flash()->success(trans('profile_tables.profile_updated_successfully'));
      return redirect()->route('profile');
    }
    catch (\Exception $exception) {
      dd($exception->getMessage());
    }
  }

  /**
   * @param PasswordRequest $request
   * @param $id
   * @param ProfileService $profileService
   * @return \Illuminate\Http\RedirectResponse
   */
  public function changePassword(PasswordRequest $request, $id, ProfileService $profileService)
  {
    $data = $request->all();
    try {
      $message = $profileService->changePassword($data, $id);
        flash()->success(trans('profile_tables.password_updated_successfully'));
      return back();
    } catch (\Exception $exception) {
      dd($exception->getMessage());
    }
  }
}
