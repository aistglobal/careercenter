<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReviewRequest;
use App\Review;
use App\ReviewData;
use App\Services\ImageService;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ReviewController extends Controller
{
    protected $imageService;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(ImageService $imageService)
    {

        $this->imageService = $imageService;
    }

    public function updateData($requestData, $object, $dataObject)
    {
        $fillableArray = array_flip($dataObject->getFillable());
        $langs = LaravelLocalization::getSupportedLocales();
        foreach ($langs as $localeCode => $properties) {
            $create=false;
            $dataToFind = [];
            $data = [];

            foreach ($fillableArray as $key => $value) {
                if ($key == $dataObject->foreignKey) {
                    $dataToFind[$key] = $object->id;
                } elseif ($key == 'lang') {
                    $dataToFind[$key] = $localeCode;
                } else {
                    if (isset($requestData[$key . '_' . $localeCode])) {
                        $create = true;
                        $requestDataKey = $key . '_' . $localeCode;
                        $data[$key] = $requestData[$requestDataKey];
                    }
                }
            }
            if($create==true)
            {
                $dataObject::updateOrCreate($dataToFind, $data);
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $order = 20;
        if (isset($request->order_by)) {
            $order = $request->order_by;
        }

        $reviews = Review::when($search, function ($query) use ($search) {

            $query->whereHas('data', function ($query) use ($search) {
                return $query->where('review', 'LIKE', "%$search%");
            });

        })->paginate($order);

        return view('profile.review.index', compact('reviews', 'search', 'order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $langs = LaravelLocalization::getSupportedLocales();
        $data = [];
        foreach ($langs as $localeCode => $properties) {
            $data[$localeCode] = new ReviewData();
        }
        return view('profile.review.create', compact('langs', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReviewRequest $request)
    {
        $review = Review::create($request->all());
        if ($request->hasFile('image')) {
            $this->imageService->savePhoto($request->image, 'public', $review, $key = 'image');
        }

        $this->updateData($request->all(), $review, new ReviewData());

        return redirect()->route('reviews.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $langs = LaravelLocalization::getSupportedLocales();
        $data = [];
        foreach ($langs as $localeCode => $properties) {
            $data[$localeCode] = ReviewData::where('review_id', $id)->where('lang', $localeCode)->first();
        }
        $review = Review::findorfail($id);

        return view('profile.review.edit', compact('review', 'langs', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function update(ReviewRequest $request, $id)
    {
        $review = Review::findorfail($id);
        $review->update($request->all());
        if ($request->hasFile('image')) {
            $this->imageService->savePhoto($request->image, 'public', $review, $key = 'image');
        }

        $this->updateData($request->all(), $review, new ReviewData());
        return redirect()->route('reviews.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Review::destroy($id);
        ReviewData::where('review_id', $id)->delete();
        return redirect()->route('reviews.index');
    }
}
