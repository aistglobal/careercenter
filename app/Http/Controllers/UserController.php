<?php

namespace App\Http\Controllers;

use App\Country;
use App\Http\Requests\BankDetailsRequest;
use App\Http\Requests\FinalRegistrationRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\UserDetailsRequest;
use App\Http\Requests\UserPasswordRequest;
use App\Http\Requests\UserRegisterStep1Request;
use App\Organization;
use App\Permission;
use App\PermissionUser;
use App\Role;
use App\RoleUser;
use App\Services\Helper;
use App\Services\ImageService;
use App\Services\ProfileService;
use App\User;
use App\UserBank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class UserController extends Controller
{
    protected $imageService;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(ImageService $imageService)
    {

        $this->imageService = $imageService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::all();
        $role = $request->role;
        $searchUsername = $request->searchUsername;
        $searchFirstName = $request->searchFirstName;
        $arr = explode(' ', $searchFirstName);
        $searchLastName = $request->searchLastName;
        $status = $request->get('status', 1);
        $order = 20;
        if (isset($request->order_by)) {
            $order = $request->order_by;
        }

        Session::put('back', $request->getRequestUri());


        $users = User::when($searchUsername, function ($query) use ($searchUsername) {
            return $query->where(function ($q) use ($searchUsername) {
                return $q->where('username', 'LIKE', "%$searchUsername%")->orwhere('email', 'LIKE', "$searchUsername%")->
                orwhereHas('organization', function ($a) use ($searchUsername) {
                    return $a->where('name', 'LIKE', "%$searchUsername%");
                });
            });
        })->when($searchFirstName, function ($query) use ($arr) {
            if (isset($arr[0]) && $arr[0] != '') {
                $query->where(function ($q) use ($arr) {
                    $q->where('first_name', 'LIKE', "%$arr[0]%");
                });
            }
            if (isset($arr[1]) && $arr[1] != '') {
                $query->where(function ($q) use ($arr) {
                    $q->where('middle_name', 'LIKE', "%$arr[1]%");
                });
            }
            return $query;
        })
            ->when($searchLastName, function ($query) use ($searchLastName) {
                return $query->where('last_name', 'LIKE', "%$searchLastName%");
            })
            ->when($role, function ($query) use ($role) {

                $query->whereHas('role', function ($query) use ($role) {
                    return $query->where('id', $role);
                });

            })
            ->where('status', $status)->latest()->paginate($order)->appends(request()->query());

        return view('profile.user.index', compact('users', 'searchUsername', 'searchFirstName', 'searchLastName', 'order', 'roles', 'role', 'status'));
    }

    public function registerStep1(UserRegisterStep1Request $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt($request->get('password'));

        $user = User::create($data);
        $user->attachRole($request->role_id);
        $user->is_organization = ($user->role[0]->name && $user->role[0]->name != 'organization' && $user->role[0]->name != 'pro_organization' && $user->role[0]->name != 'admin') ? 0 : 1;
        $user->save();
        if ($user->is_organization == '0') {
            return redirect()->route('profile.register_form_step_2_individual', $user->id);

        } else {
            $organization = Organization::create(['user_id' => $user->id]);
            $user->organization_id = $organization->id;
            $user->save();
            return redirect()->route('profile.register_form_step_2_organization', $user->id);

        }
    }

    public function registerOrganizationForm($id)
    {
        $user = User::find($id);
        if (isset($user->email)) {
            return redirect()->route('profile');
        }
        if ($user->is_organization == '0') {
            return redirect()->route('profile.register_form_step_2_individual', $id);
        }
        $countries = Country::all();

        return view('profile.user.register-step-2-organization', compact('user', 'countries'));
    }

    public function registerIndividualForm($id)
    {
        $user = User::find($id);
        if (isset($user->email)) {
            return redirect()->route('profile');
        }
        if ($user->is_organization == '1') {
            return redirect()->route('profile.register_form_step_2_organization', $id);
        }
        $countries = Country::all();
        return view('profile.user.register-step-2-individual', compact('user', 'countries'));
    }

    public function registerStep2(FinalRegistrationRequest $request, $id)
    {
        $user = User::find($id);
        $count = count($request->address);
        for ($i = 0; $i < $count; $i++) {
            if ($request->address[$i] != null || $request->zip[$i] != null || $request->country[$i] != null || $request->city[$i] != null) {
                $user->address()->create([
                    'address' => $request->address[$i],
                    'zip' => $request->zip[$i],
                    'country_id' => $request->country[$i],
                    'city' => $request->city[$i],
                    'address_type' => $request->address_type[$i]
                ]);
            }

        }
        $data = $request->all();
        $data['phone'] = null;
        if (count($request->phone) != 0) {
            $comb = array_combine($request->phone, $request->phone_type);
            $b = [];
            foreach ($comb as $key => $value) {
                $b[] = array('phone' => $key, 'type' => $value);
            }

            $phone = json_encode($b);
            $data['phone'] = $phone;
        }
        $user->update($data);
        if ($request->hasFile('image')) {
            $this->imageService->saveProfilePhoto($request['image'], 'public', $user, $key = 'image');
        }
        if ($user->is_organization == 1) {
            $organization = Organization::where('id', $user->organization_id)->first();
            $data['about_company'] = str_replace("\r\n", "<br />", $request->about_company);
            $slug = Helper::getSlug($data['name'], 'App\Organization', $organization);
            $organization->slug = $slug;
            $organization->save();
            if (isset($request->legal_structure) && $request->legal_structure == 'other') {
                if (isset($request->legal_other)) {
                    $data['legal_structure'] = $data['legal_other'];
                }
            }
            $organization->update($data);
        }
        flash()->success(trans('profile_tables.user_created_successfully'));
        return redirect()->route('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
//        if (!Auth::user()->can('edit_admin')) {
//            unset($roles['admin']);
//        }
        return view('profile.user.register-step-1', compact('roles'));
    }

    public function loginUser($id, $origin = 'yes')
    {
        Session::put('origin_account', Auth::user());
        if ($origin == 'no' || $id == Auth::id()) {
            Session::forget('origin_account');
        }
        Auth::loginUsingId($id);
        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $about_company = null;
        $role = $user->role[0]->id;
        $roles = Role::all();
        $countries = Country::all();
        $phone = json_decode($user->phone, true);
        $back = route('users.index');
        if (Session::has('back')) {
            $back = Session::get('back');

        }

        if (isset($user->organization) && isset($user->organization->about_company)) {
            $about_company = str_replace("<br />", "\r\n", $user->organization->about_company);
        }
        if ($user->is_organization == 0) {
            $users = User::whereHas('organization')->with('organization')->where('status', 1)->get()->unique('organization_id');
            return view('profile.user.individual-edit', compact('user', 'countries', 'phone', 'roles', 'role', 'about_company', 'users', 'back'));

        } else {
            return view('profile.user.organization-edit', compact('user', 'countries', 'phone', 'roles', 'role', 'about_company', 'back'));

        }


    }

    public function getOrganizations(Request $request)
    {

        $organizations = Organization::select('id', 'name')->whereHas('user')->get();

        return json_encode($organizations);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserDetailsRequest $request, $id)
    {
        $user = User::find($id);
        $user->address()->delete();
        if (isset($request->address)) {
            $count = count($request->address);
            for ($i = 0; $i < $count; $i++) {
                $user->address()->create([
                    'address' => $request->address[$i],
                    'zip' => $request->zip[$i],
                    'country_id' => $request->country[$i],
                    'city' => $request->city[$i],
                    'address_type' => $request->address_type[$i]

                ]);
            }
        }
        $data = $request->all();
        $data['phone'] = null;
        if (count($request->phone) != 0) {
            $comb = array_combine($request->phone, $request->phone_type);
            $b = [];
            foreach ($comb as $key => $value) {
                $b[] = array('phone' => $key, 'type' => $value);
            }

            $phone = json_encode($b);
            $data['phone'] = $phone;
        }
        RoleUser::where('user_id', $user->id)->delete();
        $user->attachRole($request->role_id);
        $data['is_organization'] = ($user->role[0]->name && $user->role[0]->name != 'organization' && $user->role[0]->name != 'pro_organization' && $user->role[0]->name != 'admin') ? 0 : 1;
        $user->update($data);

        if ($request->hasFile('image')) {
            $this->imageService->saveProfilePhoto($request['image'], 'public', $user, $key = 'image');
        }
        $data['user_id'] = $user->id;
        $org = $user->organization;
        if ($user->is_organization == 1) {
            if (isset($request->about_company)) {
                $data['about_company'] = str_replace("\r\n", "<br />", $request->about_company);
            }
            if (isset($request->legal_structure) && $request->legal_structure == 'other') {
                if (isset($request->legal_other)) {
                    $data['legal_structure'] = $data['legal_other'];
                }
            }

            if (isset($org)) {
                $org->update($data);
            } else {
                $org = Organization::create($data);
                $org->slug = $org->id;
                $org->save();
                $user->organization_id = $org->id;
                $user->save();

            }

        } else {
            if (isset($org)) {
                $user->organization_id = null;
                $user->save();
            }
        }
        flash()->success(trans('profile_tables.user_profile_updated_successfully'));
        return back();
    }

    public function changePassword(UserPasswordRequest $request, $id, ProfileService $profileService)
    {
        $data = $request->all();
        try {
            $message = $profileService->changePassword($data, $id);
            flash()->success(trans('profile_tables.password_updated_successfully'));
            return back();
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }

    }

    public function bankDetails(BankDetailsRequest $request, $id)
    {
        $data = $request->all();

        if (isset($request->individual)) {
            $data['individual'] = 1;

        } else {
            $data['individual'] = 0;
        }
        $user = User::findorfail($id);
        $bankDetails = $user->bankDetails;
        if (isset($bankDetails)) {
            $bankDetails = UserBank::where('user_id', $id)->first();
            $bankDetails->update($data);
        } else {
            $user->bankDetails()->create($data);
        }

        flash()->success(trans('profile_tables.bank_details_updated_successfully'));
        return back();
    }

    public function editPermissions($id)
    {
        $user = User::find($id);
        $permissions = Permission::all();
        return view('profile.user.permissions', compact('user', 'permissions'));

    }

    public function updatePermissions(Request $request, $id)
    {
        $user = User::find($id);
        PermissionUser::where('user_id', $id)->delete();
        $permissions = Permission::all();
        $data = $request->all();
        foreach ($permissions as $item) {
            if (isset($data[$item->name])) {
                $user->givePermissionsTo($item->name);
            }
        }
        flash()->success(trans('profile_tables.permissions_updated_successfully'));
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        flash()->success(trans('profile_tables.user_deleted_successfully'));
        return back();
    }

    public function changeStatus($id, Request $request)
    {
        $user = User::findorfail($id);
        $user->status = $request->status;
//        if ($request->status == 0) {
//            PermissionUser::where('user_id', $id)->delete();
//        } elseif ($request->status == 1) {
//            PermissionUser::where('user_id', $id)->delete();
//            foreach (config('permissions.' . $user->role) as $key => $value) {
//                $user->givePermissionsTo($key);
//            }
//        }
        $user->save();
        flash()->success(trans('profile_tables.status_updated_successfully'));
        return back();
    }

    public function connectToOrganization(Request $request)
    {
        $user = User::find($request->user_id)->update(['organization_id' => $request->company_id]);
        return response()->json(['data' => $user]);
    }
}
