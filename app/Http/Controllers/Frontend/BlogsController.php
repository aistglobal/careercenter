<?php

namespace App\Http\Controllers\Frontend;

use App\Blog;
use App\Counter;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\SeoService;

class BlogsController extends Controller
{
    protected $seoService;
    protected $counter;

    /**
     * CourseController constructor.
     */
    public function __construct(SeoService $seoService, Counter $counter)
    {
        $this->seoService = $seoService;
        $this->counter = $counter;
    }


    public function index()
    {
        $this->counter->count();
        $blogs = Blog::whereHas('data')->where('status',1)->paginate(20);
        $this->seoService->setPageMeta('articles');
        if (count($blogs) != 0) {
            $first = $blogs[0];
            $blogs->shift();
        }

        return view('blog', compact('blogs', 'first'));
    }

  /**
   * @param $slug
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
    public function show($slug)
    {
        $this->counter->count();
        $blog = Blog::whereHas('data')->where('slug', $slug)->first();
        if (!$blog) {
            abort(404);
        }
        $related = Blog::where('id', '!=', $blog->id)->whereHas('data')->limit(3)->get();
        $this->seoService->setMetaData($blog, 'blog');

        return view('blog-single', compact('blog', 'related'));
    }
}
