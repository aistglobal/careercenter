<?php

namespace App\Http\Controllers\Frontend;

use App\Blog;
use App\Counter;
use App\Resource;
use App\ResourceCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\SeoService;

class ResourceController extends Controller
{
    protected $seoService;
    protected $counter;

    /**
     * CourseController constructor.
     */
    public function __construct(SeoService $seoService, Counter $counter)
    {
        $this->seoService = $seoService;
        $this->counter = $counter;
    }
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->counter->count();
        $activeItem = $request->get('c',null);
        $activeSub = $request->get('s',null);
        $sub = null;
        if(isset($activeItem))
        {
            $resource = ResourceCategory::where('slug',$activeItem)->first();

            if($resource->has('child'))
            {
                $sub = $resource->child;

            }
        }

//        $latestBlogs = Blog::whereHas('data')->with('data')->latest()->take(2)->get();
        $resources = Resource::whereHas('data')->
        when($activeItem, function ($query) use ($activeItem) {
            $query->whereHas('category', function ($query) use ($activeItem) {
                return $query->where('slug', '=', $activeItem);
            });
        })->when($activeSub, function ($query) use ($activeSub) {
        $query->whereHas('subCategory', function ($query) use ($activeSub) {
            return $query->where('slug', '=', $activeSub);
        });
    })->where('status', 'active')->orderby('id','desc')->paginate(100);
        $categories = ResourceCategory::whereHas('data')->where('parent_id',null)->get();
        $this->seoService->setPageMeta('resources');

        return view('resource', compact('activeItem', 'resources','categories','sub','activeSub'));
    }
}
