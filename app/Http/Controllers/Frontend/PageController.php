<?php

namespace App\Http\Controllers\Frontend;

use App\Counter;
use App\Page;
use App\Services\SeoService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    protected $seoService;
    protected $counter;

    /**
     * CourseController constructor.
     */
    public function __construct(SeoService $seoService, Counter $counter)
    {
        $this->seoService = $seoService;
        $this->counter = $counter;
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($slug)
    {
        $this->counter->count();
        $page = Page::where('slug',$slug)->with('data')->first();
        if(!$page){
            abort(404);
        }
        $this->seoService->setMetaData($page, 'page');
        return view('pages',compact('page'));
    }
}
