<?php

namespace App\Http\Controllers\Frontend;

use App\Counter;
use App\Http\Controllers\Controller;
use App\Review;
use App\Services\SeoService;
use Illuminate\Http\Request;

class ReviewsController extends Controller
{
    protected $seoService;
    protected $counter;

    /**
     * CourseController constructor.
     */
    public function __construct(SeoService $seoService, Counter $counter)
    {
        $this->seoService = $seoService;
        $this->counter = $counter;
    }
    public function index()
    {
        $this->counter->count();
        $this->seoService->setPageMeta('testimonial');
        $reviews = Review::whereHas('data')->orderby('id','DESC')->paginate(20);
       return view('review',compact('reviews'));
    }
}
