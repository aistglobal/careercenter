<?php

namespace App\Http\Controllers\Frontend;

use App\Counter;
use App\Http\Controllers\Controller;
use App\Http\Requests\ApplyJobRequest;
use App\Job;
use App\Services\ImageService;
use App\Services\JobService;
use App\Services\SeoService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class JobsController extends Controller
{

    protected $seoService;
    protected $imageService;
    protected $counter;

    /**
     * JobsController constructor.
     * @param SeoService $seoService
     * @param ImageService $imageService
     */
    public function __construct(SeoService $seoService, ImageService $imageService, Counter $counter)
    {

        $this->seoService = $seoService;
        $this->imageService = $imageService;
        $this->counter = $counter;
    }

    /**
     * @param null $slug
     * @param Request $request
     * @param JobService $jobService
     * @return $this
     */
    public function index($slug = null, Request $request, JobService $jobService)
    {
        $data = $request->all();
        $offset = $request->offset ?? 0;
        $is_ajax = $request->ajax() ?? false;
        $jobs = $jobService->getAll($slug, $data, $offset, $is_ajax);
        if (isset($jobs['error']) && $jobs['error'] == 404) {
            abort(404);
        }
        if ($request->slug)
        {
            $job = Job::where('slug', $slug)->first();
            if (!$job) {
                return redirect()->route('jobs');
            }
        }
        try {
            if ($request->ajax()) {
                return $jobs;
            } else {
                $this->counter->count();
                return view('jobs')->with($jobs);
            }
        } catch
        (\Exception $exception) {
            //dd($exception->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param JobService $jobService
     * @return $this
     */
    public function getJob(Request $request, JobService $jobService)
    {
        try {
            $slug = $request->slug;
            $status = $request->status ?? 'all';
            $job = $jobService->getBySlug($slug, $status);
            return view('job-single')->with($job);
        } catch (\Exception $exception) {
           // dd($exception->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param JobService $jobService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function apply(ApplyJobRequest $request, JobService $jobService)
    {
        $data = $request->all();


        try {
            $jobService->apply($data);
            Session::put('apply_job', 'success');
            return back();
        } catch (\Exception $exception) {
            //dd($exception->getMessage());
        }
    }

    public function applyJob(Request $request,$slug)
    {

        return redirect()->route('jobs',$slug);

    }


}
