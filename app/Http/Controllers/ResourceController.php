<?php

namespace App\Http\Controllers;

use App\Http\Requests\ResourceRequest;
use App\Resource;
use App\ResourceCategory;
use App\ResourceData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ResourceController extends Controller
{

    public function updateData($requestData, $object, $dataObject)
    {

        $fillableArray = array_flip($dataObject->getFillable());
        $langs = LaravelLocalization::getSupportedLocales();
        foreach ($langs as $localeCode => $properties) {
            $create = false;
            $dataToFind = [];
            $data = [];

            foreach ($fillableArray as $key => $value) {
                if ($key == $dataObject->foreignKey) {
                    $dataToFind[$key] = $object->id;
                } elseif ($key == 'lang') {
                    $dataToFind[$key] = $localeCode;
                } else {
                    if (isset($requestData[$key . '_' . $localeCode])) {
                        $create = true;
                        $requestDataKey = $key . '_' . $localeCode;
                        $data[$key] = $requestData[$requestDataKey];
                    }
                }
            }
            if ($create == true) {
                $dataObject::updateOrCreate($dataToFind, $data);
            }

        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        if (!isset($user->email)) {
            if ($user->is_organization == '0') {
                return redirect()->route('user.register_form_step_2_individual');
            } else {
                return redirect()->route('user.register_form_step_2_organization');
            }
        }
        $lang = LaravelLocalization::getCurrentLocale();
        $search = $request->search;
        $order = 20;
        if (isset($request->order_by)) {
            $order = $request->order_by;
        }
        $resources = Resource::when($search, function ($query) use ($search) {
            $query->whereHas('data', function ($query) use ($search) {
                return $query->where('name', 'LIKE', "%$search%");
            });
        });
        if (!Auth::user()->can('all_resource')) {
            $resources = $resources->where('user_id', Auth::id());
        }
        $resources = $resources->orderby('id', 'desc')->paginate($order);

        return view('profile.resource.index', compact('resources', 'search', 'order', 'lang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if (!isset($user->email)) {
            if ($user->is_organization == '0') {
                return redirect()->route('user.register_form_step_2_individual');
            } else {
                return redirect()->route('user.register_form_step_2_organization');
            }
        }

        $langs = LaravelLocalization::getSupportedLocales();
        $data = [];
        foreach ($langs as $localeCode => $properties) {
            $data[$localeCode] = new ResourceData();
        }
        $lang = LaravelLocalization::getCurrentLocale();
        $category = ResourceCategory::whereHas('data')->where('parent_id', null)->get();
        return view('profile.resource.create', compact('langs', 'data', 'lang', 'category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ResourceRequest $request)
    {

        $data = $request->all();
        $data['user_id'] = Auth::id();
//        if (Auth::user()->can('resources')) {
//            $data['status'] = 'pending';
//        }
        $resource = Resource::create($data);
        $this->updateData($request->all(), $resource, new ResourceData());

        return route('resources.index');
    }

    public function getSubcategory(Request $request)
    {
        $category = ResourceCategory::findorfail($request->category_id);
        $child = [];
        if (count($category->child)) {
            foreach ($category->child as $item) {
                $c = [];
                $c['id'] = $item->id;
                $c['name'] = $item->data->name;
                $child[] = $c;
            }
            $myJSON = json_encode($child);
            return $myJSON;
        }
        return 'none';


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Resource $resource
     * @return \Illuminate\Http\Response
     */
    public function show(Resource $resource)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Resource $resource
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        if (!isset($user->email)) {
            if ($user->is_organization == '0') {
                return redirect()->route('user.register_form_step_2_individual');
            } else {
                return redirect()->route('user.register_form_step_2_organization');
            }
        }
        $langs = LaravelLocalization::getSupportedLocales();
        $data = [];
        foreach ($langs as $localeCode => $properties) {
            $data[$localeCode] = ResourceData::where('resource_id', $id)->where('lang', $localeCode)->first();
        }
        $resource = Resource::findorfail($id);
        $lang = LaravelLocalization::getCurrentLocale();
        $category = ResourceCategory::whereHas('data')->where('parent_id', null)->get();
        return view('profile.resource.edit', compact('resource', 'langs', 'data', 'lang', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Resource $resource
     * @return \Illuminate\Http\Response
     */
    public function update(ResourceRequest $request, $id)
    {
        $review = Resource::findorfail($id);
        $data = $request->all();
        if (Auth::user()->can('resources')) {
            $data['status'] = 1;
        }

        $review->update($data);
        $review->edited_id = Auth::id();
        $review->save();

        $this->updateData($request->all(), $review, new ResourceData());
        return route('resources.index');
    }

    public function changeStatus(Request $request, $id)
    {
        $resource = Resource::findorfail($id);
        $resource->update($request->all());
        $resource->edited_id = Auth::id();
        $resource->save();
        return redirect()->route('resources.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Resource $resource
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Resource::destroy($id);
        ResourceData::where('resource_id', $id)->delete();
        return redirect()->route('resources.index');
    }
}
