<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\ApiService;
use App\Services\ProfileService;
use App\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ContactRequest;
use App\Role;

class AuthController extends Controller
{
  use SendsPasswordResetEmails, ResetsPasswords {
    SendsPasswordResetEmails::broker insteadof ResetsPasswords;
    ResetsPasswords::credentials insteadof SendsPasswordResetEmails;
  }

  /**
   * Register a new user
   */
  public function register(Request $request)
  {
    $v = Validator::make($request->all(), [
        'email' => 'required|email|max:255|unique:users,email,',
        'first_name' => 'required|max:255|alpha_dash',
        'last_name' => 'required|max:255|alpha_dash',
//        'residence_country_id' => 'required',
        'phone' => 'required',
//        'zip' => 'nullable|array',
//        'zip.*' => 'nullable|min:4|max:10',
        'password' => ['required', 'string', 'min:8', 'confirmed'],
        'username' => 'required|min:3|max:100|alpha_dash|unique:users,username,'
    ]);
    if ($v->fails()) {
      return response()->json([
          'status' => 'error',
          'errors' => $v->errors()
      ], 422);
    }
    $data = $request->all();
      $role = Role::where('name','user')->first();
      if ($request->is_organization == 1) {
          $role = Role::where('name','organization')->first();
      }

    $data['phone'] = null;
    if (isset($request->phone) && $request->phone) {
      $comb = array_combine([$request->phone], [$request->phone_type]);
      $b = [];
      foreach ($comb as $key => $value) {
        $b[] = array('phone' => $key, 'type' => $value);
      }

      $phone = json_encode($b);
      $data['phone'] = $phone;
    }
    $data["password"] = $data["password"] ? bcrypt($data["password"]) : "";
    $user = User::create($data);
    $user->attachRole($role);
    $credentials = request(['username', 'password']);
    if ($token = $this->guard()->attempt($credentials)) {
      return response()->json(['status' => 'success'], 200)->header('Authorization', $token);
    }
  }

  /**
   * Login user and return a token
   */
  public function login(Request $request)
  {
    $v = Validator::make($request->all(), [
        'password' => ['required', 'string',],
        'login' => 'required|max:100'
    ]);
    if ($v->fails()) {
      return response()->json([
          'status' => 'error',
          'errors' => $v->errors()
      ], 422);
    }

    $loginField = request()->input('login');
    $credentials = null;
    $loginType = filter_var($loginField, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
    request()->merge([$loginType => $loginField]);

    $credentials = request([$loginType, 'password']);
    if ($token = $this->guard()->attempt($credentials)) {
      return response()->json(['status' => 'success'], 200)->header('Authorization', $token);
    }
    return response()->json(['error' => ['login error']], 401);
  }

  /**
   * Logout User
   */
  public function logout()
  {
    $this->guard()->logout();
    return response()->json([
        'status' => 'success',
        'msg' => 'Logged out Successfully.'
    ], 200);
  }

  /**
   * Get authenticated user
   */
  public function user()
  {
    $user = User::where('id', Auth::id())->with('address')->get();

    if ($user->count() > 0) {
      return response()->json([
          'status' => 'success',
          'data' => $user
      ]);
    } else {
      throw new \Exception('User Not Found', 404);
    }

  }

  /**
   * Refresh JWT token
   */
  public function refresh()
  {
    if ($token = $this->guard()->refresh()) {
      return response()
          ->json(['status' => 'successs'], 200)
          ->header('Authorization', $token);
    }
    return response()->json(['error' => 'refresh_token_error'], 401);
  }

  /**
   * Return auth guard
   */
  private function guard()
  {
    return Auth::guard('api');
  }


  /**
   * Send password reset link.
   */
  public function sendPasswordResetLink(Request $request)
  {
    return $this->sendResetLinkEmail($request);
  }

  /**
   * Get the response for a successful password reset link.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  string $response
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
   */
  protected function sendResetLinkResponse(Request $request, $response)
  {
    return response()->json([
        'message' => 'Password reset email sent.',
        'data' => $response
    ]);
  }

  /**
   * Get the response for a failed password reset link.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  string $response
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
   */
  protected function sendResetLinkFailedResponse(Request $request, $response)
  {
    return response()->json(['message' => 'Email could not be sent to this email address.']);
  }

  /**
   * Handle reset password
   */
  public function callResetPassword(Request $request)
  {
    return $this->reset($request);
  }

  /**
   * Reset the given user's password.
   *
   * @param  \Illuminate\Contracts\Auth\CanResetPassword $user
   * @param  string $password
   * @return void
   */
//  protected function resetPassword($user, $password)
//  {
//    $user->password = Hash::make($password);
//    $user->save();
//    event(new PasswordReset($user));
//  }

  /**
   * Get the response for a successful password reset.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  string $response
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
   */
  protected function sendResetResponse(Request $request, $response)
  {
    return response()->json(['message' => 'Password reset successfully.']);
  }

  /**
   * Get the response for a failed password reset.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  string $response
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
   */
  protected function sendResetFailedResponse(Request $request, $response)
  {
    return response()->json(['message' => 'Failed, Invalid Token.']);
  }

  /**
   * @param Request $request
   * @param ApiService $apiService
   * @return \Illuminate\Http\JsonResponse
   */
  public function resetPasswordForApp(Request $request, ApiService $apiService)
  {
    $email = $request->email;
    $user = User::where('email', $email)->first();
    if ($user) {
      $reset_token = $apiService->generateUnique();
      $user->reset_token = $reset_token;
      $user->save();
      $user->notify(new \App\Notifications\MailResetPasswordNotification($reset_token, true));
    } else {
      return response()->json(['data' => 'Invalid email Address']);
    }
  }

  /**
   * @param Request $request
   * @param ProfileService $profileService
   * @return \Illuminate\Http\JsonResponse
   */
  public function resetPassword(Request $request, ProfileService $profileService)
  {
    $data = $request->all();
    $id = $request->user_id;
    $validator = Validator::make($request->all(), [
        'password' => ['required', 'string', 'min:8', 'confirmed'],
    ]);

    if ($validator->fails()) {

      return response()->json(array(
          'success' => false,
          'error' => $validator->messages()
      ), 422);
    }
    try {
      $user = User::find($id);
      $user->update(['password' => bcrypt($data["password"]), 'reset_token' => null]);
      return response()->json([
          'status' => 'success',
          'data' => 'Password is  changed successfully!',
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }

  /**
   * @param $token
   * @return \Illuminate\Http\JsonResponse
   */
  public function checkToken($token)
  {
    $user = User::where('reset_token', $token)->first();
    if ($user) {
      return response()->json(['success' => 'true', 'data' => ['user_id' => $user->id, 'message' => 'Token is verified']]);
    }
    return response()->json(['success' => 'false', 'data' => 'Invalid Token'], 422);
  }

  /**
   * @param ContactRequest $request
   * @return \Illuminate\Http\RedirectResponse
   */
  public function contactUs(ContactRequest $request)
  {
    $data = $request->all();
    $subject = 'CareerCenter - Contact Us';

    Mail::send('email.contact', ['data' => $data], function ($message) use ($data, $subject) {
      $message->to('sose.yeritsyan.97@gmail.com')->subject($subject);
    });
    return response()->json(['success' => 'true', 'data' => ['message' => 'Success']]);
  }
}