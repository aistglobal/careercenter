<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\ApplyJobRequest;
use App\Services\ApiService;
use App\Services\JobService;
use Illuminate\Http\Request;

class JobController extends Controller
{
    protected $jobService;

    public function __construct(JobService $jobService)
    {
        $this->jobService = $jobService;
    }
  /**
   * @param ApiService $apiService
   * @return \Illuminate\Http\JsonResponse
   */
  public function getAllCountries(ApiService $apiService)
  {
    try {
      $counties = $apiService->getAllCountries();
      return response()->json([
          'status' => 'success',
          'data' => $counties,
      ], 200);
    } catch (\Exception  $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }

  /**
   * @param ApiService $apiService
   * @return \Illuminate\Http\JsonResponse
   */
  public function getJobTypes(ApiService $apiService)
  {
    try {
      $types = $apiService->getJobTypes();
      return response()->json([
          'status' => 'success',
          'data' => $types,
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }

  /**
   * @param null $slug
   * @param null $status
   * @param Request $request
   * @param JobService $jobService
   * @return \Illuminate\Http\JsonResponse
   * @throws \Exception
   */
  public function getAll($slug = null, $status = null, Request $request, JobService $jobService)
  {
    $data = $request->all();
    $offset = $request->offset ?? 0;
    $is_ajax = $request->ajax() ?? false;
    $jobs = $jobService->getAll($slug, $data, $offset, $is_ajax);
    if (isset($jobs['error']) && $jobs['error'] == 404) {
      throw new \Exception('Job Not Found');
    }
    try {
      return response()->json([
          'status' => 'success',
          'data' => $jobs,
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }

  /**
   * @param $slug
   * @param JobService $jobService
   * @return \Illuminate\Http\JsonResponse
   */
  public function getBySlug($slug, JobService $jobService)
  {
    try {
      $job = $jobService->getBySlug($slug);
      return response()->json([
          'status' => 'success',
          'data' => $job,
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }

  /**
   * @param ApplyJobRequest $request
   * @param JobService $jobService
   * @return \Illuminate\Http\JsonResponse
   */
  public function apply(ApplyJobRequest $request, JobService $jobService)
  {
    $data = $request->all();
    try {
      $jobService->applyJob($data);
      return response()->json([
          'status' => 'success',
          'data' => 'success',
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }

  /**
   * @param Request $request
   * @param JobService $jobService
   * @return \Illuminate\Http\JsonResponse
   */
  public function addBookmark(Request $request, JobService $jobService)
  {
    $job_id = $request->job_id;
    try {
      $bookmark = $jobService->addBookmark($job_id);
      return response()->json([
          'status' => 'success',
          'data' => $bookmark,
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }

  /**
   * @param Request $request
   * @return string
   */
  public function removeBookmark(Request $request, JobService $jobService)
  {
    $job_id = $request->job_id;
    try {
      $bookmark = $jobService->removeBookmark($job_id);
      return response()->json([
          'status' => 'success',
          'data' => $bookmark,
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }

  public function getCategories(ApiService $apiService)
  {
    $data = $apiService->getCategories();
    try {
      return response()->json([
          'status' => 'success',
          'data' => $data,
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }

  public function getUserBookmarks(ApiService $apiService)
  {
    $data = $apiService->getUserBookmarks();
    try {
      return response()->json([
          'status' => 'success',
          'data' => $data,
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }

  public function getHistory(Request $request, JobService $jobService)
  {
    $data = $jobService->getHistory($request->offset);
    try {
      return response()->json([
          'status' => 'success',
          'data' => $data,
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }

  public function getApplicants()
  {
      try {
          $response = $this->jobService->getApplicants();
          return response()->json([
              'status' => 'success',
              'data' => $response,
          ], 200);
      } catch (\Exception $exception) {
          return response()->json([
              'status' => 'error',
              'errors' => $exception->getMessage()
          ], 422);
      }

  }
}