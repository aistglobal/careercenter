<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Services\BlogService;

class BlogController extends Controller
{
  /***
   * @var BlogService
   */
  protected $blogService;

  /**
   * BlogController constructor.
   * @param BlogService $blogService
   */
  public function __construct(BlogService $blogService)
  {
    $this->blogService = $blogService;
  }

  /**
   * @param $slug
   * @return \Illuminate\Http\JsonResponse
   */
  public function getBySlug($slug)
  {
    try {
      $blog = $this->blogService->getBySlug($slug);
      return response()->json([
          'status' => 'success',
          'data' => $blog,
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }

  public function getAll()
  {
    try {
      $blogs = $this->blogService->getAll();
      return response()->json([
          'status' => 'success',
          'data' => $blogs,
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }
}