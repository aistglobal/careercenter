<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\PasswordApiRequest;
use App\Http\Requests\Api\UserDetailsApiRequest;
use App\Services\ProfileService;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
  protected $profileService;

  public function __construct(ProfileService $profileService)
  {
    $this->profileService = $profileService;
  }

  /**
   * @param Request $request
   * @param ProfileService $profileService
   * @return \Illuminate\Http\JsonResponse
   */
  public function profileImage(Request $request)
  {
    $data = $request->all();
    try {
      $image_url = $this->profileService->addProfileImage($data);
      return response()->json([
          'status' => 'success',
          'data' => $image_url,
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }

  /**
   * @param UserDetailsApiRequest $request
   * @param $id
   * @param ProfileService $profileService
   * @return \Illuminate\Http\JsonResponse
   */
  public function update(UserDetailsApiRequest $request, $id)
  {

    try {
      $user = $this->profileService->update($request, $id);
      return response()->json([
          'status' => 'success',
          'data' => $user,
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }

  public function changePassword(PasswordApiRequest $request, $id)
  {
    $data = $request->all();
    try {
      $message = $this->profileService->changePassword($data, $id);
      return response()->json([
          'status' => 'success',
          'data' => $message,
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }

  public function setPushNotificationToken(Request $request)
  {
    $data = $request->all();
    try {
      $response = $this->profileService->setPushNotificationToken($data);
      return response()->json([
          'status' => 'success',
          'data' => $response,
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }

  public function removePushNotificationToken(Request $request)
  {
    $data = $request->all();
    try {
      $response = $this->profileService->removePushNotificationToken($data);
      return response()->json([
          'status' => 'success',
          'data' => $response,
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }

  public function getUserNotifications($user_id)
  {
    try {
      $response = $this->profileService->getUserNotifications($user_id);
      return response()->json([
          'status' => 'success',
          'data' => $response,
      ], 200);
    } catch (\Exception $exception) {
      return response()->json([
          'status' => 'error',
          'errors' => $exception->getMessage()
      ], 422);
    }
  }
}