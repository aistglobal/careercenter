<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Services\ImageService;
use Illuminate\Http\Request;

class CkeditorController extends Controller
{
    public function uploadImage(Request $request)
    {
        $funcNum = $request['CKEditorFuncNum'];
        $message = 'File is Uploaded';
        $path = ImageService::storeUpload($request->file('upload'), 'public');
        $url = env('APP_URL').'/storage/' . $path;
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
    }
}
