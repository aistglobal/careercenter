<?php

namespace App\Http\Controllers;

use App\ApplyJob;
use App\Job;
use App\JobData;
use App\Notifications\AcceptJobNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AppliersController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $search = $request->search;
    $order = $request->get('order', 20);
    $jobs = Job::whereHas('data')->when($search, function ($query) use ($search) {

      $query->whereHas('data', function ($query) use ($search) {
        return $query->where('title', 'LIKE', "%$search%");
      });

    })->where('user_id', Auth::id())->orderby('id', 'DESC')->where('status','!=','preview')->paginate($order);
    return view('profile.appliers.index', compact('jobs', 'search', 'order'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $job = Job::find($id);
    $appliers = $job->appliers()->orderby('id', 'desc')->paginate(20);
    return view('profile.appliers.show-appliers', compact('appliers', 'job'));
  }


  public function showDetails($id)
  {
    $applier = ApplyJob::find($id);
    return view('profile.appliers.show', compact('applier'));

  }

  public function changeStatus($id, Request $request)
  {
    $applier = ApplyJob::findorfail($id);
    $applier->status = $request->status;
    $applier->save();
    $user = User::find($applier->user_id);
    $job = JobData::where('job_id', $applier->job_id)->get(['organization', 'title'])[0];
    $message = 'Your application for' . ' ' . $job->title . ' ' . 'is' . ' ' . $applier->status . '.';
    $push_notification['message'] = $job->organization . ' ' . $applier->status . ' ' . 'your application for' . ' ' . $job->title;
    $push_notification['title'] = ucwords($applier->status);
    $user_logo = $user->image;
    $data['applier'] = $applier;
    $data['job'] = $job;
    $data['message'] = $message;
    $data['user_logo'] = $user_logo;
    $user->notify(new AcceptJobNotification($data, $user, $push_notification));
    return redirect()->route('appliers.show', $applier->job_id);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }
}
