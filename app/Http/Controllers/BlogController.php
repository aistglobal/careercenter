<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogData;
use App\Http\Requests\BlogRequest;
use App\Services\ImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class BlogController extends Controller
{
    protected $imageService;


    /**
     * BlogController constructor.
     * @param ImageService $imageService
     */
    public function __construct(ImageService $imageService)
    {

        $this->imageService = $imageService;
    }

    public function updateData($requestData, $object, $dataObject)
    {
        $fillableArray = array_flip($dataObject->getFillable());
        $langs = LaravelLocalization::getSupportedLocales();
        foreach ($langs as $localeCode => $properties) {
            $create = false;
            $dataToFind = [];
            $data = [];

            foreach ($fillableArray as $key => $value) {
                if ($key == $dataObject->foreignKey) {
                    $dataToFind[$key] = $object->id;
                } elseif ($key == 'lang') {
                    $dataToFind[$key] = $localeCode;
                } else {
                    if (isset($requestData[$key . '_' . $localeCode])) {
                        $create = true;
                        $requestDataKey = $key . '_' . $localeCode;
                        $data[$key] = $requestData[$requestDataKey];
                    }
                }
            }
            if ($create == true) {
                $dataObject::updateOrCreate($dataToFind, $data);
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $search = $request->search;
        $order = 20;
        if (isset($request->order_by)) {
            $order = $request->order_by;
        }

        $blogs = Blog::when($search, function ($query) use ($search) {

            $query->whereHas('data', function ($query) use ($search) {
                return $query->where('title', 'LIKE', "%$search%");
            });

        });
        if (!Auth::user()->can('all_article')) {
            $blogs = $blogs->where('user_id', Auth::id());
        }
        $blogs = $blogs->orderby('id', 'desc')->paginate($order);


        return view('profile.blog.index', compact('blogs', 'search', 'order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $langs = LaravelLocalization::getSupportedLocales();
        $data = [];
        foreach ($langs as $localeCode => $properties) {
            $data[$localeCode] = new BlogData();
        }
        return view('profile.blog.create', compact('langs', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        $blog = Blog::create(['user_id' => Auth::id()]);
        $slug = $blog->slugify($request->title_en);
        $blog->slug = $slug;
        $blog->save();
        $this->imageService->savePhoto($request->image, 'public', $blog, $key = 'image');
        $this->updateData($request->all(), $blog, new BlogData());

        return redirect()->route('blogs.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $langs = LaravelLocalization::getSupportedLocales();
        $data = [];
        foreach ($langs as $localeCode => $properties) {
            $data[$localeCode] = BlogData::where('blog_id', $id)->where('lang', $localeCode)->first();
        }
        $blog = Blog::findorfail($id);

        return view('profile.blog.edit', compact('blog', 'langs', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function update(BlogRequest $request, $id)
    {
        $blog = Blog::findorfail($id);
        $slug = $blog->slugify($request->title_en);
        $blog->slug = $slug;
        $blog->edited_id = Auth::id();
        $blog->save();
        if ($request->hasFile('image')) {
            $this->imageService->savePhoto($request->image, 'public', $blog, $key = 'image');
        }

        $this->updateData($request->all(), $blog, new BlogData());
        return redirect()->route('blog-single', $blog->slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Blog::destroy($id);
        BlogData::where('blog_id', $id)->delete();
        return redirect()->route('blogs.index');

    }
    public function changeStatus(Request $request, $id)
    {
        $blog = Blog::findorfail($id);
        $blog->update($request->all());
        return redirect()->route('blogs.index');
    }
}
