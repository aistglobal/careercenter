<?php

namespace App\Http\Controllers;

use App\Subscribtion;
use Illuminate\Http\Request;

class SubscribeController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;
        $order = 20;
        if (isset($request->order_by)) {
            $order = $request->order_by;
        }

        $subscribtions = Subscribtion::when($search, function ($query) use ($search) {
            return $query->where('email', 'LIKE', "%$search%");
        })->paginate($order);

        return view('profile.subscribtion.index', compact('subscribtions', 'search', 'order'));
    }
}
