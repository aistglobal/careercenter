<?php

namespace App\Http\Controllers;

use App\Http\Requests\ResourceCategoryRequest;
use App\ResourceCategory;
use App\ResourceCategoryData;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ResourceCategoryController extends Controller
{
    public function updateData($requestData, $object, $dataObject)
    {
        $fillableArray = array_flip($dataObject->getFillable());
        $langs = LaravelLocalization::getSupportedLocales();
        foreach ($langs as $localeCode => $properties) {
            $dataToFind = [];
            $data = [];

            foreach ($fillableArray as $key => $value) {
                if ($key == $dataObject->foreignKey) {
                    $dataToFind[$key] = $object->id;
                } elseif ($key == 'lang') {
                    $dataToFind[$key] = $localeCode;
                } else {
                    if (isset($requestData[$key . '_' . $localeCode])) {
                        $requestDataKey = $key . '_' . $localeCode;
                        $data[$key] = $requestData[$requestDataKey];
                    }
                }
            }
            $dataObject::updateOrCreate($dataToFind, $data);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $order = 20;
        if (isset($request->order_by)) {
            $order = $request->order_by;
        }
        $resources = ResourceCategory::when($search, function ($query) use ($search) {

            $query->whereHas('data', function ($query) use ($search) {
                return $query->where('name', 'LIKE', "%$search%");
            });

        })->where('parent_id', null)->paginate(20);

        return view('profile.resource-category.index', compact('resources', 'search', 'order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $langs = LaravelLocalization::getSupportedLocales();
        $resources = ResourceCategory::whereHas('data')->where('parent_id', null)->get();
        $data = [];
        foreach ($langs as $localeCode => $properties) {
            $data[$localeCode] = new ResourceCategoryData();
        }
        return view('profile.resource-category.create', compact('langs', 'data', 'resources'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ResourceCategoryRequest $request)
    {
        $resource = ResourceCategory::create($request->all());
        $slug = $resource->slugify($request->name_en);
        $resource->slug = $slug;
        $resource->save();
        $this->updateData($request->all(), $resource, new ResourceCategoryData());
        return redirect()->route('resource_category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $langs = LaravelLocalization::getSupportedLocales();
        $resources = ResourceCategory::whereHas('data')->where('parent_id', null)->get();
        $data = [];
        foreach ($langs as $localeCode => $properties) {
            $data[$localeCode] = ResourceCategoryData::where('resource_id', $id)->where('lang', $localeCode)->first();
        }
        $resource = ResourceCategory::findorfail($id);

        return view('profile.resource-category.edit', compact('resource', 'langs', 'data', 'resources'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ResourceCategoryRequest $request, $id)
    {
        $resource = ResourceCategory::find($id);
        $resource->update($request->all());

        $resource->save();
        $this->updateData($request->all(), $resource, new ResourceCategoryData());
        return redirect()->route('resource_category.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $resource = ResourceCategory::findorfail($id);
        foreach ($resource->child as $item)
        {
            ResourceCategory::destroy($item->id);
            ResourceCategoryData::where('resource_id', $item->id)->delete();
        }
        ResourceCategory::destroy($id);
        ResourceCategoryData::where('resource_id', $id)->delete();

        return redirect()->route('resource_category.index');

    }
}
