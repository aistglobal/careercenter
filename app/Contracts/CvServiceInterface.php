<?php

namespace App\Contracts;

interface CvServiceInterface
{
  /**
   * @param $id
   * @return mixed
   */
  public function getById($id);

  /**
   * @return mixed
   */
  public function getAll();

  /**
   * @param $user_id
   * @return mixed
   */
  public function getAllByUserId($user_id);
}