<?php
namespace App\Contracts;

interface JobServiceInterface
{
  /**
   * @param $slug
   * @param $data
   * @param int $offset
   * @param bool $is_ajax
   * @return array
   */
  public function getAll($slug,$data,int $offset,bool $is_ajax):array;

    /**
     * @param $slug
     * @param $status
     * @return array
     */
  public function getBySlug($slug,$status):array;

  /**
   * @param $data
   * @return mixed
   */
  public function apply($data);
}