<?php

namespace App\Contracts;

interface BlogServiceInterface{
  /**
   * @param $slug
   * @return mixed
   */
public function getBySlug($slug);

  /**
   * @return mixed
   */
public function getAll();
}