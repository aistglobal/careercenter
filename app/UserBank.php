<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBank extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_banks';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    protected $fillable = ['user_id','address','bank','tax','account','customer','legal_address','physical_address',
        'signing_person_1','position_1','signing_person_2','position_2','passport','individual'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
