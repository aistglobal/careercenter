<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'organizations';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'user_id';
    protected $fillable = ['user_id', 'name', 'short_name', 'legal_structure', 'type', 'field_of_activities', 'number_of_employees', 'about_company','position','slug'];
    public function user()
    {
        return $this->hasMany(User::class);
    }
}
