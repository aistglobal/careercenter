<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CvContact extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cv_contacts';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'cv_id';
    protected $fillable = [
        'cv_id',
        'first_name',
        'last_name',
        'middle_name',
        'email',
        'title',
        'website',
        'phone',
        'residence_country_id',
    ];

}
