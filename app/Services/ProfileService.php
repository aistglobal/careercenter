<?php

namespace App\Services;


use App\Contracts\ProfileServiceInterface;
use App\Core\Services\BaseService;
use App\User;
use App\UserDevice;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProfileService extends BaseService implements ProfileServiceInterface
{
  /**
   * @param $data
   * @return mixed
   */
  public function addProfileImage($data)
  {
    $user = Auth::user();
    if (isset($data['image']) && $data['image'] != 'null') {
      $imageFileName = time() . rand(1, 999999999) . '.png';
      $dataImage = $data['image'];
      list($type, $dataImage) = explode(';', $dataImage);
      list(, $dataImage) = explode(',', $dataImage);
      $dataImage = base64_decode($dataImage);
      $s3 = Storage::disk('public');
      $filePath = 'logos/' . $imageFileName;
      $s3->put($filePath, $dataImage, 'public');
      $user->image = 'logos/' . $imageFileName;
      $user->save();
      return $user->image;
    }
  }


  /**
   * @param $request
   * @param $id
   * @return mixed
   */
  public function update($request, $id)
  {
    $user = User::find($id);
    $user->address()->delete();
    if (isset($request->address)) {
      $count = count($request->address);
      for ($i = 0; $i < $count; $i++) {
        $user->address()->create([
            'address' => $request->address[$i],
            'zip' => $request->zip[$i],
            'country_id' => $request->country[$i],
            'city' => $request->city[$i],
            'address_type' => $request->address_type[$i]

        ]);
      }
    }
    $data = $request->all();
    $data['phone'] = null;

    if (count($request->phone) != 0) {
      $comb = array_combine($request->phone, $request->phone_type);

      $b = [];
      foreach ($comb as $key => $value) {
        $b[] = array('phone' => $key, 'type' => $value);
      }

      $phone = json_encode($b);
      $data['phone'] = $phone;
    }

    $user->update($data);
    $data['user_id'] = $user->id;
    if ($user->is_organization == 1) {
      $organization = $user->organization();
      if (isset($request->about_company)) {
        $data['about_company'] = str_replace("\r\n", "<br />", $request->about_company);
      }
        if(isset($request->legal_structure) && $request->legal_structure == 'other')
        {
            if(isset($request->legal_other))
            {
                $data['legal_structure'] =  $data['legal_other'];
            }
        }
      $organization->update($data);
      if ($request->hasFile('image')) {
        $this->imageService->savePhoto($request['image'], 'public', $user, $key = 'image');
      }
    }
    return $user;
  }

  /**
   * @param $data
   * @param $id
   * @return string
   */
  public function changePassword($data, $id)
  {
    $user = User::find($id);
    $user->update(['password' => bcrypt($data["password"])]);
    return 'Password is  changed successfully!';
  }

  public function setPushNotificationToken($data)
  {
    return UserDevice::updateOrCreate($data, ['user_id' => Auth::id(), 'device_type' => $data['device_type']]);
  }

  public function removePushNotificationToken($data)
  {
    return UserDevice::where(['user_id' => Auth::id(), 'device_type' => $data['device_type']])->delete();
  }

  public function getUserNotifications($user_id)
  {
    $user = User::find($user_id);
    $notifications = $user->notifications;
    return $notifications;
  }
}