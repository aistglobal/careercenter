<?php
namespace App\Services;
use Edujugon\PushNotification\PushNotification;
use Log;
class PushNotificationService
{
  /**
   * @param $title
   * @param $message
   * @param $data
   * @param array $deviceTokens
   * @param int $badge
   */
  public function sendNotifications($title, $message, $data, $deviceTokens = [], $badge = 0): void
  {
    //   Log::error($data);
    $push = new PushNotification();
    $push->setConfig([
        'priority' => 'normal',
        'dry_run' => true,
        'time_to_live' => 3
    ]);
    $push->setService('fcm')->setMessage([
        'data' => [
            'message' => $message,
            'sound' => 'default',
            'title' => $title,
//                'data' => $data,
            'extraPayLoad' => [
                'data' => $data,
            ]
        ],
    ])->setDevicesToken($deviceTokens)->send();
    $push->setService('apn')->setMessage([
        'aps' => [
            'alert' => [
                'title' => $title,
                'body' => $message,
            ],
            'sound' => 'default',
//                'data' => $data,
            'badge' => $badge
        ],
        'extraPayLoad' => [
            'data' => $data,
        ],
        'foreground' => false,
    ])->setDevicesToken($deviceTokens)->send();
  }
}
