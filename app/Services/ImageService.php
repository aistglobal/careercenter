<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ImageService
{

  /**
   * Save photo to Storage server
   * @param $file
   * @param $disc
   * @param $object
   * @param string $key
   * @return string
   */
    public static function  storeUpload($file, $disc)
    {
        if ($file && $file != '') {

            $imageFileName = time() . rand(1, 999999999) . '.' . $file->getClientOriginalExtension();

            $storage = Storage::disk($disc);
            $storage->put($imageFileName, file_get_contents($file), 'public');
            return $imageFileName;

        }
    }



  public function savePhoto($file, $disc, $object, $key = 'image')
  {

    if ($file && $file != '') {

      $imageFileName = time() . rand(1, 999999999) . '.' . $file->getClientOriginalExtension();

      $storage = Storage::disk($disc);
      $storage->put($imageFileName, file_get_contents($file), 'public');

      $object->$key = $imageFileName;
      $object->save();
    }
  }

  public function saveResume($file, $disc, $object, $key = 'resume')
  {

    if ($file && $file != '') {

      $imageFileName = time() . rand(1, 999999999) . '.' . $file->getClientOriginalExtension();

      $storage = Storage::disk($disc);

      $storage->put($imageFileName, file_get_contents($file), 'public');

      $object->$key = $imageFileName;
      $object->save();
    }
  }

  public function saveAttachments($job_id, $file, $disc, $object, $key = 'image')
  {
    if ($file && $file != '') {

      $imageFileName = 'attachments/' . $job_id . '/' . $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();

      $storage = Storage::disk($disc);
      $storage->put($imageFileName, file_get_contents($file), 'public');

      $object->$key = $imageFileName;
      $object->save();

    }

  }

  public function saveProfilePhoto($file, $disc, $object, $key = 'image')
  {
    if ($file && $file != '') {

      $imageFileName = '/logos/' . time() . rand(1, 999999999) . '.' . $file->getClientOriginalExtension();

      $storage = Storage::disk($disc);
      $storage->put($imageFileName, file_get_contents($file), 'public');

      $object->$key = $imageFileName;
      $object->save();


    }

  }

  public function savePDF($file, $disc, $object, $key = 'pdf')
  {
    if ($file && $file != '') {

      if (file_exists(storage_path() . '/' . $file->getClientOriginalName())) {

        abort(404);
      }

      $imageFileName = $file->getClientOriginalName();

      $storage = Storage::disk($disc);
      $storage->put($imageFileName, file_get_contents($file), 'public');

      $object->$key = $imageFileName;
      $object->save();


    }
  }

  public static function thumbGenerate($input, $name, $widths, $repo = null)
  {
    foreach ($widths as $width) {

      $img = Image::make($input)->resize($width, null, function ($constraint) {
        $constraint->aspectRatio();
      })->response();
      $localStorage = Storage::disk($repo);
      $filePath = "/thumb/$width/" . $name;

      $localStorage->put($filePath, $img->getContent(), 'public');
    }
  }
  /**
   *
   * /**
   * Save photo to Storage server
   * @param $file
   * @param $disc
   * @param $imageFileName
   * @return string
   */

}