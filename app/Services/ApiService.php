<?php

namespace App\Services;


use App\Bookmark;
use App\Category;
use App\Contracts\ApiServiceInterface;
use App\Core\Services\BaseService;
use App\Country;
use App\Type;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ApiService extends BaseService implements ApiServiceInterface
{

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllCountries()
    {
        return Country::all();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getJobTypes()
    {
        $types = Type::all();
        $types->transform(function ($item) {
            return [$item->id => trans('types.' . $item->key)];
        });
        return $types;
    }

    public function getCategories()
    {
        return Category::where('category_id', null)->with('child')->get();

    }

    public function getUserBookmarks()
    {
        $user_id = Auth::id();
        $bookmarks = Bookmark::where('user_id', $user_id)->with(['user:id,image', 'jobs.data','jobs.user'])->get();
        return $bookmarks;
    }


    /**
     * @return string
     */
    public function generateUnique(): string
    {
        $reset_token = Str::random(6);
        $count = User::where('reset_token', $reset_token)->count();
        if ($count > 0) {
            $this->generateUnique();
        } else {
            return $reset_token;
        }
    }
}