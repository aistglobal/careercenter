<?php

namespace App\Services;

use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\SEOTools;
use Artesaos\SEOTools\Facades\TwitterCard;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;


class SeoService
{
    /**
     * @param $object
     */
    public function setMetaData($object, $case)
    {

        if(isset($object))
        {
            if ($case == 'blog' || $case=='page')
            {
                SEOMeta::addKeyword($object->data->meta_keywords);
                SEOTools::setTitle($object->data->meta_title);
                SEOTools::setDescription($object->data->meta_description);
                SEOTools::addImages(URL::to('').Storage::disk('')->url($object->image));

            }

            else
            {
                if(isset($object->data))
                {
                    SEOTools::setDescription(strip_tags($object->data->description));
                    SEOMeta::addKeyword($object->data->title);
                    SEOTools::setTitle($object->data->title);
                }
                if(isset($object->user))
                {
                    SEOTools::addImages(URL::to('/').'/storage/'.$object->user->image);
                }

            }
        }


    }

    public function setPageMeta($page)
    {

        switch ($page) {
            case 'resources':
                SEOTools::setTitle('Resources');
                SEOTools::setDescription(' Resoruces Description');
                SEOMeta::addKeyword('careercenter');
                break;
            case 'articles':
                SEOTools::setTitle('Articles');
                SEOTools::setDescription(' Articles');
                SEOMeta::addKeyword('careercenter');
                break;
            case 'resume':
                SEOTools::setTitle('Resume');
                SEOTools::setDescription(' Resume');
                SEOMeta::addKeyword('careercenter');
                break;
            case 'announcements':
                SEOTools::setTitle('Announcements');
                SEOTools::setDescription('Announcements');
                SEOMeta::addKeyword('CareerCenter');
                break;
            case 'login':
                SEOTools::setTitle('Login CareerCenter');
                SEOTools::setDescription('Login CareerCenter');
                SEOMeta::addKeyword('careerCenter');
                break;
            case 'register':
                SEOTools::setTitle('Register CareerCenter');
                SEOTools::setDescription('Register CareerCenter');
                SEOMeta::addKeyword('careerCenter');
                break;
            case 'register_step_2':
                SEOTools::setTitle('Register  Step 2 CareerCenter');
                SEOTools::setDescription('Register CareerCenter');
                SEOMeta::addKeyword('careerCenter');
                break;
            case 'contact-us':
                SEOTools::setTitle('Contact Us');
                SEOTools::setDescription('Contact Us CareerCenter');
                SEOMeta::addKeyword('careerCenter');
                break;
            case 'about-us':
                SEOTools::setTitle('About Us');
                SEOTools::setDescription('About CareerCenter');
                SEOMeta::addKeyword('careerCenter');
                break;
            case 'testimonial':
                SEOTools::setTitle('Testimonial');
                SEOTools::setDescription('Testimonial CareerCenter');
                SEOMeta::addKeyword('careerCenter');
                break;

        }
    }


}