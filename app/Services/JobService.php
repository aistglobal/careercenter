<?php

namespace App\Services;


use App\Category;
use App\Contracts\JobServiceInterface;
use App\Core\Services\BaseService;
use App\Country;
use App\Cv;
use App\Job;
use App\Type;
use App\ApplyJob;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Bookmark;
use Illuminate\Support\Facades\Storage;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class JobService extends BaseService implements JobServiceInterface
{
    /**
     * @var SeoService
     */
    protected $seoService;
    /**
     * @var ImageService
     */
    protected $imageService;

    /**
     * JobService constructor.
     * @param SeoService $seoService
     * @param ImageService $imageService
     */
    public function __construct(SeoService $seoService, ImageService $imageService)
    {
        $this->seoService = $seoService;
        $this->imageService = $imageService;
    }

    /**
     * @param $slug
     * @param $data
     * @param int $offset
     * @param bool $is_ajax
     * @return array
     */
    public function getAll($slug,$data, int $offset, bool $is_ajax): array
    {
        $view = 'classic';
        $response_data = [];
        $status = $data["status"] ?? 'all';
        $buttons = false;
        $types = Type::all();
        $type = $data["type"] ?? null;
        $category_id = $data["category_id"] ?? null;
        $sub_category_id = $data["sub_category_id"] ?? null;
        $limit = 15;
        $all_categories = Category::where('category_id', null)->get();
        $all_sub_categories = null;
        $mirror = null;
        $sort_by = 'jobs.sort_by';
        $applied_job_status = '';
        $applied_job_date = '';
        if(isset($data['view']))
        {
            $view = $data["view"];
        }
        if($view == 'list')
        {
            $limit = 30;
        }
        if ($status == 'expired') {
            $sort_by = 'jobs.expiration_date';
        }

        if (isset($category_id)) {
            $all_sub_categories = Category::where('category_id', $category_id)->get();
        }
        $title = $data["search"] ?? '';

        $result = DB::table('jobs')->where('jobs.slug', '!=', null)->leftJoin('users', 'jobs.user_id', '=', 'users.id')->join('organizations', 'users.organization_id', '=', 'organizations.id');
        if ($category_id || $sub_category_id) {
            $result->join('job_categories', 'jobs.id', '=', 'job_categories.job_id');
        }
        if ($status == 'applied_job') {
            $result->join('apply_jobs', 'jobs.id', '=', 'apply_jobs.job_id')->where('apply_jobs.user_id', Auth::id())->where('jobs.status', '=', 'posted');
        }
        if ($type) {
            $result->where('type_id', $type);
        }
        if ($category_id && !$sub_category_id) {
            $result->where('category_id', $category_id);
        } elseif ($sub_category_id) {
            $result->where('category_id', $sub_category_id);
        }

        if ((Auth::check() && Auth::user()->is_organization == '1') || (Auth::check() && Auth::user()->can('accessAll_announcement'))) {
            $buttons = true;
            switch ($status) {
                case 'all':
                    $result->where('jobs.status', '=', 'posted');
                    break;
                case 'posted':
                    $result->where('jobs.status', '=', 'posted');
                    break;
                case 'expired':
                    $result->where(function ($q) use ($data) {
                        if (isset($data['expired_date'])) {
                            $q->whereYear('jobs.expiration_date', '=', Carbon::parse($data['expired_date'])->format('Y'))
                                ->whereMonth('jobs.expiration_date', '=', Carbon::parse($data['expired_date'])->format('m'));
                        }
                        return $q->where('jobs.status', '=', 'expired');
                    });
                    break;
                case 'draft':
                    $result->where('jobs.status', '=', 'draft');
                    break;
                case 'pending':
                    $result->where('jobs.status', '=', 'pending');
                    break;
                case 'rejected':
                    $result->where(function ($q) use ($data) {
                        if (isset($data['rejected_date'])) {
                            $q->whereYear('jobs.updated_at', '=', Carbon::parse($data['rejected_date'])->format('Y'))
                                ->whereMonth('jobs.updated_at', '=', Carbon::parse($data['rejected_date'])->format('m'));
                        }
                        return $q->where('jobs.status', '=', 'rejected');
                    });
                    break;
                case 'my-draft':
                    $result = $result->where('organizations.id', Auth::user()->organization_id)->where('jobs.status', '=', 'draft');
                    break;
                case 'my-expired':
                    $result->where('organizations.id', Auth::user()->organization_id)->where(function ($q) use ($data) {
                        if (isset($data['expired_date'])) {
                            $q->whereYear('jobs.expiration_date', '=', Carbon::parse($data['expired_date'])->format('Y'))
                                ->whereMonth('jobs.expiration_date', '=', Carbon::parse($data['expired_date'])->format('m'));
                        }
                        return $q->where('jobs.status', '=', 'expired');
                    });
                    break;
                case 'my-pending':
                    $result = $result->where('organizations.id', Auth::user()->organization_id)->where('jobs.status', '=', 'pending');
                    break;
                case 'my-rejected':
                    $result->where('organizations.id', Auth::user()->organization_id)->where('jobs.status', '=', 'rejected');
                    break;
                case 'my-posted':
                    $result->where('organizations.id', Auth::user()->organization_id)->where('jobs.status', '=', 'posted');
                    break;
            }

        } else {
            $data['status'] = 'all';
            $status = 'all';
            $result->where('jobs.status', '=', 'posted');
        }
        if ($title) {
            $result->having('title', 'LIKE', "%$title%")->orHaving('organization', 'LIKE', "%$title%");
        }
        if ($slug) {
            $result->orWhere('jobs.slug', '=', $slug);
        }

        $result->join('job_data', 'jobs.id', '=', 'job_data.job_id')
            ->select('jobs.id as jobId', 'jobs.slug', 'jobs.expiration_date', 'jobs.updated_at', 'job_data.opening_date',
                'job_data.title', 'job_data.menu', 'job_data.organization', 'job_data.description',
                DB::raw('group_concat(Distinct users.image) as logo'));

        if ($status == 'applied_job') {
            $sort_by = 'apply_jobs.created_at';
            $result->groupBy('jobs.id', 'jobs.slug', 'jobs.updated_at', 'jobs.sort_by', 'job_data.opening_date', 'job_data.title','job_data.menu', 'job_data.organization', 'job_data.description', 'jobs.expiration_date')
                ->orderby($sort_by, 'DESC');
        } else {
            $result->groupBy('jobs.id', 'jobs.slug', 'jobs.updated_at', 'jobs.sort_by', 'job_data.opening_date', 'job_data.title','job_data.menu', 'job_data.organization', 'job_data.description', 'jobs.expiration_date')
                ->orderby($sort_by, 'DESC');
        }

        if ($slug) {
            if (!$is_ajax) {
                $job_id = Job::where('slug', $slug)->first() ? Job::where('slug', $slug)->first()->sort_by : "";
                $limit = $job_id ? $result->where('jobs.sort_by', '>=', $job_id)->get()->count() : $limit;
            }
        }

        $jobs = $result->skip($offset)->take($limit)->get();


//        $offset = $limit;
        if (isset($slug)) {
            $first = Job::with('data')->where('slug', $slug)->first();

            $response_data["first"] = $first;
        }
        elseif ($jobs->count() > 0) {
            if($status != 'all'&& $status!='my-posted')
            {
                $first = Job::with('data')->find($jobs[0]->jobId);
                $response_data["first"] = $first;
            }

        }
        if (isset($first) && $first->status != "posted" && !(((Auth::check() && Auth::user()->is_organization == '1') || (Auth::check() && Auth::user()->can('accessAll_announcement'))))) {
            $response_data['error'] = '404';
            return $response_data;
        }
//        $category = $data["category"] ?? null;
        $phone = null;
        if (isset($first) && $first) {
            $this->seoService->setMetaData($first, 'job');
            $country = Country::find($first->location_country_id);
            $response_data["country"] = $country;
            if (isset($first->phone)) {
                $phone = $first->phone;
                $response_data['phone'] = $phone;
            }
            if (isset($first->mirror_id)) {
                $mirror = $first->mirror;
            }
            $updated_at = $first->updated_at;
            if ($first->status == 'posted') {
                $first->views += 1;
                $first->save();
                $first->updated_at = $updated_at;
                $first->save();
            }


            $applied_job = ApplyJob::where('user_id', Auth::id())->where('job_id', $first->id)->latest()->first();
        } else {
            $this->seoService->setPageMeta('announcements');
        }
        $index_of_question = strpos($_SERVER['REQUEST_URI'], '?');
        if ($index_of_question) {
            $url = substr($_SERVER['REQUEST_URI'], $index_of_question);
        } else {
            $url = '';
        }
        $resumes = [];
        if (Auth::check()) {
            $resumes = Auth::user()->cv;
        }
        if (isset($applied_job) && isset($applied_job->status)) {
            $applied_job_status = $applied_job['status'];
        }
        if (isset($applied_job)) {
            $applied_job_date = '(' . Carbon::parse($applied_job['updated_at'])->format('d F Y H:i') . ')';
        }
        $localeCode = LaravelLocalization::getCurrentLocale();
        $status = $data["status"] ?? null;
        $response_data["types"] = $types;
        $response_data["jobs"] = $jobs;
        $response_data["title"] = $title;
        $response_data["type"] = $type;
        $response_data["category_id"] = $category_id;
        $response_data["sub_category_id"] = $sub_category_id;
        $response_data["buttons"] = $buttons;
        $response_data["status"] = $status;
        $response_data["url"] = $url;
        $response_data["resumes"] = $resumes;
        $response_data["all_categories"] = $all_categories;
//        $response_data["all_types"] = $all_types;
        $response_data["all_subtypes"] = $all_sub_categories;
        $response_data["count"] = $jobs->count();
        $response_data["expired_date"] = $data['expired_date'] ?? "";
        $response_data["rejected_date"] = $data['rejected_date'] ?? "";
        $response_data["applied_job"] = $applied_job_status;
        $response_data["applied_job_date"] = $applied_job_date;
        $response_data["mirror"] = $mirror;
        $response_data["view"] = $view;
        $response_data["localeCode"] = $localeCode;
//        dd($response_data['jobs']);
        return $response_data;
    }

    /**
     * @param $slug
     * @param $status
     * @return array
     */
    public function getBySlug($slug, $status = null): array
    {
        $data = [];
        $buttons = false;
        $first = Job::with('data')->where('slug', $slug)->first();
        $updated_at = $first->updated_at;
        if ($first->status == 'posted') {
            $first->views += 1;
            $first->save();
            $first->updated_at = $updated_at;
            $first->save();
        }
        $first->updated_at = $updated_at;
        $first->save();
        $applied_job_status = '';
        $applied_job_date = '';
        if ($first) {
            $applied_job = ApplyJob::where('user_id', Auth::id())->where('job_id', $first->id)->first();
        }
        if (isset($applied_job) && isset($applied_job->status)) {
            $applied_job_status = $applied_job['status'];
        }
        if (isset($applied_job)) {
            $applied_job_date = '(' . Carbon::parse($applied_job['updated_at'])->format('d F Y H:i') . ')';
        }
        if ((Auth::check() && Auth::user()->is_organization == '1') || (Auth::check() && Auth::user()->can('accessAll_announcement'))) {
            $buttons = true;
        };

        if (isset($first->phone)) {
            $phone = $first->phone;
            $data['phone'] = $phone;
        }

        $country = Country::find($first->location_country_id);
        $data["country"] = $country;
        $data['buttons'] = $buttons;
        $data["applied_job"] = $applied_job_status;
        $data["applied_job_date"] = $applied_job_date;
        if (isset($first) && $first) {
            $data['first'] = $first;
            $data['job_status'] = $status;
        }
        return $data;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function apply($data)
    {
        $data['user_id'] = Auth::id();
        $data['status'] = 'pending';
        $applier = ApplyJob::create($data);
        if (isset($data["upload_resume"])) {
            $this->imageService->savePhoto($data["upload_resume"], 'public', $applier, $key = 'resume');
        } elseif (isset($data["choose_resume"])) {
            $cv = Cv::findorfail($data["choose_resume"]);
            $applier->resume = $cv->resumePdf($cv->id);
            $applier->save();
        }
        return $applier;
    }

    public function applyJob($data)
    {
        $data['user_id'] = Auth::id();
        $data['status'] = 'pending';
        $applier = ApplyJob::create($data);
        if (isset($data["file"])) {
            $imageFileName = $this->addCv($data);
            $applier->resume = $imageFileName;
            $applier->save();
        } elseif (isset($data["choose_resume"])) {
            $cv = Cv::findorfail($data["choose_resume"]);
            $applier->resume = $cv->resumePdf($cv->id);
            $applier->save();
        }
        return $applier;
    }

    /**
     * @param $job_id
     * @return string
     */
    public function addBookmark($job_id)
    {
        $user_id = Auth::id();
        if (Bookmark::firstOrCreate(['user_id' => $user_id, 'job_id' => $job_id])) {
            return 'remove';
        }
        return 'add';
    }

    /**
     * @param $job_id
     * @return string
     */
    public function removeBookmark($job_id)
    {
        $user_id = Auth::id();
        if (Bookmark::where(['user_id' => $user_id, 'job_id' => $job_id])->delete()) {
            return 'add';
        }
        return 'remove';

    }

    /**
     * @param $offset
     * @return mixed
     */
    public function getHistory($offset)
    {
        $user_id = Auth::id();
        $history = ApplyJob::where('user_id', $user_id)->whereHas('job')->with('job.data')->skip($offset)->take(15)->get();
        return $history;
    }


    public function addCv($data)
    {
        if (isset($data['file']) && $data['file'] != 'null') {
            $imageFileName = time() . rand(1, 999999999) . $data['name'];
            $dataFile = $data['file'];
            list($type, $dataFile) = explode(';', $dataFile);
            list(, $dataFile) = explode(',', $dataFile);
            $dataFile = base64_decode($dataFile);
            $s3 = Storage::disk('public');
            $filePath = $imageFileName;
            $s3->put($filePath, $dataFile, 'public');
            return $imageFileName;
        }
    }

    public function getApplicants()
    {
        $appliers = ApplyJob::whereHas('job', function ($query)  {
            return $query->where('user_id', Auth::id());
        })->with('user', 'job.data')->get();
        return $appliers;
    }
}