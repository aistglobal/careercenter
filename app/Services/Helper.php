<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 07.02.2019
 * Time: 17:01
 */

namespace App\Services;

use Illuminate\Support\Str;


class Helper
{
    /**
     * @param $object
     * @param $title
     * @param null $id
     * @return string
     */
    public static function getSlug($text, $modelName, $obj)
    {
        $slug = Str::slug($text, '-');
        if ($slug == '') {
            if($modelName == 'App\Job')
            {
                $slug = 'announcement-' . $obj->type->key. '-' . $obj->id;
            }
          else
          {
              $slug = 'company-'. $obj->id;
          }

        }
        else
        {
            $oldDbItem = $modelName::where('slug', $slug)->first();
            $i = 1;
            $slugTemp = $slug;
            while ($oldDbItem) {
                $slug = $slugTemp . "-" . $i;
                $i++;
                $oldDbItem = $modelName::where('slug', $slug)->first();
            }
        }


        return $slug;




    }
}