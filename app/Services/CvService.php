<?php
/**
 * Created by PhpStorm.
 * User: Aist
 * Date: 8/30/2019
 * Time: 3:51 PM
 */

namespace App\Services;


use App\Contracts\CvServiceInterface;
use App\Core\Services\BaseService;
use App\Cv;

class CvService extends BaseService implements CvServiceInterface
{
  /**
   * @param $id
   * @return mixed
   */
  public function getById($id)
  {
    // TODO: Implement getById() method.
    $cv = Cv::find($id);
    return $cv;
  }

  /**
   * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
   */
  public function getAll()
  {
    // TODO: Implement getAll() method.
    $cvs = Cv::all();
    return $cvs;
  }

  /**
   * @param $id
   * @return mixed
   */
  public function getAllByUserId($id)
  {
    // TODO: Implement getAllByUserId() method.
    $user_csv = Cv::where('user_id',$id)->get();
    return $user_csv;
  }
}