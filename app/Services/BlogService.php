<?php
/**
 * Created by PhpStorm.
 * User: Aist
 * Date: 8/31/2019
 * Time: 12:43 PM
 */

namespace App\Services;


use App\Blog;
use App\Contracts\BlogServiceInterface;
use App\Core\Services\BaseService;

class BlogService extends BaseService implements BlogServiceInterface
{
  /**
   * @var SeoService
   */
  protected $seoService;
  /**
   * @var ImageService
   */
  protected $imageService;

  /**
   * JobService constructor.
   * @param SeoService $seoService
   * @param ImageService $imageService
   */
  public function __construct(SeoService $seoService, ImageService $imageService)
  {
    $this->seoService = $seoService;
    $this->imageService = $imageService;
  }

  /**
   * @param $slug
   * @return mixed
   */
  public function getBySlug($slug)
  {
    // TODO: Implement getBySlug() method.
    $data['blog'] = '';
    $blog = Blog::whereHas('data')->where('slug', $slug)->first();
    if ($blog) {
      $data['blog'] = $blog;
      $related = Blog::where('id', '!=', $blog->id)->whereHas('data')->limit(3)->get();
      $data['related'] = $related->count() > 0 ? $related : '';
    }
    $this->seoService->setMetaData($blog, 'blog');
    return $data;

  }

  /**
   * @return array|mixed
   */
  public function getAll()
  {
    // TODO: Implement getAll() method.
    $data = [];
    $blogs = Blog::whereHas('data')->with('data')->get();
    $this->seoService->setPageMeta('articles');
    if ($blogs->count() > 0) {
      $first = $blogs[0];
      $blogs->shift();
      $data['blogs'] = $blogs;
      $data['first'] = $first;
    }
    return $data;
  }
}