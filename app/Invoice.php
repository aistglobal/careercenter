<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Invoice extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'invoices';
    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'performer','performer_address','performer_bank','performer_tax','performer_account',
        'client','client_address','client_bank','client_tax','client_account',
        'booking_number',
        'account_product',
        'date',
        'money',
        'currency',
        'director',
        'position',
        'posted_announcements',
        'notes',
        'individual',
        'passport',
        'status'
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function invoiceItems()
    {
        return $this->hasMany(InvoiceItem::class, 'invoice_id');
    }
}

