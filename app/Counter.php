<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Counter extends Model
{

    /**
     * @var string
     */
    protected $table = 'counters';

    /**
     * @var array
     */
    protected $fillable = ['count'];
    public static function count()
    {
        $counter = Counter::first();
        if (isset($counter)) {
            $counter->count++;
            $counter->save();
        }
        else
        {
            Counter::create(['count'=>17000001]);
        }
    }
}
