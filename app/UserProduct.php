<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProduct extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_products';
    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'product_id','quantity','type'];

    public function  service()
    {
        return $this->belongsTo(Service::class, 'product_id');
    }

}
