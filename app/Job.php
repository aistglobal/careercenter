<?php

namespace App;

use App\Services\Helper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;

class Job extends Model
{

    use LogsActivity;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jobs';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'type_id';
    protected $fillable = [
        'code', 'type_id', 'status', 'expiration_date', 'phone', 'email',
        'online_submission', 'user_id', 'isbn', 'location_country_id', 'slug', 'same_place', 'added_user_id',
        'edited_user_id', 'added_IP', 'edited_IP', 'mirror_id', 'views','product_id'];

    protected $dates = ['expiration_date', 'sort_by'];

    /**
     * @param $text
     * @return bool|null|string|string[]
     */

    protected static $logAttributes = ['slug', 'status'];

    public static function getOpeningDate($value)
    {
        if ($value && strtotime($value)) {
            return \Carbon\Carbon::parse($value)->format('d') . ' ' . trans('months.' . \Carbon\Carbon::parse($value)->format('M')) . ' ' . \Carbon\Carbon::parse($value)->format('Y');
        }
        return null;

    }

    public static function slugify($text)
    {

        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function data()
    {
        return $this->hasOne(JobData::class, 'job_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function appliers()
    {
        return $this->hasMany(ApplyJob::class, 'job_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(Type::class, 'type_id', 'id');
    }

    public function category()
    {
        return $this->belongsToMany(Category::class, 'job_categories', 'job_id', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachment()
    {
        return $this->hasMany(Attachment::class, 'job_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookmarks()
    {
        return $this->hasMany(Bookmark::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class, 'user_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function added_user()
    {
        return $this->belongsTo(User::class, 'added_user_id', 'id');
    }

    public function edited_user()
    {
        return $this->belongsTo(User::class, 'edited_user_id', 'id');
    }

    public function mirror()
    {
        return $this->belongsTo(Job::class, 'mirror_id', 'id');
    }

    /**
     * @param null $status
     * @param null $category_id
     * @param null $sub_category_id
     * @param null $type
     * @param null $expired_date
     * @return string
     */
    public static function getCount($status = null, $category_id = null, $sub_category_id = null, $type = null, $expired_date = null): string
    {
        $status = $status ?? 'all';
        $jobs = DB::table('jobs')->where('jobs.slug', '!=', null)->leftJoin('users', 'jobs.user_id', '=', 'users.id')->join('organizations', 'users.organization_id', '=', 'organizations.id');
        if ($category_id || $sub_category_id) {
            $jobs = $jobs->leftJoin('job_categories', 'jobs.id', '=', 'job_categories.job_id');
        }
        if ($category_id && !$sub_category_id) {
            $jobs = $jobs->where('category_id', $category_id);
        } elseif ($sub_category_id) {
            $jobs = $jobs->where('category_id', $sub_category_id);
        }
        if ($type) {
            $jobs = $jobs->where('type_id', $type);
        }
        if ($expired_date) {
            $jobs = $jobs->whereYear('jobs.expiration_date', '=', Carbon::parse($expired_date)->format('Y'))
                ->whereMonth('jobs.expiration_date', '=', Carbon::parse($expired_date)->format('m'));
        }
        switch ($status) {
            case 'all':
                $jobs = $jobs->where('jobs.status', '=', 'posted');
                break;
            case 'posted':
                $jobs = $jobs->where('jobs.status', '=', 'posted');
                break;
            case 'expired':
                $jobs = $jobs->where('jobs.status', '=', 'expired');
                break;
            case 'draft':
                $jobs = $jobs->where('jobs.status', '=', 'draft');
                break;
            case 'pending':
                $jobs = $jobs->where('jobs.status', '=', 'pending');
                break;
            case 'rejected':
                $jobs = $jobs->where('jobs.status', '=', 'rejected');
                break;
            case 'my-draft':
                $jobs = $jobs->where('organizations.id', Auth::user()->organization_id)->where('jobs.status', '=', 'draft');
                break;
            case 'my-expired':
                $jobs->where('organizations.id', Auth::user()->organization_id)->where('jobs.status', '=', 'expired');
                break;
            case 'my-pending':
                $jobs = $jobs->where('organizations.id', Auth::user()->organization_id)->where('jobs.status', '=', 'pending');
                break;
            case 'my-rejected':
                $jobs = $jobs->where('organizations.id', Auth::user()->organization_id)->where('jobs.status', '=', 'rejected');
                break;
            case 'my-posted':
                $jobs = $jobs->where('organizations.id', Auth::user()->organization_id)->where('jobs.status', '=', 'posted');
                break;
            case 'applied_job':
                $jobs = $jobs->join('apply_jobs', 'jobs.id', '=', 'apply_jobs.job_id')->where('apply_jobs.user_id', Auth::id())->where('jobs.status', '=', 'posted')->get();
                break;
        }


        return $jobs->count() > 0 ? '(' . $jobs->count() . ')' : '';

    }

    /**
     * @param int $id
     * @return int
     */
    public static function getAppliers(int $id): int
    {
        $job = (new static)::find($id);
        return count($job->appliers);
    }

    public static function is_enumeration($field = null): string
    {

        if (isset($field) && $field != '') {
            if ($field[0] == '-') {
                return true;
            } else {
                return false;
            }

        } else {
            return true;
        }


    }

    public static function expiredJobFilter()
    {
        return DB::table('jobs')->select(DB::raw('count(id) as `total`'), DB::raw("DATE_FORMAT(expiration_date, '%Y-%m') expired_date"))->where('status', 'expired')
            ->groupBy('expired_date')->orderBy('expired_date', 'desc')
            ->get();
    }

    public static function rejectedJobFilter()
    {
        return DB::table('jobs')->select(DB::raw('count(id) as `total`'), DB::raw("DATE_FORMAT(created_at, '%Y-%m') rejected_date"))->where('status', 'rejected')
            ->groupBy('rejected_date')->orderBy('rejected_date', 'desc')
            ->get();
    }

    public static function emailize($text)
    {
//        if (strpos($text, 'https://') !== false) {
//            $text = str_replace('https://', '', $text);
//        }
//        if (strpos($text, 'http://') !== false) {
//            $text = str_replace('https://', '', $text);
//        }
        $text = str_replace('www.', 'https://www.', $text);
        $url = '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i';
        $text = preg_replace($url, '<a href="$0" target="_blank" title="$0">\2 \3</a>', $text);
        $text = preg_replace('/([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,20})/i', '<a href="mailto:\1" rel="nofollow">\1</a>', $text);
        return $text;
    }

    public static function procedures($text, $first, $applied_job)
    {
        $apply_online = trans('announcements.apply_online');
        $link =  $apply_online;
        if (Auth::check() && Auth::user()->is_organization != 1 && $first->online_submission == 1)
        {
            if ($applied_job == "") {
                $link = '<a  data-id="' . $first->id . '" class="apply">' . trans('announcements.apply_now') . '</a>';

            } elseif ($applied_job == 'rstat_Incomplete') {
                $link = '<a  data-id="' . $first->id . '" class="apply">' . trans('announcements.apply_now') . '</a>';

            }
        }
        elseif (!Auth::check() && $first->online_submission == 1)
        {
            $link = '<a href=' . route('applyJob', $first->slug) . '>' . trans('announcements.apply_now') . '</a>';
        }


        if (strpos($text, $apply_online) !== false) {
            $text = str_replace($apply_online, $link, $text);
        }
        return $text;

    }

    public static function clone($id)
    {
        $job = Job::with('data')->find($id);
        $new_job = $job->replicate();
        $new_job->status = 'preview';
        $new_job->mirror_id = $id;
        $new_job->save();


        $job_data = $job->data;
        $new_data = $job_data->replicate();
        $new_data->job_id = $new_job->id;
        $new_data->save();

        $new_job_slug = Helper::getSlug($new_job->data->title, 'App\Job', $new_job);
        $new_job->slug = $new_job_slug;
        $new_job->save();
        $attachments = $job->attachment;
        if (count($attachments) != 0) {
            foreach ($attachments as $item) {
                $new_attachment = $item->replicate();
                $new_attachment->job_id = $new_job->id;
                $new_attachment->save();
            }
        }
        $categories = JobCategory::where('job_id', $job->id)->get();


        if (count($categories) != 0) {
            foreach ($categories as $item) {
                $new_category = $item->replicate();
                $new_category->job_id = $new_job->id;
                $new_category->save();
            }
        }

        return $new_job;
    }

    public static function companyName($company, $dataCompany)
    {
        $dataComp = explode(" ", $dataCompany);
        if (count($dataComp) >= 2) {
            for ($i = 0; $i < count($dataComp); $i++) {
                if (isset($company->legal_structure)) {
                    if ($dataComp[$i] == $company->legal_structure || $dataComp[$i] == trans('legal_structure.' . $company->legal_structure)) {
                        array_splice($dataComp, $i);
                    }
                }
            }

        }
        $r = implode(" ", $dataComp);
        return $r;

    }

}
