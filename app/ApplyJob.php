<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplyJob extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'apply_jobs';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    protected $fillable = ['user_id','job_id','resume','cover_letter','status'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    public function job()
    {
        return $this->belongsTo('App\Job', 'job_id', 'id');
    }

}
