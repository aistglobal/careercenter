<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CvEducation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cv_education';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $foreignKey = 'cv_id';
    protected $fillable = [
        'cv_id',
        'name',
        'city',
        'country_id',
        'type',
        'department',
        'study_area',
        'major_field',
        'period_from',
        'period_to',
        'degree',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
}
