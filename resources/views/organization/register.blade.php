@extends('layouts.app')

@section('css')
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .is-invalid .invalid-feedback {
            display: block;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid form-container">
        <h1 class="title-h1 font-bold text-center">{{trans('register_step_2.step_2')}}</h1>
        <form method="POST" action="{{route('user.register_final', $user->id)}}" enctype="multipart/form-data">
            @csrf
            <div class="row justify-content-center">

                <div class="col-lg-4 z-index-1">
                    <div class="form-group secondary" style="display: none">
                        <select name="title" id="title">
                            <option></option>
                            @foreach(config('enums.title') as $item)
                                <option @if(old('title')!=null && old('title')==$item)selected
                                        @endif   value="{{$item}}">{{trans('title.'.$item)}}</option>
                            @endforeach
                        </select>
                        <label for="title" class="control-label">{{trans('register_step_2.title')}}</label>
                        @error('title')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="text" id="first_name" name="first_name"
                               class="@error('first_name') is-invalid @enderror" value="{{ old('first_name') }}"
                               required>
                        <label for="first_name" class="control-label">{{trans('register_step_2.first_name')}} <span
                                    class="star">*</span></label>
                        @error('first_name')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group secondary" style="display: none">
                        <input type="text" id="middle_name" name="middle_name"
                               class="@error('middle_name') is-invalid @enderror" value="{{ old('middle_name') }}"
                               data-required="no">
                        <label for="middle_name" class="control-label">{{trans('register_step_2.middle_name')}}</label>
                        @error('middle_name')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="text" id="last_name" name="last_name"
                               class="@error('last_name') is-invalid @enderror" value="{{ old('last_name') }}" required>
                        <label for="last_name" class="control-label">{{trans('register_step_2.last_name')}} <span
                                    class="star">*</span></label>
                        @error('last_name')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input required id="position" type="text" class="@error('position') is-invalid @enderror"
                               name="position"
                               value="{{ old('position')}}">
                        <label for="position" class="control-label">{{trans('register_step_2.position')}} <span
                                    class="star">*</span></label>
                        @error('position')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input id="org_name" type="text" class="@error('name') is-invalid @enderror" name="name"
                               value="{{ old('name') }}" required>
                        <label for="org_name" class="control-label">{{trans('register_step_2.org_name')}} <span
                                    class="star">*</span></label>
                        @error('name')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group secondary" style="display: none">
                        <input id="short_name" type="text" class="@error('short_name') is-invalid @enderror"
                               name="short_name" value="{{ old('short_name') }}">
                        <label for="short_name"
                               class="control-label">{{trans('register_step_2.org_short_name')}}</label>
                        @error('short_name')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group secondary" style="display: none">
                        <select class="form-control" name="legal_structure" id="legal_structure">
                            <option></option>
                            @foreach(config('enums.legal') as $item)
                                <option @if(old('legal_structure')!=null && old('legal_structure')==$item)selected
                                        @endif value="{{$item}}">{{trans('legal_structure.'.$item)}}</option>
                            @endforeach
                        </select>
                        <label for="legal_structure" class="control-label">{{trans('register_step_2.legal_structure')}}
                        </label>
                        @error('legal_structure')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div id="legal-other" class="form-group" style="display: @if(old('legal_structure') == 'other') block @else none @endif">
                        <input  id="legal-other-input" type="text" class="@error('legal-other') is-invalid @enderror"
                               name="legal_other" value="{{ old('legal_other') }}">
                        <label for="legal-other"
                               class="control-label">{{trans('register_step_2.legal-other')}}</label>
                        @error('legal_other')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group secondary" style="display: none">
                        <select class="form-control" name="type" id="org_type">
                            <option></option>
                            @foreach(config('enums.org_type') as $item)
                                <option @if(old('type')!=null && old('type')==$item)selected
                                        @endif value="{{$item}}">{{trans('org_type.'.$item)}}</option>
                            @endforeach
                        </select>
                        <label for="org_type" class="control-label">{{trans('register_step_2.org_type')}}</label>
                        @error('type')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group secondary" style="display: none">
                        <select class="form-control" name="field_of_activities" id="fields_of_activity">
                            <option></option>
                            @foreach(config('enums.fields_of_activity') as $item)
                                <option @if(old('field_of_activities')!=null && old('field_of_activities')==$item)selected
                                        @endif value="{{$item}}">{{trans('fields_of_activity.'.$item)}}</option>
                            @endforeach
                        </select>
                        <label for="fields_of_activity"
                               class="control-label">{{trans('register_step_2.fields_of_activity')}}</label>
                        @error('fields_of_activities')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group secondary" style="display: none">
                        <select class="form-control" name="number_of_employees" id="number_of_employees">
                            <option></option>
                            @foreach(config('enums.number_of_employees') as $item)
                                <option @if(old('number_of_employees')!=null && old('number_of_employees')==$item)selected
                                        @endif value="{{$item}}">{{$item}}</option>
                            @endforeach
                        </select>
                        <label for="number_of_employees"
                               class="control-label">{{trans('register_step_2.number_of_employees')}}</label>
                        @error('number_of_employees')
                        <span class="number_of_employees" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="col-lg-4 z-index-1">
                    <div class="form-group">
                        <textarea id="about_company" class="@error('about_company') is-invalid @enderror"
                                  name="about_company">{{ \Illuminate\Support\Facades\Input::old('about_company') }}</textarea>
                        <label for="about_company" class="control-label">{{trans('register_step_2.about_company')}}</label>

                            @error('about_company')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div id="dynamic_field3" class="secondary" style="display: none">

                        <div class="form-group">
                            <input name="address[]" value="{{old('address')[0]}}" id="address" type="text"
                                   @if(old('key_address_count')>1) required @endif>
                            <label id="address_label_0" for="address" class="control-label">{{trans('register_step_2.address')}}</label>
                            @error('address')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <select data-id="0" class="address_type" name="address_type[]" id="address_type_0">
                                <option value=""></option>
                                <option @if(old('address_type')[0]=='legal') selected
                                        @endif value="legal">{{trans('register_step_2.legal')}}</option>
                                <option @if(old('address_type')[0]=='business') selected
                                        @endif  value="business">{{trans('register_step_2.business')}}</option>
                            </select>
                            <label for="address_type_0"
                                   class="control-label">{{trans('register_step_2.address_type')}}</label>
                        </div>
                        <div class="form-group">
                            <select name="country[]" id="address-country"
                                    @if(old('key_address_count')>1) required @endif>
                                <option></option>
                                @foreach($countries as $item)
                                    <option @if(old('country')[0]!=null && old('country')[0] ==$item->id)selected
                                            @endif value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                                @endforeach
                            </select>
                            <label for="address-country"
                                   class="control-label">{{trans('register_step_2.country')}}</label>

                            @error('country')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input class="@error('city.0') is-invalid @enderror" type="text" name="city[]" id="City "
                                   value="{{old('city')[0]}}" @if(old('key_address_count')>1) required @endif>
                            <label for="City " class="control-label">{{trans('register_step_2.city')}}</label>
                            @error('city.0')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="text" class="@error('zip.0') is-invalid @enderror" name="zip[]" id="zip"
                                   value="{{old('zip')[0]}}" @if(old('key_address_count')>1) required @endif>
                            <label for="zip" class="control-label">{{trans('register_step_2.zip')}}</label>
                            @error('zip.0')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                            @enderror
                        </div>

                        @for($i=1;$i<old('key_address_count');$i++)
                            <div class="dynamic-added-block deleting3 position-relative" id="row3_{{$i+1}}">
                                <div class="form-group">
                                    <input class="form-control deleting-input3_1" id="address{{$i+1}}" required
                                           name="address[]" value="{{old('address')[$i]}}" type="text">
                                    <label for="address{{$i+1}}"
                                           class="control-label">{{trans('register_step_2.address')}} {{$i+1}} <span
                                                class="star">*</span></label>
                                </div>
                                <div class="form-group">
                                    <select class="" name="address_type[]" id="address_type_{{$i}}">
                                        <option value=""></option>
                                        <option @if(old('address_type')[$i]=='legal') selected
                                                @endif value="legal">{{trans('register_step_2.legal')}}</option>
                                        <option @if(old('address_type')[$i]=='business') selected
                                                @endif  value="business">{{trans('register_step_2.business')}}</option>
                                    </select>
                                    <label for="address_type_{{$i}}"
                                           class="control-label">{{trans('register_step_2.address_type')}}</label>
                                </div>
                                <div class="form-group">
                                    <select name="country[]" id="address-country{{$i}}" required>
                                        <option></option>
                                        @foreach($countries as $item)
                                            <option @if(old('country')[$i]!=null && old('country')[$i] ==$item->id)selected
                                                    @endif value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                                        @endforeach
                                    </select>
                                    <label for="address-country"
                                           class="control-label">{{trans('register_step_2.country')}} {{$i+1}} <span
                                                class="star">*</span></label>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="city[]"
                                           class="@error('city.'.$i) is-invalid @enderror deleting-input3_{{$i}}"
                                           id="City{{$i+1}}"
                                           value="{{old('city')[$i]}}" required>
                                    <label for="City " class="control-label">{{trans('register_step_2.city')}}{{$i+1}}
                                        <span class="star">*</span></label>
                                    @error('city.'.$i)
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input id="zip{{$i+1}}" type="text"
                                           class="@error('zip.'.$i) is-invalid @enderror deleting-input3_{{$i}}"
                                           name="zip[]"
                                           value="{{old('zip')[$i]}}" required>
                                    <label for="zip" class="control-label">{{trans('register_step_2.zip')}}{{$i+1}}
                                        <span class="star">*</span></label>
                                    @error('zip.'.$i)
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                    @enderror
                                </div>
                                <button type="button" name="remove" id="{{$i+1}}"
                                        class="btn btn-danger deleting-button3 btn_remove 3">{{trans('register_step_2.delete')}}
                                </button>
                            </div>
                        @endfor
                        <input type="hidden" name="key_address_count" id="key_address_count" value="{{old('key_address_count')}}">
                    </div>
                    <button type="button" name="add3" id="add3" class="add-input font-bold secondary" style="display: none">{{trans('register_step_2.add_address')}}</button>
                    <input type="hidden" name="is_organization" value="{{$user->is_organization}}">
                </div>

                <div class="col-lg-4 z-index-1">

                    <div id="dynamic_field4">
                        <div class="form-group" id="row4_1">
                            <div class="form-group">
                                <input class="phone @error('phone.0') is-invalid @enderror" required name="phone[]"
                                       id="phone-0" value="{{old('phone')[0]}}">
                                <label id="phone_label_0" for="phone-0" class="control-label">{{trans('register_step_2.phone')}} <span
                                            class="star">*</span></label>
                                @error('phone.0')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <select data-id="0" class="phone_type" name="phone_type[]" id="phone_type_0">
                                    <option value=""></option>
                                    @foreach(config('enums.phone_type') as $item)
                                        <option @if(old('phone_type')[0]==$item) selected
                                                @endif value="{{$item}}">{{trans('register_step_2.'.$item)}}</option>
                                    @endforeach
                                </select>
                                <label for="phone_type_0" class="control-label">{{trans('register_step_2.phone_type')}}</label>

                            </div>


                        </div>
                        @for($i=1;$i<old('key_phone_count');$i++ )
                            <div id="row4_{{$i+1}}" class="dynamic-added deleting4 form-group ">
                                <div class="form-group">
                                    <input name="phone[]"
                                           class="@error('phone.'.$i) is-invalid @enderror deleting-input4_1 "
                                           value="{{old('phone')[$i]}}">
                                    <button type="button" name="remove" id="{{$i+1}}"
                                            class="btn btn-danger deleting-button4 btn_remove 4">X
                                    </button>
                                    <label for="phone-{{$i}}"
                                           class="control-label">{{trans('register_step_2.phone')}}{{$i+1}} </label>
                                    @error('phone.'.$i)
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <select class="mt-2" name="phone_type[]" id="phone_type_{{$i}}">
                                        <option value=""></option>
                                        @foreach(config('enums.phone_type') as $item)
                                            <option @if(old('phone_type')[$i]==$item) selected
                                                    @endif value="{{$item}}">{{trans('register_step_2.'.$item)}}</option>
                                        @endforeach
                                    </select>
                                    <label for="phone_type_{{$i}}"
                                           class="control-label">{{trans('register_step_2.phone_type')}}</label>
                                </div>
                            </div>
                        @endfor
                        <input type="hidden" name="key_phone_count" id="key_phone_count"
                               value="{{old('key_phone_count')}}">
                    </div>
                    <button type="button" name="add4" id="add4" class="add-input font-bold">{{trans('register_step_2.add_phone')}}</button>

                    <div class="form-group">
                        <input id="email" type="text" class="@error('email') is-invalid @enderror" name="email"
                               value="{{ old('email') }}" required>
                        <label for="email" class="control-label">{{trans('register_step_2.email')}} <span
                                    class="star">*</span></label>
                        @error('email')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input  class="@error('website') is-invalid @enderror" name="website" id="web"
                               value="{{ old('website') }}" data-required="no">
                        <label for="web" class="control-label">{{trans('register_step_2.web')}}</label>
                        @error('website')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="@error('image')is-invalid @enderror form-group-file file-input-style">
                        <label for="logo" class="sr-only">{{trans('register_step_2.logo')}}</label>
                        <input type="file" id="logo" name="image" accept="image/*">
                        @error('image')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror

                    </div>
                </div>

                <div class="col-lg-12 z-index-1">
                    <div class="text-center">
                        <button type="button" class="btn btn-orange-gradient"
                                id="advanced">{{trans('login.advanced')}}</button>
                    </div>
                    <div class="text-center mt-3">
                        <button type="submit" class="btn btn-blue-gradient"
                                id="register"> {{trans('login.register')}}</button>
                    </div>
                </div>

            </div>
            <input id="is_advanced" type="hidden" name="is_advanced" value="no">
        </form>

    </div>

    <img src="/img/register-wave-2.svg" alt="" class="w-100">


@endsection

@section('scripts')
    <script src="/js/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
    <script>

        // CKEDITOR.replace('about_company');


        $(document).ready(function () {
            $('#legal_structure').on('change',function()
            {
                $('#legal-other').css('display', 'none');
                let val = $(this).val();
                if(val == 'other')
                {
                    $('#legal-other').css('display', 'block');
                }
                else
                {
                    $('#legal-other-input').val('');
                }
            })

            var is_advanced = '{{old('is_advanced')}}';
            if (is_advanced == 'yes') {
                $('#advanced').css('display', 'none');
                $('#is_advanced').val('yes');
                $('.secondary').each(function () {
                    $(this).css('display', 'block');
                })
            }
            $('#advanced').on('click', function () {
                $(this).css('display', 'none');
                $('#is_advanced').val('yes');
                $('.secondary').each(function () {
                    $(this).css('display', 'block');
                })
            })



            var a = '';
            var sum1 = '';
            let type;
            @if (old('key_phone_count') == null ||  empty(old()))
                a = 1;
            sum1 = 1;
            @else
                a = parseInt('{{old('key_phone_count')}}');
            sum1 = parseInt('<?php echo old('key_phone_count')  ?>');
            @endif
            $('#add4').on('click', function () {

                if (sum1 <= 5) {
                    ++a;
                    var select_phone = '<select data-id="'+a+'" class="form-control phone_type" name="phone_type[]"><option></option>';
                        @foreach(config('enums.phone_type') as $item)
                    {
                        select_phone += "<option value='{{$item}}'>{{trans('register_step_2.'.$item)}}</option>"
                    }
                    @endforeach
                        select_phone += '</select>';
                    $('#dynamic_field4').append('<div id="row4_' + a + '" class="dynamic-added deleting4">' +
                        '<div class="form-group"><input id="phone' + a + '" class="phone" required name="phone[]" class="deleting-input4_1 " value="">' +
                        '<label id="phone_label_'+a+'" class="control-label" for="phone' + a + '" >{{trans('register_step_2.phone')}} '  + '</label>' +
                        '<button type="button" name="remove" id="' + a + '" class="btn btn-danger deleting-button4 btn_remove 4">X</button></div><div class="form-group">' +
                        select_phone +
                        '<label class="control-label" for="">{{trans('register_step_2.phone_type')}} '  + '</label></div></div>');
                    sum1++;
                    if(sum1==5){
                        $('#add4').css('display','none')
                    }
                }

                $('#key_phone_count').val(a);
            });

            $(document).on('click', '.4', function () {
                console.log('bo');
                var button_id3 = $(this).attr("id");

                var key_phone_val = $('#key_phone_count').val();
                $('#key_phone_count').val(key_phone_val - 1);
                $('#row4_' + button_id3).remove();
                sum1--;
                a--;

//                var count = 2;
//                $(".deleting4").each(function (key, element) {
//                    $(this).attr("id", 'row4_' + count);
//                    $(this).find('.deleting-input4_1').attr('name', 'current_city_' + count);
//                    $(this).find('.deleting-button4').attr('id', count);
//                    count++;
//                });
            });

            var l = '';
            var sum = '';
            @if (old('key_address_count') == null ||  empty(old()))
                l = 1;
            sum = 1;
            @else
                l = parseInt('{{old('key_address_count')}}');
            sum = parseInt('<?php echo old('key_address_count')  ?>');
            @endif

            $('#add3').on('click', function () {

                $('#address').attr('required', 'required')
                $('#address-country').attr('required', 'required')
                $('#City').attr('required', 'required')
                $('#zip').attr('required', 'required')


                var select = '<select class="form-control" name="country[]" required id="address-country' + l + '"><option></option>';
                        @foreach($countries as $item)
                            select += "<option value ={{$item->id}}>{{trans('countries.'.$item->code)}}</option>"
                        @endforeach
                select += '</select>';

                if (sum <= 5) {
                    ++l;
                    $('#dynamic_field3').append(
                        '<div id="row3_' + l + '" class="dynamic-added-block deleting3 position-relative">' +
                        '<div class="form-group">' +
                        '<input required  name="address[]" class="form-control deleting-input3_1 " value="" id="address' + l + '">' +
                        '<label id="address_label_'+l+'" for="address' + l + '" class="control-label">{{trans('register_step_2.address')}} '  + '</label></div><div class="form-group">' +
                        '<select data-id="'+l+'" id="address_type_' + l + '" class="address_type" name="address_type[]"><option value=""></option><option value="legal">{{trans('register_step_2.legal')}}</option><option value="business">{{trans('register_step_2.business')}}</option></select>' +
                        '<label for="address_type_' + l + '" class="control-label">{{trans('register_step_2.address_type')}}</label>' +
                        '</div>' +
                        '<div class="form-group">' +
                        select + '' +
                        '<label for="address-country' + l + '" class="control-label">{{trans('register_step_2.country')}} '  + '</label>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<input id="city' + l + '" type="text" required name="city[]" class="deleting-input3_1 " value="">' +
                        '<label for="City' + l + '" class="control-label">{{trans('register_step_2.city')}} '  + '</label>' +
                        '</div>' +
                        '<div class="form-group"> ' +
                        '<input type="text" required  name="zip[]" class="deleting-input3_1" value="" id="zip' + l + '">' +
                        '<label for="zip' + l + '" class="control-label">{{trans('register_step_2.zip')}}'  + '</label>' +
                        '</div>' +
                        '<button type="button" name="remove" id="' + l + '" class="btn btn-danger deleting-button3 btn_remove 3">{{trans('register_step_2.delete')}} '  + '</button>' +
                        '</div>');
                    sum++;
                    if(sum==5){
                        $('#add3').css('display','none')
                    }
                }
                $('#key_address_count').val(l);
            });

            $(document).on('click', '.3', function () {
                var button_id3 = $(this).attr("id");

                var key_address_val = $('#key_address_count').val();
                $('#key_address_count').val(key_address_val - 1);
                $('#row3_' + button_id3).remove();
                sum--;
                l--;
                if (sum == 1) {
                    $('#address').removeAttr('required')
                    $('#address-country').removeAttr('required')
                    $('#City').removeAttr('required')
                    $('#zip').removeAttr('required')
                }

//                var count = 2;
//                $(".deleting3").each(function (key, element) {
//                    $(this).attr("id", 'row3_' + count);
//                    $(this).find('.deleting-input3_1').attr('name', 'current_address_' + count);
//                    $(this).find('.deleting-button3').attr('id', count);
//                    count++;
//                });
            });
            $(document).on('change', '.phone_type', function () {
                type = $(this).find(':selected').text();
                $('#phone_label_' + $(this).attr('data-id')).text('{{trans('register_step_2.phone')}}' + ' ' + type);

            });
            $(document).on('change', '.address_type', function () {
                type = $(this).find(':selected').text();
                $('#address_label_' + $(this).attr('data-id')).text('{{trans('register_step_2.address')}}' + ' ' + type);

            });
        });

    </script>
@endsection





