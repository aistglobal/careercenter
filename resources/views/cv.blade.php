@extends('layouts.app')
@section('css')

    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        @media print {
            header,
            footer {
                display: none;
            }
        }
    </style>
@endsection
@section('content')

    <section class="cv-page">
        <a href="{{route('resume.create',$cv->id)}}" title="Edit" class="btn btn-blue-gradient">
            <i class="fa fa-arrow-left"></i> {{trans('resume.edit')}}
        </a>
        <a href="{{route('resume.index')}}" title="Back" class="btn btn-blue-gradient">
            <i class="fa fa-arrow-left"></i> {{trans('resume.back')}}
        </a>
        <div class="cv-head text-center">
            <h3 class="print">{{$cv->contacts->first_name}} {{$cv->contacts->middle_name}}  {{$cv->contacts->last_name}}</h3>
            <div class="print">
                <div> @if(count($cv->address)!=0) {{$cv->address[0]->address}},  {{$cv->address[0]->city}}
                    , {{$cv->address[0]->zip}} {{trans('countries.'.$cv->address[0]->country->code)}}  @endif </div>
                @if(isset($phone))
                    <div>
                        {{trans('resume.phone')}}: <a href="tel:{{$phone[0]["phone"]}}">{{$phone[0]["phone"]}}</a>
                    </div>
                @endif
                @if(isset($cv->contacts->email))
                    <div>
                        {{trans('resume.email')}}: <a
                                href="mailto:{{$cv->contacts->email}}">{{$cv->contacts->email}}</a>
                    </div>
                @endif
            </div>

        </div>
        @if(isset($cv->personal))
            <div class="info-section">

                <h3 class="cv-title print"><img width='22px' src="/img/prsonal-info.svg" alt=""
                                                class="mr-2">{{trans('resume.personal')}}</h3>
                <div class="d-flex justify-content-between flex-column-reverse flex-sm-row">
                    <div>
                        <h3 class="display">{{$cv->contacts->first_name}} {{$cv->contacts->middle_name}} {{$cv->contacts->last_name}}</h3>
                        <table class="mr-2 info-table">
                            @if(isset($cv->personal->birthday))
                                <tr>
                                    <td>{{trans('resume.birth_date')}}:</td>
                                    <td>{{$cv->personal->birthday}}</td>
                                </tr>
                            @endif
                            @if(isset($cv->personal->marital_status))
                                <tr>
                                    <td>{{trans('resume.marital_status')}}:</td>
                                    <td> {{trans('cv_personal.'.$cv->personal->marital_status)}}</td>
                                </tr>
                            @endif
                            @if(isset($cv->personal->sex))
                                <tr>
                                    <td>{{trans('resume.sex')}}:</td>
                                    <td> {{trans('resume.'.$cv->personal->sex)}}</td>
                                </tr>
                            @endif
                            @if(isset($cv->personal->height))
                                <tr>
                                    <td>{{trans('resume.height')}}:</td>
                                    <td>{{$cv->personal->height}} {{trans('cv_personal.'.$cv->personal->height_measurement)}}</td>
                                </tr>
                            @endif
                            @if(isset($cv->personal->weight))
                                <tr>
                                    <td>{{trans('resume.weight')}}:</td>
                                    <td>{{$cv->personal->weight}}   {{trans('cv_personal.'.$cv->personal->weight_measurement)}}</td>
                                </tr>
                            @endif
                            @if(isset($cv->personal->nationality_id))
                                <tr>
                                    <td>{{trans('resume.nationality')}}:</td>
                                    <td>{{trans('countries.'.$cv->personal->nationality->code)}}</td>
                                </tr>
                            @endif
                            @if(isset($personal_citizenship) && count($personal_citizenship)!=0)
                                <tr>
                                    <td>{{trans('resume.citizenship')}}:</td>
                                    <td>
                                        @foreach($countries as $item)
                                            @if(in_array($item->id, $personal_citizenship))
                                                {{trans('countries.'.$item->code)}}
                                            @endif
                                        @endforeach
                                    </td>
                                </tr>
                            @endif
                            @if(isset($cv->personal->official_service))
                                <tr>
                                    <td>{{trans('resume.official_service')}}:</td>
                                    <td>{{trans('cv_personal.'.$cv->personal->official_service)}}</td>
                                </tr>
                            @endif

                            @if(isset($cv->personal->dependants))
                                <tr>
                                    <td>{{trans('resume.dependants')}}:</td>
                                    <td>{{trans('resume.'.$cv->personal->dependants)}}</td>
                                </tr>
                                @if($cv->personal->dependants == 'yes' && isset($cv->dependants) && count($cv->dependants)!=0)

                                    @foreach($cv->dependants as $item)
                                        <tr>
                                            <td>
                                                {{trans('cv_personal.'.$item->relationship)}}
                                            </td>
                                            <td>
                                                {{$item->name}} {{$item->birthday}}
                                            </td>
                                        </tr>
                                    @endforeach


                                @endif
                            @endif
                            <tr>
                                <td>{{trans('resume.disabled')}}:</td>
                                <td>{{trans('resume.'.$cv->personal->disabled)}}</td>
                            </tr>
                            @if($cv->personal->disabled=='yes' && isset($cv->personal->disabled_description))
                                <tr>
                                    <td>{{trans('resume.disabled_details')}}:</td>
                                    <td>{{trans('resume.disabled_details')}}</td>
                                </tr>
                            @endif
                            @if(isset($phone) && count($phone)>1)
                                <tr>
                                    <td class="text-right">{{trans('resume.mobile')}}:</td>
                                    @for($i=1;$i<count($phone);$i++)
                                        <td>{{$phone[$i]['phone']}}</td>
                                    @endfor
                                </tr>
                            @endif
                        </table>
                    </div>
                    @if(isset($cv->personal->image))
                        <div class="pb-sm-0 pb-3 text-center">
                            <img src="{{Storage::disk('')->url($cv->personal->image)}}"
                                 alt=""
                                 class="cv-img">
                        </div>
                    @endif
                </div>

            </div>
        @endif
        @if(isset($cv->objective))
            <div class="info-section">
                <h3 class="cv-title"><img width='22px' src="/img/icon.svg" alt=""
                                          class="mr-2">{{trans('resume.personal_summary')}}</h3>
                <div>
                    {{$cv->objective}}
                </div>
            </div>
        @endif
        @if(count($cv->education)!=0)
            <div class="info-section">
                <h3 class="cv-title"><img width='22px' src="/img/college-graduation.svg" alt=""
                                          class="mr-2">{{trans('resume.background_education')}}</h3>
                @foreach($cv->education as $item)
                    <div class="d-flex justify-content-between pb-2 flex-sm-row flex-column-reverse">
                        <div>
                            <div class="mr-2 bold-text">
                                {{\App\Cv::educationName($item->name,trans('cv_education.'.$item->type))}}
                                , {{$item->department}}
                                ,
                                {{trans('cv_education.'.$item->degree)}}
                            </div>
                            <div class="mr-2">{{trans('category.'.$item->study_area)}}, ({{$item->major_field}})</div>
                        </div>
                        <div class="white-space text-sm-right">
                            <div>{{$item->city}}, {{trans('countries.'. $item->country->code)}}</div>
                            <div>

                                {{Carbon\Carbon::parse($item->period_from.'-02')->format('Y/m')}} -
                                {{Carbon\Carbon::parse($item->period_to.'-02')->format('Y/m')}}

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
        @if(count($cv->language)!=0)
            <div class="info-section">
                <h3 class="cv-title">
                    <img width='22px' src="/img/language.svg" alt="" class="mr-2"> {{trans('resume.language_skills')}}
                </h3>
                <div class="table-responsive p-0">
                    <table class="language-table w-100">
                        <tr>
                            <th>{{trans('resume.language')}}</th>
                            <th>{{trans('resume.reading')}}</th>
                            <th>{{trans('resume.writing')}}</th>
                            <th>{{trans('resume.communication')}}</th>
                            <th>{{trans('resume.typing')}}</th>
                            <th>{{trans('resume.shorthand')}}</th>
                            <th>{{trans('resume.total_study')}}</th>
                        </tr>
                        @foreach($cv->language as $item)
                            <tr>
                                <td>{{trans('languages.'.$item->language->name)}}
                                    ({{trans('resume.'.$item->language_type)}})
                                </td>
                                <td>{{trans('cv_language_skills.'.$item->reading)}}</td>
                                <td>{{trans('cv_language_skills.'.$item->writing)}}</td>
                                <td>{{trans('cv_language_skills.'.$item->communication)}}</td>
                                <td>{{$item->typing}}</td>
                                <td>{{$item->shorthand}}</td>
                                <td>{{trans('cv_language_skills.'.$item->total_study)}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        @endif

        @if(count($cv->experience)!=0)
            <div class="info-section">
                <h3 class="cv-title"><img width='22px' src="/img/user-experience.svg" alt=""
                                          class="mr-2">{{trans('resume.professional_experience')}}</h3>
                @foreach($cv->experience as $item)
                    <div class="d-flex justify-content-between pb-2 flex-sm-row flex-column-reverse">
                        <div>
                            <div class="mr-2 bold-text">{{$item->name}}</div>
                            <div class="mr-2">{{$item->position}}, {{trans('category.'.$item->position_field)}}
                            </div>
                        </div>
                        <div class="text-sm-right">
                            <div class="white-space">{{$item->city}}, {{trans('countries.'.$item->country->code)}}</div>
                            <div class="white-space">
                                {{Carbon\Carbon::parse($item->period_from.'-02')->format('Y/m')}} -

                                @if($item->period_to=='-')
                                    {{trans('resume.till_now')}}
                                @else
                                    {{Carbon\Carbon::parse($item->period_to.'-02')->format('Y/m')}}
                                @endif

                            </div>
                        </div>
                    </div>
                    @if(isset($item->main_duties))
                        <div class="pt-3 pb-2">
                            <i>{{trans('resume.main_duties')}}: ✓</i>
                            <ul>
                                <li>{{$item->main_duties}}</li>
                            </ul>
                        </div>
                    @endif
                    @if(isset($item->job_description))
                        <div class="pt-3 pb-2">
                            <i>{{trans('resume.job_description')}}: ✓</i>
                            <ul>
                                <li>  {{$item->job_description}}</li>
                            </ul>

                        </div>
                    @endif

                    @if(isset($item->salary))
                        <div class="pt-3 pb-1">
                            <i>{{trans('resume.salary')}}: </i>
                            <span>
                                {{$item->salary}} {{trans('currency.'.$item->currency)}} {{trans('currency.'.$item->period)}}
                            </span>

                        </div>
                    @endif
                    @if(isset($item->supervisor_name))
                        <div class="pt-3 pb-1">
                            <i>{{trans('resume.supervisor_name')}}: </i>
                            <span>
                                {{$item->supervisor_name}}
                            </span>

                        </div>
                    @endif
                    @if(isset($item->employer_numbers))
                        <div class="pt-3 pb-1">
                            <i>{{trans('resume.employer_numbers')}}: </i>
                            <span>
                                {{$item->employer_numbers}}
                            </span>
                        </div>
                    @endif

                    @if(isset($item->reason))
                        <div class="pt-3 pb-4">
                            <i>{{trans('resume.reason')}}: </i>
                            <span>
                                {{$item->reason}}
                            </span>
                        </div>
                    @endif

                @endforeach

            </div>
        @endif

        @if(count($cv->training)!=0)
            <div class="info-section">
                <h3 class="cv-title"><img width='22px' src="/img/seo-training.svg" alt=""
                                          class="mr-2">{{trans('resume.short_term_training')}}</h3>
                @foreach($cv->training as $item)
                    <div class="d-flex justify-content-between pb-2 flex-sm-row flex-column-reverse">
                        <div>
                            <div class="mr-2 bold-text">{{$item->name}} {{trans('cv_training.'.$item->type)}}</div>
                            <div class="mr-2">{{$item->course_name}}, {{trans('cv_training.'.$item->certificate_type)}}
                                ({{trans('category.'.$item->study_area)}})
                            </div>
                        </div>
                        <div class="text-sm-right">
                            <div class="white-space">{{$item->city}}, {{trans('countries.'.$item->country->code)}}</div>
                            <div class="white-space">
                                {{Carbon\Carbon::parse($item->period_from.'-02')->format('Y/m')}} -
                                {{Carbon\Carbon::parse($item->period_to.'-02')->format('Y/m')}}
                            </div>
                        </div>
                    </div>

                @endforeach

            </div>
        @endif

        @if(count($cv->visa)!=0)
            <div class="info-section">
                <h3 class="cv-title"><img width='22px' src="/img/passport.svg" alt=""
                                          class="mr-2">{{trans('resume.visa')}}</h3>
                @foreach($cv->visa as $item)
                    <div class="d-flex justify-content-between pb-2 flex-sm-row flex-column-reverse">
                        <div>
                            <div class="mr-2 bold-text">{{trans('countries.'.$item->country->code)}}
                                , {{$item->type}} </div>
                            <div class="mr-2">
                                {{Carbon\Carbon::parse($item->period_from.'-02')->format('Y/m')}} -
                                {{Carbon\Carbon::parse($item->period_to.'-02')->format('Y/m')}}
                            </div>
                        </div>

                    </div>

                @endforeach

            </div>
        @endif

        @if(count($cv->reference)!=0)
            <div class="info-section">
                <h3 class="cv-title"><img width='22px' src="/img/refer.svg" alt=""
                                          class="mr-2">{{trans('resume.references')}}</h3>
                @foreach($cv->reference as $item)
                    <div class="d-flex justify-content-between pb-2 flex-sm-row flex-column-reverse">
                        <div>
                            <div class="mr-2 bold-text">{{trans('title.'.$item->title)}} {{$item->full_name}}</div>
                            <div class="mr-2">{{$item->position}}, {{trans('cv_reference.'.$item->relationship)}}
                            </div>
                        </div>
                    </div>
                    @if(isset($item->current_position))
                        <div class="pt-3 pb-1">
                            <i>{{trans('resume.current_position')}}: </i>
                            <span>
                                {{$item->current_position}}
                            </span>
                        </div>
                    @endif
                    @if(isset($item->email))
                        <div class="pt-3 pb-1">
                            <i>{{trans('resume.email')}}: </i>
                            <span>
                                {{$item->email}}
                            </span>
                        </div>
                    @endif
                    @if(isset($item->phone))
                        <div class="pt-3 pb-1">
                            <i>{{trans('resume.phone')}}: </i>

                            <span>{{$item->phone}}</span>

                        </div>
                    @endif
                    @if(isset($item->duration))
                        <div class="pt-3 pb-4">
                            <i>{{trans('resume.duration')}}: </i>

                            <span>{{$item->duration}} {{trans('cv_reference.'.$item->duration_type)}}</span>

                        </div>
                    @endif
                @endforeach

            </div>
        @endif

        @if(isset($cv->volunteering))
            <div class="info-section">
                <h3 class="cv-title"><img width='22px' src="/img/volunteer.svg" alt=""
                                          class="mr-2">{{trans('resume.volunteering')}}</h3>
                <div class="pt-3 pb-1">
                    <i>{{trans('resume.willing_to_volunteer')}} : </i>
                    <span>
                        {{trans('resume.'.$cv->volunteering->willing)}}
                    </span>

                </div>
                @if($cv->volunteering->willing=='yes')
                    <div class="pt-3 pb-1">
                        <i>{{trans('resume.public_woks')}} : </i>
                        <span>
                            {{trans('resume.'.$cv->volunteering->public_work)}}
                        </span>

                    </div>
                @endif
                @if(isset($work_type))
                    <div class="pt-3 pb-1">
                        <i>{{trans('resume.work_type')}} : </i>
                        <ul class="mb-0">
                            @foreach(config('enums.work_type') as $item)
                                @if(in_array($item, $work_type))
                                    <li>
                                        {{trans('cv_volunteering.'.$item)}}
                                    </li>
                                @endif
                            @endforeach
                        </ul>

                    </div>
                @endif
                @if(isset($volunteering_countries))
                    <div class="pt-3 pb-1">
                        <i>{{trans('resume.countries')}} : </i>
                        <ul>
                            @foreach($countries as $item)
                                @if(in_array($item->id, $volunteering_countries))
                                    <li>
                                        {{trans('countries.'.$item->code)}}
                                    </li>
                                @endif
                            @endforeach
                        </ul>

                    </div>
                @endif

            </div>
        @endif
        @if(isset($cv->computer_skills))
            <div class="info-section">
                @if(isset($cv->computer_skills->level) ||  isset($cv->computer_skills->software) || isset($cv->computer_skills->hardware))
                    <h3 class="cv-title"><img width='22px' src="/img/monitor.svg" alt=""
                                              class="mr-2">{{trans('resume.computer_skills')}}</h3>
                @endif
                @if(isset($cv->computer_skills->level))
                    <div class="pt-3 pb-1">
                        <i>{{trans('resume.level')}}: </i>
                        <span>
                            {{trans('cv_computer_skills.'.$cv->computer_skills->level)}}
                        </span>
                    </div>
                @endif

                @if(isset($cv->computer_skills->software))
                    <div class="pt-3 pb-1">
                        <i>{{trans('resume.software')}}: </i>
                        <span>
                           {{$cv->computer_skills->software}}
                      </span>
                    </div>
                @endif
                @if(isset($cv->computer_skills->hardware))
                    <div class="pt-3 pb-1">
                        <i>{{trans('resume.hardware')}}: </i>

                        <span>{{$cv->computer_skills->hardware}}</span>

                    </div>
                @endif
            </div>
        @endif
        @if(isset($cv->availability))
            <div class="info-section">
                <h3 class="cv-title"><img width='22px' src="/img/hand.svg" alt=""
                                          class="mr-2">{{trans('resume.availability')}}</h3>

                <div class="pt-3 pb-2">
                    <i>{{trans('resume.availability')}}: </i>
                    <ul class="mb-0">
                        @if(isset($cv->availability->period_from) && isset($cv->availability->period_to))
                            <li>
                                {{Carbon\Carbon::parse($cv->availability->period_from)->format('d.m.Y')}}
                                -{{Carbon\Carbon::parse($cv->availability->period_to)->format('d.m.Y')}}
                            </li>
                        @endif
                        @if(isset($cv->availability->hours_from) && $cv->availability->hours_from )
                            <li>
                                {{$cv->availability->hours_from}} - {{$cv->availability->hours_to}}
                            </li>
                        @endif

                        @if(isset($availability_days))
                            <li>
                                @foreach(config('enums.days') as $item)
                                    @if(in_array($item,$availability_days))
                                        {{trans('cv_availability.'.$item)}}
                                    @endif
                                @endforeach
                            </li>
                        @endif
                    </ul>
                </div>
                @if(isset($cv->availability->short_salary))
                    <div class="pt-3 pb-1">
                        <i>{{trans('resume.short_term_salary')}}: </i>
                        <span>
                            {{$cv->availability->short_salary}} {{trans('currency.'.$cv->availability->short_currency)}} {{trans('currency.'.$cv->availability->short_period)}}
                        </span>
                    </div>
                @endif
                @if(isset($cv->availability->long_salary))
                    <div class="pt-3 pb-1">
                        <i>{{trans('resume.long_term_salary')}}: </i>
                        <span>
                            {{$cv->availability->long_salary}} {{trans('currency.'.$cv->availability->long_currency)}} {{trans('currency.'.$cv->availability->long_period)}}
                        </span>
                    </div>
                @endif
                @if(isset($availability_terms))
                    <div class="pt-3 pb-4">
                        <i>{{trans('resume.terms')}}: </i>
                        <ul>

                            <li>
                                @foreach(config('enums.terms') as $item)
                                    @if(in_array($item,$availability_terms))
                                        {{trans('cv_availability.'.$item)}}
                                    @endif
                                @endforeach
                            </li>
                        </ul>
                    </div>
                @endif


            </div>
        @endif

        @if(isset($cv->special_skills))
            <div class="info-section">
                <h3 class="cv-title"><img width='22px' src="/img/skills.svg" alt=""
                                          class="mr-2">{{trans('resume.special_skills')}}</h3>
                <ul>
                    <li>{{$cv->special_skills}}</li>
                </ul>
            </div>
        @endif
        @if(isset($cv->questions))
            <div class="info-section">
                <h3 class="cv-title"><img width='22px' src="/img/question.svg" alt=""
                                          class="mr-2">{{trans('resume.questions')}}</h3>
                <div class="pt-3 pb-1">
                    <i>{{trans('cv_questions.make_inquiries')}} {{trans('cv_questions.present_employer')}}: </i>
                    <span>
                        {{trans('resume.'.$cv->questions->present_employer)}}
                    </span>
                </div>

                <div class="pt-3 pb-1">
                    <i>{{trans('cv_questions.make_inquiries')}} {{trans('cv_questions.previous_employer')}}: </i>
                    <span>
                        {{trans('resume.'.$cv->questions->previous_employer)}}
                    </span>
                </div>
                @if(isset($cv->questions->government))
                    <div class="pt-3 pb-1">
                        <i>{{trans('cv_questions.government')}}: </i>
                        <span>
                           {{$cv->questions->government}}
                        </span>
                    </div>
                @endif
                @if(isset($cv->questions->arrested))
                    <div class="pt-3 pb-1">
                        <i>{{trans('cv_questions.arrested')}}: </i>
                        <span>
                            {{trans('resume.'.$cv->questions->arrested)}}
                        </span>
                    </div>
                @endif

                @if(isset($cv->questions->arrested_details))
                    <div class="pt-3 pb-1">
                        <i>{{trans('cv_questions.arrested_details')}}: </i>
                        <div>
                            {{trans('resume.'.$cv->questions->arrested_details)}}
                        </div>
                    </div>
                @endif
            </div>
        @endif

        @if(count($cv->attachment)!=0)
            <div class="info-section">
                <h3 class="cv-title"><img width='22px' src="/img/clip.svg" alt=""
                                          class="mr-2">{{trans('resume.attachments')}}</h3>
                @foreach($cv->attachment as $item)
                    <div class="pt-3 pb-2">
                        @if($item->is_image==1)
                            <a href="{{Storage::disk('')->url($item->attachment)}}" class="jobs-img">
                                <img width='22px' data-src="{{Storage::disk('')->url($item->attachment)}}" alt=""
                                     class="lazyload">
                            </a>
                        @else
                            <p><a href="{{Storage::disk('')->url($item->attachment)}}"
                                  download>{{$item->description}}</a>
                            </p>
                        @endif

                        @endforeach

                    </div>

                    @endif


                    <div class="print" style="padding:0 10px">
                        <div>
                            <span class="bold-text">{{trans('resume.added_by')}}: </span> {{$cv->user->username}}
                        </div>
                        <div>
                            <span class="bold-text">{{trans('resume.on')}}: </span>{{$cv->created_at->format('d.m.Y')}}
                        </div>
                    </div>
            </div>
    </section>
@endsection