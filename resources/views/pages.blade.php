@extends('layouts.app')
@section('css')
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
@endsection
@section('content')

    <section class="static-page">
        <div class="static-container">
            <h1 class="static-date">{{$page->data->title}}<span>({{trans('pages.last_updated')}}: {{\Carbon\Carbon::parse($page->data->updated_at)->format('Y-m-d H:i')}})</span></h1>
            {!! $page->data->content !!}
        </div>

    </section>

@endsection