@extends('layouts.app')
@section('css')

    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
@endsection
@section('content')

    <section class="blog-page-banner d-flex w-100">
        <div class="position-relative blog">
            @if(isset($first))
                <a href="{{route('blog-single',$first->slug)}}">
                    <img src="{{Storage::disk('')->url($first->image)}}" alt=""
                         class="w-100 img-fluid h-100 object-fit-cover">
                    <div class="hover-gradient">
                        <div class="d-flex justify-content-between align-items-end">
                            <div>
                                <div class="title">{{$first->data->title}}</div>
                                <span class="date">{{$first->created_at->format('M d, Y')}}</span>
                            </div>
                        </div>
                    </div>
                </a>
            @endif
        </div>

    </section>
    <section class="blog-boxes w-100">

        <div class="d-flex flex-wrap blog-boxes-content">
            @foreach($blogs as $item)
            <div class="blog-box">
                <a href="{{route('blog-single',$item->slug)}}">
                    <img src="{{Storage::disk('')->url($item->image)}}" alt="blogs" class="img-fluid blog-box-img">
                    <div class="blog-box-text">
                        <div class="blog-box-padding">
                            <h6 class="title">{{$item->data->title}}</h6>
                            <span class="date">{{$item->created_at->format('M d, Y')}}</span>
                            <div class="blog-text">
                               {{$item->data->short_description}}
                            </div>
                        </div>
                        <a href="{{route('blog-single',$item->slug)}}" class="blog-link"> {{trans('blogs.read_more')}}</a>
                    </div>
                </a>
            </div>

            @endforeach
                {{$blogs->links()}}
        </div>

    </section>
@endsection
