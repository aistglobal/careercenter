<div class="d-flex align-items-start justify-content-between job-right-title">
    <h1 class="font-bold mb-0">{{$first->data->title}}</h1>

        <span class="d-flex align-items-center job-view-count" flow="left" tooltip="{{trans('announcements.views_text')}}  {{$first->views}} {{trans('announcements.times')}}">
            <img src="/img/eye.svg" alt="" width="20">
        </span>

    <div class="d-flex justify-content-center flex-column">

        @if(($buttons && $first->user_id==Auth::id()) || ($buttons && \Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement')))
            @if($first->online_submission == 1)
                <button style="cursor: no-drop" disabled type="button"
                        class="btn btn-orange-gradient">{{trans('announcements.apply_now')}}
                </button>
            @endif
            <div class="btn-group drop-group-block drop-group">
                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                    {{trans('announcements.actions')}}
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    @if($first->status == 'preview')
                        {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['after-preview', $first->id]]) !!}
                        {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.save_as_draft'), array(
                                                           'type' => 'submit',
                                                           'class' => 'dropdown-item',
                                                           'title' => trans('announcements.post'),
                                                           )) !!}
                        <input type="hidden" name="action" value="save_as_draft">
                        {!! Form::close() !!}

                        @if(isset($mirror))
                            @if($mirror->status == 'pending')
                                @permission('post_announcement')
                                {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['after-preview', $first->id]]) !!}
                                {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.post'), array(
                                                                   'type' => 'submit',
                                                                   'class' => 'dropdown-item',
                                                                   'title' => trans('announcements.post'),
                                                                   )) !!}
                                <input type="hidden" name="action" value="post">
                                {!! Form::close() !!}
                                @endpermission
                            @endif
                            @if($mirror->status == 'draft')
                                @permission('post_announcement')
                                {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['after-preview', $first->id]]) !!}
                                {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.post'), array(
                                                                   'type' => 'submit',
                                                                   'class' => 'dropdown-item',
                                                                   'title' => trans('announcements.post'),
                                                                   )) !!}
                                <input type="hidden" name="action" value="post">
                                {!! Form::close() !!}
                                @endpermission
                            @endif

                            @if($mirror->status == 'expired')
                                @if(\Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement'))
                                    @permission('post_announcement')
                                    {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['after-preview', $first->id]]) !!}
                                    {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.post'), array(
                                                                       'type' => 'submit',
                                                                       'class' => 'dropdown-item',
                                                                       'title' => trans('announcements.post'),
                                                                       )) !!}
                                    <input type="hidden" name="action" value="post">
                                    {!! Form::close() !!}
                                    @endpermission
                                @endif
                            @endif
                        @endif
                        @permission('edit_announcement')
                        <a title="{{trans('profile_tables.edit')}}" class="dropdown-item"
                           href="{{route('announcements-edit',$first->id)}}"><img
                                    src="/img/pencil-edit.svg"
                                    alt=""
                                    height="20">{{trans('profile_tables.edit')}}
                        </a>
                        @endpermission
                        @permission('postEmail_announcement')
                        {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['after-preview', $first->id]]) !!}
                        {!! Form::button('<img src="/img/postandemail.svg">'.trans('announcements.post_and_email'), array(
                                                           'type' => 'submit',
                                                           'class' => 'dropdown-item',
                                                           'title' => trans('announcements.save_changes'),
                                                           )) !!}
                        <input type="hidden" name="action" value="post_and_email">
                        {!! Form::close() !!}
                        @endpermission

                        @if(\Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement'))
                            {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['after-preview', $first->id]]) !!}
                            {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.save_changes'), array(
                                                               'type' => 'submit',
                                                               'class' => 'dropdown-item',
                                                               'title' => trans('announcements.save_changes'),
                                                               )) !!}
                            <input type="hidden" name="action" value="save_changes">
                            {!! Form::close() !!}
                        @else
                            {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['after-preview', $first->id]]) !!}
                            {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.submit'), array(
                                                               'type' => 'submit',
                                                               'class' => 'dropdown-item',
                                                               'title' => trans('announcements.submit'),
                                                               )) !!}
                            <input type="hidden" name="action" value="submit">
                            {!! Form::close() !!}
                        @endif

                        {{--{!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['after-preview', $first->id]]) !!}--}}
                        {{--{!! Form::button('<img src="/img/delete-button.svg">'.trans('announcements.cancel'), array(--}}
                                                           {{--'type' => 'submit',--}}
                                                           {{--'class' => 'dropdown-item',--}}
                                                           {{--'title' => trans('announcements.cancel'),--}}
                                                           {{--)) !!}--}}
                        {{--<input type="hidden" name="action" value="cancel">--}}
                        {{--{!! Form::close() !!}--}}

                    @elseif($first->status == 'pending' || $first->status=='draft')

                        @permission('edit_announcement')
                        <a title="{{trans('profile_tables.edit')}}" class="dropdown-item"
                           href="{{route('announcements-edit',$first->id)}}"><img
                                    src="/img/pencil-edit.svg"
                                    alt=""
                                    height="20">{{trans('profile_tables.edit')}}
                        </a>
                        @endpermission

                        @if(\Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement'))

                            @permission('delete_announcement')
                            {!! Form::open([
        'method'=>'DELETE',
       'route' => ['job-destroy',$first->id],
       'style' => 'display:inline'
       ]) !!}
                            <input type="hidden" name="status" value="{{$job_status}}">
                            {!! Form::button('<img src="/img/delete-button.svg">'.trans('announcements.delete'), array(
                                                   'type' => 'submit',
                                                   'class' => 'dropdown-item',
                                                   'title' => trans('announcements.delete'),
                                                   'onclick'=>'return confirm("Confirm delete?")'
                                           )) !!}

                            {!! Form::close() !!}
                            @endpermission

                            @permission('post_announcement')
                            {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['status-change', $first->id]]) !!}
                            {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.post'), array(
                                                               'type' => 'submit',
                                                               'class' => 'dropdown-item',
                                                               'title' => trans('announcements.post'),
                                                               )) !!}
                            <input type="hidden" name="status" value="posted">
                            {!! Form::close() !!}
                            @endpermission

                            @permission('postEmail_announcement')
                            {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['status-change', $first->id]]) !!}
                            <input type="hidden" name="status" value="postandemail">
                            {!! Form::button('<img src="/img/postandemail.svg">'.trans('announcements.post_and_email'), array(
                     'type' => 'submit',
                     'class' => 'dropdown-item',
                     'title' => trans('announcements.post_and_email'),
                     )) !!}
                            {!! Form::close() !!}
                            @endpermission

                            @permission('reject_announcement')
                            {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['status-change', $first->id]]) !!}
                            <input type="hidden" name="status" value="rejected">
                            {!! Form::button('<img src="/img/reject.svg">'.trans('announcements.reject'), array(
      'type' => 'submit',
      'class' => 'dropdown-item',
      'title' => trans('announcements.reject'),
      )) !!}
                            {!! Form::close() !!}
                            @endpermission

                        @else

                            @if($first->status == 'draft')
                                @permission('delete_announcement')
                                {!! Form::open([
            'method'=>'DELETE',
           'route' => ['job-destroy',$first->id],
           'style' => 'display:inline'
           ]) !!}
                                <input type="hidden" name="status" value="{{$job_status}}">
                                {!! Form::button('<img src="/img/delete-button.svg">'.trans('announcements.delete'), array(
                                                       'type' => 'submit',
                                                       'class' => 'dropdown-item',
                                                       'title' => trans('announcements.delete'),
                                                       'onclick'=>'return confirm("Confirm delete?")'
                                               )) !!}

                                {!! Form::close() !!}
                                @endpermission
                            @endif

                            @permission('submit_announcement')
                            {!! Form::open([
                            'method'=>'post',
                            'route' => ['jobs.submit',$first->id],
                            'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.submit'), array(
                            'type' => 'submit',
                            'class' => 'dropdown-item',
                            'title' => trans('announcements.submit'),
                            )) !!}
                            {!! Form::close() !!}
                            @endpermission

                        @endif
                    @elseif($first->status == 'posted')
                        @permission('postEmail_announcement')
                        {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['status-change', $first->id]]) !!}
                        <input type="hidden" name="status" value="postandemail">
                        {!! Form::button('<img src="/img/postandemail.svg">'.trans('announcements.post_and_email'), array(
                 'type' => 'submit',
                 'class' => 'dropdown-item',
                 'title' => trans('announcements.post_and_email'),
                 )) !!}
                        {!! Form::close() !!}
                        @endpermission
                        @permission('edit_announcement')
                        <a title="{{trans('profile_tables.edit')}}" class="dropdown-item"
                           href="{{route('announcements-edit',$first->id)}}"><img
                                    src="/img/pencil-edit.svg"
                                    alt=""
                                    height="20">{{trans('profile_tables.edit')}}
                        </a>
                        @endpermission
                        {{--@permission('delete_announcement')--}}
                        {{--{!! Form::open([--}}
                        {{--'method'=>'DELETE',--}}
                        {{--'route' => ['job-destroy',$first->id],--}}
                        {{--'style' => 'display:inline'--}}
                        {{--]) !!}--}}
                        {{--<input type="hidden" name="status" value="{{$job_status}}">--}}
                        {{--{!! Form::button('<img src="/img/delete-button.svg">'.trans('announcements.delete'), array(--}}
                                               {{--'type' => 'submit',--}}
                                               {{--'class' => 'dropdown-item',--}}
                                               {{--'title' => trans('announcements.delete'),--}}
                                               {{--'onclick'=>'return confirm("Confirm delete?")'--}}
                                       {{--)) !!}--}}

                        {{--{!! Form::close() !!}--}}
                        {{--@endpermission--}}

                            @if(Auth::user()->can('accessAll_announcement'))
                                @permission('expired_announcement')
                                {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['status-change', $first->id]]) !!}
                                <input type="hidden" name="status" value="expired">
                                {!! Form::button('<img src="/img/time.svg">'.trans('announcements.expired'), array(
         'type' => 'submit',
         'class' => 'dropdown-item',
         'title' => trans('announcements.expired'),
         )) !!}
                                {!! Form::close() !!}
                                @endpermission
                            @endif


                        @permission('clone_announcement')
                        {!! Form::open([
              'method'=>'post',
              'route' => ['jobs.clone',$first->id],
              'style' => 'display:inline'
              ]) !!}
                        {!! Form::button('<img src="/img/copy.svg">'.trans('announcements.clone'), array(
                        'type' => 'submit',
                        'class' => 'dropdown-item',
                        'title' => trans('announcements.copy'),
                        )) !!}

                        {!! Form::close() !!}
                        @endpermission

                    @elseif($first->status=='expired')

                            @if(\Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement'))
                                {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['status-change', $first->id]]) !!}
                                {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.post'), array(
                                                                   'type' => 'submit',
                                                                   'class' => 'dropdown-item',
                                                                   'title' => trans('announcements.post'),
                                                                   )) !!}
                                <input type="hidden" name="status" value="posted">
                                {!! Form::close() !!}
                            @endif
                       
                        @permission('edit_announcement')
                        <a title="{{trans('profile_tables.edit')}}" class="dropdown-item"
                           href="{{route('announcements-edit',$first->id)}}"><img
                                    src="/img/pencil-edit.svg"
                                    alt=""
                                    height="20">{{trans('profile_tables.edit')}}
                        </a>
                        @endpermission
                        @permission('delete_announcement')
                        {!! Form::open([
                        'method'=>'DELETE',
                        'route' => ['job-destroy',$first->id],
                        'style' => 'display:inline'
                        ]) !!}
                        <input type="hidden" name="status" value="{{$job_status}}">
                        {!! Form::button('<img src="/img/delete-button.svg">'.trans('announcements.delete'), array(
                                               'type' => 'submit',
                                               'class' => 'dropdown-item',
                                               'title' => trans('announcements.delete'),
                                               'onclick'=>'return confirm("Confirm delete?")'
                                       )) !!}

                        {!! Form::close() !!}
                        @endpermission
                        @permission('clone_announcement')
                        {!! Form::open([
                                          'method'=>'post',
                                          'route' => ['jobs.clone',$first->id],
                                          'style' => 'display:inline'
                                          ]) !!}
                        {!! Form::button('<img src="/img/copy.svg">'.trans('announcements.clone'), array(
                        'type' => 'submit',
                        'class' => 'dropdown-item',
                        'title' => trans('announcements.copy'),
                        )) !!}

                        {!! Form::close() !!}
                        @endpermission

                    @elseif($first->status=='rejected')
                        @permission('edit_announcement')
                        <a title="{{trans('profile_tables.edit')}}" class="dropdown-item"
                           href="{{route('announcements-edit',$first->id)}}"><img
                                    src="/img/pencil-edit.svg"
                                    alt=""
                                    height="20">{{trans('profile_tables.edit')}}
                        </a>
                        @endpermission
                        @permission('delete_announcement')
                        {!! Form::open([
                        'method'=>'DELETE',
                        'route' => ['job-destroy',$first->id],
                        'style' => 'display:inline'
                        ]) !!}
                        <input type="hidden" name="status" value="{{$job_status}}">
                        {!! Form::button('<img src="/img/delete-button.svg">'.trans('announcements.delete'), array(
                                               'type' => 'submit',
                                               'class' => 'dropdown-item',
                                               'title' => trans('announcements.delete'),
                                               'onclick'=>'return confirm("Confirm delete?")'
                                       )) !!}

                        {!! Form::close() !!}
                        @endpermission
                        @permission('clone_announcement')
                        {!! Form::open([
                                 'method'=>'post',
                                 'route' => ['jobs.clone',$first->id],
                                 'style' => 'display:inline'
                                 ]) !!}
                        {!! Form::button('<img src="/img/copy.svg">'.trans('announcements.clone'), array(
                        'type' => 'submit',
                        'class' => 'dropdown-item',
                        'title' => trans('announcements.copy'),
                        )) !!}

                        {!! Form::close() !!}
                        @endpermission
                    @endif
                </div>

            </div>
        @elseif(Auth::check() &&  Auth::user()->is_organization!=1 && $first->online_submission==1)
            @if($applied_job=="" )
                <a data-id="{{$first->id}}"
                   class="btn btn-orange-gradient apply">{{trans('announcements.apply_now')}}</a>
            @elseif($applied_job =='rstat_Incomplete')
                <a data-id="{{$first->id}}"
                   class="btn btn-orange-gradient apply">{{trans('announcements.apply_now')}}</a>

                <span
                        class="btn btn-orange-gradient mt-2">{{trans('appliers.'.$applied_job)}}&nbsp
                    {{$applied_job_date}}
                                </span>

            @else
                <span class="btn btn-orange-gradient ">{{trans('appliers.'.$applied_job)}}&nbsp
                    {{$applied_job_date}}
                                </span>

            @endif
        @elseif(!Auth::check() && $first->online_submission==1)
            <a class="btn btn-orange-gradient" href="{{route('applyJob',$first->slug)}}">{{trans('announcements.apply_now')}}</a>
            {{--<form action="{{route('applyJob')}}" method="POST">--}}
                {{--@csrf--}}
                {{--<input type="hidden" name="slug" value="{{$first->slug}}">--}}
                {{--<button data-id="{{$first->id}}" type="submit"--}}
                        {{--class="btn btn-orange-gradient">{{trans('announcements.apply_now')}}</button>--}}
            {{--</form>--}}
        @endif
    </div>
</div>
<div class="table-responsive d-flex justify-content-between align-items-start pr-4">
    <table class="table">
        <tr>
            <th>{{trans('announcements.company')}}:</th>
            <td>
                @if(isset($first->user->organization) && $first->user->organization->slug)
                    <a href="{{route('companySingle',$first->user->organization->slug)}}">
                        {{ \App\Job::companyName($first->user->organization,$first->data->organization)}}
                        @if(isset($first->user->organization->legal_structure) && $first->user->organization->legal_structure!='other' && $first->user->organization->legal_structure!='brand')
                            @if(\Lang::has('legal_structure.'.$first->user->organization->legal_structure))
                                {{trans('legal_structure.'.$first->user->organization->legal_structure)}}
                            @else
                                {{$first->user->organization->legal_structure}}
                            @endif

                        @endif
                    </a>
                @else
                    {{$first->data->organization}}
                @endif
            </td>
        </tr>
        @if(isset($first->data->location_city) && isset($country))
            <tr>
                <th>{{trans('announcements.location')}}:</th>
                <td>{{$first->data->location_city}},&nbsp{{trans('countries.'.$country->code)}}</td>
            </tr>
        @endif
        @if(isset($first->data->duration) && $first->data->duration!='')
            <tr>
                <th>{{trans('announcements.duration')}}:</th>
                <td>{{$first->data->duration}}</td>
            </tr>
        @endif
        @if(isset($first->data->start_date) && $first->data->start_date!='')
            <tr>
                <th>{{trans('announcements.start_date')}}:</th>
                <td>{{$first->data->start_date}}</td>
            </tr>
        @endif
        {{--@if(isset($first->data->opening_date) && $first->data->opening_date!='')--}}
        {{--<tr>--}}
        {{--<th>{{trans('announcements.opening_date')}}:</th>--}}
        {{--<td> {{$first->data->opening_date}}</td>--}}
        {{--</tr>--}}
        {{--@endif--}}
        @if(isset($first->data->deadline) && $first->data->deadline!='')
            <tr>
                <th>{{trans('announcements.deadline')}}:</th>
                <td style="white-space: nowrap;">{{$first->data->deadline}}</td>
            </tr>
        @endif

    </table>
    @if(isset($first->user->image) && $first->user->image)
        <picture>
            {{--<source data-srcset="/img/job-1.webp" type="image/webp">--}}
            <source srcset="{{Storage::disk('')->url($first->user->image)}}" type="image/jpeg">
            <img src="{{Storage::disk('')->url($first->user->image)}}" class="job-logo">
        </picture>
    @endif

</div>
@if($first->status=='posted')
    <div class="job-share-this d-flex align-items-center">
        <p class="font-bold">{{trans('announcements.share_this_page')}}</p>
        <div class="job-share-links">
            <a href="http://www.facebook.com/sharer.php?u={{route('jobs',$first->slug)}}"
               target="_blank"
               rel="noreferrer" aria-hidden="true">
                <svg xmlns="http://www.w3.org/2000/svg" width="10.032" height="20.064"
                     viewBox="0 0 10.032 20.064">
                    <g transform="translate(0 0)">
                        <path d="M335.843,7259.064v-9.029h2.741l.448-4.013h-3.189v-1.954c0-1.034.026-2.06,1.47-2.06h1.462v-2.869a16.4,16.4,0,0,0-2.527-.14c-2.654,0-4.316,1.662-4.316,4.715v2.308H329v4.013h2.933v9.029Z"
                              transform="translate(-329 -7239)" fill="#fff" fill-rule="evenodd"/>
                    </g>
                </svg>
            </a>
            <a href="https://twitter.com/share?url={{route('jobs',$first->slug)}}" target="_blank"
               rel="noreferrer" aria-hidden="true">
                <svg xmlns="http://www.w3.org/2000/svg" width="24.598" height="19.679"
                     viewBox="0 0 24.598 19.679">
                    <g transform="translate(0 0)">
                        <path d="M11.736,7380.679a14.147,14.147,0,0,0,14.359-14.139c0-.215,0-.43-.015-.643a10.215,10.215,0,0,0,2.518-2.57,10.193,10.193,0,0,1-2.9.781,5.009,5.009,0,0,0,2.219-2.75,10.15,10.15,0,0,1-3.205,1.208,5.108,5.108,0,0,0-7.141-.216,4.925,4.925,0,0,0-1.46,4.747,14.41,14.41,0,0,1-10.4-5.19,4.925,4.925,0,0,0,1.563,6.631,5.052,5.052,0,0,1-2.291-.62v.062a4.989,4.989,0,0,0,4.049,4.87,5.071,5.071,0,0,1-2.278.086,5.044,5.044,0,0,0,4.714,3.452,10.228,10.228,0,0,1-6.267,2.13,10.533,10.533,0,0,1-1.2-.071,14.452,14.452,0,0,0,7.736,2.229"
                              transform="translate(-4 -7361)" fill="#fff" fill-rule="evenodd"/>
                    </g>
                </svg>
            </a>
        </div>
    </div>
@endif
<ul class="nav nav-pills mb-3 job-tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="job-tab" data-toggle="pill" href="#job" role="tab"
           aria-controls="job" aria-selected="true">{{trans('types.'.$first->type->key)}}</a>
    </li>
    @if((isset($first->data->about_company) && $first->data->about_company!='') || (Auth::check() && Auth::user()->can('accessAll_announcement')))
        <li class="nav-item">
            <a class="nav-link" id="company-tab" data-toggle="pill" href="#company" role="tab"
               aria-controls="company" aria-selected="false">{{trans('announcements.company')}}</a>
        </li>
    @endif
</ul>
<div class="tab-content job-tab-content">
    <div class="tab-pane fade show active" id="job" role="tabpanel" aria-labelledby="job-tab">
        @if(isset($first->data->term) && $first->data->term!='')
            @if($first->type->key =='job_opp' || $first->type->key =='volunteering' || $first->type->key=='internship')
                <h4 class="font-bold no-wrap @if(!\App\Job::is_enumeration($first->data->term)) not-dash @endif">{{trans('announcements.term')}}:&nbsp</h4>
            @else
                <h4 class="font-bold no-wrap @if(!\App\Job::is_enumeration($first->data->term)) not-dash @endif">{{trans('announcements.'.$first->type->key.'_type')}}
                    :&nbsp</h4>
            @endif
            {{$first->data->term}}
            <div class="clearfix mb-2"></div>
        @endif
            @if($first->type->key =='job_opp' || $first->type->key =='volunteering' || $first->type->key=='internship')
                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->description)) not-dash @endif">{{trans('announcements.description')}}:&nbsp</h4>
            @elseif($first->type->key =='news')
                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->description)) not-dash @endif">{{trans('announcements.news_details')}}:&nbsp</h4>
            @else
                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->description)) not-dash @endif">{{trans('announcements.detail_description')}}:&nbsp</h4>
            @endif

            {!!\App\Job::emailize($first->data->description)!!}
        <div class="clearfix mb-2"></div>

        @if(isset($first->data->responsibilities) && $first->data->responsibilities!='')
            @if($first->type->key =='job_opp' ||$first->type->key =='volunteering' || $first->type->key=='internship')
                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->responsibilities)) not-dash @endif">{{trans('announcements.responsibilities')}}:&nbsp</h4>
            @else
                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->responsibilities)) not-dash @endif">{{trans('announcements.educational_level')}}:&nbsp</h4>
            @endif
            {!! \App\Job::emailize($first->data->responsibilities) !!}
            <div class="clearfix mb-2"></div>
        @endif
        @if(isset($first->data->requirements) && $first->data->requirements!='')
            @if($first->type->key =='job_opp' || $first->type->key =='volunteering' || $first->type->key=='internship')
                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->requirements)) not-dash @endif">{{trans('announcements.required_qualifications')}}:&nbsp</h4>
            @else
                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->requirements)) not-dash @endif">{{trans('announcements.requirements')}}:&nbsp</h4>
            @endif
            {!! \App\Job::emailize($first->data->requirements) !!}
            <div class="clearfix mb-2"></div>
        @endif
        @if(isset($first->data->procedures) && $first->data->procedures!='')
            <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->procedures)) not-dash @endif">{{trans('announcements.app_procedures')}}:&nbsp</h4>
                {!!\App\Job::procedures(\App\Job::emailize($first->data->procedures),$first,$applied_job)!!}
            {{--{!! \App\Job::emailize($first->data->procedures) !!}--}}
            <p>{!!trans('announcements.application_procedures_default_text') !!}</p>
            <div class="clearfix mb-2"></div>
        @endif
        @if(isset($first->data->salary) && $first->data->salary!='')
            <h4 class="font-bold no-wrap @if(!\App\Job::is_enumeration($first->data->salary)) not-dash @endif">{{trans('announcements.salary')}}:&nbsp</h4>
            {{$first->data->salary}}
            <div class="clearfix mb-2"></div>
        @endif
        @if(isset($first->data->open) && $first->data->open!='')
            <h4 class="font-bold no-wrap @if(!\App\Job::is_enumeration($first->data->open)) not-dash @endif">{{trans('announcements.open')}}:&nbsp</h4>
            {{$first->data->open}}
            <div class="clearfix mb-2"></div>
        @endif
        @if(isset($first->data->audience) && $first->data->audience!='')
            <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->audience)) not-dash @endif">{{trans('announcements.audience')}}:&nbsp</h4>
            {{$first->data->audience}}
            <div class="clearfix mb-2"></div>
        @endif
        @if(isset($first->data->notes) && $first->data->notes!='' )
            <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->notes)) not-dash @endif">{{trans('announcements.notes')}}:&nbsp</h4>
            {!!\App\Job::emailize($first->data->notes)!!}
            <div class="clearfix mb-2"></div>
        @endif
        @if(isset($first->data->about) && $first->data->about!='')
            <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->about)) not-dash @endif">{{trans('announcements.about')}}:&nbsp</h4>
            {!!\App\Job::emailize($first->data->about)!!}
            <div class="clearfix mb-2"></div>
        @endif
        @if(isset($first->data->author) && $first->data->author!='' )
            <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->author)) not-dash @endif">{{trans('announcements.author')}}:&nbsp</h4>
            {{$first->data->author}}
            <div class="clearfix mb-2"></div>
        @endif
        @if(isset($first->data->pages) && $first->data->pages!='' )
            <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->pages)) not-dash @endif">{{trans('announcements.pages')}}:&nbsp</h4>
            {{$first->data->pages}}
            <div class="clearfix mb-2"></div>
        @endif
        @if(isset($first->data->language) && $first->data->language!='')
            <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->language)) not-dash @endif">{{trans('announcements.language')}}:&nbsp</h4>
            {{$first->data->language}}
            <div class="clearfix mb-2"></div>
        @endif
        @if(count($first->attachment)!=0)
            <h4 class="font-bold">{{trans('announcements.attachments')}}:</h4>
            @foreach($first->attachment as $file)
                @if($file->image==1)
                    <br>
                    <a href="{{Storage::disk('')->url($file->file)}}" target="_blank"
                       class="jobs-img jobs-img-description">
                        <img src="{{Storage::disk('')->url($file->file)}}" alt="">
                        <span>{{$file->description}}</span>
                    </a>
                @else
                    <span class="jobs-img-file">
                                        <a href="{{Storage::disk('')->url($file->file)}}" download>
                                            <img src="/img/down-white.svg" alt="" width="17" height="17"
                                                 class="mr-2">{{$file->name}} - {{$file->description}}</a>
                                    </span>
                @endif
            @endforeach
        @endif
    </div>

    <div class="tab-pane fade" id="company" role="tabpanel" aria-labelledby="company-tab">
        @if(isset($first->data->about_company) && $first->data->about_company!='')
            <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->about_company)) not-dash @endif">{{trans('announcements.about_company')}}:&nbsp</h4>
            {!!\App\Job::emailize($first->data->about_company)!!}<br><br>
        @endif
        @if(Auth::check() && Auth::user()->can('accessAll_announcement'))
            @if(isset($first->expiration_date))
                <h4 class="font-bold not-dash">{{trans('jobs.expiration_date')}}:&nbsp</h4>
                {!! \Carbon\Carbon::parse($first->expiration_date)->format('d F Y')!!}<br>
            @endif
            @if(isset($first->data->contact_person))
                <h4 class="font-bold not-dash">{{trans('jobs.contact_person')}}:&nbsp</h4>
                {!! $first->data->contact_person !!}<br>
            @endif
            @if(isset($first->data->contact_title))
                <h4 class="font-bold not-dash">{{trans('jobs.contact_title')}}:&nbsp</h4>
                {!! $first->data->contact_title !!}<br>
            @endif
            @if(isset($first->email))
                <h4 class="font-bold not-dash">{{trans('company.email')}}:&nbsp</h4>
                <a
                        href="mailto:{{$first->email}}">{{$first->email}}</a><br>
            @endif
            @if(isset($phone))
                <h4 class="font-bold not-dash">{{trans('company.tel')}}:&nbsp</h4>
                    <a href="tel:{{$phone}}" class="job-tel">{{$phone}}</a>
            @endif
            <div class="clearfix mt-3"></div>
            @if(isset($first->added_user))
                <h4 class="font-bold not-dash">{{trans('announcements.added_by')}}:&nbsp</h4>
                {{$first->user->username}}@if($first->added_user->username!= $first->user->username )
                    ({{$first->added_user->username}})@endif,
            @endif
            <b>{{trans('announcements.on')}}
                :</b> {{\Carbon\Carbon::parse($first->created_at)->format('Y-m-d H:i:s')}},
            @if(isset($first->added_IP))
                <b>{{trans('announcements.from')}}:</b> {{$first->added_IP}}
            @endif
            <div class="clearfix"></div>
            @if(isset($first->edited_user))
                <h4 class="font-bold not-dash">{{trans('announcements.last_edited_by')}}:&nbsp</h4>
                {{$first->edited_user->username}},
            @endif

            <b>{{trans('announcements.on')}}
                :</b> {{\Carbon\Carbon::parse($first->updated_at)->format('Y-m-d H:i:s')}},
            @if(isset($first->edited_IP))
                <b> {{trans('announcements.from')}}:</b>
                {{$first->edited_IP}}
            @endif


            <div class="clearfix mb-2"></div>
        @endif

    </div>
</div>