<div class="profile-container float-right">
    <ul class="nav nav-pills mb-3 profile-tab" role="tablist">
        @foreach($langs as $localeCode => $properties)
            <li class="nav-item">
                <a class="nav-link  @if($loop->first)  active @endif" id="job-tab" data-toggle="pill"
                   href="#tab-{{$localeCode}}" role="tab"
                   aria-controls="tab-{{$localeCode}}"
                   aria-selected="true">{{trans('profile_tables.'.$properties['name'])}}</a>
            </li>

        @endforeach
    </ul>
    <div class="tab-content profile-tab-content">
        {{--tab 1--}}
        @foreach($langs as $localeCode => $properties)
            <div id="tab-{{$localeCode}}" class="tab-pane @if($loop->first) active @endif fade show">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            {{ Form::text("name_$localeCode",isset($data[$localeCode]) ? $data[$localeCode]->name : null)}}
                            <label for="{{"name_$localeCode"}}" class="control-label">{{trans('reviews.name')}}
                                <span class="star">*</span></label>
                            {!! $errors->first("name_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group">
                            {{ Form::text("profession_$localeCode",isset($data[$localeCode]) ? $data[$localeCode]->profession : null)}}
                            <label for="{{"profession_$localeCode"}}" class="control-label">{{trans('reviews.profession')}}
                                <span class="star">*</span></label>
                            {!! $errors->first("profession_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group">
                            {{ Form::textarea("review_$localeCode",isset($data[$localeCode]) ? $data[$localeCode]->review : null,['rows'=>3])}}

                            <label for="{{"review_$localeCode"}}" class="control-label">{{trans('reviews.review')}}
                                <span class="star">*</span> </label>
                            {!! $errors->first("review_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>

                    </div>
                </div>
            </div>
        @endforeach
        <div class="row">

            <div class="col-lg-12 cover-image">
                <div class="form-label">
                    {{ Form::label('image', trans('reviews.user_image'))}}
                </div>
                <div class="file-input-style form-group-file">
                    <label for="review_image" class="sr-only" >{{trans('reviews.upload_image')}}</label>
                    <input  type="file" name="image" id="review_image">
                </div>

                @if(isset($review))
                    <div class="col-12 col-md-6 p-0">
                        <img src="{{Storage::disk('')->url($review->image)}}" alt="" class="img-fluid">
                    </div>
                @endif

                {!! $errors->first('image', '<p style="color:red" class="help-block">:message</p>') !!}
            </div>
            <div class="w-100"></div>
            <div class="col-lg-12">
                <div class="d-flex justify-content-end mt-5">
                    <div class="text-center">
                        <button type="submit" class="btn btn-orange-gradient save">{{trans('reviews.save')}}</button>
                    </div>
                </div>
            </div>

        </div>



    </div>

</div>
@section('scripts')
    <script src="//cdn.ckeditor.com/4.7.1/full/ckeditor.js"></script>

    <script>
        var locale = '{{ Config::get('app.locale')}}';
        $('a[href="#tab-' + locale + '"]').tab('show');
        @if($errors->has('name_en') || $errors->has('profession_en') || $errors->has('review_en') )
          $('a[href="#tab-en"]').tab('show');
        @elseif ($errors->has('name_ru') || $errors->has('profession_ru') || $errors->has('review_ru') )
         $('a[href="#tab-ru"]').tab('show');
        @elseif($errors->has('name_hy') || $errors->has('profession_hy') || $errors->has('review_hy') )
         $('a[href="#tab-hy"]').tab('show');
        @endif




    </script>
@endsection