@extends('layouts.app')


@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')
    <section class="position-relative">
        @include('profile.menu')
        <div class="profile-container float-right">
            <ul class="nav nav-pills mb-3 profile-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="job-tab" data-toggle="pill" href="#job" role="tab"
                       aria-controls="job" aria-selected="true">{{trans('profile_tables.personal_info')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="company-tab" data-toggle="pill" href="#company" role="tab"
                       aria-controls="company" aria-selected="false">{{trans('profile_tables.settings')}}</a>
                </li>
            </ul>
            <div class="tab-content profile-tab-content">

                <div class="tab-pane fade show active" id="job" role="tabpanel" aria-labelledby="job-tab">
                    {!! Form::model($user, [
        'method' => 'PATCH',
        'route' => ['profile.user_details',$user->id],
        'files' => true,

    ]) !!}
                    <div class="row">
                        <div class="col-lg-4">
                            @permission('changeUsername_user')
                            <div class="form-group">
                                <input class="@error('username') is-invalid @enderror" name="username" type="text"
                                       id="username"
                                       value="{{old('username')!=null ? old('username') :$user->username}}" required>
                                <label for="username" class="control-label">{{trans('register_step_1.username')}}<span
                                            class="star">*</span></label>
                                @error('username')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            @endpermission

                            <div class="form-group">
                                <select name="title" id="title">
                                    <option></option>
                                    @foreach(config('enums.title') as $item)
                                        <option @if(old('title')==$item) selected
                                                @elseif($user->title == $item) selected
                                                @endif
                                                value="{{$item}}">{{trans('title.'.$item)}}</option>
                                    @endforeach
                                </select>
                                <label for="title" class="control-label">{{trans('register_step_2.title')}} </label>
                                @error('title')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="text" id="first_name" name="first_name"
                                       class="@error('first_name') is-invalid @enderror"
                                       value="{{old('first_name') ? old('first_name') :$user->first_name }}"
                                       required>
                                <label for="first_name" class="control-label">{{trans('register_step_2.first_name')}}
                                    <span
                                            class="star">*</span></label>
                                @error('first_name')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="text" id="middle_name" name="middle_name"
                                       class="@error('middle_name') is-invalid @enderror"
                                       value="{{old('middle_name') ? old('middle_name') :$user->middle_name }}"
                                       data-required="no">
                                <label for="middle_name"
                                       class="control-label">{{trans('register_step_2.middle_name')}} </label>
                                @error('middle_name')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="text" id="last_name" name="last_name"
                                       class="@error('last_name') is-invalid @enderror"
                                       value="{{old('last_name') ? old('last_name') :$user->last_name }}"
                                       required>
                                <label for="last_name" class="control-label">{{trans('register_step_2.last_name')}}
                                    <span
                                            class="star">*</span></label>
                                @error('last_name')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <input max="9999-12-31" type="date" id="birthday" name="birthday"
                                       class="form-control @error('birthday') is-invalid @enderror"
                                       value="{{old('birthday') ? old('birthday') :$user->birthday }}">
                                <label for="birthday" class="control-label">{{trans('register_step_2.birthday')}} </label>
                                @error('birthday')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <select name="residence_country_id" required>
                                    <option></option>
                                    @foreach($countries as $item)
                                        <option @if(old('residence_country_id')== $item->id) selected
                                                @elseif($user->residence_country_id == $item->id) selected
                                                @endif value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                                    @endforeach
                                </select>

                                <label for="address-country"
                                       class="control-label">{{trans('register_step_2.residence_country')}} </label>
                                @error('residence_country_id')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <select name="citizenship_id">
                                    <option></option>
                                    @foreach($countries as $item)
                                        <option @if(old('citizenship_id')==$item->id) selected
                                                @elseif($user->citizenship_id == $item->id) selected
                                                @endif value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                                    @endforeach
                                </select>

                                <label for="address-country"
                                       class="control-label">{{trans('register_step_2.citizenship')}} </label>
                                @error('citizenship_id')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-4 z-index-1">
                            <div id="dynamic_field3">
                                @if(old('key_address_count')==null)
                                    @for($i=0;$i<count($user->address);$i++)
                                        <div class="mb-3 @if($i != 0) deleting3 @endif position-relative"
                                             id="row3_{{$i+1}}">
                                            <div class="form-group">
                                                <input class="form-control @if($i != 0) deleting-input3_1  @endif"
                                                       id="address{{$i+1}}"
                                                       name="address[]"
                                                       value="{{old('address')[$i] ?  old('address')[$i] : $user->address[$i]->address}}"
                                                       type="text"  required>
                                                <label id="address_label_{{$i}}" for="address{{$i+1}}"
                                                       class="control-label">{{trans('register_step_2.address')}} {{trans('register_step_2.'.$type=$user->address[$i]->address_type?$user->address[$i]->address_type:'work')}}
                                                    <span class="star">*</span>
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <select required class="address_type" name="address_type[]" id="address_type_{{$i}}"  data-id="{{$i}}">
                                                    {{--<option value=""></option>--}}
                                                    <option @if($user->address[$i]->address_type == 'home') selected @endif value="home">{{trans('register_step_2.home')}}</option>
                                                    <option @if($user->address[$i]->address_type =='work' || !$user->address[$i]->address_type) selected @endif  value="work">{{trans('register_step_2.work')}}</option>
                                                </select>
                                                {{--<label for="address_type_{{$i}}"--}}
                                                       {{--class="control-label">{{trans('register_step_2.address_type')}}--}}
                                                {{--</label>--}}
                                            </div>
                                            <div class="form-group">
                                                <select name="country[]" id="address-country{{$i}}" required>
                                                    <option></option>
                                                    @foreach($countries as $item)
                                                        <option @if(old('country')[$i]==$item->id) selected
                                                                @elseif($user->address[$i]->country_id ==$item->id) selected
                                                                @endif value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                                                    @endforeach
                                                </select>
                                                <label for="address-country"
                                                       class="control-label">{{trans('register_step_2.country')}}
                                                    <span class="star">*</span>
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <input  required type="text" name="city[]"
                                                       class="@error('zip.'.$i) is-invalid @enderror @if($i!= 0) deleting-input3_{{$i}} @endif"
                                                       id="City{{$i+1}}"
                                                       value="{{old('city')[$i] ? old('city')[$i] : $user->address[$i]->city}}">
                                                <label for="City "
                                                       class="control-label">{{trans('register_step_2.city')}}
                                                    <span class="star">*</span>
                                                </label>
                                                @error('city.'.$i)
                                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <input  required  id="zip{{$i+1}}" type="text"
                                                       class="@error('zip.'.$i) is-invalid @enderror @if($i!= 0) deleting-input3_{{$i}} @endif"
                                                       name="zip[]"
                                                       value="{{old('zip')[$i] ?  old('zip')[$i] : $user->address[$i]->zip}}">
                                                <label for="zip"
                                                       class="control-label">{{trans('register_step_2.zip')}}
                                                    <span class="star">*</span>
                                                </label>
                                                @error('zip.'.$i)
                                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            @if($i!=0)
                                                <button type="button" name="remove" id="{{$i+1}}"
                                                        class="btn btn-danger btn-danger-2 deleting-button3 btn_remove 3">{{trans('register_step_2.delete')}}
                                                </button>
                                            @endif
                                        </div>
                                    @endfor
                                @else
                                    @for($i=0;$i<old('key_address_count');$i++)
                                        <div class="mb-3 @if($i != 0) deleting3 @endif position-relative"
                                             id="row3_{{$i+1}}">
                                            <div class="form-group">
                                                <input  required class="form-control @if($i != 0) deleting-input3_1 @endif"
                                                       id="address{{$i+1}}"
                                                       name="address[]"
                                                       value="{{old('address')[$i]}}"
                                                       type="text">
                                                <label id="address_label{{$i}}" for="address{{$i+1}}"
                                                       class="control-label">{{trans('register_step_2.address')}} {{trans('register_step_2.'.$type=old('address_type')[$i]?old('address_type')[$i]:'work')}}
                                                    <span class="star">*</span>
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <select data-id="{{$i}}" required  class="" name="address_type[]" id="address_type_{{$i}}">
                                                    <option value=""></option>
                                                    <option @if(old('address_type')[$i]=='home') selected @endif value="home">{{trans('register_step_2.home')}}</option>
                                                    <option @if(old('address_type')[$i]=='work') selected @endif  value="work">{{trans('register_step_2.work')}}</option>
                                                </select>
                                                <label for="address_type_{{$i}}"
                                                       class="control-label">{{trans('register_step_2.address_type')}}

                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <select name="country[]" id="address-country{{$i}}" required >
                                                    <option></option>
                                                    @foreach($countries as $item)
                                                        <option @if(old('country')[$i]==$item->id) selected
                                                                @endif value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                                                    @endforeach
                                                </select>
                                                <label for="address-country"
                                                       class="control-label">{{trans('register_step_2.country')}}
                                                    <span class="star">*</span>
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="city[]"
                                                       class="@error('city.'.$i) is-invalid @enderror @if($i!= 0) deleting-input3_{{$i}} @endif"
                                                       id="City{{$i+1}}"
                                                       value="{{old('city')[$i]}}" required>
                                                <label for="City "
                                                       class="control-label">{{trans('register_step_2.city')}}
                                                    <span class="star">*</span>
                                                </label>
                                                @error('city.'.$i)
                                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <input id="zip{{$i+1}}" type="text"
                                                       class="@error('zip.'.$i) is-invalid @enderror @if($i!= 0) deleting-input3_{{$i}} @endif"
                                                       name="zip[]"
                                                       value="{{old('zip')[$i]}}"
                                                       required >
                                                <label for="zip"
                                                       class="control-label">{{trans('register_step_2.zip')}}
                                                    <span class="star">*</span>
                                                </label>
                                                @error('zip.'.$i)
                                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            @if($i!=0)
                                                <button type="button" name="remove" id="{{$i+1}}"
                                                        class="btn btn-danger btn-danger-2 deleting-button3 btn_remove 3">{{trans('register_step_2.delete')}}
                                                </button>
                                            @endif
                                        </div>
                                    @endfor
                                @endif

                                <input type="hidden" name="key_address_count" id="key_address_count"
                                       value="{{old('key_address_count') ? old('key_address_count') : count($user->address)}}">
                            </div>
                            <button type="button" name="add3" id="add3"
                                    class="add-input font-bold">{{trans('register_step_2.add_address')}}</button>
                            <input type="hidden" name="is_organization" value="{{$user->is_organization}}">

                            <div id="dynamic_field4">

                                @if(old('key_phone_count')==null && isset($phone))
                                    @for($i=0;$i<count($phone);$i++)
                                        <div id="row4_{{$i+1}}"
                                             class="dynamic-added @if($i != 0) deleting4 @endif form-group ">
                                            <div class="form-group">
                                                <input name="phone[]" class="@error('phone.'.$i) is-invalid @enderror phone @if($i != 0)deleting-input4_1 @endif " required
                                                       value="{{old('phone')[$i] ? old('phone')[$i] : $phone[$i]['phone']}}">
                                                @if($i!=0)
                                                    <button type="button" name="remove" data-id="{{$i+1}}"
                                                            class="btn btn-danger deleting-button4 btn_remove 4">X
                                                    </button>
                                                @endif
                                                <label id="phone_label_{{$i}}"  class="control-label">{{trans('register_step_2.phone')}} {{trans('register_step_2.'.$type=$phone[$i]['type']?$phone[$i]['type']:'work')}} <span
                                                            class="star">*</span></label>
                                                @error('phone.'.$i)
                                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <select data-id="{{$i}}" class="phone_type" name="phone_type[]" id="phone_type_{{$i}}">
                                                    {{--<option value=""></option>--}}
                                                    @foreach(config('enums.phone_type') as $item)
                                                        @php($selected = "")
                                                        @if(!$phone[$i]['type'] && $item=='work')
                                                            @php($selected = 'selected')
                                                        @elseif($phone[$i]['type']==$item)
                                                            @php($selected = 'selected')
                                                        @endif
                                                        <option {{$selected}} value="{{$item}}">{{trans('register_step_2.'.$item)}}</option>
                                                    @endforeach
                                                </select>
                                                {{--<label for="phone_type_{{$i}}"  class="control-label">{{trans('register_step_2.phone_type')}}</label>--}}
                                            </div>
                                        </div>

                                    @endfor

                                @else
                                    @for($i=0;$i<old('key_phone_count');$i++)
                                        <div id="row4_{{$i+1}}"
                                             class="dynamic-added @if($i != 0) deleting4 @endif form-group ">
                                            <div  class="form-group" >
                                                <input name="phone[]" class="@error('phone.'.$i) is-invalid @enderror phone @if($i != 0)deleting-input4_1 @endif " required
                                                       value="{{old('phone')[$i]}}">
                                                @if($i!=0)
                                                    <button type="button" name="remove" data-id="{{$i+1}}"
                                                            class="btn btn-danger deleting-button4 btn_remove 4">X
                                                    </button>
                                                @endif
                                                <label id="phone_label_{{$i}}" class="control-label">{{trans('register_step_2.phone')}}{{trans('register_step_2.'.$type=$phone[$i]['type']?$phone[$i]['type']:'work')}}@if($i==0) <span
                                                            class="star">*</span>@endif</label>
                                                @error('phone.'.$i)
                                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div  class="form-group">
                                                <select data-id="{{$i}}" class="phone_type" name="phone_type[]" id="phone_type_{{$i}}">
                                                    {{--<option value=""></option>--}}
                                                    @foreach(config('enums.phone_type') as $item)
                                                        <option @if(old('phone_type')[$i]==$item) selected @endif value="{{$item}}">{{trans('register_step_2.'.$item)}}</option>
                                                    @endforeach
                                                </select>
{{--                                                <label for="phone_type_{{$i}}"  class="control-label">{{trans('register_step_2.phone_type')}}</label>--}}
                                            </div>

                                        </div>
                                    @endfor
                                @endif
                                <input type="hidden" name="key_phone_count" id="key_phone_count"
                                       value="{{old('key_phone_count') ? old('key_phone_count') : (isset($phone) ? count($phone): 0)}}">
                            </div>
                            <button type="button" name="add4" id="add4"
                                    class="add-input font-bold">{{trans('register_step_2.add_phone')}}</button>
                            <div class="form-group">
                                <input id="email" type="text" class="@error('email') is-invalid @enderror" name="email"
                                       value="{{old('email') ? old('email') : $user->email }}" required>
                                <label for="email" class="control-label">{{trans('register_step_2.email')}} <span
                                            class="star">*</span></label>
                                @error('email')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input  class="@error('website') is-invalid @enderror" name="website" id="web"
                                       value="{{old('website') ? old('website') : $user->website }}" data-required="no">
                                <label for="web" class="control-label">{{trans('register_step_2.web')}}</label>
                                @error('website')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>


                        <div class="col-lg-12">
                            <div class="d-flex justify-content-end mt-5">
                                <div class="text-center">
                                    <button type="submit"
                                            class="btn btn-orange-gradient">{{trans('profile_tables.save')}}</button>
                                </div>
                            </div>
                        </div>

                    </div>

                    {!! Form::close() !!}
                </div>


                {{--tab 2--}}

                <div class="tab-pane fade" id="company" role="tabpanel" aria-labelledby="company-tab">
                    <div class="row">
                        <div class="col-lg-4">
                            <h5 id="pass-success" style="display: none">Password is changed successfully!</h5>
                            <form method="POST" action="{{route('changePassword',$user->id)}}"
                                  enctype="multipart/form-data">
                                {{method_field('PATCH')}}
                                @csrf
                                <div class="form-group">
                                    <input required
                                           type="password" class="@error('current_password') is-invalid @enderror"
                                           name="current_password">
                                    <label for="username" class="control-label">{{trans('register_step_2.current_password')}}<span
                                                class="star">*</span></label>
                                    @if ($errors->has('current_password'))
                                        <span class="invalid-feedback">{{ $errors->first('current_password') }}</span>
                                    @endif
                                </div>


                                <div class="form-group">
                                    <input type="password" required id="password" name="password"
                                           autocomplete="new-password"
                                           class="@error('password') is-invalid @enderror">
                                    <label for="password" class="control-label">{{trans('register_step_1.new_password')}}
                                        <span
                                                class="star">*</span></label>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <input type="Password" id="password-confirm" name="password_confirmation" required
                                           autocomplete="new-password"
                                           class="@error('password-confirm') is-invalid @enderror">
                                    <label for="password-confirm"
                                           class="control-label">{{trans('register_step_1.confirm_pass')}} <span
                                                class="star">*</span></label>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="invalid-feedback">{{ $errors->first('password_confirmation') }}</span>
                                    @endif
                                </div>

                                <div class="d-flex justify-content-end mt-5">
                                    <div class="text-center">
                                        <button type="submit"
                                                class="btn btn-orange-gradient">{{trans('profile_tables.change_password')}}</button>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                    <div class="col-lg-12">
                        <div class="d-flex justify-content-end mt-5">
                            <div class="text-center">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </section>
@endsection
@section('scripts')
    <script src="/js/jquery.maskedinput.js" type="text/javascript"></script>
    <script>
          @if ($errors->has('password') || $errors->has('password_confirmation') || $errors->has('current_password') )
           $('a[href="#company"]').tab('show');
        @endif


        $(document).ready(function () {
            var key_phone = $('#key_phone_count').val();

            var a = key_phone;
            var sum1 = key_phone;
            let type;
            $('#add4').on('click', function () {

                if (sum1 <= 5) {
                    ++a;
                    var select_phone = '<select data-id="'+a+'" id="phone_type_'+a+'" class="form-control phone_type" name="phone_type[]"><option></option>';
                        @foreach(config('enums.phone_type') as $item)
                    {
                        select_phone += "<option value='{{$item}}'>{{trans('register_step_2.'.$item)}}</option>"
                    }
                    @endforeach
                        select_phone += '</select>';
                    $('#dynamic_field4').append('<div id="row4_' + a + '" class="dynamic-added deleting4">' +
                        '<div class="form-group"><input id="phone' + a + '" class="phone" required name="phone[]" class="deleting-input4_1 " value="">' +
                        '<label id="phone_label_'+a+'" class="control-label" for="phone' + a + '" >{{trans('register_step_2.phone')}} '  + '</label>'+
                        '<button type="button" name="remove" data-id="' + a + '" class="btn btn-danger deleting-button4 btn_remove 4">X</button></div><div class="form-group">' +
                        select_phone+
                        '<label class="control-label" for="phone_type_"'+a+'>{{trans('register_step_2.phone_type')}} '  + '</label></div></div>');
                    sum1++;
                    if(sum1==5){
                        $('#add4').css('display','none')
                    }
                }

                $('#key_phone_count').val(a);
            });

            $(document).on('click', '.4', function () {
                var button_id3 = $(this).attr("data-id");

                var key_phone_val = $('#key_phone_count').val();
                $('#key_phone_count').val(key_phone_val - 1);
                $('#row4_' + button_id3).remove();
                sum1--;
                a--;

//                var count = 2;
//                $(".deleting4").each(function (key, element) {
//                    $(this).attr("id", 'row4_' + count);
//                    $(this).find('.deleting-input4_1').attr('name', 'current_city_' + count);
//                    $(this).find('.deleting-button4').attr('id', count);
//                    count++;
//                });
            });

            var key_add = $('#key_address_count').val();
            var l = key_add;
            var sum = key_add;

            $('#add3').on('click', function () {

                var select = '<select class="form-control" name="country[]" required id="address-country' + l + '"><option></option>';
                @foreach($countries as $item)
                    select += "<option value ={{$item->id}}>{{trans('countries.'.$item->code)}}</option>"
                @endforeach
                    select += '</select>';


                if (sum <= 5) {
                    ++l;
                    $('#dynamic_field3').append(
                        '<div id="row3_' + l + '" class="dynamic-added-block deleting3 position-relative">' +
                        '<div class="form-group">' +
                        '<input required  name="address[]" class="form-control deleting-input3_1 " value="" id="address' + l + '">' +
                        '<label id="address_label_'+l+'" for="address' + l + '" class="control-label">{{trans('register_step_2.address')}} '  + '</label></div><div class="form-group">' +
                        '<select required data-id="'+l+'" id="address_type_'+l+'" class="address_type" name="address_type[]"><option value=""></option><option value="home">{{trans('register_step_2.home')}}</option><option value="work">{{trans('register_step_2.work')}}</option></select>'+
                        '<label for="address_type_'+l+'" class="control-label">{{trans('register_step_2.address_type')}}</label>'+
                        '</div>' +
                        '<div class="form-group">' +
                        select + '' +
                        '<label for="address-country' + l + '" class="control-label">{{trans('register_step_2.country')}} '  + '</label>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<input id="city' + l + '" type="text" required name="city[]" class="deleting-input3_1 " value="">' +
                        '<label for="City' + l + '" class="control-label">{{trans('register_step_2.city')}} '  + '</label>' +
                        '</div>' +
                        '<div class="form-group"> ' +
                        '<input type="text" required  name="zip[]" class="deleting-input3_1" value="" id="zip' + l + '">' +
                        '<label for="zip' + l + '" class="control-label">{{trans('register_step_2.zip')}}'  + '</label>' +
                        '</div>' +
                        '<button type="button" name="remove" id="' + l + '" class="btn btn-danger deleting-button3 btn_remove 3">{{trans('register_step_2.delete')}} '  + '</button>' +
                        '</div>');
                    sum++;
                    if(sum==5){
                        $('#add3').css('display','none')
                    }
                }
                $('#key_address_count').val(l);
            });

            $(document).on('click', '.3', function () {
                var button_id3 = $(this).attr("id");

                var key_address_val = $('#key_address_count').val();
                $('#key_address_count').val(key_address_val - 1);
                $('#row3_' + button_id3).remove();
                sum--;
                l--;

//                var count = 2;
//                $(".deleting3").each(function (key, element) {
//                    $(this).attr("id", 'row3_' + count);
//                    $(this).find('.deleting-input3_1').attr('name', 'current_address_' + count);
//                    $(this).find('.deleting-button3').attr('id', count);
//                    count++;
//                });
            });
            $(document).on('change', '.phone_type', function () {
                type = $(this).find(':selected').text();
                $('#phone_label_' + $(this).attr('data-id')).text('{{trans('register_step_2.phone')}}' + ' ' + type);

            });
            $(document).on('change', '.address_type', function () {
                type = $(this).find(':selected').text();
                $('#address_label_' + $(this).attr('data-id')).text('{{trans('register_step_2.address')}}' + ' ' + type);

            });
        });

    </script>
@endsection











