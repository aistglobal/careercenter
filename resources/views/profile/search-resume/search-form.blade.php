@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link rel="stylesheet" type="text/css" href="/css/select2.min.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }

        @media (min-width: 576px) {
            .modal-dialog {
                max-width: 1000px;
                margin: 1.75rem auto;
            }
        }
    </style>
@endsection

@section('content')

    <section class="position-relative">
        @include('profile.menu')


        <div class="modal fade" id="search-filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        {{--<h5 class="modal-title" id="exampleModalCenterTitle">{{trans('resume.fail')}}</h5>--}}
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form  id="job_form" action="{{route('searchResume')}}"
                              class="form-horizontal">
                        <div class="row">
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <select name="updated_at[]" id="updated_at" multiple class="form-file">
                                        <option value=""></option>
                                        @foreach($updated_at as $item)
                                            <option @if(isset($request['updated_at']) && in_array($item, $request['updated_at'])) selected @endif value="{{$item}}">{{\Carbon\Carbon::parse($item)->format('d F Y')}}</option>
                                        @endforeach
                                    </select>
                                    <label for="updated_at" class="control-label">{{trans('resume.select_updated_at')}} </label>
                                </div>

                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <select name="sex" id="sex" class="form-file">
                                        <option value=""></option>
                                        <option @if(isset($request['sex']) &&  $request->sex=='male') selected
                                                @endif value="male">{{trans('resume.male')}}</option>
                                        <option @if(isset($request['sex']) && $request->sex=='female') selected
                                                @endif value="female">{{trans('resume.female')}}</option>
                                    </select>

                                    <label for="sex" class="control-label">{{trans('resume.sex')}}</label>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <h5 class="mb-3">{{trans('resume.location')}}</h5>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input name="location" type="text"
                                           id="location"
                                           value="{{isset($request['location'])? $request['location'] : null}}">
                                    <label for="location" class="control-label">{{trans('resume.location')}}</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <select multiple name="residence_country_id[]" id="residence_country_id" class="form-file">
                                        <option value=""></option>
                                        @foreach($country as $item)
                                            <option @if(isset($request['residence_country_id']) &&  in_array($item->id, $request['residence_country_id'])) selected
                                                    @endif value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                                        @endforeach
                                    </select>
                                    <label for="title" class="control-label">{{trans('resume.country')}} </label>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <h5 class="mb-3">{{trans('resume.work_experience')}}</h5>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <select name="position_field[]" multiple id="position_field">
                                        @foreach(config('enums.category') as $item)
                                            <option @if(isset($request['position_field']) && in_array($item, $request['position_field'])) selected
                                                    @endif  value="{{$item}}">{{trans('category.'.$item)}}</option>
                                        @endforeach
                                    </select>
                                    <label for="position_field"
                                           class="control-label">{{trans('resume.work_field')}}</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <select name="study_area[]" multiple id="study_area">
                                        @foreach(config('enums.category') as $item)
                                            <option @if(isset($request['study_area']) && in_array($item, $request['study_area'])) selected
                                                    @endif  value="{{$item}}">{{trans('category.'.$item)}}</option>
                                        @endforeach
                                    </select>
                                    <label for="title"
                                           class="control-label">{{trans('resume.background_study_area')}} </label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <select multiple class="edu" id="degree" name="degree[]">
                                        <option value=""></option>
                                        @foreach(config('enums.degree') as $item)
                                            <option @if(isset($request['degree']) && in_array($item, $request['degree'])) selected
                                                    @endif value="{{$item}}">{{trans('cv_education.'.$item)}}</option>
                                        @endforeach
                                    </select>
                                    <label for="title"
                                           class="control-label">{{trans('resume.background_degree_level')}} </label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                {{--<div class="form-group">--}}
                                    {{--<select multiple class="edu" id="degree" name="degree[]">--}}
                                        {{--<option value=""></option>--}}
                                        {{--@foreach(config('enums.degree') as $item)--}}
                                            {{--<option @if(isset($request['salary']) && in_array($item, $request['degree'])) selected--}}
                                                    {{--@endif value="{{$item}}">{{trans('cv_education.'.$item)}}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                    {{--<label for="title"--}}
                                           {{--class="control-label">{{trans('resume.background_degree_level')}} </label>--}}
                                {{--</div>--}}
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input name="major_field" type="text" value="{{isset($request['major_field'])? $request['major_field'] : null}}">
                                    <label class="control-label">{{trans('resume.major_field')}}</label>
                                </div>
                            </div>


                            <div class="col-lg-12">
                                <h5 class="mb-3">{{trans('resume.availability')}}</h5>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <input value="{{isset($request['salary'])? $request['salary'] : null}}" name="salary" type="number">
                                    <label class="control-label">{{trans('resume.salary')}}</label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <select id="currency" name="currency">
                                        @foreach(config('enums.currency') as $item)
                                            <option @if(isset($request['currency']) &&  $request['currency']==$item) selected
                                                    @endif  value="{{$item}}">{{trans('currency.'.$item)}}</option>
                                        @endforeach
                                    </select>
                                    <label for="currency"
                                           class="control-label">{{trans('resume.select_currency')}} </label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <select id="period" name="period">
                                        @foreach(config('enums.period') as $item)
                                            <option value=""></option>
                                            <option @if(isset($request['period']) &&  $request['period']==$item) selected
                                                    @endif   value="{{$item}}">{{trans('period.'.$item)}}</option>
                                        @endforeach
                                    </select>
                                    <label for="title"
                                           class="control-label">{{trans('resume.select_payment_method')}} </label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <select name="term" id="term" class="form-file">
                                        <option value=""></option>
                                        @foreach(config('enums.terms') as $item)
                                            <option @if(isset($request['term'])) selected
                                                    @endif value="{{$item}}"></option>
                                        @endforeach
                                    </select>
                                    <label for="title" class="control-label">{{trans('resume.select_term')}} </label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">

                                    <select multiple name="work_type[]" id="work_type">
                                        <option value=""></option>
                                        @foreach(config('enums.work_type') as $item)
                                            <option @if(isset($request['work_type'])) selected
                                                    @endif value="{{$item}}">{{trans('cv_volunteering.'.$item)}}</option>
                                        @endforeach
                                    </select>
                                    <label for="work_type"
                                           class="control-label">{{trans('resume.select_job_type')}} </label>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <select name="willing" id="willing_to_volunteer">
                                        <option value=""></option>
                                        <option @if(isset($request['willing']) && $request['willing']=='yes') selected
                                                @endif value="yes">{{trans('resume.yes')}}
                                        </option>
                                        <option @if(isset($request['willing']) && $request['willing']=='no') selected
                                                @endif value="no">{{trans('resume.no')}}
                                        </option>
                                    </select>
                                    <label for="willing_to_volunteer"
                                           class="control-label">{{trans('resume.select_volunteering')}} </label>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <select multiple id="interest_area" name="interest_area[]">
                                        <option value=""></option>
                                        @foreach(config('enums.category') as $item)
                                            <option @if(isset($request['interest_area']) &&  in_array($item,$request['interest_area'])) selected
                                                    @endif value="{{$item}}">{{trans('category.'.$item)}}</option>
                                        @endforeach
                                    </select>
                                    <label for="title" class="control-label">{{trans('resume.interest_area')}} </label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <select multiple id="position_type" name="position_type[]">
                                        <option value=""></option>
                                        @foreach(config('enums.types_of_positions') as $item)
                                            <option @if(isset($request['position_type']) &&  in_array($item,$request['position_type'])) selected
                                                    @endif value="{{$item}}">{{trans('cv_availability.'.$item)}}</option>
                                        @endforeach
                                    </select>
                                    <label for="title" class="control-label">{{trans('resume.position_type')}} </label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <select multiple name="citizenship_id[]" id="citizenship_id" class="form-file">
                                        <option value=""></option>
                                        @foreach($country as $item)
                                            <option @if(isset($request['citizenship_id']) &&  in_array($item->id,$request['citizenship_id'])) selected
                                                    @endif value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                                        @endforeach
                                    </select>
                                    <label for="title"
                                           class="control-label">{{trans('resume.select_citizenship')}} </label>
                                </div>
                            </div>


                            <div class="col-lg-12">
                                <h5 class="mb-3">{{trans('resume.language_computer_skills')}} </h5>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <select class="language" id="language_id" name="language_id[]" multiple>
                                        <option value=""></option>
                                        @foreach($languages as $item)
                                            <option @if(isset($request['language_id']) &&  in_array($item,$request['language_id'])) selected
                                                    @endif  value="{{$item->id}}">{{trans('languages.'.$item->name)}}</option>
                                        @endforeach
                                    </select>
                                    <label for="title"
                                           class="control-label">{{trans('resume.select_language_skills')}}  </label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <input name="software"
                                           value="{{isset($request['software'])? $request['software'] : null}}"
                                           type="text">
                                    <label class="control-label">{{trans('resume.software')}}  </label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <input name="hardware"
                                           value="{{isset($request['hardware'])? $request['hardware'] : null}}"
                                           type="text">
                                    <label class="control-label">{{trans('resume.hardware')}}  </label>
                                </div>
                            </div>
                            <div class="col-lg-12 text-right">
                                <button type="submit"
                                         class="btn btn-blue-gradient">{{trans('resume.search')}} </button>
                            </div>
                        </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <a href="#" class="btn btn-blue-gradient" data-toggle="modal" data-target="#search-filter">Search</a>
            </div>
            <div class="table-responsive">
                <table class="table mobile-table">
                    <thead>
                    <tr class="table-tr-th">
                        <th>
                            {{trans('profile_tables.last_updated_on')}}
                        </th>
                        <th>
                            {{trans('profile_tables.full_name')}}
                        </th>
                        <th>{{trans('profile_tables.age')}}</th>
                        <th>{{trans('resume.city')}}</th>
                        <th>{{trans('profile_tables.salary')}}</th>
                        <th class="text-center">{{trans('profile_tables.action')}}</th>
                    </tr>
                    </thead>

                    <tbody>

                    @foreach($resumes as $item)

                        <tr class="col-sd-3">
                            <td data-label="{{trans('profile_tables.last_updated_on')}}">@if(isset($item)){{$item->updated_at->format('M d, Y H:i')}}@endif</td>
                            <td data-label=" {{trans('profile_tables.full_name')}}">@if(isset($item) && isset($item->contacts)){{$item->contacts->first_name}} {{$item->contacts->last_name}}@endif</td>
                            <td data-label="{{trans('profile_tables.age')}}">@if(isset($item) && isset($item->personal) && isset($item->personal->birthday)) {{Carbon\Carbon::parse($item->personal->birthday)->age}} @endif</td>
                            <td data-label="{{trans('resume.city')}}">@if(isset($item) && count($item->address)!=0) {{$item->address[0]->city}} @endif</td>
                            <td data-label="{{trans('profile_tables.salary')}}">
                                @if(isset($item) && isset($item->availability) && isset($item->availability->long_salary))

                                    {{$item->availability->long_salary}} {{trans('currency.'.$item->availability->long_currency)}}
                                    {{trans('currency.'.$item->availability->long_period)}}
                                @endif
                            </td>


                            <td data-label="{{trans('profile_tables.action')}}">
                                <div class="table-buttons d-flex justify-content-center align-items-center">
                                    <a href="{{route('showCv',$item->id)}}" title="{{trans('profile_tables.view')}}"><img src="/img/visibility.svg" alt=""></a>
                                    {{--@if(Auth::id() == $item->user_id)--}}
                                    @permission('edit_resume')
                                    <a href="{{route('resume.create',$item->id)}}" title="{{trans('profile_tables.edit')}}"><img src="/img/edit.svg" alt=""></a>
                                    @endpermission
                                    {{--@endif--}}
                                    <a href="{{route('resumePDF',$item->id)}}" class="pdf-down" title="{{trans('profile_tables.download')}}"><img src="/img/download.svg" alt=""></a>

                                    @permission('delete_resume')
                                    {!! Form::open([
                                    'method'=>'DELETE',
                                    'route' => ['resume.destroy',$item->id],
                                    'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<img src="/img/delete.svg">', array(
                                            'type' => 'submit',
                                            'class' => '',
                                            'title' => trans('resume.delete'),
                                            'onclick'=>'return confirm("Confirm delete?")'
                                    )) !!}
                                    {!! Form::close() !!}
                                    @endpermission

                                </div>
                            </td>
                        </tr>

                    @endforeach

                    </tbody>
                </table>
            </div>
            <div class="d-flex justify-content-end my-pagination align-items-center">
                {{--<form id="myForm" action="{{route('resume.index')}}">--}}
                    {{--<div>--}}
                        {{--<label for="rr">Item per page:</label>--}}

                        {{--<select name="order" id="rr">--}}
                            {{--<option @if($order==20) selected @endif value="20">20</option>--}}
                            {{--<option @if($order==50) selected @endif value="50">50</option>--}}
                            {{--<option @if($order==100) selected @endif value="100">100</option>--}}
                        {{--</select>--}}

                    {{--</div>--}}
                {{--</form>--}}

                <div class="pagination-links d-flex">
                    {{ $resumes->appends(['search' => Request::get('search'),'order_by' => Request::get('order_by'),'order' => Request::get('order')])->links('vendor.pagination.bootstrap-4')}}
                </div>
            </div>

        </div>


    </section>


@endsection
@section('scripts')
    <script type="text/javascript" src="/js/moment.min.js" defer></script>

    <script src="/js/Select2-4.0.7.js" type="text/javascript"></script>
    <script>
        $('#updated_at').select2();
        $('#residence_country_id').select2();
        $('#position_field').select2();
        $('#study_area').select2();
        $('#degree').select2();
        $('#work_type').select2();
        $('#interest_area').select2();
        $('#position_type').select2();
        $('#citizenship_id').select2();
        $('#language_id').select2();

    </script>
@endsection

