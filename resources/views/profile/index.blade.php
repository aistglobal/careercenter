@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')
    <section class="position-relative">
        @include('profile.menu')
        <div class="profile-container float-right">

            <div class="tab-content profile-tab-content">
                <div class="tab-pane fade show active" id="job" role="tabpanel" aria-labelledby="job-tab">
                    <form>
                        <div class="row">

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" id="username" required>
                                    <label for="username" class="control-label">{{trans('register.job')}}<span class="star">*</span></label>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group pr-4">
                                        <input type="text" id="calendar1" data-required=no>
                                        <label for="username" class="control-label">Opening Date</label>
                                    </div>
                                    <div class="form-group flex-grow-1">
                                        <input type="text" id="Application" required>
                                        <label for="Application" class="control-label">Application Deadline<span class="star">*</span></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea required="" id="textarea"></textarea>
                                    <label for="textarea" class="control-label">Textarea<span class="star">*</span></label>
                                </div>
                                <div class="form-group height-textarea">
                                    <textarea required="" id="tt"></textarea>
                                    <label for="tt" class="control-label">Textarea click height</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" id="username" required>
                                    <label for="username" class="control-label">Announcement code</label>
                                </div>
                                <div class="form-group-file mb-input file-input-style">
                                    <label for="file" class="sr-only">Choose file</label>
                                    <input type="file" id="file">
                                    <span class="input-help">Permitted files: txt, pdf, jpg, gif, png, doc,xls, zip, docx,  xlsx (Max: 1MB).</span>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"/><i class="helper"></i>This Announcement will be translated
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"/><i class="helper"></i>Enable On-line applications Submission
                                    </label>
                                </div>

                            </div>

                            <div class="col-lg-12">
                                <div class="d-flex justify-content-end mt-5">
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-orange-gradient">{{trans('user.edit')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </section>
@endsection
