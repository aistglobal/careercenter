<div class="profile-container float-right">
    <ul class="nav nav-pills mb-3 profile-tab" role="tablist">
        @foreach($langs as $localeCode => $properties)
            <li class="nav-item">
                <a class="nav-link  @if($loop->first)  active @endif" id="job-tab" data-toggle="pill"
                   href="#tab-{{$localeCode}}" role="tab"
                   aria-controls="tab-{{$localeCode}}"
                   aria-selected="true">{{trans('profile_tables.'.$properties['name'])}}</a>
            </li>

        @endforeach
    </ul>
    <div class="tab-content profile-tab-content">
        <div class="row">
            <input type="hidden" name="status" value="{{$resource->status??'pending'}}">
            <div class="col-lg-12 max-850">
                <div class="form-group">
                    {{ Form::url("url",null,['required'=>'required'])}}
                    <label for="{{"url"}}" class="control-label">{{trans('resources.url')}}
                        <span class="star">*</span></label>
                    {!! $errors->first("url", '<p style="color:red" class="help-block">:message</p>') !!}
                </div>
                <div class="form-group">
                    <select class="form-control" name="category_id" id="category_id" required>
                        <option></option>
                        @foreach($category as $item)
                            <option @if(old('category_id')!=null && old('category_id')==$item->id) selected
                                    @elseif(isset($resource) && $resource->category_id==$item->id) selected
                                    @endif value="{{$item->id}}">{{$item->data->name}}</option>
                        @endforeach
                    </select>
                    <label for="category"
                           class="control-label">{{trans('resources.category')}}  <span class="star">*</span></label>
                    @error('category')
                    <span class="invalid-feedback" role="alert">{{ $message }}</span>
                    @enderror
                </div>

                @if(isset($resource))
                    <div class="form-group" id="sub_div">
                        <select class="form-control" name="subcategory_id" id="subcategory">
                            <option value=""></option>
                            @foreach($resource->category->child as $item)
                            <option value="{{$item->id}}" @if($resource->subcategory_id == $item->id) selected @endif id="{{$item->id}}">{{$item->data->name}}</option>
                                @endforeach
                        </select>
                        <label for="subcategory"
                               class="control-label">{{trans('resources.sub_category')}}</label>
                        @error('subcategory_id')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    @else
                    <div class="form-group" id="sub_div"  style="display: none">
                        <select class="form-control" name="subcategory_id" id="subcategory">
                            <option></option>
                        </select>
                        <label for="subcategory"
                               class="control-label">{{trans('resources.sub_category')}}</label>
                        @error('subcategory_id')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    @endif

            </div>
        </div>
        {{--tab 1--}}
        @foreach($langs as $localeCode => $properties)

            <div id="tab-{{$localeCode}}" class="tab-pane @if($loop->first) active @endif fade show">

                <div class="row">
                    <div class="col-lg-12 max-850">
                        <div class="form-group">
                            {{ Form::text("name_$localeCode",isset($data[$localeCode]) ? $data[$localeCode]->name : null,$lang==$localeCode?['required']:['data-required'=>'no'])}}
                            <label for="{{"name_$localeCode"}}" class="control-label">{{trans('resources.name')}}
                                <span class="star">*</span></label>
                            <p id="error_name_{{$localeCode}}" style="color:red" class="help-block"></p>
                        </div>

                        <div class="form-group">
                            {{ Form::textarea("note_$localeCode",isset($data[$localeCode]) ? $data[$localeCode]->note : null,['data-required'=>'no'],['rows'=>1])}}

                            <label for="{{"note_$localeCode"}}" class="control-label">{{trans('resources.note')}}
                                </label>
                            {!! $errors->first("note_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group">
                            {{ Form::text("link_to_$localeCode",isset($data[$localeCode]) ? $data[$localeCode]->link_to : null)}}

                            <label for="{{"link_to_$localeCode"}}" class="control-label">{{trans('resources.link_to')}}
                            </label>
                            {!! $errors->first("link_to_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>

                    </div>
                </div>
            </div>
        @endforeach

        <div class="col-lg-12 max-850">
            <div class="d-flex justify-content-end mt-5">
                <div class="text-center">
                    <button type="submit" class="btn btn-orange-gradient save">{{trans('jobs.submit')}}</button>
                </div>
            </div>
        </div>
    </div>
    @permission('all_resource')
    @if(isset($resource))
        @if(isset($resource->user))
            <h5>{{trans('blogs.added_by')}} :
                {{$resource->user->username}}</h5>
        @endif
        @if(isset($resource->editor))
            <h5>  {{trans('blogs.last_updated_by')}} :
                {{$resource->editor->username}}</h5>

        @endif
    @endif
    @endpermission
</div>
