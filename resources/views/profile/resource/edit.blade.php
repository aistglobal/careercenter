@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')

    <section class="position-relative">
        @include('profile.menu')

        {!! Form::model($resource, [
                              'method' => 'PATCH',
                              'route' => ['resources.update',$resource->id],
                              'class' => 'form-horizontal',
                              'id' => 'job_form'
                          ]) !!}

        @include ('profile.resource.form')
        {!! Form::close() !!}

    </section>


@endsection
@section('scripts')
    <script type="text/javascript" src="/js/moment.min.js" defer></script>
    <script src="//cdn.ckeditor.com/4.7.1/full/ckeditor.js"></script>
    <script>
        var locale = '{{ Config::get('app.locale')}}';
        $('a[href="#tab-'+locale+'"]').tab('show');
        @if($errors->has('name_en')  )
        $('a[href="#tab-en"]').tab('show');
        @elseif ($errors->has('name_ru') )
         $('a[href="#tab-ru"]').tab('show');
        @elseif($errors->has('name_hy')  )
         $('a[href="#tab-hy"]').tab('show');
        @endif
        function initInputs(){

            $(".form-group:not(.no-focus)")
                .find(".form-control, select, input, textarea")
                .each(function () {
                    var targetItem = $(this).parent();

                    if ($(this).val()) {
                        $(targetItem)
                            .find(".form-control, select, input, textarea")
                            .addClass("form-file");
                    }
                });
            $(".form-group")
                .find(".form-control, select, input, textarea")
                .focus(function () {
                    // $(this)
                    //     .parent(".form-group")
                    //     .addClass("form-file");
                    $(this)
                        .parent()
                        .find(".form-control, select, input, textarea")
                        .addClass("form-file");

                });
            $(".form-group")
                .find(".form-control, select, input, textarea")
                .blur(function () {
                    if ($(this).val().length == 0) {
                        $(this)
                            .parent(".form-group")
                            .removeClass("form-file");
                        $(this)
                            .parent()
                            .find(".form-control, select, input, textarea")
                            .removeClass("form-file");
                    }
                });

        }
                  $(document).ready(function () {
            $('#category_id').on('change', function () {
                $('#subcategory')
                    .find('option')
                    .remove();
                $('#sub_div').css('display','none');
                let cat_id = $(this).val();
                let locale = $('#current_locale').val();
                $.ajax({
                    type: "POST",
                    data: {
                        'category_id': cat_id,
                        'locale': locale
                    },
                    url: '{{route('getSubCategory')}}',
                    success: function (result)
                    {
                        if (result!='none') {
                            $('#sub_div').css('display','block');
                            var d = JSON.parse(result);
                            var select = $('#subcategory');
                            var a = '';
                            for (var i in d) {
                                a += "<option value =" + d[i]['id']+ ">" + d[i]['name'] + "</option>"
                            }
                            select.html(a);
                            initInputs();
                        }
                    }
                });

            })
            $('#job_form').submit(function (evt) {
                evt.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    type: "POST",
                    data: formData,
                    url: '{{route('resources.update',$resource->id)}}',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        $('.help-block').each(function () {
                            $(this).text('');
                        });
                        window.location = result;
                    },
                    error: function (data) {
                        var errors = data.responseJSON;
                        $('.help-block').each(function () {
                            $(this).text('');
                        });
                        if (errors['errors']['name_en'] != undefined) {
                            $('a[href="#tab-en"]').tab('show')
                            $('#error_name_en').html(errors['errors']['name_en']);
                        }
                        if (errors['errors']['name_ru'] != undefined) {
                            $('a[href="#tab-ru"]').tab('show')
                            $('#error_name_ru').html(errors['errors']['name_ru']);
                        }
                        if (errors['errors']['name_hy'] != undefined) {
                            $('a[href="#tab-hy"]').tab('show')
                            $('#error_name_hy').html(errors['errors']['name_hy']);
                        }
                    }
                });
            })


        });
    </script>
@endsection






