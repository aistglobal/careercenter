@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')
    <section class="position-relative bg-grey">
        @include('profile.menu')

        <div class="profile-container float-right">
            <div class="profile-table">
                <div class="d-flex justify-content-between">
                    @permission('create_pages')
                    <a href="{{route('pages.create')}}" class="btn btn-orange-gradient mr-2">{{trans('pages.create')}}</a>
                    @endpermission
                    <form id="myForm1" action="{{route('pages.index')}}"
                          class="profile-search-form d-flex position-relative ">
                        <input value="{{$search}}" id="search" name="search" class="form-control" type="search"
                               placeholder="{{trans('profile_tables.search')}}…" aria-label="Search">
                        <button id="search_btn" class="btn" type="button"><img src="/img/search.svg" alt=""></button>
                    </form>


                </div>


                <div class="table-responsive">
                    <table class="table mobile-table">
                        <thead>
                        <tr class="table-tr-th">
                            <th>{{trans('profile_tables.title')}}</th>
                            <th>{{trans('profile_tables.created')}}</th>
                            <th class="text-center">{{trans('profile_tables.action')}}</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($pages as $item)
                            @if(isset($item->data))
                                <tr class="col-sd-3">
                                    <td data-label="{{trans('profile_tables.title')}}">@if(isset($item)){{$item->data->title}}@endif</td>
                                    <td data-label="{{trans('profile_tables.created')}}">@if(isset($item)){{$item->created_at->format('M d, Y H:i')}}@endif</td>
                                    <td data-label="{{trans('profile_tables.action')}}">
                                        <div class="table-buttons d-flex justify-content-center">
                                            <a href="{{route('pages',$item->slug)}}" title="{{trans('profile_tables.view')}}"><img src="/img/visibility.svg" alt=""></a>
                                            @permission('edit_pages')
                                            <a href="{{route('pages.edit',$item->id)}}" title="{{trans('profile_tables.edit')}}" ><img src="/img/edit.svg" alt=""></a>
                                            @endpermission
                                            @permission('delete_pages')
                                            {!! Form::open([
                                            'method'=>'DELETE',
                                            'route' => ['pages.destroy',$item->id],
                                            'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<img src="/img/delete.svg">', array(
                                                    'type' => 'submit',
                                                    'class' => '',
                                                    'title' => trans('page.delete'),
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                            {!! Form::close() !!}
                                            @endpermission
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        @endforeach

                        </tbody>
                    </table>
                </div>

                <div class="d-flex justify-content-end my-pagination align-items-center">
                    <form id="myForm" action="{{route('pages.index')}}">
                        <div>
                            <label for="rr">{{trans('jobs.item_per_page')}}:</label>

                            <select name="order_by" id="rr">
                                <option @if($order==20) selected @endif value="20">20</option>
                                <option @if($order==50) selected @endif value="50">50</option>
                                <option @if($order==100) selected @endif value="100">100</option>
                            </select>

                        </div>
                    </form>
                    {{--<span>1-20 of 23</span>--}}
                    {{ $pages->appends(['search' => Request::get('search'),'order_by' => Request::get('order_by')])->links('vendor.pagination.bootstrap-4')}}
                    <div class="pagination-links d-flex">

                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
@section('scripts')
    <script>
        $('#rr').on('change',function()
        {
            var search = $('#search').val();
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "search").val(search).appendTo('#myForm');
            $( "#myForm" ).submit();

        });
        $('#search_btn').on('click',function()
        {
            var order = $('#rr').val();
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "order_by").val(order).appendTo('#myForm1');
            $( "#myForm1" ).submit();
        });
    </script>
@endsection
