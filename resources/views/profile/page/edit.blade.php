@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')

    <section class="position-relative">
        @include('profile.menu')

        {!! Form::model($page, [
                              'method' => 'PATCH',
                              'route' => ['pages.update',$page->id],
                              'class' => 'form-horizontal',
                              'files' => true,
                              'id' => 'job_form'
                          ]) !!}

        @include ('profile.page.form')
        {!! Form::close() !!}

    </section>


@endsection
@section('scripts')
    <script type="text/javascript" src="/js/moment.min.js" defer></script>
@endsection







