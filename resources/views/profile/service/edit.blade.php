@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')

    <section class="position-relative">
        @include('profile.menu')

        {!! Form::model($service, [
                              'method' => 'PATCH',
                              'route' => ['services.update',$service->id],
                              'class' => 'form-horizontal',
                              'id' => 'job_form'
                          ]) !!}

        @include ('profile.service.form')
        {!! Form::close() !!}

    </section>


@endsection
@section('scripts')
    <script type="text/javascript" src="/js/moment.min.js" defer></script>
    <script src="//cdn.ckeditor.com/4.7.1/full/ckeditor.js"></script>
    <script>
        var locale = '{{ Config::get('app.locale')}}';
        $('a[href="#tab-'+locale+'"]').tab('show');
        @if($errors->has('name_en')  )
        $('a[href="#tab-en"]').tab('show');
        @elseif ($errors->has('name_ru') )
         $('a[href="#tab-ru"]').tab('show');
        @elseif($errors->has('name_hy')  )
         $('a[href="#tab-hy"]').tab('show');
        @endif

    </script>
@endsection






