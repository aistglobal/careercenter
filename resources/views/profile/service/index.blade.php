@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
        .table-header{
            background-color: #014a7c;
            padding: 10px 15px;
            border-radius: 5px;
            margin-bottom: 5px;
            color: white;
        }
    </style>
@endsection

@section('content')
    <section class="position-relative bg-grey">
        @include('profile.menu')

        <div class="profile-container float-right">

            <div class="profile-table">
                <div class="d-flex justify-content-between">
                    {{--@permission('create_service')--}}
                    <a href="{{route('services.create')}}" class="btn btn-orange-gradient mr-2">Create</a>
                    {{--@endpermission--}}

                    {{--<form id="myForm1" action="{{route('services.index')}}"--}}
                          {{--class="profile-search-form d-flex position-relative ">--}}
                        {{--<input value="{{$search}}" id="search" name="search" class="form-control" type="search"--}}
                               {{--placeholder="{{trans('profile_tables.search')}}…" aria-label="Search">--}}
                        {{--<button id="search_btn" class="btn" type="button"><img src="/img/search.svg" alt=""></button>--}}
                    {{--</form>--}}
                </div>

                <div class="table-responsive">
                    @foreach($categories as $item)
                        <div class="w-100 table-header">
                            {{$item->data->name}}
                        </div>
                            <table class="table mobile-table">
                                <thead>
                                <tr class="table-tr-th">
                                    <th>{{trans('profile_tables.name')}}</th>
                                    <th>{{trans('profile_tables.2co_id')}}</th>
                                    <th>{{trans('profile_tables.product_id')}}</th>
                                    <th>{{trans('profile_tables.amd')}}</th>
                                    <th>{{trans('profile_tables.usd')}}</th>
                                    <th>{{trans('profile_tables.euro')}}</th>
                                    <th class="text-center">{{trans('profile_tables.action')}}</th>
                                </tr>
                                </thead>

                                <tbody>

                                @foreach($item->services as $service)
                                    @if(isset($service->data))
                                        <tr class="col-sd-3">
                                            <td data-label="{{trans('profile_tables.name')}}">@if(isset($service->data)){{$service->data->name}}@endif</td>
                                            <td data-label="{{trans('profile_tables.2co_id')}}">{{$service->co_id}}</td>
                                            <td data-label="{{trans('profile_tables.product_id')}}">{{$service->product_id}}</td>
                                            <td data-label="{{trans('profile_tables.amd')}}">{{$service->amd}}</td>
                                            <td data-label="{{trans('profile_tables.usd')}}">{{$service->usd}}</td>
                                            <td data-label="{{trans('profile_tables.euro')}}">{{$service->euro}}</td>


                                            <td data-label="{{trans('profile_tables.action')}}">
                                                <div class="table-buttons d-flex justify-content-center">
                                                    {{--@permission('edit_service')--}}
                                                    <a href="{{route('services.edit',$service->id)}}"
                                                       title="{{trans('profile_tables.edit')}}"><img src="/img/edit.svg"
                                                                                                     alt=""></a>
                                                    {{--@endpermission--}}
                                                    {{--@permission('delete_service')--}}
                                                    {!! Form::open([
                                                    'method'=>'DELETE',
                                                    'route' => ['services.destroy',$service->id],
                                                    'style' => 'display:inline'
                                                    ]) !!}
                                                    {!! Form::button('<img src="/img/delete.svg">', array(
                                                            'type' => 'submit',
                                                            'class' => '',
                                                            'title' => trans('resource.delete'),
                                                            'onclick'=>'return confirm("Confirm delete?")'
                                                    )) !!}
                                                    {!! Form::close() !!}
                                                    {{--@endpermission--}}

                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach

                                </tbody>
                            </table>
                        @endforeach


                </div>

                <div class="d-flex justify-content-end my-pagination align-items-center">
                    <form id="myForm" action="{{route('services.index')}}">
                        <div>
                            <label for="rr">{{trans('jobs.item_per_page')}}:</label>

                            <select name="order_by" id="rr">
                                <option @if($order==20) selected @endif value="20">20</option>
                                <option @if($order==50) selected @endif value="50">50</option>
                                <option @if($order==100) selected @endif value="100">100</option>
                            </select>

                        </div>
                    </form>

                    <div class="pagination-links d-flex">
                        {{ $categories->appends(['search' => Request::get('search'),'order_by' => Request::get('order_by')])->links('vendor.pagination.bootstrap-4')}}
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
@section('scripts')
    <script>
        $('#rr').on('change', function () {
            var search = $('#search').val();
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "search").val(search).appendTo('#myForm');
            $("#myForm").submit();

        });
        $('#search_btn').on('click', function () {
            var order = $('#rr').val();
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "order_by").val(order).appendTo('#myForm1');
            $("#myForm1").submit();
        });
        $('.status').on('change', function () {
            $(this).closest('form').submit();
        });
    </script>
@endsection
