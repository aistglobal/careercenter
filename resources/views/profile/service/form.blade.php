<div class="profile-container float-right">
    <ul class="nav nav-pills mb-3 profile-tab" role="tablist">
        @foreach($langs as $localeCode => $properties)
            <li class="nav-item">
                <a class="nav-link  @if($loop->first)  active @endif" id="job-tab" data-toggle="pill"
                   href="#tab-{{$localeCode}}" role="tab"
                   aria-controls="tab-{{$localeCode}}"
                   aria-selected="true">{{trans('profile_tables.'.$properties['name'])}}</a>
            </li>

        @endforeach
    </ul>
    <div class="tab-content profile-tab-content">
        {{--tab 1--}}
        @foreach($langs as $localeCode => $properties)

            <div id="tab-{{$localeCode}}" class="tab-pane @if($loop->first) active @endif fade show">

                <div class="row">
                    <div class="col-lg-12 max-850">
                        <div class="form-group">
                            {{ Form::text("name_$localeCode",isset($data[$localeCode]) ? $data[$localeCode]->name : null,$lang==$localeCode?['required']:['data-required'=>'no'])}}
                            <label for="{{"name_$localeCode"}}" class="control-label">{{trans('services.name')}}
                                <span class="star">*</span></label>
                            {!! $errors->first("name_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>

                    </div>
                </div>
            </div>
        @endforeach
        <div class="row">
            <div class="col-lg-12 max-850">

                    <div class="form-group">
                        <select class="form-control" name="category_id" id="category_id" required>
                            <option></option>
                            @foreach($category as $item)
                                <option @if(old('category_id')!=null && old('category_id')==$item->id) selected
                                        @elseif(isset($service) && $service->category_id==$item->id) selected
                                        @endif value="{{$item->id}}">{{$item->data->name}}</option>
                            @endforeach
                        </select>
                        <label
                               class="control-label">{{trans('services.category')}}  <span class="star">*</span></label>
                        @error('category_id')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                <div class="form-group">
                    {{ Form::number("co_id",null,['required'=>'required','id'=>'co_id'])}}
                    <label for="co_id" class="control-label">{{trans('services.2co_id')}}
                        <span class="star">*</span></label>
                    {!! $errors->first("co_id", '<p style="color:red" class="help-block">:message</p>') !!}
                </div>
                <div class="form-group">
                    {{ Form::text("product_id",null,['required'=>'required','id'=>'product_id'])}}
                    <label for="product_id" class="control-label">{{trans('services.product_id')}}
                        <span class="star">*</span></label>
                    {!! $errors->first("product_id", '<p style="color:red" class="help-block">:message</p>') !!}
                </div>
                <div class="form-group">
                    {{ Form::number("amd",null,['required'=>'required','id'=>'amd'])}}
                    <label for="amd" class="control-label">{{trans('services.amd')}}
                        <span class="star">*</span></label>
                    {!! $errors->first("amd", '<p style="color:red" class="help-block">:message</p>') !!}
                </div>
                <div class="form-group">
                    {{ Form::number("usd",null,['required'=>'required','id'=>'usd'])}}
                    <label for="usd" class="control-label">{{trans('services.usd')}}
                        <span class="star">*</span></label>
                    {!! $errors->first("usd", '<p style="color:red" class="help-block">:message</p>') !!}
                </div>
                <div class="form-group">
                    {{ Form::number("euro",null,['required'=>'required','id'=>'euro'])}}
                    <label for="euro" class="control-label">{{trans('services.euro')}}
                        <span class="star">*</span></label>
                    {!! $errors->first("euro", '<p style="color:red" class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>

        <div class="col-lg-12 max-850">
            <div class="d-flex justify-content-end mt-5">
                <div class="text-center">
                    <button type="submit" class="btn btn-orange-gradient save">{{trans('jobs.submit')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>
