@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')

    <section class="position-relative">
        @include('profile.menu')

        {!! Form::model($resource, [
                              'method' => 'PATCH',
                              'route' => ['resource_category.update',$resource->id],
                              'class' => 'form-horizontal',
                              'files' => true,
                              'id' => 'job_form'
                          ]) !!}

        @include ('profile.resource-category.form')
        {!! Form::close() !!}
        @if(count($resource->child)!=0)
            <div class="profile-container float-right">

                <div class="profile-table">
                    <div class="table-responsive">
                        <table class="table mobile-table">
                            <thead>
                            <tr class="table-tr-th">
                                <th>{{trans('profile_tables.category_name')}}</th>
                                <th>{{trans('profile_tables.sub_categories')}}</th>
                                <th class="text-center">{{trans('profile_tables.action')}}</th>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach($resource->child as $item)
                                @if(isset($item->data))
                                    <tr class="col-sd-3">
                                        <td data-label="{{trans('profile_tables.category_name')}}">@if(isset($item->data)){{$item->data->name}}@endif</td>
                                        <td data-label="{{trans('profile_tables.sub_categories')}}">
                                            @if(count($item->child))
                                                @foreach($item->child as $cat)
                                                    {{$cat->data->name}}
                                                    <br>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td data-label="{{trans('profile_tables.action')}}">
                                            <div class="table-buttons d-flex justify-content-center">
                                                <a href="{{route('resource_category.edit',$item->id)}}" title="{{trans('profile_tables.edit')}}"><img src="/img/edit.svg" alt=""></a>
                                                {!! Form::open([
                                                'method'=>'DELETE',
                                                'route' => ['resource_category.destroy',$item->id],
                                                'style' => 'display:inline'
                                                ]) !!}
                                                {!! Form::button('<img src="/img/delete.svg">', array(
                                                        'type' => 'submit',
                                                        'class' => '',
                                                        'title' => trans('resource_category.delete'),
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                                {!! Form::close() !!}

                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif

    </section>

@endsection
@section('scripts')
    <script type="text/javascript" src="/js/moment.min.js" defer></script>
@endsection







