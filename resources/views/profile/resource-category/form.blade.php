<div class="profile-container float-right">
    <ul class="nav nav-pills mb-3 profile-tab" role="tablist">
        @foreach($langs as $localeCode => $properties)
            <li class="nav-item">
                <a class="nav-link  @if($loop->first)  active @endif" id="job-tab" data-toggle="pill"
                   href="#tab-{{$localeCode}}" role="tab"
                   aria-controls="tab-{{$localeCode}}"
                   aria-selected="true">{{trans('profile_tables.'.$properties['name'])}}</a>
            </li>

        @endforeach
    </ul>
    <div class="tab-content profile-tab-content">
        <div class="row">

            <div class="col-lg-4">
                <div class="form-group">
                    <select class="form-control" name="parent_id" id="category">
                        <option></option>
                        @foreach($resources as $item)
                            <option @if(old('parent_id')!=null && old('parent_id')==$item->id) selected
                                    @elseif(isset($resource) && $resource->parent_id==$item->id) selected
                                    @endif value="{{$item->id}}">{{$item->data->name}}</option>
                        @endforeach
                    </select>
                    <label for="category"
                           class="control-label">{{trans('resource_category.parent_category')}}</label>
                    @error('resource_id')
                    <span class="invalid-feedback" role="alert">{{ $message }}</span>
                    @enderror
                </div>

            </div>


        </div>
        {{--tab 1--}}
        @foreach($langs as $localeCode => $properties)

            <div id="tab-{{$localeCode}}" class="tab-pane @if($loop->first) active @endif fade show">

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            {{ Form::text("name_$localeCode",isset($data[$localeCode]) ? $data[$localeCode]->name : null)}}
                            <label for="{{"name_$localeCode"}}" class="control-label">{{trans('resources.name')}}
                                <span class="star">*</span></label>
                            {!! $errors->first("name_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>

                    </div>
                </div>
            </div>
        @endforeach

        <div class="col-lg-4">
            <div class="d-flex justify-content-end mt-5">
                <div class="text-center">
                    <button type="submit" class="btn btn-orange-gradient save">{{trans('resource_category.save')}}</button>
                </div>
            </div>
        </div>
    </div>

</div>
@section('scripts')
    <script src="//cdn.ckeditor.com/4.7.1/full/ckeditor.js"></script>
    <script>
        @if($errors->has('name_en'))
          $('a[href="#tab-en"]').tab('show');
        @elseif ($errors->has('name_ru') )
         $('a[href="#tab-ru"]').tab('show');
        @elseif($errors->has('name_hy'))
         $('a[href="#tab-hy"]').tab('show');
        @endif

    </script>
@endsection
