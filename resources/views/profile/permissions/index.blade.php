@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    <style>
        .navigation {
            background-color: #ffffff;
        }

        .table-responsive-permission {
            height: calc(100vh - 200px);
        }

        .th-fix th {
            position: sticky;
            top: 0;
            background-color: #fff;
            border-top: 0;
            font-weight: normal;
            font-family: lato-bold;
        }
    </style>
@endsection

@section('content')
    <section class="position-relative">
        @include('profile.menu')

        <div class="profile-container float-right">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{trans('permissions.permissions')}}</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive table-responsive-permission">
                            <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                                <thead>
                                <tr class="th-fix">
                                    <th> {{trans('permissions.permission')}}</th>
                                    @foreach($roles as $role)
                                        @switch($role->name)
                                            @case('user')
                                            <th class="text-center">

                                                {{ trans('permissions.'.$role->name) }}
                                            </th>
                                            @break
                                            @case('organization')
                                            <th class="text-center">

                                                {{ trans('permissions.'.$role->name) }}
                                            </th>
                                            @break
                                            @case('pro_organization')
                                            <th class="text-center">

                                                {{ trans('permissions.'.$role->name) }}
                                            </th>
                                            @break
                                            @case('moderator')
                                            <th class="text-center">

                                                {{ trans('permissions.'.$role->name) }}
                                            </th>
                                            @break
                                            @case('head_moderator')
                                            <th class="text-center">

                                                {{ trans('permissions.'.$role->name) }}
                                            </th>
                                            @break
                                            @case('admin')
                                            <th class="text-center">

                                                {{ trans('permissions.'.$role->name) }}
                                            </th>
                                            @break
                                        @endswitch

                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($permission_data as $key=>$permissionList)
                                    <tr style="background-color: #c3c3c370">
                                        <td colspan="{{1 + count($roles)}}"><b>{{ trans('permissions.'.$key) }}</b></td>
                                    </tr>
                                    @foreach($permissionList as $item)
                                        <tr>
                                            <td>
                                                <p style="margin: 0">{{ trans('permissions.'.$item[1]) }}

                                                    @if($item[3] == 2 )
                                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" y="0px"
                                                             width="20px" height="20px" viewBox="0 0 88.71 88.709">
                                                            <g>
                                                                <path d="M49.044,24.514c0-8.1,6.565-14.666,14.666-14.666s14.666,6.566,14.666,14.666c0,8.1-6.565,14.666-14.666,14.666
		S49.044,32.613,49.044,24.514z M69.932,40.18H57.488c-10.354,0-18.777,8.424-18.777,18.777V74.18l0.039,0.236l1.05,0.328
		c9.88,3.086,18.466,4.117,25.531,4.117c13.801,0,21.8-3.936,22.294-4.186l0.98-0.498l0.104,0.001V58.958
		C88.71,48.604,80.287,40.18,69.932,40.18z M25,39.18c8.1,0,14.666-6.566,14.666-14.666c0-8.1-6.566-14.666-14.666-14.666
		s-14.666,6.566-14.666,14.666C10.334,32.614,16.9,39.18,25,39.18z M35.326,74.18V58.958c0-6.061,2.445-11.55,6.385-15.568
		c-2.997-2.025-6.607-3.209-10.488-3.209H18.778C8.424,40.18,0,48.604,0,58.958V74.18l0.039,0.236l1.051,0.328
		c9.879,3.086,18.465,4.117,25.531,4.117c4.493,0,8.359-0.42,11.563-0.99l-2.422-0.758L35.326,74.18z"/>
                                                            </g>
                                                        </svg>
                                                    @elseif($item[3] == 3)
                                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px"
                                                             y="0px"
                                                             width="20px" height="20px" viewBox="0 0 80.13 80.13">
                                                            <g>
                                                                <path d="M48.355,17.922c3.705,2.323,6.303,6.254,6.776,10.817c1.511,0.706,3.188,1.112,4.966,1.112 c6.491,0,11.752-5.261,11.752-11.751c0-6.491-5.261-11.752-11.752-11.752C53.668,6.35,48.453,11.517,48.355,17.922z M40.656,41.984
		c6.491,0,11.752-5.262,11.752-11.752s-5.262-11.751-11.752-11.751c-6.49,0-11.754,5.262-11.754,11.752S34.166,41.984,40.656,41.984
		z M45.641,42.785h-9.972c-8.297,0-15.047,6.751-15.047,15.048v12.195l0.031,0.191l0.84,0.263
		c7.918,2.474,14.797,3.299,20.459,3.299c11.059,0,17.469-3.153,17.864-3.354l0.785-0.397h0.084V57.833
		C60.688,49.536,53.938,42.785,45.641,42.785z M65.084,30.653h-9.895c-0.107,3.959-1.797,7.524-4.47,10.088
		c7.375,2.193,12.771,9.032,12.771,17.11v3.758c9.77-0.358,15.4-3.127,15.771-3.313l0.785-0.398h0.084V45.699
		C80.13,37.403,73.38,30.653,65.084,30.653z M20.035,29.853c2.299,0,4.438-0.671,6.25-1.814c0.576-3.757,2.59-7.04,5.467-9.276
		c0.012-0.22,0.033-0.438,0.033-0.66c0-6.491-5.262-11.752-11.75-11.752c-6.492,0-11.752,5.261-11.752,11.752
		C8.283,24.591,13.543,29.853,20.035,29.853z M30.589,40.741c-2.66-2.551-4.344-6.097-4.467-10.032
		c-0.367-0.027-0.73-0.056-1.104-0.056h-9.971C6.75,30.653,0,37.403,0,45.699v12.197l0.031,0.188l0.84,0.265
		c6.352,1.983,12.021,2.897,16.945,3.185v-3.683C17.818,49.773,23.212,42.936,30.589,40.741z"/>
                                                            </g>
                                                        </svg>
                                                    @endif
                                                </p>


                                            </td>
                                            @foreach($roles as $role)

                                                <td class="text-center">
                                                    <a style="cursor: pointer" class="check-permission"
                                                       id="check-{{$item[0]}}-{{$role->id}}"
                                                       data-permission_id="{{$item[0]}}" data-permission="{{$item[2]}}"
                                                       data-role="{{$role->display_name}}" data-role_id="{{$role->id}}">
                                                        @if(in_array($item[0], $role->permissions->pluck('id')->toArray()))
                                                            <i class="fa fa-check-circle"
                                                               style="font-size: 20px; color: #7fba00"></i>
                                                        @else
                                                            <i class="fa fa-times-circle"
                                                               style="font-size: 20px; color: #fa7373"></i>
                                                        @endif
                                                    </a>
                                                </td>

                                            @endforeach
                                        </tr>
                                    @endforeach
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.check-permission').click(function () {

                var check = 'false';
                if ($(this).children().hasClass('fa-check-circle')) {
                    check = 'true';
                }
                var permission_id = $(this).data('permission_id');
                var role_id = $(this).data('role_id');
                var permission = $(this).data('permission');
                var role = $(this).data('role');
                var title = "Are You sure to add '" + permission + "' permission to " + role + " role?";
                if (check == 'true') {
                    title = "Are You sure to remove '" + permission + "' permission from " + role + " role?";
                }
                var yes = 'Yes';
                var no = 'No';

                swal({
                        title: title,
                        type: "info",
                        showCancelButton: true,
                        confirmButtonColor: "#337ab7",
                        confirmButtonText: yes,
                        cancelButtonText: no,
                        closeOnConfirm: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: "{{route('permissionsUpdate')}}",
                                type: "POST",
                                data: {
                                    permission_id: permission_id,
                                    role_id: role_id,
                                    check: check,
                                    _token: $('meta[name="csrf-token"]').attr('content')
                                },
                                success: function (data) {
                                    if (data.check == 'false') {
                                        $('#check-' + data.permission_id + '-' + data.role_id).html("<i class='fa fa-check-circle' style='font-size: 20px; color: #7fba00'></i>");

                                    } else {
                                        $('#check-' + data.permission_id + '-' + data.role_id).html("<i class='fa fa-times-circle' style='font-size: 20px; color: #fa7373'></i>");
                                    }
                                }
                            });
                            swal.close()
                        } else {
                            swal.close()
                        }
                    });
            });

        });
    </script>
@endsection


