@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')

    <section class="position-relative">
        @include('profile.menu')

        {!! Form::open(['route' => 'registerUserStep1', 'class' => 'form-horizontal', 'files' => true,'id'=>'job_form']) !!}

        <div class="profile-container float-right">
            <div class="tab-content profile-tab-content">

                <div class="tab-pane fade show active" id="job" role="tabpanel" aria-labelledby="job-tab">

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <input type="text" id="name" name="username"
                                       class="{{ $errors->has('username') || $errors->has('email') ? ' is-invalid' : '' }}"
                                       value="{{ old('username') }}" required>
                                <label for="name" class="control-label">{{trans('register_step_1.username')}} <span
                                            class="star">*</span></label>
                                @if ($errors->has('username') || $errors->has('email'))
                                    <span class="invalid-feedback">{{ $errors->first('username') ?: $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="password" required id="password" name="password"
                                       autocomplete="new-password"
                                       class="{{ $errors->has('password')  ? ' is-invalid' : '' }}">
                                <label for="password" class="control-label">{{trans('register_step_1.password')}} <span
                                            class="star">*</span></label>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">{{ $errors->first('password') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="Password" id="password-confirm" name="password_confirmation" required
                                       autocomplete="new-password"
                                       class="{{ $errors->has('password_confirmation')  ? ' is-invalid' : '' }}">
                                <label for="password-confirm"
                                       class="control-label">{{trans('register_step_1.confirm_pass')}} <span
                                            class="star">*</span></label>
                                @if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback">{{ $errors->first('password_confirmation') }}</span>
                                @endif
                            </div>



                            <div class="form-group">
                                <select name="role_id" id="role" required>
                                    <option value=""></option>
                                    @foreach($roles as $item)
                                        @if($item->name == 'admin')
                                            @if(isset(\Illuminate\Support\Facades\Auth::user()->role[0]) && \Illuminate\Support\Facades\Auth::user()->role[0]->name == 'admin')
                                                <option @if(old('role_id')!=null && old('role_id') == $item->id)selected
                                                        @endif  value="{{$item->id}}">{{$item->display_name}}</option>
                                                @endif
                                            @else
                                            <option @if(old('role_id')!=null && old('role_id') == $item->id)selected
                                                    @endif  value="{{$item->id}}">{{$item->display_name}}</option>
                                            @endif

                                    @endforeach
                                </select>
                                <label for="role" class="control-label">{{trans('register_step_1.role') }}<span
                                            class="star">*</span></label>
                            </div>
                            <input type="hidden" name="agree_terms" value="yes">
                        </div>

                        <div class="w-100"></div>

                        <div class="col-lg-4">
                            <div class="d-flex justify-content-end mt-5">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-orange-gradient save">SAVE</button>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>

            </div>
        </div>

        {!! Form::close() !!}

    </section>


@endsection
@section('scripts')
    <script type="text/javascript" src="/js/moment.min.js" defer></script>
    <script>
        $('#is_organization').on('change',function()
        {
            $('#role').empty();
            $("#role").append('<option value=""></option>');
            @foreach($roles as $key=>$value)
             $("#role").append('<option value="{{$key}}">{{$key}}</option>');
            @endforeach

            var value = $(this).val();
            if(value==1)
            {
                $("#role option[value='user']").remove();
                $("#role option[value='moderator']").remove();
                $("#role option[value='head_moderator']").remove();
                $("#role option[value='admin']").remove();
            }
            else
            {
                $("#role option[value='organization']").remove();
                $("#role option[value='pro_organization']").remove();
            }
        })
    </script>
@endsection







