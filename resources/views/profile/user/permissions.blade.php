@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')
    <section class="position-relative">
        @include('profile.menu')
        <div class="profile-container float-right">

            <div class="tab-content profile-tab-content">
                <h4 class="mb-5">{{$user->username}} @if(isset($user->first_name) || isset($user->first_name)) - {{$user->first_name}} {{$user->last_name}} @endif</h4>
                <div class="tab-pane fade show active" id="job" role="tabpanel" aria-labelledby="job-tab">
                    <form method="POST" action="{{route('user-permission-update',$user->id)}}"
                          enctype="multipart/form-data">
                        {{method_field('PATCH')}}
                        @csrf
                        <div class="row permissions-container">
                            <div class="col-12 d-flex flex-wrap">
                                @foreach($permissions as $item)
                                    @if($item->name=='edit_admin')
                                        @if(Auth::user()->can('edit_admin'))
                                            <div class="checkbox">
                                                <label>
                                                    <input @if($user->can($item->name)) checked
                                                           @endif   name="{{$item->name}}" type="checkbox"/><i
                                                            class="helper"></i>{{$item->display_name}}
                                                </label>
                                            </div>
                                        @endif
                                    @else
                                        <div class="checkbox">
                                            <label>
                                                <input @if($user->can($item->name)) checked
                                                       @endif   name="{{$item->name}}" type="checkbox"/><i
                                                        class="helper"></i>{{$item->display_name}}
                                            </label>
                                        </div>
                                    @endif

                                @endforeach
                            </div>
                            <div class="w-100"></div>
                            <div class="col-12">
                                <div class="d-flex justify-content-end mt-5">
                                    <div class="text-center" style="display: none" id="edit_btn">
                                        <button type="submit"
                                                class="btn btn-orange-gradient">{{trans('profile_tables.save')}}</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </form>
                </div>


            </div>
        </div>

    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('.checkbox').on('change',function()
            {
                $('#edit_btn').css('display','block');
                console.log('aa');
            })

        })
    </script>
@endsection













