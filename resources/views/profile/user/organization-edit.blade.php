@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')
    <section class="position-relative">
        @include('profile.menu')
        <div class="profile-container float-right">
            <ul class="nav nav-pills mb-3 profile-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="job-tab" data-toggle="pill" href="#job" role="tab"
                       aria-controls="job" aria-selected="true">{{trans('profile_tables.personal_info')}}</a>
                </li>
                @if(Auth::user()->can('changePassword_profile'))
                    <li class="nav-item">
                        <a class="nav-link" id="company-tab" data-toggle="pill" href="#settings" role="tab"
                           aria-controls="company" aria-selected="false">{{trans('profile_tables.settings')}}</a>
                    </li>
                @endif
                @permission('changeBankDetails_user')
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" id="bank-tab" data-toggle="pill" href="#bank" role="tab"--}}
                       {{--aria-controls="bank" aria-selected="false">{{trans('profile_tables.bank_details')}}</a>--}}
                {{--</li>--}}
                @endpermission
            </ul>
            <div class="tab-content profile-tab-content position-relative">
                <a href="{{$back}}" class="mb-2 p-2 d-inline-block" style="position: absolute; top: 0"><i class="arrow-left-orange-heavy">Left</i></a>
                <div class="tab-pane fade show active" id="job" role="tabpanel" aria-labelledby="job-tab">
                    {!! Form::model($user, [
        'method' => 'PATCH',
        'route' => ['users.update',$user->id],
        'files' => true,

    ]) !!}

                    <div class="row">
                        <div class="col-lg-4">
                            @permission('changeUsernameAll_user')
                            <div class="form-group">
                                <input class="@error('username') is-invalid @enderror" name="username" type="text"
                                       id="username"
                                       value="{{old('username')!=null ? old('username') :$user->username}}" required>
                                <label for="username" class="control-label">{{trans('register_step_1.username')}}<span
                                            class="star">*</span></label>
                                @error('username')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            @endpermission
                            <div class="form-group">
                                <select name="title" id="title">
                                    <option></option>
                                    @foreach(config('enums.title') as $item)
                                        <option @if(old('title')==$item) selected
                                                @elseif($user->title == $item) selected
                                                @endif
                                                value="{{$item}}">{{trans('title.'.$item)}}</option>
                                    @endforeach
                                </select>
                                <label for="title" class="control-label">{{trans('register_step_2.title')}} </label>
                                @error('title')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="text" id="first_name" name="first_name"
                                       class="@error('first_name') is-invalid @enderror"
                                       value="{{old('first_name') ? old('first_name') :$user->first_name }}"
                                       required>
                                <label for="first_name" class="control-label">{{trans('register_step_2.first_name')}}
                                    <span
                                            class="star">*</span></label>
                                @error('first_name')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="text" id="middle_name" name="middle_name"
                                       class="@error('middle_name') is-invalid @enderror"
                                       value="{{old('middle_name') ? old('middle_name') :$user->middle_name }}"
                                       data-required="no">
                                <label for="middle_name"
                                       class="control-label">{{trans('register_step_2.middle_name')}} </label>
                                @error('middle_name')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="text" id="last_name" name="last_name"
                                       class="@error('last_name') is-invalid @enderror"
                                       value="{{old('last_name') ? old('last_name') :$user->last_name }}"
                                       required>
                                <label for="last_name" class="control-label">{{trans('register_step_2.last_name')}}
                                    <span
                                            class="star">*</span></label>
                                @error('last_name')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input id="position" type="text" class="@error('position') is-invalid @enderror"
                                       name="position" required
                                       value="{{ old('position') ? old('position') :$user->organization->position }}">
                                <label for="position" class="control-label">{{trans('register_step_2.position')}} <span
                                            class="star">*</span></label>
                                @error('position')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <select name="role_id" id="role" required>
                                    <option value=""></option>
                                    @foreach($roles as $item)
                                        @if($item->name =='admin')
                                            @if(isset(\Illuminate\Support\Facades\Auth::user()->role[0]) && \Illuminate\Support\Facades\Auth::user()->role[0]->name == 'admin')
                                                <option @if($role==$item->id) selected
                                                        @endif value="{{$item->id}}">{{trans('users.'.$item->name)}}</option>
                                            @endif
                                        @else
                                            <option @if($role==$item->id) selected
                                                    @endif  value="{{$item->id}}">{{trans('users.'.$item->name)}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <label for="role" class="control-label">{{trans('register_step_1.role') }}<span
                                            class="star">*</span></label>
                            </div>
                            <div class="@error('image')is-invalid @enderror form-group-file file-input-style">
                                <label for="logo" class="sr-only">{{trans('register_step_2.logo')}}</label>
                                <input type="file" id="logo" name="image" accept="image/*">
                                @error('image')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                                @if(isset($user->image))
                                    <img style="width: 200px;" src="{{Storage::disk('')->url($user->image)}}" alt="">
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <input id="org_name" type="text" class="@error('name') is-invalid @enderror" name="name"
                                       value="{{ old('name') ? old('name') :$user->organization->name}}" required>
                                <label for="org_name" class="control-label">{{trans('register_step_2.org_name')}} <span
                                            class="star">*</span></label>
                                @error('name')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input id="short_name" type="text" class="@error('short_name') is-invalid @enderror"
                                       name="short_name"
                                       value="{{ old('short_name') ? old('short_name') :$user->organization->short_name }}"
                                >
                                <label for="short_name"
                                       class="control-label">{{trans('register_step_2.org_short_name')}}</label>
                                @error('short_name')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="legal_structure" id="legal_structure">
                                    <option value=""></option>
                                    @foreach(config('enums.legal') as $item)
                                        <option @if(old('legal_structure') == $item)selected
                                                @elseif($user->organization->legal_structure == $item) selected
                                                @elseif(old('legal_structure')==null && isset($user->organization->legal_structure) && !in_array($user->organization->legal_structure,config('enums.legal'))  && $item == 'other') selected
                                                @endif value="{{$item}}">{{trans('legal_structure.'.$item)}}</option>
                                    @endforeach
                                </select>
                                <label for="legal_structure"
                                       class="control-label">{{trans('register_step_2.legal_structure')}}
                                </label>
                                @error('legal_structure')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div id="legal-other" class="form-group"
                                 style="display:@if(old('legal_structure')!= null) @if(old('legal_structure')=='other') block @else none @endif @else @if(isset($user->organization->legal_structure) && (!in_array($user->organization->legal_structure,config('enums.legal')) || $user->organization->legal_structure=='other') ) ) block @else none @endif @endif">
                                <input id="legal-other-input" type="text"
                                       class="@error('legal-other') is-invalid @enderror"
                                       name="legal_other" value="@if(old('legal_structure') != null){{old('legal_other')}}@else @if(old('legal_structure')== null && !in_array($user->organization->legal_structure,config('enums.legal')) ) @if(isset($user->organization->legal_structure)){{$user->organization->legal_structure}}@endif @endif @endif">
                                <label for="legal-other"
                                       class="control-label">{{trans('register_step_2.legal-other')}}</label>
                                @error('legal_other')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group secondary" style="display: none">
                                <select class="form-control" name="type" id="org_type">
                                    @foreach(config('enums.org_type') as $item)
                                        <option @if(old('type')==$item)selected
                                                @elseif($user->organization->type==$item) selected
                                                @endif value="{{$item}}">{{trans('org_type.'.$item)}}</option>
                                    @endforeach
                                </select>
                                <label for="org_type"
                                       class="control-label">{{trans('register_step_2.org_type')}} </label>
                                @error('type')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="field_of_activities" id="fields_of_activity"
                                >
                                    <option value=""></option>
                                    @foreach(config('enums.fields_of_activity') as $item)
                                        <option @if(old('field_of_activities')==$item)selected
                                                @elseif($user->organization->field_of_activities==$item) selected
                                                @endif value="{{$item}}">{{trans('fields_of_activity.'.$item)}}</option>
                                    @endforeach
                                </select>
                                <label for="fields_of_activity"
                                       class="control-label">{{trans('register_step_2.fields_of_activity')}} </label>
                                @error('field_of_activities')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="number_of_employees" id="number_of_employees">
                                    <option value=""></option>
                                    @foreach(config('enums.number_of_employees') as $item)
                                        <option @if(old('number_of_employees')==$item)selected
                                                @elseif($user->organization->number_of_employees==$item) selected
                                                @endif value="{{$item}}">{{$item}}</option>
                                    @endforeach
                                </select>
                                <label for="number_of_employees"
                                       class="control-label">{{trans('register_step_2.number_of_employees')}}</label>
                                @error('number_of_employees')
                                <span class="number_of_employees" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group height-textarea">
                                {{ Form::textarea("about_company",isset($about_company)? $about_company:null,array('rows'=>3))}}
                                <label for="about_company"
                                       class="control-label">{{trans('register_step_2.about_company')}}</label>
                                {!! $errors->first("about_company", '<p style="color:red" class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="col-lg-4 z-index-1">

                            <div id="dynamic_field3">
                                @if(old('key_address_count')==null)
                                    @for($i=0;$i<count($user->address);$i++)
                                        <div class="mb-3 @if($i != 0) deleting3 @endif position-relative"
                                             id="row3_{{$i+1}}">
                                            <div class="form-group">
                                                <input class="form-control @if($i != 0) deleting-input3_1 @endif"
                                                       id="address{{$i+1}}" required
                                                       name="address[]"
                                                       value="{{old('address')[$i] ?  old('address')[$i] : $user->address[$i]->address}}"
                                                       type="text">
                                                <label for="address{{$i+1}}" id="address_label{{$i}}"
                                                       class="control-label">{{trans('register_step_2.address')}} {{trans('register_step_2.'.$type=$user->address[$i]->address_type?$user->address[$i]->address_type:'business')}}
                                                    <span class="star">*</span></label>
                                            </div>
                                            <div class="form-group">
                                                <select class="mt-2 address_type" name="address_type[]" id="" data-id="{{$i}}">
                                                    {{--<option value=""></option>--}}
                                                    <option @if($user->address[$i]->address_type == 'legal') selected
                                                            @endif value="legal">{{trans('register_step_2.legal')}}</option>
                                                    <option @if($user->address[$i]->address_type =='business' || !$user->address[$i]->address_type) selected
                                                            @endif  value="business">{{trans('register_step_2.business')}}</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <select name="country[]" id="address-country{{$i}}" required>
                                                    <option></option>
                                                    @foreach($countries as $item)
                                                        <option @if(old('country')[$i]==$item->id) selected
                                                                @elseif($user->address[$i]->country_id ==$item->id) selected
                                                                @endif value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                                                    @endforeach
                                                </select>
                                                <label for="address-country"
                                                       class="control-label">{{trans('register_step_2.country')}}
                                                    <span
                                                            class="star">*</span></label>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="city[]"
                                                       class="@error('city.'.$i) is-invalid @enderror @if($i!= 0) deleting-input3_{{$i}} @endif"
                                                       id="City{{$i+1}}"
                                                       value=" {{old('city')[$i] ?  old('city')[$i] : $user->address[$i]->city}}"
                                                       required>
                                                <label for="City "
                                                       class="control-label">{{trans('register_step_2.city')}}
                                                    <span
                                                            class="star">*</span></label>
                                                @error('city.'.$i)
                                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <input id="zip{{$i+1}}" type="text"
                                                       class="@error('zip.'.$i) is-invalid @enderror @if($i!= 0) deleting-input3_{{$i}} @endif"
                                                       name="zip[]"
                                                       value="{{old('zip')[$i] ?  old('zip')[$i] : $user->address[$i]->zip}}"
                                                       required>
                                                <label for="zip"
                                                       class="control-label">{{trans('register_step_2.zip')}}
                                                    <span
                                                            class="star">*</span></label>
                                                @error('zip.'.$i)
                                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            @if($i!=0)
                                                <button type="button" name="remove" id="{{$i+1}}"
                                                        class="btn btn-danger btn-danger-2 deleting-button3 btn_remove 3">{{trans('register_step_2.delete')}}
                                                </button>
                                            @endif
                                        </div>
                                    @endfor
                                @else
                                    @for($i=0;$i<old('key_address_count');$i++)
                                        <div class="@if($i != 0) deleting3 @endif position-relative"
                                             id="row3_{{$i+1}}">
                                            <div class="form-group">
                                                <input class="form-control @if($i != 0) deleting-input3_1 @endif"
                                                       id="address{{$i+1}}" required
                                                       name="address[]"
                                                       value="{{old('address')[$i]}}"
                                                       type="text">
                                                <label for="address{{$i+1}}" id="address_label_{{$i}}"
                                                       class="control-label">{{trans('register_step_2.address')}} {{trans('register_step_2.'.$type=$user->address[$i]->address_type?$user->address[$i]->address_type:'business')}}
                                                    <span class="star">*</span></label>
                                            </div>
                                            <div class="form-group">
                                                <select class="mt-2 address_type" name="address_type[]" id="" data-id="{{$i}}">
                                                    <option value=""></option>
                                                    <option @if(old('address_type')[$i]=='legal') selected
                                                            @endif value="legal">{{trans('register_step_2.legal')}}</option>
                                                    <option @if(old('address_type')[$i]=='business') selected
                                                            @endif  value="business">{{trans('register_step_2.business')}}</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <select class="mt-2" name="country[]" id="address-country{{$i}}"
                                                        required>
                                                    <option></option>
                                                    @foreach($countries as $item)
                                                        <option @if(old('country')[$i]==$item->id) selected
                                                                @endif value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                                                    @endforeach
                                                </select>
                                                <label for="address-country"
                                                       class="control-label">{{trans('register_step_2.country')}}
                                                    <span
                                                            class="star">*</span></label>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="city[]"
                                                       class="@error('city.'.$i) is-invalid @enderror @if($i!= 0) deleting-input3_{{$i}} @endif"
                                                       id="City{{$i+1}}"
                                                       value=" {{old('city')[$i]}}"
                                                       required>
                                                <label for="City "
                                                       class="control-label">{{trans('register_step_2.city')}}
                                                    <span
                                                            class="star">*</span></label>
                                                @error('city.'.$i)
                                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <input id="zip{{$i+1}}" type="text"
                                                       class="@error('zip.'.$i) is-invalid @enderror @if($i!= 0) deleting-input3_{{$i}} @endif"
                                                       name="zip[]"
                                                       value="{{old('zip')[$i]}}"
                                                       required>
                                                <label for="zip"
                                                       class="control-label">{{trans('register_step_2.zip')}}
                                                    <span
                                                            class="star">*</span></label>
                                                @error('zip.'.$i)
                                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            @if($i!=0)
                                                <button type="button" name="remove" id="{{$i+1}}"
                                                        class="btn btn-danger deleting-button3 btn_remove 3">{{trans('register_step_2.delete')}}
                                                </button>
                                            @endif
                                        </div>
                                    @endfor
                                @endif

                                <input type="hidden" name="key_address_count" id="key_address_count"
                                       value="{{old('key_address_count') ? old('key_address_count') : count($user->address)}}">
                            </div>
                            <button type="button" name="add3" id="add3"
                                    class="add-input font-bold">{{trans('register_step_2.add_address')}}</button>
                            <input type="hidden" name="is_organization" value="{{$user->is_organization}}">

                            <div id="dynamic_field4">
                                @if(old('key_phone_count')==null && isset($phone))
                                    @for($i=0;$i<count($phone);$i++)
                                        <div id="row4_{{$i+1}}"
                                             class="dynamic-added @if($i != 0) deleting4 @endif form-group ">
                                            <div class="form-group">
                                                <input name="phone[]" class="@error('phone.'.$i) is-invalid @enderror phone @if($i != 0)deleting-input4_1 @endif "
                                                       value="{{old('phone')[$i] ? old('phone')[$i] : $phone[$i]['phone']}}">
                                                @if($i!=0)
                                                    <button type="button" name="remove" data-id="{{$i+1}}"
                                                            class="btn btn-danger deleting-button4 btn_remove 4">X
                                                    </button>
                                                @endif
                                                <label id="phone_label_{{$i}}"  class="control-label">{{trans('register_step_2.phone')}} {{trans('register_step_2.'.$type=$phone[$i]['type']?$phone[$i]['type']:'work')}} <span
                                                            class="star">*</span></label>
                                                @error('phone.'.$i)
                                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <select class="phone_type" name="phone_type[]" id="phone_type_{{$i}}" data-id="{{$i}}">
                                                    {{--<option value=""></option>--}}
                                                    @foreach(config('enums.phone_type') as $item)
                                                        @php($selected = "")
                                                        @if(!$phone[$i]['type'] && $item=='work')
                                                            @php($selected = 'selected')
                                                        @elseif($phone[$i]['type']==$item)
                                                            @php($selected = 'selected')
                                                        @endif
                                                        <option {{$selected}} value="{{$item}}">{{trans('register_step_2.'.$item)}}</option>
                                                    @endforeach
                                                </select>
                                                {{--<label for="phone_type_{{$i}}"  class="control-label">{{trans('register_step_2.phone_type')}}</label>--}}
                                            </div>
                                            <input type="hidden" name="key_phone_count" id="key_phone_count"
                                                   value="{{count($phone)}}">
                                        </div>
                                    @endfor
                                @elseif(!isset($phone) && old('key_phone_count')==null )
                                    <div class="form-group" id="row4_1">
                                        <div class="form-group">
                                            <input class="phone @error('phone.0') is-invalid @enderror" required
                                                   name="phone[]" id="phone1"
                                                   value="{{old('phone')[0]}}">
                                            <label for="phone-0" id="phone_label_0"
                                                   class="control-label">{{trans('register_step_2.phone')}} <span
                                                        class="star">*</span></label>
                                            @error('phone.0')
                                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <select class="phone_type" name="phone_type[]" id="phone_type_0" data-id="0">
                                                <option value=""></option>
                                                @foreach(config('enums.phone_type') as $item)
                                                    <option @if(old('phone_type')[0]==$item) selected
                                                            @endif value="{{$item}}">{{trans('register_step_2.'.$item)}}</option>
                                                @endforeach
                                            </select>
                                            <label for="phone_type_0"
                                                   class="control-label">{{trans('register_step_2.phone_type')}}</label>

                                        </div>
                                        <input type="hidden" name="key_phone_count" id="key_phone_count"
                                               value="1">
                                    </div>

                                @else
                                    @for($i=0;$i<old('key_phone_count');$i++)
                                        <div id="row4_{{$i+1}}"
                                             class="dynamic-added @if($i != 0) deleting4 @endif form-group ">
                                            <div  class="form-group" >
                                                <input name="phone[]" class="@error('phone.'.$i) is-invalid @enderror phone @if($i != 0)deleting-input4_1 @endif "
                                                       value="{{old('phone')[$i]}}">
                                                @if($i!=0)
                                                    <button type="button" name="remove" data-id="{{$i+1}}"
                                                            class="btn btn-danger deleting-button4 btn_remove 4">X
                                                    </button>
                                                @endif
                                                <label id="phone_label_{{$i}}"
                                                       class="control-label">{{trans('register_step_2.phone')}}{{trans('register_step_2.'.$type=$phone[$i]['type']?$phone[$i]['type']:'work')}}@if($i==0)
                                                        <span
                                                                class="star">*</span>@endif</label>
                                                @error('phone.'.$i)
                                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div  class="form-group">
                                                <select class="phone_type" name="phone_type[]" id="phone_type_{{$i}}"  data-id="{{$i}}">
                                                    {{--<option value=""></option>--}}
                                                    @foreach(config('enums.phone_type') as $item)
                                                        <option @if(old('phone_type')[$i]==$item) selected @endif value="{{$item}}">{{trans('register_step_2.'.$item)}}</option>
                                                    @endforeach
                                                </select>
                                                {{--<label for="phone_type_{{$i}}"  class="control-label">{{trans('register_step_2.phone_type')}}</label>--}}
                                            </div>
                                            <input type="hidden" name="key_phone_count" id="key_phone_count"
                                                   value="{{old('key_phone_count')}}">
                                        </div>
                                    @endfor
                                @endif

                            </div>
                            <button type="button" name="add4" id="add4"
                                    class="add-input font-bold">{{trans('register_step_2.add_phone')}}</button>
                            <div class="form-group">
                                <input id="email" type="text" class="@error('email') is-invalid @enderror" name="email"
                                       value="{{old('email') ? old('email') : $user->email }}" required>
                                <label for="email" class="control-label">{{trans('register_step_2.email')}} <span
                                            class="star">*</span></label>
                                @error('email')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input  class="@error('website') is-invalid @enderror" name="website" id="web"
                                       value="{{old('website') ? old('website') : $user->website }}" data-required="no">
                                <label for="web" class="control-label">{{trans('register_step_2.web')}}</label>
                                @error('website')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>


                        <div class="col-lg-12">
                            <div class="d-flex justify-content-end mt-5">
                                <div class="text-center">
                                    <button type="submit"
                                            class="btn btn-orange-gradient">{{trans('profile_tables.save')}}</button>
                                </div>
                            </div>
                        </div>

                    </div>
                    <input type="hidden" name="id" value="{{$user->id}}">
                    {!! Form::close() !!}
                </div>
                @if(Auth::user()->can('changePassword_profile'))
                    <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="company-tab">
                        <div class="row">
                            <div class="col-lg-4">
                                <h5 id="pass-success" style="display: none">Password is changed successfully!</h5>
                                <form method="POST" action="{{route('changeUserPassword',$user->id)}}"
                                      enctype="multipart/form-data">
                                    {{method_field('PATCH')}}
                                    @csrf
                                    <div class="form-group">
                                        <input type="password" required id="password" name="password"
                                               autocomplete="new-password"
                                               class="@error('password') is-invalid @enderror">
                                        <label for="password"
                                               class="control-label">{{trans('register_step_1.new_password')}}
                                            <span
                                                    class="star">*</span></label>
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback">{{ $errors->first('password') }}</span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <input type="Password" id="password-confirm" name="password_confirmation"
                                               required
                                               autocomplete="new-password"
                                               class="@error('password-confirm') is-invalid @enderror">
                                        <label for="password-confirm"
                                               class="control-label">{{trans('register_step_1.confirm_pass')}} <span
                                                    class="star">*</span></label>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="invalid-feedback">{{ $errors->first('password_confirmation') }}</span>
                                        @endif
                                    </div>

                                    <div class="d-flex justify-content-end mt-5">
                                        <div class="text-center">
                                            <button type="submit"
                                                    class="btn btn-orange-gradient">{{trans('profile_tables.change_password')}}</button>
                                        </div>
                                    </div>

                                </form>
                            </div>

                        </div>
                        <div class="col-lg-12">
                            <div class="d-flex justify-content-end mt-5">
                                <div class="text-center">
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                {{--tab 3--}}
                {{--<div class="tab-pane fade" id="bank" role="tabpanel" aria-labelledby="bank-tab">--}}
                    {{--<div class="row">--}}

                        {{--<div class="col-lg-4">--}}
                            {{--<form method="POST" action="{{route('bankDetails',$user->id)}}"--}}
                                  {{--enctype="multipart/form-data">--}}
                                {{--{{method_field('PATCH')}}--}}
                                {{--@csrf--}}
                                {{--<div class="form-group">--}}
                                    {{--<input required--}}
                                           {{--type="text" class="@error('address') is-invalid @enderror"--}}
                                           {{--name="address"--}}
                                           {{--value="{{old('address') ? old('address') : ($user->bankDetails ? $user->bankDetails->address : null ) }}">--}}
                                    {{--<label for="address"--}}
                                           {{--class="control-label">{{trans('bank_details.address')}}<span--}}
                                                {{--class="star">*</span></label>--}}
                                    {{--@if ($errors->has('address'))--}}
                                        {{--<span class="invalid-feedback">{{ $errors->first('address') }}</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}


                                {{--<div class="form-group">--}}
                                    {{--<input required--}}
                                           {{--type="text" class="@error('bank') is-invalid @enderror"--}}
                                           {{--name="bank"--}}
                                           {{--value="{{old('bank') ? old('bank') : ($user->bankDetails ? $user->bankDetails->bank : null ) }}">--}}
                                    {{--<label for="bank"--}}
                                           {{--class="control-label">{{trans('bank_details.bank')}}<span--}}
                                                {{--class="star">*</span></label>--}}
                                    {{--@if ($errors->has('bank'))--}}
                                        {{--<span class="invalid-feedback">{{ $errors->first('bank') }}</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}


                                {{--<div class="form-group">--}}
                                    {{--<input required--}}
                                           {{--type="text" class="@error('tax') is-invalid @enderror"--}}
                                           {{--name="tax"--}}
                                           {{--value="{{old('tax') ? old('tax') : ($user->bankDetails ? $user->bankDetails->tax : null ) }}">--}}
                                    {{--<label for="bank"--}}
                                           {{--class="control-label">{{trans('bank_details.tax')}}<span--}}
                                                {{--class="star">*</span></label>--}}
                                    {{--@if ($errors->has('tax'))--}}
                                        {{--<span class="invalid-feedback">{{ $errors->first('tax') }}</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                                {{--<div class="form-group">--}}
                                    {{--<input required--}}
                                           {{--type="text" class="@error('account') is-invalid @enderror"--}}
                                           {{--name="account"--}}
                                           {{--value="{{old('account') ? old('account') : ($user->bankDetails ? $user->bankDetails->account : null ) }}">--}}
                                    {{--<label for="bank"--}}
                                           {{--class="control-label">{{trans('bank_details.account')}}<span--}}
                                                {{--class="star">*</span></label>--}}
                                    {{--@if ($errors->has('account'))--}}
                                        {{--<span class="invalid-feedback">{{ $errors->first('account') }}</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                                {{--<div class="d-flex justify-content-end mt-5">--}}
                                    {{--<div class="text-center">--}}
                                        {{--<button type="submit"--}}
                                                {{--class="btn btn-orange-gradient">{{trans('profile_tables.save')}}--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                            {{--</form>--}}

                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<div class="col-lg-12">--}}
                        {{--<div class="d-flex justify-content-end mt-5">--}}
                            {{--<div class="text-center">--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="/js/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
    <script>

        @if ($errors->has('password') || $errors->has('password_confirmation'))
         $('a[href="#settings"]').tab('show');
        @endif



        $(document).ready(function () {
            $('#legal_structure').on('change', function () {
                $('#legal-other').css('display', 'none');
                let val = $(this).val();
                if (val == 'other') {
                    $('#legal-other').css('display', 'block');
                }
                else {
                    $('#legal-other-input').val('');
                }
            })

            var select_phone = '<select class="form-control mt-2 phone_type" name="phone_type[]"><option></option>';
                @foreach(config('enums.phone_type') as $item)
            {
                select_phone += "<option value='{{$item}}'>{{$item}}</option>"
            }
            @endforeach
                select_phone += '</select>';
            var key_phone = $('#key_phone_count').val();

            var a = key_phone;
            var sum1 = key_phone;
            let type;
            $('#add4').on('click', function () {

                if (sum1 <= 5) {
                    ++a;
                    var select_phone = '<select id="phone_type_'+a+'" data-id="' + a + '" class="form-control phone_type" name="phone_type[]"><option></option>';
                        @foreach(config('enums.phone_type') as $item)
                    {
                        select_phone += "<option value='{{$item}}'>{{trans('register_step_2.'.$item)}}</option>"
                    }
                    @endforeach
                        select_phone += '</select>';
                    $('#dynamic_field4').append('<div id="row4_' + a + '" class="dynamic-added deleting4">' +
                        '<div class="form-group"><input id="phone' + a + '" class="phone" required name="phone[]" class="deleting-input4_1 " value="">' +
                        '<label id="phone_label_'+a+'" class="control-label" for="phone' + a + '" >{{trans('register_step_2.phone')}} '  + '</label>'+
                        '<button type="button" name="remove" data-id="' + a + '" class="btn btn-danger deleting-button4 btn_remove 4">X</button></div><div class="form-group">' +
                        select_phone+
                        '<label class="control-label" for="phone_type_"'+a+'>{{trans('register_step_2.phone_type')}} '  + '</label></div></div>');
                    sum1++;
                    if(sum1==5){
                        $('#add4').css('display','none')
                    }
                }
                $('#key_phone_count').val(a);
            });

            $(document).on('click', '.4', function () {
                var button_id3 = $(this).attr("data-id");
                var key_phone_val = $('#key_phone_count').val();
                $('#key_phone_count').val(key_phone_val - 1);
                $('#row4_' + button_id3).remove();
                sum1--;
                a--;
            });
            var key_add = $('#key_address_count').val();
            var l = key_add;
            var sum = key_add;


            $('#add3').on('click', function () {

                var select = '<select class="form-control" name="country[]" required id="address-country' + l + '"><option></option>';
                @foreach($countries as $item)
                    select += "<option value ={{$item->id}}>{{trans('countries.'.$item->code)}}</option>"
                @endforeach
                    select += '</select>';


                if (sum <= 5) {
                    ++l;
                    $('#dynamic_field3').append(
                        '<div id="row3_' + l + '" class="mb-4 deleting3 position-relative">' +
                        '<div class="form-group">' +
                        '<input required  name="address[]" class="form-control deleting-input3_1 " value="" id="address' + l + '">' +
                        '<label id="address_label_'+l+'" for="address' + l + '" class="control-label">{{trans('register_step_2.address')}} '  + '</label>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<select  data-id="'+l+'" id="address_type_'+l+'" class="address_type" name="address_type[]"><option value=""></option><option value="legal">{{trans('register_step_2.legal')}}</option><option value="business">{{trans('register_step_2.business')}}</option></select>' +
                        '<label for="address_type_' + l + '" class="control-label">{{trans('register_step_2.address_type')}} '  + '</label>' +
                        '</div>' +
                        '<div class="form-group">' +
                        select + '' +
                        '<label for="address-country' + l + '" class="control-label">{{trans('register_step_2.country')}} '  + '</label>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<input id="city' + l + '" type="text" required name="city[]" class="deleting-input3_1 " value="">' +
                        '<label for="City' + l + '" class="control-label">{{trans('register_step_2.city')}} ' + '</label>' +
                        '</div>' +
                        '<div class="form-group"> ' +
                        '<input type="text" required  name="zip[]" class="deleting-input3_1" value="" id="zip' + l + '">' +
                        '<label for="zip' + l + '" class="control-label">{{trans('register_step_2.zip')}}'  + '</label>' +
                        '</div>' +
                        '<button type="button" name="remove" id="' + l + '" class="btn btn-danger deleting-button3 btn_remove 3 btn-danger-2">{{trans('register_step_2.delete')}} '  + '</button>' +
                        '</div>');
                    sum++;
                    if(sum==5){
                        $('#add3').css('display','none')
                    }
                }
                $('#key_address_count').val(l);
            });

            $(document).on('click', '.3', function () {
                var button_id3 = $(this).attr("id");

                var key_address_val = $('#key_address_count').val();
                $('#key_address_count').val(key_address_val - 1);
                $('#row3_' + button_id3).remove();
                sum--;
                l--;
            });
            $(document).on('change', '.phone_type', function () {
                type = $(this).find(':selected').text();
                $('#phone_label_' + $(this).attr('data-id')).text('{{trans('register_step_2.phone')}}' + ' ' + type);

            });
            $(document).on('change', '.address_type', function () {
                type = $(this).find(':selected').text();
                $('#address_label_' + $(this).attr('data-id')).text('{{trans('register_step_2.address')}}' + ' ' + type);

            });
        });


    </script>
@endsection











