@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')
    <section class="position-relative bg-grey">
        @include('profile.menu')

        <div class="profile-container float-right">
            <div class="profile-table" style="max-width: inherit">
                <div class="d-flex justify-content-between flex-wrap align-items-center">
                    @permission('create_user')
                    <a href="{{route('users.create')}}"
                       class="btn btn-orange-gradient mr-1 mb-2">{{trans('profile_tables.create')}}</a>
                    @endpermission

                    <form id="myForm1" action="{{route('users.index')}}"
                          class="d-flex flex-wrap position-relative align-items-start search-select-form">
                        <div class="position-relative profile-search-form mr-3 mb-2">
                            <input value="{{$searchUsername}}" id="searchUsername" name="searchUsername"
                                   class="form-control" type="search"
                                   placeholder="{{trans('profile_tables.searchUsername')}}…" aria-label="Search">
                            <button class="btn search_btn" type="button"><img src="/img/search.svg" alt="">
                            </button>
                        </div>
                        <div class="position-relative profile-search-form mr-3 mb-2">
                            <input value="{{$searchFirstName}}" id="searchFirstName" name="searchFirstName"
                                   class="form-control" type="search"
                                   placeholder="{{trans('profile_tables.searchFirstName')}}…" aria-label="Search">
                            <button class="btn search_btn" type="button"><img src="/img/search.svg" alt="">
                            </button>
                        </div>
                        <div class="position-relative profile-search-form mr-3 mb-2">
                            <input value="{{$searchLastName}}" id="searchLastName" name="searchLastName"
                                   class="form-control" type="search"
                                   placeholder="{{trans('profile_tables.searchLastName')}}…" aria-label="Search">
                            <button class="btn search_btn" type="button"><img src="/img/search.svg" alt="">
                            </button>
                        </div>
                    </form>
                </div>

                <div class="table-responsive new-table">
                    <table class="table mobile-table">
                        <thead>
                        <tr class="table-tr-th">
                            <th>{{trans('users.username')}}</th>
                            <th>{{trans('users.full_name')}}</th>
                            <th>{{trans('users.joined')}}</th>
                            <th style="width: 190px">
                                <div class="form-group mb-0">
                                    <form id="myFormRole" action="{{route('users.index')}}">
                                        <select class="form-control" name="role" id="role">
                                            <option value="">{{trans('profile_tables.role')}}</option>
                                            @foreach($roles as $item)
                                                @if($item->name =='admin')
                                                    @role('admin')
                                                        <option @if($role==$item->id) selected
                                                                @endif value="{{$item->id}}">{{trans('users.'.$item->name)}}</option>
                                                    @endrole
                                                @else
                                                    <option @if($role==$item->name) selected
                                                            @endif  value="{{$item->id}}">{{trans('users.'.$item->name)}}</option>
                                                @endif
                                            @endforeach
                                        </select>

                                    </form>
                                </div>
                            </th>
                            <th>
                                <div class="form-group mb-0" style="width: 100px">
                                    <form id="myFormStatus" action="{{route('users.index')}}">
                                        <select name="status" id="user-status">
                                            <option @if($status==0) selected
                                                    @endif  value="0">{{trans('profile_tables.closed')}}</option>
                                            <option @if($status==1) selected
                                                    @endif  value="1">{{trans('profile_tables.active')}}</option>
                                        </select>
                                        {{--<label for="user-status" class="control-label">{{trans('profile_tables.status')}}</label>--}}
                                    </form>
                                </div>
                            </th>
                            <th class="text-center">{{trans('profile_tables.action')}}</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                        $array = [];
                        $users->map(function ($item, $key) use ($searchUsername, &$array) {
                            if (strcasecmp($searchUsername, $item->username) < 0) {
                                array_push($array, $item);

                            } elseif (strcasecmp($searchUsername, $item->username) > 0) {
                                array_push($array, $item);
                            } else {

                                array_unshift($array, $item);
                            }
                        });
                        ?>
                        @foreach($array as $item)
                            @if($item->role == 'admin')
                                @if(isset(\Illuminate\Support\Facades\Auth::user()->role[0]) && \Illuminate\Support\Facades\Auth::user()->role[0]->name == 'admin')
                                    <tr class="col-sd-3">
                                        <td data-label="{{trans('users.username')}}">{{$item->username}}
                                            <div class="d-inline-block">
                                                <span tooltip="{{$item->email}}" flow="right" class="tooltip-img-table">
                                                    <img src="/img/envelope.svg" alt="envelope" width="15" class="ml-1"></span>
                                            </div>
                                        </td>
                                        <td data-label="{{trans('users.full_name')}}">{{$item->first_name}} {{$item->last_name}}</td>
                                        <td data-label="{{trans('users.joined')}}"
                                            class="no-wrap">{{\Carbon\Carbon::parse($item->created_at)->format('d F Y')}}</td>
                                        <td data-label="{{trans('users.role')}}">@if(isset($item->role[0])){{trans('users.'.$item->role[0]->name)}} @endif</td>
                                        <td data-label="{{trans('users.status')}}" style="width: 115px;">
                                            @permission('close_user')
                                            {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['user-status-change', $item->id]]) !!}
                                            <select class="form-control status" name="status">
                                                <option @if($item->status == '0') selected
                                                        @endif value="0">{{trans('profile_tables.closed')}}</option>
                                                <option @if($item->status == '1') selected
                                                        @endif value="1">{{trans('profile_tables.active')}}</option>
                                            </select>
                                            {!! Form::close() !!}
                                            @endpermission
                                        </td>
                                        <td data-label="{{trans('profile_tables.action')}}">
                                            <div class="table-buttons d-flex justify-content-center">

                                                <a href="{{route('usersLogin',$item->id)}}" title="{{trans('users.login')}}">{{trans('users.login')}}</a>
                                                {{--<a href="{{route('user-permission',$item->id)}}" title="{{trans('profile_tables.lock')}}"><img src="/img/lock.svg" alt=""></a>--}}
                                                @permission('edit_user')
                                                <a href="{{route('users.edit',$item->id)}}"
                                                   title="{{trans('profile_tables.edit')}}"><img src="/img/edit.svg"
                                                                                                 alt=""></a>
                                                @endpermission

                                                @permission('delete_user')
                                                {!! Form::open([
                                                'method'=>'DELETE',
                                                'route' => ['users.destroy',$item->id],
                                                'style' => 'display:inline'
                                                ]) !!}
                                                {!! Form::button('<img src="/img/delete.svg">', array(
                                                        'type' => 'submit',
                                                        'class' => '',
                                                        'title' => trans('user.delete'),
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                                {!! Form::close() !!}
                                                @endpermission

                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @else
                                <tr class="col-sd-3">
                                    <td data-label="{{trans('users.username')}}">{{$item->username}}
                                        <div class="d-inline-block">
                                            {{--<span tooltip="{{$item->email}}" flow="right" class="tooltip-img-table">--}}
                                                {{--<img src="/img/envelope.svg" alt="envelope" width="15"--}}
                                                     {{--class="ml-1"></span>--}}
                                        </div>
                                    </td>
                                    <td data-label="{{trans('users.full_name')}}">{{$item->first_name}} {{$item->last_name}}</td>
                                    <td data-label="{{trans('users.joined')}}"
                                        class="no-wrap">{{\Carbon\Carbon::parse($item->created_at)->format('d F Y')}}</td>
                                    <td data-label="{{trans('users.role')}}">@if(isset($item->role[0])) {{trans('users.'.$item->role[0]->name)}} @endif</td>
                                    <td data-label="{{trans('users.status')}}" style="width: 115px;">
                                        @permission('close_user')
                                        {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['user-status-change', $item->id]]) !!}
                                        <select class="form-control status" name="status">
                                            <option @if($item->status == '0') selected
                                                    @endif value="0">{{trans('profile_tables.closed')}}</option>
                                            <option @if($item->status == '1') selected
                                                    @endif value="1">{{trans('profile_tables.active')}}</option>
                                        </select>
                                        {!! Form::close() !!}
                                        @endpermission
                                    </td>
                                    <td data-label="{{trans('profile_tables.action')}}">
                                        <div class="table-buttons d-flex justify-content-center align-items-center">
                                            <a href="{{route('usersLogin',$item->id)}}" title="{{trans('users.login')}}">{{trans('users.login')}}</a>
                                            {{--<a href="{{route('user-permission',$item->id)}}" title="{{trans('profile_tables.lock')}}"><img src="/img/lock.svg" alt=""></a>--}}
                                            @permission('edit_user')
                                            <a href="{{route('users.edit',$item->id)}}"
                                               title="{{trans('profile_tables.edit')}}"><img src="/img/edit.svg" alt="" style="height: 17px;"></a>
                                            @endpermission
                                            <span tooltip="{{$item->email}}" flow="left" class="tooltip-img-table"> <a
                                                        href="mailto:{{$item->email}}"
                                                        style="color: #014A7C;"><img src="/img/envelope.svg" alt="envelope" width="15"
                                                                                     class="ml-1" style="width: 19px"></a></span>
                                            @permission('delete_user')
                                            {!! Form::open([
                                            'method'=>'DELETE',
                                            'route' => ['users.destroy',$item->id],
                                            'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<img src="/img/delete.svg">', array(
                                                    'type' => 'submit',
                                                    'class' => '',
                                                    'title' => trans('user.delete'),
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                            {!! Form::close() !!}
                                            @endpermission

                                        </div>

                                    </td>
                                </tr>
                            @endif

                        @endforeach

                        </tbody>
                    </table>
                </div>

                <div class="d-flex justify-content-end my-pagination align-items-center">
                    <form id="myForm" action="{{route('users.index')}}">
                        <div>
                            <label for="rr">{{trans('jobs.item_per_page')}}:</label>

                            <select name="order_by" id="rr">
                                <option @if($order==20) selected @endif value="20">20</option>
                                <option @if($order==50) selected @endif value="50">50</option>
                                <option @if($order==100) selected @endif value="100">100</option>
                            </select>

                        </div>
                    </form>
                    {{--<span>1-20 of 23</span>--}}

                    <div class="pagination-links d-flex">
                        {{ $users->appends(['search' => Request::get('search'),'order_by' => Request::get('order_by'),'role' => Request::get('role')])->links('vendor.pagination.bootstrap-4')}}
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('.status').on('change', function () {
                $(this).closest('form').submit();
            })

            $('#rr').on('change', function () {
                var searchUsername = $('#searchUsername').val();
                var searchFirstName = $('#searchFirstName').val();
                var searchLastName = $('#searchLastName').val();

                var status = $('#user-status').val();
                var role = $('#role').val();
                var input = $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "searchUsername").val(searchUsername).appendTo('#myForm');
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "searchFirstName").val(searchFirstName).appendTo('#myForm');
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "searchLastName").val(searchLastName).appendTo('#myForm');
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "status").val(status).appendTo('#myForm');
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "role").val(role).appendTo('#myForm');
                $("#myForm").submit();

            });
            $('#role').on('change', function () {
                var order = $('#rr').val();
                var searchUsername = $('#searchUsername').val();
                var searchFirstName = $('#searchFirstName').val();
                var searchLastName = $('#searchLastName').val();
                var status = $('#user-status').val();
                var input = $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "order_by").val(order).appendTo('#myFormRole');
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "searchUsername").val(searchUsername).appendTo('#myFormRole');
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "searchFirstName").val(searchFirstName).appendTo('#myFormRole');
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "searchLastName").val(searchLastName).appendTo('#myFormRole');
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "status").val(status).appendTo('#myFormRole');
                $("#myFormRole").submit();


            });
            $('#user-status').on('change', function () {
                var order = $('#rr').val();
                var role = $('#role').val();
                var searchUsername = $('#searchUsername').val();
                var searchFirstName = $('#searchFirstName').val();
                var searchLastName = $('#searchLastName').val();
                var input = $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "order_by").val(order).appendTo('#myFormStatus');
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "role").val(role).appendTo('#myFormStatus');
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "searchUsername").val(searchUsername).appendTo('#myFormStatus');
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "searchFirstName").val(searchFirstName).appendTo('#myFormStatus');
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "searchLastName").val(searchLastName).appendTo('#myFormStatus');

                $("#myFormStatus").submit();
            })

            $('.search_btn').on('click', function () {
                var order = $('#rr').val();
                var role = $('#role').val();
                var status = $('#user-status').val();
                var input = $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "order_by").val(order).appendTo('#myForm1');
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "status").val(status).appendTo('#myForm1');
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "role").val(role).appendTo('#myForm1');
                $("#myForm1").submit();
            });

            $('#searchUsername,#searchFirstName,#searchLastName').keypress(function (e) {
                var key = e.which;
                if (key == 13)  // the enter key code
                {
                    var order = $('#rr').val();
                    var role = $('#role').val();
                    var status = $('#user-status').val();
                    var input = $("<input>")
                        .attr("type", "hidden")
                        .attr("name", "order_by").val(order).appendTo('#myForm1');
                    $("<input>")
                        .attr("type", "hidden")
                        .attr("name", "status").val(status).appendTo('#myForm1');
                    $("<input>")
                        .attr("type", "hidden")
                        .attr("name", "role").val(role).appendTo('#myForm1');
                    $("#myForm1").submit();
                }
            });
        });
    </script>
@endsection
