@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')
    <section class="position-relative bg-grey">
        @include('profile.menu')

        <div class="profile-container float-right">

            <div class="profile-table">
                <div class="d-flex justify-content-between">
                    <form id="myForm1" action="{{route('appliers.index')}}"
                          class="profile-search-form d-flex position-relative ml-auto">
                        <input value="{{$search}}" id="search" name="search" class="form-control" type="search"
                               placeholder="{{trans('profile_tables.search')}}…" aria-label="Search">
                        <button id="search_btn" class="btn" type="button"><img src="/img/search.svg" alt=""></button>
                    </form>
                </div>

                <div class="table-responsive">
                    <table class="table mobile-table">
                        <thead>
                        <tr class="table-tr-th">
                            <th>{{trans('profile_tables.title')}}</th>
                            <th>{{trans('profile_tables.type')}}</th>
                            <th>{{trans('profile_tables.created_at')}}</th>
                            <th>{{trans('profile_tables.expiration_date')}}</th>
                            <th class="text-center">{{trans('profile_tables.action')}}</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($jobs as $item)
                            @if(isset($item->data))
                                <tr class="col-sd-3">
                                    <td data-label="{{trans('profile_tables.title')}}">@if(isset($item)){{$item->data->title}}@endif</td>
                                    <td data-label="{{trans('profile_tables.type')}}">@if(isset($item)){{trans('types.'.$item->type->key)}}@endif</td>
                                    <td data-label="{{trans('profile_tables.created_at')}}">
                                        {{\App\Job::getOpeningDate($item->created_at)}}
                                        {{--{{Carbon\Carbon::parse($item->created_at)->format('d M Y')}}--}}
                                    </td>
                                    <td data-label="{{trans('profile_tables.expiration_date')}}">
                                        {{\App\Job::getOpeningDate($item->expiration_date)}}
                                        {{--{{Carbon\Carbon::parse($item->expiration_date)->format('d M Y')}}--}}
                                    </td>
                                    <td data-label="{{trans('profile_tables.action')}}">
                                        <div class="table-buttons d-flex justify-content-center">
                                            {{--<a title="{{trans('profile_tables.view')}}" href="{{route('jobs',$item->slug)}}"><img src="/img/visibility.svg" alt="view"></a>--}}
                                            <a title="{{trans('appliers.show')}}" href="{{route('appliers.show',$item->id)}}" class="position-relative notif-cv">{{trans('appliers.view_applicants')}} <span>({{\App\Job::getAppliers($item->id)}})</span></a>
                                        </div>
                                    </td>

                                </tr>
                            @endif
                        @endforeach

                        </tbody>
                    </table>
                </div>

                <div class="d-flex justify-content-end my-pagination align-items-center">
                    <form id="myForm" action="{{route('resources.index')}}">
                        <div >
                            <label for="rr">{{trans('jobs.item_per_page')}}:</label>

                            <select name="order" id="rr">
                                <option @if($order==20) selected @endif value="20">20</option>
                                <option @if($order==50) selected @endif value="50">50</option>
                                <option @if($order==100) selected @endif value="100">100</option>
                            </select>

                        </div>
                    </form>
                    {{ $jobs->appends(['search' => Request::get('search'),'order_by' => Request::get('order_by')])->links('vendor.pagination.bootstrap-4')}}
                    <div class="pagination-links d-flex">

                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
@section('scripts')
    <script>
        $('#rr').on('change',function()
        {
            var search = $('#search').val();
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "search").val(search).appendTo('#myForm');
            $( "#myForm" ).submit();

        });
        $('#search_btn').on('click',function()
        {
            var order = $('#rr').val();
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "order").val(order).appendTo('#myForm1');
            $( "#myForm1" ).submit();
        });
        $('.status').on('change',function()
        {
            $(this).closest('form').submit();
        });
    </script>
@endsection
