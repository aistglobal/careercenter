@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')
    <section class="position-relative bg-grey">
        @include('profile.menu')

        <div class="profile-container float-right">

            <div class="profile-table">
                <h4>{{trans('profile_tables.applicants_for')}} {{$job->data->title}}</h4>
                {{--<a href="{{route('appliers.index') }}" title="Back" class="add btn btn-outline btn-success mt-3">--}}
                {{--<i class="fa fa-arrow-left"></i> Back--}}
                {{--</a>--}}
                <div class="table-responsive">
                    <table class="table mobile-table">
                        <thead>
                        <tr class="table-tr-th">
                            <th>{{trans('profile_tables.full_name')}}</th>
                            <th>{{trans('profile_tables.status')}}</th>
                            <th>{{trans('profile_tables.change_status')}}</th>
                            <th>{{trans('profile_tables.apply_at')}}</th>
                            <th class="text-center">{{trans('profile_tables.action')}}</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($appliers as $item)
                            <tr class="col-sd-3">
                                <td data-label="{{trans('profile_tables.title')}}">{{$item->user->first_name}} {{$item->user->last_name}}</td>
                                <td data-label="{{trans('profile_tables.status')}}"> {{trans('appliers.'.$item->status)}}</td>
                                <td data-label="{{trans('profile_tables.change_status')}}">
                                    {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['applier-status-change', $item->id]]) !!}
                                    <select class="status form-control" name="status">
                                        @foreach(config('enums.applier_status') as $status)
                                            <option @if($item->status == $status) selected
                                                    @endif value="{{$status}}">{{trans('appliers.'.$status)}}</option>
                                        @endforeach
                                    </select>
                                    {{--<label for="job-status" class="control-label"> {{trans('profile_tables.status')}}</label>--}}
                                    {!! Form::close() !!}
                                </td>
                                <td data-label="{{trans('profile_tables.apply_at')}}">
                                    {{\App\Job::getOpeningDate($item->created_at)}}
                                </td>
                                <td data-label="{{trans('profile_tables.action')}}">
                                    <div class="table-buttons d-flex justify-content-center">
                                        <a title="{{trans('appliers.show')}}"
                                           href="{{route('appliers.show-details',$item->id)}}"
                                           class="notif-cv">{{trans('appliers.view_application')}}</a>
                                    </div>
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

                <div class="d-flex justify-content-end my-pagination align-items-center">

                    {{ $appliers->links()}}
                    <div class="pagination-links d-flex">

                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
@section('scripts')
    <script>
        $('#rr').on('change', function () {
            var search = $('#search').val();
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "search").val(search).appendTo('#myForm');
            $("#myForm").submit();

        });
        $('#search_btn').on('click', function () {
            var order = $('#rr').val();
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "order").val(order).appendTo('#myForm1');
            $("#myForm1").submit();
        });
        $('.status').on('change', function () {
            $(this).closest('form').submit();
        });
    </script>
@endsection
