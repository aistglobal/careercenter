@extends('layouts.app')

@section('css')
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')
    <section class="position-relative bg-grey">
        @include('profile.menu')

        <div class="profile-container float-right">
            <div class="profile-tab-content">

                <h5 class="font-bold mb-4">{{trans('appliers.details')}}</h5>
                <div class="table-responsive">
                    <table class="applier-table mb-5">
                        <tr>
                            <td>{{trans('appliers.name')}}:</td>
                            <td>{{$applier->user->first_name}} {{$applier->user->last_name}}</td>
                        </tr>
                        <tr>
                            <td>{{trans('appliers.job_title')}}:</td>
                            <td>{{$applier->job->data->title}}</td>
                        </tr>
                        <tr>
                            <td>{{trans('appliers.application_date')}}:</td>
                            <td>{{\App\Job::getOpeningDate($applier->created_at)}}</td>
                        </tr>
                        @if(isset($applier->cover_letter))
                        <tr>
                            <td>{{trans('appliers.cover_letter')}}:</td>
                            <td>{!! $applier->cover_letter !!}
                            </td>
                        </tr>
                        @endif
                        <tr>
                            <td>{{trans('appliers.status')}}:</td>
                            <td>  {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['applier-status-change', $applier->id]]) !!}
                                <div class="form-group mb-0">
                                    <select name="status" id="status">
                                        @foreach(config('enums.applier_status') as $item)
                                            <option @if($applier->status == $item) selected
                                                    @endif value="{{$item}}">{{trans('appliers.'.$item)}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                {!! Form::close() !!}</td>
                        </tr>
                    </table>
                </div>

                @if(isset($applier->resume))
                    <a target="_blank" href="{{Storage::disk('')->url($applier->resume)}}" class="d-flex align-items-center"><img src="/img/down-cv.svg" alt="" class="mr-2">{{trans('appliers.download_cv')}}</a>
                </p>
                @endif

        </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $('#status').on('change',function()
        {
            $(this).closest('form').submit();
        });
    </script>


@endsection