<section>
    {!! Form::open(['class' => 'form-horizontal','id'=>'cv_experience']) !!}
    <div class="row">
        <div class="col-12 experiences">

            @if(count($resume->experience)!=0)
                @foreach($resume->experience as $item)
                    <div class="edit-field d-flex" data-exp="{{$item->id}}">
                        <p data-id="{{$item->id}}" class="mb-0 mr-auto">{{$item->name}}</p>
                        <span data-id="{{$item->id}}" class="exp_edit"><img src="/img/edu-edit.svg" alt=""></span>
                        <span  data-id="{{$item->id}}" class="exp_remove"><img
                                    src="/img/edu-delete.svg" alt=""></span>
                    </div>
                @endforeach

            @endif
            <input type="hidden" value="" id="exp_hidden" name="exp">
        </div>
        <div class="col-12">
            <h3 id="for_edit_exp"></h3>
        </div>
        <div class="col-lg-6">

            <div class="form-group">
                <input  id="name"
                       class="form-control exp"
                       required name="name"
                       type="text">
                <label class="control-label">{{trans('register_step_2.name')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="name_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input  id="city"
                       class="form-control exp"
                       required name="city"
                       type="text">
                <label class="control-label">{{trans('resume.city')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="city_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select class="exp" name="country_id" id="exp_country">
                    <option value=""></option>
                    @foreach($countries as $item)
                        <option  value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.country')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="country_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input id="position"
                       class="form-control exp"
                       required name="position"
                       type="text">
                <label class="control-label">{{trans('resume.position')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="position_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select class="exp" id="position_field" name="position_field">
                    <option value=""></option>
                    @foreach(config('enums.category') as $item)
                        <option  value="{{$item}}">{{trans('category.'.$item)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.position_field')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="position_field_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input  id="salary"
                       class="form-control exp"
                       name="salary"
                       type="text">
                <label class="control-label">{{trans('resume.salary')}}</label>
                <span class="v-error" id="salary_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select id="currency" name="currency" class="exp">
                    <option value=""></option>
                    @foreach(config('enums.currency') as $item)
                        <option  value="{{$item}}">{{trans('currency.'.$item)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.currency')}}</label>
                <span class="v-error" id="currency_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select class="exp" id="period" name="period">
                    <option value=""></option>
                    @foreach(config('enums.period') as $item)
                        <option  value="{{$item}}">{{trans('period.'.$item)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.period')}}</label>
                <span class="v-error" id="period_error" role="alert"></span>
            </div>
            <div class="buttons d-flex justify-content-between flex-wrap">
                <button type="button" id="exp_add"
                        class="btn btn-orange-gradient">{{trans('resume.add_more')}}</button>
            </div>
        </div>
        <div class="col-lg-6">
            <p>{{trans('resume.from')}}</p>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        {!! Form::selectYear('exp_year_from', 1900, 2019, null,['required'=>'required','id'=>'exp_year_from','class'=>'exp']) !!}
                        <label class="control-label">{{trans('resume.year')}}<span
                                    class="star">*</span></label>
                        <span class="v-error" id="exp_year_from_error" role="alert"></span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <select name="exp_month_from" id="exp_month_from" required>
                            @for($i=0;$i<count(config('enums.months'));$i++)
                                <option value="{{$i+1}}">{{trans('months.'.config('enums.months')[$i])}}</option>
                            @endfor
                        </select>

                        <label class="control-label">{{trans('resume.month')}}</label>
                        <span class="v-error" id="exp_month_from_error" role="alert"></span>
                    </div>
                </div>
                <p class="col-12">{{trans('resume.to')}}</p>
                <div class="col-lg-6">
                    <div class="form-group" id="exp_year">
                        {!! Form::selectYear('exp_year_to', 1900, 2019, null,['id'=>'exp_year_to','class'=>'exp']) !!}
                        <label class="control-label">{{trans('resume.year')}}</label>
                        <span class="v-error" id="exp_year_to_error" role="alert"></span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group" id="exp_month">
                        <select name="exp_month_to" id="exp_month_to">
                            @for($i=0;$i<count(config('enums.months'));$i++)
                                <option value="{{$i+1}}">{{trans('months.'.config('enums.months')[$i])}}</option>
                            @endfor
                        </select>
                        <label class="control-label">{{trans('resume.month')}}</label>
                        <span class="v-error" id="exp_month_to_error" role="alert"></span>
                    </div>
                </div>
            </div>

            <div class="mb-4">
                <div class="checkbox mb-0">
                    <label>
                        <input class="till_now"  type="checkbox"  value="till_now"><i class="helper"></i>{{trans('resume.till_now')}}
                    </label>
                </div>
            </div>

            <div class="form-group">
                {{ Form::textarea("job_description",null,array('rows'=>1,'id'=> 'job_description','class'=>'exp'))}}
                <label for="job_description" class="control-label">{{trans('resume.job_description')}}
                </label>
                <span class="v-error" id="job_description_error" role="alert"></span>
            </div>
            <div class="form-group">
                {{ Form::textarea("main_duties", null,array('rows'=>1,'required'=>'required','id'=> 'main_duties','class'=>'exp'))}}
                <label for="main_duties" class="control-label">{{trans('resume.main_duties')}}<span
                            class="star">*</span>
                </label>
                <span class="v-error" id="main_duties_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input id="supervisor_name" class="form-control exp"
                       name="supervisor_name"
                     type="text">
                <label class="control-label">{{trans('resume.supervisor_name')}}</label>
                <span class="v-error" id="supervisor_name_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input id="employer_numbers" class="form-control exp"
                       name="employer_numbers"
                    type="text">
                <label class="control-label">{{trans('resume.employer_numbers')}}</label>
                <span class="v-error" id="employer_numbers_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input id="reason" class="form-control exp"
                       name="reason" type="text">
                <label class="control-label">{{trans('resume.reason')}}</label>
                <span class="v-error" id="reason_error" role="alert"></span>
            </div>
        </div>

        <input type="hidden" name="cv_id" value="{{$resume->id}}">
        <input type="hidden" id="count_exp"  value="{{count($resume->experience)}}">
    </div>
    {!! Form::close() !!}

</section>