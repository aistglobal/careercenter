<section>
    {!! Form::open(['class' => 'form-horizontal','id'=>'cv_training']) !!}
    <div class="row">
        <div class="col-12 trainings">
            @if(count($resume->training)!=0)
                @foreach($resume->training as $item)
                    <div class="edit-field d-flex" data-training="{{$item->id}}">
                        <p data-id="{{$item->id}}" class="mb-0 mr-auto">{{$item->name}}</p>
                        <span data-id="{{$item->id}}" class="training_edit"><img src="/img/edu-edit.svg" alt=""></span>
                        <span  data-id="{{$item->id}}" class="training_remove"><img
                                    src="/img/edu-delete.svg" alt=""></span>
                    </div>
                @endforeach
            @endif

            <input type="hidden" value="" id="training_hidden" name="training">
        </div>
        <div class="col-12">
            <h3 id="for_edit_training"></h3>
        </div>
        <div class="col-lg-6">

            <div class="form-group">
                <input  id="training_name" class="form-control training"
                       required name="name"
                       type="text">
                <label class="control-label">{{trans('register_step_2.name')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="training_name_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input  id="training_city" class="form-control training"
                       required name="city"
                       type="text">
                <label class="control-label">{{trans('resume.city')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="training_city_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select class="training" id="training_type" name="type">
                    <option value=""></option>
                    @foreach(config('enums.training_type') as $item)
                        <option  value="{{$item}}">{{trans('cv_training.'.$item)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.training_type')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="training_type_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select  class="training" name="country_id" id="training_country">
                    <option value=""></option>
                    @foreach($countries as $item)
                        <option  value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.country')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="training_country_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select class="training" id="training_study_area" name="study_area">
                    <option value=""></option>
                    @foreach(config('enums.category') as $item)
                        <option  value="{{$item}}">{{trans('category.'.$item)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.study_area')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="training_study_area_error" role="alert"></span>
            </div>

            <div class="form-group">
                <input  id="course_name" class="form-control training"
                       name="course_name"
                       type="text">
                <label class="control-label">{{trans('resume.course_name')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="course_name_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select id="certificate_type" name="certificate_type" class="training">
                    <option value=""></option>
                    @foreach(config('enums.certificate_type') as $item)
                        <option  value="{{$item}}">{{trans('cv_training.'.$item)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.certificate_type')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="certificate_type_error" role="alert"></span>
            </div>

            <div class="buttons d-flex justify-content-between flex-wrap">
                <button type="button" id="training_add"
                        class="btn btn-orange-gradient">{{trans('resume.add_more')}}</button>
            </div>
        </div>
        <div class="col-lg-6">
            <p>{{trans('resume.from')}}</p>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        {!! Form::selectYear('training_year_from', 1900, 2019, null,['required'=>'required','id'=>'training_year_from','class'=>'training']) !!}
                        <label class="control-label">{{trans('resume.year')}}<span
                                    class="star">*</span></label>
                        <span class="v-error" id="training_year_from_error" role="alert"></span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <select name="training_month_from" id="training_month_from" required class="training">
                            @for($i=0;$i<count(config('enums.months'));$i++)
                                <option value="{{$i+1}}">{{trans('months.'.config('enums.months')[$i])}}</option>
                            @endfor
                        </select>
                        <label class="control-label">{{trans('resume.month')}}<span
                                    class="star">*</span></label>
                        <span class="v-error" id="training_month_from_error" role="alert"></span>
                    </div>
                </div>
                <p class="col-12">{{trans('resume.to')}}</p>
                <div class="col-lg-6">
                    <div class="form-group">
                        {!! Form::selectYear('training_year_to', 1900, 2019, null,['required'=>'required','id'=>'training_year_to','class'=>'training']) !!}
                        <label class="control-label">{{trans('resume.year')}}<span
                                    class="star">*</span></label>
                        <span class="v-error" id="training_year_to_error" role="alert"></span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <select class="training" name="training_month_to" id="training_month_to" required>
                            @for($i=0;$i<count(config('enums.months'));$i++)
                                <option value="{{$i+1}}">{{trans('months.'.config('enums.months')[$i])}}</option>
                            @endfor
                        </select>
                        <label class="control-label">{{trans('resume.month')}}<span
                                    class="star">*</span></label>
                        <span class="v-error" id="training_month_to_error" role="alert"></span>
                    </div>
                </div>
            </div>

        </div>

        <input type="hidden" name="cv_id" value="{{$resume->id}}">
        <input type="hidden" id="count_training"  value="{{count($resume->training)}}">
    </div>
    {!! Form::close() !!}

</section>