<section>

    {!! Form::open(['class' => 'form-horizontal','id'=>'cv_reference']) !!}
    <div class="row">
        <div class="col-12 refs">
            @if(count($resume->reference)!=0)
                @foreach($resume->reference as $item)
                    <div class="edit-field d-flex " data-ref="{{$item->id}}">
                        <p data-id="{{$item->id}}" class="mb-0 mr-auto">{{$item->full_name}}</p>
                        <span data-id="{{$item->id}}" class="ref_edit"><img src="/img/edu-edit.svg" alt=""></span>
                        <span  data-id="{{$item->id}}" class="ref_remove"><img
                                    src="/img/edu-delete.svg" alt=""></span>
                    </div>
                @endforeach
            @endif
            <input type="hidden" value="" id="ref_hidden" name="ref">
        </div>
        <div class="col-12">
            <h3 id="for_edit_ref"></h3>
        </div>
        <div class="col-lg-6">


            <div class="form-group">
                <select class="ref" name="title" id="ref_title" required>
                    <option></option>
                    @foreach(config('enums.title') as $item)
                        <option   value="{{$item}}">{{trans('title.'.$item)}}
                        </option>
                    @endforeach
                </select>
                <label for="ref_title" class="control-label">{{trans('register_step_2.title')}} <span
                            class="star">*</span></label>
                <span class="v-error" id="ref_title_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input  id="full_name" class="form-control ref"
                       required name="full_name"
                       type="text">
                <label class="control-label">{{trans('resume.full_name')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="full_name_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input  id="ref_position" class="form-control ref"
                       required name="position"
                       type="text">
                <label class="control-label">{{trans('cv.position/organization')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="ref_position_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select class="ref"  id="ref_relationship" name="relationship">
                    <option></option>
                    @foreach(config('enums.reference_relationship') as $item)
                        <option  value="{{$item}}">{{trans('cv_reference.'.$item)}}</option>
                    @endforeach
                </select>
                <label for="ref_relationship" class="control-label">{{trans('resume.relationship')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="ref_relationship_error" role="alert"></span>
            </div>


            <div class="buttons d-flex justify-content-between flex-wrap">
                <button type="button" id="ref_add" class="btn btn-orange-gradient">{{trans('resume.add_more')}}</button>

            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                {!! Form::selectYear('duration', 0 ,31, 0,['required'=>'required','id'=>'duration','class'=>'ref']) !!}
                <label class="control-label">{{trans('resume.duration')}}<span
                            class="star">*</span></label>
                <span class="v-error" id=duration_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select class="ref" id="duration_type" name="duration_type">
                    <option></option>
                    @foreach(config('enums.duration_type') as $item)
                        <option  value="{{$item}}">{{trans('cv_reference.'.$item)}}</option>
                    @endforeach
                </select>
                <label for="duration_type" class="control-label">{{trans('resume.duration_type')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="duration_type_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input  id="current_position" class="form-control ref"
                       required name="current_position"
                       type="text">
                <label class="control-label">{{trans('resume.current_position')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="current_position_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input   id="ref_email" class="form-control ref"
                       required name="email"
                       type="email">
                <label class="control-label">{{trans('resume.email')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="ref_email_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input  id="ref_phone" class="form-control phone ref"
                       required name="phone"
                       type="text">
                <label for="ref_phone" class="control-label">{{trans('resume.phone')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="ref_phone_error" role="alert"></span>
            </div>
        </div>
        <input type="hidden" name="cv_id" value="{{$resume->id}}">
        <input type="hidden" id="count_ref"  value="{{count($resume->reference)}}">
    </div>
    {!! Form::close() !!}
</section>