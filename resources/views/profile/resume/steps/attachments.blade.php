<section>
    {!! Form::open(['class' => 'form-horizontal','id'=>'cv_attachments']) !!}
    <div class="form-group-file mb-input">
        <label for="file" class="sr-only">{{trans('announcements.choose_file')}}</label>
        <input id="attachment" type="file"
               class="filepond"
               name="filepond[]"
               multiple
               data-max-file-size="3MB"
               data-max-files="10"/>
        <span class="input-help">{{trans('announcements.file_size')}}</span>
      <span id="file_error" role="alert"></span>
    </div>
    <input id="attachment_cv_id" type="hidden" name="cv_id" value="{{$resume->id}}">
    {!! Form::close() !!}
</section>