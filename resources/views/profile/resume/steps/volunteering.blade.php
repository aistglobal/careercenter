<section>
    {!! Form::open(['class' => 'form-horizontal','id'=>'cv_volunteering']) !!}
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <select name="willing" id="willing_to_volunteer">
                    <option @if(isset($resume->volunteering) && $resume->volunteering->willing=='yes') selected
                            @endif value="yes">{{trans('resume.yes')}}
                    </option>
                    <option @if(isset($resume->volunteering) && $resume->volunteering->willing=='no') selected
                            @endif value="no">{{trans('resume.no')}}
                    </option>
                </select>
                <label class="control-label"
                       for="willing_to_volunteer">{{trans('resume.willing_to_volunteer')}}</label>
            </div>
            <div id="volunteering_details"
                 style="display: @if(isset($resume->volunteering) && $resume->volunteering->willing=='no') none @else block @endif">
                <div class="form-group volunteering">
                    <select name="public_works" id="public_works">
                        <option value=""></option>
                        <option @if(isset($resume->volunteering) && ($resume->volunteering->public_work)=='yes') selected
                                @endif value="yes">{{trans('resume.yes')}}
                        </option>
                        <option @if(isset($resume->volunteering) && ($resume->volunteering->public_work)=='no') selected
                                @endif value="no">{{trans('resume.no')}}
                        </option>
                    </select>
                    <label class="control-label" for="public_works">{{trans('resume.public_woks')}}</label>
                </div>
                <div class="form-group volunteering">
                    <select multiple name="work_type[]" id="work_type">
                        <option value=""></option>
                        @foreach(config('enums.work_type') as $item)
                            <option @if(isset($work_type) &&  in_array($item, $work_type)) selected
                                    @endif value="{{$item}}">{{trans('cv_volunteering.'.$item)}}</option>
                        @endforeach
                    </select>
                    <label class="control-label" for="work_type">{{trans('resume.work_type')}}</label>
                </div>
                <div class="form-group volunteering">
                    <select id="country_id" multiple name="countries[]">
                        <option value=""></option>
                        @foreach($countries as $item)
                            <option @if(isset($v_countries) && in_array($item->id, $v_countries)) selected
                                    @endif value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                        @endforeach
                    </select>
                    <label for="country_id" class="control-label">{{trans('resume.countries')}}</label>
                </div>
            </div>
            <input type="hidden" name="cv_id" id="resume_id" value="{{$resume->id}}">
        </div>
    </div>
    {!! Form::close() !!}
</section>