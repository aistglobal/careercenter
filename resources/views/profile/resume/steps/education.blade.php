<section>

    {!! Form::open(['class' => 'form-horizontal','id'=>'cv_education']) !!}
    <div class="row">
        <div class="col-12 educations">
            @if(count($resume->education)!=0)
                @foreach($resume->education as $item)
                    <div class="edit-field d-flex align-items-center single-edu" data-edu="{{$item->id}}">
                        <p data-id="{{$item->id}}" class="mb-0 mr-auto educ-name">{{$item->name}}</p>
                        <span data-id="{{$item->id}}" class="edu_edit"><img src="/img/edu-edit.svg" alt=""></span>
                        <span data-id="{{$item->id}}" class="edu_remove"><img
                                    src="/img/edu-delete.svg" alt=""></span>
                    </div>
                @endforeach
            @endif
            <input type="hidden" value="" id="edu_hidden" name="edu">
        </div>
        <div class="col-12">
            <h3 class="edu for_edit"></h3>
        </div>
        <div class="col-lg-6">

            <div class="form-group">
                <input id="edu_name" class="form-control edu"
                       required name="name"
                       type="text">
                <label class="control-label">{{trans('register_step_2.name')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="edu_name_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input id="edu_city" class="form-control edu"
                       required name="city"
                       type="text">
                <label class="control-label">{{trans('cv.city')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="edu_city_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select class="edu" id="edu_type" name="type">
                    <option value=""></option>
                    @foreach(config('enums.education_type') as $item)
                        <option value="{{$item}}">{{trans('cv_education.'.$item)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.education_type')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="edu_type_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select name="country_id" id="edu_country" class="edu">
                    <option value=""></option>
                    @foreach($countries as $item)
                        <option value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.country')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="edu_country_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input id="department"
                       class="form-control edu"
                       required name="department"
                       type="text">
                <label class="control-label">{{trans('resume.department')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="department_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select id="study_area" name="study_area" class="edu">
                    <option value=""></option>
                    @foreach(config('enums.category') as $item)
                        <option value="{{$item}}">{{trans('category.'.$item)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.study_area')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="study_area_error" role="alert"></span>
            </div>

            <div class="form-group">
                <input id="major_field"
                       class="form-control edu"
                       name="major_field"
                       type="text">
                <label class="control-label">{{trans('resume.major_field')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="major_field_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select class="edu" id="degree" name="degree">
                    <option value=""></option>
                    @foreach(config('enums.degree') as $item)
                        <option value="{{$item}}">{{trans('cv_education.'.$item)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.degree')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="degree_error" role="alert"></span>
            </div>

        </div>
        <div class="col-lg-6">
            <p>{{trans('resume.from')}}</p>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        {!! Form::selectYear('edu_year_from', 1900, 2019, null,['required'=>'required','id'=>'edu_year_from','class'=>'exp']) !!}
                        <label class="control-label">{{trans('resume.year')}}<span
                                    class="star">*</span></label>
                        <span class="v-error" id="edu_year_from_error" role="alert"></span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <select name="edu_month_from" id="edu_month_from" required>
                          @for($i=0;$i<count(config('enums.months'));$i++)
                                <option value="{{$i+1}}">{{trans('months.'.config('enums.months')[$i])}}</option>
                            @endfor

                        </select>
                        <label class="control-label">{{trans('resume.month')}}<span
                                    class="star">*</span></label>
                        <span class="v-error" id="edu_month_from_error" role="alert"></span>
                    </div>
                </div>
                <p class="col-12">{{trans('resume.to')}}</p>
                <div class="col-lg-6">
                    <div class="form-group">
                        {!! Form::selectYear('edu_year_to', 1900, 2019, null,['required'=>'required','id'=>'edu_year_to']) !!}
                        <label class="control-label">{{trans('resume.year')}}<span
                                    class="star">*</span></label>
                        <span class="v-error" id="edu_year_to_error" role="alert"></span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">

                        <select name="edu_month_to" id="edu_month_to" required>
                            @for($i=0;$i<count(config('enums.months'));$i++)
                                <option value="{{$i+1}}">{{trans('months.'.config('enums.months')[$i])}}</option>
                            @endfor
                        </select>

                        <label class="control-label">{{trans('resume.month')}}<span
                                    class="star">*</span></label>
                        <span class="v-error" id="edu_month_to_error" role="alert"></span>
                    </div>
                </div>
            </div>
            <div class="d-flex align-items-center mt-4">

                <button type="button" id="edu_add" class="btn btn-orange-gradient">{{trans('resume.add')}}</button>
            </div>


        </div>

        <input type="hidden" name="cv_id" value="{{$resume->id}}">
        <input type="hidden" id="count_edu" value="{{count($resume->education)}}">
    </div>
    {!! Form::close() !!}

</section>