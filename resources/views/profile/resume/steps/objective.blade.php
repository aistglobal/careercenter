<section>
    {!! Form::open(['class' => 'form-horizontal','id'=>'cv_objective']) !!}
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group height-textarea">
                {{ Form::textarea("objective",$resume->objective,array('rows'=>3,'id'=>'objective'))}}
                <label for="objective" class="control-label">{{trans('resume.objective')}}
                </label>
                <input type="hidden" name="cv_id" value="{{$resume->id}}">
                {!! $errors->first("objective", '<p style="color:red" class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>