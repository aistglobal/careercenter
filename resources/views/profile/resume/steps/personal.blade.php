<section>
    {!! Form::open(['class' => 'form-horizontal','id'=>'cv_personal','enctype'=>'multipart/form-data']) !!}
    <div class="row">
        <div class="col-lg-6">

            <div class="form-group">
                <input max="9999-12-31" type="date" id="birthday" name="birthday" class="form-control"
                       value="{{$resume->personal->birthday }}" required>
                <label for="birthday" class="control-label">{{trans('register_step_2.birthday')}} <span
                            class="star">*</span></label>
                <span id="personal_birthday_error" role="alert"></span>
            </div>

            <div class="form-group">
                <select id="marital_status" name="marital_status">
                    <option value=""></option>
                    @foreach(config('enums.marital_status') as $item)
                        <option @if($resume->personal->marital_status == $item) selected
                                @endif value="{{$item}}">{{trans('cv_personal.'.$item)}}</option>
                    @endforeach
                </select>
                <label for="marital_status" class="control-label">{{trans('resume.marital_status')}}</label>
            </div>

            <div class="form-group">
                <p class="mb-1">{{trans('resume.sex')}}<span
                            class="star">*</span></p>
                <div class="form-radio d-flex">
                    <div class="radio mr-3">
                        <label><input value="male" type="radio" name="sex"
                                      checked="checked"/><i
                                    class="helper"></i>{{trans('resume.male')}}</label>
                    </div>
                    <div class="radio">
                        <label><input value="female" @if($resume->personal->sex == 'female') checked="checked"
                                      @endif type="radio" name="sex"/><i
                                    class="helper"></i>{{trans('resume.female')}}</label>
                    </div>
                    <span class="error" id="personal_sex_error" role="alert"></span>
                </div>
            </div>

            <div class="form-group">
                <select id="disabled" name="disabled">
                    <option @if($resume->personal->disabled == 'yes') selected
                            @endif value="yes">{{trans('resume.yes')}}</option>
                    <option @if($resume->personal->disabled == 'no') selected
                            @endif value="no">{{trans('resume.no')}}</option>
                </select>
                <label for="disabled" class="control-label">{{trans('resume.disabled')}}</label>
                <span class="error" id="personal_disabled_error" role="alert"></span>
            </div>

            <div id="disabled_details" class="form-group"
                 style="display: @if($resume->personal->disabled=='yes') block @else none @endif">
                <input type="text" id="disabled_details-label" name="disabled_description"
                       value="{{$resume->personal->disabled_description}}" data-required="no">
                <label for="disabled_details-label"
                       class="control-label">{{trans('resume.disabled_details')}}</label>
                <span class="error" id="disabled_description_error" role="alert"></span>
            </div>

            <div class="form-group mb-0">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="height" data-required="no" id="resume_height" value="{{$resume->personal->height}}">
                            <label for="resume_height" class="control-label">{{trans('resume.height')}}</label>
                            <span class="error" id="personal_height_error" role="alert"></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <select name="height_measurement" data-required="no" id="height_measurement">
                                <option></option>
                                @foreach(config('enums.height_measurement') as $item)
                                    <option @if($resume->personal->height_measurement == $item) selected
                                            @endif value="{{$item}}">{{trans('cv_personal.'.$item)}}</option>
                                @endforeach
                            </select>
                            <label for="height_measurement"
                                   class="control-label">{{trans('resume.height_measurement')}}</label>
                            <span class="error" id="personal_height_measurement_error" role="alert"></span>
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-group mb-0">

                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" name="weight" id="resume_weight" data-required="no" value="{{$resume->personal->weight}}">
                        <label for="resume_weight" class="control-label">{{trans('resume.weight')}}</label>
                        <span class="error" id="personal_weight_error" role="alert"></span>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                        <select name="weight_measurement" id="weight_measurement">
                            <option></option>
                            @foreach(config('enums.weight_measurement') as $item)
                                <option @if($resume->personal->weight_measurement == $item) selected
                                        @endif value="{{$item}}">{{trans('cv_personal.'.$item)}}</option>
                            @endforeach
                        </select>
                        <label for="weight_measurement"
                               class="control-label">{{trans('resume.weight_measurement')}}</label>
                        <span class="error" id="personal_weight_measurement_error" role="alert"></span>
                    </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <input accept="image/png, image/jpeg" type="file" name="image" id="cv_file">
                <label for="cv_file" class="control-label">{{trans('register_step_2.image')}}</label>
                <div class="mt-2 position-relative input-file-img">
                    <img id="cv_image" src="{{Storage::disk('')->url($resume->personal->image)}}" alt="" class="img-fluid">
                    <button style="display: @if(isset($resume->personal->image)) block @else none @endif"
                            type="button" class="btn btn-danger" id="delete">X
                    </button>
                    <span role="alert" class="error" id="cv_image_error"></span>
                </div>


            </div>
        </div>


        <div class="col-lg-6">

            <div class="form-group">
                <select id="citizenship" multiple name="citizenship_id[]" required>
                    <option></option>
                    @foreach($countries as $item)
                        <option @if(in_array($item->id, $citizenship)) selected
                                @endif value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                    @endforeach
                </select>
                <label for="citizenship" class="control-label">{{trans('register_step_2.citizenship')}} <span
                            class="star">*</span></label>
                <span class="error" id="personal_citizenship_error" role="alert"></span>
            </div>

            <div class="form-group">
                <select name="nationality_id" id="nationality">
                    <option></option>
                    @foreach($countries as $item)
                        <option @if($resume->personal->nationality_id == $item->id) selected
                                @endif value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                    @endforeach
                </select>
                <label for="nationality" class="control-label">{{trans('resume.nationality')}}</label>
                <span class="error" id="personal_nationality_id_error" role="alert"></span>
            </div>

            <div class="form-group">
                <select name="official_service" id="official_service">
                    <option value=""></option>
                    @foreach(config('enums.official_service') as $item)
                        <option @if($resume->personal->official_service == $item) selected
                                @endif value="{{$item}}">{{trans('cv_personal.'.$item)}}</option>
                    @endforeach
                </select>
                <label for="official_service"
                       class="control-label">{{trans('resume.official_service')}} </label>
                <span class="error" id="personal_official_service_error" role="alert"></span>
            </div>

            <div class="form-group">
                <select id="dependants" name="dependants">
                    <option @if($resume->personal->dependants == 'no') selected
                            @endif value="no">{{trans('resume.no')}}
                    </option>
                    <option @if($resume->personal->dependants == 'yes') selected
                            @endif value="yes">{{trans('resume.yes')}}
                    </option>

                </select><label for="dependants" class="control-label">{{trans('resume.dependants')}}</label>
                <span class="error" id="personal_dependants_error" role="alert"></span>
            </div>

            <div id="dependants_details"
                 style="display:  @if(count($resume->dependants) !=0) block @else none @endif;">

                @if(isset($resume->dependants) && count($resume->dependants) !=0 )
                    @for ($i=0;$i<count($resume->dependants);$i++)
                        <div id="row_{{$i+1}}">
                            <div class="form-group">
                                <input required class="dependants" type="text" name="name[]"
                                       value="{{$resume->dependants[$i]['name']}}">
                                <label class="control-label">{{trans('resume.name')}}</label>
                            </div>
                            <div class="form-group">
                                <input max="9999-12-31" required class="dependants" name="birth_day[]" type="date"
                                       value="{{$resume->dependants[$i]['birthday']}}">
                                <label class="control-label">{{trans('resume.birthday')}}</label>
                            </div>
                            <div class="form-group">
                                <select required class="dependants" name="relationship[]">
                                    <option value=""></option>
                                    @foreach(config('enums.relationship') as $rel)
                                        <option @if($resume->dependants[$i]['relationship'] == $rel) selected
                                                @endif value="{{$rel}}">{{trans('cv_personal.'.$rel)}}</option>
                                    @endforeach
                                </select>
                                <label class="control-label">{{trans('resume.relationship')}}</label>
                            </div>
                            @if($i!=0)
                                <button data-id="{{$i+1}}" id="remove_{{$i+1}}"
                                        class="btn btn-danger btn-danger-2 delete_dependant mb-3"
                                        type="button">{{trans('resume.delete')}}</button>
                            @endif
                        </div>

                    @endfor
                @else
                    <div class="form-group">
                        <div class="form-group">
                            <input required id="name_1" class="dependants" type="text" name="name[]">
                            <label for="name_1" class="control-label">{{trans('resume.name')}}</label>
                        </div>
                        <div class="form-group">
                            <input max="9999-12-31" required id="birthday_1" class="dependants" name="birth_day[]" type="date">
                            <label for="birthday_1" class="control-label">{{trans('resume.birthday')}}</label>
                        </div>
                        <div class="form-group">

                            <select required id="relationship_1" class="dependants" name="relationship[]" id="">
                                <option value=""></option>
                                @foreach(config('enums.relationship') as $item)
                                    <option value="{{$item}}">{{trans('cv_personal.'.$item)}}</option>
                                @endforeach
                            </select>
                            <label for="relationship_1"
                                   class="control-label">{{trans('resume.relationship')}}</label>
                        </div>
                    </div>
                @endif
                <div id="dynamic_field">


                </div>
                <button type="button" id="add_dependant"
                        class="add-input font-bold">{{trans('resume.add')}}</button>
                <input name="key_count" id="d_count" type="hidden"
                       value="{{(isset($resume->dependants) && count($resume->dependants)!=0) ? count($resume->dependants) : 1}}">
            </div>
        </div>
        <input id="cv_id" type="hidden" name="cv_id" value="{{$resume->id}}">
    </div>
    {!! Form::close() !!}
</section>