<section>
    {!! Form::open(['class' => 'form-horizontal','id'=>'cv_contacts']) !!}
    <div class="row">
        <div class="col-lg-6">

            <div class="form-group">
                <input type="text" id="first_name" name="first_name"
                       value="{{ $resume->contacts ? $resume->contacts->first_name : $user->first_name}}"
                       required>
                <label for="first_name" class="control-label">{{trans('register_step_2.first_name')}}<span
                            class="star">*</span></label>
                <span id="first_name_error" role="alert"></span>
            </div>

            <div class="form-group">
                <input type="text" id="middle_name" name="middle_name"
                       value="{{ $resume->contacts ? $resume->contacts->middle_name : $user->middle_name}}"
                       data-required="no">
                <label for="middle_name" class="control-label">{{trans('register_step_2.middle_name')}} </label>
                <span id="middle_name_error" role="alert"></span>
            </div>

            <div class="form-group">
                <input type="text" id="last_name" name="last_name"
                       value="{{ $resume->contacts ? $resume->contacts->last_name : $user->last_name}}"
                       required>
                <label for="last_name" class="control-label">{{trans('register_step_2.last_name')}}<span
                            class="star">*</span></label>
                <span id="last_name_error" role="alert"></span>
            </div>

            <div class="form-group">
                <select name="title" id="title" required>
                    <option></option>
                    @foreach(config('enums.title') as $item)
                        <option @if( isset($resume->contacts->title) && $resume->contacts->title ==$item) selected
                                @endif value="{{$item}}">{{trans('title.'.$item)}}
                        </option>
                    @endforeach
                </select>
                <label for="title" class="control-label">{{trans('register_step_2.title')}} <span
                            class="star">*</span></label>
                <span id="title_error" role="alert"></span>
            </div>

            <div class="form-group">
                <select name="residence_country_id" required id="residence-country-id">
                    <option></option>
                    @foreach($countries as $item)
                        <option @if(isset($resume->contacts->residence_country_id) && $resume->contacts->residence_country_id ==$item->id) selected
                                @endif value="{{$item->id}}">{{trans('countries.'.$item->code)}}
                        </option>
                    @endforeach
                </select>
                <label for="residence-country-id"
                       class="control-label">{{trans('register_step_2.residence_country')}} <span
                            class="star">*</span></label>
                <span id="residence_country_id_error" role="alert"></span>
            </div>

            <div id="dynamic_field4">

                @if(isset($phone))
                    @for($i=0;$i<count($phone);$i++)
                        <div id="row4_{{$i+1}}" class="dynamic-added @if($i != 0) deleting4 @endif">

                            <div class="form-group">
                                <input name="phone[]" class="@if($i != 0)deleting-input4_1 @endif "
                                       value="{{$phone[$i]['phone']}}" id="phone_{{$i}}" data-required="no">
                                <label for="phone_{{$i}}"
                                       class="control-label">{{trans('register_step_2.phone')}} {{$i+1}}<span
                                            class="star">*</span></label>
                                @if($i!=0)
                                    <button type="button" name="remove" id="{{$i+1}}"
                                            class="btn btn-danger deleting-button4 btn_remove 4">X
                                    </button>
                                @endif
                                <span id="phone_{{$i+1}}_error" role="alert"></span>
                            </div>

                            <div class="form-group">
                                <select name="phone_type[]" id="phone_type_{{$i}}" required>
                                    <option value=""></option>
                                    @foreach(config('enums.phone_type') as $item)
                                        <option @if($phone[$i]['type']==$item) selected
                                                @endif value="{{$item}}">{{trans('register_step_2.'.$item)}}</option>
                                    @endforeach
                                </select>
                                <label for="phone_type_{{$i}}"
                                       class="control-label">{{trans('register_step_2.phone_type')}}</label>

                            </div>

                        </div>
                    @endfor
                    @else
                    <div id="row4_1" class="dynamic-added">

                        <div class="form-group">
                            <input name="phone[]" class=""
                                   value="" id="phone_0" data-required="no">
                            <label for="phone_0"
                                   class="control-label">{{trans('register_step_2.phone')}} 1<span
                                        class="star">*</span></label>

                            <span id="phone_1_error" role="alert"></span>
                        </div>

                        <div class="form-group">
                            <select name="phone_type[]" id="phone_type_1" required>
                                <option value=""></option>
                                @foreach(config('enums.phone_type') as $item)
                                    <option  value="{{$item}}">{{trans('register_step_2.'.$item)}}</option>
                                @endforeach
                            </select>
                            <label for="phone_type_1"
                                   class="control-label">{{trans('register_step_2.phone_type')}}</label>

                        </div>

                    </div>
                @endif
                <input type="hidden" name="key_phone_count" id="key_phone_count"
                       value="{{old('key_phone_count') ? old('key_phone_count') : (isset($phone) ? count($phone): 1)}}">
            </div>

            <button type="button" name="add4" id="add4"
                    class="add-input font-bold">{{trans('register_step_2.add_phone')}}</button>

            <div class="form-group">
                <input id="email" type="email" name="email" value="{{ $resume->contacts->email }}" required>
                <label for="email" class="control-label">{{trans('register_step_2.email')}} <span
                            class="star">*</span></label>
                <span id="email_error" role="alert"></span>
            </div>

            <div class="form-group">
                <input type="url" name="website" id="web" value="{{ $resume->contacts->website }}"
                       data-required="no">
                <label for="web" class="control-label">{{trans('register_step_2.web')}}</label>
                <span id="website_error" role="alert"></span>
            </div>

        </div>


        <div class="col-lg-6">
            <div id="dynamic_field3">
                @if(count($resume->address) == 0)

                    <div class="mb-3  position-relative" id="row3_1">
                        <div class="form-group">
                            <input required  name="address[]" value="{{old('address')[0]}}" id="address1" type="text" @if(old('key_address_count')>1) required @endif>
                            <label for="address" class="control-label">{{trans('register_step_2.address')}} 1<span
                                        class="star">*</span></label>
                            <span id="address_1_error" role="alert"></span>
                        </div>
                        <div class="form-group">
                            <select name="address_type[]" id="address_type_1">
                                <option value=""></option>
                                <option @if(old('address_type')[0]=='home') selected @endif value="home">{{trans('register_step_2.home')}}</option>
                                <option @if(old('address_type')[0]=='business') selected @endif  value="business">{{trans('register_step_2.business')}}</option>
                            </select>
                            <label for="address_type_0" class="control-label">{{trans('register_step_2.address_type')}}</label>
                        </div>

                        <div class="form-group">
                            <select name="country[]" id="address-country1" required>
                                <option></option>
                                @foreach($countries as $item)
                                    <option @if(old('country')[0]!=null && old('country')[0] ==$item->id)selected
                                            @endif value="{{$item->id}}">
                                        {{trans('countries.'.$item->code)}}</option>
                                @endforeach
                            </select>
                            <label for="address-country" class="control-label">{{trans('register_step_2.country')}} 1<span
                                        class="star">*</span>
                            </label>
                            <span id="country_1_error" role="alert"></span>
                        </div>


                        <div class="form-group">
                            <input required  class="@error('city.0') is-invalid @enderror" type="text" name="city[]" id="City1"
                                    value="{{old('city')[0]}}">
                            <label for="City " class="control-label">{{trans('register_step_2.city')}} 1<span
                                        class="star">*</span> </label>
                            <span id="city_1_error" role="alert"></span>
                        </div>

                        <div class="form-group">
                            <input required type="text" class="@error('zip.0') is-invalid @enderror" name="zip[]" id="zip1"
                                   value="{{old('zip')[0]}}">
                            <label for="zip" class="control-label">{{trans('register_step_2.zip')}} 1<span
                                        class="star">*</span></label>
                            <span id="zip_1_error" role="alert"></span>
                        </div>
                    </div>

                @else
                    @for($i=0;$i<count($resume->address);$i++)
                        <div class="mb-3 @if($i != 0) deleting3 @endif position-relative" id="row3_{{$i+1}}">

                            <div class="form-group">
                                <input class="form-control @if($i != 0) deleting-input3_1 @endif"
                                       id="address{{$i+1}}" required name="address[]"
                                       value="{{ $resume->address[$i]->address}}" type="text">
                                <label for="address{{$i+1}}"
                                       class="control-label">{{trans('register_step_2.address')}} {{$i+1}}<span
                                            class="star">*</span></label>
                                <span id="address_{{$i+1}}_error" role="alert"></span>
                            </div>

                            <div class="form-group">
                                <select name="address_type[]" id="address_type_{{$i+1}}">
                                    <option value=""></option>
                                    <option @if( $resume->address[$i]->address_type == 'home') selected
                                            @endif value="home">{{trans('register_step_2.home')}}
                                    </option>
                                    <option @if( $resume->address[$i]->address_type == 'business') selected
                                            @endif value="business">{{trans('register_step_2.business')}}
                                    </option>
                                </select>
                                <label for="address_type_{{$i+1}}"
                                       class="control-label">{{trans('register_step_2.address_type')}} {{$i+1}}</label>
                            </div>

                            <div class="form-group">
                                <select name="country[]" id="address-country{{$i}}" required>
                                    <option></option>
                                    @foreach($countries as $item)
                                        <option @if($resume->address[$i]->country_id ==$item->id) selected
                                                @endif value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                                    @endforeach
                                </select>
                                <label for="address-country{{$i}}"
                                       class="control-label">{{trans('register_step_2.country')}} {{$i+1}}<span
                                            class="star">*</span></label>
                                <span id="country_{{$i+1}}_error" role="alert"></span>
                            </div>

                            <div class="form-group">
                                <input type="text" name="city[]" class="@if($i!= 0) deleting-input3_{{$i}} @endif"
                                       id="City_{{$i+1}}" value="{{$resume->address[$i]->city}}" required>
                                <label for="City_{{$i+1}}"
                                       class="control-label">{{trans('register_step_2.city')}}{{$i+1}}<span
                                            class="star">*</span></label>
                                <span id="city_{{$i+1}}_error" role="alert"></span>
                            </div>

                            <div class="form-group">
                                <input id="zip{{$i+1}}" type="text"
                                       class="@if($i!= 0) deleting-input3_{{$i}} @endif" name="zip[]"
                                       value="{{$resume->address[$i]->zip}}" required>
                                <label for="zip{{$i+1}}"
                                       class="control-label">{{trans('register_step_2.zip')}}{{$i+1}}<span
                                            class="star">*</span></label>
                                <span id="zip_{{$i+1}}_error" role="alert"></span>
                            </div>

                            @if($i!=0)
                                <button type="button" name="remove" id="{{$i+1}}"
                                        class="btn btn-danger btn-danger-2 deleting-button3 btn_remove 3">{{trans('register_step_2.delete')}} {{$i+1}}
                                </button>
                            @endif
                        </div>
                    @endfor
                @endif
                <input type="hidden" name="key_address_count" id="key_address_count"
                       value="{{count($resume->address) ? count($resume->address)  : 1}}">
                <button type="button" name="add3" id="add3"
                        class="add-input font-bold">{{trans('register_step_2.add_address')}}</button>
            </div>
        </div>
    </div>
    <input id="view_cv_id" name="cv_id" type="hidden" value="{{$resume->id}}">

    {!! Form::close() !!}
</section>