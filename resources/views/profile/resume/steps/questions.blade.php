<section>
    {!! Form::open(['class' => 'form-horizontal','id'=>'cv_questions']) !!}
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <p class="mb-1">{{trans('cv_questions.make_inquiries')}} {{trans('cv_questions.present_employer')}}</p>
                <div class="form-radio d-flex">
                    <div class="radio mr-3">
                        <label><input value="yes" type="radio" name="present_employer"
                                     @if(isset($resume->questions) && $resume->questions->present_employer=='yes' ) checked="checked" @endif/><i
                                    class="helper"></i>{{trans('resume.yes')}}</label>
                    </div>
                    <div class="radio">
                        <label><input value="no" @if(isset($resume->questions) && $resume->questions->present_employer=='no' ) checked="checked" @endif type="radio" name="present_employer"/><i
                                    class="helper"></i>{{trans('resume.no')}}</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <p class="mb-1">{{trans('cv_questions.make_inquiries')}} {{trans('cv_questions.previous_employer')}}</p>
                <div class="form-radio d-flex">
                    <div class="radio mr-3">
                        <label><input value="yes" type="radio" name="previous_employer"
                                      @if(isset($resume->questions) && $resume->questions->previous_employer=='yes' ) checked="checked" @endif/><i
                                    class="helper"></i>{{trans('resume.yes')}}</label>
                    </div>
                    <div class="radio">
                        <label><input value="no" @if(isset($resume->questions) && $resume->questions->previous_employer=='no' ) checked="checked" @endif type="radio" name="previous_employer"/><i
                                    class="helper"></i>{{trans('resume.no')}}</label>
                    </div>
                </div>
            </div>
            <div  class="form-group">
                <input id="government" type="text"  name="government"
                     @if(isset($resume->questions))  value="{{$resume->questions->government}}" @endif data-required="no">
                <label for="government"
                       class="control-label">{{trans('resume.government')}}</label>
            </div>
            <div class="form-group">
                <select id="arrested" name="arrested">
                    <option @if( isset($resume->questions) && $resume->questions->arrested == 'no') selected
                            @endif value="no">{{trans('resume.no')}}</option>
                    <option @if (isset($resume->questions) && $resume->questions->arrested == 'yes') selected
                            @endif value="yes">{{trans('resume.yes')}}</option>

                </select>
                <label for="arrested" class="control-label">{{trans('resume.arrested')}}</label>
            </div>

            <div id="arrested_details" class="form-group"
                 style="display: @if(isset($resume->questions) && $resume->questions->arrested =='yes') block @else none @endif">
                {{ Form::textarea("arrested_details",(isset($resume->questions) && $resume->questions->arrested =='yes')? $resume->questions->arrested_details : null ,array('rows'=>3,'id'=>'arrested_details'))}}
                <label for="disabled_details-label"
                       class="control-label">{{trans('resume.arrested_details')}}</label>
            </div>
            <input type="hidden" name="cv_id" value="{{$resume->id}}">
        </div>
    </div>
    {!! Form::close() !!}
</section>
