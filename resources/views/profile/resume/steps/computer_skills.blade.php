<section>
    {!! Form::open(['class' => 'form-horizontal','id'=>'cv_computer_skills']) !!}
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <select id="level" name="level">
                    <option value=""></option>
                    @foreach(config('enums.computer_level') as $item)
                        <option @if(isset($resume->computer_skills) &&  $resume->computer_skills->level == $item) selected
                                @endif value="{{$item}}">{{trans('cv_computer_skills.'.$item)}}
                        </option>
                    @endforeach
                </select>
                <label for="level"
                       class="control-label">{{trans('resume.level')}}</label>
            </div>
            <div class="form-group">
                {{ Form::textarea("software",isset($resume->computer_skills)? $resume->computer_skills->software : null,array('rows'=>3,'id'=>'software'))}}
                <label for="software" class="control-label">{{trans('resume.software')}}
                </label>
                <input type="hidden" name="cv_id" value="{{$resume->id}}">
                {!! $errors->first("software", '<p style="color:red" class="help-block">:message</p>') !!}
            </div>
            <div class="form-group">
                {{ Form::textarea("hardware",isset($resume->computer_skills)? $resume->computer_skills->hardware : null,array('rows'=>3,'id'=>'hardware'))}}
                <label for="hardware" class="control-label">{{trans('resume.hardware')}}
                </label>
                <input type="hidden" name="cv_id" value="{{$resume->id}}">
                {!! $errors->first("hardware", '<p style="color:red" class="help-block">:message</p>') !!}
            </div>
            <input type="hidden" name="cv_id" value="{{$resume->id}}">
        </div>
    </div>
    {!! Form::close() !!}
</section>