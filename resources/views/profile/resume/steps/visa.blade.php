<section>

    {!! Form::open(['class' => 'form-horizontal','id'=>'cv_visa']) !!}
    <div class="row">
        <div class="col-12 visas">
            @if(count($resume->visa)!=0)
                @foreach($resume->visa as $item)
                    <div class="edit-field d-flex " data-visa="{{$item->id}}">
                        <p data-id="{{$item->id}}" class="mb-0 mr-auto">{{trans('countries.'.$item->country->code)}}</p>
                        <span data-id="{{$item->id}}" class="visa_edit"><img src="/img/edu-edit.svg" alt=""></span>
                        <span  data-id="{{$item->id}}" class="visa_remove"><img
                                    src="/img/edu-delete.svg" alt=""></span>
                    </div>
                @endforeach
            @endif
            <input type="hidden" value="" id="visa_hidden" name="visa">
        </div>
        <div class="col-12">
            <h3 id="for_edit_visa"></h3>
        </div>
        <div class="col-lg-6">

            <div class="form-group">
                <select class="visa" id="visa_country_id" name="country_id">
                    @foreach($countries as $item)
                        <option  value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.countries')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="visa_country_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input  id="visa_type" class="form-control visa"
                       name="type"
                       type="text">
                <label class="control-label">{{trans('resume.visa_type')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="visa_type_error" role="alert"></span>
            </div>

            <div class="form-group">
                <input max="9999-12-31"  id="visa_period_from" class="form-control visa"
                       name="period_from"
                       type="date">
                <label class="control-label">{{trans('resume.period_from')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="period_from_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input max="9999-12-31"  id="visa_period_to" class="form-control visa"
                       name="period_to"
                       type="date">
                <label class="control-label">{{trans('resume.period_to')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="period_to_error" role="alert"></span>
            </div>

            <div class="buttons d-flex justify-content-between flex-wrap">
                <button type="button" id="visa_add"
                        class="btn btn-orange-gradient">{{trans('resume.add_more')}}</button>
            </div>
        </div>

        <input type="hidden" id="count_visa"  value="{{count($resume->visa)}}">
        <input type="hidden" name="cv_id" value="{{$resume->id}}">
    </div>
    {!! Form::close() !!}

</section>