<section style="display: none">
    {!! Form::open(['class' => 'form-horizontal','id'=>'cv_skills']) !!}
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group height-textarea">
                {{ Form::textarea("special_skills",$resume->special_skills,array('rows'=>1,'id'=>'special_skills'))}}
                <label for="special_skills" class="control-label">{{trans('resume.special_skills')}}</label>
                <input type="hidden" name="cv_id" value="{{$resume->id}}">
                {!! $errors->first("special_skills", '<p style="color:red" class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>