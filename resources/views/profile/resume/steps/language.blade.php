<section>
    <input id="current_locale" type="hidden" value="{{LaravelLocalization::getCurrentLocale()}}">
    {!! Form::open(['class' => 'form-horizontal','id'=>'cv_language']) !!}
    <div class="row">
        <div class="col-12 languages">
            @if(count($resume->language)!=0)
                @foreach($resume->language as $item)
                    <div class="edit-field d-flex" data-lang="{{$item->id}}">
                        <p data-id="{{$item->id}}" class="mb-0 mr-auto">{{trans('languages.'.$item->language->name)}}</p>
                        <span data-id="{{$item->id}}" class="lang_edit"><img src="/img/edu-edit.svg" alt=""></span>
                        <span  data-id="{{$item->id}}" class="lang_remove"><img
                                    src="/img/edu-delete.svg" alt=""></span>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="col-12">
            <h3 id="for_edit_lang"></h3>
        </div>
        <div class="col-lg-6 ">
            <input type="hidden" value="" id="lang_hidden" name="language">
            <div class="form-group">
                <select class="language" id="language_id" name="language_id">
                    <option value=""></option>
                    @foreach($languages as $item)
                        <option  value="{{$item->id}}">{{trans('languages.'.$item->name)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.languages')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="language_id_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select class="language" id="reading" name="reading">
                    <option value=""></option>
                    @foreach(config('enums.reading') as $item)
                        <option  value="{{$item}}">{{trans('cv_language_skills.'.$item)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.reading')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="reading_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select class="language" id="writing" name="writing">
                    <option value=""></option>
                    @foreach(config('enums.reading') as $item)
                        <option  value="{{$item}}">{{trans('cv_language_skills.'.$item)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.writing')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="writing_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select class="language" id="communication" name="communication">
                    <option value=""></option>
                    @foreach(config('enums.reading') as $item)
                        <option  value="{{$item}}">{{trans('cv_language_skills.'.$item)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.communication')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="communication_error" role="alert"></span>
            </div>

            <div class="form-group">
                <p class="mb-1">{{trans('resume.language_type')}}</p>
                <div class="form-radio d-flex">
                    <div class="radio mr-3">
                        <label><input  id="radio_native" checked value="native" type="radio"
                                      name="language_type"/><i
                                    class="helper"></i>{{trans('language.native')}}</label>
                    </div>
                    <div class="radio">
                        <label><input   id="radio_foreign" value="foreign" type="radio" name="language_type"/><i
                                    class="helper"></i>{{trans('resume.foreign')}}</label>
                    </div>
                    <span class="v-error" id=language_type_error" role="alert"></span>
                </div>
            </div>
            <div class="form-group">
                <select class="language" id="total_study" name="total_study">
                    <option value=""></option>
                    @foreach(config('enums.total_study') as $item)
                        <option  value="{{$item}}">{{trans('cv_language_skills.'.$item)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.total_study')}}<span
                            class="star">*</span></label>
                <span class="v-error" id="total_study_error" role="alert"></span>
            </div>

            <div class="form-group">
                <input min="0"  id="typing" class="form-control language"
                       name="typing"
                       type="number">
                <label class="control-label">{{trans('resume.typing')}}</label>
                <span class="v-error" id="typing_error" role="alert"></span>
            </div>

            <div class="form-group">
                <input min="0"  id="shorthand" class="form-control language"
                       name="shorthand"
                       type="number">
                <label class="control-label">{{trans('resume.shorthand')}}</label>
                <span class="v-error" id="shorthand_error" role="alert"></span>
            </div>

            <div class="buttons d-flex justify-content-between flex-wrap">
                <button type="button" id="language_add"
                        class="btn btn-orange-gradient">{{trans('resume.add_more')}}</button>

            </div>
        </div>


        <input type="hidden" name="cv_id" value="{{$resume->id}}">
        <input type="hidden" id="count_lang"  value="{{count($resume->language)}}">
    </div>
    {!! Form::close() !!}

</section>