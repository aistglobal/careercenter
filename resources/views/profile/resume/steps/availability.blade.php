<section>
    {!! Form::open(['class' => 'form-horizontal','id'=>'cv_availability']) !!}

    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <input max="9999-12-31" @if(isset($resume->availability)) value="{{$resume->availability->period_from}}"
                       @endif type="date" name="period_from" id="period_from">
                <label class="control-label">{{trans('resume.period_from')}}</label>
                <span></span>
            </div>
            <div class="form-group">
                <input max="9999-12-31" type="date" name="period_to"
                       id="period_to" @if(isset($resume->availability)) value="{{$resume->availability->period_to}}" @endif>
                <label class="control-label">{{trans('resume.period_to')}}</label>
            </div>
            <div class="mb-4">
                @foreach(config('enums.days') as $item)
                    <div class="checkbox mb-0">
                        <label class="d-inline-flex">
                            <input @if(in_array($item,$days)) checked @endif id="{{$item}}" type="checkbox" name="days[]" value="{{$item}}"><i class="helper"></i>{{trans('cv_availability.'.$item)}}
                        </label>
                    </div>
                    {{--<input @if(in_array($item,$days)) checked @endif id="{{$item}}" type="checkbox" name="days[]" value="{{$item}}">--}}
                    {{--<label class="mr-1" for="{{$item}}"--}}
                    {{--class="control-label">{{trans('cv_availability.'.$item)}}</label>--}}
                @endforeach
            </div>

            <div class="form-group">
                {{ Form::time('hours_from',isset($resume->availability->hours_from)? $resume->availability->hours_from : Carbon\Carbon::now()->format('H:i')) }}
                <label class="control-label">{{trans('resume.hours_from')}}</label>
            </div>
            <div class="form-group">
                {{ Form::time('hours_to',isset($resume->availability->hours_to)? $resume->availability->hours_to : Carbon\Carbon::now()->format('H:i')) }}
                <label class="control-label">{{trans('resume.hours_to')}}</label>
            </div>
            <div class="mb-4">
                @foreach(config('enums.terms') as $item)
                    <div class="checkbox mb-0">
                        <label class="d-inline-flex">
                            <input @if(in_array($item,$term)) checked @endif id="{{$item}}" type="checkbox" name="term[]" value="{{$item}}"><i class="helper"></i>{{trans('cv_availability.'.$item)}}
                        </label>
                    </div>
                    {{--<input @if(in_array($item,$term)) checked @endif id="{{$item}}" type="checkbox" name="term[]" value="{{$item}}">--}}
                    {{--<label class="mr-1" for="{{$item}}" class="control-label">{{trans('cv_availability.'.$item)}}</label>--}}
                @endforeach
                <div class="form-group">
                    <span  id="term_error" class="error" role="alert"></span>
                </div>

            </div>

            <div class="form-group">
                <select name="travel" id="travel">
                    <option @if(isset($resume->availability) && $resume->availability->travel=='yes') selected @endif  value="yes">{{trans('resume.yes')}}</option>
                    <option  @if(isset($resume->availability) && $resume->availability->travel=='no') selected @endif  value="no">{{trans('resume.no')}}</option>
                </select>
                <label class="control-label" for="travel">{{trans('resume.travel')}} <span class="star">*</span></label>
            </div>
            <div id="travel_reason_div"  class="form-group" style="display:  @if(isset($resume->availability) && $resume->availability->travel=='no' ) @else none @endif ">
                <input id="travel_reason" type="text" name="travel_reason" @if(isset($resume->availability) && $resume->availability->travel=='no' ) value="{{$resume->availability->travel_reason}}" @endif>
                <label class="control-label" for="travel_reason">{{'resume.travel_reason'}}</label>
            </div>
            <div class="form-group">
                <select name="show_resume" id="show_resume">
                    @foreach(config('enums.show_resume') as $item)
                        <option @if(isset($resume->availability) && $resume->availability->show_resume==$item) selected @endif value="{{$item}}">{{trans('cv_availability.'.$item)}}</option>
                    @endforeach
                </select>
                <label class="control-label" for="show_resume">{{trans('resume.show_resume')}}<span class="star">*</span></label>
            </div>
            <div class="form-group">
                <input @if(isset($resume->availability)) value="{{$resume->availability->short_salary}}"
                       @endif  id="short_term_salary" type="text" name="short_salary">
                <label  class="control-label" for="short_term_salary">{{trans('resume.short_term_salary')}}</label>
            </div>
            <div class="form-group">
                <select id="short_currency" name="short_currency">
                    @foreach(config('enums.currency') as $item)
                        <option @if(isset($resume->availability) && $resume->availability->short_currency==$item) selected @endif  value="{{$item}}">{{trans('currency.'.$item)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.currency')}}</label>
                <span class="error" id="short_currency_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select  id="short_period" name="short_period">
                    @foreach(config('enums.period') as $item)
                        <option @if(isset($resume->availability) && $resume->availability->short_period==$item) selected @endif   value="{{$item}}">{{trans('period.'.$item)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.period')}}</label>
                <span class="error" id="short_period_error" role="alert"></span>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <input  @if(isset($resume->availability)) value="{{$resume->availability->long_salary}}"
                        @endif id="long_term_salary" type="text" name="long_salary">
                <label class="control-label" for="long_term_salary">{{trans('resume.long_term_salary')}}<span class="star">*</span></label>
                <span class="error" id="long_salary_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select id="long_currency" name="long_currency">
                    @foreach(config('enums.currency') as $item)
                        <option @if(isset($resume->availability) && $resume->availability->long_currency==$item) selected @endif  value="{{$item}}">{{trans('currency.'.$item)}}</option>
                    @endforeach
                </select>
                <label for="long_currency" class="control-label">{{trans('resume.currency')}}<span class="star">*</span></label>
                <span class="error" id="long_currency_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select id="long_period" name="long_period">
                    @foreach(config('enums.period') as $item)
                        <option  @if(isset($resume->availability) && $resume->availability->long_period==$item) selected @endif  value="{{$item}}">{{trans('period.'.$item)}}</option>
                    @endforeach
                </select>
                <label for="long_period" class="control-label">{{trans('resume.period')}}<span class="star">*</span></label>
                <span class="error" id="long_period_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input @if(isset($resume->availability)) value="{{$resume->availability->time}}"
                       @endif type="text" name="time" id="time">
                <label class="control-label" for="time">{{trans('resume.time')}}</label>
            </div>
            <div class="form-group">
                <select multiple id="interest_area"  name="interest_area[]">
                    <option value=""></option>
                    @foreach(config('enums.category') as $item)
                        <option @if(in_array($item,$interest_area)) selected @endif value="{{$item}}">{{trans('category.'.$item)}}</option>
                    @endforeach
                </select>
                <label class="control-label">{{trans('resume.interest_area')}}</label>
                <span class="error" id="interest_area_error" role="alert"></span>
            </div>
            <div class="form-group">

                <select multiple id="position_type"  name="position_type[]">
                    <option value=""></option>
                    @foreach(config('enums.types_of_positions') as $item)
                        <option @if(in_array($item,$position_type)) selected @endif value="{{$item}}">{{trans('cv_availability.'.$item)}}</option>
                    @endforeach
                </select>
                <label for="position_type" class="control-label">{{trans('resume.position_type')}}</label>
                <span class="error" id="position_type_error" role="alert"></span>
            </div>
            <div class="form-group">
                <select multiple id="org_type"  name="org_type[]">
                    <option value=""></option>
                    @foreach(config('enums.legal') as $item)
                        <option   @if(in_array($item,$org_type)) selected @endif value="{{$item}}">{{trans('legal_structure.'.$item)}}</option>
                    @endforeach
                </select>
                <label for="org_type" class="control-label">{{trans('resume.org_type')}}</label>
                <span class="error" id="org_type_error" role="alert"></span>
            </div>
            <div class="form-group">
                <input @if(isset($resume->availability)) value="{{$resume->availability->undesired_org}}" @endif id="undesired_org" type="text" name="undesired_org">
                <label  class="control-label" for="undesired_org">{{trans('resume.undesired_org')}}</label>
            </div>
        </div>
    </div>
    <input id="availabilit_cv_id" type="hidden" name="cv_id" value="{{$resume->id}}">
    {!! Form::close() !!}

</section>