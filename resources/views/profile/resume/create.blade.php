@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link rel="stylesheet" type="text/css" href="/css/select2.min.css"/>
    <link href="{{ asset('css/jquery.steps.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css"
          rel="stylesheet">
    <link href="https://unpkg.com/filepond/dist/filepond.min.css" rel="stylesheet">

@endsection

@section('content')

    @include ('profile.resume.form')

@endsection

@section('scripts')
    <script src="/js/jquery.steps.js" type="text/javascript"></script>
    <script src="/js/jquery.maskedinput.js" type="text/javascript"></script>

    <script src="/js/Select2-4.0.7.js" type="text/javascript"></script>


    <script src="https://unpkg.com/filepond-plugin-file-encode/dist/filepond-plugin-file-encode.min.js"></script>
    <script src="https://unpkg.com/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.min.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-exif-orientation/dist/filepond-plugin-image-exif-orientation.min.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.js"></script>
    <script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/7.3.2/jquery.mmenu.all.js"></script>

    <script>
        var next = false;
        var change_step = 'yes';

        function experience(is_next, newIndex) {
            $("a[href='#next']").css({"pointer-events":"none"});
            var form = $('#cv_experience')[0]; // You need to use standard javascript object here
            var formData = new FormData(form);

            $.ajax({
                type: "POST",
                data: formData,
                url: '/cv_experience',
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    $("a[href='#next']").css({"pointer-events":"auto"});
                    $('#example-basic-t-6').addClass('filled');
                    $('.till_now').prop('checked', false);
                    $('#exp_year').css('display', 'block');
                    $('#exp_month').css('display', 'block');
                    $('#for_edit_exp').text('');
                    $('.edit-field').each(function () {
                        $(this).removeAttr('style');
                    })
                    if (is_next) {
                        next = true;
                        $('.v-error').each(function () {
                            $(this).empty();
                        });
                        $("#example-basic-t-" + newIndex).click();
                    }

                    var exp = $('#exp_hidden').val();
                    $('.exp').each(function () {
                        $(this).val('');
                    })

                    $('.v-error').each(function () {
                        $(this).empty();
                    });
                    if (exp == '') {

                        var new_exp = '<div class="edit-field d-flex" data-exp="' + result['id'] + '">' +
                            '<p data-id="' + result['id'] + '" class="mb-0 mr-auto">' + result['name'] + '</p>' +
                            '<span data-id="' + result['id'] + '" class="exp_edit"><img src="/img/edu-edit.svg" alt=""></span>' +
                            '<span data-id="' + result['id'] + '" class="exp_remove">' +
                            '<img src="/img/edu-delete.svg" alt=""></span>' +
                            '</div>';
                        $('.experiences').append(new_exp);
                        var count = parseInt($("#count_exp").val())+1;
                        $("#count_exp").val(parseInt(count));

                    }
                    else {
                        var name = $("p[data-id='" + result['id'] + "']");
                        name.text(result['name']);
                    }
                    $('#exp_hidden').val('');
                },
                error: function (data) {
                    $("a[href='#next']").css({"pointer-events":"auto"});
                    var errors = data.responseJSON;
                    if (errors['errors']['name'] != undefined) {
                        $('#name_error').html(errors['errors']['name']);
                    } else {
                        $('#name_error').html('');
                    }
                    if (errors['errors']['city'] != undefined) {
                        $('#city_error').html(errors['errors']['city']);
                    } else {
                        $('#city_error').html('');
                    }
                    if (errors['errors']['country_id'] != undefined) {
                        $('#country_error').html(errors['errors']['country_id']);
                    } else {
                        $('#country_error').html('');
                    }
                    if (errors['errors']['position'] != undefined) {
                        $('#position_error').html(errors['errors']['position']);
                    } else {
                        $('#position_error').html('');
                    }
                    if (errors['errors']['position_field'] != undefined) {
                        $('#position_field_error').html(errors['errors']['position_field']);
                    } else {
                        $('#position_field_error').html('');
                    }
                    if (errors['errors']['salary'] != undefined) {
                        $('#salary_error').html(errors['errors']['salary']);
                    } else {
                        $('#salary_error').html('');
                    }
                    if (errors['errors']['job_description'] != undefined) {
                        $('#job_description_error').html(errors['errors']['job_description']);
                    } else {
                        $('#job_description_error').html('');
                    }
                    if (errors['errors']['main_duties'] != undefined) {
                        $('#main_duties_error').html(errors['errors']['main_duties']);
                    } else {
                        $('#main_duties_error').html('');
                    }
                    if (errors['errors']['exp_year_from'] != undefined) {
                        $('#exp_year_from_error').html(errors['errors']['exp_year_from']);
                    } else {
                        $('#exp_year_from_error').html('');
                    }
                    if (errors['errors']['exp_year_to'] != undefined) {
                        $('#exp_year_to_error').html(errors['errors']['exp_year_to']);
                    } else {
                        $('#exp_year_to_error').html('');
                    }
                    if (errors['errors']['exp_month_from'] != undefined) {
                        $('#exp_month_from_error').html(errors['errors']['exp_month_from']);
                    } else {
                        $('#exp_month_from_error').html('');
                    }
                    if (errors['errors']['exp_month_to'] != undefined) {
                        $('#exp_month_to_error').html(errors['errors']['exp_month_to']);
                    } else {
                        $('#exp_month_to_error').html('');
                    }
                }

            })

        }
        function education(is_next, newIndex) {
            $('#edu_add').attr('disabled', true);
            $("a[href='#next']").css({"pointer-events":"none"});
            var form = $('#cv_education')[0]; // You need to use standard javascript object here
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                data: formData,
                url: '/cv_education',
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    $('#example-basic-t-1').addClass('filled');
                    $('#edu_add').attr('disabled', false);
                    $("a[href='#next']").css({"pointer-events":"auto"});
                    $('.for_edit').text('');
                    $('.edit-field').each(function () {
                        $(this).removeAttr('style');
                    })


                    var edu = $('#edu_hidden').val();
                    $('.edu').each(function () {
                        $(this).val('');
                    })
                    $('#edu_year_from').val(1900);
                    $('#edu_month_from').val(1);
                    $('#edu_year_to').val(1900);
                    $('#edu_month_to').val(1);

                    $('.v-error').each(function () {
                        $(this).empty();
                    });
                    if (edu == '') {

                        var new_edu = '<div class="edit-field d-flex single-edu" data-edu="' + result['id'] + '">' +
                            '<p data-id="' + result['id'] + '" class="mb-0 mr-auto educ-name">' + result['name'] + '</p>' +
                            '<span data-id="' + result['id'] + '" class="edu_edit"><img src="/img/edu-edit.svg" alt=""></span>' +
                            '<span data-id="' + result['id'] + '" class="edu_remove">' +
                            '<img src="/img/edu-delete.svg" alt=""></span>' +
                            '</div>';
                        var count = parseInt($("#count_edu").val()) + 1;
                        $("#count_edu").val(parseInt(count));
                        $('.educations').append(new_edu);
                    }
                    else {
                        var name = $("p[data-id='" + result['id'] + "']");
                        console.log(name.attr('data-id'));
                        name.text(result['name']);
                    }
                    $('#edu_hidden').val('');
                    if (is_next) {
                        next = true;
                        $('#example-basic-t-1').addClass('filled');
                        $("#example-basic-t-" + newIndex).click();
                    }


                },
                error: function (data) {
                    $('#edu_add').attr('disabled', false);
                    $("a[href='#next']").css({"pointer-events":"auto"});
                    var errors = data.responseJSON;
                    if (errors['errors']['name'] != undefined) {
                        $('#edu_name_error').html(errors['errors']['name']);
                    } else {
                        $('#edu_name_error').html('');
                    }
                    if (errors['errors']['city'] != undefined) {
                        $('#edu_city_error').html(errors['errors']['city']);
                    } else {
                        $('#edu_city_error').html('');
                    }
                    if (errors['errors']['country_id'] != undefined) {
                        $('#edu_country_error').html(errors['errors']['country_id']);
                    } else {
                        $('#edu_country_error').html('');
                    }
                    if (errors['errors']['type'] != undefined) {
                        $('#edu_type_error').html(errors['errors']['type']);
                    } else {
                        $('#edu_type_error').html('');
                    }
                    if (errors['errors']['study_area'] != undefined) {
                        $('#study_area_error').html(errors['errors']['study_area']);
                    } else {
                        $('#study_area_error').html('');
                    }

                    if (errors['errors']['department'] != undefined) {
                        $('#department_error').html(errors['errors']['department']);
                    } else {
                        $('#department_error').html('');
                    }

                    if (errors['errors']['major_field'] != undefined) {
                        $('#major_field_error').html(errors['errors']['major_field']);
                    } else {
                        $('#major_field_error').html('');
                    }
                    if (errors['errors']['degree'] != undefined) {
                        $('#degree_error').html(errors['errors']['degree']);
                    } else {
                        $('#degree_error').html('');
                    }
                    if (errors['errors']['edu_year_from'] != undefined) {
                        $('#edu_year_from_error').html(errors['errors']['edu_year_from']);
                    } else {
                        $('#edu_year_from_error').html('');
                    }
                    if (errors['errors']['edu_year_to'] != undefined) {
                        $('#edu_year_to_error').html(errors['errors']['edu_year_to']);
                    } else {
                        $('#edu_year_to_error').html('');
                    }
                    if (errors['errors']['edu_month_from'] != undefined) {
                        $('#edu_month_from_error').html(errors['errors']['edu_month_from']);
                    } else {
                        $('#edu_month_from_error').html('');
                    }
                    if (errors['errors']['edu_month_to'] != undefined) {
                        $('#edu_month_to_error').html(errors['errors']['edu_month_to']);
                    } else {
                        $('#edu_month_to_error').html('');
                    }
                }

            })

        }
        function training(is_next, newIndex) {
            $("a[href='#next']").css({"pointer-events":"none"});
            var form = $('#cv_training')[0]; // You need to use standard javascript object here
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                data: formData,
                url: '/cv_training',
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    $("a[href='#next']").css({"pointer-events":"auto"});
                    $('#example-basic-t-8').addClass('filled');
                    $('#for_edit_training').text('');
                    $('.edit-field').each(function () {
                        $(this).removeAttr('style');
                    })
                    if (is_next) {
                        next = true;
                        $("#example-basic-t-" + newIndex).click();
                    }
                    var training = $('#training_hidden').val();
                    $('.training').each(function () {
                        $(this).val('');
                    })
                    $('#training_year_from').val(1900);
                    $('#training_month_from').val(1);
                    $('#training_year_to').val(1900);
                    $('#training_month_to').val(1);
                    $('.v-error').each(function () {
                        $(this).empty();
                    })

                    if (training == '') {

                        var new_training = '<div class="edit-field d-flex" data-training="' + result['id'] + '">' +
                            '<p data-id="' + result['id'] + '" class="mb-0 mr-auto">' + result['name'] + '</p>' +
                            '<span data-id="' + result['id'] + '" class="training_edit"><img src="/img/edu-edit.svg" alt=""></span>' +
                            '<span data-id="' + result['id'] + '" class="training_remove">' +
                            '<img src="/img/edu-delete.svg" alt=""></span>' +
                            '</div>';
                        var count = parseInt($("#count_training").val()) + 1;
                        $("#count_training").val(parseInt(count));
                        $('.trainings').append(new_training);
                    }
                    else {
                        var name = $("p[data-id='" + result['id'] + "']");
                        console.log(name.attr('data-id'));
                        name.text(result['name']);
                    }
                    $('#training_hidden').val('');


                },
                error: function (data) {
                    $("a[href='#next']").css({"pointer-events":"auto"});
                    var errors = data.responseJSON;
                    if (errors['errors']['name'] != undefined) {
                        $('#training_name_error').html(errors['errors']['name']);
                    } else {
                        $('#training_name_error').html('');
                    }
                    if (errors['errors']['city'] != undefined) {
                        $('#training_city_error').html(errors['errors']['city']);
                    } else {
                        $('#training_city_error').html('');
                    }
                    if (errors['errors']['country_id'] != undefined) {
                        $('#training_country_error').html(errors['errors']['country_id']);
                    } else {
                        $('#training_country_error').html('');
                    }
                    if (errors['errors']['type'] != undefined) {
                        $('#training_type_error').html(errors['errors']['type']);
                    } else {
                        $('#training_type_error').html('');
                    }
                    if (errors['errors']['study_area'] != undefined) {
                        $('#training_study_area_error').html(errors['errors']['study_area']);
                    } else {
                        $('#training_study_area_error').html('');
                    }


                    if (errors['errors']['course_name'] != undefined) {
                        $('#course_name_error').html(errors['errors']['course_name']);
                    } else {
                        $('#course_name_error').html('');
                    }

                    if (errors['errors']['certificate_type'] != undefined) {
                        $('#certificate_type_error').html(errors['errors']['certificate_type']);
                    } else {
                        $('#certificate_type_error').html('');
                    }
                    if (errors['errors']['training_year_from'] != undefined) {
                        $('#training_year_from_error').html(errors['errors']['training_year_from']);
                    } else {
                        $('#training_year_from_error').html('');
                    }
                    if (errors['errors']['training_year_to'] != undefined) {
                        $('#training_year_to_error').html(errors['errors']['training_year_to']);
                    } else {
                        $('#training_year_to_error').html('');
                    }
                    if (errors['errors']['training_month_from'] != undefined) {
                        $('#training_month_from_error').html(errors['errors']['training_month_from']);
                    } else {
                        $('#training_month_from_error').html('');
                    }
                    if (errors['errors']['training_month_to'] != undefined) {
                        $('#training_month_to_error').html(errors['errors']['training_month_to']);
                    } else {
                        $('#training_month_to_error').html('');
                    }
                }
            })
        }
        function language(is_next, newIndex) {
            $("a[href='#next']").css({"pointer-events":"none"});
            var current = $('#current_locale').val();
            var form = $('#cv_language')[0]; // You need to use standard javascript object here
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                data: formData,
                url: '/' + current + '/cv_language',
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    $("a[href='#next']").css({"pointer-events":"auto"});
                    $('#example-basic-t-2').addClass('filled');
                    $('.edit-field').each(function () {
                        $(this).removeAttr('style');
                    })
                    $('#for_edit_lang').text('');
                    if (is_next) {
                        next = true;
                        $("#example-basic-t-" + newIndex).click();
                    }
                    var language = $('#lang_hidden').val();
                    $('.language').each(function () {
                        $(this).val('');
                    })
                    $("#radio_native").prop("checked", true);
                    $('.v-error').each(function () {
                        $(this).empty();
                    })
                    if (language == '') {

                        var new_edu = '<div class="edit-field d-flex" data-lang="' + result['id'] + '">' +
                            '<p data-id="' + result['id'] + '" class="mb-0 mr-auto">' + result['code'] + '</p>' +
                            '<span data-id="' + result['id'] + '" class="lang_edit"><img src="/img/edu-edit.svg" alt=""></span>' +
                            '<span data-id="' + result['id'] + '" class="lang_remove">' +
                            '<img src="/img/edu-delete.svg" alt=""></span>' +
                            '</div>';
                        $('.languages').append(new_edu);
                        var count = parseInt($("#count_lang").val()) + 1;
                        $("#count_lang").val(parseInt(count));
                    }
                    else {
                        var name = $("p[data-id='" + result['id'] + "']");
                        name.text(result['code']);
                    }
                    $('#lang_hidden').val('');

                },
                error: function (data) {
                    $("a[href='#next']").css({"pointer-events":"auto"});
                    var errors = data.responseJSON;
                    if (errors['errors']['language_id'] != undefined) {
                        $('#language_id_error').html(errors['errors']['language_id']);
                    } else {
                        $('#language_id_error').html('');
                    }
                    if (errors['errors']['communication'] != undefined) {
                        $('#communication_error').html(errors['errors']['communication']);
                    } else {
                        $('#communication_error').html('');
                    }
                    if (errors['errors']['reading'] != undefined) {
                        $('#reading_error').html(errors['errors']['reading']);
                    } else {
                        $('#reading_error').html('');
                    }
                    if (errors['errors']['total_study'] != undefined) {
                        $('#total_study_error').html(errors['errors']['total_study']);
                    } else {
                        $('#total_study_error').html('');
                    }
                    if (errors['errors']['writing'] != undefined) {
                        $('#writing_error').html(errors['errors']['writing']);
                    } else {
                        $('#writing_error').html('');
                    }

                    if (errors['errors']['typing'] != undefined) {
                        $('#typing_error').html(errors['errors']['typing']);
                    } else {
                        $('#typing_error').html('');
                    }
                    if (errors['errors']['shorthand'] != undefined) {
                        $('#shorthand_error').html(errors['errors']['shorthand']);
                    } else {
                        $('#shorthand_error').html('');
                    }

                    if (errors['errors']['language_type'] != undefined) {
                        $('#language_type_error').html(errors['errors']['language_type']);
                    } else {
                        $('#language_type_error').html('');
                    }

                }

            })

        }
        function visa(is_next, newIndex) {
            $("a[href='#next']").css({"pointer-events":"none"});
            var current = $('#current_locale').val();
            var form = $('#cv_visa')[0]; // You need to use standard javascript object here
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                data: formData,
                url: '/' + current + '/cv_visa',
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    $("a[href='#next']").css({"pointer-events":"auto"});
                    $('#example-basic-t-10').addClass('filled');
                    $('#for_edit_visa').text('');
                    $('.edit-field').each(function () {
                        $(this).removeAttr('style');
                    })
                    if (is_next) {
                        next = true;
                        $("#example-basic-t-" + newIndex).click();
                    }
                    var visa = $('#visa_hidden').val();
                    $('.visa').each(function () {
                        $(this).val('');
                    })
                    $('.v-error').each(function () {
                        $(this).empty();
                    })

                    if (visa == '') {
                        var new_visa = '<div class="edit-field d-flex" data-visa="' + result['id'] + '">' +
                            '<p data-id="' + result['id'] + '" class="mb-0 mr-auto">' + result['country_name'] + '</p>' +
                            '<span data-id="' + result['id'] + '" class="visa_edit"><img src="/img/edu-edit.svg" alt=""></span>' +
                            '<span data-id="' + result['id'] + '" class="visa_remove">' +
                            '<img src="/img/edu-delete.svg" alt=""></span>' +
                            '</div>';
                        var count = parseInt($("#count_visa").val()) + 1;
                        $("#count_visa").val(parseInt(count));
                        $('.visas').append(new_visa);
                    }
                    else {
                        var name = $("p[data-id='" + result['id'] + "']");
                        name.text(result['country_name']);
                    }
                    $('#visa_hidden').val('');

                },
                error: function (data) {
                    $("a[href='#next']").css({"pointer-events":"auto"});
                    var errors = data.responseJSON;
                    if (errors['errors']['period_from'] != undefined) {
                        $('#period_from_error').html(errors['errors']['period_from']);
                    } else {
                        $('#period_from_error').html('');
                    }
                    if (errors['errors']['period_to'] != undefined) {
                        $('#period_to_error').html(errors['errors']['period_to']);
                    } else {
                        $('#period_to_error').html('');
                    }

                    if (errors['errors']['type'] != undefined) {
                        $('#visa_type_error').html(errors['errors']['type']);
                    } else {
                        $('#visa_type_error').html('');
                    }

                    if (errors['errors']['country_id'] != undefined) {
                        $('#visa_country_error').html(errors['errors']['country_id']);
                    } else {
                        $('#visa_country_error').html('');
                    }

                }

            })

        }
        function reference(is_next, newIndex) {
            $("a[href='#next']").css({"pointer-events":"none"});
            var form = $('#cv_reference')[0]; // You need to use standard javascript object here
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                data: formData,
                url: '/cv_reference',
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    $("a[href='#next']").css({"pointer-events":"auto"});
                    $('#example-basic-t-12').addClass('filled');
                    $('#for_edit_ref').text('');
                    $('.edit-field').each(function () {
                        $(this).removeAttr('style');
                    })
                    if (is_next) {
                        next = true;
                        $("#example-basic-t-" + newIndex).click();
                    }
                    var ref = $('#ref_hidden').val();
                    $('.ref').each(function () {
                        $(this).val('');
                    })
                    $('#duration').val(0);
                    $('.v-error').each(function () {
                        $(this).empty();
                    })
                    if (ref == '') {
                        var new_ref = '<div class="edit-field d-flex" data-ref="' + result['id'] + '">' +
                            '<p data-id="' + result['id'] + '" class="mb-0 mr-auto">' + result['full_name'] + '</p>' +
                            '<span data-id="' + result['id'] + '" class="ref_edit"><img src="/img/edu-edit.svg" alt=""></span>' +
                            '<span data-id="' + result['id'] + '" class="ref_remove">' +
                            '<img src="/img/edu-delete.svg" alt=""></span>' +
                            '</div>';
                        $('.refs').append(new_ref);
                        var count = parseInt($("#count_ref").val()) + 1;
                        $("#count_ref").val(parseInt(count));
                    }
                    else {

                        var name = $("p[data-id='" + result['id'] + "']");
                        name.text(result['full_name']);

                    }

                    $('#ref_hidden').val('');
                },
                error: function (data) {
                    $("a[href='#next']").css({"pointer-events":"auto"});
                    var errors = data.responseJSON;
                    if (errors['errors']['title'] != undefined) {
                        $('#ref_title_error').html(errors['errors']['title']);
                    } else {
                        $('#ref_title_error').html('');
                    }
                    if (errors['errors']['full_name'] != undefined) {
                        $('#full_name_error').html(errors['errors']['full_name']);
                    } else {
                        $('#full_name_error').html('');
                    }
                    if (errors['errors']['position'] != undefined) {
                        $('#ref_position_error').html(errors['errors']['position']);
                    } else {
                        $('#ref_position_error').html('');
                    }

                    if (errors['errors']['relationship'] != undefined) {
                        $('#ref_relationship_error').html(errors['errors']['relationship']);
                    } else {
                        $('#ref_relationship_error').html('');
                    }
                    if (errors['errors']['duration'] != undefined) {
                        $('#duration_error').html(errors['errors']['duration']);
                    } else {
                        $('#duration_error').html('');
                    }
                    if (errors['errors']['duration_type'] != undefined) {
                        $('#duration_type_error').html(errors['errors']['duration_type']);
                    } else {
                        $('#duration_type_error').html('');
                    }
                    if (errors['errors']['current_position'] != undefined) {
                        $('#current_position_error').html(errors['errors']['current_position']);
                    } else {
                        $('#current_position_error').html('');
                    }
                    if (errors['errors']['email'] != undefined) {
                        $('#ref_email_error').html(errors['errors']['email']);
                    } else {
                        $('#ref_email_error').html('');
                    }
                    if (errors['errors']['phone'] != undefined) {
                        $('#ref_phone_error').html(errors['errors']['phone']);
                    } else {
                        $('#ref_phone_error').html('');
                    }


                }

            })

        }

        $(document).ready(function () {

            $(document).on('click', '.till_now', function () {
                if ($(".till_now").prop('checked') == true) {
                    $('#exp_year_to').val(0);
                    $('#exp_month_to').val(0);
                    $('#exp_year').css('display', 'none');
                    $('#exp_month').css('display', 'none');
                }
                else {
                    $('#exp_year').css('display', 'block');
                    $('#exp_month').css('display', 'block');
                }

            })


            $(document).on('click', '.view-cv', function () {
                var id = $('#view_cv_id').val();

                $.ajax({

                    type: "POST",
                    data: {
                        'id': id,
                    },
                    url: '/preview/cv',
                    success: function (result) {
                        if (result == 'success') {
                            window.location.href = '/cv/' + id;

                        }
                        else {
                            $('#preview_resume').modal('show');
                        }
                    }
                });

            })


            for (var i = 4; i < 15; i++) {
                $('#example-basic-t-' + i).css('display', 'none')
            }
            $('.steps').append('<div class="d-flex align-items-center"><button id="advanced" class="btn btn-view-cv mr-auto">{{trans('resume.more')}}</button><span class="view-cv ml-auto pr-2 pt-2">{{trans('resume.view_your_cv')}}</span></div>');
            $(document).on('click', '#advanced', function () {
                $("#advanced").remove();
                for (var i = 4; i < 15; i++) {
                    $('#example-basic-t-' + i).css('display', 'block')
                }
                $('#skip').css('display', 'inline-flex');
                $('#finish4-div').css('display', 'none');
            });

            var input = $('<li class="skip_div" aria-hidden="true"><a id="skip"  role="menuitem">{{trans('resume.skip')}}</a></li>');
            input.appendTo($('ul[aria-label=Pagination]'));

            var input = $('<li id="finish4-div"" style="display:none" aria-hidden="true"><a id="finish4" role="menuitem">{{trans('resume.finish')}}</a></li>');
            input.appendTo($('ul[aria-label=Pagination]'));

            $(document).delegate('#skip', 'click', function () {
                next = true;
                var a = $("#example-basic").steps("next");
                if (!a) {
                    $(".example-basic").steps("finish");
                }
            });


            //filepond
            FilePond.registerPlugin(
                // encodes the file as base64 data
                FilePondPluginFileEncode,

                // validates the size of the file
                FilePondPluginFileValidateSize,

                // corrects mobile image orientation
                FilePondPluginImageExifOrientation,
                //
                // // edit image plugin
                // FilePondPluginImageEdit,

                // previews dropped images
                FilePondPluginImagePreview
            );


            // Select the file input and use create() to turn it into a pond
            const pond = FilePond.create(
                document.querySelector('.filepond'),
                {
                    allowMultiple: true,
                }
            );
            id = $('#attachment_cv_id').val();

            $.ajax({

                type: "POST",
                data: {
                    'id': id,
                },
                url: '/cv/attachments',
                success: function (result) {

                    if (result.length != 0) {
                        for (var i in result) {
                            pond.addFile('/storage/' + result[i].attachment);

                        }
                    }
                }
            });


            $('.change_step_btn').on('click', function () {
                var val = $(this).attr('data-value');
                change_step = val;
            })


            $('#exp_add').on('click', function () {
                experience(false, 4);
            });
            $(document).on('click', '.exp_edit', function () {
                var id = $(this).attr('data-id');
                $('#exp_hidden').val(id);
                if (id != '') {
                    $.ajax({
                        type: "POST",
                        data: {'id': id},
                        url: '/get_experience',
                        success: function (result) {
                            $('.till_now').prop('checked', false);
                            $('.edit-field').each(function () {
                                $(this).removeAttr('style');
                            })

                            var div = $("div[data-exp='" + id + "']");
                            div.css('background-color', '#27c9d7');
                            $("#cv_experience")[0].reset();
                            $('.v-error').each(function () {
                                $(this).empty();
                            })
                            $('.exp').each(function () {
                                $(this).val('');
                            })
                            var period_from = result['period_from'].split('-');
                            var exp_year_from = period_from[0];
                            var exp_month_from = period_from[1];

                            if (result['period_to'] != '-') {
                                var period_to = result['period_to'].split('-');
                                var exp_year_to = period_to[0];
                                var exp_month_to = period_to[1];
                            }
                            else {
                                $('.till_now').prop('checked', true);
                                $('#exp_year').css('display', 'none');
                                $('#exp_month').css('display', 'none');
                            }


                            $('#for_edit_exp').text(result['name']);

                            $("#experience").val(result['id']);

                            $('#name').val(result['name']);
                            $('#city').val(result['city']);
                            $('#position').val(result['position']);
                            $('#salary').val(result['salary']);
                            $("#exp_country").val(result['country_id']);
                            $("#position_field").val(result['position_field']);
                            $('#currency').val(result['currency']);
                            $('#period').val(result['period']);
                            $("#exp_year_from").val(exp_year_from);
                            $("#exp_month_from").val(exp_month_from);
                            $("#exp_year_to").val(exp_year_to);
                            $("#exp_month_to").val(exp_month_to);
                            $('#job_description').val(result['job_description']);
                            $('#main_duties').val(result['main_duties']);
                            $('#supervisor_name').val(result['supervisor_name']);
                            $('#employer_numbers').val(result['employer_numbers']);
                            $('#reason').val(result['reason']);

                            $('#exp_remove').attr('data-id', result['id']);
                            $('#exp_remove').css('display', 'block');


                        },
                        error: function (data) {
                            console.log(data);
                        }
                    })
                }
            })
            $(document).on('click', '.exp_remove', function () {
                var id = $(this).attr('data-id');
                $(this).css({"pointer-events":"none"});
                $.ajax({
                    type: "POST",
                    data: {'id': id},
                    url: '/delete_experience',
                    success: function (result) {
                        $(this).css({"pointer-events":"auto"});
                        var exp = $('#exp_hidden').val();
                        var div = $("div[data-exp='" + id + "']");
                        if (exp == id) {
                            $('.till_now').prop('checked', false);
                            $('#exp_year').css('display', 'block');
                            $('#exp_month').css('display', 'block');
                            $('#for_edit_exp').text('');
                            $('.exp').each(function () {
                                $(this).val('');
                            })
                            $('.v-error').each(function () {
                                $(this).empty();
                            })
                            $('#exp_hidden').val('');
                        }
                        div.remove();
                        var count = parseInt($("#count_exp").val())- 1;
                        if(count==0)
                        {
                            $('#example-basic-t-6').removeClass('filled');
                        }
                        $("#count_exp").val(parseInt(count));
                    },
                    error: function (data) {
                        $(this).css({"pointer-events":"auto"});
                        console.log(data);
                    }

                });
            });


            $('#edu_add').on('click', function () {
                education(false, 4);
            });

            $(document).on('click', '.edu_edit', function () {
                var id = $(this).attr('data-id');
                $('#edu_hidden').val(id);


                if (id != '') {
                    $.ajax({
                        type: "POST",
                        data: {'id': id},
                        url: '/get_education',
                        success: function (result) {
                            $('.for_edit').text(result['name']);
                            $('.edit-field').each(function () {
                                $(this).removeAttr('style');
                            })
                            var div = $("div[data-edu='" + id + "']");
                            div.css('background-color', '#27c9d7');

                            $('.edu').each(function () {
                                $(this).val('');
                            })
                            $('#edu_year_from').val(1900);
                            $('#edu_month_from').val(1);
                            $('#edu_year_to').val(1900);
                            $('#edu_month_to').val(1);

                            $('.v-error').each(function () {
                                $(this).empty();
                            })
                            var period_from = result['period_from'].split('-');
                            var edu_year_from = period_from[0];
                            var edu_month_from = period_from[1];

                            var period_to = result['period_to'].split('-');
                            var edu_year_to = period_to[0];
                            var edu_month_to = period_to[1];

                            $('#edu_name').val(result['name']);
                            $('#edu_city').val(result['city']);
                            $('#edu_type').val(result['type']);
                            $('#department').val(result['department']);
                            $("#edu_country").val(result['country_id']);
                            $("#study_area").val(result['study_area']);
                            $("#edu_year_from").val(edu_year_from);
                            $("#edu_month_from").val(edu_month_from);
                            $("#edu_year_to").val(edu_year_to);
                            $("#edu_month_to").val(edu_month_to);
                            $('#major_field').val(result['major_field']);
                            $('#degree').val(result['degree']);
                            $('#edu_remove').attr('data-id', result['id']);
                            $('#edu_remove').css('display', 'block');
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    })
                }
            })
            $(document).on('click', '.edu_remove', function () {
                var id = $(this).attr('data-id');
                $(this).css({"pointer-events":"none"});

                $.ajax({
                    type: "POST",
                    data: {'id': id},
                    url: '/delete_education',
                    success: function (result) {
                        $(this).css({"pointer-events":"auto"});
                        var edu = $('#edu_hidden').val();
                        var div = $("div[data-edu='" + id + "']");
                        $('.v-error').each(function () {
                            $(this).empty();
                        });

                        if (edu == id) {
                            $('.for_edit').text('');
                            $('.edu').each(function () {
                                $(this).val('');
                            })
                            $('#edu_year_from').val(1900);
                            $('#edu_month_from').val(1);
                            $('#edu_year_to').val(1900);
                            $('#edu_month_to').val(1);
                            $('#edu_hidden').val('');
                        }
                        div.remove();
                        var count = parseInt($("#count_edu").val()) - 1;
                        if(count==0)
                        {
                            $('#example-basic-t-1').removeClass('filled');

                        }
                        $("#count_edu").val(parseInt(count));

                    },
                    error: function (data) {
                        $(this).css({"pointer-events":"auto"});
                        console.log(data);
                    }

                });
            });

            $('#training_add').on('click', function () {
                training(false, 4);
            });
            $(document).on('click', '.training_edit', function () {
                var id = $(this).attr('data-id');
                $('#training_hidden').val(id);
                if (id != '') {
                    $.ajax({
                        type: "POST",
                        data: {'id': id},
                        url: '/get_training',
                        success: function (result) {
                            $('.edit-field').each(function () {
                                $(this).removeAttr('style');
                            })
                            var div = $("div[data-training='" + id + "']");
                            div.css('background-color', '#27c9d7');
                            $('#for_edit_training').text(result['name']);
                            $('.training').each(function () {
                                $(this).val('');
                            })
                            $('#training_year_from').val(1900);
                            $('#training_month_from').val(1);
                            $('#training_year_to').val(1900);
                            $('#training_month_to').val(1);
                            $('.v-error').each(function () {
                                $(this).empty();
                            })
                            var period_from = result['period_from'].split('-');
                            var training_year_from = period_from[0];
                            var training_month_from = period_from[1];

                            var period_to = result['period_to'].split('-');
                            var training_year_to = period_to[0];
                            var training_month_to = period_to[1];


                            $("#training").val(result['id']);

                            $('#training_name').val(result['name']);
                            $('#training_city').val(result['city']);
                            $('#training_type').val(result['type']);
                            $("#training_country").val(result['country_id']);
                            $("#training_study_area").val(result['study_area']);
                            $("#training_year_from").val(training_year_from);
                            $("#training_month_from").val(training_month_from);
                            $("#training_year_to").val(training_year_to);
                            $("#training_month_to").val(training_month_to);
                            $('#course_name').val(result['course_name']);
                            $('#certificate_type').val(result['certificate_type']);
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    })
                }
                else {
                    $('.training').each(function () {
                        $(this).val('');
                    })
                    $('.v-error').each(function () {
                        $(this).empty();
                    })
                    $('#training_year_from').val(1900);
                    $('#training_month_from').val(1);
                    $('#training_year_to').val(1900);
                    $('#training_month_to').val(1);
                    $('#training_remove').removeAttr("data-id");
                    $('#training_remove').css('display', 'none');

                }

            })

            $(document).on('click', '.training_remove', function () {
                var id = $(this).attr('data-id');
                $(this).css({"pointer-events":"none"});
                $.ajax({
                    type: "POST",
                    data: {'id': id},
                    url: '/delete_training',
                    success: function (result) {
                        $(this).css({"pointer-events":"auto"});
                        var training = $('#training_hidden').val();
                        var div = $("div[data-training='" + id + "']");
                        $('.v-error').each(function () {
                            $(this).empty();
                        });
                        if (training == id) {
                            $('#for_edit_training').text('');
                            $('.training').each(function () {
                                $(this).val('');
                            })
                            $('#training_year_from').val(1900);
                            $('#training_month_from').val(1);
                            $('#training_year_to').val(1900);
                            $('#training_month_to').val(1);
                            $('.v-error').each(function () {
                                $(this).empty();
                            })
                            $('#training_hidden').val();
                        }
                        div.remove();
                        var count = parseInt($("#count_training").val()) - 1;
                        if(count == 0)
                        {
                            $('#example-basic-t-8').removeClass('filled');
                        }
                        $("#count_training").val(parseInt(count));
                    },
                    error: function (data) {
                        console.log(data);
                    }

                });
            });


            $('#language_add').on('click', function () {
                language(false, 4);
            });
            $(document).on('click', '.lang_edit', function () {
                var current = $('#current_locale').val();
                var id = $(this).attr('data-id');
                $('#lang_hidden').val(id);
                if (id != '') {
                    $.ajax({
                        type: "POST",
                        data: {'id': id},
                        url: '/' + current + '/get_language',
                        success: function (result) {
                            $('.edit-field').each(function () {
                                $(this).removeAttr('style');
                            })
                            var div = $("div[data-lang='" + id + "']");
                            div.css('background-color', '#27c9d7');
                            $('#for_edit_lang').text(result['code']);

                            $('.language').each(function () {
                                $(this).val('');
                            })

                            $('.v-error').each(function () {
                                $(this).empty();
                            })
                            $("#language").val(result['id']);
                            $("#language_id").val(result['language_id']);
                            $("#radio_" + result['language_type']).prop("checked", true);
                            $("#education").val(result['id']);
                            $('#reading').val(result['reading']);
                            $('#writing').val(result['writing']);
                            $('#communication').val(result['communication']);
                            $('#total_study').val(result['total_study']);
                            $("#typing").val(result['typing']);
                            $("#shorthand").val(result['shorthand']);
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    })
                }


            })
            $(document).on('click', '.lang_remove', function () {
                var id = $(this).attr('data-id');
                $(this).css({"pointer-events":"none"});
                $.ajax({
                    type: "POST",
                    data: {'id': id},
                    url: '/delete_language',
                    success: function (result) {
                        $(this).css({"pointer-events":"auto"});
                        var lang = $('#lang_hidden').val();
                        var div = $("div[data-lang='" + id + "']");
                        if (lang == id) {
                            $('#for_edit_lang').text('');
                            $('.language').each(function () {
                                $(this).val('');
                            })
                            $("#radio_native").prop("checked", true);
                            $('.v-error').each(function () {
                                $(this).empty();
                            })
                            $('#lang_hidden').val('');
                        }
                        div.remove();
                        var count = parseInt($("#count_lang").val()) - 1;
                        if(count==0)
                        {
                            $('#example-basic-t-2').removeClass('filled');
                        }
                        $("#count_lang").val(parseInt(count));
                    },
                    error: function (data) {
                        console.log(data);
                        $(this).css({"pointer-events":"auto"});
                    }

                });
            });


            $('#visa_add').on('click', function () {
                visa(false, 4);
            });
            $(document).on('click', '.visa_edit', function () {
                var current = $('#current_locale').val();
                var id = $(this).attr('data-id');
                $('#visa_hidden').val(id);
                if (id != '') {
                    $.ajax({
                        type: "POST",
                        data: {'id': id},
                        url: '/' + current + '/get_visa',
                        success: function (result) {
                            $('.edit-field').each(function () {
                                $(this).removeAttr('style');
                            })
                            var div = $("div[data-visa='" + id + "']");
                            div.css('background-color', '#27c9d7');
                            $('#for_edit_visa').text(result['country_name']);
                            $('.visa').each(function () {
                                $(this).val('');
                            })
                            $('.v-error').each(function () {
                                $(this).empty();
                            })
                            $("#visa").val(result['id']);
                            $("#visa_country_id").val(result['country_id']);
                            $("#visa_type").val(result['type']);
                            $('#visa_period_from').val(result['period_from']);
                            $('#visa_period_to').val(result['period_to']);
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    })
                }

                else {
                    $('.visa').each(function () {
                        $(this).val('');
                    })
                    $('.v-error').each(function () {
                        $(this).empty();
                    })
                    $('#visa_remove').removeAttr("data-id");
                    $('#visa_remove').css('display', 'none');
                }

            })
            $(document).on('click', '.visa_remove', function () {
                var id = $(this).attr('data-id');
                $(this).css({"pointer-events":"none"});


                $.ajax({
                    type: "POST",
                    data: {'id': id},
                    url: '/delete_visa',
                    success: function (result) {
                        $(this).css({"pointer-events":"auto"});
                        var visa = $('#visa_hidden').val();
                        var div = $("div[data-visa='" + id + "']");
                        if (visa == id) {
                            $('#for_edit_visa').text('');
                            $('.visa').each(function () {
                                $(this).val('');
                            })
                            $('.v-error').each(function () {
                                $(this).empty();
                            })
                            $('#visa_hidden').val('');
                        }
                        div.remove();
                        var count = parseInt($("#count_visa").val()) - 1;
                        if(count==0)
                        {
                            $('#example-basic-t-10').removeClass('filled');
                        }
                        $("#count_visa").val(parseInt(count));
                    },
                    error: function (data) {
                        console.log(data);
                    }

                });
            });


            $('#ref_add').on('click', function () {
                reference(false, 4);
            });
            $(document).on('click', '.ref_edit', function () {
                var id = $(this).attr('data-id');
                $('#ref_hidden').val(id);
                if (id != '') {
                    $.ajax({
                        type: "POST",
                        data: {'id': id},
                        url: '/get_reference',
                        success: function (result) {
                            $('.edit-field').each(function () {
                                $(this).removeAttr('style');
                            })
                            var div = $("div[data-ref='" + id + "']");
                            div.css('background-color', '#27c9d7');
                            $('#for_edit_ref').text(result['full_name']);
                            $('.ref').each(function () {
                                $(this).val('');
                            })
                            $('#duration').val(0);

                            $('.v-error').each(function () {
                                $(this).empty();
                            })
                            $("#ref").val(result['id']);
                            $("#ref_title").val(result['title']);
                            $("#full_name").val(result['full_name']);
                            $('#duration').val(result['duration']);
                            $('#duration_type').val(result['duration_type']);
                            $('#ref_relationship').val(result['relationship']);
                            $('#ref_email').val(result['email']);
                            $('#ref_phone').val(result['phone']);
                            $('#ref_position').val(result['position']);
                            $('#current_position').val(result['current_position']);
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    })
                }
                else {
                    $('.ref').each(function () {
                        $(this).val('');
                    })
                    $('#duration').val(0);
                    $('.v-error').each(function () {
                        $(this).empty();
                    })
                    $('#ref_remove').removeAttr("data-id");
                    $('#ref_remove').css('display', 'none');
                }

            })
            $(document).on('click', '.ref_remove', function () {
                var id = $(this).attr('data-id');
                $(this).css({"pointer-events":"none"});

                $.ajax({
                    type: "POST",
                    data: {'id': id},
                    url: '/delete_reference',
                    success: function (result) {
                        $(this).css({"pointer-events":"auto"});
                        var ref = $('#ref_hidden').val();
                        var div = $("div[data-ref='" + id + "']");
                        if (ref == id) {
                            $('.ref').each(function () {
                                $(this).val('');
                            })
                            $('#duration').val(0);
                            $('.v-error').each(function () {
                                $(this).empty();
                            })
                            $('#ref_hidden').val('');
                            $('#for_edit_ref').text('');
                        }
                        div.remove();
                        var count = parseInt($("#count_ref").val()) - 1;
                        if(count == 0)
                        {
                            $('#example-basic-t-12').removeClass('filled');
                        }
                        $("#count_ref").val(parseInt(count));
                    },
                    error: function (data) {
                        $(this).css({"pointer-events":"auto"});
                        console.log(data);
                    }

                });
            });
            var key_phone = $('#key_phone_count').val();

            var a = key_phone;
            var sum1 = key_phone;
            $('#add4').on('click', function () {

                if (sum1 <= 4) {
                    ++a;

                    var select_phone = '<select id="phone_type_' + a + '" class="form-control mt-2" name="phone_type[]"><option></option>';
                        @foreach(config('enums.phone_type') as $item)
                    {
                        select_phone += "<option value='{{$item}}'>{{trans('register_step_2.'.$item)}}</option>"
                    }
                    @endforeach
                        select_phone += '</select>';
                    $('#dynamic_field4').append('<div id="row4_' + a + '" class="dynamic-added deleting4">' +
                        '<div class="form-group">' +
                        '<input id="phone_' + a + '" class="phone" required name="phone[]" class="deleting-input4_1 " value="">' +
                        '<label for="phone_' + a + '"  class="control-label">{{trans("register_step_2.phone")}}' + a + '</label>' +
                        '<span role="alert" id="phone_' + a + '_error"></span>' +
                        '<button type="button" name="remove" id="' + a + '" class="btn btn-danger deleting-button4 btn_remove 4">X</button> </div>' +
                        '<div class="form-group">' +
                        select_phone +
                        '<label  class="control-label" for="phone_type_' + a + '">{{trans('register_step_2.phone_type')}}' + a + '</label>' +
                        '</div>' +
                        '</div>');
                    sum1++;

                }
                $('#key_phone_count').val(a);
            });

            $(document).on('click', '.4', function () {
                var button_id3 = $(this).attr("id");

                var key_phone_val = $('#key_phone_count').val();
                $('#key_phone_count').val(key_phone_val - 1);
                $('#row4_' + button_id3).remove();
                sum1--;
                a--;

            });

            var key_add = $('#key_address_count').val();
            var l = key_add;
            var sum = key_add;

            $('#add3').on('click', function () {

                var select = '<select class="form-control" name="country[]" required id="address-country' + l + '"><option></option>';
                @foreach($countries as $item)
                    select += "<option value ={{$item->id}}>{{trans('countries.'.$item->code)}}</option>"
                @endforeach
                    select += '</select>';


                if (sum <= 4) {
                    ++l;
                    $('#dynamic_field3').append(
                        '<div id="row3_' + l + '" class="deleting3 position-relative mb-4">' +
                        '<div class="form-group">' +
                        '<input required  name="address[]" class="form-control deleting-input3_1 " value="" id="address' + l + '">' +
                        '<label for="address' + l + '" class="control-label">{{trans('register_step_2.address')}} ' + l + '</label>' +
                        '<span role="alert" id="address_' + l + '_error"></span></div>' +
                        '<div class="form-group">' +
                        '<select id="address_type_' + l + '" class="" name="address_type[]"><option value=""></option><option value="home">{{trans('register_step_2.home')}}</option><option value="business">{{trans('register_step_2.business')}}</option></select>' +
                        '<label  class="control-label" for="address_type_' + l + '">{{trans('register_step_2.address_type')}}' + l + '</label>' +
                        '</div>' +
                        '<div class="form-group">' +
                        select + '' +
                        '<label for="address-country' + l + '" class="control-label">{{trans('register_step_2.country')}} ' + l + '</label>' +
                        '<span role="alert" id="country_' + l + '_error"></span>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<input id="city' + l + '" type="text" required name="city[]" class="deleting-input3_1 " value="">' +
                        '<label for="City' + l + '" class="control-label">{{trans('register_step_2.city')}} ' + l + '</label>' +
                        '<span role="alert" id="city_' + l + '_error"></span>' +
                        '</div>' +
                        '<div class="form-group"> ' +
                        '<input type="text" required  name="zip[]" class="deleting-input3_1" value="" id="zip' + l + '">' +
                        '<label for="zip' + l + '" class="control-label">{{trans('register_step_2.zip')}}' + l + '</label>' +
                        '<span  role="alert" id="zip_' + l + '_error"></span>' +
                        '</div>' +
                        '<button type="button" name="remove" id="' + l + '" class="btn btn-danger btn-danger-2 deleting-button3 btn_remove 3">{{trans('register_step_2.delete')}} ' + l + '</button>' +
                        '</div>');
                    sum++;
                }
                $('#key_address_count').val(l);
            });

            $(document).on('click', '.3', function () {
                var button_id3 = $(this).attr("id");

                var key_address_val = $('#key_address_count').val();
                $('#key_address_count').val(key_address_val - 1);
                $('#row3_' + button_id3).remove();
                sum--;
                l--;


//
//
//
//                var count = 2;
//                $(".deleting3").each(function (key, element) {
//                    $(this).attr("id", 'row2_' + count);
//                    $(this).find('.deleting-input3_1').attr('name', 'started_' + count);
//                    $(this).find('.deleting-input3_2').attr('name', 'finished_' + count);
//                    $(this).find('.deleting-input3_3').attr('name', 'status_' + count);
//                    $(this).find('.deleting-input3_4').attr('name', 'institution_' + count);
//                    $(this).find('.deleting-button3').attr('id', count);
//                    count++;
//                });
            });

            $('#disabled').on('change', function () {

                $('#disabled_details').css('display', 'none');
                $('#disabled_details').val('');
                var value = $(this).val();
                if (value == 'yes') {
                    $('#disabled_details-label').val('');
                    $('#disabled_details').css('display', 'block');

                }
            });

            $('#travel').on('change', function () {

                $('#travel_reason_div').css('display', 'none');
                $('#travel_reason').val('');
                var value = $(this).val();
                if (value == 'no') {
                    $('#travel_reason').val('');
                    $('#travel_reason_div').css('display', 'block');

                }
            });

            $('#dependants').on('change', function () {
                $('#dependants_details').css('display', 'none');
                var value = $(this).val();
                if (value == 'yes') {
                    $('#dependants_details').css('display', 'block');
                    $('.dependants').each(function () {
                        $(this).val('');
                    })

                }
                else {
                    $('.dependants').each(function () {
                        $(this).val('');
                    })
                    $('#d_count').val(0);
                }
            })
            $('#arrested').on('change', function () {
                $('#arrested_details').css('display', 'none');
                $('#arrested_details').val('xzvxcvzxc');

                var value = $(this).val();
                if (value == 'yes') {
                    $('#arrested_details').css('display', 'block');
                    $('#arrested_details').val('aa');

                }
            })
            $('#citizenship').select2();
            $('#country_id').select2();
            $('#work_type').select2();
            $('#position_type').select2();
            $('#org_type').select2();
            $('#interest_area').select2();

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    var cv_id = $('#cv_id').val();

                    reader.onload = function (e) {

                        $.ajax({
                            url: "{{route('cv-image')}}",
                            type: "POST",
                            data: {
                                image: e.target.result,
                                'cv_id': cv_id
                            },
                            success: function (data) {
                                $('#cv_image').attr('src', e.target.result);
                                $('#delete').css('display', 'block');
                            },
                            error: function (data) {
                                var errors = data.responseJSON;
                                console.log(errors);
                            }
                        });

                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#cv_file").change(function () {
                $('#cv_image_error').html('');
                var type = this.files[0].type;
                var mime = type.substring(0, 5);

                var size = this.files[0].size;
                if (size > 3000000) {
                    $('#cv_image_error').html('The image must be less than 3mb');
                    $('#cv_image').attr('src', '');
                    $('#cv_file').val('');
                    $('#delete').css('display', 'none');
                }
                else {
                    $('#cv_image_error').html('');
                    if (mime == 'image') {
                        $('#cv_image_error').html('');
                        readURL(this);
                    }
                    else {
                        var cv_id = $('#cv_id').val();
                        $.ajax({
                            url: "{{route('cv-image')}}",
                            type: "POST",
                            data: {
                                image: 'no_image',
                                'cv_id': cv_id
                            },
                            success: function (data) {
                                $('#cv_image').attr('src', '');
                                $('#delete').css('display', 'none');
                                $('#cv_image_error').html('The image must be  image');
                            },
                            error: function (data) {
                                var errors = data.responseJSON;
                                console.log(errors);
                            }
                        });


                    }
                }


            });
            $('#delete').on('click', function () {
                var cv_id = $('#cv_id').val();
                $.ajax({
                    url: "{{route('cv-image')}}",
                    type: "POST",
                    data: {
                        'delete': 'yes',
                        'cv_id': cv_id
                    },
                    success: function (data) {
                        $("#cv_file").val('');
                        $('#cv_image').attr('src', '');
                        $('#delete').css('display', 'none');
                    }
                });


            })


            //dependants
            var key_d = $('#d_count').val();
            var d = key_d;
            var sum2 = key_d;
            $('#add_dependant').on('click', function () {


                if (sum2 <= 4) {
                    ++d;
                    var relationship = '<select class="dependants"  name="relationship[]" required id="relationship_' + d + '"><option></option>';
                    @foreach(config('enums.relationship') as $item)
                        relationship += "<option value ='{{$item}}'>{{trans('cv_personal.'.$item)}}</option>"
                    @endforeach
                        relationship += '</select>';
                    $('#dynamic_field').append('<div id="row_' + d + '">' +
                        '<div class="form-group">' +
                        '<input id="name_' + d + '"  required name="name[]" class="dependants deleting-dependant" value="">' +
                        '<label for="name' + d + '"  class="control-label">{{trans("resume.name")}}</label>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<input max="9999-12-31"  type="date" id="birthday_' + d + '"  required name="birth_day[]" class="dependants deleting-dependant" value="">' +
                        '<label for="birthday' + d + '"  class="control-label">{{trans("resume.birthday")}}</label>' +
                        '</div>' +
                        '<div class="form-group">' +
                        relationship +
                        '<label class="control-label" for=="relationship_' + d + '">{{trans('resume.relationship')}}</label>' +
                        '</div>' +
                        '<button type="button" data-id="' + d + '" id="remove_' + d + '" class="btn btn-danger btn-danger-2 mb-3 delete_dependant">{{trans('resume.delete')}}</button>' +
                        '</div>'
                    );
                    sum2++;
                }
                $('#d_count').val(d);

            });
            $(document).on('click', '.delete_dependant', function () {

                var id = $(this).attr('data-id');
                var d_count = $('#d_count').val();
                $('#d_count').val(d_count - 1);
                $('#row_' + id).remove();
                d--;
                sum2--;
            });

            $('#willing_to_volunteer').on('change', function () {
                $('#volunteering_details').css('display', 'block');
                var value = $(this).val();
                if (value == 'no') {
                    $('#volunteering_details').css('display', 'none');
                    $("#work_type").select2('data', null);
                    $('#public_works').val('');
                    $("#country_id").select2('val', null);
                }
            });

            $('#finish4').on('click', function () {
                var resume_id = $('#availabilit_cv_id').val();
                var form = $('#cv_availability')[0];
                var formData = new FormData(form);
                formData.append('finish', 'true');
                $.ajax({
                    type: "POST",
                    data: formData,
                    url: '/cv_availability',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        if (result == 'finish') {
                            location.href = "/cv/" + resume_id;
                        }
                        else if (result == 'fail') {
                            $('#preview_resume').modal('show');
                        }
                    },
                    error: function (data) {
                        console.log(data);

                        var errors = data.responseJSON;

                        if (errors['errors']['term'] != undefined) {
                            $('#term_error').html(errors['errors']['term']);
                        } else {
                            $('#term_error').html('');
                        }
                        if (errors['errors']['long_salary'] != undefined) {
                            $('#long_salary_error').html(errors['errors']['long_salary']);
                        } else {
                            $('#long_salary').html('');
                        }


                    }
                });
            })


        });


        $("#example-basic").steps({
            labels: {
                next: "{{trans('resume.next')}}",
                previous: "{{trans('resume.previous')}}",
                finish: "{{trans('resume.finish')}}",
            },
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true,
            enableAllSteps: true,
            onStepChanging: function (event, currentIndex, newIndex) {

                $("span[role='alert']").text('');
                if (newIndex == 3 && currentIndex != 2) {
                    if ($('#example-basic-t-4:visible').length == 0) {
                        $('.skip_div').css('display', 'none');
                        $('#skip').css('display', 'none');
                    }

                }
                else {
                    $('#skip').css('display', 'inline_flex');
                    $('.skip_div').css('display', 'block');
                    $('#finish4-div').css('display', 'none');
                }

                if (newIndex == 14) {
                    $('#skip').css('display', 'none');
                }
                else {
                    $('#skip').css('display', 'inline-flex');
                }
                if (currentIndex > newIndex) {
                    return true;
                }

                if (currentIndex == 0) {


                    if (next == true) {

                        next = false;
                        return true;
                    }

                    var form = $('#cv_contacts')[0]; // You need to use standard javascript object here
                    var formData = new FormData(form);

                    $.ajax({
                        type: "POST",
                        data: formData,
                        url: '/cv_contacts',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            next = true;
                            $('#example-basic-t-0').addClass('filled');
                            $("#example-basic-t-" + newIndex).click();
                        },
                        error: function (data) {
                            var address_count = $('#key_address_count').val();
                            var phone_count = $('#key_phone_count').val();
                            var errors = data.responseJSON;
                            for (var i = 0; i < phone_count; i++) {
                                var tiv = i + 1;
                                if (errors['errors']['phone.' + i] != undefined) {

                                    $('#phone_' + tiv + '_error').html(errors['errors']['phone.' + i]);
                                } else {
                                    $('#phone_' + tiv + '_error').html('');
                                }
                            }

                            for (var i = 0; i < address_count; i++) {
                                var tiv = i + 1;
                                if (errors['errors']['country.' + i] != undefined) {

                                    $('#country_' + tiv + '_error').html(errors['errors']['country.' + i]);
                                } else {
                                    $('#country_' + tiv + '_error').html('');
                                }
                                if (errors['errors']['address.' + i] != undefined) {
                                    $('#address_' + tiv + '_error').html(errors['errors']['address.' + i]);
                                } else {
                                    $('#address_' + tiv + '_error').html('');
                                }
                                if (errors['errors']['zip.' + i] != undefined) {
                                    $('#zip_' + tiv + '_error').html(errors['errors']['zip.' + i]);
                                } else {
                                    $('#zip_' + tiv + '_error').html('');
                                }
                                if (errors['errors']['city.' + i] != undefined) {
                                    $('#city_' + tiv + '_error').html(errors['errors']['city.' + i]);
                                } else {
                                    $('#city_' + tiv + '_error').html('');
                                }
                            }
                            if (errors['errors']['first_name'] != undefined) {
                                $('#first_name_error').html(errors['errors']['first_name']);
                            } else {
                                $('#first_name_error').html('');
                            }

                            if (errors['errors']['last_name'] != undefined) {
                                $('#last_name_error').html(errors['errors']['last_name']);
                            } else {
                                $('#last_name_error').html('');
                            }

                            if (errors['errors']['middle_name'] != undefined) {
                                $('#middle_name_error').html(errors['errors']['middle_name']);
                            } else {
                                $('#middle_name_error').html('');
                            }
                            if (errors['errors']['title'] != undefined) {
                                $('#title_error').html(errors['errors']['title']);
                            } else {
                                $('#title_error').html('');
                            }
                            if (errors['errors']['residence_country_id'] != undefined) {
                                $('#residence_country_id_error').html(errors['errors']['residence_country_id']);
                            } else {
                                $('#residence_country_id_error').html('');
                            }
                            if (errors['errors']['email'] != undefined) {
                                $('#email_error').html(errors['errors']['email']);
                            } else {

                                $('#email_error').html('');
                            }

                        }
                    });

                }
                if (currentIndex == 1) {
                    var count = $("#count_edu").val();
                    if (count != 0) {
                        next = true;
                    }

                    if (next == true) {
                        next = false;
                        return true;
                    }
                    education(true, newIndex);


                }
                if (currentIndex == 2) {
                    var count = $("#count_lang").val();
                    if (count != 0) {
                        next = true;
                    }

                    if (next == true) {
                        if (newIndex == 3) {
                            if ($('#example-basic-t-4:visible').length == 0) {
                                $('.skip_div').css('display', 'none');
                                $('#skip').css('display', 'none');
                                $('#finish4-div').css('display', 'block');
                            }

                        }
                        next = false;
                        return true;
                    }
                    language(true, newIndex);


                }
                if (currentIndex == 3) {


                    if (next == true) {
                        next = false;
                        return true;
                    }

                    var form = $('#cv_availability')[0];
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        data: formData,
                        url: '/cv_availability',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            $('#example-basic-t-3').addClass('filled');
                            if (newIndex > 3) {
                                $("#advanced").remove();
                                for (var i = 4; i < 15; i++) {
                                    $('#example-basic-t-' + i).css('display', 'block')
                                }
                                $('#skip').css('display', 'inline-flex');
                                $('.skip_div').css('display', 'block');
                                $('#finish4-div').css('display', 'none');
                            }
                            next = true;
                            $("#example-basic-t-" + newIndex).click();
                        },
                        error: function (data) {
                            console.log(data);

                            var errors = data.responseJSON;

                            if (errors['errors']['term'] != undefined) {
                                $('#term_error').html(errors['errors']['term']);
                            } else {
                                $('#term_error').html('');
                            }
                            if (errors['errors']['long_salary'] != undefined) {
                                $('#long_salary_error').html(errors['errors']['long_salary']);
                            } else {
                                $('#long_salary').html('');
                            }


                        }
                    });


                }
                if (currentIndex == 4) {
                    var special_skills = $('#special_skills').val();

                    if (next == true) {
                        next = false;
                        return true;
                    }
                    if(special_skills == '')
                    {
                        $('#example-basic-t-4').removeClass('filled');
                        return true;
                    }
                    else
                    {
                        var form = $('#cv_skills')[0]; // You need to use standard javascript object here
                        var formData = new FormData(form);
                        $.ajax({
                            type: "POST",
                            data: formData,
                            url: '/cv_skills',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (result) {
                                next = true;
                                $('#example-basic-t-4').addClass('filled');
                                $("#example-basic-t-" + newIndex).click();

                            },
                            error: function (data) {
                                console.log(data);
                            }
                        })
                    }


                }
                if (currentIndex == 5) {

                    if (next == true) {
                        next = false;
                        return true;
                    }
                    var form = $('#cv_volunteering')[0]; // You need to use standard javascript object here
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        data: formData,
                        url: '/cv_volunteering',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            next = true;
                            $('#example-basic-t-5').addClass('filled');
                            $("#example-basic-t-" + newIndex).click();

                        },
                        error: function (data) {
                            console.log(data);
                        }
                    })
                }
                if (currentIndex == 6) {
                    var count = $("#count_exp").val();
                    if (count != 0) {
                        next = true;
                    }
                    if (next == true) {
                        next = false;
                        return true;
                    }
                    experience(true, newIndex);

                }
                if (currentIndex == 7) {

                    if (next == true) {
                        next = false;
                        return true;
                    }
                    var result = 1;
                    var d_result = $('#dependants').val();
                    if (d_result == 'yes') {
                        var error = 0;
                        $(':input[required]', '#dependants_details').each(function () {

                            if ($(this).val() == '') {
                                $(this).css('border', '1px solid red');
                                result = 0;
                                error = 1;
                            }
                        });
                        if (error == 1) {
                            result = 0
                        } else {
                            $(':input[required]', '#dependants_details').each(function () {
                                $(this).css('border', '1px solid #006db7');
                            });
                            result = 1;
                        }
                    }
                    if (result == 1) {

                        var form = $('#cv_personal')[0]; // You need to use standard javascript object here
                        var formData = new FormData(form);
                        $.ajax({
                            type: "POST",
                            data: formData,
                            url: '/cv_personal',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (result) {
                                next = true;
                                $('#example-basic-t-7').addClass('filled');
                                $('.error').each(function () {
                                    $(this).val('');
                                })
                                $("#example-basic-t-" + newIndex).click();

                            },
                            error: function (data) {
                                var errors = data.responseJSON;

                                if (errors['errors']['birthday'] != undefined) {
                                    $('#personal_birthday_error').html(errors['errors']['birthday']);
                                } else {
                                    $('#personal_error').html('');
                                }
                                if (errors['errors']['sex'] != undefined) {
                                    $('#personal_sex_error').html(errors['errors']['sex']);
                                } else {
                                    $('#personal_sex_error').html('');
                                }
                                if (errors['errors']['citizenship_id'] != undefined) {
                                    $('#personal_citizenship_error').html(errors['errors']['citizenship_id']);
                                } else {
                                    $('#personal_citizenship_error').html('');
                                }
                                if (errors['errors']['height'] != undefined) {
                                    $('#personal_height_error').html(errors['errors']['height']);
                                } else {
                                    $('#personal_height_error').html('');
                                }
                                if (errors['errors']['weight'] != undefined) {
                                    $('#personal_weight_error').html(errors['errors']['weight']);
                                } else {
                                    $('#personal_weight_error').html('');
                                }

                            }
                        });
                    }

                }
                if (currentIndex == 8) {
                    var count = $("#count_training").val();
                    if (count != 0) {
                        next = true;
                    }
                    if (next == true) {
                        next = false;
                        return true;
                    }
                    training(true, newIndex);
                }
                if (currentIndex == 9) {
                    var objective = $('#objective').val();
                    if (next == true) {
                        next = false;
                        return true;
                    }
                    if (objective == '') {
                        $('#example-basic-t-9').removeClass('filled');
                        return true;
                    }
                    else {
                        var form = $('#cv_objective')[0]; // You need to use standard javascript object here
                        var formData = new FormData(form);
                        $.ajax({
                            type: "POST",
                            data: formData,
                            url: '/cv_objective',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (result) {
                                next = true;
                                $('#example-basic-t-9').addClass('filled');
                                $("#example-basic-t-" + newIndex).click();

                            },
                            error: function (data) {
                                console.log(data);
                            }
                        })
                    }


                }
                if (currentIndex == 10) {

                    var count = $("#count_visa").val();
                    if (count != 0) {
                        next = true;
                    }
                    if (next == true) {
                        next = false;
                        return true;
                    }
                    visa(true, newIndex);
                }
                if (currentIndex == 11) {

                    var level = $('#level').val();
                    var software = $('#software').val();
                    var hardware = $('#hardware').val();
                    if (next == true) {
                        next = false;
                        return true;
                    }

                    if (level == '' && software == '' && hardware == '') {
                        $('#example-basic-t-11').removeClass('filled');
                        return true;
                    }
                    else {
                        var form = $('#cv_computer_skills')[0]; // You need to use standard javascript object here
                        var formData = new FormData(form);
                        $.ajax({
                            type: "POST",
                            data: formData,
                            url: '/cv_computer_skills',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (result) {
                                next = true;
                                $('#example-basic-t-11').addClass('filled');
                                $("#example-basic-t-" + newIndex).click();

                            },
                            error: function (data) {
                                console.log(data);
                            }
                        })
                    }


                }
                if (currentIndex == 12) {
                    var count = $("#count_ref").val();
                    if (count != 0) {
                        next = true;
                    }
                    if (next == true) {
                        next = false;
                        return true;
                    }
                    reference(true, newIndex);
                }
                if (currentIndex == 13) {

                    if (next == true) {
                        next = false;
                        return true;
                    }

                    var form = $('#cv_questions')[0]; // You need to use standard javascript object here
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        data: formData,
                        url: '/cv_questions',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            console.log(result);
                            $('#example-basic-t-13').addClass('filled');
                            next = true;
                            $("#example-basic-t-" + newIndex).click();
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });

                }
                if (currentIndex == 14) {

                    if (next == true) {
                        next = false;
                        return true;
                    }
                    var form = $('#cv_attachments')[0]; // You need to use standard javascript object here
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        data: formData,
                        url: '/cv_attachments',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            console.log(result);
                            next = true;
                            $('#example-basic-t-14').addClass('filled');
                            $("#example-basic-t-" + newIndex).click();

                        },
                        error: function (data) {
                            console.log(data);
                        }
                    })
                }


            },
            onFinished: function (event, currentIndex) {


                var form = $('#cv_attachments')[0]; // You need to use standard javascript object here
                var formData = new FormData(form);
                var resume_id = $('#availabilit_cv_id').val();

                $.ajax({
                    type: "POST",
                    data: formData,
                    url: '/cv_attachments',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        $('#example-basic-t-14').addClass('filled');
                        if (result == 'finish') {
                            location.href = "/cv/" + resume_id;
                        }
                        else if (result == 'fail') {
                            $('#preview_resume').modal('show');
                        }

                    },
                    error: function (data) {
                        console.log(data);
                    }
                })
            }


        });

    </script>
@endsection







