<div class="step-container">

    <div id="example-basic">
        <h3>{{trans('resume.contact_information')}}</h3>

        @include('profile.resume.steps.contacts')

        <h3>{{trans('resume.background_education')}}</h3>
        @include('profile.resume.steps.education')

        <h3>{{trans('resume.language_skills')}}</h3>
        @include('profile.resume.steps.language')

        <h3>{{trans('resume.availability')}}</h3>
        @include('profile.resume.steps.availability')





        <h3>{{trans('resume.special_skills')}}</h3>
        @include('profile.resume.steps.special_skills')

        <h3>{{trans('resume.volunteering')}}</h3>
        @include('profile.resume.steps.volunteering')

        <h3>{{trans('resume.professional_experience')}}</h3>
        @include('profile.resume.steps.experience')

        <h3>{{trans('resume.personal_information')}}</h3>
        @include('profile.resume.steps.personal')


        <h3>{{trans('resume.short_term_training')}}</h3>
        @include('profile.resume.steps.training')

        <h3>{{trans('resume.objective')}}</h3>
        @include('profile.resume.steps.objective')


        <h3>{{trans('resume.visa')}}</h3>
        @include('profile.resume.steps.visa')


        <h3>{{trans('resume.computer_skills')}}</h3>
        @include('profile.resume.steps.computer_skills')

        <h3>{{trans('resume.references')}}</h3>
        @include('profile.resume.steps.reference')

        <h3>{{trans('resume.questions')}}</h3>
        @include('profile.resume.steps.questions')
        <h3>{{trans('resume.attachments')}}</h3>


        @include('profile.resume.steps.attachments')



    </div>


</div>


<!-- Modal -->
<div class="modal fade" id="preview_resume" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('resume.fail')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <h5 class="mb-5" style="color: #8c3030;">{{trans('resume.fill_first_4_steps')}} </h5>
            </div>

        </div>
    </div>
</div>