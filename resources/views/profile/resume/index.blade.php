@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')
    <section class="position-relative bg-grey">
        @include('profile.menu')

        <div class="profile-container float-right">
            <div class="profile-table">
                <div class="d-flex justify-content-between">
                    @permission('create_resume')
                        <a href="{{route('resume.create')}}" class="btn btn-orange-gradient mr-2">{{trans('resume.create')}}</a>
                    @endpermission
                    <form id="myForm1" action="{{route('resume.index')}}"
                          class="profile-search-form d-flex position-relative ml-auto">
                        <input value="{{$search}}" id="search" name="search" class="form-control" type="search"
                               placeholder="{{trans('profile_tables.search')}}…" aria-label="Search">
                        <button id="search_btn" class="btn" type="button"><img src="/img/search.svg" alt=""></button>
                    </form>
                </div>

                <div class="table-responsive">
                    <table class="table mobile-table">
                        <thead>
                        <tr class="table-tr-th">
                            <th>
                                {{trans('profile_tables.last_updated_on')}}
                            </th>
                            <th>
                                {{trans('profile_tables.full_name')}}
                            </th>
                            <th>{{trans('profile_tables.age')}}</th>
                            <th>{{trans('resume.city')}}</th>
                            <th>{{trans('profile_tables.salary')}}</th>
                            <th class="text-center">{{trans('profile_tables.action')}}</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($resumes as $item)

                            <tr class="col-sd-3">
                                <td data-label="{{trans('profile_tables.last_updated_on')}}">@if(isset($item)){{$item->updated_at->format('M d, Y H:i')}}@endif</td>
                                <td data-label=" {{trans('profile_tables.full_name')}}">@if(isset($item) && isset($item->contacts)){{$item->contacts->first_name}} {{$item->contacts->last_name}}@endif</td>
                                <td data-label="{{trans('profile_tables.age')}}">@if(isset($item) && isset($item->personal) && isset($item->personal->birthday)) {{Carbon\Carbon::parse($item->personal->birthday)->age}} @endif</td>
                                <td data-label="{{trans('resume.city')}}">@if(isset($item) && count($item->address)!=0) {{$item->address[0]->city}} @endif</td>
                                <td data-label="{{trans('profile_tables.salary')}}">
                                    @if(isset($item) && isset($item->availability) && isset($item->availability->long_salary))

                                        {{$item->availability->long_salary}} {{trans('currency.'.$item->availability->long_currency)}}
                                        {{trans('currency.'.$item->availability->long_period)}}
                                    @endif
                                </td>


                                <td data-label="{{trans('profile_tables.action')}}">
                                    <div class="table-buttons d-flex justify-content-center align-items-center">
                                        <a href="{{route('showCv',$item->id)}}" title="{{trans('profile_tables.view')}}"><img src="/img/visibility.svg" alt=""></a>
                                        {{--@if(Auth::id() == $item->user_id)--}}
                                            @permission('edit_resume')
                                            <a href="{{route('resume.create',$item->id)}}" title="{{trans('profile_tables.edit')}}"><img src="/img/edit.svg" alt=""></a>
                                            @endpermission
                                        {{--@endif--}}
                                        <a href="{{route('resumePDF',$item->id)}}" class="pdf-down" title="{{trans('profile_tables.download')}}"><img src="/img/download.svg" alt=""></a>

                                        @permission('delete_resume')
                                        {!! Form::open([
                                        'method'=>'DELETE',
                                        'route' => ['resume.destroy',$item->id],
                                        'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<img src="/img/delete.svg">', array(
                                                'type' => 'submit',
                                                'class' => '',
                                                'title' => trans('resume.delete'),
                                                'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        {!! Form::close() !!}
                                        @endpermission

                                    </div>
                                </td>
                            </tr>

                        @endforeach

                        </tbody>
                    </table>
                </div>

                <div class="d-flex justify-content-end my-pagination align-items-center">
                    <form id="myForm" action="{{route('resume.index')}}">
                        <div>
                            <label for="rr">Item per page:</label>

                            <select name="order" id="rr">
                                <option @if($order==20) selected @endif value="20">20</option>
                                <option @if($order==50) selected @endif value="50">50</option>
                                <option @if($order==100) selected @endif value="100">100</option>
                            </select>

                        </div>
                    </form>

                    <div class="pagination-links d-flex">
                        {{ $resumes->appends(['search' => Request::get('search'),'order_by' => Request::get('order_by'),'order' => Request::get('order')])->links('vendor.pagination.bootstrap-4')}}
                    </div>
                </div>


            </div>
        </div>

    </section>
@endsection
@section('scripts')
    <script>
        $('#rr').on('change', function () {
            var search = $('#search').val();
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "search").val(search).appendTo('#myForm');
            $("#myForm").submit();

        });
        $('#search_btn').on('click', function () {
            var order = $('#rr').val();
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "order").val(order).appendTo('#myForm1');
            $("#myForm1").submit();
        });
    </script>
@endsection


