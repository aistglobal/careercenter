@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')

    <section class="position-relative">
        @include('profile.menu')

        {!! Form::open(['route' => 'blogs.store', 'class' => 'form-horizontal', 'files' => true,'id'=>'job_form']) !!}

        @include ('profile.blog.form')
        {!! Form::close() !!}

    </section>


@endsection
@section('scripts')
    <script type="text/javascript" src="/js/moment.min.js" defer></script>
@endsection







