<div class="profile-container float-right">
    <ul class="nav nav-pills mb-3 profile-tab" role="tablist">
        @foreach($langs as $localeCode => $properties)
            <li class="nav-item">
                <a class="nav-link  @if($loop->first)  active @endif" id="job-tab" data-toggle="pill"
                   href="#tab-{{$localeCode}}" role="tab"
                   aria-controls="tab-{{$localeCode}}"
                   aria-selected="true">{{trans('profile_tables.'.$properties['name'])}}</a>
            </li>

        @endforeach
    </ul>
    <div class="tab-content profile-tab-content">
        {{--tab 1--}}
        @foreach($langs as $localeCode => $properties)

            <div id="tab-{{$localeCode}}" class="tab-pane @if($loop->first) active @endif fade show">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::text("title_$localeCode",isset($data[$localeCode]) ? $data[$localeCode]->title : null)}}
                            <label for="{{"title_$localeCode"}}" class="control-label">{{trans('blogs.title')}}
                                <span class="star">*</span></label>
                            {!! $errors->first("title_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group">
                            {{ Form::text("short_description_$localeCode",isset($data[$localeCode]) ? $data[$localeCode]->short_description : null)}}
                            <label for="{{"short_description_$localeCode"}}"
                                   class="control-label">{{trans('blogs.short_description')}}
                                <span class="star">*</span></label>
                            {!! $errors->first("short_description_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group no-focus">
                            <label for="{{"content_$localeCode"}}">{{trans('blogs.content')}}
                                <span class="star">*</span></label>
                            {{ Form::textarea("content_$localeCode",isset($data[$localeCode]) ? $data[$localeCode]->content : null,['rows'=>3])}}
                            {!! $errors->first("content_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group">
                            {{ Form::text("meta_title_$localeCode",isset($data[$localeCode]) ? $data[$localeCode]->meta_title : null)}}
                            <label for="{{"meta_title_$localeCode"}}"
                                   class="control-label">{{trans('blogs.meta_title')}}
                                <span class="star">*</span></label>
                            {!! $errors->first("meta_title_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group">
                            {{ Form::textarea("meta_description_$localeCode",isset($data[$localeCode]) ? $data[$localeCode]->meta_description : null)}}
                            <label for="{{"meta_description_$localeCode"}}"
                                   class="control-label">{{trans('blogs.meta_description')}}
                                <span class="star">*</span></label>
                            {!! $errors->first("meta_description_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group">
                            {{ Form::textarea("meta_keywords_$localeCode",isset($data[$localeCode]) ? $data[$localeCode]->meta_keywords : null)}}
                            <label for="{{"meta_keywords_$localeCode"}}"
                                   class="control-label">{{trans('blogs.meta_keywords')}}
                                <span class="star">*</span></label>
                            {!! $errors->first("meta_keywords_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="row">

            <div class="col-md-12 cover-image">
                <div class="form-label">
                    {{ Form::label('image', trans('blogs.cover_image'))}}
                </div>

                <div class="file-input-style form-group-file">
                    <label for="cover_image" class="sr-only">{{trans('blogs.upload_image')}}</label>
                    <input type="file" name="image" id="cover_image">
                </div>

                @if(isset($blog))
                    <div class="col-12 col-md-6 p-0">
                        <img src="{{Storage::disk('')->url($blog->image)}}" alt="" class="img-fluid">
                    </div>
                @endif

                {!! $errors->first('image', '<p style="color:red" class="help-block">:message</p>') !!}
            </div>

        </div>

        <div class="d-flex justify-content-end mt-5">
            <div class="text-center">
                <button type="submit" class="btn btn-orange-gradient save">{{trans('blogs.save')}}</button>
            </div>
        </div>
        @permission('all_article')
        @if(isset($blog))
        @if(isset($blog->user))
        <h5>{{trans('blogs.added_by')}} :
        {{$blog->user->username}}</h5>
        @endif
        @if(isset($blog->editor))
        <h5>  {{trans('blogs.last_updated_by')}} :
        {{$blog->editor->username}}</h5>

        @endif
        @endif
        @endpermission
    </div>

</div>
@section('scripts')
    <script src="//cdn.ckeditor.com/4.7.1/full/ckeditor.js"></script>
    <script>
        var locale = '{{ Config::get('app.locale')}}';
        $('a[href="#tab-' + locale + '"]').tab('show');
                @foreach($langs as $localeCode => $properties)
        var selector_content = 'content_' + '{{$localeCode}}';
        CKEDITOR.replace(selector_content);
        @endforeach
        @if($errors->has('title_en') || $errors->has('content_en') || $errors->has('short_description_en')  || $errors->has('meta_description_en') || $errors->has('meta_keywords_en'))
          $('a[href="#tab-en"]').tab('show');
        @elseif ($errors->has('title_ru') || $errors->has('content_ru') || $errors->has('short_description_ru') || $errors->has('meta_description_ru') || $errors->has('meta_keywords_ru') )
         $('a[href="#tab-ru"]').tab('show');
        @elseif($errors->has('title_hy') || $errors->has('content_hy') || $errors->has('short_description_hy') || $errors->has('meta_description_hy') || $errors->has('meta_keywords_hy'))
         $('a[href="#tab-hy"]').tab('show');
        @endif

    </script>
@endsection