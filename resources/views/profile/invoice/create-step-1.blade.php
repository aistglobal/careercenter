@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }

        .table-header {
            background-color: #014a7c;
            padding: 10px 15px;
            border-radius: 5px;
            margin-bottom: 5px;
            color: white;
        }
    </style>
@endsection

@section('content')
    <section class="position-relative bg-grey">
        @include('profile.menu')

        <div class="profile-container float-right">

            <div class="profile-table">
                <div class="d-flex justify-content-between">
                    <a href="{{route('invoice.index')}}" class="mb-0 p-2 d-inline-block"><i
                                class="arrow-left-orange-heavy">Left</i></a>

                    {{--<form id="myForm1" action="{{route('invoice.index')}}"--}}
                    {{--class="profile-search-form d-flex position-relative ">--}}
                    {{--<input value="{{$search}}" id="search" name="search" class="form-control" type="search"--}}
                    {{--placeholder="{{trans('profile_tables.search')}}…" aria-label="Search">--}}
                    {{--<button id="search_btn" class="btn" type="button"><img src="/img/search.svg" alt=""></button>--}}
                    {{--</form>--}}
                    <div class="form-group mb-0">
                        <select class="form-control" name="currency" id="currency">
                        @foreach(config('bank-details.currency') as $cur)
                        <option @if($currency == $cur) selected
                        @endif  value="{{$cur}}">{{trans('invoice.'.$cur)}}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                @permission('all_invoice')
                <a href="{{route('invoice.create')}}" class="btn btn-orange-gradient mr-2">Create for Admins</a>
                @endpermission


                <div class="table-responsive">
                    @foreach($categories as $item)
                        <div class="w-100 table-header pt-2 pb-2">
                            {{$item->data->name}}
                        </div>
                        <table class="table mobile-table">
                            <thead>
                            <tr class="table-tr-th">
                                <th>{{trans('invoice.name')}}</th>
                                <th>{{trans('profile_tables.quantity')}}</th>
                                <th style="width: 190px">
                                    {{trans('profile_tables.amount')}}
                                </th>
                                <th>{{trans('profile_tables.sum')}}</th>

                            </tr>
                            </thead>

                            <tbody>

                            @foreach($item->services as $service)
                                @if(isset($service->data))
                                    <tr class="col-sd-3" data-id="{{$service->id}}">
                                        <td data-label="{{trans('profile_tables.name')}}">@if(isset($service->data)){{$service->data->name}}@endif</td>
                                        <td data-label="{{trans('invoice.quantity')}}">
                                            {!! Form::selectYear('quantity', 0, 20, null,['id'=>'quantity','class'=>"form-control col-md-5 quantity"]) !!}
                                        </td>
                                        <td data-amd="{{$service->amd}}" data-usd="{{$service->usd}}"
                                            data-euro="{{$service->euro}}" data-label="{{trans('invoice.amount')}}"
                                            class="amount">
                                            {{$service->amd}}
                                        </td>
                                        <td data-sum="0" data-label="{{trans('profile_tables.sum')}}" class="sum">
                                            0
                                        </td>
                                    </tr>
                                @endif
                            @endforeach

                            </tbody>
                        </table>
                    @endforeach

                </div>
                <div class="text-right">
                    <input disabled="disabled" type="text" id="total" value="0"
                           class="form-control col-sm-3 ml-auto mb-2">
                </div>


                <form method="POST" id="myFormInvoice" action="{{route('createInvoice')}}">
                    @csrf
                    <input type="hidden" name="quant" id="quant">
                    <input type="hidden" name="serv" id="serv">
                    <input type="hidden" name="cur" id="cur">

                    <button type="button" class="btn btn-blue-gradient" id="continue">Continue</button>
                </form>


                <div class="d-flex justify-content-end my-pagination align-items-center">
                    <form id="myForm" action="{{route('invoice.index')}}">
                        <div>
                            <label for="rr">{{trans('jobs.item_per_page')}}:</label>

                            <select name="order_by" id="rr">
                                <option @if($order==20) selected @endif value="20">20</option>
                                <option @if($order==50) selected @endif value="50">50</option>
                                <option @if($order==100) selected @endif value="100">100</option>
                            </select>

                        </div>
                    </form>

                    <div class="pagination-links d-flex">
                        {{ $categories->appends(['search' => Request::get('search'),'order_by' => Request::get('order_by')])->links('vendor.pagination.bootstrap-4')}}
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="create_invoice" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">quanity 0</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <h5 class="mb-5" style="color: #8c3030;">Please select quantity </h5>
                    </div>

                </div>
            </div>
        </div>

        <!-- Modal Invoice Fail -->
        <div class="modal fade" id="fail-invoice" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Fail</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <h5 class="mb-5" style="color: #8c3030;">You can not create invoice, because you don't have bank
                            details. Please fill it. </h5>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        var total = parseInt($('#total').val());


        $('#currency').on('change', function () {
            var currency = $('#currency').val();
            total = 0;
            $('.quantity').each(function () {
                var val = $(this).val();
                var amount = $(this).closest('tr').find('.amount').attr('data-' + currency);
                $(this).closest('tr').find('.amount').text(amount);
                var newAmount = val * amount;
                total += newAmount;
                $(this).closest('tr').find('.sum').text(newAmount);
                $(this).closest('tr').find('.sum').attr("data-sum", newAmount);
            });
            $('#total').val(total);
        })


        $('.quantity').on('change', function () {
            var quantity = $(this).val();
            var currency = $('#currency').val();
            var amount = $(this).closest('tr').find('.amount').attr('data-' + currency);
            var sum = $(this).closest('tr').find('.sum').attr('data-sum')
            total -= sum;
            var newAmount = quantity * amount;
            $(this).closest('tr').find('.sum').text(newAmount);
            $(this).closest('tr').find('.sum').attr("data-sum", newAmount);
            total += newAmount;
            $('#total').val(total);
        })
        $('#continue').on('click', function () {
            var quant = [];
            var serv = [];
            $('.quantity').each(function () {
                var val = $(this).val();
                var serv_id = $(this).closest('tr').attr('data-id')
                if (val != 0) {
                    quant.push(val);
                    serv.push(serv_id)
                }
            });
            $('#quant').val(quant);
            $('#serv').val(serv);
            var currency = $('#currency').val();
            $('#cur').val(currency);
            if (quant.length != 0) {
                $("#myFormInvoice").submit();
            }
            else {

                $('#create_invoice').modal('show');
            }

        })
        @if(Session::has('fail-invoice'))
$('#fail-invoice').modal('show')
        {{Session::forget('fail-invoice')}}
        @endif

    </script>
@endsection