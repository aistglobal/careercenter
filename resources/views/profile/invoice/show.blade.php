@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }

        .invoice-show-table {
            padding: 0 15px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        .invoice-show-table-content {
            max-width: 1200px;
            width: 100%;
        }

        .text-bold {
            font-family: lato-bold, sans-serif;
        }

        .span-width {
            min-width: 150px;
            display: inline-block;
            text-align: right;
            padding-right: 10px;
        }

        .with-border-bottom {
            border-bottom: 1px solid;
            margin-left: 5px;
            padding: 0 5px;
        }
        @media (max-width: 767px){
            .span-width{
                min-width: inherit;
                margin: 7px 0 3px;
                width: 100%;
                text-align: left;
            }
            .first-table.mobile-table td{
                padding: 5px!important;
            }
        }
    </style>
@endsection

@section('content')

    <section class="position-relative bg-grey">
        @include('profile.menu')
        <div class="profile-container float-right">
            <div class="invoice-show-table pt-5 pb-5 profile-table">
                <div class="invoice-show-table-content">
                    <h2 class="text-center pb-4">
                        Hashiv carayutyunneri matucum
                    </h2>
                    <div class="table-responsive">
                        <table class="table table-bordered mb-0 mobile-table first-table">
                            <tbody style="vertical-align: top;">
                            <tr>
                                <td style="width: 60%" >

                                    <div>
                                        <span class="text-bold span-width" >{{trans('invoice.performer')}}</span>
                                        <span>{{$invoice->performer}}</span>
                                    </div>
                                    <div>
                                        <span class="text-bold span-width">{{trans('invoice.address')}}</span>
                                        <span> {{$invoice->performer_address}}</span>
                                    </div>
                                    <div>
                                    <span class="text-bold span-width">
                                        {{trans('invoice.account')}}</span>
                                        <span>    {{$invoice->performer_account}}</span>

                                        <span class="text-bold span-width">{{trans('invoice.tax')}}</span>
                                        <span>    {{$invoice->performer_tax}}</span>

                                    </div>
                                    <div>
                                        <span class="text-bold span-width">{{trans('invoice.bank')}}</span>
                                        <span>  {{$invoice->performer_bank}}</span>
                                    </div>


                                </td>
                                <td>
                                    <div>{{trans('invoice.booking_number')}} N {{$invoice->booking_number}}</div>
                                    <span class="text-bold">{{trans('invoice.account_product')}}
                                        N </span>
                                    <span>{{$invoice->account_product}}</span>
                                    <br>
                                    {{ \Carbon\Carbon::parse($invoice->date)->format('d M Y')}}
                                </td>
                            </tr>
                            <tr>

                                <td style="width: 60%">

                                    <div>
                                        <span class="text-bold span-width">{{trans('invoice.client')}}</span>
                                        <span>{{$invoice->client}}</span>
                                    </div>
                                    <div>
                                        <span class="text-bold span-width">{{trans('invoice.address')}}</span>
                                        <span> {{$invoice->client_address}}</span>
                                    </div>
                                    @if($invoice->individual == 0)
                                        <div>

                                        <span class="text-bold span-width">
                                            {{trans('invoice.account')}}</span>
                                            <span>    {{$invoice->client_account}}</span>

                                            <span class="text-bold span-width">{{trans('invoice.tax')}}</span>
                                            <span>    {{$invoice->client_tax}}</span>

                                        </div>
                                        <div>
                                            <span class="text-bold span-width">{{trans('invoice.bank')}}</span>
                                            <span>  {{$invoice->client_bank}}</span>
                                        </div>

                                    @else
                                        <div>
                                            <span class="text-bold span-width">{{trans('invoice.passport')}}</span>
                                            <span> {{$invoice->passport}}</span>
                                        </div>

                                    @endif


                                </td>


                                    <td>
                                        @if(isset($invoice->posted_announcements) && $invoice->posted_announcements!='' )
                                        <div>{{trans('invoice.posted_announcements')}}</div>
                                        {{$invoice->posted_announcements}}
                                        @endif
                                    </td>

                            </tr>
                            @if(isset($invoice->notes) && $invoice->notes!='' )
                                <tr>
                                    <td colspan="2" class="border-bottom-0">
                                        <span class="text-bold">{{trans('invoice.notes')}}</span>
                                        {{$invoice->notes}}
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td colspan="2" class="border-bottom-0">
                                    <span class="text-bold">Lracum</span> poxancumov AAh-ov vcharvox fdsgdg dfgjhdfjkg
                                    gdfjghfjdghg
                                </td>
                            </tr>

                            </tbody>


                        </table>

                    </div>
                    <div class="table-responsive mt-1">

                        <table class="table table-bordered text-center mobile-table">
                            <thead>
                            <tr class="table-tr-th">
                                {{--<td class="text-bold text-center" style="width:80px;">hamaar</td>--}}
                                <th class="text-bold">{{trans('invoice.name')}}</th>
                                <th class="text-bold">{{trans('invoice.quantity')}}</th>
                                <th class="text-bold">{{trans('invoice.measure')}}</th>
                                <th class="text-bold">{{trans('invoice.amount')}} ({{$invoice->currency}})</th>
                                <th class="text-bold">{{trans('invoice.sum')}} ({{$invoice->currency}})</th>
                            </tr>
                            </thead>

                            @foreach($invoice->invoiceItems as $item)

                                <tr>

                                    <td data-label="{{trans('invoice.name')}}">
                                        {{$item->name}}
                                    </td>

                                    <td data-label="{{trans('invoice.quantity')}}">
                                        {{$item->quantity}}
                                    </td>

                                    <td data-label="{{trans('invoice.measure')}}">
                                        {{$item->measure}}
                                    </td>

                                    <td data-label="{{trans('invoice.amount')}}">
                                        {{$item->amount}}
                                    </td>

                                    <td data-label="{{trans('invoice.sum')}}">
                                        {{$item->sum}}
                                    </td>
                                </tr>
                            @endforeach


                            <tr>

                                <td colspan="4" class="d-md-table-cell d-none">{{trans('invoice.total')}}</td>

                                <td  data-label="{{trans('invoice.total')}}">
                                    {{$invoice->money}} ({{$invoice->currency}})
                                </td>


                            </tr>
                            <tr>

                                <td colspan="5">tivy barerov</td>
                            </tr>
                        </table>


                    </div>
                    <div class="d-flex justify-content-between flex-wrap pt-4" style="max-width: 1000px">
                        <div class="d-flex flex-column mb-3">
                            <span class="text-bold">"profexx spy"</span><br>
                            <div>
                                <span>Tnoren`</span><br>
                                <span>A Eremyan</span>
                                <span class="with-border-bottom"></span>
                            </div>

                            <div class="mt-auto">k.t.</div>
                        </div>

                        @if($invoice->indiivdual == 1)
                            <div class="mb-3">
                                <span class="text-bold">Career</span><br>
                                <div>
                                    <div>
                                        <span>Tnoren`</span><br>

                                        {{--<span>fghfhhfg</span>--}}
                                        <span class="with-border-bottom">{{$invoice->director}}</span>
                                    </div>
                                </div>
                                <div>
                                    <div>
                                        {{--<span>Tnoren`</span><br>--}}
                                        {{--<span>gfhhgh</span>--}}
                                        <span>{{$invoice->position}}</span>
                                    </div>
                                </div>
                                <div class="mt-5">k.t.</div>
                            </div>
                        @endif
                    </div>
                    <div class="d-flex align-items-center justify-content-between flex-wrap pt-3">
                        @permission('edit_invoice')
                        <a href="{{route('invoice.edit',$invoice->id)}}" class="btn btn-orange-gradient"> {{trans('profile_tables.edit')}}</a>
                        @endpermission

                        <form method="POST" id="myFormInvoice" action="{{route('saveInvoice',$invoice->id)}}">
                            @csrf


                            <button type="submit" class="btn btn-blue-gradient" >Save</button>
                        </form>

                    </div>

                </div>

            </div>
        </div>


    </section>


@endsection