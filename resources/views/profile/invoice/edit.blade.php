@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')

    <section class="position-relative">
        @include('profile.menu')

        {!! Form::model($invoice, [
                              'method' => 'PATCH',
                              'route' => ['invoice.update',$invoice->id],
                              'class' => 'form-horizontal',
                              'files' => true,
                              'id' => 'job_form'
                          ]) !!}

        @include ('profile.invoice.form')
        {!! Form::close() !!}

    </section>


@endsection


@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/7.3.2/jquery.mmenu.all.js"></script>

    <script type="text/javascript" src="/js/moment.min.js" defer></script>
    <script type="text/javascript" src="/js/daterangepicker.min.js" defer></script>
    <script src="/js/jquery.maskedinput.js" type="text/javascript"></script>

    <script src="/js/Select2-4.0.7.js" type="text/javascript"></script>
    <script src="/js/comboTreePlugin.js"></script>
    <script src="/js/icontains.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
        function individual(isIndividual) {
            if (isIndividual) {
                $('.not-individual').each(function () {
                    $(this).css('display', 'none');
                    $(this).find('.changeable').removeAttr('required');
                })
                $('.individual').each(function () {
                    $(this).find('input').removeAttr('required');
                    $(this).css('display', 'block');
                })
            }
            else {

                $('.not-individual').each(function () {
                    $(this).find('.changeable').attr('required');
                    $(this).css('display', 'block');
                })

                $('.individual').each(function () {
                    $(this).find('.changeable').val('');
                    $(this).find('input').removeAttr('required');
                    $(this).css('display', 'none');
                })
            }


        }
        $(document).ready(function () {
            //select user , get user bank details
            $('#user_id').on('change', function () {
                var userId = $(this).val();
                $.ajax({
                    type: "POST",
                    data: {
                        'id': userId
                    },
                    url: '/get/bankDetails',
                    success: function (result) {
                        var result = result['result'];
                        if (result['individual'] == 1) {
                            $('#individual').attr('checked', true);
                            individual(true);
                            $('#client_address').val(result['address']);
                            $('#passport').val(result['passport']);

                        }
                        else {
                            console.log("sdf");
                            $('#individual').attr('checked', false);
                            individual(false);
                            $('#client').val(result['company']);
                            $('#client_address').val(result['physical_address']);
                            $('#client_bank').val(result['bank']);
                            $('#client_tax').val(result['tax']);
                            $('#client_account').val(result['account']);
                            $('#director').val(result['signing_person_1']);
                            $('#position').val(result['position_1']);
                            $('#client_address').addClass('form-file');
                            $('#client_bank').addClass('form-file');
                            $('#client_tax').addClass('form-file');
                            $('#client_account').addClass('form-file');
                            $('#client').addClass('form-file');

                        }


                    }
                });

            })

            //datepicker on date field
            $(function () {
                $('#date').daterangepicker({
                    singleDatePicker: true,
                    drops: 'up',
                    "locale": {
                        format: 'DD MMM YYYY'
                    }
                });
            });


            //add new row to table
            var counter = parseInt($('#counter').val());
            var total = parseInt($('#total').val());
            var options = "<option value>Choose service</option>";
            @foreach($services as $item)
                options += '<option value="{{$item->id}}">' + "{{$item->data->name}}" + '</option>'
            @endforeach

              $('#add').on('click', function () {
                counter++;
                if (counter <= 5) {
                    var newRow = $("<tr>");
                    var cols = "";
                    cols += '<td><select class="form-control services" name="service_id[]">' +
                        options
                        + '</select></td>';
                    cols += '<td class="text-center" data-label="{{trans('invoice.quantity')}}">{!! Form::selectYear('quantity[]', 1, 20, null,['class'=>"form-control  quantity"]) !!}</td>';
                    cols += '<td class="text-center" data-label="{{trans('invoice.measure')}}"> <input type="text" name="measure[]" class="form-control" required></td>';
                    cols += '<td class="text-center" data-label="{{trans('invoice.amount')}}"> <input readonly min="1" type="number" name="amount[]" class="form-control amount" required></td>';
                    cols += '<td class="text-center"  data-label="{{trans('invoice.sum')}}"> <input readonly min="1" type="number" name="sum[]" style="width: calc(100% - 35px);" class="form-control d-inline-block sum" required><button class="delete bg-transparent border-0 pr-0" type="button"><img src="/img/delete.svg" class="loading" data-was-processed="true"></button></td>';
                    newRow.append(cols);
                    $("#invoice-items").append(newRow);
                    $('#counter').val(counter);
                }

            })

            //remove row
            $(document).on('click', '.delete', function () {
                counter--;
                $('#counter').val(counter);
                var sum = $(this).closest('tr').find('.sum').val();
                total -= sum;


                if (sum != '') {
                    $('#total').val(total);
                }
                $(this).closest("tr").remove();


            })


            $("#invoice-items").on("change", ".quantity, .amount", function (event) {

                var quantity = $(this).closest('tr').find('.quantity').val();
                var amount = $(this).closest('tr').find('.amount').val();
                var sum = $(this).closest('tr').find('.sum').val();

                if (sum != '') {
                    total -= sum;
                }
                if (amount != '' && quantity != '') {
                    var newAmount = quantity * amount;
                    $(this).closest('tr').find('.sum').val(newAmount);
                    total += newAmount;
                    $('#total').val(total);
                }


            })

            $('#individual').on('change', function () {
                var val = $(this).prop('checked');
                individual(val);
            })


            //get service amount
            $(document).on('change', '.services', function () {
                var amount = $(this).closest('tr').find('.amount');
                var quantity = $(this).closest('tr').find('.quantity').val();
                var sum = $(this).closest('tr').find('.sum').val();
                var sumObj = $(this).closest('tr').find('.sum');
                var serviceId = $(this).val();
                var currency = $('#currency').val();

                $.ajax({
                    type: "POST",
                    data: {
                        'id': serviceId
                    },
                    url: '/get/service-amount',
                    success: function (result) {
                        var result = result['result'];
                        var amountCurrency = result[currency];


                        amount.val(amountCurrency);
                        console.log(amountCurrency);
                        if (sum != '') {
                            total -= sum;
                        }
                        if (amount != '' && quantity != '') {
                            var newAmount = quantity * amountCurrency;
                            sumObj.val(newAmount);
                            total += newAmount;
                            $('#total').val(total);
                        }

                    }
                });


            })


        });
    </script>
@endsection





