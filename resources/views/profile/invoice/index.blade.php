@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }

        table.mobile-table th .form-group select {
            height: inherit;
            font-size: 14px;
            padding: 0 0 0 20px;
            background-color: transparent;
            color: #fff;
            background-image: linear-gradient(45deg, transparent 50%, #fff 0), linear-gradient(135deg, #fff 50%, transparent 0);
            background-position: 0 7px, 7px 7px, 13px 6.5em;
            border: none;
        }

        table.mobile-table th .form-group select option {
            color: #000;
        }
    </style>
@endsection

@section('content')
    <section class="position-relative bg-grey">
        @include('profile.menu')

        <div class="profile-container float-right">

            <div class="profile-table">
                <div class="d-flex justify-content-between">
                    @permission('create_invoice')
                    <a href="{{route('createInvoiceStep1')}}" class="btn btn-orange-gradient mr-2">Create</a>
                    @endpermission
                    {{--<form id="myForm1" action="{{route('invoice.index')}}"--}}
                    {{--class="profile-search-form d-flex position-relative ">--}}
                    {{--<input value="{{$search}}" id="search" name="search" class="form-control" type="search"--}}
                    {{--placeholder="{{trans('profile_tables.search')}}…" aria-label="Search">--}}
                    {{--<button id="search_btn" class="btn" type="button"><img src="/img/search.svg" alt=""></button>--}}
                    {{--</form>--}}
                </div>

                <div class="table-responsive">
                    <table class="table mobile-table">
                        <thead>
                        <tr class="table-tr-th">
                            <th>{{trans('profile_tables.userName')}}</th>
                            <th>{{trans('profile_tables.date')}}</th>
                            <th>{{trans('profile_tables.amount')}}</th>
                            <th>{{trans('profile_tables.currency')}}</th>
                            <th>
                                <div class="form-group mb-0" style="width: 150px">
                                    <form id="myFormStatus" action="{{route('invoice.index')}}">
                                        <select name="status" id="invoice-status">
                                            <option value="">Choose status...</option>
                                            <option @if($status=='pending') selected
                                                    @endif  value="pending">{{trans('profile_tables.pending')}}</option>
                                            <option @if($status=='posted') selected
                                                    @endif  value="posted">{{trans('profile_tables.posted')}}</option>
                                            <option @if($status=='draft') selected
                                                    @endif  value="draft">{{trans('profile_tables.draft')}}</option>
                                        </select>
                                        {{--<label for="user-status" class="control-label">{{trans('profile_tables.status')}}</label>--}}
                                    </form>
                                </div>
                            </th>
                            <th class="text-center">{{trans('profile_tables.action')}}</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($invoices as $item)

                            <tr class="col-sd-3">
                                <td data-label="{{trans('profile_tables.name')}}">@if(isset($item->user)){{$item->user->username}}@endif</td>
                                <td data-label="{{trans('profile_tables.date')}}">{{ \Carbon\Carbon::parse($item->date)->format('d M Y')}}</td>
                                <td data-label="{{trans('profile_tables.amount')}}">{{$item->money}}</td>
                                <td data-label="{{trans('profile_tables.currency')}}">{{$item->currency}}</td>
                                <td data-label="{{trans('users.status')}}" style="width: 115px;">
                                    @if(\Illuminate\Support\Facades\Auth::user()->can('all_invoice'))
                                        @if($item->status != 'posted' && $item->status!='draft')
                                            {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['invoice-status-change', $item->id]]) !!}
                                            <select class="form-control status" name="status">
                                                <option @if($item->status =='pending') selected
                                                        @endif  value="pending">{{trans('profile_tables.pending')}}</option>
                                                <option @if($item->status =='posted') selected
                                                        @endif  value="posted">{{trans('profile_tables.posted')}}</option>
                                            </select>
                                            {!! Form::close() !!}
                                        @else
                                            {{trans('profile_tables.'.$item->status)}}
                                        @endif
                                    @else
                                        {{trans('profile_tables.'.$item->status)}}
                                    @endif

                                </td>

                                <td data-label="{{trans('profile_tables.action')}}">
                                    <div class="table-buttons d-flex justify-content-center">
                                        <a href="{{route('invoice.show',$item->id)}}"
                                           title="{{trans('profile_tables.view')}}"><img src="/img/visibility.svg"
                                                                                         alt=""></a>
                                        <a href="{{route('invoicePDF',$item->id)}}" class="pdf-down"
                                           title="{{trans('profile_tables.download')}}"><img src="/img/download.svg"
                                                                                             alt=""></a>
                                        @permission('edit_invoice')
                                        <a href="{{route('invoice.edit',$item->id)}}"
                                           title="{{trans('profile_tables.edit')}}"><img src="/img/edit.svg" alt=""></a>
                                        @endpermission
                                        @permission('delete_invoice')
                                        {!! Form::open([
                                        'method'=>'DELETE',
                                        'route' => ['invoice.destroy',$item->id],
                                        'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<img src="/img/delete.svg">', array(
                                                'type' => 'submit',
                                                'class' => '',
                                                'title' => trans('invoice.delete'),
                                                'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        {!! Form::close() !!}
                                        @endpermission

                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

                <div class="d-flex justify-content-end my-pagination align-items-center">
                    <form id="myForm" action="{{route('invoice.index')}}">
                        <div>
                            <label for="rr">{{trans('jobs.item_per_page')}}:</label>

                            <select name="order_by" id="rr">
                                <option @if($order==20) selected @endif value="20">20</option>
                                <option @if($order==50) selected @endif value="50">50</option>
                                <option @if($order==100) selected @endif value="100">100</option>
                            </select>

                        </div>
                    </form>

                    <div class="pagination-links d-flex">
                        {{ $invoices->appends(['search' => Request::get('search'),'order_by' => Request::get('order_by')])->links('vendor.pagination.bootstrap-4')}}
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
@section('scripts')
    <script>
        $('#invoice-status').on('change', function () {
            $("#myFormStatus").submit();
        })
        $('.status').on('change', function () {
            $(this).closest('form').submit();
        });
    </script>
@endsection
