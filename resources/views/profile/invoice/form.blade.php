<style>
    table td {
        word-break: keep-all;
    }

    .text-bold {
        font-weight: 600;
        padding-right: 5px;
    }

    .table-td-right {
        width: 156px;
        max-width: 100%;
        display: inline-block;
        text-align: right;
    }

    .invoice-table td > div {
        padding: 0 0 10px;
    }

    input {
        outline: none;
        box-shadow: none;
        border: 1px solid #ced4da;

    }

    input.date {
        height: calc(1.5em + .75rem + 2px);
        padding: .375rem .75rem;
        border-radius: .25rem;
    }

    .add-invoice {
        border: 1px solid #dee2e6;
        border-top: 0;
        border-bottom: 0;
        padding: 0 12px;
    }

    input.with-border-bottom {
        border: 0;
        border-bottom: 1px solid #ced4da;
    }

    input.with-border-bottom:focus {
        border-color: #0375ba;
        box-shadow: none;
    }

    input.text-center.border-0 {
        width: 100%;
    }


</style>
<div class="profile-container float-right">
    <div class="tab-content profile-tab-content">
        <div class="tab-pane fade show active" id="job" role="tabpanel" aria-labelledby="job-tab">
            <div class="row">


                <div class="d-flex align-items-center">
                    {{--chgitem inch e sa--}}


                    {{--<div class="form-group mr-3">--}}
                    {{--<select class="form-file">--}}
                    {{--<option>Poxancumov</option>--}}
                    {{--<option>Mobile</option>--}}
                    {{--<option>landline</option>--}}
                    {{--</select>--}}
                    {{--<label for="phone_type_0" class="control-label">Label</label>--}}

                    {{--</div>--}}
                    <div class="checkbox mb-3">
                        <label class="d-inline-flex">
                            anhati tvyalner <input
                                    @if(old('individual') != null || (isset($invoice) && $invoice->individual==1)) checked
                                    @endif
                                    id="individual" type="checkbox" name="individual"><i
                                    class="helper"></i>
                        </label>
                    </div>
                    <input name="currency" type="hidden" id="currency"
                           value="{{isset($invoice)? $invoice->currency : 'amd'}}">
                    {{--<div class="form-group ml-3">--}}
                    {{--<select class="form-control" name="currency" id="currency">--}}
                    {{--@foreach(config('bank-details.currency') as $cur)--}}
                    {{--<option @if((old('currency') == $cur) || (isset($invoice) && $invoice->currency == $cur) ) selected--}}
                    {{--@endif  value="{{$cur}}">{{trans('invoice.'.$cur)}}</option>--}}
                    {{--@endforeach--}}
                    {{--</select>--}}
                    {{--<label for="phone_type_0" class="control-label">Currency</label>--}}
                    {{--</div>--}}

                </div>


                <div class="table-responsive">
                    <table class="table table-bordered invoice-table mb-0">
                        <tr>
                            <td class="w-50">
                                <div>
                                    <span class="text-bold table-td-right">{{trans('invoice.performer')}}</span>
                                    {{ Form::text("performer",isset($invoice)? $invoice->performer: config('bank-details.performer'),array('required'=>'required','id'=>'performer', 'class' => 'border-0'))}}
                                    {!! $errors->first("performer", '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>
                                <div>
                                    <span class="text-bold table-td-right">{{trans('invoice.address')}}</span>
                                    {{ Form::text("performer_address",isset($invoice)? $invoice->performer_address: config('bank-details.performer_address'),array('required'=>'required','id'=>'performer_address', 'class' => 'border-0'))}}
                                    {!! $errors->first("performer_address", '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>
                                <div class="d-flex align-items-center">
                                    <div class="d-flex align-items-center">
                                        <span class="text-bold table-td-right">{{trans('invoice.account')}}</span>
                                        {{ Form::text("performer_account", isset($invoice)? $invoice->performer_account: config('bank-details.performer_account'),array('required'=>'required','id'=>'performer_account','class' => 'ml-1 border-0'))}}
                                        {!! $errors->first("performer_account", '<p style="color:red" class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <span class="text-bold pl-2">{{trans('invoice.tax')}}</span>
                                        {{ Form::text("performer_tax", isset($invoice)? $invoice->performer_tax: config('bank-details.performer_tax'),array('required'=>'required','id'=>'performer_tax', 'class' => 'border-0'))}}
                                        {!! $errors->first("performer_tax", '<p style="color:red" class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div>
                                    <span class="text-bold table-td-right">{{trans('invoice.bank')}}</span>
                                    {{ Form::text("performer_bank", isset($invoice)? $invoice->performer_bank: config('bank-details.performer_bank'),array('required'=>'required','id'=>'performer_bank', 'class' => 'border-0'))}}
                                    {!! $errors->first("performer_bank", '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>
                            </td>
                            <td class="w-50">
                                <div><span style="min-width: 225px;display: inline-block">{{trans('invoice.booking_number')}}
                                        N</span>
                                    {{ Form::text("booking_number",null,array('required'=>'required','id'=>'booking_number','class'=>'check with-border-bottom'))}}
                                    {!! $errors->first("booking_number", '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>
                                <span class="text-bold" style="min-width: 225px;display: inline-block">{{trans('invoice.account_product')}}
                                    N</span>
                                {{ Form::text("account_product",null,array('required'=>'required','id'=>'account_product','class'=>'check with-border-bottom'))}}
                                {!! $errors->first("account_product", '<p style="color:red" class="help-block">:message</p>') !!}
                                <br>
                                {{ Form::text("date",isset($invoice)? Carbon\Carbon::parse($invoice->date)->format('d F Y'): null,array('required'=>'required','id'=>'date','class'=>'check mt-4 date'))}}
                                {!! $errors->first("date", '<p style="color:red" class="help-block">:message</p>') !!}
                            </td>
                        </tr>
                        <tr>
                            <td class="w-50">
                                @permission('all_invoice')
                                <div class="d-flex">
                                    <span class="text-bold table-td-right">{{trans('announcements.select_organization')}}</span>
                                    <select class="form-control ml-1" name="user_id" id="user_id" style="width: 209px"
                                            required>
                                        <option value=""></option>
                                        @foreach($users as $item)
                                            <option @if((old('user_id') != null && old('user_id') == $item->id) || (isset($invoice) && $invoice->user_id == $item->id)) selected
                                                    @endif
                                                    value="{{$item->id}}"
                                                    data-name="{{$item->organization->name}}">{{$item->organization->name}}
                                                ( {{$item->username}})
                                            </option>
                                        @endforeach

                                    </select>
                                </div>
                                @endpermission
                                <div>
                                    <span class="text-bold table-td-right">{{trans('invoice.client')}}</span>
                                    {{ Form::text("client",null,array('required'=>'required','id'=>'client','class' => 'with-border-bottom'))}}
                                    {!! $errors->first("client", '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>
                                <div>
                                    <span class="text-bold table-td-right">{{trans('invoice.address')}}</span>
                                    {{ Form::text("client_address",null,array('required'=>'required','id'=>'client_address','class' => 'with-border-bottom'))}}
                                    {!! $errors->first("client_address", '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>
                                <div class="individual"
                                     @if(old('individual') != null || (isset($invoice) && $invoice->individual == 1)) style="display: block;"
                                     @else style="display: none;" @endif>
                                    <span class="text-bold table-td-right">{{trans('invoice.passport')}}</span>

                                    <input id="passport"
                                           @if(old('individual') != null || (isset($invoice) && $invoice->individual == 1)) required
                                           @endif
                                           type="text"
                                           class="with-border-bottom @error('passport') is-invalid @enderror changeable"
                                           name="passport"
                                           value="{{old('passport') ? old('passport') : (isset($invoice) ? $invoice->passport : null ) }}">

                                    {!! $errors->first("passport", '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>
                                {{--<div class="d-flex align-items-center" id="account">--}}
                                <div class="not-individual"
                                     @if(old('individual')!=null  || (isset($invoice) && $invoice->individual == 1)) style="display: none;" @endif>
                                    <span class="text-bold table-td-right">{{trans('invoice.account')}}</span>
                                    <input id="client_account"
                                           @if(old('individual') != null || (isset($invoice) && $invoice->individual == 1)) @else required
                                           @endif
                                           type="text"
                                           class="with-border-bottom @error('client_account') is-invalid @enderror changeable"
                                           name="client_account"
                                           value="{{old('client_account') ? old('client_account') : (isset($invoice) ? $invoice->client_account : null ) }}">

                                    {!! $errors->first("client_account", '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>
                                <div class="not-individual"
                                     @if(old('individual')!=null  || (isset($invoice) && $invoice->individual == 1)) style="display: none;" @endif>
                                    <span class="text-bold table-td-right">{{trans('invoice.tax')}}</span>
                                    <input id="client_tax"
                                           @if(old('individual') != null || (isset($invoice) && $invoice->individual == 1)) @else required
                                           @endif
                                           type="text"
                                           class="with-border-bottom @error('client_tax') is-invalid @enderror changeable"
                                           name="client_tax"
                                           value="{{old('client_tax') ? old('client_tax') : (isset($invoice) ? $invoice->client_tax : null ) }}">

                                    {!! $errors->first("client_tax", '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>
                                {{--</div>--}}
                                <div class="not-individual"
                                     @if(old('individual')!=null  || (isset($invoice) && $invoice->individual == 1)) style="display: none;" @endif>
                                    <span class="text-bold table-td-right">{{trans('invoice.bank')}}</span>

                                    <input id="client_bank"
                                           @if(old('individual') != null || (isset($invoice) && $invoice->individual == 1)) @else required
                                           @endif
                                           type="text"
                                           class="with-border-bottom @error('client_bank') is-invalid @enderror changeable"
                                           name="client_bank"
                                           value="{{old('client_bank') ? old('client_bank') : (isset($invoice) ? $invoice->client_bank : null ) }}">
                                    {!! $errors->first("client_bank", '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>


                            </td>
                            <td class="w-50">
                                <div>{{trans('invoice.posted_announcements')}}</div>
                                {{ Form::textarea("posted_announcements",null,['class'=>'w-100 form-control','id'=>'posted_announcements','rows'=>15])}}
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <span class="text-bold">{{trans('invoice.notes')}}</span>
                                {{ Form::textarea("notes",null,['class'=>'w-100 form-control','id'=>'notes','rows'=>15])}}
                                {!! $errors->first("notes", '<p style="color:red" class="help-block">:message</p>') !!}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span class="text-bold">Lracum</span> poxancumov AAh-ov vcharvox fdsgdg dfgjhdfjkg
                                gdfjghfjdghg
                            </td>
                        </tr>

                    </table>

                </div>
                <div class="table-responsive">
                    @if(isset($invoice))
                        <input name="counter" type="hidden" id="counter"
                               value="@if(old('counter')!= null) {{old('counter')}}  @else{{count($invoice->invoiceItems)}} @endif">

                        <table class="table table-bordered invoice-table mb-0" id="invoice-items">
                            <thead>
                            <tr>
                                {{--<td class="text-bold text-center" style="width:80px;">hamaar</td>--}}
                                <td class="text-bold text-center">{{trans('invoice.name')}}</td>
                                <td class="text-bold text-center"
                                    style="width: 80px;">{{trans('invoice.quantity')}}</td>
                                <th>{{trans('invoice.measure')}}</th>
                                <th>{{trans('invoice.amount')}}
                                    ({{$invoice->currency}})
                                </th>
                                <th>{{trans('invoice.sum')}}
                                    ({{$invoice->currency}})
                                </th>

                            </tr>
                            </thead>
                            @if(old('counter')== null)
                                <tbody>
                                @for($i=0;$i<count($invoice->invoiceItems);$i++)
                                    <tr class="col-sd-3">
                                        <td class="text-center">
                                            <select required name="service_id[]" class="form-control services">
                                                <option value="">Choose services</option>
                                                @foreach($services as $item)
                                                    <option @if($invoice->invoiceItems[$i]->service_id == $item->id) selected
                                                            @endif value="{{$item->id}}">{{$item->data->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('service_id.0')
                                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                            @enderror

                                        </td>


                                        <td class="text-center dsh">
                                            {!! Form::selectYear('quantity[]', 1, 20,$invoice->invoiceItems[$i]->quantity,['class'=>"form-control  quantity"])!!}
                                            @error('quantity.'.$i)
                                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                            @enderror

                                        </td>


                                        <td class="text-center">
                                            <input type="text" name="measure[]" class="form-control"
                                                   required
                                                   value="{{$invoice->invoiceItems[$i]->measure}}">
                                            @error('measure.'.$i)
                                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                            @enderror

                                        </td>

                                        <td class="text-center">
                                            <input readonly min="1" type="number" name="amount[]"
                                                   class="form-control amount"
                                                   required
                                                   value="{{$invoice->invoiceItems[$i]->amount}}">
                                            @error('amount.'.$i)
                                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                            @enderror

                                        </td>

                                        <td class="text-center">
                                            <input readonly min="1" type="number" name="sum[]"
                                                   style="width: calc(100% - 35px);"
                                                   class="form-control sum    @if($i!=0) d-inline-block @endif"
                                                   required
                                                   value="{{$invoice->invoiceItems[$i]->sum}}">
                                            @if($i!=0)
                                                <button class="bg-transparent border-0 pr-0 delete"><img
                                                            src="/img/delete.svg" class="loading"
                                                            data-was-processed="true"></button>
                                            @endif
                                            @error('sum.'.$i)
                                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                            @enderror

                                        </td>

                                    </tr>
                                @endfor


                                </tbody>
                            @else
                                <tbody>
                                <tr class="col-sd-3">

                                    <td class="text-center">

                                        <select required name="service_id[]" class="form-control services">
                                            <option value="">Choose services</option>
                                            @foreach($services as $item)
                                                <option @if(old('service_id')[0] == $item->id) selected
                                                        @endif value="{{$item->id}}">{{$item->data->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('service_id.0')
                                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                        @enderror

                                    </td>

                                    <td class="text-center">
                                        {!! Form::selectYear('quantity[]', 1, 20, old('quantity')[0],['class'=>"form-control  quantity"]) !!}
                                        @error('quantity.0')
                                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                        @enderror

                                    </td>
                                    <td class="text-center">
                                        <input type="text" name="measure[]" class="form-control measure" required
                                               value="{{old('measure')[0]}}">
                                        @error('measure.0')
                                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                        @enderror
                                    </td>
                                    <td class="text-center">
                                        <input readonly min="1" type="number" name="amount[]"
                                               class="form-control amount"
                                               required
                                               value="{{old('amount')[0]}}">
                                        @error('amount.0')
                                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                        @enderror
                                    </td>
                                    <td class="text-center">
                                        <input readonly min="1" type="number" name="sum[]" class="form-control sum"
                                               required
                                               value="{{old('sum')[0]}}">
                                        @error('sum.0')
                                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                        @enderror
                                    </td>
                                </tr>


                                @for($i=1;$i<old('counter');$i++)
                                    <tr>
                                        <td class="text-center">
                                            <select required name="service_id[]" class="form-control services">
                                                <option value="">Choose services</option>
                                                @foreach($services as $item)
                                                    <option @if(old('service_id')[$i] == $item->id) selected
                                                            @endif value="{{$item->id}}">{{$item->data->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('name.'.$i)
                                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                            @enderror

                                        </td>
                                        <td class="text-center">
                                            {!! Form::selectYear('quantity[]', 1, 20, old('quantity')[$i],['class'=>"form-control  quantity"]) !!}
                                            @error('quantity.'.$i)
                                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                            @enderror

                                        </td>

                                        <td class="text-center">
                                            <input type="text" name="measure[]" class="form-control measure" required
                                                   value="{{old('measure')[$i]}}">
                                            @error('measure.'.$i)
                                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                            @enderror
                                        </td>
                                        <td class="text-center">
                                            <input readonly min="1" type="number" name="amount[]" class="form-control amount"
                                                   required
                                                   value="{{old('amount')[$i]}}">
                                            @error('amount.'.$i)
                                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                            @enderror
                                        </td>
                                        <td class="text-center">
                                            <input style="width: calc(100% - 35px);" readonly min="1" type="number"
                                                   name="sum[]" class="form-control sum d-inline-block"
                                                   required
                                                   value="{{old('sum')[$i]}}">
                                            <button class="bg-transparent border-0 pr-0 delete"><img
                                                        src="/img/delete.svg"
                                                        class="loading"
                                                        data-was-processed="true">
                                            </button>
                                            @error('sum.'.$i)
                                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                            @enderror

                                        </td>
                                    </tr>

                                @endfor


                                </tbody>
                            @endif

                        </table>

                    @else
                        <input name="counter" type="hidden" id="counter"
                               value="@if(old('counter')!= null) {{old('counter')}}  @else 1 @endif">
                        <table class="table table-bordered invoice-table mb-0" id="invoice-items">
                            <tr>
                                <td class="text-bold text-center">{{trans('invoice.name')}}</td>
                                <td class="text-bold text-center"
                                    style="width: 80px;">{{trans('invoice.quantity')}}</td>
                                <th>{{trans('invoice.measure')}}</th>
                                <th>
                                    {{trans('invoice.amount')}}
                                    @if(old('currency') != null)
                                        ({{old('currency')}})
                                    @else
                                (AMD)
                                    @endif

                                </th>
                                <th>
                                    {{trans('invoice.sum')}}
                                    @if(old('currency') != null)
                                        ({{old('currency')}})
                                    @else
                                (AMD)
                                    @endif
                                </th>

                            </tr>
                            <tr>

                                <td class="text-center">
                                    <select required name="service_id[]" class="form-control services">
                                        <option value="">Choose services</option>
                                        @foreach($services as $item)
                                            <option @if(old('service_id')[0] == $item->id) selected
                                                    @endif value="{{$item->id}}">{{$item->data->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('service_id.0')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                    @enderror

                                </td>

                                <td class="text-center">
                                    {!! Form::selectYear('quantity[]', 1, 20, old('quantity')[0],['class'=>"form-control  quantity"]) !!}
                                    @error('quantity.0')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                    @enderror

                                </td>


                                <td class="text-center">
                                    <input type="text" name="measure[]" class="form-control measure" required
                                           value="{{old('measure')[0]}}">
                                    @error('measure.0')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                    @enderror

                                </td>
                                <td class="text-center">
                                    <input readonly min="1" type="number" name="amount[]" class="form-control amount"
                                           required
                                           value="{{old('amount')[0]}}">
                                    @error('amount.0')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                    @enderror
                                </td>
                                <td class="text-center">
                                    <input readonly min="1" type="number" name="sum[]" class="form-control sum" required
                                           value="{{old('sum')[0]}}">
                                    @error('sum.0')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                    @enderror
                                </td>

                            </tr>
                            @for($i=1;$i<old('counter');$i++)
                                <tr>
                                    {{--<td style="width:80px;"><input class="text-center border-0" type="text" value="1"--}}
                                    {{--style="width:80px;"></td>--}}
                                    <td class="text-center">

                                        <select required name="service_id[]" class="form-control services">
                                            <option value="">Choose services</option>
                                            @foreach($services as $item)
                                                <option @if(old('service_id')[$i] == $item->id) selected
                                                        @endif value="{{$item->id}}">{{$item->data->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('service_id.'.$i)
                                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                        @enderror

                                    </td>

                                    <td class="text-center">
                                        {!! Form::selectYear('quantity[]', 1, 20, old('quantity')[$i],['class'=>"form-control  quantity"]) !!}
                                        @error('quantity.'.$i)
                                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                        @enderror

                                    </td>


                                    <td class="text-center">
                                        <input type="text" name="measure[]" class="form-control"
                                               required
                                               value="{{old('measure')[$i]}}">
                                        @error('measure.'.$i)
                                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                        @enderror

                                    </td>


                                    <td class="text-center">
                                        <input readonly min="1" type="number" name="amount[]"
                                               class="form-control amount"
                                               required
                                               value="{{old('amount')[$i]}}">
                                        @error('amount.'.$i)
                                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                        @enderror
                                    </td>
                                    <td class="text-center">
                                        <input style="width: calc(100% - 35px);" readonly min="1" type="number"
                                               name="sum[]" class="form-control sum d-inline-block" required
                                               value="{{old('sum')[$i]}}">
                                        <button class="bg-transparent border-0 pr-0 delete"><img src="/img/delete.svg"
                                                                                                 class="loading"
                                                                                                 data-was-processed="true">
                                        </button>
                                        @error('sum.'.$i)
                                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                        @enderror

                                    </td>

                                </tr>
                            @endfor

                        </table>
                    @endif

                </div>

                <div class="text-right w-100 pt-2 pb-2 add-invoice">
                    <button type="button" class="btn btn-blue-gradient"
                            id="add">{{trans('invoice.add')}}</button>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered invoice-table  mb-5" style="table-layout: fixed">
                        <tr>
                            <td>{{trans('invoice.total')}}
                                @if(isset($invoice))
                                    ({{$invoice->currency}})
                                @else
                                (AMD)
                                @endif
                            </td>
                            @if(isset($invoice))
                                <td class="text-center">
                                    <input name="total" id="total" readonly class="text-center border-0 form-control"
                                           type="text"
                                           value="@if(old('total')!= null) {{old('total')}}@else {{$invoice->money}}  @endif">
                                </td>
                            @else
                                <td class="text-center">
                                    <input id="total" readonly name="total" class="text-center border-0 form-control"
                                           type="text"
                                           value="@if(old('total')!= null) {{old('total')}}@else 0 @endif">
                                </td>
                            @endif

                        </tr>
                        <tr>
                            <td colspan="2">tivy barerov</td>
                        </tr>
                    </table>
                </div>

                <div class="d-flex justify-content-between w-100 flex-wrap">
                    <div class="d-flex flex-column">
                        <span class="text-bold mb-3 d-block">"profexx spy"</span><br>
                        <div>
                            <span>Tnoren`</span><br>
                            <span>A Eremyan</span>
                            <input type="text" style="width: 120px;border:0;border-bottom: 1px solid darkgrey;">
                        </div>

                        <div class="pt-5 mt-auto pl-2">k.t.</div>
                    </div>
                    <div class="d-flex flex-column pr-5">
                        <span class="text-bold mb-3 d-block">Career</span><br>


                        {{--<div class="mb-3 not-individual d-flex align-items-end flex-wrap"--}}
                        {{--@if(old('individual')!=null  || (isset($invoice) && $invoice->individual == 1)) style="display: none;" @endif>--}}
                        {{--<div class="mr-2 mb-3">--}}
                        {{--<span>{{trans('invoice.director')}}</span>--}}
                        {{--<div class=" not-individual"--}}
                        {{--@if(old('individual')!=null  || (isset($invoice) && $invoice->individual == 1)) style="display: none;" @endif>--}}
                        {{--<select @if(old('individual') != null || (isset($invoice) && $invoice->individual == 1)) @else required--}}
                        {{--@endif name="position" id="position"--}}
                        {{--class="@error('position') is-invalid @enderror form-control changeable"--}}
                        {{--style="width: 210px">--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        {{--<input id="director"--}}
                        {{--@if(old('individual') != null || (isset($invoice) && $invoice->individual == 1)) @else required--}}
                        {{--@endif--}}
                        {{--type="text"--}}
                        {{--class="mb-3 with-border-bottom @error('director') is-invalid @enderror changeable"--}}
                        {{--name="director"--}}
                        {{--value="{{old('director') ? old('director') : (isset($invoice) ? $invoice->director : null ) }}">--}}

                        {{--{!! $errors->first("director", '<p style="color:red" class="help-block">:message</p>') !!}--}}
                        {{--</div>--}}

                        {{--<div id="director_div_2"  class="mb-3 not-individual d-flex align-items-end flex-wrap"--}}
                        {{--@if(old('individual')!=null  || (isset($invoice) && $invoice->individual == 1)) style="display: none;" @endif>--}}
                        {{--<div class="mr-2 mb-3">--}}
                        {{--<span>{{trans('invoice.director')}}</span>--}}
                        {{--<div class=" not-individual"--}}
                        {{--@if(old('individual')!=null  || (isset($invoice) && $invoice->individual == 1)) style="display: none;" @endif>--}}
                        {{--<select  name="position_2"--}}
                        {{--class="@error('position_2') is-invalid @enderror form-control changeable"--}}
                        {{--style="width: 210px">--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        {{--<input type="text"--}}
                        {{--class="mb-3 with-border-bottom @error('director_2') is-invalid @enderror changeable"--}}
                        {{--name="director_2"--}}
                        {{--value="{{old('director_2') ? old('director_2') : (isset($invoice) ? $invoice->director : null ) }}">--}}

                        {{--{!! $errors->first("director_2", '<p style="color:red" class="help-block">:message</p>') !!}--}}
                        {{--</div>--}}


                        <div class="pt-5 mt-auto pl-2">k.t.</div>
                    </div>
                </div>


            </div>
            <div class="d-flex justify-content-end mt-5">
                <div class="text-center">
                    <button type="submit" class="btn btn-orange-gradient save">{{trans('pages.save')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>

