@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')
    <section class="position-relative bg-grey">
        @include('profile.menu')

        <div class="profile-container float-right">

            <div class="profile-table">
                <div class="d-flex justify-content-between">
                    <form id="myForm1" action="{{route('subscribtion.index')}}"
                          class="profile-search-form d-flex position-relative ml-auto">
                        <input value="{{$search}}" id="search" name="search" class="form-control" type="search"
                               placeholder="{{trans('profile_tables.search')}}…" aria-label="Search">
                        <button id="search_btn" class="btn" type="button"><img src="/img/search.svg" alt=""></button>
                    </form>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr class="table-tr-th">
                            <th>{{trans('profile_tables.email')}}</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($subscribtions as $item)
                                <tr class="col-sd-3">
                                    <td>@if(isset($item)){{$item->email}}@endif</td>
                                </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

                <div class="d-flex justify-content-end my-pagination align-items-center">
                    <form id="myForm" action="{{route('subscribtion.index')}}">
                        <div>
                            <label for="rr">{{trans('jobs.item_per_page')}}:</label>

                            <select name="order_by" id="rr">
                                <option @if($order==20) selected @endif value="20">20</option>
                                <option @if($order==50) selected @endif value="50">50</option>
                                <option @if($order==100) selected @endif value="100">100</option>
                            </select>

                        </div>
                    </form>
                    {{ $subscribtions->appends(['search' => Request::get('search'),'order_by' => Request::get('order_by')])->links('vendor.pagination.bootstrap-4')}}
                    <div class="pagination-links d-flex">

                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
@section('scripts')
    <script>
        $('#rr').on('change',function()
        {
            var search = $('#search').val();
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "search").val(search).appendTo('#myForm');
            $( "#myForm" ).submit();

        });
        $('#search_btn').on('click',function()
        {
            var order = $('#rr').val();
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "order_by").val(order).appendTo('#myForm1');
            $( "#myForm1" ).submit();
        });
    </script>
@endsection
