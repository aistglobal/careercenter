<ul class="nav nav-pills mb-3 profile-tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="job-tab" data-toggle="pill" href="#job" role="tab"
           aria-controls="job" aria-selected="true">{{trans('announcements.job')}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="company-tab" data-toggle="pill" href="#company" role="tab"
           aria-controls="company" aria-selected="false">{{trans('announcements.company')}}</a>
    </li>
</ul>
<div class="tab-content profile-tab-content pt-1">

    {{--tab 1--}}
    <div class="tab-pane fade show active" id="job" role="tabpanel" aria-labelledby="job-tab">
        <h4 class="mb-4">{{trans('types.'.$type->key)}}</h4>
        <div class="row">
            <div class="col-md-6">
                {{--@if(!isset($job))--}}
                    {{--<div class="form-group">--}}
                        {{--<select class="form-control" name="product_id" id="product_id">--}}
                            {{--@foreach($userProducts as $item)--}}
                                {{--<option  value="{{$item->id}}">{{$item->service->data->name}}--}}
                                {{--</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        {{--<label for="product_id"--}}
                               {{--class="control-label">{{trans('announcements.select_service')}}--}}
                            {{--<span class="star">*</span>--}}

                        {{--</label>--}}
                    {{--</div>--}}
                {{--@endif--}}
                @if(Auth::user()->can('accessAll_announcement'))
                    <div class="form-group">
                        <select class="form-control" name="org_id" id="org_id"
                                @if(!isset(Auth::user()->organization)) required @endif>
                            <option value=""></option>
                            @foreach($users as $item)
                                <option @if(isset($job)) @if($job->user_id == $item->id) selected @endif
                                @else
                                @if(Auth::id()== $item->id) selected
                                        @endif @endif value="{{$item->id}}"
                                        data-name="{{$item->organization->name}}">{{$item->organization->name}}
                                    ( {{$item->username}})
                                </option>
                            @endforeach

                        </select>
                        <label for="org_id"
                               class="control-label">{{trans('announcements.select_organization')}}
                            @if(!isset(Auth::user()->organization))
                                <span class="star">*</span>
                            @endif
                        </label>
                    </div>
                @endif


                <div class="form-group">
                    {{ Form::text("organization_$localeCode",isset($data[$localeCode]->organization)? $data[$localeCode]->organization : (isset($user->organization) ?$user->organization->name : null),array(' class'=>'form-control','required'=>'required','id'=>'org_name'))}}
                    <label for="{{"organization_$localeCode"}}"
                           class="control-label">{{trans('announcements.organization_name')}}
                        <span class="star">*</span></label>
                    {!! $errors->first("organization_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                </div>
                <div class="form-group">
                    {{ Form::text("title_$localeCode", $data[$localeCode]->title,array('required'=>'required','id'=>'title','class'=>'check'))}}
                    <label for="{{"title_$localeCode"}}" class="control-label">{{trans('announcements.title')}}<span
                                class="star">*</span></label>
                    {!! $errors->first("title_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                </div>

                @permission('accessAll_announcement')
                <div class="form-group">
                    {{ Form::text("menu_$localeCode",$data[$localeCode]->menu,array('required'=>'required','id'=>'menu','class'=>'check'))}}
                    <label for="{{"menu_$localeCode"}}" class="control-label">{{trans('announcements.menu')}}<span
                                class="star">*</span></label>
                    {!! $errors->first("menu_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                </div>
                @endpermission
                <div class="form-group">

                    @if($type->key =='job_opp' || $type->key =='volunteering' || $type->key=='internship')
                        {{ Form::text("term_$localeCode",$data[$localeCode]->term),['id'=>'term','class'=>'check']}}
                        <label for="username" class="control-label">{{trans('announcements.term')}}</label>
                    @elseif($type->key =='event' || $type->key=='publication' ||  $type->key=='competition')
                        {{ Form::text("term_$localeCode",$data[$localeCode]->term,['required'=>'required','id'=>'term','class'=>'check'])}}
                        <label for="username" class="control-label">{{trans('announcements.'.$type->key.'_type')}} <span
                                    class="star">*</span></label>
                    @else
                        {{ Form::text("term_$localeCode",$data[$localeCode]->term,['id'=>'term','class'=>'check'])}}
                        <label for="username"
                               class="control-label">{{trans('announcements.'.$type->key.'_type')}}</label>
                    @endif
                    {!! $errors->first("term_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                </div>

                @if($job_id==null && $translate==null)
                    @if($type->key =='publication')
                        <div class="form-group">
                            {{ Form::text("isbn",isset($job) ? $job->isbn : null ,['class'=>'check'])}}
                            <label for="{{"isbn"}}" class="control-label">{{trans('announcements.isbn')}}
                            </label>
                            {!! $errors->first("isbn", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    @endif
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <select name="location_country_id" required>
                                <option></option>
                                @foreach($countries as $item)
                                    <option @if(old('location_country_id')==$item->id) selected
                                            @elseif(isset($job) && $job->location_country_id == $item->id) selected
                                            @elseif(isset($loc_country) && $loc_country==$item->id) selected
                                            @endif value="{{$item->id}}">{{trans('countries.'.$item->code)}}</option>
                                @endforeach
                            </select>

                            <label for="address-country"
                                   class="control-label">{{trans('announcements.location_country')}} <span
                                        class="star">*</span></label>
                            @error('location_country_id')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::text("location_city_$localeCode", isset($data[$localeCode]->location_city) ? $data[$localeCode]->location_city : (isset($loc_city)? $loc_city : null),array('required'=>'required'))}}
                            <label for="{{"location_city_$localeCode"}}"
                                   class="control-label">{{trans('announcements.location_city')}}<span
                                        class="star">*</span></label>
                            {!! $errors->first("location_city_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    </div>

                </div>


                @if($job_id==null && $translate==null)
                    <div class="mb-3">


                        <input autocomplete="off" value="" required type="text" id="example"
                               placeholder="{{trans('announcements.select')}}"
                               name="interest_area" class="form-control">

                        {{--<select name="category[]" id="category" multiple--}}
                        {{--placeholder="{{trans('announcements.category')}} *">--}}
                        {{--@foreach($category as $item)--}}
                        {{--@if($item->name!='all_fields' && $item->name!='other' && $item->category_id==null)--}}
                        {{--<optgroup label="{{trans('category.'.$item->name)}}">--}}
                        {{--@if(isset($item->child) && count($item->child)!=0)--}}
                        {{--@foreach($item->child as $sub)--}}
                        {{--<option @if(isset($job) && isset($sub_category) && in_array($sub->id,$sub_category)) selected--}}
                        {{--@endif value="{{$sub->id}}">{{trans('sub_category_'.$item->name.'.'.$sub->name)}}</option>--}}
                        {{--@endforeach--}}
                        {{--@endif--}}
                        {{--</optgroup>--}}
                        {{--@endif--}}
                        {{--@if($item->name=='other')--}}
                        {{--<optgroup label="{{trans('category.'.$item->name)}}">--}}
                        {{--<option @if(isset($job) && isset($sub_category) && in_array($item->id,$sub_category)) selected--}}
                        {{--@endif value="{{$item->id}}">{{trans('category.'.$item->name)}}</option>--}}
                        {{--</optgroup>--}}
                        {{--@endif--}}
                        {{--@endforeach--}}
                        {{--</select>--}}
                        {{----}}
                        <p id="error_category" style="color:red" class="help-block"></p>


                    </div>
                    <div class="form-group mt-2" style="display: none" id="other-child">
                        {{ Form::text("other_child",null,array('id'=>'other-child-input'))}}
                        <label for="{{"other_child"}}"
                               class="control-label">{{trans('announcements.other')}}
                            <span
                                    class="star">*</span></label>
                        {!! $errors->first("other_child", '<p style="color:red" class="help-block">:message</p>') !!}
                    </div>
                @endif
                @if($type->key =='publication' || $type->key =='news')
                    <div class="form-group mt-2">
                        @if($type->key =='news')
                            {{ Form::text("author_$localeCode", $data[$localeCode]->author)}}
                            <label for="{{"author_$localeCode"}}"
                                   class="control-label">{{trans('announcements.author')}}
                            </label>
                        @else
                            {{ Form::text("author_$localeCode", $data[$localeCode]->author,array('required'=>'required'))}}
                            <label for="{{"author_$localeCode"}}"
                                   class="control-label">{{trans('announcements.author')}}
                                <span
                                        class="star">*</span></label>
                        @endif

                        {!! $errors->first("author_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                    </div>
                @endif
                @if($type->key =='publication')
                    <div class="form-group">
                        {{ Form::text("language_$localeCode", $data[$localeCode]->language,array('required'=>'required'))}}
                        <label for="{{"language_$localeCode"}}"
                               class="control-label">{{trans('announcements.language')}}
                            <span class="star">*</span></label>
                        {!! $errors->first("language_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group">
                        {{ Form::text("pages_$localeCode", $data[$localeCode]->language)}}
                        <label for="{{"pages_$localeCode"}}"
                               class="control-label">{{trans('announcements.pages')}}</label>
                        {!! $errors->first("pages_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                    </div>
                @endif
                <div class="form-group">


                    @if($type->key =='job_opp' || $type->key =='volunteering' || $type->key=='internship')

                        {{ Form::textarea("description_$localeCode",$data[$localeCode]->description,['id'=>'description','required'=>'required','rows'=>3])}}
                        <label class="control-label"
                               for="{{"description_$localeCode"}}">{{trans('announcements.description')}}
                            <span
                                    class="star">*</span></label>
                    @elseif($type->key =='news')

                        {{ Form::textarea("description_$localeCode",$data[$localeCode]->description,['id'=>'description','required'=>'required','rows'=>3])}}
                        <label class="control-label"
                               for="{{"description_$localeCode"}}">{{trans('announcements.news_details')}}
                            <span
                                    class="star">*</span></label>

                    @elseif($type->key =='publication')

                        {{ Form::textarea("description_$localeCode",$data[$localeCode]->description,['id'=>'description','rows'=>3])}}
                        <label class="control-label"
                               for="{{"description_$localeCode"}}">{{trans('announcements.detail_description')}}  </label>

                    @else

                        {{ Form::textarea("description_$localeCode",$data[$localeCode]->description,['id'=>'description','required'=>'required','rows'=>3])}}
                        <label class="control-label"
                               for="{{"description_$localeCode"}}">{{trans('announcements.detail_description')}} <span
                                    class="star">*</span> </label>
                    @endif
                    {!! $errors->first("description_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                </div>


                @if($type->key !='news' && $type->key !='event' && $type->key !='competition')
                    <div class="form-group height-textarea">

                        {{ Form::textarea("responsibilities_$localeCode",$data[$localeCode]->responsibilities, ['id' => 'responsibilities', 'data-required'=>'no','rows'=>3])}}
                        @if($type->key =='job_opp' || $type->key =='volunteering' || $type->key=='internship')
                            <label class="control-label" for="responsibilities"
                            >{{trans('announcements.responsibilities')}}</label>
                        @else
                            <label class="control-label"
                                   for="responsibilities">{{trans('announcements.educational_level')}}</label>
                        @endif

                        {!! $errors->first("responsibilities_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                    </div>
                @endif
                @if($type->key !='news' && $type->key !='publication')
                    <div class="form-group">

                        @if($type->key =='job_opp' || $type->key =='volunteering' || $type->key=='internship')
                            {{ Form::textarea("requirements_$localeCode",$data[$localeCode]->requirements,array('id' => 'requirements','required'=>'required','rows'=>3))}}
                            <label for="requirements"
                                   class="control-label">{{trans('announcements.required_qualifications')}}
                                <span
                                        class="star">*</span></label>
                        @else
                            {{ Form::textarea("requirements_$localeCode",$data[$localeCode]->requirements,array('id' => 'requirements','rows'=>3))}}
                            <label for="textarea" class="control-label">{{trans('announcements.requirements')}}</label>
                        @endif

                        {!! $errors->first("requirements_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                    </div>
                @endif
                @if($type->key =='job_opp' || $type->key =='volunteering' || $type->key=='internship')
                    <div class="form-group height-textarea">
                        {{ Form::textarea("salary_$localeCode",$data[$localeCode]->salary)}}
                        <label for="textarea" class="control-label">{{trans('announcements.salary')}}</label>
                        <p id="error_salary" style="color:red" class="help-block"></p>

                    </div>
                @endif
                @if($type->key !='news' && $type->key !='publication')
                    <div class="form-group">
                        @if($type->key == 'event')
                            {{ Form::textarea("procedures_$localeCode",$data[$localeCode]->procedures,array('data-required'=>'no','rows'=>3))}}
                            <label for="textarea"
                                   class="control-label">{{trans('announcements.app_procedures')}}</label>
                        @else
                            @if(isset($job))
                                @if($job->online_submission == 1)
                                    {{ Form::textarea("procedures_$localeCode",$data[$localeCode]->procedures,array('data-required'=>'no','rows'=>3,'id'=>'procedures'))}}
                                    <label for="textarea"
                                           class="control-label">{{trans('announcements.app_procedures')}}
                                        <span id="procedure-star" class="star"></span></label>
                                    </label>
                                @else
                                    {{ Form::textarea("procedures_$localeCode",$data[$localeCode]->procedures,array('required'=>'required','rows'=>3,'id'=>'procedures'))}}
                                    <label for="textarea"
                                           class="control-label">{{trans('announcements.app_procedures')}}
                                        <span id="procedure-star" class="star">*</span></label>
                                @endif
                            @else

                                {{ Form::textarea("procedures_$localeCode",$data[$localeCode]->procedures,array('required'=>'required','rows'=>3,'id'=>'procedures'))}}
                                <label for="textarea" class="control-label">{{trans('announcements.app_procedures')}}
                                    <span id="procedure-star" class="star">*</span></label>
                            @endif
                        @endif

                        {!! $errors->first("procedures_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                    </div>
                @endif
                <div class="d-flex flex-column flex-sm-row">
                    @if($type->key !='news' && $type->key !='publication')
                        <div class="form-group pr-sm-4 pr-0">
                            @if(\Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement'))
                                <input id="opening_date_admin" type="text" class="form-control"
                                       name="opening_date_{{$localeCode}}"
                                       value="{{strtotime($data[$localeCode]->opening_date)?\Carbon\Carbon::parse($data[$localeCode]->opening_date)->format('d F Y'):null}}">
                            @else
                                <input id="opening_date" type="text" class="form-control"
                                       name="opening_date_{{$localeCode}}"
                                       value="{{strtotime($data[$localeCode]->opening_date)?\Carbon\Carbon::parse($data[$localeCode]->opening_date)->format('d F Y'):null}}">
                            @endif
                            <label for="username" class="control-label">{{trans('announcements.opening_date')}}</label>
                            {!! $errors->first("opening_date_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    @endif
                    @if($type->key!='news' && $type->key!='publication')
                        <div class="form-group flex-grow-1">
                            @if($type->key=='event')
                                {{ Form::text("deadline_$localeCode",$data[$localeCode]->deadline)}}
                                <label for="Application"
                                       class="control-label">{{trans('announcements.deadline')}}</label>
                            @else
                                {{ Form::text("deadline_$localeCode",$data[$localeCode]->deadline,array('required'=>'required'))}}
                                <label for="Application" class="control-label">{{trans('announcements.deadline')}}<span
                                            class="star">*</span></label>
                            @endif

                            {!! $errors->first("deadline_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    @endif
                </div>


            </div>
            <div class="col-md-6">

                @if($job_id==null && $translate==null)
                    <div class="form-group">
                        {{ Form::text("code",isset($job) ? $job->code : null)}}
                        <label for="{{"code"}}" class="control-label">{{trans('announcements.code')}}
                        </label>
                        {!! $errors->first("code", '<p style="color:red" class="help-block">:message</p>') !!}
                    </div>
                @endif
                @if($type->key !='news' || $type->key !='publication')
                    <div class="form-group height-textarea">
                        {{ Form::textarea("open_$localeCode",$data[$localeCode]->open,array('id'=>'open_'.$localeCode,'data-required'=>'no','rows'=>3))}}
                        <label for="open_{{$localeCode}}" class="control-label">{{trans('announcements.open')}}</label>
                        {!! $errors->first("open_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                    </div>
                @endif
                <div class="form-group">
                    {{ Form::text("audience_$localeCode",$data[$localeCode]->audience)}}
                    <label for="audience_{{$localeCode}}"
                           class="control-label">{{trans('announcements.audience')}}</label>
                    {!! $errors->first("audience_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                </div>
                <div class="form-group">
                    @if($type->key =='publication')
                        {{ Form::text("start_date_$localeCode",$data[$localeCode]->start_date,['required'=>'required'])}}
                        <label for="start_date_{{$localeCode}}"
                               class="control-label">{{trans('announcements.publication_date')}}<span
                                    class="star">*</span></label>
                    @else
                        {{ Form::text("start_date_$localeCode",$data[$localeCode]->start_date)}}
                        <label for="start_date_{{$localeCode}}"
                               class="control-label">{{trans('announcements.start_date')}}</label>
                    @endif

                    {!! $errors->first("start_date_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                </div>
                @if($type->key !='news' && $type->key !='publication')
                    <div class="form-group">
                        {{ Form::text("duration_$localeCode",$data[$localeCode]->duration)}}
                        <label for="duration_{{$localeCode}}"
                               class="control-label">{{trans('announcements.duration')}}</label>
                        {!! $errors->first("duration_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                    </div>
                @endif

                <div class="form-group height-textarea">

                    {{ Form::textarea("about_$localeCode",$data[$localeCode]->about,array('data-required'=>'no','rows'=>3))}}
                    <label class="control-label" for="about_{{$localeCode}}">{{trans('announcements.about')}}</label>
                    {!! $errors->first("about_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                </div>
                <div class="form-group height-textarea">
                    {{ Form::textarea("about_company_$localeCode",isset($data[$localeCode]->about_company)? $data[$localeCode]->about_company : (isset($about_company) ? $about_company : null),array('data-required'=>'no','rows'=>3,'id'=>'about_company'))}}
                    <label class="control-label"
                           for="about_company_{{$localeCode}}">{{trans('announcements.about_company')}}</label>
                    {!! $errors->first("about_company_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                </div>
                <div class="form-group height-textarea">
                    {{ Form::textarea("notes_$localeCode",$data[$localeCode]->notes,array('data-required'=>'no','rows'=>3))}}
                    <label for="notes_{{$localeCode}}" class="control-label">{{trans('announcements.notes')}}</label>
                    {!! $errors->first("notes_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}
                </div>

                <div id="dynamic_field">
                    @if(isset($job) && count($job->attachment)!=0)
                        @for($i=0;$i<count($job->attachment);$i++)
                            <div id="div_{{$i+1}}" class="d-flex deleting">
                                <div class="form-group job-file-upload">
                                    @if($job->attachment[$i]->image==1)
                                        <a href="{{Storage::disk('')->url($job->attachment[$i]->file)}}" target="_blank"
                                           class="job-file-upload-img">
                                            <img data-src="{{Storage::disk('')->url($job->attachment[$i]->file)}}"
                                                 alt="" class="job-file-image">
                                        </a>
                                    @else
                                        <a href="{{Storage::disk('')->url($job->attachment[$i]->file)}}" download
                                           class="job-file-upload-img"><img src="/img/download.svg" alt="" width="20"
                                                                            height="20px"
                                                                            class="mr-2">{{$job->attachment[$i]->name}}
                                        </a>
                                    @endif
                                    <div class="position-relative">
                                        <input type="text" value="{{$job->attachment[$i]->description}}"
                                               name="edited_description[{{$job->attachment[$i]->id}}]">
                                        <label for=""
                                               class="control-label">{{trans('announcements.attachment-description')}}</label>
                                    </div>
                                    {{--<input type="text" value="{{$job->attachment[$i]->name}}">--}}
                                    {{--<label for="" class="control-label">{{trans('announcements.file')}}</label>--}}
                                    <button data-file="{{$job->attachment[$i]->id}}" type="button" name="remove"
                                            id="{{$i+1}}" class="btn btn-danger deleting-button btn_remove existing">X
                                    </button>
                                </div>

                            </div>
                        @endfor
                    @endif
                </div>
                <button type="button" name="add" id="add"
                        class="add-input font-bold">{{trans('announcements.add_attachment')}}</button>
                @if(isset($job))
                    <input type="hidden" name="file_count" id="file_count"
                           value="{{count($job->attachment)}}">
                @else
                    <input type="hidden" name="file_count" id="file_count"
                           value="{{old('file_count')}}">
                @endif




                {{--<div class="form-group-file mb-input">--}}
                {{--<label for="file" class="sr-only">{{trans('announcements.choose_file')}}</label>--}}
                {{--<input type="file"--}}
                {{--class="filepond"--}}
                {{--name="filepond[]"--}}
                {{--multiple--}}
                {{--data-max-file-size="3MB"--}}
                {{--data-max-files="10"/>--}}
                {{--<span class="input-help">{{trans('announcements.file_size')}}</span>--}}
                {{--{!! $errors->first("notes_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}--}}
                {{--</div>--}}

                <div class="checkbox">
                    <label>
                        <input id="online_submission" @if(isset($job) && $job->online_submission==1)  checked
                               @endif name="online_submission" type="checkbox"/><i
                                class="helper"></i>{{trans('announcements.submission')}}
                    </label>
                    @if(isset($job))
                        @if($job->status != 'pending')
                            @if(!isset($job->mirror) || (isset($job->mirror) && $job->mirror->status!='pending'))
                                @permission('accessAll_announcement')
                                <label>
                                    <input name="same_place" type="checkbox"/><i
                                            class="helper"></i>{{trans('announcements.move_up')}}
                                </label>
                                @endpermission
                            @endif
                        @endif
                    @endif
                </div>
                <input type="hidden" value="{{$type->id}}" name="type_id">
                <input type="hidden" value="{{$job_id}}" name="job_id">
                <input type="hidden" value="{{$type->key}}" name="type">
                @if(isset($job))
                    <input type="hidden" value="{{$job->id}}" name="id">
                @endif
            </div>

        </div>
        <div class="d-flex justify-content-end mt-5 flex-wrap">
            @permission('preview_announcement')
            <div class="text-center mb-2">
                <button name="action" onclick="submitClick(this)" value="preview" type="submit"
                        class="job-btn btn btn-green-gradient">{{trans('announcements.preview')}}
                </button>
            </div>
            @endpermission

            <div class="text-center ml-3 mb-2">
                <a href="{{$previous_url}}"
                   class="btn btn-red-gradient">{{trans('announcements.cancel')}}</a>
            </div>
            @if(isset($job))
                @permission('saveAsDraft_announcement')
                <div class="text-center ml-3 mb-2">
                    <button id="draft" name="action" onclick="submitClick(this)" value="draft" type="submit"
                            class="job-btn btn btn-dark-blue-gradient">{{trans('announcements.save_as_draft')}}
                    </button>
                </div>
                @endpermission
            @endif

        </div>
    </div>
    {{--end tab 1--}}


    {{--tab 2--}}
    <div class="tab-pane fade" id="company" role="tabpanel" aria-labelledby="company-tab">
        <h4 class="mb-4">{{trans('announcements.information_for_career_use_only')}}</h4>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    {{ Form::text("contact_person_$localeCode", isset($data[$localeCode]->contact_person)? $data[$localeCode]->contact_person : $user->first_name.' '.$user->last_name,['id'=>'contact_person'])}}
                    <label for="{{"contact_person_$localeCode"}}"
                           class="control-label">{{trans('announcements.contact_person')}}
                        <span class="star">*</span></label>
                    <p id="error_contact_person" style="color:red" class="help-block"></p>
                    {{--{!! $errors->first("contact_person_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}--}}
                </div>
                <div class="form-group">
                    {{ Form::text("contact_title_$localeCode",isset($data[$localeCode]->contact_title)? $data[$localeCode]->contact_title : (isset($user->organization) ?$user->organization->position : null),['id'=>'contact_title'])}}
                    <label for="{{"contact_title_$localeCode"}}"
                           class="control-label">{{trans('announcements.contact_title')}}
                        <span class="star">*</span>
                    </label>
                    <p id="error_contact_title" style="color:red" class="help-block"></p>
                    {{--{!! $errors->first("contact_title_$localeCode", '<p style="color:red" class="help-block">:message</p>') !!}--}}
                </div>
                <div class="form-group">
                    {{ Form::text("phone",isset($job)? $job->phone : $phone ,array('id'=>'phone'))}}
                    <label for="{{"phone"}}" class="control-label">{{trans('announcements.phone')}}
                        <span class="star">*</span></label>
                    {{--{!! $errors->first("phone", '<p style="color:red" class="help-block">:message</p>') !!}--}}
                    <p id="error_phone" style="color:red" class="help-block"></p>
                </div>
                <div class="form-group">
                    {{ Form::text("email",isset($job)? $job->email : $user->email ,array('id'=>'email'))}}
                    <label for="{{"email"}}" class="control-label">{{trans('announcements.email')}}
                        <span class="star">*</span></label>
                    <p id="error_email" style="color:red" class="help-block"></p>
                    {{--{!! $errors->first("email", '<p style="color:red" class="help-block">:message</p>') !!}--}}
                </div>
                <div class="form-group height-textarea">
                    {{ Form::textarea("organization_notes_$localeCode",$data[$localeCode]->organization_notes,array('rows'=>3))}}
                    <label for="organization_notes_{{$localeCode}}"
                           class="control-label">{{trans('announcements.organization_notes')}}</label>
                </div>
                <div class="form-group">
                    @if(Auth::user()->can('accessAll_announcement'))
                        <input @if(isset($job)) value="{{\Carbon\Carbon::parse($job->expiration_date)->format('d F Y') }}"
                               @endif  type="text" id="expiration_date_admin" name="expiration_date"
                               class="form-control date-style">
                    @else
                        <input @if(isset($job)) value="{{\Carbon\Carbon::parse($job->expiration_date)->format('d F Y') }}"
                               @endif  type="text" id="expiration_date" name="expiration_date"
                               class="form-control date-style">
                    @endif
                    <label for="expiration_date" class="control-label">{{trans('announcements.expiration_date')}}<span
                                class="star">*</span></label>
                    {!! $errors->first("expiration_date", '<p style="color:red" class="help-block">:message</p>') !!}
                </div>
                @if(isset($job) && \Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement'))
                    <div>
                        <input type="hidden" value="{{$job->id}}" name="jobId">
                        @if(isset($job->added_user))
                            <p class="mb-0"><b>{{trans('announcements.added_by')}}:</b>
                                {{$job->user->username}}   @if($job->added_user->username!= $job->user->username )
                                    ({{$job->added_user->username}}) @endif,
                                <b>{{trans('announcements.on')}}</b> {{\Carbon\Carbon::parse($job->created_at)->format('Y-m-d h:i:s')}}
                                ,
                                <b> {{trans('announcements.from')}}:</b>
                                {{$job->added_IP}} </p>
                        @endif
                        @if(isset($job->edited_user))
                            <p><b>{{trans('announcements.last_edited_by')}}:</b> {{$job->edited_user->username}},
                                <b>{{trans('announcements.on')}}</b> {{\Carbon\Carbon::parse($job->updated_at)->format('Y-m-d h:i:s')}}
                                ,
                                <b> {{trans('announcements.from')}}:</b>
                                {{$job->edited_IP}}</p>
                        @endif
                    </div>
                @endif

                <input type="hidden" name="announcement_id" id="announcement_id">
                <input type="hidden" name="new_cat_id" id="new_cat_id">
            </div>
        </div>
    </div>
    {{--end tab 2--}}

    {{--<div class="col-lg-12">--}}
    {{--<div class="d-flex justify-content-end mt-5 flex-wrap">--}}
    {{--@permission('preview_announcement')--}}
    {{--<div class="text-center mb-2">--}}
    {{--<button name="action" onclick="submitClick(this)" value="preview" type="submit"--}}
    {{--class="job-btn btn btn-green-gradient">{{trans('announcements.preview')}}--}}
    {{--</button>--}}
    {{--</div>--}}
    {{--@endpermission--}}

    {{--<div class="text-center ml-3 mb-2">--}}
    {{--<a href="{{$previous_url}}"--}}
    {{--class="btn btn-red-gradient">{{trans('announcements.cancel')}}</a>--}}
    {{--</div>--}}
    {{--@if(isset($job))--}}
    {{--@permission('saveAsDraft_announcement')--}}
    {{--<div class="text-center ml-3 mb-2">--}}
    {{--<button id="draft" name="action" onclick="submitClick(this)" value="draft" type="submit"--}}
    {{--class="job-btn btn btn-dark-blue-gradient">{{trans('announcements.save_as_draft')}}--}}
    {{--</button>--}}
    {{--</div>--}}
    {{--@endpermission--}}
    {{--@endif--}}

    {{--</div>--}}
    {{--</div>--}}
</div>

