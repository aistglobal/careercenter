@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css"
          rel="stylesheet">
    <link href="https://unpkg.com/filepond/dist/filepond.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link href="https://unpkg.com/multiple-select@1.4.0/dist/multiple-select.css" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }

        #category {
            width: 500px;
        }

        .ms-parent {
            width: 100% !important;
        }

        .ms-choice {
            height: 49px;
        }

        .ms-choice > div, .ms-choice > span {
            top: 10px;
            padding-left: 11px;
        }

        .ms-drop ul > li:not(.group):not(.ms-select-all) {
            padding-left: 17px;
        }

        .ms-drop ul > li label.optgroup {
            font-weight: normal;
            font-family: "lato-black", sans-serif;
        }

        html:lang(hy) .ms-drop ul > li label.optgroup,
        html:lang(ru) .ms-drop ul > li label.optgroup {
            font-weight: bold;
        }

        .ms-choice > span.placeholder {
            padding-left: 25px;
        }
    </style>
@endsection

@section('content')

    <section class="position-relative">
        @include('profile.menu')
        <div class="profile-container float-right">
            {!! Form::model($job, [
                                       'method' => 'PATCH',
                                       'route' => ['announcements.update',$job->id],
                                       'class' => 'form-horizontal',
                                       'files' => true,
                                       'id' => 'job_form'
                                   ]) !!}

            @include ('profile.job.form')
            <input type="hidden" id="job_id" value="{{$job->id}}">
            {!! Form::close() !!}
        </div>
    </section>


@endsection
@section('scripts')
    <script src="https://unpkg.com/filepond-plugin-file-encode/dist/filepond-plugin-file-encode.min.js"></script>
    <script src="https://unpkg.com/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.min.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-exif-orientation/dist/filepond-plugin-image-exif-orientation.min.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.js"></script>
    <script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/7.3.2/jquery.mmenu.all.js"></script>
    <script src="/js/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
    <script src="/js/Select2-4.0.7.js" type="text/javascript"></script>
    <script src="/js/comboTreePlugin.js"></script>
    <script src="/js/icontains.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <script src="https://unpkg.com/multiple-select@1.4.0/dist/multiple-select.js"></script>
    <script>

        var data = [];
        var obj = {};
        var subs = [];
        var sub_obj = {};
        var categories = [];
        var selected = [];

        @foreach($sub_category as $item)
                selected.push({{$item}})
                @endforeach

                @foreach($category as $item)
                @if($item->name!='all_fields' && $item->category_id==null)
            obj = {};
        obj.id = '{{$item->id}}';
        obj.title = '{{trans('category.'.$item->name)}}';
        @if(isset($item->child) && count($item->child)!=0)
            subs = [];
        @foreach($item->child as $sub)
            sub_obj = {};
        sub_obj.id = '{{$sub->id}}';
        sub_obj.title = '{{trans('sub_category_'.$item->name.'.'.$sub->name)}}';
        subs.push(sub_obj);
        @endforeach
            obj.subs = subs;
        @endif
data.push(obj);
        @endif
        @endforeach
        selected = selected;

        var combo = $('#example').comboTree({
            source: data,
            isMultiple: true,
            cascadeSelect: true,
            selected: selected
        });

        $('.form-group input').focus(function () {
            var err = $(this).parent().find('.help-block');
            err.text('');
        })
        $('#org_id').select2();
        $('#sub_category').select2();
        var about = 'about_' + '{{$localeCode}}';
        var about_company = 'about_company_' + '{{$localeCode}}';
        var description = 'description_' + '{{$localeCode}}';
        var responsibilities = 'responsibilities_' + '{{$localeCode}}';
        var requirements = 'requirements_' + '{{$localeCode}}';


        $('#org_id').on('change', function () {
            $('#about_company').removeClass('form-file');
            var user_id = $(this).val();
            var name = $('option:selected', this).data("name");
            $('#org_name').val(name);
//            CKEDITOR.instances['about_company'].setData(' ');
            $.ajax({

                type: "POST",
                data: {
                    'id': user_id
                },
                url: '/get/company',
                success: function (result) {
//                    CKEDITOR.instances['about_company'].setData(result['about_company']);
                    $('#contact_person').val(result['contact_person']);
                    $('#contact_title').val(result['contact_title']);
                    $('#email').val(result['email']);
                    $('#phone').val(result['phone']);
                    if (result['about_company'] != '') {
                        $('#about_company').addClass('form-file');
                    }
                    $('#about_company').val(result['about_company']);
                    $('#about_company').css('height', 'auto');
                    $('#about_company').css('height', $('#about_company')[0].scrollHeight + 'px')
                    var org_name = $('#org_name').val();
                    var title = $('#title').val();
                    var menu = org_name + ' / ' + title;
                    $('#menu').val(menu);
                    $('#menu').addClass('form-file');
                }
            });


        })

        $('#title').focusout(function () {
            var org_name = $('#org_name').val();
            var title = $('#title').val();
            var menu = org_name + ' / ' + title;
            $('#menu').val(menu);
            $('#menu').addClass('form-file');
        });
        @if($errors->has('expiration_date') || $errors->has('contact_title_en') || $errors->has('contact_title_ru') || $errors->has('contact_title_hy') || $errors->has('contact_person_en') || $errors->has('contact_person_ru') || $errors->has('contact_person_hy')|| $errors->has('phone') ||  $errors->has('email') )
$('a[href="#company"]').tab('show');
        @endif

        function submitClick(button) {
            $('<input/>').attr('type', 'hidden')
                .attr('name', "action")
                .attr('value', button.value)
                .appendTo('#job_form');
        }


        //        $('.save').click(function(){
        //            var error = 0;
        //            $(':input[required]', '#job_form').each(function(){
        //
        //                if($(this).val() == ''){
        //                    $(this).css('border','2px solid red');
        //                    if(error == 0){
        //                        $(this).focus();
        //                        var tab = $(this).closest('.tab-pane').attr('id');
        //                        console.log(tab);
        //                        $('a[href="#' + tab + '"]').tab('show');
        //                    }
        //                    error = 1;
        //                }
        //            });
        //            if(error == 1) {
        //                return false;
        //            } else {
        //                return true;
        //            }
        //        });

        $(document).ready(function () {
            $(document).on('change', '.cat', function (e) {
                var el = $(this);
                var span = $(this).closest('span');
                if (span.text() == 'Other') {
                    setTimeout(function () {
                        if(e.currentTarget.checked)
                        {
                            $('.comboTreeDropDownContainer').css('display', 'none');
                            $('#other-child').css('display', 'block');
                            $('#other-child-input').addClass('form-file');
                            $('#other-child-input').attr('required', 'required');
                            $('#other-child-input').focus();
                        }
                        else
                        {
                            $('#other-child-input').val('');
                            $('.comboTreeDropDownContainer').css('display', 'block');
                            $('#other-child').css('display', 'none');
                            $('#other-child-input').removeAttr('required');
                            console.log("chnshvac");

                        }
                        console.log(e.currentTarget.checked)
                    }, 200);

                }

                if (span.closest('li').hasClass('ComboTreeItemChlid')) {

                    var parent = span.closest('.ComboTreeItemParent');
                    var spanParent = parent.children('.comboTreeItemTitle');
                    var input = spanParent.find('input');
                    if (input.prop('checked') == false) {
                        input.prop('checked', 'true');
                    }
                }
                else {
                    console.log('parent');
                }

            })

            $('#online_submission').on('click', function () {
                if ($(this).prop('checked') == true) {
                    $('#procedures').removeAttr('required');
                    $('#procedure-star').text('');
                }
                else {
                    $('#procedures').attr("required", "required");
                    $('#procedure-star').text('*');
                }
            })
            let select = $('.ms-select-all')
            let txt = select.find('span');
            let new_sel = '{{trans('announcements.select_all')}}';
            txt.html(new_sel);
            var end = '{{$end}}';
            var start = '{{$start}}';
            var start_opening_date = '{{$start_opening_date}}';
            $(function () {
                $('#expiration_date').daterangepicker({
                    singleDatePicker: true,
                    timePicker: false,
                    "minDate": start,
                    "maxDate": end,
                    locale: {
                        format: 'DD MMM YYYY'
                    }
                });
            });
            $(function () {
                $('#expiration_date_admin').daterangepicker({
                    singleDatePicker: true,
                    timePicker: false,
                    locale: {
                        format: 'DD MMM YYYY'
                    }
                });
            });
            $(function () {
                $('#opening_date').daterangepicker({
                    singleDatePicker: true,
                    "minDate": start_opening_date,
                    drops: 'up',
                    "locale": {
                        format: 'DD MMM YYYY'
                    }
                });
            });
            $(function () {
                $('#opening_date_admin').daterangepicker({
                    singleDatePicker: true,
                    drops: 'up',
                    "locale": {
                        format: 'DD MMM YYYY'
                    }
                });
            });

            var l = $('#file_count').val();
            var sum = $('#file_count').val();
            $('#add').click(function () {
                if (sum <= 4) {
                    l++;
                    $('#dynamic_field').append('<div id="div_' + l + '" class="d-flex deleting position-relative">' +
                        '<div class="job-file-upload-add w-100">' +
                        '<div class="form-group"><input data-id="' + l + '" class="form-control files" type="file" id="file' + l + '" required name="file[]">' +
                        '<label class="control-label" for="file' + l + '" >{{trans('announcements.file')}}</label>' +
                        '<p id="error_file_' + l + '" style="color:red" class="help-block"></p>' +
                        '</div>' +
                        '<div class="position-relative form-group">' +
                        '<input name="file_description[]" class="form-control" id="description' + l + '"  required>' +
                        '<label class="control-label" for="file' + l + '" >{{trans('announcements.attachment-description')}}</label>' +
                        '</div>' +
                        '<button type="button" name="remove" id="' + l + '" class="btn btn-danger deleting-button btn_remove delete">X</button></div>');
                    sum++;
                }
                $('#file_count').val(l);
                console.log(l);
            });
            $(document).on('click', '.delete', function () {
                var button_id = $(this).attr("id");
                var key_val = $('#file_count').val();
                $('#file_count').val(key_val - 1);
                $('#div_' + button_id).remove();
                sum--;
                l--;

            });

            $(document).on('change', '.files', function () {
                $('#add').attr('disabled', false);
                var size = this.files[0].size;
                var id = $(this).attr('data-id');
                $('#error_file_' + id).text('');
                if (size > 3000000) {
                    $('#add').attr('disabled', true);
                    $(this).val('');
                    $('#error_file_' + id).text('file is too large');
                }

            });
            var localecode = '{{$localeCode}}';
            $('#job_form').submit(function (evt) {
                var selectedIds = combo.getSelectedItemsId();

                evt.preventDefault();
                $('<input/>').attr('type', 'hidden')
                    .attr('name', "category")
                    .attr('value', selectedIds)
                    .appendTo('#job_form');
                $('.job-btn').attr('disabled', true);

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    data: formData,
                    url: '{{route('announcements.update',$job->id)}}',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        console.log(result);
                        $('.job-btn').attr('disabled', false);
                        if (result['cikl'] != undefined ) {

                        }
                        else {

                            $('.help-block').each(function () {
                                $(this).text('');
                            });
                            window.location = result['redirect'];
                        }
                    },
                    error: function (data) {
                        $('.job-btn').attr('disabled', false);
                        var errors = data.responseJSON;
                        $('.help-block').each(function () {
                            $(this).text('');
                        })
                        if (errors['errors']['phone'] != undefined) {
                            $('a[href="#company"]').tab('show');
                            $('#error_phone').html(errors['errors']['phone']);
                        }
                        if (errors['errors']['email'] != undefined) {
                            $('a[href="#company"]').tab('show');
                            $('#error_email').html(errors['errors']['email']);
                        }
                        if (errors['errors']['contact_person_' + localecode] != undefined) {
                            $('a[href="#company"]').tab('show');
                            $('#error_contact_person').html(errors['errors']['contact_person_' + localecode]);
                        }
                        if (errors['errors']['contact_title_' + localecode] != undefined) {
                            $('a[href="#company"]').tab('show');
                            $('#error_contact_title').html(errors['errors']['contact_title_' + localecode]);
                        }
                        if (errors['errors']['category'] != undefined) {
                            $('#error_category').html(errors['errors']['category']);
                        }
                        if (errors['errors']['salary_' + localecode] != undefined) {
                            $('#error_salary').html(errors['errors']['salary_' + localecode]);
                        }


                    }
                })
            });

            $(document).on('click', '#draft', function (e) {
                e.preventDefault();
                $('.job-btn').attr('disabled', true);
                $('#job_form').attr('novalidate');
                $('#job_form').submit();
            });

            $('.existing').on('click', function () {
                var file_id = $(this).attr('data-file');
                var button_id = $(this).attr("id");
                $.ajax({
                    type: "POST",
                    data: {'id': file_id},
                    url: '/' + localecode + '/remove/attachment',
                    success: function (result) {

                        var key_val = $('#file_count').val();
                        $('#file_count').val(key_val - 1);
                        $('#div_' + button_id).remove();
                        sum--;
                        l--;
                    },
                    error: function (data) {
                        var errors = data.responseJSON;
                        console.log(errors);
                    }
                });

            })

            FilePond.registerPlugin(
                // encodes the file as base64 data
                FilePondPluginFileEncode,

                // validates the size of the file
                FilePondPluginFileValidateSize,

                // corrects mobile image orientation
                FilePondPluginImageExifOrientation,
                //
                // // edit image plugin
                // FilePondPluginImageEdit,

                // previews dropped images
                FilePondPluginImagePreview
            );


            // Select the file input and use create() to turn it into a pond
            const pond = FilePond.create(
                document.querySelector('.filepond'),
                {
                    allowMultiple: true,
                }
            );
            var id = $('#job_id').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({

                type: "POST",
                data: {
                    'id': id,
                },
                url: '/job/attachments',
                success: function (result) {

                    if (result.length != 0) {
                        for (var i in result) {
                            pond.addFile('/storage/' + result[i].file);

                        }
                    }
                }
            });

            @if($job->status == 'draft' || (isset($job->mirror) && $job->mirror->status=='draft'))
            setInterval(function(){
                $('.job-btn').attr('disabled', true);
                $('#job_form').attr('novalidate');
                $('#job_form').append("<input value='cikl' name='action' type='hidden'/>");
                $('#job_form').submit();
            },10000);
            @endif

        });
    </script>
@endsection






