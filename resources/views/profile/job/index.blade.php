@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        .navigation {
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')
    <section class="position-relative bg-grey">
        @include('profile.menu')

        <div class="profile-container float-right">

            <div class="profile-table">
                <div class="d-flex flex-wrap flex-md-nowrap mt-2 filter-div">
                    {{--<div class="form-group mb-0 mr-2">--}}
                        {{--<form id="myForm2" action="{{route('announcements.index')}}">--}}
                            {{--<select name="type" id="type">--}}
                                {{--<option value=""></option>--}}
                                {{--@foreach($types as $item)--}}
                                    {{--<option @if($type == $item->id) selected @endif value="{{$item->id}}">{{trans('types.'.$item->key)}}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                            {{--<label for="type" class="control-label"> {{trans('profile_tables.type')}}</label>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                    {{--<div class="form-group mb-0 mr-2">--}}
                        {{--<form id="myForm3" action="{{route('announcements.index')}}">--}}
                            {{--<select name="status" id="job-status">--}}
                                {{--<option value=""></option>--}}
                                {{--@foreach(config('enums.job_status') as $item)--}}
                                    {{--<option @if($status == $item) selected @endif  value="{{$item}}">{{trans('profile_tables.'.$item)}}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                            {{--<label for="job-status" class="control-label">   {{trans('profile_tables.status')}}</label>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                    <form id="myForm1" action="{{route('announcements.index')}}"
                          class="profile-search-form d-flex position-relative ml-auto">
                        <input value="{{$search}}" id="search" name="search" class="form-control" type="search"
                               placeholder="{{trans('profile_tables.search')}}…" aria-label="Search">
                        <button id="search_btn" class="btn" type="button"><img src="/img/search.svg" alt=""></button>
                    </form>
                </div>


                <div class="table-responsive new-table">
                    <table class="table mobile-table">
                        <thead>
                        <tr class="table-tr-th">
                            <th>{{trans('profile_tables.title')}}</th>
                            <th>
                                {{--{{trans('profile_tables.type')}}--}}
                                <form id="myForm2" action="{{route('announcements.index')}}" class="form-group mb-0" style="width: 280px">
                                    <select name="type" id="type">
                                        <option value="">{{trans('profile_tables.all')}}</option>
                                        @foreach($types as $item)
                                            <option @if($type == $item->id) selected @endif value="{{$item->id}}">{{trans('types.'.$item->key)}}</option>
                                        @endforeach
                                    </select>
                                    {{--<label for="type" class="control-label"> {{trans('profile_tables.type')}}</label>--}}
                                </form>
                            </th>

                            <th>
                                <form id="myForm3" action="{{route('announcements.index')}}" class="form-group mb-0" style="width: 175px">
                                    <select name="status" id="job-status">
                                        <option value="">{{trans('profile_tables.status')}}</option>
                                        @foreach(config('enums.job_status') as $item)
                                            <option @if($status == $item) selected @endif  value="{{$item}}">{{trans('profile_tables.'.$item)}}</option>
                                        @endforeach
                                    </select>
                                    {{--<label for="job-status" class="control-label">   {{trans('profile_tables.status')}}</label>--}}
                                </form>
                                {{--{{trans('profile_tables.status')}}--}}
                            </th>
                            @if(\Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement'))
                            <th>{{trans('profile_tables.change_status')}}</th>
                            @endif
                            <th class="text-center">{{trans('profile_tables.action')}}</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($jobs as $job)
                            @if(isset($job->data))
                                <tr class="col-sd-3">
                                    <td data-label="{{trans('profile_tables.title')}}">@if(isset($job)){{$job->data->title}}@endif</td>
                                    <td data-label="{{trans('profile_tables.type')}}">@if(isset($job)){{trans('types.'.$job->type->key)}}@endif</td>
                                    <td data-label="{{trans('profile_tables.status')}}">{{trans('profile_tables.'.$job->status)}}</td>
                                    @if(\Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement'))
                                    <td data-label="{{trans('profile_tables.change_status')}}">
                                    @if($job->status!='draft')
                                        {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['status-change', $job->id]]) !!}
                                         <select class="form-control status pr-0 select-width" name="status">
                                             @foreach(config('enums.job_status') as $item)
                                                 <option @if($job->status == $item) selected @endif value="{{$item}}">{{trans('profile_tables.'.$item)}}</option>
                                                 @endforeach
                                        </select>
                                        {!! Form::close() !!}
                                    @endif
                                   </td>
                                    @endif
                                    <td data-label="{{trans('profile_tables.action')}}">
                                        <div class="table-buttons d-flex justify-content-center">
                                            @permission('preview_announcement')
                                            <a title="{{trans('profile_tables.view')}}" href="{{route('jobs',$job->slug)}}"><img src="/img/visibility.svg" alt="view"></a>
                                            @endpermission

                                            @permission('edit_announcement')
                                            <a title="{{trans('profile_tables.edit')}}" href="{{route('announcements-edit',$job->id)}}"><img src="/img/edit.svg" alt=""></a>
                                            @endpermission

                                            @permission('delete_announcement')
                                            {!! Form::open([
                                            'method'=>'DELETE',
                                            'route' => ['announcements.destroy',$job->id],
                                            'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<img src="/img/delete.svg">', array(
                                                    'type' => 'submit',
                                                    'class' => '',
                                                    'title' => trans('profile_tables.delete'),
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                            {!! Form::close() !!}
                                            @endpermission

                                        </div>
                                    </td>
                                </tr>
                            @endif
                        @endforeach

                        </tbody>
                    </table>
                </div>

                <div class="d-flex justify-content-end my-pagination align-items-center">
                    <form id="myForm" action="{{route('announcements.index')}}">
                        <div>
                            <label for="rr">{{trans('jobs.item_per_page')}}:</label>

                            <select name="order_by" id="rr">
                                <option @if($order==20) selected @endif value="20">20</option>
                                <option @if($order==50) selected @endif value="50">50</option>
                                <option @if($order==100) selected @endif value="100">100</option>
                            </select>

                        </div>
                    </form>
                    {{--<span>1-20 of 23</span>--}}

                    <div class="pagination-links d-flex">
                        {{ $jobs->appends(['search' => Request::get('search'),'order_by' => Request::get('order_by'),'status' => Request::get('status'),'type' => Request::get('type')])->links('vendor.pagination.bootstrap-4')}}
                        {{--<a href="#" class="d-flex justify-content-center align-items-center"><img--}}
                                    {{--src="/img/arrow-left.svg" alt=""></a>--}}
                        {{--<a href="#" class="d-flex justify-content-center align-items-center"><img--}}
                                    {{--src="/img/arrow-right.svg" alt=""></a>--}}
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
@section('scripts')
    <script>
        $('#rr').on('change',function()
        {
            var search = $('#search').val();
            var type = $('#type').val();
            var status = $('#job-status').val();
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "search").val(search).appendTo('#myForm');
               $("<input>")
                .attr("type", "hidden")
                .attr("name", "type").val(type).appendTo('#myForm');
            $("<input>")
                .attr("type", "hidden")
                .attr("name", "status").val(status).appendTo('#myForm');


            $( "#myForm" ).submit();

        });

        $('#type').on('change',function()
        {
            var search = $('#search').val();
            var order = $('#rr').val();
            var status = $('#job-status').val();
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "search").val(search).appendTo('#myForm2');
            var input1 = $("<input>")
                .attr("type", "hidden")
                .attr("name", "order_by").val(order).appendTo('#myForm2');
            $("<input>")
                .attr("type", "hidden")
                .attr("name", "status").val(status).appendTo('#myForm2');

            $( "#myForm2" ).submit();

        });
        $('#job-status').on('change',function()
        {
            var order = $('#rr').val();
            var search = $('#search').val();
            var type = $('#type').val();
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "order_by").val(order).appendTo('#myForm3');
            $("<input>")
                .attr("type", "hidden")
                .attr("name", "search").val(search).appendTo('#myForm3');
            $("<input>")
                .attr("type", "hidden")
                .attr("name", "type").val(type).appendTo('#myForm3');

            $( "#myForm3" ).submit();
        })
        $('#search_btn').on('click',function()
        {
            var order = $('#rr').val();
            var status = $('#job-status').val();
            var type = $('#type').val();
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "order_by").val(order).appendTo('#myForm1');
            $("<input>")
                .attr("type", "hidden")
                .attr("name", "status").val(status).appendTo('#myForm1');
            $("<input>")
                .attr("type", "hidden")
                .attr("name", "type").val(type).appendTo('#myForm1');

            $( "#myForm1" ).submit();
        });


        $('#search').keypress(function (e) {
            var key = e.which;
            if(key == 13)  // the enter key code
            {
                var order = $('#rr').val();
                var status = $('#job-status').val();
                var type = $('#type').val();
                var input = $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "order_by").val(order).appendTo('#myForm1');
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "status").val(status).appendTo('#myForm1');
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "type").val(type).appendTo('#myForm1');

                $( "#myForm1" ).submit();
            }
        });
        $('.status').on('change',function()
        {
            $(this).closest('form').submit();
        });

    </script>
@endsection
