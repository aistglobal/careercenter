<style>
    .profile-user-info h6 a {
        color: white;
        text-decoration: none;
    }
</style>

<div class="left-menu float-left">
    <a class="open_close-menu" href="#">
        <i></i>
        <i></i>
        <i></i>
    </a>
    <div class="profile-user-info d-flex align-items-center">
        <label class="cabinet center-block">
            <figure class="position-relative">
                <img src="" class="gambar" id="item-img-output"/>
                <figcaption>{{trans('menu.edit')}}</figcaption>
            </figure>
            @if(\Illuminate\Support\Facades\Auth::user()->is_organization == 1)
                <input accept="image/png, image/jpeg" type="file" class="org-img file center-block" name="file_photo"/>
                @else
                <input accept="image/png, image/jpeg" type="file" class="item-img file center-block" name="file_photo"/>
                @endif

        </label>

        <h6>
            {{ Auth::user()->username }}

            @if(\Illuminate\Support\Facades\Session::has('origin_account'))
                /
                <a href="{{route('usersLogin',['id'=>Session::get('origin_account')->id,'origin'=>'no'])}}">{{Session::get('origin_account')->username}}</a>
            @endif

        </h6>
    </div>
    <ul class="nav flex-column menu-nav">
        @permission('give_permissions')
        <li class="nav-item">
            <a class="{{ (Request::is("*/profile/permission*")  ? 'active' : '') }} nav-link d-flex align-items-center"
               href="{{route('permission.index')}}">
                <div><img src="/img/user-check.svg" alt="USERS"></div>
                {{trans('profile_menu.permissions')}}
            </a>
        </li>
        @endpermission
        <li class="nav-item">
            <a class=" {{ (Request::is("*/profile")  ? 'active' : '') }} nav-link  d-flex align-items-center"
               href="{{route('profile')}}">
                <div><img src="/img/my-profile-2.svg" alt="MY PROFILE"></div>
                {{trans('profile_menu.my_profile')}}
            </a>
        </li>
        @permission(['create_user','edit_user','delete_user','close_user'])
            <li class="nav-item">
                <a class="{{ (Request::is("*/profile/users*")  ? 'active' : '') }} nav-link d-flex align-items-center"
                   href="{{route('users.index')}}">
                    <div><img src="/img/uiser-3-w.svg" alt="USERS"></div>
                    {{trans('profile_menu.users')}}
                </a>
            </li>
        @endpermission
        @permission('translate')
            <li class="nav-item">
                <a class="{{ (Request::is("/translations*")  ? 'active' : '') }} nav-link d-flex align-items-center"
                   href="/translations">
                    <div><img src="/img/Translation.svg" alt="translations"></div>
                    {{trans('profile_menu.translations')}}
                </a>
            </li>
        @endpermission
        @permission(['create_article','edit_article','delete_article'])
        <li class="nav-item">
            <a class="{{ (Request::is("*/profile/blogs*")  ? 'active' : '') }} nav-link d-flex align-items-center"
               href="{{route('blogs.index')}}">
                <div><img src="/img/article.svg" alt="article"></div>
                {{trans('profile_menu.blogs')}}
            </a>
        </li>
        @endpermission
        @permission(['create_review','edit_review','delete_review'])
        <li class="nav-item">
            <a class="{{ (Request::is("*/profile/reviews*")  ? 'active' : '') }} nav-link d-flex align-items-center"
               href="{{route('reviews.index')}}">
                <div><img src="/img/Reviews.svg" alt="reviews"></div>
                {{trans('profile_menu.reviews')}}
            </a>
        </li>
        @endpermission
        @permission(['create_resource','edit_resource','delete_resource'])
        <li class="nav-item">
            <a class=" {{ (Request::is("*/profile/resources*")  ? 'active' : '') }} nav-link  d-flex align-items-center"
               href="{{route('resources.index')}}">
                <div><img src="/img/resources.svg" alt="Resources"></div>
                {{trans('profile_menu.resources')}}
            </a>
        </li>
        @endpermission
        @permission('resourceCategory_resources')
            <li class="nav-item">
                <a class="{{ (Request::is("*/profile/resource_category*")  ? 'active' : '') }} nav-link d-flex align-items-center"
                   href="{{route('resource_category.index')}}">
                    <div><img src="/img/article.svg" alt="article"></div>
                    {{trans('profile_menu.resource_category')}}
                </a>
            </li>
        @endpermission
        @permission(['create_pages','edit_pages','delete_pages'])
        <li class="nav-item">
            <a class=" {{ (Request::is("*/profile/pages*")  ? 'active' : '') }} nav-link  d-flex align-items-center"
               href="{{route('pages.index')}}">
                <div><img src="/img/pages.svg" alt="Pages"></div>
                {{trans('profile_menu.pages')}}
            </a>
        </li>
        @endpermission
        @if(Auth::user()->can('subscribtions'))
            <li class="nav-item">
                <a class="{{ (Request::is("*/profile/subscribtion*")  ? 'active' : '') }} nav-link  d-flex align-items-center"
                   href="{{route('subscribtion.index')}}">
                    <div><img src="/img/heart-2.svg" alt="subscribtions"></div>
                    {{trans('profile_menu.subscribtions')}}
                </a>
            </li>
        @endif

        @permission(['create_resume','edit_resume','delete_resume'])
        <li class="nav-item">
            <a class="{{(Request::is("*/profile/resume*")  ? 'active' : '')}} nav-link d-flex align-items-center"
               href="{{route('resume.index')}}">
                <div><img src="/img/resume.svg" alt="RESUMES"></div>
                {{trans('profile_menu.resumes')}}
            </a>
        </li>
        @endpermission
        {{--</li>--}}
        {{--<li class="nav-item">--}}
        {{--<a class="nav-link d-flex align-items-center" href="#">--}}
        {{--<img src="/img/recruitment.svg" alt="RECRUITMENT">--}}
        {{--{{trans('profile_menu.recruitment')}}--}}
        {{--</a>--}}
        {{--</li>--}}
        @if(Auth::user()->is_organization==1)
            <li class="nav-item">
                <a class="{{(Request::is("*/profile/appliers*")  ? 'active' : '')}} nav-link d-flex align-items-center"
                   href="{{route('appliers.index')}}">
                    <div><img src="/img/User.svg" alt="appliers"></div>
                    {{trans('profile_menu.appliers')}}
                </a>
            </li>
        @endif

        @permission(['serviceCategory_service'])
        <li class="nav-item">
            <a class="{{(Request::is("*/profile/service_category*")  ? 'active' : '')}} nav-link d-flex align-items-center"
               href="{{route('service_category.index')}}">
                <div><img src="/img/User.svg" alt="appliers"></div>
                {{trans('profile_menu.service_categories')}}
            </a>
        </li>
        @endpermission


        @permission(['create_service','edit_service','delete_service','all_service'])

        <li class="nav-item">
            <a class="{{(Request::is("*/profile/services*")  ? 'active' : '')}} nav-link d-flex align-items-center"
               href="{{route('services.index')}}">
                <div><img src="/img/User.svg" alt="appliers"></div>
                {{trans('profile_menu.services')}}
            </a>
        </li>

        @endpermission

        @permission(['create_invoice','edit_invoice','delete_invoice','all_invoice'])
        <li class="nav-item">
            <a class="{{(Request::is("*/profile/invoice*")  ? 'active' : '')}} nav-link d-flex align-items-center"
               href="{{route('invoice.index')}}">
                <div><img src="/img/User.svg" alt="appliers"></div>
                {{trans('profile_menu.invoice')}}
            </a>
        </li>
        @endpermission



        {{--@permission(['create_invoice','edit_invoice','delete_invoice','all_invoice'])--}}
        <li class="nav-item">
            <a class="{{(Request::is("*/profile/search/resume*")  ? 'active' : '')}} nav-link d-flex align-items-center"
               href="{{route('searchResume')}}">
                <div><img src="/img/User.svg" alt="appliers"></div>
                {{trans('profile_menu.search_resume')}}
            </a>
        </li>


        {{--@endpermission--}}

        {{--@permission(['create_announcement','edit_announcement','delete_announcement'])--}}
        {{--<li class="nav-item">--}}
            {{--<a class="{{ (Request::is("*/profile/announcements*")  ? 'active' : '') }} nav-link d-flex align-items-center"--}}
               {{--href="{{route('announcements.index')}}">--}}
                {{--<div><img src="/img/megaphone.svg" alt="announcements"></div>--}}
                {{--{{trans('profile_menu.announcements')}}--}}
            {{--</a>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link d-flex align-items-center" data-toggle="collapse" href="#post-an-announcement"--}}
               {{--role="button" aria-expanded="false" aria-controls="POST AN ANNOUNCEMENT">--}}
                {{--<div><img src="/img/Post-announcement.svg" alt="POST AN ANNOUNCEMENT" style="width: 27px">--}}
                {{--</div> {{trans('profile_menu.post_announcement')}}--}}
            {{--</a>--}}
            {{--<div class="collapse" id="post-an-announcement">--}}
                {{--<ul class="nav flex-column menu-nav collapse-ul">--}}
                    {{--@foreach($types as $item)--}}
                        {{--<li class="nav-item"><a class="nav-link"--}}
                                                {{--href="{{route('make-job',$item->key)}}"><span>-</span>{{trans('types.'.$item->key)}}--}}
                            {{--</a></li>--}}
                    {{--@endforeach--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</li>--}}
        {{--@endpermission--}}

    </ul>
</div>


<div class="modal fade profile-img-modal" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                </h4>
            </div>
            <div class="modal-body">
                <div id="upload-demo" class="center-block"></div>
            </div>
            <div class="modal-footer mt-4">
                <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                <button type="button" id="cropImageBtn" class="btn btn-green">Crop</button>
            </div>
        </div>
    </div>
</div>

