@extends('layouts.app')
@section('css')

    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="blue-gradient"></div>
    <div class="page-404 position-relative">
        <img src="/img/404-error.png" alt="" class="img-fluid">
        <div class="error-text">
            <div class="error">
                ERROR 404
            </div>
            <div class="not-found">
                PAGE NOT FOUND
            </div>
            <a href="/" class="btn btn-blue-gradient">BACK TO HOMEPAGE</a>
        </div>

    </div>
@endsection