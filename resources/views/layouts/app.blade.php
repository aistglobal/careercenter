<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! SEO::generate() !!}
    <meta name="application-name" content="careercenter">
    <meta name="theme-color" content="#26c8d7">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">

    <meta name="apple-mobile-web-app-status-bar-style" content="#f89b4d">
    <meta name="msapplication-navbutton-color" content="#f89b4d">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    {{--<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">--}}
    {{--<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">--}}
    {{--<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">--}}
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="manifest" href="/manifest.json">
    <link href="/css/sweetalert.css" rel="stylesheet"/>
    <script src="/js/sweetalert.min.js"></script>

    <title>{{ config('app.name', 'careercenter') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="/css/croppie.css"/>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('css')
    <style>
        .job-left a:visited {
            color: #adadad !important;
        }
    </style>
</head>

<body class="@if(isset($view) && $view=='list') menu-list @endif">
<div class="wrap">

    <!--navigation-->
    <div class="navigation">
        <nav class="navbar navbar-expand-xl my-navbar w-100">
            <a class="navbar-brand" href="{{ url('/') }}" aria-label="CareerCenter"><img src="/img/logo.png"
                                                                                         alt="CareerCenter"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span id="nav-icon3" class="open">
                <span></span><span></span><span></span><span></span>
            </span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav w-100 align-items-center">

                    @if((\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->is_organization==1  ) || (\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement')) )
                        <div class="dropdown nav-user-drop">
                            <a data-toggle="dropdown"
                               class="@if(Request::is('*/jobs')) active @endif nav-link dropdown-toggle d-flex align-items-center"
                               href="#">{{trans('menu.announcements')}}</a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="nav-link"
                                   href="/jobs?status=all">{{trans('menu.all')}}&nbsp{{\App\Job::getCount('all')}}</a>
                                @if(\Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement'))
                                    <a class="nav-link"
                                       href="/jobs?status=expired">{{trans('menu.all-expired')}}
                                        &nbsp{{\App\Job::getCount('expired')}}</a>
                                    <a class="nav-link"
                                       href="/jobs?status=draft">{{trans('menu.all-draft')}}
                                        &nbsp {{\App\Job::getCount('draft')}}</a>
                                    {{--<a class="nav-link"--}}
                                    {{--href="/jobs?status=posted">{{trans('menu.all-posted')}}--}}
                                    {{--&nbsp{{\App\Job::getCount('posted')}}</a>--}}
                                    <a class="nav-link"
                                       href="/jobs?status=pending">{{trans('menu.all-pending')}}
                                        &nbsp{{\App\Job::getCount('pending')}}</a>
                                    <a class="nav-link"
                                       href="/jobs?status=rejected">{{trans('menu.all-rejected')}}
                                        &nbsp{{\App\Job::getCount('rejected')}}</a>
                                @endif
                                @if(\Illuminate\Support\Facades\Auth::user()->is_organization == 1 || \Illuminate\Support\Facades\Auth::user()->organization_id)
                                    @if(\App\Job::getCount('my-expired') != '')

                                        <a class="nav-link"
                                           href="/jobs?status=my-expired">{{trans('menu.my-expired')}}
                                            &nbsp{{\App\Job::getCount('my-expired')}}</a>
                                    @endif

                                    @if(\App\Job::getCount('my-draft') != '')
                                        <a class="nav-link"
                                           href="/jobs?status=my-draft">{{trans('menu.my-draft')}}
                                            &nbsp{{\App\Job::getCount('my-draft')}}</a>
                                    @endif

                                    @if(\App\Job::getCount('my-posted') != '')
                                        <a class="nav-link"
                                           href="/jobs?status=my-posted">{{trans('menu.my-posted')}}
                                            &nbsp{{\App\Job::getCount('my-posted')}}</a>
                                    @endif

                                    @if(!\Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement'))
                                        @if(\App\Job::getCount('my-pending') != '')
                                            <a class="nav-link"
                                               href="/jobs?status=my-pending">{{trans('menu.my-pending')}}
                                                &nbsp{{\App\Job::getCount('my-pending')}}</a>
                                        @endif

                                        @if(\App\Job::getCount('my-rejected') != '')
                                            <a class="nav-link"
                                               href="/jobs?status=my-rejected">{{trans('menu.my-rejected')}}
                                                &nbsp{{\App\Job::getCount('my-rejected')}}</a>
                                        @endif
                                    @endif
                                @endif
                            </div>
                        </div>
                    @elseif(\Illuminate\Support\Facades\Auth::check() && count(Auth::user()->applied_job))
                        <div class="dropdown nav-user-drop">
                            <a data-toggle="dropdown"
                               class="@if(Request::is('*/jobs')) active @endif nav-link dropdown-toggle d-flex align-items-center"
                               href="#">{{trans('menu.announcements')}}</a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="nav-link"
                                   href="{{route('jobs')}}">{{trans('menu.all')}}&nbsp{{\App\Job::getCount('all')}}</a>
                                <a class="nav-link"
                                   href="/jobs?status=applied_job">{{trans('menu.apply_job')}}
                                    &nbsp{{\App\Job::getCount('applied_job')}}
                                </a>
                            </div>
                        </div>
                    @else

                        <li class="nav-item active @if(Request::is('jobs')) active @endif">
                            <a class="nav-link" href="{{route('jobs')}}">{{trans('menu.announcements')}}</a>
                        </li>
                    @endif
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('resume.index')}}">{{trans('menu.resume_builder')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="@if(Request::is('*/links')) active @endif nav-link"
                           href="{{route('resource.index',['c'=>'educational'])}}">{{trans('menu.resources')}}</a>
                    </li>
                    <li class="active  nav-item  mr-0 mr-xl-auto">
                        <a class="@if(Request::is('*/blog')) active @endif nav-link"
                           href="{{route('blog')}}">{{trans('menu.blog')}}</a>
                    </li>
                    @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{trans('login.login')}}</a>
                    </li>
                    <li class="nav-item nav-item-line position-relative">
                        <a class="nav-link"
                           href="{{ route('user.register_form_step_1')}}">{{trans('login.register')}}</a>
                    </li>

                    @else
                        <li class="nav-item">
                            <div class="dropdown nav-user-drop">
                                <a class="nav-link dropdown-toggle d-flex align-items-center" href="#"
                                   title="{{ Auth::user()->username }}" role="button" id="dropdownMenuLink"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if(isset(Auth::user()->image)) <img
                                            src="{{Storage::disk('')->url(Auth::user()->image)}}" alt=""
                                            id="profile-pic">@endif
                                    <span>
                                        {{ Auth::user()->username }}
                                    </span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    @if(Auth::user()->can('profile'))
                                        <a class="nav-link d-flex align-items-center" href="{{route('profile')}}">
                                            <svg class="mr-2" width="18.152" height="18.152"
                                                 viewBox="0 -3 430.20321 430" xmlns="http://www.w3.org/2000/svg">
                                                <path d="m240.898438 88.800781c0 48.988281-39.710938 88.699219-88.699219 88.699219s-88.699219-39.710938-88.699219-88.699219 39.710938-88.699219 88.699219-88.699219 88.699219 39.710938 88.699219 88.699219zm0 0"/>
                                                <path d="m249 269.902344c-27.140625-22.515625-61.335938-34.765625-96.601562-34.601563h-.296876c-41.101562.101563-79.402343 16.101563-107.703124 45.101563-27.199219 28-42.898438 65.699218-44.398438 107l246.300781-.5c-22.542969-36.042969-21.484375-82.039063 2.699219-117zm0 0"/>
                                                <path d="m337.199219 237.402344c-51.363281 0-93 41.636718-93 93 0 51.363281 41.636719 93 93 93s93-41.636719 93-93c.207031-24.730469-9.523438-48.503906-27.011719-65.988282-17.484375-17.488281-41.257812-27.21875-65.988281-27.011718zm44.402343 75.699218-55.902343 45.5c-1.230469 1.035157-2.789063 1.601563-4.398438 1.601563-2.058593.019531-4.011719-.902344-5.300781-2.5l-24.101562-28.101563c-2.511719-2.953124-2.152344-7.386718.800781-9.898437 2.957031-2.515625 7.386719-2.15625 9.902343.796875l19.597657 22.902344 50.601562-41.199219c2.984375-2.429687 7.367188-1.980469 9.800781 1 2.425782 3.019531 1.980469 7.425781-1 9.898437zm0 0"/>
                                            </svg>
                                            {{trans('login.my_profile')}}</a>
                                    @endif
                                    @permission('give_permissions')
                                    <a class="nav-link d-flex align-items-center" href="{{route('permission.index')}}">
                                        <svg class="mr-2" xmlns="http://www.w3.org/2000/svg"
                                             viewBox="0 0 49.65 49.65" width="18.152" height="18.152"
                                             fill="CurrentColor"><title>user-avatar-main-picture</title>
                                            <path d="M24.83,0A24.83,24.83,0,1,0,49.65,24.83,24.86,24.86,0,0,0,24.83,0ZM39.14,38.51v-1c0-3.39-3.91-4.62-6-5.52-.76-.32-2.19-1-3.66-1.72a2,2,0,0,1-1-1.44l-.17-1.61a6.84,6.84,0,0,0,2.31-4.12h.25a.85.85,0,0,0,.83-.66l.4-2.45a.82.82,0,0,0-.84-.85.77.77,0,0,0,0-.15c.05-.29.08-.58.1-.85s.05-.46.06-.7A5.47,5.47,0,0,0,31,14.73a6.41,6.41,0,0,0-1.53-2.23c-1.93-1.83-4.18-2.55-6.1-1.07a4.43,4.43,0,0,0-4,1.68,5.35,5.35,0,0,0-1.16,2.35,7.51,7.51,0,0,0-.26,1.77,6.78,6.78,0,0,0,.18,1.94.83.83,0,0,0-.77.84l.4,2.45a.84.84,0,0,0,.83.66h.23a8.2,8.2,0,0,0,2.32,4.2L21,28.87a2,2,0,0,1-1,1.44c-1.42.69-2.82,1.36-3.65,1.7-2,.82-6,2.13-6,5.52v.77a19.83,19.83,0,1,1,28.82.21Z"
                                                  transform="translate(0 0)"/>
                                        </svg>
                                        {{trans('profile_menu.permissions')}}</a>
                                    @endpermission

                                    @permission(['create_user','edit_user','delete_user','close_user'])
                                    <a class="nav-link d-flex align-items-center" href="{{route('users.index')}}">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                             class="mr-2" width="18.152" height="18.152" viewBox="0 0 80.13 80.13">
                                            <g>
                                                <path d="M48.355,17.922c3.705,2.323,6.303,6.254,6.776,10.817c1.511,0.706,3.188,1.112,4.966,1.112 c6.491,0,11.752-5.261,11.752-11.751c0-6.491-5.261-11.752-11.752-11.752C53.668,6.35,48.453,11.517,48.355,17.922z M40.656,41.984
		c6.491,0,11.752-5.262,11.752-11.752s-5.262-11.751-11.752-11.751c-6.49,0-11.754,5.262-11.754,11.752S34.166,41.984,40.656,41.984
		z M45.641,42.785h-9.972c-8.297,0-15.047,6.751-15.047,15.048v12.195l0.031,0.191l0.84,0.263
		c7.918,2.474,14.797,3.299,20.459,3.299c11.059,0,17.469-3.153,17.864-3.354l0.785-0.397h0.084V57.833
		C60.688,49.536,53.938,42.785,45.641,42.785z M65.084,30.653h-9.895c-0.107,3.959-1.797,7.524-4.47,10.088
		c7.375,2.193,12.771,9.032,12.771,17.11v3.758c9.77-0.358,15.4-3.127,15.771-3.313l0.785-0.398h0.084V45.699
		C80.13,37.403,73.38,30.653,65.084,30.653z M20.035,29.853c2.299,0,4.438-0.671,6.25-1.814c0.576-3.757,2.59-7.04,5.467-9.276
		c0.012-0.22,0.033-0.438,0.033-0.66c0-6.491-5.262-11.752-11.75-11.752c-6.492,0-11.752,5.261-11.752,11.752
		C8.283,24.591,13.543,29.853,20.035,29.853z M30.589,40.741c-2.66-2.551-4.344-6.097-4.467-10.032
		c-0.367-0.027-0.73-0.056-1.104-0.056h-9.971C6.75,30.653,0,37.403,0,45.699v12.197l0.031,0.188l0.84,0.265
		c6.352,1.983,12.021,2.897,16.945,3.185v-3.683C17.818,49.773,23.212,42.936,30.589,40.741z"/>
                                            </g>
                                        </svg>
                                        {{trans('profile_menu.users')}}
                                    </a>
                                    @endpermission
                                    @permission('translate')
                                    <a class="nav-link d-flex align-items-center" href="/translations">
                                        <svg version="1.1" class="mr-2" xmlns="http://www.w3.org/2000/svg" x="0px"
                                             y="0px"
                                             viewBox="0 0 512 512" fill="CurrentColor" width="18.152"
                                             height="18.152">
                                            <g>
                                                <g>
                                                    <polygon
                                                            points="138.71,137 132.29,137 120.293,197 150.707,197"/>
                                                </g>
                                            </g>
                                            <g>
                                                <g>
                                                    <path d="M381.374,257c6.477,17.399,15.092,31.483,24.626,43.467c9.534-11.984,19.149-26.069,25.626-43.467H381.374z"/>
                                                </g>
                                            </g>
                                            <g>
                                                <g>
                                                    <path d="M467,91H280.717l38.842,311.679c0.687,12.748-2.798,24.75-11.118,34.146L242.663,512H467c24.814,0,45-20.186,45-45V137
			C512,112.186,491.814,91,467,91z M467,257h-4.006c-8.535,27.383-22.07,48.81-36.136,65.702
			c11.019,10.074,22.802,18.338,34.517,27.594c6.46,5.171,7.515,14.604,2.329,21.079c-5.162,6.465-14.632,7.513-21.079,2.329
			c-12.729-10.047-24.677-18.457-36.625-29.421c-11.948,10.964-22.896,19.374-35.625,29.421c-6.447,5.184-15.917,4.136-21.079-2.329
			c-5.186-6.475-4.131-15.908,2.329-21.079c11.715-9.256,22.498-17.52,33.517-27.594c-14.066-16.891-26.602-38.318-35.136-65.702
			H346c-8.291,0-15-6.709-15-15s6.709-15,15-15h45v-15c0-8.291,6.709-15,15-15c8.291,0,15,6.709,15,15v15h46c8.291,0,15,6.709,15,15
			S475.291,257,467,257z"/>
                                                </g>
                                            </g>
                                            <g>
                                                <g>
                                                    <path d="M244.164,39.419C241.366,16.948,222.162,0,199.516,0H45C20.186,0,0,20.186,0,45v332c0,24.814,20.186,45,45,45
			c89.67,0,154.177,0,236.551,0c4.376-5.002,8.044-8.134,8.199-14.663C289.788,405.7,244.367,41.043,244.164,39.419z
			 M183.944,286.707c-7.954,1.637-16.011-3.527-17.651-11.763L156.706,227h-42.411l-9.587,47.944
			c-1.611,8.115-9.434,13.447-17.651,11.763c-8.115-1.626-13.389-9.521-11.763-17.651l29.999-150
			C106.699,112.054,112.852,107,120,107h31c7.148,0,13.301,5.054,14.707,12.056l30,150
			C197.333,277.186,192.06,285.081,183.944,286.707z"/>
                                                </g>
                                            </g>
                                            <g>
                                                <g>
                                                    <path d="M175.261,452l2.574,20.581c1.716,13.783,10.874,27.838,25.938,34.856c28.428-31.294,11.229-12.362,50.359-55.437H175.261z
			"/>
                                                </g>
                                            </g>

                                        </svg>
                                        {{trans('profile_menu.translations')}}
                                    </a>
                                    @endpermission
                                    @permission(['create_article','edit_article','delete_article'])
                                    <a class="nav-link d-flex align-items-center" href="{{route('blogs.index')}}">
                                        <svg class="mr-2" fill="CurrentColor" width="18.152" height="18.152"
                                             viewBox="0 -30 512 512" xmlns="http://www.w3.org/2000/svg">
                                            <path d="m451.875 122h60.125v210h-60.125zm0 0"/>
                                            <path d="m436.84375 2c-41.441406 0-75.15625 33.644531-75.15625 75v15h150.3125v-15c0-41.355469-33.714844-75-75.15625-75zm0 0"/>
                                            <path d="m424.335938 445.320312c5.929687 8.878907 19.0625 8.914063 25.015624 0l55.660157-83.320312h-136.335938zm0 0"/>
                                            <path d="m361.6875 122h60.125v210h-60.125zm0 0"/>
                                            <path d="m211.441406 105v-105h-196.410156c-8.300781 0-15.03125 6.714844-15.03125 15v422c0 8.285156 6.730469 15 15.03125 15h300.625c8.285156 0 15.007812-6.6875 15.03125-14.953125l.957031-317.046875h-105.171875c-8.300781 0-15.03125-6.714844-15.03125-15zm45.09375 286h-180.375c-8.300781 0-15.03125-6.714844-15.03125-15s6.730469-15 15.03125-15h180.375c8.300782 0 15.03125 6.714844 15.03125 15s-6.730468 15-15.03125 15zm0-60h-180.375c-8.300781 0-15.03125-6.714844-15.03125-15s6.730469-15 15.03125-15h180.375c8.300782 0 15.03125 6.714844 15.03125 15s-6.730468 15-15.03125 15zm0-60h-180.375c-8.300781 0-15.03125-6.714844-15.03125-15s6.730469-15 15.03125-15h180.375c8.300782 0 15.03125 6.714844 15.03125 15s-6.730468 15-15.03125 15zm15.03125-75c0 8.285156-6.730468 15-15.03125 15h-180.375c-8.300781 0-15.03125-6.714844-15.03125-15s6.730469-15 15.03125-15h180.375c8.300782 0 15.03125 6.714844 15.03125 15zm0 0"/>
                                            <path d="m241.503906 8.785156v81.214844h81.382813zm0 0"/>
                                        </svg>
                                        {{trans('profile_menu.blogs')}}
                                    </a>
                                    @endpermission
                                    @permission(['create_review','edit_review','delete_review'])
                                    <a class="nav-link d-flex align-items-center" href="{{route('reviews.index')}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22.63 22.63"
                                             class="mr-2" fill="CurrentColor" width="18.152" height="18.152">
                                            <g id="help" transform="translate(71.459 -143.541)">
                                                <path id="Path_2470" data-name="Path 2470"
                                                      d="M212.528,205.213a7.337,7.337,0,0,0-4.024-6.533,9.978,9.978,0,0,1-9.825,9.825,7.317,7.317,0,0,0,10.256,3.007l3.561.985-.985-3.561A7.289,7.289,0,0,0,212.528,205.213Zm0,0"
                                                      transform="translate(-261.357 -46.357)" fill="CurrentColor"/>
                                                <path id="Path_2471" data-name="Path 2471"
                                                      d="M17.282,8.641A8.641,8.641,0,1,0,1.2,13.033L.032,17.25l4.217-1.166A8.643,8.643,0,0,0,17.282,8.641Z"
                                                      transform="translate(-71.459 143.541)" fill="CurrentColor"/>
                                            </g>
                                        </svg>
                                        {{trans('profile_menu.reviews')}}
                                    </a>
                                    @endpermission
                                    @permission(['create_resource','edit_resource','delete_resource'])
                                    <a class="nav-link d-flex align-items-center" href="{{route('resources.index')}}">
                                        <svg viewBox="0 0 512.001 512" class="mr-2" fill="CurrentColor" width="18.152"
                                             height="18.152" xmlns="http://www.w3.org/2000/svg">
                                            <path d="m0 448.359375 63.640625 63.640625 68.839844-68.832031-63.640625-63.648438zm0 0"/>
                                            <path d="m166.902344 69.464844c17.929687-17.925782 38.789062-32.003906 62.003906-41.839844l-11.703125-27.625c-26.785156 11.347656-50.84375 27.582031-71.515625 48.253906s-36.90625 44.730469-48.253906 71.515625l27.625 11.703125c9.835937-23.214844 23.914062-44.074218 41.84375-62.007812zm0 0"/>
                                            <path d="m484.378906 283.09375c-9.835937 23.214844-23.914062 44.074219-41.84375 62.007812-17.929687 17.925782-38.792968 32.003907-62.003906 41.839844l11.703125 27.625c26.78125-11.347656 50.84375-27.582031 71.515625-48.253906s36.902344-44.730469 48.25-71.515625zm0 0"/>
                                            <path d="m199.128906 334.089844c64.84375 54.074218 161.558594 50.550781 222.261719-10.132813 64.328125-64.339843 64.328125-169.007812 0-233.347656-64.332031-64.328125-169.011719-64.328125-233.339844 0-60.644531 60.644531-64.238281 157.382813-10.132812 222.257813l-17.296875 17.300781-21.210938-21.210938-49.359375 49.351563 63.640625 63.640625 49.359375-49.351563-21.21875-21.214844 17.296875-17.296874zm45.589844-66.8125c0-33.078125 26.910156-60 60-60-24.808594 0-45-20.179688-45-45 0-24.808594 20.191406-45 45-45 24.8125 0 45 20.191406 45 45 0 24.820312-20.1875 45-45 45 33.082031 0 60 26.921875 60 60v30h-120zm0 0"/>
                                            <path d="m304.71875 237.277344c-16.539062 0-30 13.460937-30 30h60c0-16.539063-13.460938-30-30-30zm0 0"/>
                                            <path d="m304.71875 147.277344c-8.269531 0-15 6.730468-15 15 0 8.28125 6.730469 15 15 15s15-6.71875 15-15c0-8.269532-6.730469-15-15-15zm0 0"/>
                                        </svg>
                                        {{trans('profile_menu.resources')}}
                                    </a>
                                    @endpermission
                                    @permission('resourceCategory_resources')
                                    <a class="nav-link d-flex align-items-center"
                                       href="{{route('resource_category.index')}}">
                                        <svg viewBox="0 0 512.001 512" class="mr-2" fill="CurrentColor" width="18.152"
                                             height="18.152" xmlns="http://www.w3.org/2000/svg">
                                            <path d="m0 448.359375 63.640625 63.640625 68.839844-68.832031-63.640625-63.648438zm0 0"/>
                                            <path d="m166.902344 69.464844c17.929687-17.925782 38.789062-32.003906 62.003906-41.839844l-11.703125-27.625c-26.785156 11.347656-50.84375 27.582031-71.515625 48.253906s-36.90625 44.730469-48.253906 71.515625l27.625 11.703125c9.835937-23.214844 23.914062-44.074218 41.84375-62.007812zm0 0"/>
                                            <path d="m484.378906 283.09375c-9.835937 23.214844-23.914062 44.074219-41.84375 62.007812-17.929687 17.925782-38.792968 32.003907-62.003906 41.839844l11.703125 27.625c26.78125-11.347656 50.84375-27.582031 71.515625-48.253906s36.902344-44.730469 48.25-71.515625zm0 0"/>
                                            <path d="m199.128906 334.089844c64.84375 54.074218 161.558594 50.550781 222.261719-10.132813 64.328125-64.339843 64.328125-169.007812 0-233.347656-64.332031-64.328125-169.011719-64.328125-233.339844 0-60.644531 60.644531-64.238281 157.382813-10.132812 222.257813l-17.296875 17.300781-21.210938-21.210938-49.359375 49.351563 63.640625 63.640625 49.359375-49.351563-21.21875-21.214844 17.296875-17.296874zm45.589844-66.8125c0-33.078125 26.910156-60 60-60-24.808594 0-45-20.179688-45-45 0-24.808594 20.191406-45 45-45 24.8125 0 45 20.191406 45 45 0 24.820312-20.1875 45-45 45 33.082031 0 60 26.921875 60 60v30h-120zm0 0"/>
                                            <path d="m304.71875 237.277344c-16.539062 0-30 13.460937-30 30h60c0-16.539063-13.460938-30-30-30zm0 0"/>
                                            <path d="m304.71875 147.277344c-8.269531 0-15 6.730468-15 15 0 8.28125 6.730469 15 15 15s15-6.71875 15-15c0-8.269532-6.730469-15-15-15zm0 0"/>
                                        </svg>
                                        {{trans('profile_menu.resource_category')}}
                                    </a>
                                    @endpermission

                                    @permission(['create_pages','edit_pages','delete_pages'])
                                    <a class="nav-link d-flex align-items-center" href="{{route('pages.index')}}">
                                        <svg class="mr-2" fill="CurrentColor" xmlns="http://www.w3.org/2000/svg"
                                             width="18.152" height="18.152" viewBox="0 0 51 35.54">
                                            <g id="Group_5568" data-name="Group 5568">
                                                <rect id="Rectangle_132" data-name="Rectangle 132" x="4.54"
                                                      y="16.04" width="41.93" height="18.04"/>
                                                <g id="laptop">
                                                    <path id="_Compound_Path_" data-name="&lt;Compound Path&gt;"
                                                          d="M51,12.18H0V35.54H51ZM16.84,28.06a1.78,1.78,0,0,1-1.92,3h0l-7-4.44a1.79,1.79,0,0,1-.55-2.47,1.77,1.77,0,0,1,.55-.54l7-4.45a1.79,1.79,0,0,1,1.92,3l-4.62,2.94Zm12.48-9.18L25.05,31.07a1.79,1.79,0,0,1-3.37-1.18L26,17.7a1.78,1.78,0,1,1,3.36,1.18Zm13.74,7.74-7,4.44a1.78,1.78,0,1,1-1.92-3l4.62-2.95-4.62-2.94a1.79,1.79,0,0,1,1.92-3l7,4.45a1.77,1.77,0,0,1,0,3Z"
                                                          transform="translate(0 0)"/>
                                                    <path id="_Compound_Path_2" data-name="&lt;Compound Path&gt;"
                                                          d="M51,2.57A2.57,2.57,0,0,0,48.43,0H2.57A2.57,2.57,0,0,0,0,2.57H0v5.7H51ZM6.37,6.3a1.9,1.9,0,1,1,1.9-1.9,1.9,1.9,0,0,1-1.9,1.9ZM13,6.3a1.9,1.9,0,1,1,1.9-1.9A1.9,1.9,0,0,1,13,6.3Zm31.61,0a1.9,1.9,0,1,1,1.9-1.9,1.9,1.9,0,0,1-1.9,1.9Z"
                                                          transform="translate(0 0)"/>
                                                </g>
                                            </g>
                                        </svg>
                                        {{trans('profile_menu.pages')}}
                                    </a>
                                    @endpermission
                                    @if(Auth::user()->can('subscribtions'))
                                        <a class="nav-link d-flex align-items-center"
                                           href="{{route('subscribtion.index')}}">
                                            <svg version="1.1" class="mr-2" fill="CurrentColor"
                                                 xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                 viewBox="0 0 299.995 299.995" width="18.152" height="18.152">
                                                <g>
                                                    <g>
                                                        <g>
                                                            <path d="M165.45,108.207l-9.788,9.786c-1.945,1.943-4.658,2.638-7.158,2.101c-1.541-0.241-3.009-0.936-4.173-2.103l-9.783-9.783				c-6.549-6.546-15.149-9.822-23.749-9.822c-8.6,0-17.201,3.273-23.749,9.822c-12.82,12.823-12.698,33.374,0.283,47.805				c13.232,14.708,49.605,45.027,62.666,55.788c13.061-10.758,49.442-41.085,62.677-55.788c12.978-14.428,13.1-34.982,0.28-47.805				C199.859,95.115,178.55,95.112,165.45,108.207z"/>
                                                            <path d="M149.995,0C67.156,0,0,67.158,0,149.995s67.156,150,149.995,150s150-67.163,150-150S232.834,0,149.995,0z				 M224.239,166.417c-16.98,18.866-67.049,59.619-69.174,61.343c-1.489,1.211-3.286,1.826-5.07,1.74				c-0.054,0.003-0.112,0.003-0.169,0.003c-1.725,0-3.46-0.571-4.902-1.743c-2.124-1.725-52.188-42.478-69.163-61.341				c-18.612-20.689-18.49-50.445,0.283-69.218c19.161-19.164,50.344-19.164,69.506,0l4.448,4.448l4.451-4.451				c19.161-19.161,50.341-19.159,69.508,0C242.726,115.975,242.848,145.731,224.239,166.417z"/>
                                                        </g>
                                                    </g>
                                                </g>
                                                <g></g>
                                            </svg>
                                            {{trans('profile_menu.subscribtions')}}
                                        </a>
                                    @endif
                                    @permission(['create_resume','edit_resume','delete_resume'])
                                    <a class="nav-link d-flex align-items-center" href="{{route('resume.index')}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="mr-2" fill="CurrentColor"
                                             width="18.152" height="18.152" viewBox="0 0 20.25 23">
                                            <g id="resume" transform="translate(-37.125 0)">
                                                <path id="Path_2464" data-name="Path 2464"
                                                      d="M83.329,48.833l.437-.134a.61.61,0,0,1,.752.374l.591,1.622.591-1.622a.61.61,0,0,1,.752-.374l.437.134a1.79,1.79,0,0,1,1.2,1.423V45.446A.446.446,0,0,0,87.647,45H82.571a.446.446,0,0,0-.446.446v4.809A1.79,1.79,0,0,1,83.329,48.833Zm1.78-3.185a1.438,1.438,0,1,1-1.438,1.438A1.439,1.439,0,0,1,85.109,45.648Z"
                                                      transform="translate(-40.589 -41.515)" fill="CurrentColor"/>
                                                <path id="Path_2465" data-name="Path 2465"
                                                      d="M132.568,110.3l-.659,1.81h.915v-1.333A.572.572,0,0,0,132.568,110.3Z"
                                                      transform="translate(-86.096 -101.755)" fill="CurrentColor"/>
                                                <path id="Path_2466" data-name="Path 2466"
                                                      d="M97.594,110.774v1.333h.915l-.66-1.81A.572.572,0,0,0,97.594,110.774Z"
                                                      transform="translate(-54.926 -101.755)" fill="CurrentColor"/>
                                                <path id="Path_2467" data-name="Path 2467"
                                                      d="M56.42,0H38.08a.892.892,0,0,0-.955.813V22.187A.892.892,0,0,0,38.08,23H56.42a.892.892,0,0,0,.955-.813V.813A.892.892,0,0,0,56.42,0ZM39.784,2.875a.669.669,0,0,1,.716-.61h8.437a.669.669,0,0,1,.716.61v8.086a.669.669,0,0,1-.716.61H40.5a.669.669,0,0,1-.716-.61ZM54,19.3H40.5a.618.618,0,1,1,0-1.22H54a.618.618,0,1,1,0,1.22Zm0-4.313H40.5a.618.618,0,1,1,0-1.22H54a.618.618,0,1,1,0,1.22Zm0-5.75H52.312a.669.669,0,0,1-.716-.61.669.669,0,0,1,.716-.61H54a.669.669,0,0,1,.716.61A.669.669,0,0,1,54,9.235ZM54,6.36H52.312a.669.669,0,0,1-.716-.61.669.669,0,0,1,.716-.61H54a.669.669,0,0,1,.716.61A.669.669,0,0,1,54,6.36Z"
                                                      fill="CurrentColor"/>
                                            </g>
                                        </svg>
                                        {{trans('profile_menu.resumes')}}
                                    </a>
                                    @endpermission

                                    @if(Auth::user()->is_organization==1)
                                        <a class="nav-link d-flex align-items-center"
                                           href="{{route('appliers.index')}}">
                                            <svg class="mr-2" fill="CurrentColor" version="1.1"
                                                 xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                 viewBox="0 0 350 350" width="18.152" height="18.152">
                                                <g>
                                                    <path d="M175,171.173c38.914,0,70.463-38.318,70.463-85.586C245.463,38.318,235.105,0,175,0s-70.465,38.318-70.465,85.587		C104.535,132.855,136.084,171.173,175,171.173z"/>
                                                    <path d="M41.909,301.853C41.897,298.971,41.885,301.041,41.909,301.853L41.909,301.853z"/>
                                                    <path d="M308.085,304.104C308.123,303.315,308.098,298.63,308.085,304.104L308.085,304.104z"/>
                                                    <path d="M307.935,298.397c-1.305-82.342-12.059-105.805-94.352-120.657c0,0-11.584,14.761-38.584,14.761		s-38.586-14.761-38.586-14.761c-81.395,14.69-92.803,37.805-94.303,117.982c-0.123,6.547-0.18,6.891-0.202,6.131		c0.005,1.424,0.011,4.058,0.011,8.651c0,0,19.592,39.496,133.08,39.496c113.486,0,133.08-39.496,133.08-39.496		c0-2.951,0.002-5.003,0.005-6.399C308.062,304.575,308.018,303.664,307.935,298.397z"/>
                                                </g>
                                                <g></g>
                                            </svg>
                                            {{trans('profile_menu.appliers')}}
                                        </a>
                                    @endif


                                        @permission(['serviceCategory_service'])
                                        <a class="nav-link d-flex align-items-center" href="{{route('service_category.index')}}">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="mr-2" fill="CurrentColor"
                                                 width="18.152" height="18.152" viewBox="0 0 20.25 23">
                                                <g id="resume" transform="translate(-37.125 0)">
                                                    <path id="Path_2464" data-name="Path 2464"
                                                          d="M83.329,48.833l.437-.134a.61.61,0,0,1,.752.374l.591,1.622.591-1.622a.61.61,0,0,1,.752-.374l.437.134a1.79,1.79,0,0,1,1.2,1.423V45.446A.446.446,0,0,0,87.647,45H82.571a.446.446,0,0,0-.446.446v4.809A1.79,1.79,0,0,1,83.329,48.833Zm1.78-3.185a1.438,1.438,0,1,1-1.438,1.438A1.439,1.439,0,0,1,85.109,45.648Z"
                                                          transform="translate(-40.589 -41.515)" fill="CurrentColor"/>
                                                    <path id="Path_2465" data-name="Path 2465"
                                                          d="M132.568,110.3l-.659,1.81h.915v-1.333A.572.572,0,0,0,132.568,110.3Z"
                                                          transform="translate(-86.096 -101.755)" fill="CurrentColor"/>
                                                    <path id="Path_2466" data-name="Path 2466"
                                                          d="M97.594,110.774v1.333h.915l-.66-1.81A.572.572,0,0,0,97.594,110.774Z"
                                                          transform="translate(-54.926 -101.755)" fill="CurrentColor"/>
                                                    <path id="Path_2467" data-name="Path 2467"
                                                          d="M56.42,0H38.08a.892.892,0,0,0-.955.813V22.187A.892.892,0,0,0,38.08,23H56.42a.892.892,0,0,0,.955-.813V.813A.892.892,0,0,0,56.42,0ZM39.784,2.875a.669.669,0,0,1,.716-.61h8.437a.669.669,0,0,1,.716.61v8.086a.669.669,0,0,1-.716.61H40.5a.669.669,0,0,1-.716-.61ZM54,19.3H40.5a.618.618,0,1,1,0-1.22H54a.618.618,0,1,1,0,1.22Zm0-4.313H40.5a.618.618,0,1,1,0-1.22H54a.618.618,0,1,1,0,1.22Zm0-5.75H52.312a.669.669,0,0,1-.716-.61.669.669,0,0,1,.716-.61H54a.669.669,0,0,1,.716.61A.669.669,0,0,1,54,9.235ZM54,6.36H52.312a.669.669,0,0,1-.716-.61.669.669,0,0,1,.716-.61H54a.669.669,0,0,1,.716.61A.669.669,0,0,1,54,6.36Z"
                                                          fill="CurrentColor"/>
                                                </g>
                                            </svg>
                                            {{trans('profile_menu.service_categories')}}
                                        </a>
                                        @endpermission



                                        @permission(['create_service','edit_service','delete_service','all_service'])
                                        <a class="nav-link d-flex align-items-center" href="{{route('services.index')}}">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="mr-2" fill="CurrentColor"
                                                 width="18.152" height="18.152" viewBox="0 0 20.25 23">
                                                <g id="resume" transform="translate(-37.125 0)">
                                                    <path id="Path_2464" data-name="Path 2464"
                                                          d="M83.329,48.833l.437-.134a.61.61,0,0,1,.752.374l.591,1.622.591-1.622a.61.61,0,0,1,.752-.374l.437.134a1.79,1.79,0,0,1,1.2,1.423V45.446A.446.446,0,0,0,87.647,45H82.571a.446.446,0,0,0-.446.446v4.809A1.79,1.79,0,0,1,83.329,48.833Zm1.78-3.185a1.438,1.438,0,1,1-1.438,1.438A1.439,1.439,0,0,1,85.109,45.648Z"
                                                          transform="translate(-40.589 -41.515)" fill="CurrentColor"/>
                                                    <path id="Path_2465" data-name="Path 2465"
                                                          d="M132.568,110.3l-.659,1.81h.915v-1.333A.572.572,0,0,0,132.568,110.3Z"
                                                          transform="translate(-86.096 -101.755)" fill="CurrentColor"/>
                                                    <path id="Path_2466" data-name="Path 2466"
                                                          d="M97.594,110.774v1.333h.915l-.66-1.81A.572.572,0,0,0,97.594,110.774Z"
                                                          transform="translate(-54.926 -101.755)" fill="CurrentColor"/>
                                                    <path id="Path_2467" data-name="Path 2467"
                                                          d="M56.42,0H38.08a.892.892,0,0,0-.955.813V22.187A.892.892,0,0,0,38.08,23H56.42a.892.892,0,0,0,.955-.813V.813A.892.892,0,0,0,56.42,0ZM39.784,2.875a.669.669,0,0,1,.716-.61h8.437a.669.669,0,0,1,.716.61v8.086a.669.669,0,0,1-.716.61H40.5a.669.669,0,0,1-.716-.61ZM54,19.3H40.5a.618.618,0,1,1,0-1.22H54a.618.618,0,1,1,0,1.22Zm0-4.313H40.5a.618.618,0,1,1,0-1.22H54a.618.618,0,1,1,0,1.22Zm0-5.75H52.312a.669.669,0,0,1-.716-.61.669.669,0,0,1,.716-.61H54a.669.669,0,0,1,.716.61A.669.669,0,0,1,54,9.235ZM54,6.36H52.312a.669.669,0,0,1-.716-.61.669.669,0,0,1,.716-.61H54a.669.669,0,0,1,.716.61A.669.669,0,0,1,54,6.36Z"
                                                          fill="CurrentColor"/>
                                                </g>
                                            </svg>
                                            {{trans('profile_menu.services')}}
                                        </a>
                                        @endpermission



                                        @permission(['create_invoice','edit_invoice','delete_invoice'])
                                        <a class="nav-link d-flex align-items-center" href="{{route('invoice.index')}}">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="mr-2" fill="CurrentColor"
                                                 width="18.152" height="18.152" viewBox="0 0 20.25 23">
                                                <g id="resume" transform="translate(-37.125 0)">
                                                    <path id="Path_2464" data-name="Path 2464"
                                                          d="M83.329,48.833l.437-.134a.61.61,0,0,1,.752.374l.591,1.622.591-1.622a.61.61,0,0,1,.752-.374l.437.134a1.79,1.79,0,0,1,1.2,1.423V45.446A.446.446,0,0,0,87.647,45H82.571a.446.446,0,0,0-.446.446v4.809A1.79,1.79,0,0,1,83.329,48.833Zm1.78-3.185a1.438,1.438,0,1,1-1.438,1.438A1.439,1.439,0,0,1,85.109,45.648Z"
                                                          transform="translate(-40.589 -41.515)" fill="CurrentColor"/>
                                                    <path id="Path_2465" data-name="Path 2465"
                                                          d="M132.568,110.3l-.659,1.81h.915v-1.333A.572.572,0,0,0,132.568,110.3Z"
                                                          transform="translate(-86.096 -101.755)" fill="CurrentColor"/>
                                                    <path id="Path_2466" data-name="Path 2466"
                                                          d="M97.594,110.774v1.333h.915l-.66-1.81A.572.572,0,0,0,97.594,110.774Z"
                                                          transform="translate(-54.926 -101.755)" fill="CurrentColor"/>
                                                    <path id="Path_2467" data-name="Path 2467"
                                                          d="M56.42,0H38.08a.892.892,0,0,0-.955.813V22.187A.892.892,0,0,0,38.08,23H56.42a.892.892,0,0,0,.955-.813V.813A.892.892,0,0,0,56.42,0ZM39.784,2.875a.669.669,0,0,1,.716-.61h8.437a.669.669,0,0,1,.716.61v8.086a.669.669,0,0,1-.716.61H40.5a.669.669,0,0,1-.716-.61ZM54,19.3H40.5a.618.618,0,1,1,0-1.22H54a.618.618,0,1,1,0,1.22Zm0-4.313H40.5a.618.618,0,1,1,0-1.22H54a.618.618,0,1,1,0,1.22Zm0-5.75H52.312a.669.669,0,0,1-.716-.61.669.669,0,0,1,.716-.61H54a.669.669,0,0,1,.716.61A.669.669,0,0,1,54,9.235ZM54,6.36H52.312a.669.669,0,0,1-.716-.61.669.669,0,0,1,.716-.61H54a.669.669,0,0,1,.716.61A.669.669,0,0,1,54,6.36Z"
                                                          fill="CurrentColor"/>
                                                </g>
                                            </svg>
                                            {{trans('profile_menu.invoice')}}
                                        </a>
                                        @endpermission

                                    {{--@permission(['create_announcement','edit_announcement','delete_announcement'])--}}
                                    {{--<a class="nav-link d-flex align-items-center"--}}
                                       {{--href="{{route('announcements.index')}}">--}}
                                        {{--<svg class="mr-2" fill="CurrentColor" version="1.1"--}}
                                             {{--xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"--}}
                                             {{--viewBox="0 0 512.032 512.032" width="18.152" height="18.152">--}}
                                            {{--<g>--}}
                                                {{--<g>--}}
                                                    {{--<path d="M256.016,321.632V126.368c-5.536,0.512-11.008,1.376-16.576,1.632H80.016c-8.832,0-16,7.168-16,16v16h-48			c-8.832,0-16,7.168-16,16v96c0,8.832,7.168,16,16,16h48v16c0,8.832,7.168,16,16,16h9.696l64.32,171.456			c3.936,10.272,12.736,17.728,23.552,19.904c2.144,0.448,4.288,0.672,6.4,0.672c8.544,0,16.672-3.392,22.496-9.472l38.432-38.112			c9.824-9.824,12.16-25.152,5.888-37.024L197.104,320H239.6C245.104,320.256,250.544,321.12,256.016,321.632z"/>--}}
                                                {{--</g>--}}
                                            {{--</g>--}}
                                            {{--<g>--}}
                                                {{--<g>--}}
                                                    {{--<path d="M509.392,7.744c-0.32-0.544-0.608-0.992-1.024-1.504c-1.76-2.24-3.968-4.064-6.688-5.12			c-0.096-0.032-0.16-0.16-0.256-0.192c-0.32-0.096-0.608,0.064-0.928-0.032C499.056,0.48,497.616,0,496.016,0			c-1.088,0-2.048,0.416-3.104,0.64c-0.608,0.128-1.184,0.16-1.792,0.352c-2.752,0.896-5.12,2.432-7.008,4.544			c-0.128,0.128-0.32,0.16-0.448,0.288C433.776,66.4,364.208,106.912,288.016,121.76v204.512			c76.192,14.848,145.76,55.328,195.648,115.904c0.128,0.16,0.352,0.224,0.512,0.416c1.12,1.248,2.496,2.24,3.968,3.072			c0.48,0.288,0.864,0.704,1.344,0.928c2.016,0.864,4.192,1.408,6.528,1.408c1.824,0,3.616-0.32,5.408-0.928			c0.096-0.032,0.16-0.16,0.256-0.192c2.72-1.056,4.928-2.848,6.688-5.12c0.384-0.512,0.672-0.96,1.024-1.504			c1.536-2.464,2.624-5.184,2.624-8.256V16C512.016,12.928,510.928,10.208,509.392,7.744z"/>--}}
                                                {{--</g>--}}
                                            {{--</g>--}}
                                        {{--</svg>--}}
                                        {{--{{trans('profile_menu.announcements')}}--}}
                                    {{--</a>--}}

                                    {{--@endpermission--}}
                                    <a class="nav-link" href="{{ route('logout') }}"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <svg version="1.1" class="mr-2" fill="CurrentColor" width="18.152"
                                             height="18.152" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                             viewBox="0 0 297 297">
                                            <g>
                                                <g>
                                                    <g>
                                                        <path d="M155,6.5c-30.147,0-58.95,9.335-83.294,26.995c-2.789,2.023-3.547,5.853-1.739,8.787L92.83,79.374				c0.962,1.559,2.53,2.649,4.328,3.004c1.796,0.354,3.661-0.054,5.145-1.129c14.23-10.323,31.069-15.78,48.698-15.78				c45.783,0,83.03,37.247,83.03,83.03c0,45.783-37.247,83.03-83.03,83.03c-17.629,0-34.468-5.456-48.698-15.78				c-1.484-1.076-3.349-1.486-5.145-1.129c-1.798,0.355-3.366,1.444-4.328,3.004l-22.863,37.093				c-1.808,2.934-1.05,6.763,1.739,8.787C96.05,281.165,124.853,290.5,155,290.5c78.299,0,142-63.701,142-142S233.299,6.5,155,6.5z"/>
                                                        <path d="M90.401,201.757c1.147-2.142,1.021-4.74-0.326-6.76l-15.463-23.195h93.566c12.849,0,23.302-10.453,23.302-23.302				s-10.453-23.302-23.302-23.302H74.612l15.463-23.195c1.348-2.02,1.473-4.618,0.326-6.76c-1.146-2.141-3.377-3.478-5.806-3.478				H40.019c-2.201,0-4.258,1.1-5.479,2.933L1.106,144.847c-1.475,2.212-1.475,5.093,0,7.306l33.433,50.149				c1.221,1.832,3.278,2.933,5.479,2.933h44.577C87.025,205.235,89.256,203.898,90.401,201.757z"/>
                                                    </g>
                                                </g>
                                            </g>
                                            <g></g>
                                        </svg>{{trans('login.logout')}}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>

                                </div>
                            </div>

                        </li>
                        @endguest
                        @guest
                        <li class="nav-item dropdown">
                            <a class="nav-link nav-btn dropdown-toggle " href="#" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <span>{{trans('menu.post_announcement')}}</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right mt-2">

                                @foreach($types as $item)
                                    @if($item->key == 'job_opp')
                                        <a class="dropdown-item"
                                           href="{{route('make-job',$item->key)}}">


                                            <svg xmlns="http://www.w3.org/2000/svg" width="18.152" height="18.152"
                                                 aria-label="job"
                                                 class="mr-2"
                                                 viewBox="0 0 24 24">
                                                <path d="M0 0h24v24H0z" fill="none"/>
                                                <path d="M20 19.59V8l-6-6H6c-1.1 0-1.99.9-1.99 2L4 20c0 1.1.89 2 1.99 2H18c.45 0 .85-.15 1.19-.4l-4.43-4.43c-.8.52-1.74.83-2.76.83-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5c0 1.02-.31 1.96-.83 2.75L20 19.59zM9 13c0 1.66 1.34 3 3 3s3-1.34 3-3-1.34-3-3-3-3 1.34-3 3z"/>
                                            </svg>

                                            {{trans('types.'.$item->key)}}
                                        </a>
                                    @elseif($item->key == 'internship')
                                        <a class="dropdown-item"
                                           href="{{route('make-job',$item->key)}}">


                                            <svg xmlns="http://www.w3.org/2000/svg" width="18.152" height="18.152"
                                                 aria-label="internship"
                                                 class="mr-2"
                                                 viewBox="0 0 24 24">
                                                <path d="M0 0h24v24H0zm0 0h24v24H0zm0 0h24v24H0z" fill="none"/>
                                                <path d="M20 0H4v2h16V0zM4 24h16v-2H4v2zM20 4H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm-8 2.75c1.24 0 2.25 1.01 2.25 2.25s-1.01 2.25-2.25 2.25S9.75 10.24 9.75 9 10.76 6.75 12 6.75zM17 17H7v-1.5c0-1.67 3.33-2.5 5-2.5s5 .83 5 2.5V17z"/>
                                            </svg>

                                            {{trans('types.'.$item->key)}}
                                        </a>
                                    @elseif($item->key == 'volunteering')
                                        <a class="dropdown-item"
                                           href="{{route('make-job',$item->key)}}">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" width="18.152"
                                                 height="18.152"
                                                 aria-label="volunteering"
                                                 class="mr-2" viewBox="0 0 24 24">
                                                <defs>
                                                    <path id="a" d="M0 0h24v24H0z"/>
                                                </defs>
                                                <clipPath id="b">
                                                    <use xlink:href="#a" overflow="visible"/>
                                                </clipPath>
                                                <path clip-path="url(#b)"
                                                      d="M23 5.5V20c0 2.2-1.8 4-4 4h-7.3c-1.08 0-2.1-.43-2.85-1.19L1 14.83s1.26-1.23 1.3-1.25c.22-.19.49-.29.79-.29.22 0 .42.06.6.16.04.01 4.31 2.46 4.31 2.46V4c0-.83.67-1.5 1.5-1.5S11 3.17 11 4v7h1V1.5c0-.83.67-1.5 1.5-1.5S15 .67 15 1.5V11h1V2.5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5V11h1V5.5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5z"/>
                                            </svg>
                                            {{trans('types.'.$item->key)}}
                                        </a>
                                    @elseif($item->key == 'training')
                                        <a class="dropdown-item"
                                           href="{{route('make-job',$item->key)}}">


                                            <svg xmlns="http://www.w3.org/2000/svg" width="18.152"
                                                 height="18.152"
                                                 aria-label="training"
                                                 class="mr-2" viewBox="0 0 24 24">
                                                <circle cx="9" cy="9" r="4"/>
                                                <path d="M9 15c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4zm7.76-9.64l-1.68 1.69c.84 1.18.84 2.71 0 3.89l1.68 1.69c2.02-2.02 2.02-5.07 0-7.27zM20.07 2l-1.63 1.63c2.77 3.02 2.77 7.56 0 10.74L20.07 16c3.9-3.89 3.91-9.95 0-14z"/>
                                                <path fill="none" d="M0 0h24v24H0z"/>
                                            </svg>

                                            {{trans('types.'.$item->key)}}
                                        </a>
                                    @elseif($item->key == 'education')
                                        <a class="dropdown-item"
                                           href="{{route('make-job',$item->key)}}">


                                            <svg xmlns="http://www.w3.org/2000/svg" width="18.152" height="18.152"
                                                 aria-label="education"
                                                 class="mr-2" viewBox="0 0 24 24">
                                                <path d="M0 0h24v24H0z" fill="none"/>
                                                <path d="M5 13.18v4L12 21l7-3.82v-4L12 17l-7-3.82zM12 3L1 9l11 6 9-4.91V17h2V9L12 3z"/>
                                            </svg>


                                            {{trans('types.'.$item->key)}}
                                        </a>
                                    @elseif($item->key == 'fellowship')
                                        <a class="dropdown-item"
                                           href="{{route('make-job',$item->key)}}">


                                            <svg xmlns="http://www.w3.org/2000/svg" width="18.152"
                                                 height="18.152"
                                                 aria-label="fellowship"
                                                 class="mr-2" viewBox="0 0 24 24">
                                                <path d="M0 0h24v24H0z" fill="none"/>
                                                <path d="M16 11c1.66 0 2.99-1.34 2.99-3S17.66 5 16 5c-1.66 0-3 1.34-3 3s1.34 3 3 3zm-8 0c1.66 0 2.99-1.34 2.99-3S9.66 5 8 5C6.34 5 5 6.34 5 8s1.34 3 3 3zm0 2c-2.33 0-7 1.17-7 3.5V19h14v-2.5c0-2.33-4.67-3.5-7-3.5zm8 0c-.29 0-.62.02-.97.05 1.16.84 1.97 1.97 1.97 3.45V19h6v-2.5c0-2.33-4.67-3.5-7-3.5z"/>
                                            </svg>

                                            {{trans('types.'.$item->key)}}
                                        </a>
                                    @elseif($item->key == 'scholarship')
                                        <a class="dropdown-item"
                                           href="{{route('make-job',$item->key)}}">


                                            <svg xmlns="http://www.w3.org/2000/svg" width="18.152"
                                                 height="18.152"
                                                 aria-label="scholarship"
                                                 class="mr-2"
                                                 viewBox="0 0 24 24">
                                                <path d="M0 0h24v24H0z" fill="none"/>
                                                <path d="M20 2H4c-1.11 0-2 .89-2 2v11c0 1.11.89 2 2 2h4v5l4-2 4 2v-5h4c1.11 0 2-.89 2-2V4c0-1.11-.89-2-2-2zm0 13H4v-2h16v2zm0-5H4V4h16v6z"/>
                                            </svg>
                                            {{trans('types.'.$item->key)}}
                                        </a>
                                    @elseif($item->key == 'event')
                                        <a class="dropdown-item"
                                           href="{{route('make-job',$item->key)}}">


                                            <svg xmlns="http://www.w3.org/2000/svg" width="18.152"
                                                 height="18.152"
                                                 aria-label="events"
                                                 class="mr-2" viewBox="0 0 24 24">
                                                <path d="M9 11H7v2h2v-2zm4 0h-2v2h2v-2zm4 0h-2v2h2v-2zm2-7h-1V2h-2v2H8V2H6v2H5c-1.11 0-1.99.9-1.99 2L3 20c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 16H5V9h14v11z"/>
                                                <path fill="none" d="M0 0h24v24H0z"/>
                                            </svg>

                                            {{trans('types.'.$item->key)}}
                                        </a>
                                    @elseif($item->key == 'news')
                                        <a class="dropdown-item"
                                           href="{{route('make-job',$item->key)}}">


                                            <svg xmlns="http://www.w3.org/2000/svg" width="18.152"
                                                 height="18.152"
                                                 aria-label="news"
                                                 class="mr-2" viewBox="0 0 24 24">
                                                <path fill="none"
                                                      d="M-74 29h48v48h-48V29zM0 0h24v24H0V0zm0 0h24v24H0V0z"/>
                                                <path d="M13 12h7v1.5h-7zm0-2.5h7V11h-7zm0 5h7V16h-7zM21 4H3c-1.1 0-2 .9-2 2v13c0 1.1.9 2 2 2h18c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 15h-9V6h9v13z"/>
                                            </svg>


                                            {{trans('types.'.$item->key)}}
                                        </a>
                                    @elseif($item->key == 'publication')
                                        <a class="dropdown-item"
                                           href="{{route('make-job',$item->key)}}">


                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" width="18.152"
                                                 height="18.152"
                                                 aria-label="publication"
                                                 class="mr-2" viewBox="0 0 24 24">
                                                <defs>
                                                    <path id="a" d="M0 0h24v24H0V0z"/>
                                                </defs>
                                                <clipPath id="b">
                                                    <use xlink:href="#a" overflow="visible"/>
                                                </clipPath>
                                                <path clip-path="url(#b)"
                                                      d="M21 5c-1.11-.35-2.33-.5-3.5-.5-1.95 0-4.05.4-5.5 1.5-1.45-1.1-3.55-1.5-5.5-1.5S2.45 4.9 1 6v14.65c0 .25.25.5.5.5.1 0 .15-.05.25-.05C3.1 20.45 5.05 20 6.5 20c1.95 0 4.05.4 5.5 1.5 1.35-.85 3.8-1.5 5.5-1.5 1.65 0 3.35.3 4.75 1.05.1.05.15.05.25.05.25 0 .5-.25.5-.5V6c-.6-.45-1.25-.75-2-1zm0 13.5c-1.1-.35-2.3-.5-3.5-.5-1.7 0-4.15.65-5.5 1.5V8c1.35-.85 3.8-1.5 5.5-1.5 1.2 0 2.4.15 3.5.5v11.5z"/>
                                            </svg>


                                            {{trans('types.'.$item->key)}}
                                        </a>
                                    @else
                                        <a class="dropdown-item"
                                           href="{{route('make-job',$item->key)}}">


                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 width="18.152"
                                                 height="18.152"
                                                 aria-label="publication"
                                                 class="mr-2" viewBox="0 0 24 24" enable-background="new 0 0 24 24"
                                                 xml:space="preserve">
<g id="Bounding_Box">
    <rect fill="none" width="24" height="24"/>
</g>
                                                <g id="Flat">
                                                    <g id="ui_x5F_spec_x5F_header_copy_2">
                                                    </g>
                                                    <path d="M14,9l-1-2H7V5.72C7.6,5.38,8,4.74,8,4c0-1.1-0.9-2-2-2S4,2.9,4,4c0,0.74,0.4,1.38,1,1.72V21h2v-4h5l1,2h7V9H14z M18,17h-4
		l-1-2H7V9h5l1,2h5V17z"/>
                                                </g>
</svg>

                                            {{trans('types.'.$item->key)}}
                                        </a>

                                    @endif

                                @endforeach

                            </div>
                        </li>
                        @elseif(Auth::user()->can('create_announcement'))
                            <li class="nav-item dropdown">
                                <a class="nav-link nav-btn dropdown-toggle " href="#" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    <span>{{trans('menu.post_announcement')}}</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right mt-2">

                                    @foreach($types as $item)
                                        @if($item->key == 'job_opp')
                                            <a class="dropdown-item"
                                               href="{{route('make-job',$item->key)}}">


                                                <svg xmlns="http://www.w3.org/2000/svg" width="18.152" height="18.152"
                                                     aria-label="job"
                                                     class="mr-2"
                                                     viewBox="0 0 24 24">
                                                    <path d="M0 0h24v24H0z" fill="none"/>
                                                    <path d="M20 19.59V8l-6-6H6c-1.1 0-1.99.9-1.99 2L4 20c0 1.1.89 2 1.99 2H18c.45 0 .85-.15 1.19-.4l-4.43-4.43c-.8.52-1.74.83-2.76.83-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5c0 1.02-.31 1.96-.83 2.75L20 19.59zM9 13c0 1.66 1.34 3 3 3s3-1.34 3-3-1.34-3-3-3-3 1.34-3 3z"/>
                                                </svg>

                                                {{trans('types.'.$item->key)}}
                                            </a>
                                        @elseif($item->key == 'internship')
                                            <a class="dropdown-item"
                                               href="{{route('make-job',$item->key)}}">


                                                <svg xmlns="http://www.w3.org/2000/svg" width="18.152" height="18.152"
                                                     aria-label="internship"
                                                     class="mr-2"
                                                     viewBox="0 0 24 24">
                                                    <path d="M0 0h24v24H0zm0 0h24v24H0zm0 0h24v24H0z" fill="none"/>
                                                    <path d="M20 0H4v2h16V0zM4 24h16v-2H4v2zM20 4H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm-8 2.75c1.24 0 2.25 1.01 2.25 2.25s-1.01 2.25-2.25 2.25S9.75 10.24 9.75 9 10.76 6.75 12 6.75zM17 17H7v-1.5c0-1.67 3.33-2.5 5-2.5s5 .83 5 2.5V17z"/>
                                                </svg>

                                                {{trans('types.'.$item->key)}}
                                            </a>
                                        @elseif($item->key == 'volunteering')
                                            <a class="dropdown-item"
                                               href="{{route('make-job',$item->key)}}">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" width="18.152"
                                                     height="18.152"
                                                     aria-label="volunteering"
                                                     class="mr-2" viewBox="0 0 24 24">
                                                    <defs>
                                                        <path id="a" d="M0 0h24v24H0z"/>
                                                    </defs>
                                                    <clipPath id="b">
                                                        <use xlink:href="#a" overflow="visible"/>
                                                    </clipPath>
                                                    <path clip-path="url(#b)"
                                                          d="M23 5.5V20c0 2.2-1.8 4-4 4h-7.3c-1.08 0-2.1-.43-2.85-1.19L1 14.83s1.26-1.23 1.3-1.25c.22-.19.49-.29.79-.29.22 0 .42.06.6.16.04.01 4.31 2.46 4.31 2.46V4c0-.83.67-1.5 1.5-1.5S11 3.17 11 4v7h1V1.5c0-.83.67-1.5 1.5-1.5S15 .67 15 1.5V11h1V2.5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5V11h1V5.5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5z"/>
                                                </svg>
                                                {{trans('types.'.$item->key)}}
                                            </a>
                                        @elseif($item->key == 'training')
                                            <a class="dropdown-item"
                                               href="{{route('make-job',$item->key)}}">


                                                <svg xmlns="http://www.w3.org/2000/svg" width="18.152"
                                                     height="18.152"
                                                     aria-label="training"
                                                     class="mr-2" viewBox="0 0 24 24">
                                                    <circle cx="9" cy="9" r="4"/>
                                                    <path d="M9 15c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4zm7.76-9.64l-1.68 1.69c.84 1.18.84 2.71 0 3.89l1.68 1.69c2.02-2.02 2.02-5.07 0-7.27zM20.07 2l-1.63 1.63c2.77 3.02 2.77 7.56 0 10.74L20.07 16c3.9-3.89 3.91-9.95 0-14z"/>
                                                    <path fill="none" d="M0 0h24v24H0z"/>
                                                </svg>

                                                {{trans('types.'.$item->key)}}
                                            </a>
                                        @elseif($item->key == 'education')
                                            <a class="dropdown-item"
                                               href="{{route('make-job',$item->key)}}">


                                                <svg xmlns="http://www.w3.org/2000/svg" width="18.152" height="18.152"
                                                     aria-label="education"
                                                     class="mr-2" viewBox="0 0 24 24">
                                                    <path d="M0 0h24v24H0z" fill="none"/>
                                                    <path d="M5 13.18v4L12 21l7-3.82v-4L12 17l-7-3.82zM12 3L1 9l11 6 9-4.91V17h2V9L12 3z"/>
                                                </svg>


                                                {{trans('types.'.$item->key)}}
                                            </a>
                                        @elseif($item->key == 'fellowship')
                                            <a class="dropdown-item"
                                               href="{{route('make-job',$item->key)}}">


                                                <svg xmlns="http://www.w3.org/2000/svg" width="18.152"
                                                     height="18.152"
                                                     aria-label="fellowship"
                                                     class="mr-2" viewBox="0 0 24 24">
                                                    <path d="M0 0h24v24H0z" fill="none"/>
                                                    <path d="M16 11c1.66 0 2.99-1.34 2.99-3S17.66 5 16 5c-1.66 0-3 1.34-3 3s1.34 3 3 3zm-8 0c1.66 0 2.99-1.34 2.99-3S9.66 5 8 5C6.34 5 5 6.34 5 8s1.34 3 3 3zm0 2c-2.33 0-7 1.17-7 3.5V19h14v-2.5c0-2.33-4.67-3.5-7-3.5zm8 0c-.29 0-.62.02-.97.05 1.16.84 1.97 1.97 1.97 3.45V19h6v-2.5c0-2.33-4.67-3.5-7-3.5z"/>
                                                </svg>

                                                {{trans('types.'.$item->key)}}
                                            </a>
                                        @elseif($item->key == 'scholarship')
                                            <a class="dropdown-item"
                                               href="{{route('make-job',$item->key)}}">


                                                <svg xmlns="http://www.w3.org/2000/svg" width="18.152"
                                                     height="18.152"
                                                     aria-label="scholarship"
                                                     class="mr-2"
                                                     viewBox="0 0 24 24">
                                                    <path d="M0 0h24v24H0z" fill="none"/>
                                                    <path d="M20 2H4c-1.11 0-2 .89-2 2v11c0 1.11.89 2 2 2h4v5l4-2 4 2v-5h4c1.11 0 2-.89 2-2V4c0-1.11-.89-2-2-2zm0 13H4v-2h16v2zm0-5H4V4h16v6z"/>
                                                </svg>
                                                {{trans('types.'.$item->key)}}
                                            </a>
                                        @elseif($item->key == 'event')
                                            <a class="dropdown-item"
                                               href="{{route('make-job',$item->key)}}">


                                                <svg xmlns="http://www.w3.org/2000/svg" width="18.152"
                                                     height="18.152"
                                                     aria-label="events"
                                                     class="mr-2" viewBox="0 0 24 24">
                                                    <path d="M9 11H7v2h2v-2zm4 0h-2v2h2v-2zm4 0h-2v2h2v-2zm2-7h-1V2h-2v2H8V2H6v2H5c-1.11 0-1.99.9-1.99 2L3 20c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 16H5V9h14v11z"/>
                                                    <path fill="none" d="M0 0h24v24H0z"/>
                                                </svg>

                                                {{trans('types.'.$item->key)}}
                                            </a>
                                        @elseif($item->key == 'news')
                                            <a class="dropdown-item"
                                               href="{{route('make-job',$item->key)}}">


                                                <svg xmlns="http://www.w3.org/2000/svg" width="18.152"
                                                     height="18.152"
                                                     aria-label="news"
                                                     class="mr-2" viewBox="0 0 24 24">
                                                    <path fill="none"
                                                          d="M-74 29h48v48h-48V29zM0 0h24v24H0V0zm0 0h24v24H0V0z"/>
                                                    <path d="M13 12h7v1.5h-7zm0-2.5h7V11h-7zm0 5h7V16h-7zM21 4H3c-1.1 0-2 .9-2 2v13c0 1.1.9 2 2 2h18c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 15h-9V6h9v13z"/>
                                                </svg>


                                                {{trans('types.'.$item->key)}}
                                            </a>
                                        @elseif($item->key == 'publication')
                                            <a class="dropdown-item"
                                               href="{{route('make-job',$item->key)}}">


                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" width="18.152"
                                                     height="18.152"
                                                     aria-label="publication"
                                                     class="mr-2" viewBox="0 0 24 24">
                                                    <defs>
                                                        <path id="a" d="M0 0h24v24H0V0z"/>
                                                    </defs>
                                                    <clipPath id="b">
                                                        <use xlink:href="#a" overflow="visible"/>
                                                    </clipPath>
                                                    <path clip-path="url(#b)"
                                                          d="M21 5c-1.11-.35-2.33-.5-3.5-.5-1.95 0-4.05.4-5.5 1.5-1.45-1.1-3.55-1.5-5.5-1.5S2.45 4.9 1 6v14.65c0 .25.25.5.5.5.1 0 .15-.05.25-.05C3.1 20.45 5.05 20 6.5 20c1.95 0 4.05.4 5.5 1.5 1.35-.85 3.8-1.5 5.5-1.5 1.65 0 3.35.3 4.75 1.05.1.05.15.05.25.05.25 0 .5-.25.5-.5V6c-.6-.45-1.25-.75-2-1zm0 13.5c-1.1-.35-2.3-.5-3.5-.5-1.7 0-4.15.65-5.5 1.5V8c1.35-.85 3.8-1.5 5.5-1.5 1.2 0 2.4.15 3.5.5v11.5z"/>
                                                </svg>


                                                {{trans('types.'.$item->key)}}
                                            </a>
                                        @else
                                            <a class="dropdown-item"
                                               href="{{route('make-job',$item->key)}}">


                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     width="18.152"
                                                     height="18.152"
                                                     aria-label="publication"
                                                     class="mr-2" viewBox="0 0 24 24" enable-background="new 0 0 24 24"
                                                     xml:space="preserve">
<g id="Bounding_Box">
    <rect fill="none" width="24" height="24"/>
</g>
                                                    <g id="Flat">
                                                        <g id="ui_x5F_spec_x5F_header_copy_2">
                                                        </g>
                                                        <path d="M14,9l-1-2H7V5.72C7.6,5.38,8,4.74,8,4c0-1.1-0.9-2-2-2S4,2.9,4,4c0,0.74,0.4,1.38,1,1.72V21h2v-4h5l1,2h7V9H14z M18,17h-4
		l-1-2H7V9h5l1,2h5V17z"/>
                                                    </g>
</svg>

                                                {{trans('types.'.$item->key)}}
                                            </a>

                                        @endif
                                    @endforeach
                                </div>
                            </li>
                            @endguest
                            <li class="nav-item dropdown drop-lang">
                                <a class="nav-link dropdown-toggle pr-0" href="#" id="language-Drop"
                                   aria-label="language"
                                   role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if(Config::get('app.locale')=='en')
                                        <img src="/img/us2.png" alt="language">
                                    @elseif(Config::get('app.locale')=='ru')
                                        <img src="/img/russia2.png" alt="language">
                                    @else
                                        <img src="/img/armenia2.png" alt="language">
                                    @endif
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="language-Drop">

                                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        <a class="dropdown-item"
                                           href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}"
                                           hreflang="{{ $localeCode }}">
                                            {{ $properties['native'] }}
                                        </a>
                                    @endforeach
                                </div>
                            </li>
                </ul>

            </div>
        </nav>
    </div>
    <!--end navigation-->

@include('flash::message')
@yield('content')



<!--footer-->
    <footer class="my-footer mt-auto">
        <div class="footer-container">
            <div class="logo-form">
                <a href="#" class="footer-logo" aria-label="CareerCenter">
                    <img data-src="/img/logo-footer.svg" alt="CareerCenter" class="lazyload">
                </a>
                <div class="footer-form">
                    <form action="{{route('subscribe')}}" class="d-flex community-form" method="POST">
                        @csrf
                        <label for="footer-email" aria-label="email" class="mail-label">
                            <input type="email" name="email" class="form-control w-100"
                                   placeholder="email@example.com" required="" id="footer-email">
                        </label>
                        <button type="submit" class="btn">{{trans('menu.subscribe')}}</button>
                    </form>
                    {{--<a style="color: white"--}}
                       {{--href="mailto:sose.yeritsyan.97@gmail.com">{{trans('menu.subscribe_to_email_here')}}</a>--}}
                    {{--<br>--}}
                    {{--<br>--}}

                    <span class="footer-mail-text">{{trans('menu.monthly_visits')}} ~150.000</span>
                </div>
            </div>

            <div class="footer-links">
                <ul>
                    <li class="font-bold links-title">{{trans('menu.for_job_seekers')}}</li>

                    <li>
                        <a href="{{route('jobs')}}">
                            <div class="cta d-inline"><span class="arrow-f primera next-f"></span><span
                                        class="arrow-f segunda next-f"></span></div>
                            {{trans('menu.find_jobs')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('resume.index')}}">
                            <div class="cta d-inline"><span class="arrow-f primera next-f"></span><span
                                        class="arrow-f segunda next-f"></span></div>
                            {{trans('home.create_resume')}}
                        </a>
                    </li>
                    {{--<li>--}}
                        {{--<a href="{{route('pages','help')}}">--}}
                            {{--<div class="cta d-inline"><span class="arrow-f primera next-f"></span><span--}}
                                        {{--class="arrow-f segunda next-f"></span></div>--}}
                            {{--{{trans('menu.help')}}--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    <li>
                        <a href="{{route('resource.index',['c'=>'employment'])}}">
                            <div class="cta d-inline"><span class="arrow-f primera next-f"></span><span
                                        class="arrow-f segunda next-f"></span></div>
                            {{trans('menu.resource_employment')}}
                        </a>
                        <a href="{{route('resources.create')}}">
                            <span class="footer-link-plus">
                                <svg width="7" height="7" fill="#ffffff" x="0px" y="0px" viewBox="0 0 491.86 491.86">
                                    <g>
                                        <path d="M465.167,211.614H280.245V26.691c0-8.424-11.439-26.69-34.316-26.69s-34.316,18.267-34.316,26.69v184.924H26.69 C18.267,211.614,0,223.053,0,245.929s18.267,34.316,26.69,34.316h184.924v184.924c0,8.422,11.438,26.69,34.316,26.69 s34.316-18.268,34.316-26.69V280.245H465.17c8.422,0,26.69-11.438,26.69-34.316S473.59,211.614,465.167,211.614z"/>
                                    </g>
                                </svg>
                            </span>
                        </a>
                    </li>
                </ul>

                <ul>
                    <li class="font-bold links-title">{{trans('menu.for_employers')}}</li>

                    <li>
                        <a href="{{route('make-job','job_opp')}}">
                            <div class="cta d-inline"><span class="arrow-f primera next-f"></span><span
                                        class="arrow-f segunda next-f"></span></div>
                            {{trans('menu.post_jobs')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('pages','pricing')}}">
                            <div class="cta d-inline"><span class="arrow-f primera next-f"></span><span
                                        class="arrow-f segunda next-f"></span></div>
                            {{trans('menu.pricing')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('pages','about')}}">
                            <div class="cta d-inline"><span class="arrow-f primera next-f"></span><span
                                        class="arrow-f segunda next-f"></span></div>
                            {{trans('menu.about')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('resource.index',['c'=>'for-organizations'])}}">
                            <div class="cta d-inline"><span class="arrow-f primera next-f"></span><span
                                        class="arrow-f segunda next-f"></span></div>
                            {{trans('menu.resource_for_organizations')}}
                        </a>
                        <a href="{{route('resources.create')}}">
                            <span class="footer-link-plus">
                                <svg width="7" height="7" fill="#ffffff" x="0px" y="0px" viewBox="0 0 491.86 491.86">
                                    <g>
                                        <path d="M465.167,211.614H280.245V26.691c0-8.424-11.439-26.69-34.316-26.69s-34.316,18.267-34.316,26.69v184.924H26.69 C18.267,211.614,0,223.053,0,245.929s18.267,34.316,26.69,34.316h184.924v184.924c0,8.422,11.438,26.69,34.316,26.69 s34.316-18.268,34.316-26.69V280.245H465.17c8.422,0,26.69-11.438,26.69-34.316S473.59,211.614,465.167,211.614z"/>
                                    </g>
                                </svg>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('posting-rules')}}">
                            <div class="cta d-inline"><span class="arrow-f primera next-f"></span><span
                                        class="arrow-f segunda next-f"></span></div>
                            {{trans('menu.posting_rules')}}
                        </a>
                    </li>
                </ul>

                <ul>
                    <li class="font-bold links-title">{{trans('menu.helpful_resources')}}</li>

                    <li>
                        <a href="/contact">
                            <div class="cta d-inline"><span class="arrow-f primera next-f"></span><span
                                        class="arrow-f segunda next-f"></span></div>
                            {{trans('menu.contact_us')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('pages','terms-and-conditions')}}">
                            <div class="cta d-inline"><span class="arrow-f primera next-f"></span><span
                                        class="arrow-f segunda next-f"></span></div>
                            {{trans('menu.terms_of_conditions')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('pages','privacy-policy')}}">
                            <div class="cta d-inline"><span class="arrow-f primera next-f"></span><span
                                        class="arrow-f segunda next-f"></span></div>
                            {{trans('menu.privacy_policy')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('review')}}">
                            <div class="cta d-inline"><span class="arrow-f primera next-f"></span><span
                                        class="arrow-f segunda next-f"></span></div>
                            {{trans('menu.testimonial')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('blog')}}">
                            <div class="cta d-inline"><span class="arrow-f primera next-f"></span><span
                                        class="arrow-f segunda next-f"></span></div>
                            {{trans('menu.blog')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('resource.index',['c'=>'educational'])}}">
                            <div class="cta d-inline"><span class="arrow-f primera next-f"></span><span
                                        class="arrow-f segunda next-f"></span></div>
                            {{trans('menu.resource_educational')}}
                        </a>
                        <a href="{{route('resources.create')}}">
                             <span class="footer-link-plus">
                                <svg width="7" height="7" fill="#ffffff" x="0px" y="0px" viewBox="0 0 491.86 491.86">
                                    <g>
                                        <path d="M465.167,211.614H280.245V26.691c0-8.424-11.439-26.69-34.316-26.69s-34.316,18.267-34.316,26.69v184.924H26.69 C18.267,211.614,0,223.053,0,245.929s18.267,34.316,26.69,34.316h184.924v184.924c0,8.422,11.438,26.69,34.316,26.69 s34.316-18.268,34.316-26.69V280.245H465.17c8.422,0,26.69-11.438,26.69-34.316S473.59,211.614,465.167,211.614z"/>
                                    </g>
                                </svg>
                            </span>
                        </a>
                    </li>

                    <li>
                        <a href="{{route('resource.index',['c'=>'other'])}}">
                            <div class="cta d-inline"><span class="arrow-f primera next-f"></span><span
                                        class="arrow-f segunda next-f"></span></div>
                            {{trans('menu.resource_other')}}
                        </a>
                        <a href="{{route('resources.create')}}">
                            <span class="footer-link-plus">
                                <svg width="7" height="7" fill="#ffffff" x="0px" y="0px" viewBox="0 0 491.86 491.86">
                                    <g>
                                        <path d="M465.167,211.614H280.245V26.691c0-8.424-11.439-26.69-34.316-26.69s-34.316,18.267-34.316,26.69v184.924H26.69 C18.267,211.614,0,223.053,0,245.929s18.267,34.316,26.69,34.316h184.924v184.924c0,8.422,11.438,26.69,34.316,26.69 s34.316-18.268,34.316-26.69V280.245H465.17c8.422,0,26.69-11.438,26.69-34.316S473.59,211.614,465.167,211.614z"/>
                                    </g>
                                </svg>
                            </span>
                        </a>
                    </li>
                </ul>

            </div>

            <div class="footer-address">
                <span class="font-bold links-title">{{trans('menu.stay_connected')}}</span>

                <div class="footer-social">
                    <a href="#" target="_blank" rel="noreferrer" aria-hidden="true">
                        <svg xmlns="http://www.w3.org/2000/svg" width="10.032" height="20.064"
                             viewBox="0 0 10.032 20.064">
                            <g transform="translate(0 0)">
                                <path d="M335.843,7259.064v-9.029h2.741l.448-4.013h-3.189v-1.954c0-1.034.026-2.06,1.47-2.06h1.462v-2.869a16.4,16.4,0,0,0-2.527-.14c-2.654,0-4.316,1.662-4.316,4.715v2.308H329v4.013h2.933v9.029Z"
                                      transform="translate(-329 -7239)" fill="#fff" fill-rule="evenodd"/>
                            </g>
                        </svg>
                    </a>
                    {{--<a href="#" target="_blank" rel="noreferrer" aria-hidden="true">--}}
                    {{--<svg xmlns="http://www.w3.org/2000/svg" width="16.052" height="20.091"--}}
                    {{--viewBox="0 0 16.052 20.091">--}}
                    {{--<path d="M10.984,12.649a.844.844,0,0,1,.024.1c.284,1.615,1.028,2.29,2.453,2.29,2.271,0,4.585-2.819,4.585-5.759,0-3.063-2.467-5.277-6.465-5.277A5.313,5.313,0,0,0,6.006,9.284a3.574,3.574,0,0,0,.754,2.646A1,1,0,1,1,5.4,13.4,5.375,5.375,0,0,1,4,9.284,7.3,7.3,0,0,1,11.58,2c5.06,0,8.471,3.061,8.471,7.284,0,3.969-3.114,7.765-6.591,7.765a4.2,4.2,0,0,1-3.2-1.259l-1.269,5.5a1,1,0,1,1-1.954-.451l3.01-13.042A1,1,0,0,1,12,8.245Z"--}}
                    {{--transform="translate(-4 -2)" fill="#fff"/>--}}
                    {{--</svg>--}}
                    {{--</a>--}}
                    <a href="#" target="_blank" rel="noreferrer" aria-hidden="true">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24.598" height="19.679"
                             viewBox="0 0 24.598 19.679">
                            <g transform="translate(0 0)">
                                <path d="M11.736,7380.679a14.147,14.147,0,0,0,14.359-14.139c0-.215,0-.43-.015-.643a10.215,10.215,0,0,0,2.518-2.57,10.193,10.193,0,0,1-2.9.781,5.009,5.009,0,0,0,2.219-2.75,10.15,10.15,0,0,1-3.205,1.208,5.108,5.108,0,0,0-7.141-.216,4.925,4.925,0,0,0-1.46,4.747,14.41,14.41,0,0,1-10.4-5.19,4.925,4.925,0,0,0,1.563,6.631,5.052,5.052,0,0,1-2.291-.62v.062a4.989,4.989,0,0,0,4.049,4.87,5.071,5.071,0,0,1-2.278.086,5.044,5.044,0,0,0,4.714,3.452,10.228,10.228,0,0,1-6.267,2.13,10.533,10.533,0,0,1-1.2-.071,14.452,14.452,0,0,0,7.736,2.229"
                                      transform="translate(-4 -7361)" fill="#fff" fill-rule="evenodd"/>
                            </g>
                        </svg>
                    </a>

                </div>
            </div>
            <div class="copyright w-100">
                <div class="copyright-item d-flex justify-content-between flex-wrap">
                    <span>© 2002-2019  CareerCenter</span>
                    {{--<a href="https://aist.global" target="_blank" rel="noopener noreferrer" class="aist-block">--}}
                        {{--<span>Design &  Development  by</span>--}}
                        {{--<svg xmlns="http://www.w3.org/2000/svg" width="81.82" height="27.544"--}}
                             {{--viewBox="0 0 81.82 27.544">--}}
                            {{--<g id="Group_571" data-name="Group 571" transform="translate(-12.993 -14.011)">--}}
                                {{--<g id="Shape_8" data-name="Shape 8" transform="translate(12.993 14.011)">--}}
                                    {{--<path id="Shape_8-2" data-name="Shape 8"--}}
                                          {{--d="M26.889.01H1.854A1.541,1.541,0,0,0,.04,1.464c-.091,1.68,0,23.615,0,23.615S0,26.669,1.31,26.714s6.349,0,6.349,0Z"--}}
                                          {{--transform="translate(0 0)" fill="#fff" fill-rule="evenodd"/>--}}
                                {{--</g>--}}
                                {{--<g id="Shape_9" data-name="Shape 9" transform="translate(32.082 19.289)">--}}
                                    {{--<path id="Shape_9-2" data-name="Shape 9" d="M8.526,0,0,12.534H8.708Z" fill="#fff"--}}
                                          {{--fill-rule="evenodd"/>--}}
                                {{--</g>--}}
                                {{--<g id="Shape_10" data-name="Shape 10" transform="translate(27.349 34.367)">--}}
                                    {{--<path id="Shape_10-2" data-name="Shape 10"--}}
                                          {{--d="M2.919,0S.56,3.306.379,3.815A7.044,7.044,0,0,0,.016,5.268a6.9,6.9,0,0,0,0,.908l13.062.182s.508-.182.544-2,0-4.36,0-4.36Z"--}}
                                          {{--transform="translate(0)" fill="#fff" fill-rule="evenodd"/>--}}
                                {{--</g>--}}
                                {{--<g id="Shape_11" data-name="Shape 11" transform="translate(44.179 14.255)">--}}
                                    {{--<path id="Shape_11-2" data-name="Shape 11" d="M0,26.68,4.734,0H8.32L3.729,26.823Z"--}}
                                          {{--fill="#fff" fill-rule="evenodd"/>--}}
                                {{--</g>--}}
                                {{--<g id="Shape_12" data-name="Shape 12" transform="translate(54.65 14.255)">--}}
                                    {{--<path id="Shape_12-2" data-name="Shape 12"--}}
                                          {{--d="M1,22.377a7.444,7.444,0,0,0,2.869,1.434,13.556,13.556,0,0,0,4.447,0A6.04,6.04,0,0,0,10.471,21.8c.258-.545.4-3.27-.287-4.16A35.774,35.774,0,0,0,5.738,13.77c-2.18-1.779-3.672-4.332-3.729-5.594S2.094,3.5,3.3,2.3A7.139,7.139,0,0,1,8.32,0H40.163L39.59,3.3H30.84L26.823,26.823H23.237L27.254,3.443,8.32,3.156a3.111,3.111,0,0,0-1.721,1,3.989,3.989,0,0,0-.861,3.012,4.232,4.232,0,0,0,1.721,3.012A39.491,39.491,0,0,1,10.9,13.053c.832.832,2.831,2.945,3.156,4.16a11.654,11.654,0,0,1,.287,4.447A7.036,7.036,0,0,1,12.336,25.1a8.978,8.978,0,0,1-3.729,1.865,12.567,12.567,0,0,1-5.02.143A12.174,12.174,0,0,1,0,25.963Z"--}}
                                          {{--fill="#fff" fill-rule="evenodd"/>--}}
                                {{--</g>--}}
                            {{--</g>--}}
                        {{--</svg>--}}
                    {{--</a>--}}
                </div>
            </div>
        </div>
    </footer>
    <!--end footer-->

    <!-- Modal -->
    <div id="success_subscribe" class="modal fade success-modal" role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <!-- Modal content-->
            <div class="modal-content">
                <a class="close" href="#" data-dismiss="modal">&times;</a>
                <div class="page-body">
                    <div class="head">
                        <h3 class="">Thank You </h3>
                        <h5 class="mb-5">for subscribtion</h5>
                    </div>

                    <h1 class="text-center">
                        <span class="checkmark-circle">
                            <span class="background"></span>
                            <span class="checkmark draw"></span>
                        </span>
                    </h1>
                </div>
            </div>
        </div>

    </div>
</div>


<script async src="https://www.googletagmanager.com/gtag/js?id=UA-45283750-16"></script>
{{--<script>--}}
    {{--console.log = console.warn = console.info = console.error  = function(){}--}}
{{--</script>--}}

<script src="{{ asset('js/app.js') }}"></script>

<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-45283750-16');
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        setTimeout(function () {
            $("div.alert").remove();
        }, 5000); // 5 secs

    });

    @if(Session::has('subscribe'))
$('#success_subscribe').modal('show')
    {{Session::forget('subscribe')}}
    @endif
    // Start upload preview image
    $(".gambar").attr("src", "{{isset(Auth::user()->image) ? Storage::disk('')->url(Auth::user()->image) : '/img/my-profile.svg'}}");
    var $uploadCrop,
        tempFilename,
        rawImg,
        imageId;
    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.upload-demo').addClass('ready');
                $('#cropImagePop').modal('show');
                rawImg = e.target.result;
            }
            reader.readAsDataURL(input.files[0]);
        }
        else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: 130,
            height: 130,
            square: 'circle',
            type: 'rectangle'
        },
        // enforceBoundary: false
        enableExif: true
    });
    $('#cropImagePop').on('shown.bs.modal', function () {
        // alert('Shown pop');
        $uploadCrop.croppie('bind', {
            url: rawImg
        }).then(function () {
            console.log('jQuery bind complete');
        });
    });

    $('.item-img').on('change', function () {
        imageId = $(this).data('id');
        tempFilename = $(this).val();
        $('#cancelCropBtn').data('id', imageId);
        readFile(this);
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();


            reader.onload = function (e) {

                $('#profile-pic').attr('src', e.target.result);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{route('profile-image')}}",
                    type: "POST",
                    data: {
                        image:  e.target.result
                    },
                    success: function (data) {

                        $('#item-img-output').attr('src',e.target.result);
                        $('#profile-pic').attr('src', e.target.result);
                        $('.item-img').val('');
                    }
                });

            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $('.org-img').on('change',function()
    {
        readURL(this);




    })
    $('#cropImageBtn').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'base64',
            format: 'png',
            square: 'circle',
            size: {width: 130, height: 130}

        }).then(function (resp) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('profile-image')}}",
                type: "POST",
                data: {
                    image: resp
                },
                success: function (data) {

                    $('#item-img-output').attr('src', resp);
                    $('#profile-pic').attr('src', resp);
                    $('#cropImagePop').modal('hide');
                    $('.item-img').val('');
                }
            });
        });
    });


    (function ($) {

// Check if Navigator is Internet Explorer
        if (navigator.userAgent.indexOf('MSIE') !== -1
            || navigator.appVersion.indexOf('Trident/') > -1) {
            $('body').addClass("body-ie");
            // Scroll event check
            // $(window).scroll(function (event) {
            //     var scroll = $(window).scrollTop();
            //
            //     // Activate sticky for IE if scrolltop is more than 20px
            //     if ( scroll > 20) {
            //         $('.left-menu').addClass( "sticky-top-ie" );
            //     }else{
            //         $('.left-menu').removeClass( "sticky-top-ie" );
            //     }
            //
            // });

        }
    })(jQuery);

</script>
@yield('scripts')
</body>
</html>
