<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <style>
        @font-face {
            font-family: "Dojo Sans Serif";
            font-style: normal;
            font-weight: normal;
            src: url(http://example.com/fonts/dojosans.ttf) format('truetype');
        }

        * {
            box-sizing: border-box;
            font-family: "Dojo Sans Serif", "DejaVu Sans", sans-serif;

        }

        table td {
            padding: 0;
        }

        body {
            font-size: 11px;
            line-height: 10px;
        }


    </style>
</head>
<body>
<div>

    <h2 style="text-align: center;font-weight: normal">
        Փոխանցման հաշիվ

    </h2>
    <div style="border: 1px solid black;width: 700px">
        <div>
            <table style="border-collapse: collapse;width: 700px;max-width: 700px;">
                <tbody style="vertical-align: top;">
                <tr>
                    <td style="border:1px solid;padding: 1px;width: 58%">
                        <table>
                            <tr>
                                <td style="padding-right: 10px;text-align: right;font-weight: bold;width: 120px;">{{trans('invoice.performer')}}</td>
                                <td style="font-weight: bold;font-style: italic">{{$invoice->performer}}</td>
                            </tr>
                            <tr>
                                <td style="padding-right: 10px;text-align: right;font-weight: bold">{{trans('invoice.address')}}</td>
                                <td style="font-style: italic"> {{$invoice->performer_address}}</td>
                            </tr>
                            <tr>
                                <td style="padding-right: 10px;text-align: right;font-weight: bold">
                                {{trans('invoice.account')}}
                                <td style="width: 170px">    {{$invoice->performer_account}}</td>

                                <td style="padding-right: 10px;text-align: right;font-weight: bold">{{trans('invoice.tax')}}</td>
                                <td>    {{$invoice->performer_tax}}</td>

                            </tr>
                            <tr>
                                <td style="padding-right: 10px;text-align: right;font-weight: bold">{{trans('invoice.bank')}}</td>
                                <td>  {{$invoice->performer_bank}}</td>
                            </tr>
                        </table>

                    </td>
                    <td style="border:1px solid;padding: 1px 0 0 10px;width: 40%">
                        <div style="font-style: italic">{{trans('invoice.booking_number')}}
                            N {{$invoice->booking_number}}</div>
                        <span style="font-weight: bold">{{trans('invoice.account_product')}}
                            N </span>
                        <span style="text-decoration: underline; margin-left: auto; padding: 0 5px;">{{$invoice->account_product}}</span>
                        <br>
                        {{ \Carbon\Carbon::parse($invoice->date)->format('d M Y')}}
                    </td>
                </tr>
                <tr>

                    <td style="border:1px solid;padding: 1px;width: 58%">
                        <table>
                            <tr>
                                <td style="padding-right: 10px;text-align: right;font-weight: bold;width: 120px;">{{trans('invoice.client')}}</td>
                                <td style="font-weight: bold;font-style: italic">{{$invoice->client}}</td>
                            </tr>
                            <tr>
                                <td style="padding-right: 10px;text-align: right;font-weight: bold">{{trans('invoice.address')}}</td>
                                <td style="font-style: italic"> {{$invoice->client_address}}</td>
                            </tr>
                            @if($invoice->individual == 0)
                                <tr>

                                    <td style="padding-right: 10px;text-align: right;font-weight: bold">
                                    {{trans('invoice.account')}}
                                    <td style="width: 170px">    {{$invoice->client_account}}</td>

                                    <td style="padding-right: 10px;text-align: right;font-weight: bold">{{trans('invoice.tax')}}</td>
                                    <td>    {{$invoice->client_tax}}</td>

                                </tr>
                                <tr>
                                    <td style="padding-right: 10px;text-align: right;font-weight: bold">{{trans('invoice.bank')}}</td>
                                    <td>  {{$invoice->client_bank}}</td>
                                </tr>

                            @else
                                <tr>
                                    <td style="padding-right: 10px;text-align: right;font-weight: bold">{{trans('invoice.passport')}}</td>
                                    <td> {{$invoice->passport}}</td>
                                </tr>

                            @endif


                        </table>

                    </td>


                    <td style="border:1px solid;padding: 1px 0 0 10px; border-bottom: 0">
                        @if(isset($invoice->posted_announcements) && $invoice->posted_announcements!='' )
                            <div style="text-align: center;font-style: italic">{{trans('invoice.posted_announcements')}}</div>
                            - {{$invoice->posted_announcements}}
                        @endif
                    </td>
                </tr>
                @if(isset($invoice->notes) && $invoice->notes!='' )
                    <tr>
                        <td style="border:1px solid;padding: 1px 10px 3px; border-bottom: 0" colspan="2">
                            <span style="font-weight: bold">{{trans('invoice.notes')}}</span>
                            {{$invoice->notes}}
                        </td>
                    </tr>
                @endif
                <tr>
                    <td style="border:1px solid;padding: 1px 10px 3px; border-bottom: 0" colspan="2">
                        <span style="font-weight: bold">Լրացում` </span> Փոխանցման համար , ԱԱՀ չվճարող: Ծառայությունները
                        մատուցվում են ինտերնետային կայքում:
                    </td>
                </tr>

                </tbody>


            </table>

        </div>
        <div>

            <table style="border-collapse: collapse;width: 700px;text-align: center;max-width: 700px;">
                <tr>
                    <td style="width: 70px;border:1px solid;padding: 1px 5px; font-weight: bold">hamaar</td>
                    <td style="border:1px solid;padding: 1px 5px; font-weight: bold">{{trans('invoice.name')}}</td>
                    <td style="width: 80px;border:1px solid;padding: 1px 5px; font-weight: bold">{{trans('invoice.quantity')}}</td>
                    <td style="border:1px solid;padding: 1px 5px; font-weight: bold">{{trans('invoice.measure')}}</td>
                    <td style="border:1px solid;padding: 1px 5px; font-weight: bold">{{trans('invoice.amount')}}
                        ({{$invoice->currency}})
                    </td>
                    <td style="border:1px solid;padding: 1px 5px; font-weight: bold">{{trans('invoice.sum')}}
                        ({{$invoice->currency}})
                    </td>
                </tr>
                @foreach($invoice->invoiceItems as $item)

                    <tr>
                        <td style="border:1px solid;padding: 1px 5px;">
                        1
                        </td>
                        <td style="border:1px solid;padding: 1px 5px;">
                            {{$item->name}}
                        </td>

                        <td style="border:1px solid;padding: 1px 5px;">
                            {{$item->quantity}}
                        </td>

                        <td style="border:1px solid;padding: 1px 5px;">
                            {{$item->measure}}
                        </td>

                        <td style="border:1px solid;padding: 1px 5px;">
                            {{$item->amount}}
                        </td>

                        <td style="border:1px solid;padding: 1px 5px;">
                            {{$item->sum}}
                        </td>
                    </tr>
                @endforeach


                <tr>
                    <td style="border:1px solid;padding: 1px 5px;"></td>
                    <td style="border:1px solid;padding: 1px 5px; font-weight: bold">{{trans('invoice.total')}}</td>
                    <td style="border:1px solid;padding: 1px 5px;"></td>
                    <td style="border:1px solid;padding: 1px 5px;"></td>
                    <td style="border:1px solid;padding: 1px 5px;"></td>

                    <td style="border:1px solid;padding: 1px 5px;font-weight: bold;font-size: 12px;font-style: italic">
                        {{$invoice->money}} ({{$invoice->currency}})
                    </td>


                </tr>
                <tr>
                    <td style="border:1px solid;padding: 2px 5px;" colspan="6"></td>
                </tr>
                <tr>
                    <td style="border:1px solid;padding: 1px 5px;border-right: 0;"></td>
                    <td colspan="5" style="border:1px solid;padding: 1px 5px;text-align: left;border-left: 0">Քսանվեց
                        հազար
                        հինգ հարյուր ՀՀ Դրամ

                    </td>
                </tr>
            </table>


        </div>


    </div>
    <table style="margin-top: 20px;width: 100%">
        <tbody style="vertical-align: top">
        <tr>
            <td style="width: 60%">
                <span style="font-weight: bold;display: block">"ՊՐՈՖԷՔՍ" ՍՊԸ</span><br>
                <div>
                    <span style="margin-bottom: 5px;display: block">Տնօրեն`</span>
                    <span style="font-style: italic">Ա. Երեմյան </span>
                    <span style="border-bottom: 1px solid;padding: 0 5px;width: 75px;display: inline-block;margin-top: 3px;"></span>
                </div>

                <div style="margin-top: 20px;padding-left: 15px;">Կ.Տ.</div>
            </td>

            @if($invoice->indiivdual == 1)
                <td style="width: 40%">
                    <span style="font-weight: bold;display: block">Career</span><br>
                    <div>
                        <div>
                            <span style="margin-bottom: 5px;display: block">Տնօրեն`</span>
                            {{--<span>fghfhhfg</span>--}}
                            <span style="font-style: italic">{{$invoice->director}}</span>
                            <span style="border-bottom: 1px solid;padding: 0 5px;width: 75px;display: inline-block;margin-top: 3px;"></span>
                        </div>
                    </div>
                    <div style="margin-top: 10px;">
                        <div>
                            {{--<span>Tnoren`</span><br>--}}
                            {{--<span>gfhhgh</span>--}}
                            <span style="font-style: italic">{{$invoice->position}}</span>
                            <span style="border-bottom: 1px solid;padding: 0 5px;width: 75px;display: inline-block;margin-top: 3px;"></span>
                        </div>
                    </div>
                    <div style="margin-top: 30px;padding-left: 15px;">Կ.Տ.</div>
                </td>
            @endif
        </tr>
        </tbody>


    </table>
</div>
<div style="margin-top: 50px;">
    <h2 style="text-align: center;font-weight: normal">
        Փոխանցման հաշիվ

    </h2>
    <div style="border: 1px solid black;width: 700px">
        <div>
            <table style="border-collapse: collapse;width: 700px;max-width: 700px;">
                <tbody style="vertical-align: top;">
                <tr>
                    <td style="border:1px solid;padding: 1px;width: 58%">
                        <table>
                            <tr>
                                <td style="padding-right: 10px;text-align: right;font-weight: bold;width: 120px;">{{trans('invoice.performer')}}</td>
                                <td style="font-weight: bold;font-style: italic">{{$invoice->performer}}</td>
                            </tr>
                            <tr>
                                <td style="padding-right: 10px;text-align: right;font-weight: bold">{{trans('invoice.address')}}</td>
                                <td style="font-style: italic"> {{$invoice->performer_address}}</td>
                            </tr>
                            <tr>
                                <td style="padding-right: 10px;text-align: right;font-weight: bold">
                                {{trans('invoice.account')}}
                                <td style="width: 170px">    {{$invoice->performer_account}}</td>

                                <td style="padding-right: 10px;text-align: right;font-weight: bold">{{trans('invoice.tax')}}</td>
                                <td>    {{$invoice->performer_tax}}</td>

                            </tr>
                            <tr>
                                <td style="padding-right: 10px;text-align: right;font-weight: bold">{{trans('invoice.bank')}}</td>
                                <td>  {{$invoice->performer_bank}}</td>
                            </tr>
                        </table>

                    </td>
                    <td style="border:1px solid;padding: 1px 0 0 10px;width: 40%">
                        <div style="font-style: italic">{{trans('invoice.booking_number')}}
                            N {{$invoice->booking_number}}</div>
                        <span style="font-weight: bold">{{trans('invoice.account_product')}}
                            N </span>
                        <span style="text-decoration: underline; margin-left: auto; padding: 0 5px;">{{$invoice->account_product}}</span>
                        <br>
                        {{ \Carbon\Carbon::parse($invoice->date)->format('d M Y')}}
                    </td>
                </tr>
                <tr>

                    <td style="border:1px solid;padding: 1px;width: 58%">
                        <table>
                            <tr>
                                <td style="padding-right: 10px;text-align: right;font-weight: bold;width: 120px;">{{trans('invoice.client')}}</td>
                                <td style="font-weight: bold;font-style: italic">{{$invoice->client}}</td>
                            </tr>
                            <tr>
                                <td style="padding-right: 10px;text-align: right;font-weight: bold">{{trans('invoice.address')}}</td>
                                <td style="font-style: italic"> {{$invoice->client_address}}</td>
                            </tr>
                            @if($invoice->individual == 0)
                                <tr>

                                    <td style="padding-right: 10px;text-align: right;font-weight: bold">
                                    {{trans('invoice.account')}}
                                    <td style="width: 170px">    {{$invoice->client_account}}</td>

                                    <td style="padding-right: 10px;text-align: right;font-weight: bold">{{trans('invoice.tax')}}</td>
                                    <td>    {{$invoice->client_tax}}</td>

                                </tr>
                                <tr>
                                    <td style="padding-right: 10px;text-align: right;font-weight: bold">{{trans('invoice.bank')}}</td>
                                    <td>  {{$invoice->client_bank}}</td>
                                </tr>

                            @else
                                <tr>
                                    <td style="padding-right: 10px;text-align: right;font-weight: bold">{{trans('invoice.passport')}}</td>
                                    <td> {{$invoice->passport}}</td>
                                </tr>

                            @endif


                        </table>

                    </td>


                    <td style="border:1px solid;padding: 1px 0 0 10px; border-bottom: 0">
                        @if(isset($invoice->posted_announcements) && $invoice->posted_announcements!='' )
                            <div style="text-align: center;font-style: italic">{{trans('invoice.posted_announcements')}}</div>
                            - {{$invoice->posted_announcements}}
                        @endif
                    </td>
                </tr>
                @if(isset($invoice->notes) && $invoice->notes!='' )
                    <tr>
                        <td style="border:1px solid;padding: 1px 10px 3px; border-bottom: 0" colspan="2">
                            <span style="font-weight: bold">{{trans('invoice.notes')}}</span>
                            {{$invoice->notes}}
                        </td>
                    </tr>
                @endif
                <tr>
                    <td style="border:1px solid;padding: 1px 10px 3px; border-bottom: 0" colspan="2">
                        <span style="font-weight: bold">Լրացում` </span> Փոխանցման համար , ԱԱՀ չվճարող: Ծառայությունները
                        մատուցվում են ինտերնետային կայքում:
                    </td>
                </tr>

                </tbody>


            </table>

        </div>
        <div>

            <table style="border-collapse: collapse;width: 700px;text-align: center;max-width: 700px;">
                <tr>
                    <td style="width: 70px;border:1px solid;padding: 1px 5px; font-weight: bold">hamaar</td>
                    <td style="border:1px solid;padding: 1px 5px; font-weight: bold">{{trans('invoice.name')}}</td>
                    <td style="width: 80px;border:1px solid;padding: 1px 5px; font-weight: bold">{{trans('invoice.quantity')}}</td>
                    <td style="border:1px solid;padding: 1px 5px; font-weight: bold">{{trans('invoice.measure')}}</td>
                    <td style="border:1px solid;padding: 1px 5px; font-weight: bold">{{trans('invoice.amount')}}
                        ({{$invoice->currency}})
                    </td>
                    <td style="border:1px solid;padding: 1px 5px; font-weight: bold">{{trans('invoice.sum')}}
                        ({{$invoice->currency}})
                    </td>
                </tr>
                @foreach($invoice->invoiceItems as $item)

                    <tr>
                        <td style="border:1px solid;padding: 1px 5px;">
                            1
                        </td>
                        <td style="border:1px solid;padding: 1px 5px;">
                            {{$item->name}}
                        </td>

                        <td style="border:1px solid;padding: 1px 5px;">
                            {{$item->quantity}}
                        </td>

                        <td style="border:1px solid;padding: 1px 5px;">
                            {{$item->measure}}
                        </td>

                        <td style="border:1px solid;padding: 1px 5px;">
                            {{$item->amount}}
                        </td>

                        <td style="border:1px solid;padding: 1px 5px;">
                            {{$item->sum}}
                        </td>
                    </tr>
                @endforeach


                <tr>
                    <td style="border:1px solid;padding: 1px 5px;"></td>
                    <td style="border:1px solid;padding: 1px 5px; font-weight: bold">{{trans('invoice.total')}}</td>
                    <td style="border:1px solid;padding: 1px 5px;"></td>
                    <td style="border:1px solid;padding: 1px 5px;"></td>
                    <td style="border:1px solid;padding: 1px 5px;"></td>

                    <td style="border:1px solid;padding: 1px 5px;font-weight: bold;font-size: 12px;font-style: italic">
                        {{$invoice->money}} ({{$invoice->currency}})
                    </td>


                </tr>
                <tr>
                    <td style="border:1px solid;padding: 2px 5px;" colspan="6"></td>
                </tr>
                <tr>
                    <td style="border:1px solid;padding: 1px 5px;border-right: 0;"></td>
                    <td colspan="5" style="border:1px solid;padding: 1px 5px;text-align: left;border-left: 0">Քսանվեց
                        հազար
                        հինգ հարյուր ՀՀ Դրամ

                    </td>
                </tr>
            </table>


        </div>


    </div>
    <table style="margin-top: 20px;width: 100%">
        <tbody style="vertical-align: top">
        <tr>
            <td style="width: 60%">
                <span style="font-weight: bold;display: block">"ՊՐՈՖԷՔՍ" ՍՊԸ</span><br>
                <div>
                    <span style="margin-bottom: 5px;display: block">Տնօրեն`</span>
                    <span style="font-style: italic">Ա. Երեմյան </span>
                    <span style="border-bottom: 1px solid;padding: 0 5px;width: 75px;display: inline-block;margin-top: 3px;"></span>
                </div>

                <div style="margin-top: 20px;padding-left: 15px;">Կ.Տ.</div>
            </td>

            @if($invoice->indiivdual == 1)
                <td style="width: 40%">
                    <span style="font-weight: bold;display: block">Career</span><br>
                    <div>
                        <div>
                            <span style="margin-bottom: 5px;display: block">Տնօրեն`</span>
                            {{--<span>fghfhhfg</span>--}}
                            <span style="font-style: italic">{{$invoice->director}}</span>
                            <span style="border-bottom: 1px solid;padding: 0 5px;width: 75px;display: inline-block;margin-top: 3px;"></span>
                        </div>
                    </div>
                    <div style="margin-top: 10px;">
                        <div>
                            {{--<span>Tnoren`</span><br>--}}
                            {{--<span>gfhhgh</span>--}}
                            <span style="font-style: italic">{{$invoice->position}}</span>
                            <span style="border-bottom: 1px solid;padding: 0 5px;width: 75px;display: inline-block;margin-top: 3px;"></span>
                        </div>
                    </div>
                    <div style="margin-top: 30px;padding-left: 15px;">Կ.Տ.</div>
                </td>
            @endif
        </tr>
        </tbody>


    </table>

</div>

</body>
</html>