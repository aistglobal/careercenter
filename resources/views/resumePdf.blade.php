<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <style>
        @font-face {
            font-family: "Dojo Sans Serif";
            font-style: normal;
            font-weight: normal;
            src: url(http://example.com/fonts/dojosans.ttf) format('truetype');
        }

        * {
            box-sizing: border-box;
            font-family: "Dojo Sans Serif", "DejaVu Sans", sans-serif;

        }

        body {
            font-size: 15px;
        }

        .lang-table {
            font-size: 13px;
        }

    </style>
</head>
<body>

<section>

    <div style="padding-left: 15px;">
        <div style="width:480px; float: left">
            <div style="font-size: 25px;margin: 20px 0;font-weight: bold">{{$cv->contacts->first_name}}  {{$cv->contacts->last_name}}</div>
            <div>

                @if(count($cv->address)!=0)
                    @if(isset($cv->address[0]->address))
                    {{$cv->address[0]->address}},
                    @endif
                        @if(isset($cv->address[0]->city))
                    {{$cv->address[0]->city}}
                        @endif
                        @if(isset($cv->address[0]->zip))
                     , {{$cv->address[0]->zip}}
                        @endif
                        @if(isset($cv->address[0]->country_id))
                            {{trans('countries.'.$cv->address[0]->country->code)}}
                        @endif
                @endif

            </div>
            @if(isset($phone))
                <div>
                    {{trans('resume.phone')}}: <a href="tel:{{$phone[0]["phone"]}}"
                                                  style="color: #014A7C;">{{$phone[0]["phone"]}}</a>
                </div>
            @endif
            @if(isset($cv->contacts->email))
                <div>
                    {{trans('resume.email')}}: <a
                            href="mailto:{{$cv->contacts->email}}"
                            style="color: #014A7C;">{{$cv->contacts->email}}</a>
                </div>
            @endif
            @if(isset($cv->personal))
                @if(isset($cv->personal->birthday))
                    <div>
                        {{trans('resume.birth_date')}}:{{$cv->personal->birthday}}
                    </div>
                @endif
                @if(isset($cv->personal->marital_status))
                    <div>
                        {{trans('resume.marital_status')}}: {{trans('cv_personal.'.$cv->personal->marital_status)}}
                    </div>
                @endif
                @if(isset($cv->personal->sex))
                    <div>
                        {{trans('resume.sex')}}: {{trans('resume.'.$cv->personal->sex)}}
                    </div>
                @endif
                @if(isset($cv->personal->height))
                    <div>
                        {{trans('resume.height')}}:
                        {{$cv->personal->height}} {{trans('cv_personal.'.$cv->personal->height_measurement)}}
                    </div>
                @endif
                @if(isset($cv->personal->weight))
                    <div>
                        {{trans('resume.weight')}}:
                        {{$cv->personal->weight}}   {{trans('cv_personal.'.$cv->personal->weight_measurement)}}
                    </div>
                @endif
                @if(isset($cv->personal->nationality_id))
                    <div>
                        {{trans('countries.'.$cv->personal->nationality->code)}}
                    </div>
                @endif
                @if(isset($personal_citizenship) && count($personal_citizenship)!=0)
                    <div>
                        {{trans('resume.citizenship')}}:
                        @foreach($countries as $item)
                            @if(in_array($item->id, $personal_citizenship))
                                {{trans('countries.'.$item->code)}}
                            @endif
                        @endforeach

                    </div>
                @endif
                @if(isset($cv->personal->official_service))
                    <div>
                        {{trans('resume.official_service')}}
                        :{{trans('cv_personal.'.$cv->personal->official_service)}}
                    </div>
                @endif

                @if(isset($cv->personal->dependants))
                    <div>
                        {{trans('resume.dependants')}}:
                        {{trans('resume.'.$cv->personal->dependants)}}
                    </div>
                    @if($cv->personal->dependants == 'yes' && isset($cv->dependants) && count($cv->dependants)!=0)
                        <div>
                            @foreach($cv->dependants as $item)
                                <div>
                                    {{trans('cv_personal.'.$item->relationship)}}:

                                    <span>
                                        {{$item->name}} {{$item->birthday}}
                                    </span>
                                </div>



                            @endforeach
                        </div>

                    @endif
                @endif
                <div>{{trans('resume.disabled')}}:</div>
                <div>{{trans('resume.'.$cv->personal->disabled)}}</div>

                @if($cv->personal->disabled=='yes' && isset($cv->personal->disabled_description))
                    <div>{{trans('resume.disabled_details')}}:
                        {{trans('resume.disabled_details')}}
                    </div>
                @endif
                @if(isset($phone) && count($phone)>1)
                    <div>
                        {{trans('resume.mobile')}}:
                        @for($i=1;$i<count($phone);$i++)
                            {{$phone[$i]['phone']}}
                        @endfor
                    </div>
                @endif
            @endif
        </div>
        @if(isset($cv->personal->image))
            <div style="width: 200px; float: right; position: relative;overflow: hidden;height: 250px">
                <img src="{{URL::to('/')}}/{{Storage::disk('')->url($cv->personal->image)}}"
                     style="position: relative;width: 100%;">
            </div>
        @endif
    </div>


        @if(isset($cv->objective))
            <div style="clear: both">
                <div style="background:#006DB7;font-weight:bold;margin: 20px 0;font-size:17px;color: white;padding: 5px 15px">
                    {{trans('resume.personal_summary')}}</div>
                <div style="padding:0 15px;">
                    {{$cv->objective}}
                </div>
            </div>
        @endif


    @if(count($cv->education)!=0)
        <div style="clear: both">
            <div style="background:#006DB7;font-weight:bold;margin: 20px 0;font-size:17px;color: white;padding: 5px 15px">{{trans('resume.background_education')}}</div>
            @foreach($cv->education as $item)
                <div style="width: 100%; clear: both;padding: 0 15px 20px;">
                    <div style="float: left;padding-bottom: 20px;width: 75%">
                        <div>
                            {{\App\Cv::educationName($item->name,trans('cv_education.'.$item->type))}}
                            , {{$item->department}}
                            ,
                            {{trans('cv_education.'.$item->degree)}}
                        </div>
                        <div>{{trans('category.'.$item->study_area)}}, ({{$item->major_field}})</div>
                    </div>
                    <div style="float:right;padding-bottom: 20px;width: 25%;text-align: right">
                        <div>{{$item->city}}, {{trans('countries.'. $item->country->code)}}</div>
                        <div>
                            {{Carbon\Carbon::parse($item->period_from.'-02')->format('Y/m')}} -
                            {{Carbon\Carbon::parse($item->period_to.'-02')->format('Y/m')}}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
    @if(count($cv->language)!=0)
        <div style="clear: both">
            <div style="background:#006DB7;font-weight:bold;margin: 20px 0;font-size:17px;color: white;padding: 5px 15px">
                {{trans('resume.language_skills')}}</div>
            <div style="padding: 0;">
                <table style="border-collapse: collapse;word-break: break-all;max-width: 727px;" class="lang-table">
                    <tr>
                        <th style="background: #f2f2f4;padding: 0 5px;word-break: break-all;">{{trans('resume.language')}}</th>
                        <th style="background: #f2f2f4;padding: 0 5px;word-break: break-all;">{{trans('resume.reading')}}</th>
                        <th style="background: #f2f2f4;padding: 0 5px;word-break: break-all;">{{trans('resume.writing')}}</th>
                        <th style="background: #f2f2f4;padding: 0 5px;word-break: break-all;">{{trans('resume.communication')}}</th>
                        <th style="background: #f2f2f4;padding: 0 5px;word-break: break-all;">{{trans('resume.typing')}}</th>
                        <th style="background: #f2f2f4;padding: 0 5px;word-break: break-all;">{{trans('resume.shorthand')}}</th>
                        <th style="background: #f2f2f4;padding: 0 5px;word-break: break-all;">{{trans('resume.total_study')}}</th>
                    </tr>
                    @foreach($cv->language as $item)
                        <tr style="text-align: center">
                            <td style="padding: 0 5px;word-break: break-all">{{trans('languages.'.$item->language->name)}}
                                ({{trans('resume.'.$item->language_type)}})
                            </td>
                            <td style="padding: 0 5px;word-break: break-all;word-wrap: break-word">{{trans('cv_language_skills.'.$item->reading)}}</td>
                            <td style="padding: 0 5px;word-break: break-all;word-wrap: break-word">{{trans('cv_language_skills.'.$item->writing)}}</td>
                            <td style="padding: 0 5px;word-break: break-all;word-wrap: break-word">{{trans('cv_language_skills.'.$item->communication)}}</td>
                            <td style="padding: 0 5px;word-break: break-all;word-wrap: break-word">{{$item->typing}}</td>
                            <td style="padding: 0 5px;word-break: break-all;word-wrap: break-word">{{$item->shorthand}}</td>
                            <td style="padding: 0 5px;word-break: break-all;word-wrap: break-word">{{trans('cv_language_skills.'.$item->total_study)}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    @endif

    @if(count($cv->experience)!=0)
        <div style="clear: both">
            <div style="background:#006DB7;font-weight:bold;margin: 20px 0;font-size:17px;color: white;padding: 5px 15px">{{trans('resume.professional_experience')}}</div>
            @foreach($cv->experience as $item)
                <div style="width: 100%; clear: both;padding:0 15px 20px;">
                    <div style="float: left;padding-bottom: 20px;width: 75%">
                        <div>{{$item->name}}</div>
                        <div>{{$item->position}}, {{trans('category.'.$item->position_field)}}
                        </div>
                    </div>
                    <div style="float:right;padding-bottom: 20px;width: 25%;text-align: right">
                        <div>{{$item->city}}, {{trans('countries.'.$item->country->code)}}</div>
                        <div>
                            {{Carbon\Carbon::parse($item->period_from.'-02')->format('Y/m')}} -

                            @if($item->period_to=='-')
                                {{trans('resume.till_now')}}
                            @else
                                {{Carbon\Carbon::parse($item->period_to.'-02')->format('Y/m')}}
                            @endif

                        </div>
                    </div>
                </div>
                @if(isset($item->main_duties))
                    <div style="clear: both; padding: 0 15px">
                        <i>{{trans('resume.main_duties')}}: </i>
                        <ul>
                            <li>{{$item->main_duties}}</li>
                        </ul>
                    </div>
                @endif
                @if(isset($item->job_description))
                    <div style=" padding: 0 15px">
                        <i>{{trans('resume.job_description')}}: </i>
                        <ul>
                            <li>  {{$item->job_description}}</li>
                        </ul>

                    </div>
                @endif

                @if(isset($item->salary))
                    <div style="padding: 0 15px 15px;">
                        <i>{{trans('resume.salary')}}: </i>
                        <span>
                                {{$item->salary}} {{trans('currency.'.$item->currency)}} {{trans('currency.'.$item->period)}}
                            </span>

                    </div>
                @endif
                @if(isset($item->supervisor_name))
                    <div style="padding: 0 15px 15px;">
                        <i>{{trans('resume.supervisor_name')}}: </i>
                        <span>
                                {{$item->supervisor_name}}
                            </span>

                    </div>
                @endif
                @if(isset($item->employer_numbers))
                    <div style="padding: 0 15px 15px;">
                        <i>{{trans('resume.employer_numbers')}}: </i>
                        <span>
                                {{$item->employer_numbers}}
                            </span>
                    </div>
                @endif

                @if(isset($item->reason))
                    <div style="padding: 0 15px 15px;">
                        <i>{{trans('resume.reason')}}: </i>
                        <span>
                                {{$item->reason}}
                            </span>
                    </div>
                @endif

            @endforeach

        </div>
    @endif

    @if(count($cv->training)!=0)
        <div style="clear: both">
            <div style="background:#006DB7;font-weight:bold;margin: 20px 0;font-size:17px;color: white;padding: 5px 15px">{{trans('resume.short_term_training')}}</div>
            @foreach($cv->training as $item)
                <div style="width: 100%; clear: both;padding: 0 15px 20px;">
                    <div style="float: left;padding-bottom: 20px;width: 75%">
                        <div>{{$item->name}} {{trans('cv_training.'.$item->training_type)}}</div>
                        <div class="mr-2">{{$item->course_name}}, {{$item->certificate_type}}
                            ({{trans('categories.'.$item->study_area)}})
                        </div>
                    </div>
                    <div style="float:right;padding-bottom: 20px;width: 25%;text-align: right">
                        <div>{{$item->city}}, {{trans('countries.'.$item->country->code)}}</div>
                        <div>
                            {{Carbon\Carbon::parse($item->period_from.'-02')->format('Y/m')}} -
                            {{Carbon\Carbon::parse($item->period_to.'-02')->format('Y/m')}}
                        </div>
                    </div>
                </div>

            @endforeach

        </div>
    @endif

    @if(count($cv->visa)!=0)
        <div style="clear: both">
            <div style="background:#006DB7;font-weight:bold;margin: 20px 0;font-size:17px;color: white;padding: 5px 15px">{{trans('resume.visa')}}</div>
            @foreach($cv->visa as $item)

                <div style="width: 100%; clear: both;padding: 0 15px 20px;">
                    <div style="float: left;padding-bottom: 20px;width: 75%">
                            {{trans('countries.'.$item->country->code)}}
                            , {{$item->type}} </div>
                    <div style="float:right;padding-bottom: 20px;width: 25%;text-align: right">
                            {{Carbon\Carbon::parse($item->period_from.'-02')->format('Y/m')}} -
                            {{Carbon\Carbon::parse($item->period_to.'-02')->format('Y/m')}}
                        </div>
                    </div>



            @endforeach

        </div>
    @endif

    @if(count($cv->reference)!=0)
        <div style="clear: both">
            <div style="background:#006DB7;font-weight:bold;margin: 20px 0;font-size:17px;color: white;padding: 5px 15px">{{trans('resume.references')}}</div>
            @foreach($cv->reference as $item)
                <div style="padding: 0 15px 15px;">
                    <div>
                        <div>{{trans('title.'.$item->title)}} {{$item->full_name}}</div>
                        <div>{{$item->position}}, {{trans('cv_reference.'.$item->relationship)}}
                        </div>
                    </div>
                </div>
                @if(isset($item->current_position))
                    <div style="padding: 0 15px 15px;">
                        <i>{{trans('resume.current_position')}}: </i>
                        <span>
                                {{$item->current_position}}
                            </span>
                    </div>
                @endif
                @if(isset($item->email))
                    <div style="padding: 0 15px 15px;">
                        <i>{{trans('resume.email')}}: </i>
                        <span>
                                {{$item->email}}
                            </span>
                    </div>
                @endif
                @if(isset($item->phone))
                    <div style="padding: 0 15px 15px;">
                        <i>{{trans('resume.phone')}}: </i>

                        <span>{{$item->phone}}</span>

                    </div>
                @endif
                @if(isset($item->duration))
                    <div style="padding: 0 15px 15px;">
                        <i>{{trans('resume.duration')}}: </i>

                        <span>{{$item->duration}}  {{trans('cv_reference.'.$item->duration_type)}}</span>

                    </div>
                @endif
            @endforeach

        </div>
    @endif

    @if(isset($cv->volunteering))
        <div style="clear: both">
            <div style="background:#006DB7;font-weight:bold;margin: 20px 0;font-size:17px;color: white;padding: 5px 15px">{{trans('resume.volunteering')}}</div>


            <div style="padding: 0 15px 15px;">
                <i>{{trans('resume.willing_to_volunteer')}} : </i>
                <span>
                        {{trans('resume.'.$cv->volunteering->willing)}}
                    </span>

            </div>
            @if($cv->volunteering->willing=='yes')
                <div style="padding: 0 15px 15px;">
                    <i>{{trans('resume.public_woks')}} : </i>
                    <span>
                            {{trans('resume.'.$cv->volunteering->public_work)}}
                        </span>

                </div>
            @endif
            @if(isset($work_type))
                <div style="padding: 0 15px 15px;">
                    <i>{{trans('resume.work_type')}} : </i>
                    <ul class="mb-0">
                        @foreach(config('enums.work_type') as $item)
                            @if(in_array($item, $work_type))
                                <li>
                                    {{trans('cv_volunteering.'.$item)}}
                                </li>
                            @endif
                        @endforeach
                    </ul>

                </div>
            @endif
            @if(isset($volunteering_countries))
                <div style="padding: 0 15px 15px;">
                    <i>{{trans('resume.countries')}} : </i>
                    <ul>
                        @foreach($countries as $item)
                            @if(in_array($item->id, $volunteering_countries))
                                <li>
                                    {{trans('countries.'.$item->code)}}
                                </li>
                            @endif
                        @endforeach
                    </ul>

                </div>
            @endif

        </div>
    @endif
    @if(isset($cv->computer_skills))
        <div style="clear: both">
            <div style="background:#006DB7;font-weight:bold;margin: 20px 0;font-size:17px;color: white;padding: 5px 15px">{{trans('resume.computer_skills')}}</div>
            @if(isset($cv->computer_skills->level))
                <div style="padding:0 15px 15px;">
                    <i>{{trans('resume.level')}}: </i>
                    <span>
                            {{trans('cv_computer_skills.'.$item->level)}}
                        </span>
                </div>
            @endif

            @if(isset($cv->computer_skills->software))
                <div style="padding: 0 15px 15px;">
                    <i>{{trans('resume.software')}}: </i>
                    <span>
                           {{$cv->computer_skills->software}}
                      </span>
                </div>
            @endif
            @if(isset($cv->computer_skills->hardware))
                <div style="padding: 0 15px 15px;">
                    <i>{{trans('resume.hardware')}}: </i>

                    <span>{{$cv->computer_skills->hardware}}</span>

                </div>
            @endif
        </div>
    @endif
    @if(isset($cv->availability))
        <div style="clear: both">
            <div style="background:#006DB7;font-weight:bold;margin: 20px 0;font-size:17px;color: white;padding: 5px 15px">{{trans('resume.availability')}}</div>

            <div style="padding: 0 15px;">
                <i>{{trans('resume.availability')}}: </i>
                <ul>
                    @if(isset($cv->availability->period_from) && isset($cv->availability->period_to))
                        <li>
                            {{Carbon\Carbon::parse($cv->availability->period_from)->format('d.m.Y')}}
                            -{{Carbon\Carbon::parse($cv->availability->period_to)->format('d.m.Y')}}
                        </li>
                    @endif
                    @if(isset($cv->availability->hours_from) && $cv->availability->hours_from )
                        <li>
                            {{$cv->availability->hours_from}} - {{$cv->availability->hours_to}}
                        </li>
                    @endif
                    @if(isset($availability_days))
                        <li>
                            @foreach(config('enums.days') as $item)
                                @if(in_array($item,$availability_days))
                                    {{trans('cv_availability.'.$item)}}
                                @endif
                            @endforeach
                        </li>
                    @endif
                </ul>
            </div>
            @if(isset($cv->availability->short_salary))
                <div style="padding: 0 15px 15px;">
                    <i>{{trans('resume.short_term_salary')}}: </i>
                    <span>
                            {{$cv->availability->short_salary}} {{trans('currency.'.$cv->availability->short_currency)}} {{trans('period.'.$cv->availability->short_period)}}
                        </span>
                </div>
            @endif
            @if(isset($cv->availability->long_salary))
                <div style="padding: 0 15px 15px;">
                    <i>{{trans('resume.long_term_salary')}}: </i>
                    <span>
                            {{$cv->availability->long_salary}} {{trans('currency.'.$cv->availability->long_currency)}} {{trans('period.'.$cv->availability->long_period)}}
                        </span>
                </div>
            @endif
            @if(isset($availability_terms))
                <div style="padding: 0 15px 15px;">
                    <i>{{trans('resume.terms')}}: </i>
                    <ul>

                        <li>
                            @foreach(config('enums.terms') as $item)
                                @if(in_array($item,$availability_terms))
                                    {{trans('cv_availability.'.$item)}}
                                @endif
                            @endforeach
                        </li>
                    </ul>
                </div>
            @endif


        </div>
    @endif

    @if(isset($cv->special_skills))
        <div style="clear: both">
            <div style="background:#006DB7;font-weight:bold;margin: 20px 0;font-size:17px;color: white;padding: 5px 15px">{{trans('resume.special_skills')}}</div>
            <ul>
                <li>{{$cv->special_skills}}</li>
            </ul>
        </div>
    @endif
    @if(isset($cv->questions))
        <div style="clear: both">
            <div style="background:#006DB7;font-weight:bold;margin: 20px 0;font-size:17px;color: white;padding: 5px 15px">
                {{trans('resume.questions')}}</div>
            <div style="padding: 0 15px 15px;">
                <i>{{trans('cv_questions.make_inquiries')}} {{trans('cv_questions.present_employer')}}: </i>
                <span>
                        {{trans('resume.'.$cv->questions->present_employer)}}
                    </span>
            </div>

            <div style="padding: 0 15px 15px;">
                <i>{{trans('cv_questions.make_inquiries')}} {{trans('cv_questions.previous_employer')}}: </i>
                <span>
                        {{trans('resume.'.$cv->questions->previous_employer)}}
                    </span>
            </div>
            @if(isset($cv->questions->government))
                <div style="padding: 0 15px 15px;">
                    <i>{{trans('cv_questions.government')}}: </i>
                    <span>
                            {{trans('resume.'.$cv->questions->government)}}
                        </span>
                </div>
            @endif
            @if(isset($cv->questions->arrested))
                <div style="padding: 0 15px 15px;">
                    <i>{{trans('cv_questions.arrested')}}: </i>
                    <span>
                            {{trans('resume.'.$cv->questions->arrested)}}
                        </span>
                </div>
            @endif

            @if(isset($cv->questions->arrested_details))
                <div style="padding: 0 15px 15px;">
                    <i>{{trans('cv_questions.arrested_details')}}: </i>
                    <div>
                        {{trans('resume.'.$cv->questions->arrested_details)}}
                    </div>
                </div>
            @endif
        </div>
    @endif

    @if(count($cv->attachment)!=0)
        <div style="clear: both">
            <div style="background:#006DB7;font-weight:bold;margin: 20px 0;font-size:17px;color: white;padding: 5px 15px">{{trans('resume.attachments')}}</div>

            <div style="clear: both;">
                @foreach($cv->attachment as $item)
                    @if($item->is_image==1)
                        <a href="{{URL::to('/')}}{{Storage::disk('')->url($item->attachment)}}" target="_blank"
                           style="display: inline-block;width: 220px">
                            <img src="{{URL::to('/')}}{{Storage::disk('')->url($item->attachment)}}"
                                 style="width: 200px; height: 200px;object-fit: cover;margin: 45px 15px">
                        </a>
                    @else

                        <p style="padding: 0 15px;">
                            <a href="{{URL::to('/')}}/{{Storage::disk('')->url($item->attachment)}}" download
                               target="_blank"
                               style="color: #014A7C">{{$item->description}}</a>
                        </p>
                    @endif



                @endforeach
            </div>

        </div>

    @endif
    <div class="print" style="padding:0 15px">
        <div>
            <span class="bold-text">{{trans('resume.added_by')}}: </span> {{$cv->user->username}}
        </div>
        <div>
            <span class="bold-text">{{trans('resume.on')}}: </span>{{$cv->created_at->format('d.m.Y')}}
        </div>
    </div>
</section>
</body>
</html>