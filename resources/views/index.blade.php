@extends('layouts.app')

@section('css')
    <link href="{{ asset('css/home.css') }}" rel="stylesheet">
@endsection

@section('content')

    <section class="home-head">
        <div class="home-head-container text-center">
            <h1>{{trans('home.job_for_everyone')}}</h1>
            <p>
                {{trans('home.job_description')}}
            </p>
            <form class="home-head-form d-flex" onsubmit="window.location = '/jobs?search='+ search.value;return false;">
                <input  placeholder="{{trans('home.job_title_skill')}}" aria-label="search job" class="form-control h-100" type="search" id="search">
                <button type="submit" class="btn btn-blue-gradient">{{trans('home.search_job')}}</button>
            </form>
        </div>
        <img src="/img/home-head-img.svg" alt="Job for everyone" class="w-100 home-head-img position-relative">
    </section>

    <section class="home-resume-section position-relative">
        <div class="home-resume-section-container d-flex justify-content-between align-items-center">
            <div class="home-resume-left">
                <h2 class="font-bold">{{trans('home.need_resume')}}</h2>
                <p> {{trans('home.resume_description')}}</p>
                <a href="{{route('resume.index')}}" class="btn btn-blue-gradient">{{trans('home.create_resume')}}</a>
            </div>
            <div class="p-0-15">
                <img src="/img/home-resume.svg" alt="Need a resume?" class="img-fluid">
            </div>
        </div>
        <img src="/img/home-resume-bg.svg" alt="Need a resume?" class="w-100 h-100 home-resume-bg">
    </section>

    <section class="home-post w-100 d-flex justify-content-between align-items-center">
        <div class="p-0-15">
            <img src="/img/CEO.png" alt="Post an Announcement" class="img-fluid">
        </div>
        <div class="home-post-right">
            <h2 class="font-bold">{{trans('home.post_announcement')}}</h2>
            <p>
                {{trans('home.announcement_description')}}
            </p>
            @guest<a href="{{route('make-job','job_opp')}}" class="btn btn-blue-gradient">{{trans('home.create_now')}}</a>@endguest
        </div>
    </section>

    <section class="position-relative">
        <div class="text-center home-education">
            <h2 class="font-bold">{{trans('home.training')}}</h2>
            <p>
                {{trans('home.education_description')}}
            </p>
            <a href="{{route('pages','english-language-courses')}}" class="btn btn-blue">{{trans('home.english')}}</a>
            <a href="{{route('pages','russian-language-courses')}}" class="btn btn-green">{{trans('home.russian')}}</a>
            <a href="{{route('pages','intensive-toefl-ibt-and-toefl-itp-preparation-courses')}}" class="btn btn-blue">{{trans('home.toifl')}}</a>
        </div>

        <picture>
            <source data-srcset="/img/home-education.webp" type="image/webp">
            <source data-srcset="/img/home-education.png" type="image/png">
            <img data-src="/img/home-education.png" alt="Education" class="lazyload home-education-bg">
        </picture>
    </section>

    <section class="home-articles">
        <h2 class="font-bold text-center">{{trans('home.latest_articles')}}</h2>
        <div class="home-articles-slider">
            <div class="home-articles-slider-container" id="home-articles-slider">
                <!--foreach here-->
                @foreach($blogs as $item)
                <div>
                    <a href="{{route('blog-single',$item->slug)}}" class="home-articles-slider-item">
                        <div class="position-relative item-hover">
                            <picture>
                                {{--<source data-srcset="/img/home-slide-1.webp" type="image/webp">--}}
                                <source data-srcset="{{Storage::disk('')->url($item->image)}}" type="image/jpeg">
                                <img data-src="{{Storage::disk('')->url($item->image)}}" class="lazyload w-100"  width="436" height="291">
                            </picture>
                            <div class="home-articles-slider-text text-center w-100">
                                <h3 class="font-bold">{{$item->data->title}}</h3>
                                <p class="mb-0"> {{$item->data->short_description}}</p>
                            </div>
                        </div>
                        <span class="read-more font-bold text-center d-block">{{trans('home.read_more')}}</span>
                    </a>
                </div>
                @endforeach

                <!--end foreach-->
            </div>
        </div>
    </section>

    <section class="home-people position-relative">
        <h2 class="font-bold text-center">{{trans('home.what_people_say')}}</h2>
        <img src="/img/people-slider-bg.svg" alt="What people say" class="people-slider-bg w-100">
        <div class="home-people-slider-container" id="home-people-slider">
            <!--foreach here-->
            @foreach($reviews as $item)
            <div>
                <a href="{{route('review')}}" class="home-people-slider-item d-flex flex-column">
                    <div class="d-flex">
                        <picture>
                            {{--<source data-srcset="/img/home-people-1.webp" type="image/webp">--}}
                            <source data-srcset="{{Storage::disk('')->url($item->image)}}" type="image/jpeg">
                            <img data-src="{{Storage::disk('')->url($item->image)}}" alt="user" class="lazyload"  width="67" height="67">
                        </picture>
                        <div class="home-people-info">
                            <h4 class="font-bold">{{$item->data->name}}</h4>
                            <h5 class="font-bold">{{$item->data->profession}}</h5>
                        </div>
                    </div>
                    <p>{{ substr($item->data->review, 0, 100) }}</p>
                    <span class="read-more font-bold text-right d-block mt-auto">{{trans('home.read_more')}}</span>
                </a>
            </div>
            @endforeach
            <!--end foreach-->
        </div>
    </section>

@endsection


