@extends('layouts.app')

@section('css')
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="/css/select2.min.css">
    <style>
        .not-dash {
            float: left;
            margin-top: 0 !important;
            clear: both;
        }

        footer {
            display: none;
        }

        @media (min-width: 1200px) {
            .select2-container--default .select2-results > .select2-results__options {
                max-height: inherit;
            }

        }

    </style>

@endsection

@section('content')
    <!--job search-->
    <header class="job-search-header position-relative">

        <span class="close-search">
            <img src="/img/close.svg" alt="">
        </span>

        <img src="/img/search-bg.png" alt="search" class="job-search-wave">
        <form id="filter_form" action="{{route('jobs')}}" class="job-search-form m-auto d-flex w-100">
            <input type="hidden" value="{{$status}}" name="status">
            <input type="hidden" value="{{$view}}" name="view">
            <div class="form-group d-flex flex-column form-group-category">
                <select name="category_id" class="js-example-placeholder-single js-states form-control"
                        id="job-category">
                    <option></option>
                    @foreach($all_categories as $item)
                        @if(!Auth::check() || !Auth::user()->can('accessAll_announcement'))

                            @if(App\Job::getCount($status,$item->id,$sub_category_id,$type)!= '')
                                <option @if($category_id==$item->id) selected @endif
                                value="{{$item->id}}">{{trans('category.'.$item->name)}}
                                    @if($item->name!='all_fields')
                                        {{\App\Job::getCount($status,$item->id,$sub_category_id,$type)}}
                                    @endif
                                </option>
                            @endif


                        @else
                            <option @if($category_id==$item->id) selected @endif
                            value="{{$item->id}}">{{trans('category.'.$item->name)}}
                                @if($item->name!='all_fields')
                                    {{\App\Job::getCount($status,$item->id,$sub_category_id,$type)}}
                                @endif
                            </option>
                        @endif

                    @endforeach
                </select>
            </div>

            <div id="sub_category_div" class="form-group-types"
                 style="display:@if(isset($category_id) && $category_id!=1) block @else none @endif;">
                <div class="form-group d-flex flex-column  position-relative">
                    <select id="sub_category" name="sub_category_id"
                            class="js-example-placeholder-single js-states form-control">
                        @if(isset($category_id) && isset($all_subtypes))
                            <option value=""></option>
                            @foreach($all_subtypes as $item)

                                @if(!Auth::check() || !Auth::user()->can('accessAll_announcement'))

                                    @if(\App\Job::getCount($status,$category_id,$item->id,$type)!= '')
                                        <option @if($sub_category_id==$item->id) selected
                                                @endif value="{{$item->id}}">{{trans('sub_category_'.$item->parent->name.'.'.$item->name)}}
                                            {{\App\Job::getCount($status,$category_id,$item->id,$type)}}
                                        </option>
                                    @endif


                                @else
                                    <option @if($sub_category_id==$item->id) selected
                                            @endif value="{{$item->id}}">{{trans('sub_category_'.$item->parent->name.'.'.$item->name)}}
                                        {{\App\Job::getCount($status,$category_id,$item->id,$type)}}
                                    </option>
                                @endif



                            @endforeach
                        @endif

                    </select>
                </div>
            </div>

            <div class="form-group d-flex flex-column form-group-types position-relative">
                <select name="type" class="js-example-placeholder-single js-states form-control" id="job-types">
                    <option></option>
                    @foreach($types as $item)
                        @if(!Auth::check() || !Auth::user()->can('accessAll_announcement'))
                            @if(\App\Job::getCount($status,$category_id,$sub_category_id,$item->id,$expired_date) != '')
                                <option @if($type==$item->id) selected @endif value="{{$item->id}}">
                                    {{trans('types.'.$item->key)}}
                                    {{\App\Job::getCount($status,$category_id,$sub_category_id,$item->id,$expired_date)}}
                                </option>
                            @endif
                        @else
                            <option @if($type==$item->id) selected @endif value="{{$item->id}}">
                                {{trans('types.'.$item->key)}}
                                {{\App\Job::getCount($status,$category_id,$sub_category_id,$item->id,$expired_date)}}
                            </option>
                        @endif
                    @endforeach
                </select>
            </div>
            @if($status=="expired")
                <div class="form-group d-flex flex-column form-group-types position-relative">
                    <select name="expired_date" class="js-example-placeholder-single js-states form-control"
                            id="expired-jobs">
                        <option></option>

                        @foreach(\App\Job::expiredJobFilter() as $item)

                            <option @if($expired_date == $item->expired_date)selected
                                    @endif value="{{$item->expired_date}}">{{Carbon\Carbon::parse($item->expired_date)->format('m-Y')}}
                                ({{$item->total}})
                            </option>
                        @endforeach
                    </select>
                </div>
            @endif
            @if($status=="rejected")
                <div class="form-group d-flex flex-column form-group-types position-relative">
                    <select name="rejected_date" class="js-example-placeholder-single js-states form-control"
                            id="rejected-jobs">
                        <option></option>

                        @foreach(\App\Job::rejectedJobFilter() as $item)

                            <option @if($rejected_date == $item->rejected_date)selected
                                    @endif value="{{$item->rejected_date}}">{{Carbon\Carbon::parse($item->rejected_date)->format('m-Y')}}
                                ({{$item->total}})
                            </option>
                        @endforeach
                    </select>
                </div>
            @endif
            <div class="form-group d-flex flex-grow-1 flex-column flex-md-row">
                <input id="search_input" value="{{$title}}" name="search" type="search"
                       placeholder="{{trans('announcements.job_title_keywords')}}">
                <button type="submit" class="btn btn-blue-gradient">{{trans('announcements.search_job')}}</button>
            </div>
        </form>
    </header>
    <!--end job search-->
    <span class="open-search"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="#fff"
                                   viewBox="0 0 24 24"><path
                    d="M15.5,14h-.79l-.28-.27a6.51,6.51,0,1,0-.7.7l.27.28v.79l5,4.99L20.49,19Zm-6,0A4.5,4.5,0,1,1,14,9.5,4.494,4.494,0,0,1,9.5,14Z"/><path
                    d="M0,0H24V24H0Z" fill="none"/></svg></span>
    <div class="container-fluid job-container d-flex justify-content-between mb-5 flex-column flex-md-row position-relative">






        @if($view=='list')
            <span class="change-jobs-style-old"><a
                        href="/{{$localeCode}}/jobs?view=classic"><img
                            src="/img/grid.svg" alt="" width="17" height="18" title="{{trans('announcements.classic')}}"></a></span>

        @else

            <span class="change-jobs-style"><a href="/{{$localeCode}}/jobs?view=list">

                <img src="/img/view-list-button.svg" alt=""
                        width="20" title="{{trans('announcements.list')}}"></a></span>
        @endif
        <span class="open_job-left btn btn-blue-gradient">open search
        </span>
        <span class="close_job-left btn btn-orange-gradient">close search</span>
        @if($jobs->count())

            <div class="job-left @if($view=='list') job-left-list @endif" id="job-left-part">
                @foreach($jobs as $item)
                    @if(isset($item->slug))
                        <a href="{{route('jobs',$item->slug)}}" id="job_{{$item->jobId}}"
                           class="job-single job-item d-flex @if(isset($first) && $first->id == $item->jobId )job-item-active @endif"
                           data-slug="{{$item->slug}}" data-title="{{$item->title}}">
                            @if(isset($item->logo) && $item->logo!='')
                                <picture>

                                    <source srcset="{{Storage::disk('')->url($item->logo)}}" type="image/jpeg">
                                    <img src="{{Storage::disk('')->url($item->logo)}}"
                                         width="113" height="90">
                                </picture>
                            @else
                                <picture>
                                    <source data-srcset="/Logo_Placeholder.png" type="image/png">
                                    <img src="/Logo_Placeholder.png"
                                         width="113" height="90">

                                </picture>
                            @endif
                            <span class="triangle-right"></span>
                            <div class="job-item-info d-flex ml-auto align-items-center">
                                <div class="job-item-info-text flex-grow-1">
                                    @if($view == 'list')
                                        <h5 style="user-select: all;">{{$item->title}}/ {{$item->organization}}</h5>
                                        @else
                                        <h5>@if($view == 'list') {{$item->title}}/ {{$item->organization}} @else {{$item->title}} @endif</h5>
                                            <h6 title="{{$item->organization}}">{{$item->organization}}</h6>
                                        @endif


                                </div>
                                <div class="job-item-info-date d-flex flex-column align-items-end h-100">

                                    <span title="Opening Date"
                                          class="mt-auto">@if(isset($item->opening_date)){{\App\Job::getOpeningDate($item->opening_date)}}@endif</span>
                                </div>
                            </div>
                        </a>
                    @endif
                @endforeach
            </div>



            @if(isset($first))
                <div class="job-right">
                    <div class="d-flex align-items-start flex-wrap flex-lg-nowrap job-right-title justify-content-end">
                        <h1 class="font-bold mb-0 mr-auto">{{$first->data->title}}</h1>


                        <span class="d-flex align-items-center job-view-count" flow="left"
                              tooltip="{{trans('announcements.views_text')}}  {{$first->views}} {{trans('announcements.times')}}">
                          <img src="/img/eye.svg" alt="" width="20">
                        </span>


                        <div class="d-flex justify-content-center flex-column">

                            @if(($buttons && $first->user_id==Auth::id()) || ($buttons && \Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement')))
                                @if($first->online_submission == 1)
                                    <button style="cursor: no-drop" disabled type="button"
                                            class="btn btn-orange-gradient ml-2">{{trans('announcements.apply_now')}}
                                    </button>
                                @endif
                                <div class="btn-group drop-group-block drop-group">
                                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                        {{trans('announcements.actions')}}
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        @if($first->status == 'preview')
                                            {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['after-preview', $first->id]]) !!}
                                            {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.save_as_draft'), array(
                                                                               'type' => 'submit',
                                                                               'class' => 'dropdown-item',
                                                                               'title' => trans('announcements.post'),
                                                                               )) !!}
                                            <input type="hidden" name="action" value="save_as_draft">
                                            {!! Form::close() !!}
                                            @if(isset($mirror))
                                                @if($mirror->status == 'pending')
                                                    @permission('post_announcement')
                                                    {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['after-preview', $first->id]]) !!}
                                                    {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.post'), array(
                                                                                       'type' => 'submit',
                                                                                       'class' => 'dropdown-item',
                                                                                       'title' => trans('announcements.post'),
                                                                                       )) !!}
                                                    <input type="hidden" name="action" value="post">
                                                    {!! Form::close() !!}
                                                    @endpermission
                                                @endif
                                                @if($mirror->status == 'draft')
                                                    @permission('post_announcement')
                                                    {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['after-preview', $first->id]]) !!}
                                                    {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.post'), array(
                                                                                       'type' => 'submit',
                                                                                       'class' => 'dropdown-item',
                                                                                       'title' => trans('announcements.post'),
                                                                                       )) !!}
                                                    <input type="hidden" name="action" value="post">
                                                    {!! Form::close() !!}
                                                    @endpermission
                                                @endif

                                                @if($mirror->status == 'expired')
                                                    @if(\Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement'))
                                                        @permission('post_announcement')
                                                        {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['after-preview', $first->id]]) !!}
                                                        {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.post'), array(
                                                                                           'type' => 'submit',
                                                                                           'class' => 'dropdown-item',
                                                                                           'title' => trans('announcements.post'),
                                                                                           )) !!}
                                                        <input type="hidden" name="action" value="post">
                                                        {!! Form::close() !!}
                                                        @endpermission
                                                    @endif
                                                @endif
                                            @endif

                                            @permission('edit_announcement')
                                            <a title="{{trans('profile_tables.edit')}}" class="dropdown-item"
                                               href="{{route('announcements-edit',$first->id)}}"><img
                                                        src="/img/pencil-edit.svg"
                                                        alt=""
                                                        height="20">{{trans('profile_tables.edit')}}
                                            </a>
                                            @endpermission
                                            @permission('postEmail_announcement')
                                            {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['after-preview', $first->id]]) !!}
                                            {!! Form::button('<img src="/img/postandemail.svg">'.trans('announcements.post_and_email'), array(
                                                                               'type' => 'submit',
                                                                               'class' => 'dropdown-item',
                                                                               'title' => trans('announcements.save_changes'),
                                                                               )) !!}
                                            <input type="hidden" name="action" value="post_and_email">
                                            {!! Form::close() !!}
                                            @endpermission

                                            @if(\Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement'))
                                                {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['after-preview', $first->id]]) !!}
                                                {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.save_changes'), array(
                                                                                   'type' => 'submit',
                                                                                   'class' => 'dropdown-item',
                                                                                   'title' => trans('announcements.save_changes'),
                                                                                   )) !!}
                                                <input type="hidden" name="action" value="save_changes">
                                                {!! Form::close() !!}
                                            @else
                                                {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['after-preview', $first->id]]) !!}
                                                {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.submit'), array(
                                                                                   'type' => 'submit',
                                                                                   'class' => 'dropdown-item',
                                                                                   'title' => trans('announcements.submit'),
                                                                                   )) !!}
                                                <input type="hidden" name="action" value="submit">
                                                {!! Form::close() !!}
                                            @endif

                                            {{--{!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['after-preview', $first->id]]) !!}--}}
                                            {{--{!! Form::button('<img src="/img/delete-button.svg">'.trans('announcements.cancel'), array(--}}
                                            {{--'type' => 'submit',--}}
                                            {{--'class' => 'dropdown-item',--}}
                                            {{--'title' => trans('announcements.cancel'),--}}
                                            {{--)) !!}--}}
                                            {{--<input type="hidden" name="action" value="cancel">--}}
                                            {{--{!! Form::close() !!}--}}

                                        @elseif($first->status == 'pending' || $first->status=='draft')

                                            @permission('edit_announcement')
                                            <a title="{{trans('profile_tables.edit')}}" class="dropdown-item"
                                               href="{{route('announcements-edit',$first->id)}}"><img
                                                        src="/img/pencil-edit.svg"
                                                        alt=""
                                                        height="20">{{trans('profile_tables.edit')}}
                                            </a>
                                            @endpermission

                                            @if(\Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement'))

                                                @permission('delete_announcement')
                                                {!! Form::open([
                            'method'=>'DELETE',
                           'route' => ['job-destroy',$first->id],
                           'style' => 'display:inline'
                           ]) !!}
                                                <input type="hidden" name="status" value="{{$status}}">
                                                {!! Form::button('<img src="/img/delete-button.svg">'.trans('announcements.delete'), array(
                                                                       'type' => 'submit',
                                                                       'class' => 'dropdown-item',
                                                                       'title' => trans('announcements.delete'),
                                                                       'onclick'=>'return confirm("Confirm delete?")'
                                                               )) !!}

                                                {!! Form::close() !!}
                                                @endpermission

                                                @permission('post_announcement')
                                                {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['status-change', $first->id]]) !!}
                                                {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.post'), array(
                                                                                   'type' => 'submit',
                                                                                   'class' => 'dropdown-item',
                                                                                   'title' => trans('announcements.post'),
                                                                                   )) !!}
                                                <input type="hidden" name="status" value="posted">
                                                {!! Form::close() !!}
                                                @endpermission

                                                @permission('postEmail_announcement')
                                                {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['status-change', $first->id]]) !!}
                                                <input type="hidden" name="status" value="postandemail">
                                                {!! Form::button('<img src="/img/postandemail.svg">'.trans('announcements.post_and_email'), array(
                                         'type' => 'submit',
                                         'class' => 'dropdown-item',
                                         'title' => trans('announcements.post_and_email'),
                                         )) !!}
                                                {!! Form::close() !!}
                                                @endpermission

                                                @permission('reject_announcement')
                                                {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['status-change', $first->id]]) !!}
                                                <input type="hidden" name="status" value="rejected">
                                                {!! Form::button('<img src="/img/reject.svg">'.trans('announcements.reject'), array(
                          'type' => 'submit',
                          'class' => 'dropdown-item',
                          'title' => trans('announcements.reject'),
                          )) !!}
                                                {!! Form::close() !!}
                                                @endpermission

                                            @else
                                                @if($first->status == 'draft')
                                                    @permission('delete_announcement')
                                                    {!! Form::open([
                                'method'=>'DELETE',
                               'route' => ['job-destroy',$first->id],
                               'style' => 'display:inline'
                               ]) !!}
                                                    <input type="hidden" name="status" value="{{$status}}">
                                                    {!! Form::button('<img src="/img/delete-button.svg">'.trans('announcements.delete'), array(
                                                                           'type' => 'submit',
                                                                           'class' => 'dropdown-item',
                                                                           'title' => trans('announcements.delete'),
                                                                           'onclick'=>'return confirm("Confirm delete?")'
                                                                   )) !!}

                                                    {!! Form::close() !!}
                                                    @endpermission
                                                @endif

                                                @permission('submit_announcement')
                                                {!! Form::open([
                                                'method'=>'post',
                                                'route' => ['jobs.submit',$first->id],
                                                'style' => 'display:inline'
                                                ]) !!}
                                                {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.submit'), array(
                                                'type' => 'submit',
                                                'class' => 'dropdown-item',
                                                'title' => trans('announcements.submit'),
                                                )) !!}
                                                {!! Form::close() !!}
                                                @endpermission

                                            @endif
                                        @elseif($first->status == 'posted')
                                            @permission('postEmail_announcement')
                                            {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['status-change', $first->id]]) !!}
                                            <input type="hidden" name="status" value="postandemail">
                                            {!! Form::button('<img src="/img/postandemail.svg">'.trans('announcements.post_and_email'), array(
                                     'type' => 'submit',
                                     'class' => 'dropdown-item',
                                     'title' => trans('announcements.post_and_email'),
                                     )) !!}
                                            {!! Form::close() !!}
                                            @endpermission
                                            @permission('edit_announcement')
                                            <a title="{{trans('profile_tables.edit')}}" class="dropdown-item"
                                               href="{{route('announcements-edit',$first->id)}}"><img
                                                        src="/img/pencil-edit.svg"
                                                        alt=""
                                                        height="20">{{trans('profile_tables.edit')}}
                                            </a>
                                            @endpermission
                                            {{--@permission('delete_announcement')--}}
                                            {{--{!! Form::open([--}}
                                            {{--'method'=>'DELETE',--}}
                                            {{--'route' => ['job-destroy',$first->id],--}}
                                            {{--'style' => 'display:inline'--}}
                                            {{--]) !!}--}}
                                            {{--<input type="hidden" name="status" value="{{$status}}">--}}
                                            {{--{!! Form::button('<img src="/img/delete-button.svg">'.trans('announcements.delete'), array(--}}
                                            {{--'type' => 'submit',--}}
                                            {{--'class' => 'dropdown-item',--}}
                                            {{--'title' => trans('announcements.delete'),--}}
                                            {{--'onclick'=>'return confirm("Confirm delete?")'--}}
                                            {{--)) !!}--}}

                                            {{--{!! Form::close() !!}--}}
                                            {{--@endpermission--}}

                                            @if(Auth::user()->can('accessAll_announcement'))
                                                @permission('expired_announcement')
                                                {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['status-change', $first->id]]) !!}
                                                <input type="hidden" name="status" value="expired">
                                                {!! Form::button('<img src="/img/time.svg">'.trans('announcements.expired'), array(
                         'type' => 'submit',
                         'class' => 'dropdown-item',
                         'title' => trans('announcements.expired'),
                         )) !!}
                                                {!! Form::close() !!}
                                                @endpermission
                                            @endif


                                            @permission('clone_announcement')
                                            {!! Form::open([
                                  'method'=>'post',
                                  'route' => ['jobs.clone',$first->id],
                                  'style' => 'display:inline'
                                  ]) !!}
                                            {!! Form::button('<img src="/img/copy.svg">'.trans('announcements.clone'), array(
                                            'type' => 'submit',
                                            'class' => 'dropdown-item',
                                            'title' => trans('announcements.copy'),
                                            )) !!}

                                            {!! Form::close() !!}
                                            @endpermission

                                        @elseif($first->status=='expired')

                                            @if(\Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement'))
                                                {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['status-change', $first->id]]) !!}
                                                {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.post'), array(
                                                                                   'type' => 'submit',
                                                                                   'class' => 'dropdown-item',
                                                                                   'title' => trans('announcements.post'),
                                                                                   )) !!}
                                                <input type="hidden" name="status" value="posted">
                                                {!! Form::close() !!}
                                            @endif

                                            @permission('edit_announcement')
                                            <a title="{{trans('profile_tables.edit')}}" class="dropdown-item"
                                               href="{{route('announcements-edit',$first->id)}}"><img
                                                        src="/img/pencil-edit.svg"
                                                        alt=""
                                                        height="20">{{trans('profile_tables.edit')}}
                                            </a>
                                            @endpermission
                                            @permission('delete_announcement')
                                            {!! Form::open([
                                            'method'=>'DELETE',
                                            'route' => ['job-destroy',$first->id],
                                            'style' => 'display:inline'
                                            ]) !!}
                                            <input type="hidden" name="status" value="{{$status}}">
                                            {!! Form::button('<img src="/img/delete-button.svg">'.trans('announcements.delete'), array(
                                                                   'type' => 'submit',
                                                                   'class' => 'dropdown-item',
                                                                   'title' => trans('announcements.delete'),
                                                                   'onclick'=>'return confirm("Confirm delete?")'
                                                           )) !!}

                                            {!! Form::close() !!}
                                            @endpermission
                                            @permission('clone_announcement')
                                            {!! Form::open([
                                                              'method'=>'post',
                                                              'route' => ['jobs.clone',$first->id],
                                                              'style' => 'display:inline'
                                                              ]) !!}
                                            {!! Form::button('<img src="/img/copy.svg">'.trans('announcements.clone'), array(
                                            'type' => 'submit',
                                            'class' => 'dropdown-item',
                                            'title' => trans('announcements.copy'),
                                            )) !!}

                                            {!! Form::close() !!}
                                            @endpermission

                                        @elseif($first->status=='rejected')
                                            @permission('edit_announcement')
                                            <a title="{{trans('profile_tables.edit')}}" class="dropdown-item"
                                               href="{{route('announcements-edit',$first->id)}}"><img
                                                        src="/img/pencil-edit.svg"
                                                        alt=""
                                                        height="20">{{trans('profile_tables.edit')}}
                                            </a>
                                            @endpermission
                                            @permission('delete_announcement')
                                            {!! Form::open([
                                            'method'=>'DELETE',
                                            'route' => ['job-destroy',$first->id],
                                            'style' => 'display:inline'
                                            ]) !!}
                                            <input type="hidden" name="status" value="{{$status}}">
                                            {!! Form::button('<img src="/img/delete-button.svg">'.trans('announcements.delete'), array(
                                                                   'type' => 'submit',
                                                                   'class' => 'dropdown-item',
                                                                   'title' => trans('announcements.delete'),
                                                                   'onclick'=>'return confirm("Confirm delete?")'
                                                           )) !!}

                                            {!! Form::close() !!}
                                            @endpermission
                                            @permission('clone_announcement')
                                            {!! Form::open([
                                                     'method'=>'post',
                                                     'route' => ['jobs.clone',$first->id],
                                                     'style' => 'display:inline'
                                                     ]) !!}
                                            {!! Form::button('<img src="/img/copy.svg">'.trans('announcements.clone'), array(
                                            'type' => 'submit',
                                            'class' => 'dropdown-item',
                                            'title' => trans('announcements.copy'),
                                            )) !!}

                                            {!! Form::close() !!}
                                            @endpermission
                                        @endif
                                    </div>
                                </div>
                            @elseif(Auth::check() &&  Auth::user()->is_organization!=1 && $first->online_submission==1)
                                @if($applied_job=="" )
                                    <a data-id="{{$first->id}}"
                                       class="btn btn-orange-gradient apply">{{trans('announcements.apply_now')}}</a>
                                @elseif($applied_job =='rstat_Incomplete')
                                    <a data-id="{{$first->id}}"
                                       class="btn btn-orange-gradient apply">{{trans('announcements.apply_now')}}</a>

                                    <span
                                            class="btn btn-orange-gradient mt-2">{{trans('appliers.'.$applied_job)}}
                                        &nbsp
                                        {{$applied_job_date}}
                                </span>

                                @else
                                    <span
                                            class="btn btn-orange-gradient ">{{trans('appliers.'.$applied_job)}}&nbsp
                                        {{$applied_job_date}}
                                </span>

                                @endif
                            @elseif(!Auth::check() && $first->online_submission==1)
                                <a class="btn btn-orange-gradient"
                                   href="{{route('applyJob',$first->slug)}}">{{trans('announcements.apply_now')}}</a>
                                {{--<form action="{{route('applyJob')}}" method="POST">--}}
                                {{--@csrf--}}
                                {{--<input type="hidden" name="slug" value="{{$first->slug}}">--}}
                                {{--<button data-id="{{$first->id}}" type="submit"--}}
                                {{--class="btn btn-orange-gradient">{{trans('announcements.apply_now')}}</button>--}}
                                {{--</form>--}}
                            @endif
                        </div>
                    </div>
                    <div class="table-responsive d-flex justify-content-between align-items-start pr-4">
                        <table class="table">
                            <tr>
                                <th>{{trans('announcements.company')}}:</th>
                                <td>
                                    @if(isset($first->user->organization) && $first->user->organization->slug)
                                        <a href="{{route('companySingle',$first->user->organization->slug)}}">
                                            {{ \App\Job::companyName($first->user->organization,$first->data->organization)}}
                                            @if(isset($first->user->organization->legal_structure) && $first->user->organization->legal_structure!='other' && $first->user->organization->legal_structure!='brand')
                                                @if(\Lang::has('legal_structure.'.$first->user->organization->legal_structure))
                                                    {{trans('legal_structure.'.$first->user->organization->legal_structure)}}
                                                @else
                                                    {{$first->user->organization->legal_structure}}
                                                @endif

                                            @endif
                                        </a>
                                    @else
                                        {{$first->data->organization}}
                                    @endif
                                </td>
                            </tr>
                            @if(isset($first->data->location_city) && isset($country))
                                <tr>
                                    <th>{{trans('announcements.location')}}:</th>
                                    <td>{{$first->data->location_city}},&nbsp{{trans('countries.'.$country->code)}}</td>
                                </tr>
                            @endif
                            @if(isset($first->data->duration) && $first->data->duration!='')
                                <tr>
                                    <th>{{trans('announcements.duration')}}:</th>
                                    <td>{{$first->data->duration}}</td>
                                </tr>
                            @endif
                            @if(isset($first->data->start_date) && $first->data->start_date!='')
                                <tr>
                                    @if($first->type->key =='publication')
                                        <th>{{trans('announcements.publication_date')}}:</th>
                                    @else
                                        <th>{{trans('announcements.start_date')}}:</th>
                                    @endif

                                    <td>{{$first->data->start_date}}</td>
                                </tr>
                            @endif
                            {{--@if(isset($first->data->opening_date) && $first->data->opening_date!='')--}}
                            {{--<tr>--}}
                            {{--<th>{{trans('announcements.opening_date')}}:</th>--}}
                            {{--<td> {{$first->data->opening_date}}</td>--}}
                            {{--</tr>--}}
                            {{--@endif--}}
                            @if(isset($first->data->deadline) && $first->data->deadline!='')
                                <tr>
                                    <th>{{trans('announcements.deadline')}}:</th>
                                    <td style="white-space: nowrap;">{{$first->data->deadline}}</td>
                                </tr>
                            @endif

                        </table>
                        @if(isset($first->user->image) && $first->user->image)
                            <picture>
                                {{--<source data-srcset="/img/job-1.webp" type="image/webp">--}}
                                <source srcset="{{Storage::disk('')->url($first->user->image)}}" type="image/jpeg">
                                <img src="{{Storage::disk('')->url($first->user->image)}}" class="job-logo">
                            </picture>
                        @endif

                    </div>
                    @if($first->status=='posted')
                        <div class="job-share-this d-flex align-items-center">
                            <p class="font-bold">{{trans('announcements.share_this_page')}}</p>
                            <div class="job-share-links">
                                <a href="http://www.facebook.com/sharer.php?u={{route('jobs',$first->slug)}}"
                                   target="_blank"
                                   rel="noreferrer" aria-hidden="true">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="10.032" height="20.064"
                                         viewBox="0 0 10.032 20.064">
                                        <g transform="translate(0 0)">
                                            <path
                                                    d="M335.843,7259.064v-9.029h2.741l.448-4.013h-3.189v-1.954c0-1.034.026-2.06,1.47-2.06h1.462v-2.869a16.4,16.4,0,0,0-2.527-.14c-2.654,0-4.316,1.662-4.316,4.715v2.308H329v4.013h2.933v9.029Z"
                                                    transform="translate(-329 -7239)" fill="#fff" fill-rule="evenodd"/>
                                        </g>
                                    </svg>
                                </a>
                                <a href="https://twitter.com/share?url={{route('jobs',$first->slug)}}" target="_blank"
                                   rel="noreferrer" aria-hidden="true">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24.598" height="19.679"
                                         viewBox="0 0 24.598 19.679">
                                        <g transform="translate(0 0)">
                                            <path
                                                    d="M11.736,7380.679a14.147,14.147,0,0,0,14.359-14.139c0-.215,0-.43-.015-.643a10.215,10.215,0,0,0,2.518-2.57,10.193,10.193,0,0,1-2.9.781,5.009,5.009,0,0,0,2.219-2.75,10.15,10.15,0,0,1-3.205,1.208,5.108,5.108,0,0,0-7.141-.216,4.925,4.925,0,0,0-1.46,4.747,14.41,14.41,0,0,1-10.4-5.19,4.925,4.925,0,0,0,1.563,6.631,5.052,5.052,0,0,1-2.291-.62v.062a4.989,4.989,0,0,0,4.049,4.87,5.071,5.071,0,0,1-2.278.086,5.044,5.044,0,0,0,4.714,3.452,10.228,10.228,0,0,1-6.267,2.13,10.533,10.533,0,0,1-1.2-.071,14.452,14.452,0,0,0,7.736,2.229"
                                                    transform="translate(-4 -7361)" fill="#fff" fill-rule="evenodd"/>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    @endif
                    <ul class="nav nav-pills mb-3 job-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="job-tab" data-toggle="pill" href="#job" role="tab"
                               aria-controls="job" aria-selected="true">{{trans('types.'.$first->type->key)}}</a>
                        </li>
                        <li class="nav-item">
                            @if((isset($first->data->about_company) && $first->data->about_company!='') || (Auth::check() && Auth::user()->can('accessAll_announcement')))
                                <a class="nav-link" id="company-tab" data-toggle="pill" href="#company" role="tab"
                                   aria-controls="company" aria-selected="false">{{trans('announcements.company')}}</a>
                            @endif
                        </li>
                    </ul>
                    <div class="tab-content job-tab-content">
                        <div class="tab-pane fade show active" id="job" role="tabpanel" aria-labelledby="job-tab">
                            @if(isset($first->data->term) && $first->data->term!='')
                                @if($first->type->key =='job_opp' || $first->type->key =='volunteering' || $first->type->key=='internship')
                                    <h4 class="font-bold mt-0 no-wrap @if(!\App\Job::is_enumeration($first->data->term)) not-dash @endif">{{trans('announcements.term')}}
                                        :&nbsp</h4>
                                @else
                                    <h4 class="font-bold mt-0 no-wrap @if(!\App\Job::is_enumeration($first->data->term)) not-dash @endif">{{trans('announcements.'.$first->type->key.'_type')}}
                                        :&nbsp</h4>
                                @endif
                                {{$first->data->term}}
                                <div class="clearfix mb-2"></div>
                            @endif
                            @if($first->type->key =='job_opp' || $first->type->key =='volunteering' || $first->type->key=='internship')
                                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->description)) not-dash @endif">{{trans('announcements.description')}}
                                    :&nbsp</h4>
                            @elseif($first->type->key =='news')
                                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->description)) not-dash @endif">{{trans('announcements.news_details')}}
                                    :&nbsp</h4>
                            @else
                                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->description)) not-dash @endif">{{trans('announcements.detail_description')}}
                                    :&nbsp</h4>
                            @endif

                            {!!\App\Job::emailize($first->data->description)!!}
                            <div class="clearfix mb-2"></div>
                            @if(isset($first->data->responsibilities) && $first->data->responsibilities!='')
                                @if($first->type->key =='job_opp' ||$first->type->key =='volunteering' || $first->type->key=='internship')
                                    <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->responsibilities)) not-dash @endif">{{trans('announcements.responsibilities')}}
                                        :&nbsp</h4>
                                @else
                                    <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->responsibilities)) not-dash @endif">{{trans('announcements.educational_level')}}
                                        :&nbsp</h4>
                                @endif
                                {!!\App\Job::emailize($first->data->responsibilities) !!}
                                <div class="clearfix mb-2"></div>
                            @endif
                            @if(isset($first->data->requirements) && $first->data->requirements!='')
                                @if($first->type->key =='job_opp' || $first->type->key =='volunteering' || $first->type->key=='internship')
                                    <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->requirements)) not-dash @endif">{{trans('announcements.required_qualifications')}}
                                        :&nbsp</h4>
                                @else
                                    <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->requirements)) not-dash @endif">{{trans('announcements.requirements')}}
                                        :&nbsp</h4>
                                @endif
                                {!! \App\Job::emailize($first->data->requirements) !!}
                                <div class="clearfix mb-2"></div>
                            @endif
                            @if(isset($first->data->procedures) && $first->data->procedures!='')
                                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->procedures)) not-dash @endif">{{trans('announcements.app_procedures')}}
                                    :&nbsp</h4>
                                {!!\App\Job::procedures(\App\Job::emailize($first->data->procedures),$first,$applied_job)!!}
                                <p>{!! trans('announcements.application_procedures_default_text')!!}</p>
                                <div class="clearfix mb-2"></div>
                            @endif
                            @if(isset($first->data->salary) && $first->data->salary!='')
                                <h4 class="font-bold mt-0 no-wrap @if(!\App\Job::is_enumeration($first->data->salary)) not-dash @endif">{{trans('announcements.salary')}}
                                    :&nbsp</h4>
                                {{$first->data->salary}}
                                <div class="clearfix mb-2"></div>
                            @endif
                            @if(isset($first->data->open) && $first->data->open!='')
                                <h4 class="font-bold mt-0 no-wrap @if(!\App\Job::is_enumeration($first->data->open)) not-dash @endif">{{trans('announcements.open')}}
                                    :&nbsp</h4>
                                {{$first->data->open}}
                                <div class="clearfix mb-2"></div>
                            @endif
                            @if(isset($first->data->audience) && $first->data->audience!='')
                                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->audience)) not-dash @endif">{{trans('announcements.audience')}}
                                    :&nbsp</h4>
                                {{$first->data->audience}}
                                <div class="clearfix mb-2"></div>
                            @endif
                            @if(isset($first->data->notes) && $first->data->notes!='' )
                                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->notes)) not-dash @endif">{{trans('announcements.notes')}}
                                    :&nbsp</h4>
                                {!!\App\Job::emailize($first->data->notes)!!}
                                <div class="clearfix mb-2"></div>
                            @endif
                            @if(isset($first->data->about) && $first->data->about!='')
                                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->about)) not-dash @endif">{{trans('announcements.about')}}
                                    :&nbsp</h4>
                                {!!\App\Job::emailize($first->data->about)!!}
                                <div class="clearfix mb-2"></div>
                            @endif
                            @if(isset($first->data->author) && $first->data->author!='' )
                                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->author)) not-dash @endif">{{trans('announcements.author')}}
                                    :&nbsp</h4>
                                {{$first->data->author}}
                                <div class="clearfix mb-2"></div>
                            @endif
                            @if(isset($first->data->pages) && $first->data->pages!='' )
                                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->pages)) not-dash @endif">{{trans('announcements.pages')}}
                                    :&nbsp</h4>
                                {{$first->data->pages}}
                                <div class="clearfix mb-2"></div>
                            @endif
                            @if(isset($first->data->language) && $first->data->language!='')
                                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->language)) not-dash @endif">{{trans('announcements.language')}}
                                    :&nbsp</h4>
                                {{$first->data->language}}
                                <div class="clearfix mb-2"></div>
                            @endif
                            @if(count($first->attachment)!=0)
                                <h4 class="font-bold">{{trans('announcements.attachments')}}:</h4>
                                @foreach($first->attachment as $file)
                                    @if($file->image==1)
                                        <br>
                                        <a href="{{Storage::disk('')->url($file->file)}}" target="_blank"
                                           class="jobs-img jobs-img-description">
                                            <img src="{{Storage::disk('')->url($file->file)}}" alt="">
                                            <span>{{$file->description}}</span>
                                        </a>
                                    @else
                                        <span class="jobs-img-file">
                                        <a href="{{Storage::disk('')->url($file->file)}}" download>
                                            <img src="/img/down-white.svg" alt="" width="17" height="17"
                                                 class="mr-2">{{$file->name}} - {{$file->description}}</a>
                                    </span>
                                    @endif
                                @endforeach
                            @endif
                        </div>

                        <div class="tab-pane fade" id="company" role="tabpanel" aria-labelledby="company-tab">
                            @if(isset($first->data->about_company) && $first->data->about_company!='')
                                <h4 class="font-bold @if(!\App\Job::is_enumeration($first->data->about_company)) not-dash @endif">{{trans('announcements.about_company')}}
                                    :&nbsp</h4>
                                {!!\App\Job::emailize($first->data->about_company)!!}<br><br>
                            @endif
                            @if(Auth::check() && Auth::user()->can('accessAll_announcement'))
                                @if(isset($first->expiration_date))
                                    <h4 class="font-bold not-dash">{{trans('jobs.expiration_date')}}:&nbsp</h4>
                                    {!! \Carbon\Carbon::parse($first->expiration_date)->format('d F Y')!!}<br>
                                @endif
                                @if(isset($first->data->contact_person))
                                    <h4 class="font-bold not-dash">{{trans('jobs.contact_person')}}:&nbsp</h4>
                                    {!! $first->data->contact_person !!}<br>
                                @endif
                                @if(isset($first->data->contact_title))
                                    <h4 class="font-bold not-dash">{{trans('jobs.contact_title')}}:&nbsp</h4>
                                    {!! $first->data->contact_title !!}<br>
                                @endif
                                @if(isset($first->email))
                                    <h4 class="font-bold not-dash">{{trans('company.email')}}:&nbsp</h4>
                                    <a
                                            href="mailto:{{$first->email}}">{{$first->email}}</a><br>
                                @endif
                                @if(isset($phone))
                                    <h4 class="font-bold not-dash">{{trans('company.tel')}}:&nbsp</h4>

                                    <a href="tel:{{$phone}}" class="job-tel">{{$phone}}</a>

                                @endif
                                <div class="clearfix mt-3"></div>
                                @if(isset($first->added_user))
                                    <h4 class="font-bold not-dash">{{trans('announcements.added_by')}}:&nbsp</h4>
                                    {{$first->user->username}}@if($first->added_user->username!= $first->user->username )
                                        ({{$first->added_user->username}})@endif,
                                @endif
                                <b>{{trans('announcements.on')}}
                                    :</b> {{\Carbon\Carbon::parse($first->created_at)->format('Y-m-d H:i:s')}},
                                @if(isset($first->added_IP))
                                    <b>{{trans('announcements.from')}}:</b> {{$first->added_IP}}
                                @endif
                                <div class="clearfix"></div>
                                @if(isset($first->edited_user))
                                    <h4 class="font-bold not-dash">{{trans('announcements.last_edited_by')}}:&nbsp</h4>
                                    {{$first->edited_user->username}},
                                @endif

                                <b>{{trans('announcements.on')}}
                                    :</b> {{\Carbon\Carbon::parse($first->updated_at)->format('Y-m-d H:i:s')}},
                                @if(isset($first->edited_IP))
                                    <b> {{trans('announcements.from')}}:</b>
                                    {{$first->edited_IP}}
                                @endif

                            @endif
                            <div class="clearfix"></div>

                        </div>
                    </div>
                </div>
            @else
                <div class="job-right">
                    <h2 class="text-center p-5" style="word-break: keep-all;"> {{trans('announcements.welcome_text')}}</h2>

                </div>
            @endif

        @else
            <h4 class="text-center m-auto">{{trans('announcements.no_result')}}</h4>
        @endif
    </div>


    <!-- Modal -->
    <div class="modal fade" id="applicant-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('announcements.apply_now')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="applyForm" action="{{ route('apply') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input name="job_id" type="hidden" id="job_id">
                        <div class="form-group">
                            @if(count($resumes)!=0)
                                <select name="choose_resume" id="choose_resume">
                                    <option></option>
                                    @for($i=0;$i<count($resumes); $i++)
                                        <option @if(old('choose_resume')==$resumes[$i]->id) selected
                                                @endif value="{{$resumes[$i]->id}}">{{trans('announcements.resume')}} {{$i+1}}</option>
                                    @endfor
                                </select>
                                <label for="resume"
                                       class="control-label">{{trans('announcements.your_resumes')}} </label>
                                @error('resume')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            @endif

                        </div>

                        <div class="@error('upload_resume ')is-invalid @enderror form-group-file file-input-style">
                            <label for="upload_resume" class="sr-only">{{trans('announcements.upload_resume')}}</label>
                            <input type="file" id="upload_resume" name="upload_resume" accept=".pdf,.doc,.docx">
                            @error('upload_resume')
                            <span id="error_upload" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <span style="color: red" role="alert" id="resume_error" class="d-block mb-4"></span>
                        <div class="form-group no-focus">
                            <label for="cover_letter">{{trans('announcements.cover_letter')}}</label>
                            <textarea id="cover_letter" class="@error('cover_letter') is-invalid @enderror"
                                      name="cover_letter">{{ \Illuminate\Support\Facades\Input::old('cover_letter') }}</textarea>

                            @error('cover_letter')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="text-right">
                            <button type="button" id="apply_submit"
                                    class="btn btn-blue-gradient">{{trans('jobs.apply')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="submit_success" class="success-modal modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <!-- Modal content-->
            <div class="modal-content">
                <a class="close" href="#" data-dismiss="modal">&times;</a>
                <div class="page-body">
                    <div class="head">
                        <h5 class="mb-5">{{trans('appliers.applier_success')}}</h5>
                    </div>

                    <h1 class="text-center">
                        <span class="checkmark-circle">
                            <span class="background"></span>
                            <span class="checkmark draw"></span>
                        </span>
                    </h1>
                </div>
            </div>
        </div>

    </div>

    <!-- Modal Submit Job-->
    <div id="submit_job" class="success-modal modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <!-- Modal content-->
            <div class="modal-content">
                <a class="close" href="#" data-dismiss="modal">&times;</a>
                <div class="page-body">
                    <div class="head">
                        <h5 class="mb-5">{{trans('announcements.submit_success')}}</h5>
                    </div>

                    <h1 class="text-center">
                        <span class="checkmark-circle">
                            <span class="background"></span>
                            <span class="checkmark draw"></span>
                        </span>
                    </h1>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
    <script src="/js/Select2-4.0.7.js"></script>
    <script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
    {{--<script type="text/javascript" src="/js/moment.min.js" defer></script>--}}
    <script type="text/javascript" src="/js/moment-locale.js" defer></script>
    <script>
        $(".open-search").click(function () {
            $('.job-search-header').addClass('job-search-header-opened');
            $('.open-search').hide();
            $('.close-search').show();
        });
        $(".close-search").click(function () {
            $('.job-search-header').removeClass('job-search-header-opened');
            $('.open-search').show();
            $('.close-search').show();
        });

        CKEDITOR.replace('cover_letter');


        @if(Session::has('apply_job'))
        $('#submit_success').modal('show')
        {{Session::forget('apply_job')}}
        @endif

        @if(Session::has('submit'))
        $('#submit_job').modal('show')
        {{Session::forget('submit')}}
        @endif

        @if($errors->has('upload_resume'))
        $('#error_upload').text('{{ $errors->first('upload_resume')}}');
        var job_id = '{{old('job_id')}}';
        $('#job_id').val(job_id);
        $('#applicant-modal').modal('show');

        @endif

        function CKupdate() {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
                CKEDITOR.instances[instance].setData('');
            }
        }

        $(document).on('click', '.apply', function () {
            $("span[role='alert']").text('');
            var choose = $('#choose_resume').val();
            {
                if (choose != null) {
                    $('#choose_resume').val('');
                }
            }
            $('.file-selected').text('');
            $('#upload_resume').val('');
            $('#resume_error').text('');
            $('#cover_letter').val('');
            CKupdate();


            var job_id = $(this).attr('data-id');
            $('#job_id').val(job_id);
            $('#applicant-modal').modal('show');
        });

        $('#apply_submit').on('click', function () {
            $('#resume_error').html('');
            var choose = $('#choose_resume').val();
            var upload = $('#upload_resume').val();
            if (choose == null) {
                choose = '';
            }
            if (upload == null) {
                upload = ''
            }

            var error = '';

            if (choose == '' && upload == '') {
                error = 'Please select or upload resume';
            }
            else if (choose != '' && upload !== '') {
                error = 'Please either select resume or upload';
            }
            if (error != '') {
                $('#choose_resume').addClass('is-invalid');
                $('#upload_resume').addClass('is-invalid');
                $('#resume_error').html(error);
            }
            else {
                $('#applyForm').submit();
            }
            console.log(error);

        })
        var status = '{{$status}}';
        $('#job-category').on('change', function () {
//            var input = $("<input>")
//                .attr("type", "hidden")
//                .attr("name", "status").val(status).appendTo('#filter_form');
            $('#filter_form').submit();
        });
        $('#sub_category').on('change', function () {
//            var input = $("<input>")
//                .attr("type", "hidden")
//                .attr("name", "status").val(status).appendTo('#filter_form');
            $('#filter_form').submit();
        });
        $('#job-types').on('change', function () {
//            var input = $("<input>")
//                .attr("type", "hidden")
//                .attr("name", "status").val(status).appendTo('#filter_form');
            $('#filter_form').submit();
        });


        $('#expired-jobs').on('change', function () {
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "status").val(status).appendTo('#filter_form');
            $('#filter_form').submit();
        });
        $('#rejected-jobs').on('change', function () {
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "status").val(status).appendTo('#filter_form');
            $('#filter_form').submit();
        })
        id = '';
                @if(isset($first))
        var id = '{{$first->id}}';
        @endif
        $(function () {
            $('.job-left').animate({
                scrollTop: $("#job_" + id).offset().top - 274
            }, 1000);
        });


        $("#job-category").select2({
            placeholder: "{{trans('announcements.job_category')}}",
            allowClear: true
        });
        $("#job-types").select2({
            placeholder: "{{trans('announcements.job_types')}}",
            minimumResultsForSearch: Infinity,
            allowClear: true
        });
        $("#sub_category").select2({
            placeholder: "{{trans('announcements.sub_category')}}",
            allowClear: true
        });
        $("#expired-jobs").select2({
            placeholder: "{{trans('announcements.filter_by_date')}}",
            placeholder: "{{trans('announcements.filter_by_date')}}",
            allowClear: true
        });
        $("#rejected-jobs").select2({
            placeholder: "{{trans('announcements.filter_by_date_rejected')}}",
            placeholder: "{{trans('announcements.filter_by_date_rejected')}}",
            allowClear: true
        });


        $(document).ready(function () {

            {{--var view = "{{$view}}";--}}
            {{--if(view == 'list')--}}
            {{--{--}}
                {{--$("body").addClass("menu-list");--}}
            {{--}--}}






        @permission('accessAll_announcement')
            $('body').addClass('can-copy')
            @endpermission
                url = '{{$url}}';


            $(document).on('click', '.job-item', function (e) {
                var stateObj = {foo: "bar"};
                history.pushState(stateObj, "page", $(this).attr('href'));
                e.preventDefault();
                var _this = $(this);
                var title = $(this).attr('data-title');

                var slug = $(this).attr('data-slug');
                var redirect = '/jobs/' + slug + url;

                $.ajax({

                    url: "/get/job",
                    method: "GET",
                    data: {
                        'slug': slug,
                        'status': status
                    },
                    success: function (data) {
                        $('.job-left').removeClass('job-left-opened');
                        $('body').removeClass('body-overflow');

                        $('.close_job-left').css('display', 'none');
                        $('.open_job-left').css('display', 'block');


                        $('.job-item').each(function () {
                            $(this).removeClass('job-item-active');
                        })
                        _this.addClass('job-item-active');
                        $('.job-right').empty();
                        $('.job-right').append(data);

                        document.title = title;
                        window.history.pushState({"html": 'chgitem', "pageTitle": 'chgitem'}, "titlee", redirect);

                    },
                    error: function (data) {
                        console.log(data);

                    }
                });
            });

            $(".open_job-left").click(function () {
                $('.job-search-header').addClass('job-search-header-opened');
                $('.close_job-left').show();
                // $('body').addClass('body-overflow');
                // $(this).hide();
            });
            $(".close_job-left").click(function () {
                $('.job-search-header').removeClass('job-search-header-opened');
                // $('body').removeClass('body-overflow');
                // $('.open_job-left').show();
                $(this).hide();
            });
            let offset = parseInt('{{$count}}') - 15;
            let request_pending = false;
                    @if($count>=15)
            var locale = '{{LaravelLocalization::getCurrentLocale()}}';
            if (locale == 'hy') {
                locale = 'hy-am';
            }

            $(window).scroll(function () {


                if ($(".job-left").hasClass("job-left-list")) {
                    if (request_pending) {
                        return;
                    }
                    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                        offset = offset + 15;


                        $.ajax({
                            url: window.location.href,
                            method: "GET",
                            data: {
                                offset: offset
                            },
                            beforeSend: function (xhr) {
                                request_pending = true;
                                if ($('.scroll').length === 0) {
                                    $('#job-left-part').append('<div class="text-center scroll">\n' +
                                        '            <div class="spinner-border spinner-border-sm" role="status">\n' +
                                        '              <span class="sr-only">Loading...</span>\n' +
                                        '            </div>\n' +
                                        '          </div>');
                                }

                            },
                            success: function (data) {
                                $('.scroll').remove();
                                let html = '';
                                if (data.jobs.length > 0) {
                                    for (let i = 0; i < data.jobs.length; ++i) {
                                        let item = data.jobs[i];
                                        html = html + '<a href="jobs/' + item.slug + '" id="job_' + item.jobId + '"' +
                                            'class = "job-single job-item d-flex" data-slug="' + item.slug + '" data-title = "' + item.menu + '"><span class="triangle-right"></span>';
                                        let logo_html = '';
                                        if (item.logo) {
                                            let logo_url = '{{Storage::disk()->url(":url")}}';
                                            logo_url = logo_url.replace(':url', item.logo);
                                            logo_html = '<picture>' +
                                                '<source srcset="' + logo_url + '" type="image/jpeg">' +
                                                '<img src = "' + logo_url + '" >' +
                                                '</picture>'
                                        } else {
                                            logo_html = '<picture>' +
                                                '<source = data-srcset="/Logo_Placeholder.png" type="image/jpeg">' +
                                                '<img src = "/Logo_Placeholder.png"  width = "113" height="90">' +
                                                '</picture>'
                                        }
                                        html = html + logo_html;
                                        let date;
                                        if (item.opening_date && new Date(item.opening_date.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"))) {
                                            item.opening_date = new Date(item.opening_date.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                                        }
                                        if (isNaN(Date.parse(item.opening_date))) {
                                            date = "";
                                        } else {
                                            date = moment(Date.parse(item.opening_date)).locale(locale).format('DD MMM YYYY');
                                        }

                                        html = html + '<div class="job-item-info d-flex ml-auto align-items-center">' +
                                            '<div class="job-item-info-text flex-grow-1">' +
                                            '<h5 style="user-select: all;">' + item.title + '/ ' + item.organization + '</h5>' +
                                            '</div>' +
                                            '<div class="job-item-info-date d-flex flex-column align-items-end h-100">' +
                                            '<span class="mt-auto">' + date + '</span>' +
                                            '</div>' +
                                            '</div>' +
                                            '</a>'
                                    }
                                    ;

                                    $('#job-left-part').append(html);
                                    request_pending = false;
                                }

                            },
                        });

                    }
                }
            });


            $('#job-left-part').scroll(function (e) {

                if (request_pending) {
                    return;
                }
                if ($('#job-left-part').prop('scrollHeight') - $('#job-left-part').outerHeight() - $(this).scrollTop() < 300) {
                    offset = offset + 15;


                    $.ajax({
                        url: window.location.href,
                        method: "GET",
                        data: {
                            offset: offset
                        },
                        beforeSend: function (xhr) {
                            request_pending = true;
                            if ($('.scroll').length === 0) {
                                $('#job-left-part').append('<div class="text-center scroll">\n' +
                                    '            <div class="spinner-border spinner-border-sm" role="status">\n' +
                                    '              <span class="sr-only">Loading...</span>\n' +
                                    '            </div>\n' +
                                    '          </div>');
                            }

                        },
                        success: function (data) {
                            $('.scroll').remove();
                            let html = '';
                            if (data.jobs.length > 0) {
                                for (let i = 0; i < data.jobs.length; ++i) {
                                    let item = data.jobs[i];
                                    html = html + '<a href="jobs/' + item.slug + '" id="job_' + item.jobId + '"' +
                                        'class = "job-single job-item d-flex" data-slug="' + item.slug + '" data-title = "' + item.title + '"><span class="triangle-right"></span>';
                                    let logo_html = '';
                                    if (item.logo) {
                                        let logo_url = '{{Storage::disk()->url(":url")}}';
                                        logo_url = logo_url.replace(':url', item.logo);
                                        logo_html = '<picture>' +
                                            '<source srcset="' + logo_url + '" type="image/jpeg">' +
                                            '<img src = "' + logo_url + '" >' +
                                            '</picture>'
                                    } else {
                                        logo_html = '<picture>' +
                                            '<source = data-srcset="/Logo_Placeholder.png" type="image/jpeg">' +
                                            '<img src = "/Logo_Placeholder.png"  width = "113" height="90">' +
                                            '</picture>'
                                    }
                                    html = html + logo_html;
                                    let date;
                                    if (item.opening_date && new Date(item.opening_date.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"))) {
                                        item.opening_date = new Date(item.opening_date.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                                    }
                                    if (isNaN(Date.parse(item.opening_date))) {
                                        date = "";
                                    } else {
                                        date = moment(Date.parse(item.opening_date)).locale(locale).format('DD MMM YYYY');
                                    }

                                    html = html + '<div class="job-item-info d-flex ml-auto align-items-center">' +
                                        '<div class="job-item-info-text flex-grow-1">' +
                                        '<h5>' + item.title + '</h5>' +
                                        '<h6 title="' + item.organization + '">' + item.organization + '</h6>' +
                                        '</div>' +
                                        '<div class="job-item-info-date d-flex flex-column align-items-end h-100">' +
                                        '<span class="mt-auto">' + date + '</span>' +
                                        '</div>' +
                                        '</div>' +
                                        '</a>'
                                }
                                ;

                                $('#job-left-part').append(html);
                                request_pending = false;
                            }

                        },
                    });

                }
            });


            @endif

        });


    </script>
@endsection





