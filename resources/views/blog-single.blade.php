@extends('layouts.app')
@section('css')

    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
@endsection
@section('content')
    <section class="blog-single-page">
        <h1 class="text-center">{{$blog->data->title}}</h1>
        {{--<div class="post-author d-flex">--}}
        {{--<img src="{{isset($blog->user->image) ? Storage::disk('')->url($blog->user->image) : '/img/blog-author.png' }}" alt="" class="post-author-img">--}}
        {{--<div class="post-info">--}}
        {{--{{trans('blogs.written_by')}} {{$blog->user->first_name }}  {{$blog->user->last_name}}--}}
        {{--</div>--}}

        {{--</div>--}}

        <div class="w-100">
            <img src="{{Storage::disk('')->url($blog->image)}}" alt="blog image" class="img-fluid w-100">

        </div>
    </section>
    <div class="blog-single-text">

        <div class="blog-content d-flex">
            <div class="share text-center">
                <a href="https://twitter.com/share?url={{route('blog-single',$blog->slug)}}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24.598" height="19.679" viewBox="0 0 24.598 19.679">
                        <g transform="translate(0 0)">
                            <path d="M11.736,7380.679a14.147,14.147,0,0,0,14.359-14.139c0-.215,0-.43-.015-.643a10.215,10.215,0,0,0,2.518-2.57,10.193,10.193,0,0,1-2.9.781,5.009,5.009,0,0,0,2.219-2.75,10.15,10.15,0,0,1-3.205,1.208,5.108,5.108,0,0,0-7.141-.216,4.925,4.925,0,0,0-1.46,4.747,14.41,14.41,0,0,1-10.4-5.19,4.925,4.925,0,0,0,1.563,6.631,5.052,5.052,0,0,1-2.291-.62v.062a4.989,4.989,0,0,0,4.049,4.87,5.071,5.071,0,0,1-2.278.086,5.044,5.044,0,0,0,4.714,3.452,10.228,10.228,0,0,1-6.267,2.13,10.533,10.533,0,0,1-1.2-.071,14.452,14.452,0,0,0,7.736,2.229"
                                  transform="translate(-4 -7361)" fill="#006db7" fill-rule="evenodd"/>
                        </g>
                    </svg>
                </a>
                <a href="http://www.facebook.com/sharer.php?u={{route('blog-single',$blog->slug)}}">

                    <svg xmlns="http://www.w3.org/2000/svg" width="10.032" height="20.064" viewBox="0 0 10.032 20.064">
                        <g transform="translate(0 0)">
                            <path d="M335.843,7259.064v-9.029h2.741l.448-4.013h-3.189v-1.954c0-1.034.026-2.06,1.47-2.06h1.462v-2.869a16.4,16.4,0,0,0-2.527-.14c-2.654,0-4.316,1.662-4.316,4.715v2.308H329v4.013h2.933v9.029Z"
                                  transform="translate(-329 -7239)" fill="#006db7" fill-rule="evenodd"/>
                        </g>
                    </svg>
                </a>

                {{--<a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">--}}

                {{--<svg xmlns="http://www.w3.org/2000/svg" width="16.052" height="20.091" viewBox="0 0 16.052 20.091">--}}
                {{--<path d="M10.984,12.649a.844.844,0,0,1,.024.1c.284,1.615,1.028,2.29,2.453,2.29,2.271,0,4.585-2.819,4.585-5.759,0-3.063-2.467-5.277-6.465-5.277A5.313,5.313,0,0,0,6.006,9.284a3.574,3.574,0,0,0,.754,2.646A1,1,0,1,1,5.4,13.4,5.375,5.375,0,0,1,4,9.284,7.3,7.3,0,0,1,11.58,2c5.06,0,8.471,3.061,8.471,7.284,0,3.969-3.114,7.765-6.591,7.765a4.2,4.2,0,0,1-3.2-1.259l-1.269,5.5a1,1,0,1,1-1.954-.451l3.01-13.042A1,1,0,0,1,12,8.245Z"--}}
                {{--transform="translate(-4 -2)" fill="#006db7"/>--}}
                {{--</svg>--}}
                {{--</a>--}}
            </div>
            <div>
                <div class="blog-p">
                    {!! $blog->data->content !!}
                </div>
                <div class="blog-date">
                    @permission('all_article')
                    {{trans('blogs.originally_published')}}
                    {{trans('months.'.\Carbon\Carbon::parse($blog->created_at)->format('M')).' '.\Carbon\Carbon::parse($blog->created_at)->format('d').','.'  '.  \Carbon\Carbon::parse($blog->created_at)->format('Y H:i')}}
                    <br>
                    @endpermission
                    {{trans('blogs.last_updated')}}
                    {{trans('months.'.\Carbon\Carbon::parse($blog->data->updated_at)->format('M')).' '.\Carbon\Carbon::parse($blog->data->updated_at)->format('d').','.'  '.  \Carbon\Carbon::parse($blog->data->updated_at)->format('Y H:i')}}
                    @permission('all_article')
                    @if(isset($blog->editor))
                        {{trans('blogs.last_updated_by')}}
                        {{$blog->editor->username}}
                    @endif
                    @endpermission
                    @if(\Illuminate\Support\Facades\Auth::check())
                        @if(($blog->user_id == \Illuminate\Support\Facades\Auth::id() &&  \Illuminate\Support\Facades\Auth::user()->can('edit_article')) || \Illuminate\Support\Facades\Auth::user()->can('all_article'))
                            <a class="job-btn btn btn-orange-gradient"
                               href="{{route('blogs.edit',$blog->id)}}">{{trans('blogs.edit')}}</a>
                        @endif
                    @endif
                </div>
            </div>


        </div>


    </div>
    <div class="text-center dont-forget">
        <div class="title">
            {{trans('blogs.dont_forget_to_share')}}
        </div>
        <div class="share">
            <a href="http://www.facebook.com/sharer.php?u={{route('blog-single',$blog->slug)}}">

                <svg xmlns="http://www.w3.org/2000/svg" width="10.032" height="20.064" viewBox="0 0 10.032 20.064">
                    <g transform="translate(0 0)">
                        <path d="M335.843,7259.064v-9.029h2.741l.448-4.013h-3.189v-1.954c0-1.034.026-2.06,1.47-2.06h1.462v-2.869a16.4,16.4,0,0,0-2.527-.14c-2.654,0-4.316,1.662-4.316,4.715v2.308H329v4.013h2.933v9.029Z"
                              transform="translate(-329 -7239)" fill="#006db7" fill-rule="evenodd"/>
                    </g>
                </svg>
            </a>
            {{--<a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">--}}

            {{--<svg xmlns="http://www.w3.org/2000/svg" width="16.052" height="20.091" viewBox="0 0 16.052 20.091">--}}
            {{--<path d="M10.984,12.649a.844.844,0,0,1,.024.1c.284,1.615,1.028,2.29,2.453,2.29,2.271,0,4.585-2.819,4.585-5.759,0-3.063-2.467-5.277-6.465-5.277A5.313,5.313,0,0,0,6.006,9.284a3.574,3.574,0,0,0,.754,2.646A1,1,0,1,1,5.4,13.4,5.375,5.375,0,0,1,4,9.284,7.3,7.3,0,0,1,11.58,2c5.06,0,8.471,3.061,8.471,7.284,0,3.969-3.114,7.765-6.591,7.765a4.2,4.2,0,0,1-3.2-1.259l-1.269,5.5a1,1,0,1,1-1.954-.451l3.01-13.042A1,1,0,0,1,12,8.245Z"--}}
            {{--transform="translate(-4 -2)" fill="#006db7"/>--}}
            {{--</svg>--}}
            {{--</a>--}}
            <a href="https://twitter.com/share?url={{route('blog-single',$blog->slug)}}">
                <svg xmlns="http://www.w3.org/2000/svg" width="24.598" height="19.679" viewBox="0 0 24.598 19.679">
                    <g transform="translate(0 0)">
                        <path d="M11.736,7380.679a14.147,14.147,0,0,0,14.359-14.139c0-.215,0-.43-.015-.643a10.215,10.215,0,0,0,2.518-2.57,10.193,10.193,0,0,1-2.9.781,5.009,5.009,0,0,0,2.219-2.75,10.15,10.15,0,0,1-3.205,1.208,5.108,5.108,0,0,0-7.141-.216,4.925,4.925,0,0,0-1.46,4.747,14.41,14.41,0,0,1-10.4-5.19,4.925,4.925,0,0,0,1.563,6.631,5.052,5.052,0,0,1-2.291-.62v.062a4.989,4.989,0,0,0,4.049,4.87,5.071,5.071,0,0,1-2.278.086,5.044,5.044,0,0,0,4.714,3.452,10.228,10.228,0,0,1-6.267,2.13,10.533,10.533,0,0,1-1.2-.071,14.452,14.452,0,0,0,7.736,2.229"
                              transform="translate(-4 -7361)" fill="#006db7" fill-rule="evenodd"/>
                    </g>
                </svg>
            </a>
        </div>
    </div>


    <section class="blog-boxes related-articles w-100">
        <h3 class="text-center">{{trans('blogs.related_articles')}}</h3>
        <div class="d-flex flex-wrap blog-boxes-content">
            @foreach($related as $item)
                <div class="blog-box">
                    <a href="{{route('blog-single',$item->slug)}}">
                        <img src="{{Storage::disk('')->url($item->image)}}" alt="blogs" class="img-fluid blog-box-img">
                        <div class="blog-box-text">
                            <div class="blog-box-padding">
                                <h6 class="title">{{$item->data->title}}</h6>
                                <span class="date">{{$item->created_at->format('M d, Y')}}</span>
                                <div class="blog-text">
                                    {{$item->data->short_description}}
                                </div>
                            </div>
                            <a href="{{route('blog-single',$item->slug)}}"
                               class="blog-link"> {{trans('blogs.read_more')}}</a>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </section>
@endsection
