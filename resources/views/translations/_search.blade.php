@foreach($keys as $key)
    <tr>
        <td>{{$key->key}}</td>
        <td>{{$key->value}}</td>
        <td>{{$key->locale}}</td>
        <td>{{$key->group}}</td>
        <td>
            <a href="{{route('translation_group', $key->group)}}" class="btn btn-success">Go to group</a>
        </td>
    </tr>
@endforeach