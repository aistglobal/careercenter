@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
@endsection

@section('content')

    <section class="position-relative">

        <div class="left-menu float-left">
            <a class="open_close-menu" href="#">
                <i></i>
                <i></i>
                <i></i>
            </a>

            <div class="profile-user-info d-flex align-items-center">
                <img src="/img/my-profile.svg" alt="">
                <h6>AIST Global LLC</h6>
            </div>
            <ul class="nav flex-column menu-nav">
                <li class="nav-item">
                    <a class="nav-link active d-flex align-items-center" href="#">
                        <img src="/img/my-profile.svg" alt="MY PROFILE">
                        MY PROFILE
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex align-items-center" href="#">
                        <img src="/img/resumes.svg" alt="RESUMES">
                        RESUMES
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex align-items-center" href="#">
                        <img src="/img/recruitment.svg" alt="RECRUITMENT">
                        RECRUITMENT
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex align-items-center" data-toggle="collapse" href="#post-an-announcement"
                       role="button" aria-expanded="false" aria-controls="POST AN ANNOUNCEMENT">
                        <img src="/img/post.svg" alt="POST AN ANNOUNCEMENT">POST AN ANNOUNCEMENT
                    </a>

                    <div class="collapse" id="post-an-announcement">
                        <ul class="nav flex-column menu-nav collapse-ul">
                            <li class="nav-item"><a class="nav-link" href="#"><span>-</span>JOB OPPORTUNITIES</a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><span>-</span>VOLUNTEERING OPPORTUNITIES</a></li>
                            <li class="nav-item"><a class="nav-link active" href="#"><span>-</span>INTERNSHIPS</a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><span>-</span>EDUCATION OPPORTUNITIES</a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><span>-</span>TRAININGS</a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><span>-</span>FELLOWSHIPS</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>

        <div class="profile-container float-right">
            <ul class="nav nav-pills mb-3 profile-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="job-tab" data-toggle="pill" href="#job" role="tab"
                       aria-controls="job" aria-selected="true">JOB</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="company-tab" data-toggle="pill" href="#company" role="tab"
                       aria-controls="company" aria-selected="false">COMPANY</a>
                </li>
            </ul>
            <div class="tab-content profile-tab-content">
                {{--tab 1--}}
                <div class="tab-pane fade show active" id="job" role="tabpanel" aria-labelledby="job-tab">
                    <form>
                        <div class="row">

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" id="username" required>
                                    <label for="username" class="control-label">Organization<span class="star">*</span></label>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group pr-4">
                                        <input type="text" id="calendar1" data-required=no>
                                        <label for="username" class="control-label">Opening Date</label>
                                    </div>
                                    <div class="form-group flex-grow-1">
                                        <input type="text" id="Application" required>
                                        <label for="Application" class="control-label">Application Deadline<span class="star">*</span></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea required="" id="textarea"></textarea>
                                    <label for="textarea" class="control-label">Textarea<span class="star">*</span></label>
                                </div>
                                <div class="form-group height-textarea">
                                    <textarea required="" id="tt"></textarea>
                                    <label for="tt" class="control-label">Textarea click height</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" id="username" required>
                                    <label for="username" class="control-label">Announcement code</label>
                                </div>
                                <div class="form-group-file mb-input">
                                    <label for="file" class="sr-only">Choose file</label>
                                    <input type="file" id="file">
                                    <span class="input-help">Permitted files: txt, pdf, jpg, gif, png, doc,xls, zip, docx,  xlsx (Max: 1MB).</span>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"/><i class="helper"></i>This Announcement will be translated
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"/><i class="helper"></i>Enable On-line applications Submission
                                    </label>
                                </div>

                            </div>

                            <div class="col-lg-12">
                                <div class="d-flex justify-content-end mt-5">
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-orange-gradient">SAVE AS DRAFT</button>
                                    </div>
                                    <div class="text-center ml-3">
                                        <button type="submit" class="btn btn-green">PREVIEW</button>
                                    </div>
                                    <div class="text-center ml-3">
                                        <button type="submit" class="btn btn-blue-gradient">CANCEL</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                {{--end tab 1--}}


                {{--tab 2--}}
                <div class="tab-pane fade" id="company" role="tabpanel" aria-labelledby="company-tab">
                    tab2
                </div>
                {{--end tab 2--}}
            </div>
        </div>

    </section>


@endsection

@section('scripts')
    <script type="text/javascript" src="/js/moment.min.js" defer></script>
    <script type="text/javascript" src="/js/daterangepicker.min.js" defer></script>
    <script>

         $(function() {
             $('#calendar1').daterangepicker({
                 singleDatePicker: true
             });
         });
        //
        //
        // $(document).ready(function(){
        //     $(".open_close-menu").click(function(){
        //         $('.left-menu').toggleClass('profile-menu-opened');
        //         $(this).toggleClass("active");
        //     });
        //
        // });
        //
        //
        // function checkOffset() {
        //     if ($('.left-menu').offset().top + $('.left-menu').height()
        //         >= $('footer').offset().top - 60)
        //         $('.left-menu').addClass('profile-menu-scroll');
        //     if ($(document).scrollTop() + window.innerHeight < $('footer').offset().top)
        //         $('.left-menu').removeClass('profile-menu-scroll');
        // }
        // $(document).scroll(function () {
        //     checkOffset();
        // });

    </script>
@endsection





