@extends('layouts.app')

@section('css')
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container-fluid form-container">
        <h1 class="title-h1 font-bold text-center">{{ __('Reset Password') }}</h1>
        <div class="row justify-content-center">
            <div class="col-auto w-100 max-445 position-relative z-index-1">
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <div class="form-group">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        <label for="email" class="control-label">{{ __('E-Mail Address') }} <span class="star">*</span></label>
                        @error('email')
                        <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>


                    <div class="text-center">
                        <button type="submit"
                                class="btn btn-blue-gradient">  {{ __('Send Password Reset Link') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <img src="/img/register-wave.svg" alt="" class="w-100 register-wave">
    <img src="/img/register-bg.svg" alt="" class="w-100 register-bg d-none d-lg-block">

    <!-- Modal -->
    <div id="success_tic" class="success-modal modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <!-- Modal content-->
            <div class="modal-content">
                <a class="close" href="#" data-dismiss="modal">&times;</a>
                <div class="page-body">
                    <div class="head">
                        <h5 class="mb-5">{{trans('login.we_send_yo_email')}}</h5>
                    </div>

                    <h1 class="text-center">
                        <span class="checkmark-circle">
                            <span class="background"></span>
                            <span class="checkmark draw"></span>
                        </span>
                    </h1>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('scripts')
    <script>
        @if (session('status'))
       $('#success_tic').modal('show')
        {{Session::forget('status')}}
        @endif

    </script>
@endsection