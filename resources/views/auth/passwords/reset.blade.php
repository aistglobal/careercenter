@extends('layouts.app')

@section('css')
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container-fluid form-container">
        <h1 class="title-h1 font-bold text-center">{{ __('Reset Password') }}</h1>
        <div class="row justify-content-center">
            <div class="col-auto w-100 max-445 position-relative z-index-1">
                <form method="POST" action="{{ route('password.update') }}">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-group">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                        <label for="email" class="control-label">{{ __('E-Mail Address') }} <span class="star">*</span></label>
                        @error('email')
                        <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                        <label for="password" class="control-label">{{ __('Password') }}</label>
                        @error('password')
                        <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        <label for="password-confirm" class="control-label">{{ __('Confirm Password') }}</label>

                    </div>
                    <div class="text-center">
                        <button type="submit"
                                class="btn btn-blue-gradient"> {{ __('Reset Password') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <img src="/img/register-wave.svg" alt="" class="w-100 register-wave">
    <img src="/img/register-bg.svg" alt="" class="w-100 register-bg d-none d-lg-block">


@endsection
