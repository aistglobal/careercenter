@extends('layouts.app')

@section('css')
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container-fluid form-container">
        <h1 class="title-h1 font-bold text-center">{{trans('login.login')}}</h1>

        <div class="row justify-content-center">
            <div class="col-auto w-100 max-445 position-relative z-index-1">
                <form method="POST" action="{{ route('login', app()->getLocale()) }}">
                    @csrf

                    <div class="form-group">
                        <input type="text" id="login" name="login" class="{{ $errors->has('username') || $errors->has('email') ? ' is-invalid' : '' }}"
                               value="{{ old('username') ?: old('email') }}" required>
                        <label for="login" class="control-label">{{trans('login.username_or_email')}} <span class="star">*</span></label>
                        @if ($errors->has('username') || $errors->has('email'))
                            <span class="invalid-feedback">{{ $errors->first('username') ?: $errors->first('email') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <input autocomplete="new-password" id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                        <label for="password" class="control-label">{{trans('login.password')}} <span class="star">*</span></label>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback">{{ $errors->first('password') ?: $errors->first('password') }}</span>
                        @endif
                    </div>

                    <div class="checkbox mb-5">
                        <label>
                            <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}><i class="helper"></i>{{trans('login.remember')}}
                        </label>
                        <p class="mt-2"> <a href="{{ route('password.request') }}">{{trans('login.forgot_password')}} </a></p>
                    </div>

                    <div class="text-center">
                        <button type="submit" class="btn btn-blue-gradient">{{trans('login.login')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <img src="/img/register-wave.svg" alt="" class="w-100 register-wave">
    <img src="/img/register-bg.svg" alt="" class="w-100 register-bg d-none d-lg-block">


@endsection






