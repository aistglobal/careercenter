@extends('layouts.app')

@section('css')
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container-fluid form-container">
        <h1 class="title-h1 font-bold text-center">{{trans('register_step_1.registration')}}</h1>

        <div class="row justify-content-center">
            <div class="col-auto w-100 max-445 position-relative z-index-1">
                <form method="POST" action="{{ route('user.register', app()->getLocale()) }}">
                    @csrf

                    <div class="form-group">
                        <input type="text" id="name" name="username"
                               class="{{ $errors->has('username') || $errors->has('email') ? ' is-invalid' : '' }}"
                               value="{{ old('username') }}" required>
                        <label for="name" class="control-label">{{trans('register_step_1.username')}} <span
                                    class="star">*</span></label>
                        @if ($errors->has('username') || $errors->has('email'))
                            <span class="invalid-feedback">{{ $errors->first('username') ?: $errors->first('email') }}</span>
                        @endif
                    </div>

                    <div class="form-group show_hide_password">
                        <input type="password" required id="password" name="password" autocomplete="new-password"
                               class="{{ $errors->has('password')  ? ' is-invalid' : '' }}">
                        <label for="password" class="control-label">{{trans('register_step_1.password')}} <span
                                    class="star">*</span></label>
                        <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback">{{ $errors->first('password') }}</span>
                        @endif
                    </div>

                    <div class="form-group show_hide_password">
                        <input type="Password" id="password-confirm" name="password_confirmation" required
                               autocomplete="new-password"
                               class="{{ $errors->has('password_confirmation')  ? ' is-invalid' : '' }}">
                        <label for="password-confirm" class="control-label">{{trans('register_step_1.confirm_pass')}}
                            <span class="star">*</span></label>
                        <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                        @if ($errors->has('password_confirmation'))
                            <span class="invalid-feedback">{{ $errors->first('password_confirmation') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <select name="is_organization" id="is_organization" required
                                class="{{ $errors->has('is_organization')  ? ' is-invalid' : '' }}">
                            <option></option>
                            <option @if(old('is_organization')!=null && old('is_organization') =='1')selected
                                    @endif  value="1">{{trans('user_category.org')}}</option>
                            <option @if(old('is_organization')!=null && old('is_organization') =='0')selected
                                    @endif value="0">{{trans('user_category.reg')}}</option>
                        </select>
                        <label for="is_organization"
                               class="control-label">{{trans('register_step_1.user_category') }}</label>
                        @if ($errors->has('is_organization'))
                            <span class="invalid-feedback">{{ $errors->first('is_organization') }}</span>
                        @endif
                    </div>

                    <div class="checkbox checkbox-agree">
                        <label>
                            <input @if(old('agree_terms')!=null ) checked @endif  required name="agree_terms"
                                   type="checkbox" class="{{ $errors->has('agree_terms')  ? ' is-invalid' : '' }}"><i
                                    class="helper"></i>
                        </label>
                        <p>{{trans('register_step_1.agree')}} <a href="{{route('pages','terms-and-conditions')}}"
                                                                 target="_blank">{{trans('register_step_1.terms')}}.</a>
                        </p>
                        @if ($errors->has('is_organization'))
                            <span class="invalid-feedback">{{ $errors->first('is_organization') }}</span>
                        @endif
                    </div>
                    <p style="font-size: 14px; display: @if(old('is_organization') == 1) block @else none @endif"
                        id="note_for_companies">{{trans('register_step_1.company_registration_note')}}</p>
                    <div class="text-center mt-2">
                        <button type="submit" class="btn btn-blue-gradient">{{trans('register_step_1.next')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <img src="/img/register-wave.svg" alt="" class="w-100 register-wave">
    <img src="/img/register-bg.svg" alt="" class="w-100 register-bg d-none d-lg-block">


@endsection

@section('scripts')
    <script src="https://use.fontawesome.com/b9bdbd120a.js"></script>
    <script>
        $(document).ready(function () {
            $('#is_organization').on('change', function () {
                var val = $(this).val();
                $('#note_for_companies').css('display', 'none');
                if (val == 1) {
                    $('#note_for_companies').css('display', 'block');
                }
            })

            $(".show_hide_password a").on('click', function (event) {
                event.preventDefault();
                if ($('.show_hide_password input').attr("type") == "text") {
                    $('.show_hide_password input').attr('type', 'password');
                    $('.show_hide_password i').addClass("fa-eye-slash");
                    $('.show_hide_password i').removeClass("fa-eye");
                } else if ($('.show_hide_password input').attr("type") == "password") {
                    $('.show_hide_password input').attr('type', 'text');
                    $('.show_hide_password i').removeClass("fa-eye-slash");
                    $('.show_hide_password i').addClass("fa-eye");
                }
            });
        });
    </script>

@endsection