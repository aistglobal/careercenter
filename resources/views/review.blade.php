@extends('layouts.app')
@section('css')

    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="blue-gradient"></div>
    <section class="position-relative review-page">
        <img src="/img/review-path.png" alt="" class="review-path img-fluid">
        <div class="review-page-content">
            <h1 class="title-h1 text-sm-left text-center">{{trans('reviews.reviews')}}</h1>
            @foreach($reviews as $item)
            <div class="review d-flex flex-sm-row flex-column align-items-sm-start align-items-center">
                <div class="review-img">
                    <img src="{{Storage::disk('')->url($item->image)}}" alt="">
                    <div class="review-author">{{$item->data->name}}</div>
                    <div class="author-position">{{$item->data->profession}}</div>
                </div>
                <div class="review-text">
                    <div class="stars">
                        <img src="/img/review-star.svg" alt="">
                        <img src="/img/review-star.svg" alt="">
                        <img src="/img/review-star.svg" alt="">
                        <img src="/img/review-star.svg" alt="">
                        <img src="/img/review-star.svg" alt="">
                    </div>
                {{$item->data->review}}
                </div>

            </div>
            @endforeach
            {{$reviews->links()}}

        </div>

    </section>



@endsection
