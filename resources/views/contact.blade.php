@extends('layouts.app')

@section('css')
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="position-relative">
        <img src="/img/contact-bg.svg" alt="contact" class="contact-bg w-100 d-none d-xl-block">

        <section
                class="contact-container d-flex flex-wrap flex-lg-nowrap w-100 justify-content-center justify-content-lg-between">
            <div class="contact-left">
                <h1>{{trans('contact.contact_us')}}</h1>
                <p>{{trans('contact.description')}}</p>

                <address>
                    <div class="address-item">
                        <img src="/img/phone-contact.svg" alt="phone-contact">
                        <a href="tel: {{trans('contact.telephone')}}">{{trans('contact.telephone')}}</a>
                    </div>
                    <div class="address-item">
                        <img src="/img/envelope.svg" alt="envelope">
                        <a href="mailto:{{trans('contact.mail')}}">{{trans('contact.mail')}}</a>
                    </div>
                    <div class="address-item">
                        <img src="/img/internet.svg" alt="internet">
                        <a href="#">{{trans('contact.website')}}</a>
                    </div>
                    <div class="address-item">
                        <img src="/img/maps-and-flags.svg" alt="maps-and-flags">
                        <span>{{trans('contact.address')}} </span>
                    </div>
                </address>
            </div>
            <div class="contact-form">
                <img src="/img/contact-form-bg.svg" alt="contact-form" class="w-100">
                <form action="{{route('contact-us')}}" method="POST">
                    @csrf
                    @if(Auth::check())
                        <div class="form-group">
                            <input name="name" type="hidden" id="name" required value="{{Auth::user()->first_name}} {{Auth::user()->first_name}}">
                            {!! $errors->first("name", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group">
                            <input name="email" type="hidden" id="email" required value="{{Auth::user()->email}}">
                            {!! $errors->first("email", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    @else
                        <div class="form-group">
                            <input name="name" type="text" id="name" required value="{{old('name')}}">
                            <label for="name" class="control-label">{{trans('contact.name')}}</label>
                            {!! $errors->first("name", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group">
                            <input name="email" value="{{old('name')}}" id="email" type="email" required>
                            <label for="email" class="control-label">{{trans('contact.email')}}</label>
                            {!! $errors->first("email", '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    @endif
                    <div class="form-group">
                        <textarea required rows="3" name="text" cols="50" id="message"
                                  data-required="no">{{old('text')}}</textarea>
                        <label for="message" class="control-label">{{trans('contact.message')}}</label>
                        {!! $errors->first("text", '<p style="color:red" class="help-block">:message</p>') !!}
                    </div>
                    <div>
                        <div class="g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">

                        </div>
                        {!! $errors->first("g-recaptcha-response", '<p style="color:red" class="help-block">:message</p>') !!}
                    </div>
                    <div class="text-center mt-3">
                       <button type="submit" class="btn btn-blue-gradient">{{trans('contact.send')}}</button>
                    </div>
                </form>
            </div>
        </section>
    </div>





    <!-- Modal -->
    <div id="success_tic" class="success-modal modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <!-- Modal content-->
            <div class="modal-content">
                <a class="close" href="#" data-dismiss="modal">&times;</a>
                <div class="page-body">
                    <div class="head">
                        <h3 class="">{{trans('contact.thank_you')}}</h3>
                        <h5 class="mb-5">{{trans('contact.mail_success')}}</h5>
                    </div>

                    <h1 class="text-center">
                        <span class="checkmark-circle">
                            <span class="background"></span>
                            <span class="checkmark draw"></span>
                        </span>
                    </h1>
                </div>
            </div>
        </div>

    </div>

@endsection


@section('scripts')
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script>
        @if(Session::has('contact-us'))
        $('#success_tic').modal('show')
        {{Session::forget('contact-us')}}
        @endif


    </script>
@endsection

