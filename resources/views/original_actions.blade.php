<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
    @if($first->status == 'pending' || $first->status=='draft')

        @permission('edit_announcement')
        <a title="{{trans('profile_tables.edit')}}" class="dropdown-item"
           href="{{route('announcements-edit',$first->id)}}"><img
                    src="/img/pencil-edit.svg"
                    alt=""
                    height="20">{{trans('profile_tables.edit')}}
        </a>
        @endpermission

        @if(\Illuminate\Support\Facades\Auth::user()->can('accessAll_announcement'))

            @permission('delete_announcement')
            {!! Form::open([
'method'=>'DELETE',
'route' => ['job-destroy',$first->id],
'style' => 'display:inline'
]) !!}
            <input type="hidden" name="status" value="{{$job_status}}">
            {!! Form::button('<img src="/img/delete-button.svg">'.trans('announcements.delete'), array(
                                   'type' => 'submit',
                                   'class' => 'dropdown-item',
                                   'title' => trans('announcements.delete'),
                                   'onclick'=>'return confirm("Confirm delete?")'
                           )) !!}

            {!! Form::close() !!}
            @endpermission

            @permission('post_announcement')
            {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['status-change', $first->id]]) !!}
            {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.post'), array(
                                               'type' => 'submit',
                                               'class' => 'dropdown-item',
                                               'title' => trans('announcements.post'),
                                               )) !!}
            <input type="hidden" name="status" value="posted">
            {!! Form::close() !!}
            @endpermission

            @permission('postEmail_announcement')
            {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['status-change', $first->id]]) !!}
            <input type="hidden" name="status" value="postandemail">
            {!! Form::button('<img src="/img/postandemail.svg">'.trans('announcements.post_and_email'), array(
     'type' => 'submit',
     'class' => 'dropdown-item',
     'title' => trans('announcements.post_and_email'),
     )) !!}
            {!! Form::close() !!}
            @endpermission

            @permission('reject_announcement')
            {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['status-change', $first->id]]) !!}
            <input type="hidden" name="status" value="rejected">
            {!! Form::button('<img src="/img/reject.svg">'.trans('announcements.reject'), array(
'type' => 'submit',
'class' => 'dropdown-item',
'title' => trans('announcements.reject'),
)) !!}
            {!! Form::close() !!}
            @endpermission

        @else

            @permission('submit_announcement')
            {!! Form::open([
            'method'=>'post',
            'route' => ['jobs.submit',$first->id],
            'style' => 'display:inline'
            ]) !!}
            {!! Form::button('<img src="/img/checked.svg">'.trans('announcements.submit'), array(
            'type' => 'submit',
            'class' => 'dropdown-item',
            'title' => trans('announcements.submit'),
            )) !!}
            {!! Form::close() !!}
            @endpermission

        @endif
    @elseif($first->status == 'posted')
        @if($first->expiration_date>now())
            @permission('edit_announcement')
            <a title="{{trans('profile_tables.edit')}}" class="dropdown-item"
               href="{{route('announcements-edit',$first->id)}}"><img
                        src="/img/pencil-edit.svg"
                        alt=""
                        height="20">{{trans('profile_tables.edit')}}
            </a>
            @endpermission
            @if(Auth::user()->can('accessAll_announcement'))
                @permission('expired_announcement')
                {!! Form::open(['method' => 'POST','class'=>'status-form', 'route' => ['status-change', $first->id]]) !!}
                <input type="hidden" name="status" value="expired">
                {!! Form::button('<img src="/img/time.svg">'.trans('announcements.expired'), array(
'type' => 'submit',
'class' => 'dropdown-item',
'title' => trans('announcements.expired'),
)) !!}
                {!! Form::close() !!}
                @endpermission
            @endif
        @endif

        @permission('clone_announcement')
        {!! Form::open([
'method'=>'post',
'route' => ['jobs.clone',$first->id],
'style' => 'display:inline'
]) !!}
        {!! Form::button('<img src="/img/copy.svg">'.trans('announcements.clone'), array(
        'type' => 'submit',
        'class' => 'dropdown-item',
        'title' => trans('announcements.copy'),
        )) !!}

        {!! Form::close() !!}
        @endpermission

    @elseif($first->status=='expired' || $first->expiration_date<now())
        @permission('clone_announcement')
        {!! Form::open([
                          'method'=>'post',
                          'route' => ['jobs.clone',$first->id],
                          'style' => 'display:inline'
                          ]) !!}
        {!! Form::button('<img src="/img/copy.svg">'.trans('announcements.clone'), array(
        'type' => 'submit',
        'class' => 'dropdown-item',
        'title' => trans('announcements.copy'),
        )) !!}

        {!! Form::close() !!}
        @endpermission

    @elseif($first->status=='rejected')
        @permission('clone_announcement')
        {!! Form::open([
                 'method'=>'post',
                 'route' => ['jobs.clone',$first->id],
                 'style' => 'display:inline'
                 ]) !!}
        {!! Form::button('<img src="/img/copy.svg">'.trans('announcements.clone'), array(
        'type' => 'submit',
        'class' => 'dropdown-item',
        'title' => trans('announcements.copy'),
        )) !!}

        {!! Form::close() !!}
        @endpermission
    @endif
</div>