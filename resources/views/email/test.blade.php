<div class="d-flex align-items-start justify-content-between job-right-title">
    <h1>{{$first->data->title}}</h1>

    <span class="d-flex align-items-center job-view-count" flow="left" tooltip="{{trans('announcements.views_text')}}  {{$first->views}} {{trans('announcements.times')}}">
            <img src="/img/eye.svg" alt="" width="20">
        </span>

</div>
<div class="table-responsive d-flex justify-content-between align-items-start pr-4">
    <div>
        <div style="margin-bottom: 15px;">
            <span>{{trans('announcements.company')}}:</span>
            <span>
                {{$first->data->organization}}
            </span>
        </div>
        @if(isset($first->data->location_city))
            <div style="margin-bottom: 15px;">
                <span>{{trans('announcements.location')}}:</span>
                <span>{{$first->data->location_city}}</span>
            </div>
        @endif
        @if(isset($first->data->duration) && $first->data->duration!='')
            <div style="margin-bottom: 15px;">
                <span>{{trans('announcements.duration')}}:</span>
                <span>{{$first->data->duration}}</span>
            </div>
        @endif
        @if(isset($first->data->start_date) && $first->data->start_date!='')
            <div style="margin-bottom: 15px;">
                <span>{{trans('announcements.start_date')}}:</span>
                <span>{{$first->data->start_date}}</span>
            </div>
        @endif
        @if(isset($first->data->deadline) && $first->data->deadline!='')
            <div style="margin-bottom: 15px;">
                <span>{{trans('announcements.deadline')}}:</span>
                <span>{{$first->data->deadline}}</span>
            </div>
        @endif

    </div>
    {{--@if(isset($first->user->image) && $first->user->image)--}}
        {{--<picture>--}}
            {{--<source srcset="{{Storage::disk('')->url($first->user->image)}}" type="image/jpeg">--}}
            {{--<img src="{{Storage::disk('')->url($first->user->image)}}" class="job-logo">--}}
        {{--</picture>--}}
    {{--@endif--}}

</div>

<div class="tab-content job-tab-content">
    <div class="tab-pane fade show active" id="job" role="tabpanel" aria-labelledby="job-tab">
        @if(isset($first->data->term) && $first->data->term!='')
            @if($first->type->key =='job_opp' || $first->type->key =='volunteering' || $first->type->key=='internship')
                <h4 style="margin: 0;  @if(!\App\Job::is_enumeration($first->data->term)) margin-top: 0;margin-right: 2px;float:left;clear:both; @endif">{{trans('announcements.term')}} : </h4>
            @else
                <h4 style="margin: 0;  @if(!\App\Job::is_enumeration($first->data->term))     clear: both;float:left;margin-top:0;margin-right: 2px; @endif">{{trans('announcements.'.$first->type->key.'_type')}}
                    : </h4>
            @endif
             {{$first->data->term}}
            <div style="clear: both;margin-bottom: 15px;"></div>
        @endif
        @if($first->type->key =='job_opp' || $first->type->key =='volunteering' || $first->type->key=='internship')
            <h4 style="margin: 0; @if(!\App\Job::is_enumeration($first->data->description)) float:left;margin-top:0;margin-right: 2px;clear:both; @endif">{{trans('announcements.description')}}: </h4>
        @elseif($first->type->key =='news')
            <h4 style="margin: 0; @if(!\App\Job::is_enumeration($first->data->description)) float:left;margin-top:0;margin-right: 2px;clear:both; @endif">{{trans('announcements.news_details')}}: </h4>
        @else
            <h4 style="margin: 0; @if(!\App\Job::is_enumeration($first->data->description)) float:left;margin-top:0;margin-right: 2px;clear:both; @endif">{{trans('announcements.detail_description')}}: </h4>
        @endif

        {!!\App\Job::emailize($first->data->description)!!}
        <div style="clear: both;margin-bottom: 15px;"></div>

        @if(isset($first->data->responsibilities) && $first->data->responsibilities!='')
            @if($first->type->key =='job_opp' ||$first->type->key =='volunteering' || $first->type->key=='internship')
                <h4 style="margin: 0; @if(!\App\Job::is_enumeration($first->data->responsibilities)) float:left;margin-top:0;margin-right: 2px;clear:both; @endif">{{trans('announcements.responsibilities')}} : </h4>
            @else
                <h4 style="margin: 0; @if(!\App\Job::is_enumeration($first->data->responsibilities)) float:left;margin-top:0;margin-right: 2px;clear:both; @endif">{{trans('announcements.educational_level')}} : </h4>
            @endif
            {!! \App\Job::emailize($first->data->responsibilities) !!}
            <div style="clear: both;margin-bottom: 15px;"></div>
        @endif
        @if(isset($first->data->requirements) && $first->data->requirements!='')
            @if($first->type->key =='job_opp' || $first->type->key =='volunteering' || $first->type->key=='internship')
                <h4 style="margin: 0; @if(!\App\Job::is_enumeration($first->data->requirements)) float:left;margin-top:0;margin-right: 2px;clear:both; @endif">{{trans('announcements.required_qualifications')}} : </h4>
            @else
                <h4 style="margin: 0; @if(!\App\Job::is_enumeration($first->data->requirements)) float:left;margin-top:0;margin-right: 2px;clear:both; @endif">{{trans('announcements.requirements')}} : </h4>
            @endif
            {!! \App\Job::emailize($first->data->requirements) !!}
            <div style="clear: both;margin-bottom: 15px;"></div>
        @endif
        @if(isset($first->data->procedures) && $first->data->procedures!='')
            <h4 style="margin: 0; @if(!\App\Job::is_enumeration($first->data->procedures)) float:left;margin-top:0;margin-right: 2px;clear:both; @endif">{{trans('announcements.app_procedures')}} : </h4>
            {{--{!!\App\Job::procedures(\App\Job::emailize($first->data->procedures),$first,$applied_job)!!}--}}
            {!! \App\Job::emailize($first->data->procedures) !!}
            <p>{!!trans('announcements.application_procedures_default_text') !!}</p>
            <div style="clear: both;margin-bottom: 15px;"></div>
        @endif
        @if(isset($first->data->salary) && $first->data->salary!='')
            <h4 style="margin: 0;  @if(!\App\Job::is_enumeration($first->data->salary)) float:left;margin-top:0;margin-right: 2px;clear:both; @endif">{{trans('announcements.salary')}} : </h4>
            {{$first->data->salary}}
            <div style="clear: both;margin-bottom: 15px;"></div>
        @endif
        @if(isset($first->data->open) && $first->data->open!='')
            <h4 style="margin: 0;  @if(!\App\Job::is_enumeration($first->data->open)) float:left;margin-top:0;margin-right: 2px;clear:both; @endif">{{trans('announcements.open')}} : </h4>
            {{$first->data->open}}
            <div style="clear: both;margin-bottom: 15px;"></div>
        @endif
        @if(isset($first->data->audience) && $first->data->audience!='')
            <h4 style="margin: 0; @if(!\App\Job::is_enumeration($first->data->audience)) float:left;margin-top:0;margin-right: 2px;clear:both; @endif">{{trans('announcements.audience')}} : </h4>
            {{$first->data->audience}}
            <div style="clear: both;margin-bottom: 15px;"></div>
        @endif
        @if(isset($first->data->notes) && $first->data->notes!='' )
            <h4 style="margin: 0; @if(!\App\Job::is_enumeration($first->data->notes)) float:left;margin-top:0;margin-right: 2px;clear:both; @endif">{{trans('announcements.notes')}} : </h4>
            {!!\App\Job::emailize($first->data->notes)!!}
            <div style="clear: both;margin-bottom: 15px;"></div>
        @endif
        @if(isset($first->data->about) && $first->data->about!='')
            <h4 style="margin: 0; @if(!\App\Job::is_enumeration($first->data->about)) float:left;margin-top:0;margin-right: 2px;clear:both; @endif">{{trans('announcements.about')}} : </h4>
            {!!\App\Job::emailize($first->data->about)!!}
            <div style="clear: both;margin-bottom: 15px;"></div>
        @endif
        @if(isset($first->data->author) && $first->data->author!='' )
            <h4 style="margin: 0; @if(!\App\Job::is_enumeration($first->data->author)) float:left;margin-top:0;margin-right: 2px;clear:both; @endif">{{trans('announcements.author')}} : </h4>
            {{$first->data->author}}
            <div style="clear: both;margin-bottom: 15px;"></div>
        @endif
        @if(isset($first->data->pages) && $first->data->pages!='' )
            <h4 style="margin: 0; @if(!\App\Job::is_enumeration($first->data->pages)) float:left;margin-top:0;margin-right: 2px;clear:both; @endif">{{trans('announcements.pages')}} : </h4>
            {{$first->data->pages}}
            <div style="clear: both;margin-bottom: 15px;"></div>
        @endif
        @if(isset($first->data->language) && $first->data->language!='')
            <h4 style="margin: 0; @if(!\App\Job::is_enumeration($first->data->language)) float:left;margin-top:0;margin-right: 2px;clear:both; @endif">{{trans('announcements.language')}} : </h4>
            {{$first->data->language}}
            <div style="clear: both;margin-bottom: 15px;"></div>
        @endif
        {{--@if(count($first->attachment)!=0)--}}
            {{--<h4 style="">{{trans('announcements.attachments')}} : </h4>--}}
            {{--@foreach($first->attachment as $file)--}}
                {{--@if($file->image==1)--}}
                    {{--<br>--}}
                    {{--<a href="{{Storage::disk('')->url($file->file)}}" target="_blank"--}}
                       {{--class="jobs-img jobs-img-description">--}}
                        {{--<img src="{{Storage::disk('')->url($file->file)}}" alt="">--}}
                        {{--<span>{{$file->description}}</span>--}}
                    {{--</a>--}}
                {{--@else--}}
                    {{--<span class="jobs-img-file">--}}
                                        {{--<a href="{{Storage::disk('')->url($file->file)}}" download>--}}
                                            {{--<img src="/img/down-white.svg" alt="" width="17" height="17"--}}
                                                 {{--class="mr-2">{{$file->name}} - {{$file->description}}</a>--}}
                                    {{--</span>--}}
                {{--@endif--}}
            {{--@endforeach--}}
        {{--@endif--}}
    </div>
</div>