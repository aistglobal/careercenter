<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>careercenter</title>
    <style type="text/css">

        .btn-orange-gradient {
            display: inline-block;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;

            font-size: 14px;
            border-radius: 28px;
            color: #fff;

            padding: 16px 25px;
            border: none;
            word-break: keep-all;
            background: -webkit-gradient(linear, left top, right top, from(#f3994b), color-stop(50%, #ff5f6d), to(#f3994b));
            background: linear-gradient(90deg, #f3994b 0, #ff5f6d 50%, #f3994b);
            -webkit-transition: .5s;
            transition: .5s;
            background-size: 200% auto;
            text-decoration: none;
        }
        .btn-orange-gradient:hover{
            background-position: 100%;
        }

        div, p, a, li, td {
            -webkit-text-size-adjust: none;
        }

        p {
            font-size: 14px;
            Margin-bottom: 10px;
        }

        body {
            Margin: 0;
            padding: 0;
            min-width: 100%;
            background-color: #ffffff;
        }

        table {
            border-spacing: 0;
            font-family: sans-serif;
            color: #333333;
        }

        td {
            padding: 0;
        }

        img {
            border: 0;
        }

        a {
            border-radius: 3px;
        }

        .wrapper {
            width: 100%;
            table-layout: fixed;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        .webkit {
            max-width: 600px;
        }

        .outer {
            Margin: 0 auto;
            width: 100%;
            max-width: 600px;
            -webkit-box-shadow: inset 0px -1px 5px 0px rgba(0, 0, 0, 0.75);
            -moz-box-shadow: inset 0px -1px 5px 0px rgba(0, 0, 0, 0.75);
            box-shadow: inset 0px -1px 5px 0px rgba(0, 0, 0, 0.75);
        }

        .full-width-image img {
            width: 100%;
            max-width: 600px;
            height: auto;
        }

        .inner {
            padding: 20px;
        }

        a {
            text-decoration: underline;
        }

        .contents {
            width: 100%;
        }

        .two-column .contents {
            font-size: 14px;
            text-align: center;
            color: #000;
        }

        .two-column img {
            width: 100%;
            max-width: 300px;
            height: auto;
        }

        .three-column .contents {
            font-size: 14px;
            text-align: center;
        }

        .three-column img {
            width: 100%;
            max-width: 60px;
            height: auto;
        }

        @media (max-width: 400px) {
            .media-content{
                padding-right: 20px!important;
                padding-left: 20px!important;
            }
        }
    </style>

    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        table {
            border-collapse: collapse !important;
        }
    </style>
    <![endif]-->

</head>
<body style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#ffffff;">
<center class="wrapper" style="width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;">
    <div class="webkit" style="max-width:600px;">
        <!--[if (gte mso 9)|(IE)]>
        <table width="600" align="center" style="border-spacing:0;font-family:sans-serif;color:#333333;">
            <tr>
                <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
        <![endif]-->
        <table class="outer" align="center"
               style="border-spacing:0;font-family:sans-serif;color:#333333;Margin:0 auto;width:100%;max-width:600px;-webkit-box-shadow:inset 0px -1px 5px 0px rgba(0,0,0,0.75);-moz-box-shadow:inset 0px -1px 5px 0px rgba(0,0,0,0.75);box-shadow:inset 0px -1px 5px 0px rgba(0,0,0,0.75);">
            <tr>
                <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                    <table width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;">
                        <tr>
                            <td class="inner contents" width="600" height="40"
                                style="background: linear-gradient(180deg,#006EB7 0,#149FBE);padding-top:18px;padding-bottom:18px;padding-right:20px;padding-left:50px;">
                                <a href="https://www.example.com" valign="middle"
                                   style="text-decoration:none; font-family:arial, sans-serif; color:#ffffff; font-weight:bold;">
                                    <img src="https://res.cloudinary.com/chess/image/upload/v1568111280/CareerCenter.am_q046ek.png">
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td class="one-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                    <table width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;">
                        <tr>
                            <td class="one-column"
                                style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                <table width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;">
                                    <tr>
                                        <td class="inner contents media-content"
                                            style="background-color:#ffffff; border: solid 1px #cdcdcd; border-top: none;border-bottom: none;padding-top: 12px;padding-bottom:40px;padding-right:52px;padding-left:52px;width:100%;color:#000000; font-size:20px;">
                                            <p style="font-size:16px;margin-bottom: 7px;">Հարգելի օգտատեր</p>
                                            <p style="font-size:16px;margin-top: 0;margin-bottom: 7px;">Նոր կայքի գործարկման անվտանգության պահանջների կապակցությամբ ձեր գաղտնաբառը փոխվել է:
                                              </p>
                                            <p style="font-size:16px;margin-top: 0;">  Ձեր նոր գաղտնաբառն է/ Your new password is:{{$data['password']}} </p>
                                            <p style="font-size:16px;margin-bottom: 39px;">Խորհուրդ է տրվում հաջորդ անգամ կայք մուտք գործելիս այն կրկին ինքնուրույն փոխել:
                                            </p>
                                            <p style="font-size:16px;margin-bottom: 7px;">
                                                Հարգանքներով,
                                                Կարիերայի Կենտրոն
                                            </p>
                                            <p style="font-size:16px;margin-bottom: 7px; margin-top: 0; font-weight: bold;">
                                                Հարցերի դեպքում կարող եք կապ հաստատել մեզ հետ կայքում նշված կոնտակտային միջոցներով:
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>


                        <tr>
                            <td class="one-column" style="background-color: #0274b7; padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                <table width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;">
                                    <tr>
                                        <td class="inner contents" align="left" style=" color:#333333;font-size:16px;padding-top:16px;padding-bottom:13px;padding-right:20px;padding-left:52px; width:600px;">
                                            <p style="font-size:14px; color:#fff; margin-top: 0;margin-bottom: 9px;">
                                                <img src="https://res.cloudinary.com/chess/image/upload/v1568114281/phone-contact_1_fsn8ft.png" alt="" style="margin-right: 5px;">
                                                <a href="tel:+37410519062" style="color: #fff;text-decoration: none;vertical-align: top;">+374 10 56 03 28</a>
                                            </p>
                                            <p style="color:#fff; font-size:14px; margin-top: 0;margin-bottom: 0;">
                                                <img src="https://res.cloudinary.com/chess/image/upload/v1568114281/Group_1626_qake4m.png" alt="" style="margin-right: 5px;">
                                                <a href="#" style="color:#fff; text-decoration:none;vertical-align: top;">mailbox@careercenter.am</a>
                                            </p>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="right" width="100%" height="10" style="background-color:#003553;font-size: 14px; color: #fff;padding-top: 5px;padding-right: 47px;padding-bottom: 5px">
                                            © 2002-2019  CareerCenter
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
        </table>
    </div>
</center>
</body>
</html>