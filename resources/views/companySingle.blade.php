@extends('layouts.app')
@section('css')
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="blue-gradient"></div>
    <section class="position-relative company-page">

        <img src="/img/company-bg.svg" alt="" class="company-bg img-fluid">

        <div class="company-page-content">
            @if(isset($user->image))
                <img src="{{Storage::disk('')->url($user->image)}}" alt="" class="img-fluid mb-5">
            @endif
            <h3>{{$company->name}} @if(isset($company->legal_structure) && $company->legal_structure!='other'  && $company->legal_structure!='brand')
                    @if(\Lang::has('legal_structure.'.$company->legal_structure))
                        {{trans('legal_structure.'.$company->legal_structure)}}
                    @else
                        {{$company->legal_structure}}
                    @endif
                @endif
            </h3>
            @if(isset($company->about_company) && $company->about_company!='')
                <h4>{{trans('company.about_us')}}:</h4>
                <p>{!! $company->about_company !!}</p>
            @endif
            @if(Auth::check() && Auth::user()->can('accessAll_announcement'))
                <h4>{{trans('company.contact_us')}}: </h4>

                <address class="company-address">

                    @if(isset($user->address) && count($user->address)!=0)
                        @foreach($user->address as $item)
                            <span class="d-block">
                    {{$item->address}},
                                {{$item->city}}
                                , {{trans('countries.'.$item->country->code)}}</span>
                        @endforeach
                    @endif
                    @if(isset($phone))
                        @foreach($phone as $item)
                            <span class="d-block"><b>{{trans('company.tel')}}:</b>
                        <a href="tel:{{$item['phone']}}">{{$item['phone']}}</a></span>
                        @endforeach

                    @endif
                    @if(isset($user->email))
                        <span class="d-block"><b>{{trans('company.email')}}:</b><a
                                    href="mailto:{{$user->email}}">{{$user->email}}</a></span>
                    @endif
                </address>
            @endif

            <br>
            <br>
            @if(count($user->posted_job)!=0)
                <h4>{{trans('company.working_at')}} {{$company->name}} : </h4>
                <div class="d-flex flex-wrap justify-content-between">
                    @foreach($user->posted_job as $item)
                        @if(isset($item->data))
                            <a href="{{route('jobs',$item->slug)}}" class="company-work d-flex align-items-center">
                                <div>
                                    <p>{{$item->data->title}}</p>
                                    <span>{{$item->data->deadline}}</span>
                                </div>
                                <span class="ml-auto learn-span">{{trans('company.learn_more')}}</span>
                            </a>
                        @endif
                    @endforeach

                </div>
            @endif
        </div>

    </section>



@endsection
