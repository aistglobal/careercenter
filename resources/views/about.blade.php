@extends('layouts.app')

@section('css')
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
@endsection

@section('content')

    <header class="about-header position-relative">
        <img src="/img/about-bg.svg" alt="" class="about-bg">
        <img src="/img/about-bg-1.png" alt="" class="about-head-img-1">
        <img src="/img/about-bg-2.png" alt="" class="about-head-img-2">
        <div class="about-head-text text-center">
            <img src="/img/logo-white.svg" alt="">
            <h1>

                {{trans('about.title')}}
            </h1>
        </div>
    </header>


    <section class="about-container">


        <h3 class="text-center">
            {{trans('about.what_can_individuals_do')}}
        </h3>

        <p>{{trans('about.what_can_individuals_do_first_line')}}</p>

        <ol>
            <li>{{trans('about.what_can_individuals_do_list_first')}}</li>
            <li>{{trans('about.what_can_individuals_do_list_second')}}</li>
            <li>{{trans('about.what_can_individuals_do_list_third')}}</li>
        </ol>

        <h3 class="text-center">
            {{trans('about.what_can_employers_do')}}
        </h3>

        <p>{{trans('about.what_can_employers_do_first_line')}}</p>

        <ol>
            <li>{{trans('about.what_can_employers_do_list_first')}}</li>
            <li>{{trans('about.what_can_employers_do_list_second')}}</li>
            <li>{{trans('about.what_can_employers_do_list_third')}}</li>
        </ol>

        <br>
        <p>{{trans('about.what_can_employers_do_first_line_second')}}</p>
        <br>
        <p class="text-center" style="color: #006db7">
            {{trans('about.bottom_first_line')}}
            <br>
            {{trans('about.bottom_second_line')}}
        </p>
        {{--<p> {{trans('about.what_can_individuals_do_list_first')}} </p>--}}
        {{--<p> {{trans('about.what_can_individuals_do_list_second')}} </p>--}}
        {{--<p> {{trans('about.what_can_individuals_do_list_third')}} </p>--}}
        {{--<h3>They Trust Us</h3>--}}

        {{--<div class="about-slider">--}}
            {{--@foreach($jobs as $item)--}}
                {{--<div>--}}
                    {{--<div class="about-slider-container d-flex flex-wrap justify-content-between">--}}
                        {{--<div class="about-slider-block">--}}
                            {{--<img data-src="/img/easypay.png" alt="" class="lazyload">--}}
                            {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum--}}
                                {{--has been the industry’s standard </p>--}}
                        {{--</div>--}}
                        {{--<div class="about-slider-block">--}}
                            {{--<img data-src="/img/beeline.png" alt="" class="lazyload">--}}
                            {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum--}}
                                {{--has been the industry’s standard Lorem Ipsum is simply dummy text of the printing--}}
                                {{--and </p>--}}
                        {{--</div>--}}
                        {{--<div class="about-slider-block">--}}
                            {{--<img data-src="/img/menu.png" alt="" class="lazyload">--}}
                            {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>--}}
                        {{--</div>--}}
                        {{--<div class="about-slider-block">--}}
                            {{--<img data-src="/img/allme.png" alt="" class="lazyload">--}}
                            {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum--}}
                                {{--has been the industry’s standard </p>--}}
                        {{--</div>--}}
                        {{--<div class="about-slider-block">--}}
                            {{--<img data-src="/img/viva.png" alt="" class="lazyload">--}}
                            {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum--}}
                                {{--has been the industry’s standard </p>--}}
                        {{--</div>--}}
                        {{--<div class="about-slider-block">--}}
                            {{--<img data-src="/img/solo.png" alt="" class="lazyload">--}}
                            {{--<p>Lorem Ipsum is simply </p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--@endforeach--}}
            {{--<div>--}}
                {{--<div class="about-slider-container d-flex flex-wrap justify-content-between">--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/easypay.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has--}}
                            {{--been the industry’s standard </p>--}}
                    {{--</div>--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/beeline.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has--}}
                            {{--been the industry’s standard Lorem Ipsum is simply dummy text of the printing and </p>--}}
                    {{--</div>--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/menu.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>--}}
                    {{--</div>--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/allme.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has--}}
                            {{--been the industry’s standard </p>--}}
                    {{--</div>--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/viva.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has--}}
                            {{--been the industry’s standard </p>--}}
                    {{--</div>--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/solo.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply </p>--}}
                    {{--</div>--}}
                {{--</div>--}}


            {{--</div>--}}
            {{--<div>--}}
                {{--<div class="about-slider-container d-flex flex-wrap justify-content-between">--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/easypay.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has--}}
                            {{--been the industry’s standard </p>--}}
                    {{--</div>--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/beeline.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has--}}
                            {{--been the industry’s standard Lorem Ipsum is simply dummy text of the printing and </p>--}}
                    {{--</div>--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/menu.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>--}}
                    {{--</div>--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/allme.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has--}}
                            {{--been the industry’s standard </p>--}}
                    {{--</div>--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/viva.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has--}}
                            {{--been the industry’s standard </p>--}}
                    {{--</div>--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/solo.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply </p>--}}
                    {{--</div>--}}
                {{--</div>--}}


            {{--</div>--}}
            {{--<div>--}}
                {{--<div class="about-slider-container d-flex flex-wrap justify-content-between">--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/easypay.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has--}}
                            {{--been the industry’s standard </p>--}}
                    {{--</div>--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/beeline.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has--}}
                            {{--been the industry’s standard Lorem Ipsum is simply dummy text of the printing and </p>--}}
                    {{--</div>--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/menu.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>--}}
                    {{--</div>--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/allme.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has--}}
                            {{--been the industry’s standard </p>--}}
                    {{--</div>--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/viva.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has--}}
                            {{--been the industry’s standard </p>--}}
                    {{--</div>--}}
                    {{--<div class="about-slider-block">--}}
                        {{--<img data-src="/img/solo.png" alt="" class="lazyload">--}}
                        {{--<p>Lorem Ipsum is simply </p>--}}
                    {{--</div>--}}
                {{--</div>--}}


            {{--</div>--}}
        {{--</div>--}}
    </section>

@endsection

@section('scripts')
    <script>
        $('.about-slider').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            // lazyLoad: 'ondemand',
            autoplay: false,
            autoplaySpeed: 4000
        });
    </script>
@endsection