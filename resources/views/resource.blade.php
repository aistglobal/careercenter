@extends('layouts.app')

@section('css')
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="resource-container d-flex w-100">
        <ul class="list-unstyled resource-list-left">
            @foreach($categories as $item)

                <li class="{{$activeItem === $item->slug ? 'active' : ''}}"><a
                            href="{{route('resource.index',['c'=>$item->slug])}}">{{$item->data->name}}</a></li>
            @endforeach
        </ul>
        <div class="resource-right">

            @if(isset($activeItem))
                @if(isset($sub) && count($sub))
                    <ul class="list-unstyled resource-list-right">
                        @foreach($sub as $item)

                            <li class="{{$activeSub === $item->slug ? 'active' : ''}}"><a
                                        href="{{route('resource.index',['c'=>$activeItem,'s'=>$item->slug])}}">{{$item->data->name ?? ''}}</a></li>
                        @endforeach
                    </ul>
                @else
                    <ul class="list-unstyled resource-list-right no-sub">
                        @foreach($resources as $item)
                            @if(isset($item->data))
                                <li><a href="{{$item->url}}" rel="noopener" target="_blank">{{$item->data->name ?? ''}}</a></li>
                            @endif
                        @endforeach
                    </ul>

            <div class="new-pagination">
                {{$resources->appends(Request::query())->links()}}
            </div>
                    @endif
                @endif
        </div>
        <div class="resource-right">
            @if(isset($activeItem) && isset($activeSub))
                <ul class="list-unstyled resource-list-right no-sub">
                    @foreach($resources as $item)
                        @if(isset($item->data))
                            <li><a href="{{$item->url}}" rel="noopener" target="_blank">{{$item->data->name ?? ''}}</a></li>
                        @endif
                    @endforeach
                </ul>
                <div class="new-pagination">
                    {{$resources->appends(Request::query())->links()}}
                </div>
                @endif
        </div>


        {{--<div class="sidebar ml-auto">--}}
            {{--@foreach($latestBlogs as $item)--}}
                {{--@if(isset($item->data))--}}
                {{--<div class="sidebar-block">--}}
                    {{--<a href="{{route('blog-single',$item->slug)}}" class="sidebar-item d-block">--}}
                        {{--<img data-src="{{Storage::disk('')->url($item->image)}}" alt="{{$item->data->title}}"--}}
                             {{--class="lazyload w-100" width="315" height="258">--}}
                        {{--<div class="sidebar-item-text">--}}
                            {{--<h4>{{$item->data->title}}</h4>--}}
                            {{--<span>{{$item->created_at->format('M d, Y')}}</span>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</div>--}}
                {{--@endif--}}
            {{--@endforeach--}}
        {{--</div>--}}
    </div>

@endsection
