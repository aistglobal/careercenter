(function(w, d){
    var b = d.getElementsByTagName('body')[0];
    var s = d.createElement("script");
    var v = !("IntersectionObserver" in w) ? "8.17.0" : "10.19.0";
    s.async = true; // This includes the script as async. See the "recipes" section for more information about async loading of LazyLoad.
    s.src = "https://cdn.jsdelivr.net/npm/vanilla-lazyload@" + v + "/dist/lazyload.min.js";
    w.lazyLoadOptions = {/* Your options here */};
    b.appendChild(s);
}(window, document));


$('#home-articles-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    lazyLoad: 'ondemand',
    autoplay: false,
    autoplaySpeed: 4000,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        }
    ]
});

$('#home-people-slider').slick({
    dots: false,
    slidesToShow: 1,
    centerMode: true,
    variableWidth: true,
    responsive: [

        {
            breakpoint: 575,
            settings: {
                slidesToShow: 1
            }
        }
    ]
});

// $(document).ready(function () {
//
//         $('.form-group textarea[data-required="no"]').each(function(){
//            if ( $(this).val() != '') {
//             $(this).parent().addClass('value-active');
//               }
//         });
//
//     $('.form-group').each(function (index, item) {
//         if ($(item).val()) {
//             $(this).parent().addClass('value-active')
//         }
//     });
//
//     var formInputs = $('input[type="email"],input[type="password"],input[type="text"],input[type="tel"],input[type="number"],input[type="url"],textarea[data-required="no"]');
//     formInputs.focus(function () {
//         $(this).parent().addClass('value-active')
//     });
//     formInputs.focusout(function () {
//         if ($.trim($(this).val()).length == 0) {
//             $(this).parent().removeClass('value-active')
//         }
//     });
//
//     if (performance.navigation.type == 2) { // 2 means page was accessed by navigating into the history (Browser Back Button)
//
//         $('.form-group textarea, .form-group input').each(function(){
//             if ( $(this).val() != '') {
//                 $(this).parent().addClass('value-active');
//             }
//         });
//
//     }
//
// });



$('.file-input-style input[type="file"]').each(function () {
    // get label text
    var label = $(this).parents('.form-group-file').find('label').text();
    label = (label) ? label : 'Upload File';
    // wrap the file input
    $(this).wrap('<div class="input-file"></div>');
    // display label
    $(this).before('<span class="btn">' + label + '</span>');
    // we will display selected file here
    $(this).before('<span class="file-selected"></span>');
    // file input change listener
    $(this).change(function (e) {
        // Get this file input value
        var val = $(this).val();

        // Let's only show filename.
        // By default file input value is a fullpath, something like
        // C:\fakepath\Nuriootpa1.jpg depending on your browser.
        var filename = val.replace(/^.*[\\\/]/, '');

        // Display the filename
        $(this).siblings('.file-selected').text(filename);
    });
});

// Open the file browser when our custom button is clicked.
$('.input-file .btn').click(function () {
    $(this).siblings('.file-input-style input[type="file"]').trigger('click');
});


$(document).ready(function(){
    $(".open_close-menu").click(function(){
        $('.left-menu').toggleClass('profile-menu-opened');
        $(this).toggleClass("active");
    });

});


if($('.left-menu').length > 0)
{
    // End upload preview image
    function checkOffset() {
        if ($('.left-menu').offset().top + $('.left-menu').height()
            >= $('footer').offset().top - 60)
            $('.left-menu').addClass('profile-menu-scroll');
        if ($(document).scrollTop() + window.innerHeight < $('footer').offset().top)
            $('.left-menu').removeClass('profile-menu-scroll');
    }
    $(document).scroll(function () {
        checkOffset();
    });
}



// mobile height
let vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty('--vh', `${vh}px`);
window.addEventListener('resize', () => {
    // We execute the same script as before
    let vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty('--vh', `${vh}px`);
});

function resize ($text) {
        $text.css('height', 'auto');
        $text.css('height',  $text[0].scrollHeight+'px');
}

function autosize(){
    var text = $('textarea');

    text.each(function(){
        $(this).attr('rows',1);
        resize($(this));
    });

    text.on('input', function(){
        resize($(this));
    });
}

$(document).ready(function(){
autosize();
});

$('.profile-tab').find('.nav-item').on('click',function () {
    setTimeout(function () {
        autosize();
    },200)

});

initInputs()

// $(document).on('change', '.form-group',function () {
//     initInputs()
// });
$(document).on('focus', '.form-group',function () {
    initInputs()
});
function initInputs(){

    $(".form-group:not(.no-focus)")
        .find(".form-control, select, input, textarea")
        .each(function () {
            var targetItem = $(this).parent();

            if ($(this).val()) {
                $(targetItem)
                    .find(".form-control, select, input, textarea")
                    .addClass("form-file");
            }
        });
    $(".form-group")
        .find(".form-control, select, input, textarea")
        .focus(function () {
            // $(this)
            //     .parent(".form-group")
            //     .addClass("form-file");
            $(this)
                .parent()
                .find(".form-control, select, input, textarea")
                .addClass("form-file");

        });
    $(".form-group")
        .find(".form-control, select, input, textarea")
        .blur(function () {
            if ($(this).val().length == 0) {
                $(this)
                    .parent(".form-group")
                    .removeClass("form-file");
                $(this)
                    .parent()
                    .find(".form-control, select, input, textarea")
                    .removeClass("form-file");
            }
        });

}

