<?php

return array (
  'show' => 'Show',
  'details' => 'Details',
  'name' => 'Name',
  'job_title' => 'Job title',
  'application_date' => 'Application date',
  'cover_letter' => 'Cover letter',
  'draft' => 'Draft',
  'pending' => 'Pending',
  'in_process' => 'In process',
  'accomplished' => 'Accomplished',
  'rejected' => 'Rejected',
  'refused' => 'Refused',
  'view_applicants' => 'View applicants',
  'status' => 'Status',
  'download_cv' => 'Download CV',
  'thank_you' => 'Thank you!',
  'applier_success' => 'Your application is submitted',
);
