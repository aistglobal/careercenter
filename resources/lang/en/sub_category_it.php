<?php

return array (
  'ASP' => 
  array (
    'NET_development' => 'ASP.NET development',
  ),
  'C/C++/C#_development' => 'C/C++/C# development',
  'PHP/MySQL_development' => 'PHP/MySQL development',
  'computer_graphics_and_animation' => 'Computer graphics and Animation',
  'general_pc_usage' => 'General PC usage',
  'it_management' => 'IT management',
  'java_development' => 'Java development',
  'perl_programming' => 'Perl programming',
  'software_development' => 'Software development',
  'software_engineering' => 'Software engineering',
  'system_administration' => 'System administration',
  'system_analysis' => 'System analysis',
  'web_design' => 'Web design',
  'web_site_moderator' => 'Website moderator',
  'wireless_engineering' => 'Wireless engineering',
);
