<?php

return array (
  'diplomacy' => 'Diplomacy',
  'all_fields' => 'All Fields',
  'advertising/pr' => 'Advertising/PR',
  'arts/entertainment' => 'Arts/Entertainment',
  'school_education' => 'School Education',
  'engineering/real_estate' => 'Engineering/Real Estate',
  'finance/insurance' => 'Finance/Insurance',
  'health_care/chemistry' => 'Health Care/Chemistry',
  'human_resources' => 'Human Resources',
  'it' => 'IT',
  'journalism/publishing' => 'Journalism/Publishing',
  'law/legal' => 'Law/Legal',
  'linguistics' => 'Linguistics',
  'nature/agriculture' => 'Nature/Agriculture',
  'tourism/transportation' => 'Tourism/Transportation',
  'marketing_sales' => 'Marketing / Sales',
  'other' => 'Other',
);
