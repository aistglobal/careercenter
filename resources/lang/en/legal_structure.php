<?php

return array (
  'local_ngo' => 'Local NGO',
  'llc' => 'LLC',
  'ltd' => 'LTD',
  'cjsc' => 'CJSC',
  'ojsc' => 'OJSC',
  'government' => 'Government',
  'private' => 'Private Enterpreneur',
  'fund' => 'Fund',
  'foundation' => 'Foundation',
  'charity' => 'Charity Foundation',
  'benevolent_union' => 'Benevolent Union',
  'general_benevolent_union' => 'General Benevolent Union',
  'humanitar_organization' => 'Humanitarian Organization',
  'other' => 'Other',
  'international_ngo' => 'International NGO',
);
