<?php

return array(
    'employment' => 'Employment',
    'educational' => 'Educational',
    'for_organizations' => 'Hiring',
    'other' => 'Other',
    'category' => 'Category',
    'parent_category' => 'Parent Category',
    'create' => 'Create',
    'save' => 'Save',
);
