<?php

return array (
  'cardiology' => 'Cardiology',
  'dentistry' => 'Dentistry',
  'doctor' => 'Doctor',
  'doctor_cosmetologist' => 'Doctor cosmetologist',
  'gynecology' => 'Gynecology',
  'lab_supervising' => 'Lab supervising',
  'medical_consultancy' => 'Medical consultancy',
  'nursing' => 'Nursing',
  'pediatrics' => 'Pediatrics',
  'pharmacy' => 'Pharmacy',
  'social_science' => 'Social science',
  'surgery' => 'Surgery',
  'therapeutics' => 'Therapeutics',
);
