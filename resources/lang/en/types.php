<?php

return array (
  'job_opp' => 'Job Opportunities',
  'volunteering' => 'Volunteering Opportunities',
  'internship' => 'Internships',
  'education' => 'Education opportunities',
  'training' => 'Trainings',
  'fellowship' => 'Fellowships',
  'scholarship' => 'Scholarships',
  'event' => 'Events',
  'news' => 'News',
  'publication' => 'Publication',
  'competition' => 'Competitions',
  'job opportunity' => 'Job Opportunity',
);
