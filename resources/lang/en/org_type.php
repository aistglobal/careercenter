<?php

return array (
  'library' => 'Library',
  'direct_employer' => 'Direct Employer',
  'staffing_agency' => 'Staffing Agency',
  'educational_institution' => 'Educational Institution',
  'other' => 'Other',
);
