<?php

return array (
  'job_opportunity' => 'Job opportunity',
  'organization' => 'Organization',
  'title' => 'Title',
  'location' => 'Location',
  'description' => 'Job Description',
  'responsibilities' => 'Responsibilities',
  'requirements' => 'Requirements',
  'app_procedures' => 'Application Procedures',
  'opening_date' => 'Opening Date',
  'deadline' => 'Deadline',
  'salary' => 'Remuneration/ Salary',
  'term' => 'Term',
  'open' => 'Open to/ Eligibility Criteria',
  'start_date' => 'Start Date',
  'duration' => 'Duration',
  'about_company' => 'About Company',
  'notes' => 'Additional notes',
  'choose_file' => 'Choose File',
  'file_size' => 'Permitted files: txt, pdf, jpg, gif, png, doc, xls, zip, docx, xlsx (Max: 3 MB)',
  'translate' => 'This Announcement will be translated',
  'submission' => 'Enable On-line applications Submission',
  'code' => 'Announcement Code',
  'menu' => 'Menu/List/Text',
  'educational_level' => 'Educational level',
  'author' => 'Author',
  'language' => 'Language',
  'pages' => 'Pages',
  'expiration_date' => 'Expiration Date',
  'post_organization' => 'Organization',
  'contact_person' => 'Contact Person',
  'contact_title' => 'Contact Title',
  'phone' => 'Phone',
  'email' => 'Email',
  'organization_notes' => 'Notes',
  'about' => 'About',
  'job' => 'Job',
  'company' => 'Company',
  'category' => 'Announcement Area',
  'edit_translate' => 'Edit translation',
  'news_type' => 'News Type',
  'education_type' => 'Education Type',
  'training_type' => 'Training Type',
  'fellowship_type' => 'Fellowship Type',
  'scholarship_type' => 'Scholarship Type',
  'event_type' => 'Event Type',
  'publication_type' => 'Publication Type',
  'competition_type' => 'Competition Type',
  'news_details' => 'News Details',
  'detail_description' => 'Detail Description',
  'audience' => 'Intended Audience',
  'required_qualifications' => 'Required Qualifications',
  'isbn' => 'Publication ID or ISBN',
  'job_category' => 'All fields',
  'job_types' => 'All Announcement Types',
  'job_title_keywords' => 'Announcement Title/ Keyword',
  'search_job' => 'Search',
  'apply_now' => 'Apply now',
  'share_this_page' => 'Share this page',
  'create-ru' => 'Create Russian',
  'create-hy' => 'Create Armenian',
  'create-en' => 'Create English',
  'edit-en' => 'Edit English version',
  'edit-ru' => 'Edit Russian version',
  'edit-hy' => 'Edit Armenian version',
  'sub_category' => 'Sub category',
  'edit' => 'Edit',
  'delete' => 'Delete',
  'copy' => 'Copy to New',
  'submit' => 'Submit',
  'location_city' => 'Location city',
  'location_country' => 'Location Country',
  'clone' => 'Copy to new',
  'attachments' => 'Attachments',
  'checked' => 'Left announcement in same place',
  'no_result' => 'No Result is found',
  'save' => 'Submit',
  'save_as_draft' => 'Save as draft',
  'your_resumes' => 'Your resumes',
  'upload_resume' => 'Upload resume',
  'cover_letter' => 'Cover letter',
  'select_organization' => 'Organization',
  'organization_name' => 'Organization Name',
  'preview' => 'Preview',
  'added_by' => 'Added by',
  'from' => 'From',
  'last_edited_by' => 'Last edited by',
  'publication_date' => 'Publication date',
  'cancel' => 'Cancel',
  'actions' => 'Actions',
  'application_procedures_default_text' => 'Please clearly mention in your application letter that you learned of this job opportunity through Career Center and mention the URL of its website - www.careercenter.am, Thanks.',
  'file' => 'File',
  'add_attachment' => 'Add attachment',
  'post' => 'Post',
  'post_and_email' => 'Post and email',
  'reject' => 'Reject',
  'expired' => 'Expired',
);
