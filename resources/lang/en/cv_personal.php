<?php

return array (
  'not_served_yet' => 'Not Served Yet',
  'single' => 'Single',
  'married' => 'Married',
  'separated' => 'Separated',
  'widow(er)' => 'Widow(er)',
  'divorced' => 'Divorced',
  'centimeters' => 'Centimeters',
  'meters' => 'Meters',
  'inches' => 'Inches',
  'feet' => 'Feet',
  'kilograms' => 'Kilograms',
  'pounds' => 'Pounds',
  'in_service' => 'In service',
  'served' => 'Served',
  'exempted' => 'Exempted',
  'no_intensions' => 'No intensions',
  'sister' => 'Sister',
  'brother' => 'Brother',
  'mother' => 'Mother',
  'father' => 'Father',
  'husband' => 'Husband',
  'wife' => 'Wife',
  'son' => 'Son',
  'daughter' => 'Daughter',
);
