<?php

return array (
  'title' => 'Title',
  'content' => 'Content',
  'written_by' => 'Written by',
  'dont_forget_to_share' => 'Don\'t forget to share this post',
  'related_articles' => 'Related articles',
  'read_more' => 'Read more',
  'originally_published' => 'Originally published',
  'updated' => 'Updated',
  'short_description' => 'Short Description',
  'meta_title' => 'Meta Title',
  'meta_description' => 'Meta Description',
  'meta_keywords' => 'Meta Keywords',
  'create' => 'Create',
  'save' => 'Save',
  'upload_image' => 'Upload Image',
);
