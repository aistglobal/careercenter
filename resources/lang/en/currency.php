<?php

return array (
  'amd' => 'AMD',
  'euro' => 'Euro',
  'rur' => 'RUB',
  'usd' => 'USD',
  'hourly' => 'Hourly',
  'weekly' => 'Weekly',
  'daily' => 'Daily',
  'monthly' => 'Monthly',
  'yearly' => 'Yearly',
);
