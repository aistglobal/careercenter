<?php

return array (
  'computer_center' => 'Computer center',
  'technical_center' => 'Technical center',
  'other' => 'Other',
  'no_certificate' => 'No certificate',
  'participation' => 'Participation',
  'accomplishment' => 'Accomplishment',
  'successful_accomplishment' => 'Successful accomplishment',
);
