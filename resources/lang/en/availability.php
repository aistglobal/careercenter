<?php

return array (
  'mon' => 'Monday',
  'tus' => 'Tuesday',
  'wed' => 'Wednesday',
  'thu' => 'Thursday',
  'fri' => 'Friday',
  'sat' => 'Saturday',
  'sun' => 'Sunday',
  'full_time' => 'Full time',
  'part_time' => 'Part time',
  'temporary' => 'Temporary',
  'permanent' => 'Permanent',
  'contract' => 'Contract',
);
