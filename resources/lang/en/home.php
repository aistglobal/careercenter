<?php

return array (
  'latest_articles' => 'Latest Articles',
  'read_more' => 'Read more',
  'what_people_say' => 'Testimonials',
  'job_description' => 'Find your perfect career  today. Whatever job you\'re looking for, you can find it on CareerCenter',
  'job_title_skill' => 'Job/Title',
  'search_job' => 'Search Job',
  'resume_description' => 'Don\'t have a resume? Build one with us!',
  'need_resume' => 'Need Resume?',
  'post_announcement' => 'Post an Announcement',
  'announcement_description' => 'Post jobs. Find employees. CareerCenter will help to find the right talent for your open positions - delivering a quicker, easier way to post a job and find qualified candidates',
  'create_now' => 'Create Now',
  'education' => 'Education',
  'education_description' => 'We offer a wide range of different courses',
  'job_for_everyone' => 'Job for Everyone',
  'english' => 'English',
  'russian' => 'Russian',
  'create_resume' => 'Create Resume',
);
