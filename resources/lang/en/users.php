<?php

return array (
  'username' => 'Username',
  'role' => 'Role',
  'user' => 'Individual',
  'admin' => 'Admin',
  'moderator' => 'Moderator',
  'organization' => 'Organization',
  'pro_organization' => 'Pro organization',
  'head_moderator' => 'Head moderator',
  'first_name' => 'First name',
  'last_name' => 'Last name',
  'email' => 'Email',
  'status' => 'Status',
  'profile_success_updated' => 'Profile successfully updated',
  'joined' => 'Joined',
  'full_name' => 'Full Name',
);
