<?php

return array (
  'username' => 'Username',
  'password' => 'Password',
  'confirm_pass' => 'Confirm Password',
  'user_category' => 'User category',
  'registration' => 'Registration',
  'agree' => 'Agree with',
  'terms' => 'Terms of Use',
  'next' => 'Next',
  'new_password' => 'New Password',
  'role' => 'Role',
);
