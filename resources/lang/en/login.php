<?php

return array (
  'username_or_email' => 'Username or Email',
  'password' => 'Password',
  'login' => 'Login',
  'remember' => 'Remember me',
  'register' => 'Register',
  'logout' => 'Logout',
  'my_profile' => 'My profile',
  'advanced' => 'Advanced',
  'we_send_yo_email' => 'Your password reset instructions have been emailed to you',
  'forgot_password' => 'Forgot your password?',
);
