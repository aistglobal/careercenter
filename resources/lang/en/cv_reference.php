<?php

return array (
  'my_colleague' => 'My colleague',
  'my_direct_supervisor' => 'My direct supervisor',
  'my_department_director' => 'My department director',
  'my_company_head' => 'My company head',
  'my_teacher/instructor' => 'My teacher/instructor',
  'hours' => 'Hours',
  'days' => 'Days',
  'weeks' => 'Weeks',
  'months' => 'Months',
  'years' => 'Years',
);
