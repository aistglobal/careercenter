<?php

return array (
  'make_inquiries' => 'Can a prospective employer make inquiries of your',
  'present_employer' => 'Present employer',
  'previous_employer' => 'Previous employer',
  'government' => 'Are you now, or have you ever been a Permanent Civil Servant in Your governments Employ? If yes, give full details',
  'arrested' => 'Have you ever been arrested, indicted, or summoned into court as a defendant in a criminal proceeding, or convicted, fined, or imprisoned for the violation of any law (excluding minor traffic violations)?',
  'arrested_details' => 'Details',
);
