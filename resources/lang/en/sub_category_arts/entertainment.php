<?php

return array (
  'acting' => 'Acting',
  'casting' => 'Casting',
  'cosmetology' => 'Cosmetology',
  'design' => 'Design',
  'fashion_design' => 'Fashion design',
  'hair_styling' => 'Hair styling',
  'interior_and_exterior_decoration' => 'Interior and Exterior decoration',
  'make-up' => 'Make-up',
  'manicure' => 'Manicure',
  'music' => 'Music',
  'operating' => 'Operating',
  'painting' => 'Painting',
  'production' => 'Production',
  'styling' => 'Styling',
);
