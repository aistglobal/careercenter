<?php

return array (
  'my_profile' => 'My Profile',
  'resumes' => 'Resumes',
  'recruitment' => 'Recruitment',
  'announcements' => 'Announcements',
  'post_announcement' => 'Post An Announcement',
  'blogs' => 'Articles',
  'reviews' => 'Reviews',
  'users' => 'Users',
  'resources' => 'Resources',
  'category' => 'Category',
  'pages' => 'Pages',
  'translations' => 'Translations',
  'subscribtions' => 'Subscriptions',
  'appliers' => 'Applicants',
  'resource_category' => 'Resource Category',
  'permissions' => 'Permissions',
);
