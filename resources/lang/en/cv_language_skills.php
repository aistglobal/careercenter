<?php

return array (
  'lsq_Fluent' => 'Fluent',
  'lsr_Good' => 'Good',
  'lsq_Fair' => 'Fair',
  'lsq_Poor' => 'Poor',
  '1-3_months' => '1-3 months',
  '4-6_months' => '4-6 months',
  '7-12_months' => '7-12 months',
  '1-2_years' => '1-2 years',
  '2-3_years' => '2-3 years',
  '3-5_years' => '3-5 years',
  'over_5_years' => 'Over 5 years',
  'over_10_years' => 'Over 10 years',
);
