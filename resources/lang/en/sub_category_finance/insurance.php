<?php

return array (
  'accounting' => 'Accounting',
  'administration' => 'Administration',
  'auditing' => 'Auditing',
  'business' => 'Business',
  'economics' => 'Economics',
  'finance' => 'Finance',
  'insurance_brokerage' => 'Insurance/brokerage',
  'management' => 'Management',
);
