<?php

return array (
  'agent' => 'Agent',
  'architecture' => 'Architecture',
  'construction' => 'Construction',
  'engineering' => 'Engineering',
  'manufacturing' => 'Manufacturing',
  'real_estate_brokerage' => 'Real estate/Brokerage',
);
