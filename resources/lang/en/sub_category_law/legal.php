<?php

return array (
  'advocacy' => 'Advocacy',
  'body_guard' => 'Body guard',
  'detective' => 'Detective',
  'investigation' => 'Investigation',
  'jurisprudence' => 'Jurisprudence',
  'law' => 'Law',
  'military_officer' => 'Military officer',
  'police_work' => 'Police work',
  'procurator' => 'Procurator',
  'security_service' => 'Security service',
);
