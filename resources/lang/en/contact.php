<?php

return array (
  'name' => 'Name',
  'email' => 'Email',
  'message' => 'Message',
  'send' => 'Send',
  'contact_us' => 'Contact Us',
  'description' => 'Have a question? Need Help? Feel free to contact us.',
  'thank_you' => 'Thank you!',
  'mail_success' => 'Your email was sent successfully',
  'telephone' => '+374 10 519 062',
  'mail' => 'mailbox@careerhouse.com',
  'website' => 'www careerhouse.com',
  'address' => '25 Abovyan Str.,Yerevan, 0009 Armenia',
);
