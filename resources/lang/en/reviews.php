<?php

return array(
    'name' => 'Name',
    'profession' => 'Profession',
    'review' => 'Testimonial',
    'rate' => 'Rate',
    'reviews' => 'Testimonials',
    'create' => 'Create',
    'save' => 'Save',
);
