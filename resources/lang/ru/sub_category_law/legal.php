<?php

return array (
  'jurisprudence' => 'Юриспруденция',
  'procurator' => 'Прокурор',
  'law' => 'Закон',
  'advocacy' => 'Адвокатура',
  'investigation' => 'Расследование',
  'police_work' => 'Полицейская работа',
  'detective' => 'Детектив',
  'military_officer' => 'Военнослужащий',
  'security_service' => 'Служба безопасности',
  'body_guard' => 'Телохранитель',
);
