<?php

return array (
  'it_management' => 'Управление ИТ',
  'software_engineering' => 'Разработка ПО',
  'software_development' => 'Perl разработка',
  'general_pc_usage' => 'Общее использование ПК',
  'wireless_engineering' => 'Беспроводная инженерия',
  'C/C++/C#_development' => 'Разработка C/C++/C#',
  'java_development' => 'Java  Разработка',
  'ASP' => 
  array (
    'NET_development' => 'Разработка ASP.NET',
  ),
  'PHP/MySQL_development' => 'Разработка PHP/MySQL',
  'perl_programming' => 'Perl разработка',
  'computer_graphics_and_animation' => 'Компьютерная графика и анимация',
  'system_administration' => 'Системное администрирование',
  'web_design' => 'Веб-дизайн',
  'web_site_moderator' => 'Модератор сайта',
  'system_analysis' => 'Анализ ситемы',
);
