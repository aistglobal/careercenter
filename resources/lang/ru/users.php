<?php

return array (
  'role' => 'Роль',
  'username' => 'Имя пользователя',
  'admin' => 'Админ',
  'head_moderator' => 'Главный модератор',
  'moderator' => 'Модератор',
  'organization' => 'Организация',
  'pro_organization' => 'Pro организация',
  'user' => 'Личный Аккаунт',
  'email' => 'Эл. почта',
  'first_name' => 'Имя',
  'last_name' => 'Фамилия',
  'status' => 'Статус',
  'joined' => 'Присоединился',
  'profile_success_updated' => 'Профиль был успешно обновлен',
  'full_name' => 'Имя Фамилия',
);
