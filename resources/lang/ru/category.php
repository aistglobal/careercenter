<?php

return array (
  'advertising/pr' => 'Реклама/PR',
  'arts/entertainment' => 'Искусство / Развлечения',
  'diplomacy' => 'Дипломатия',
  'engineering/real_estate' => 'Инженерное искусство / Недвижимость',
  'finance/insurance' => 'Финансы/ Страхование',
  'health_care/chemistry' => 'Здравоохранение/ Химия',
  'human_resources' => 'Отдел кадров',
  'it' => 'Информационные Технологии',
  'journalism/publishing' => 'Журналистика/ Издательство',
  'law/legal' => 'Закон/ Право',
  'linguistics' => 'Лингвистика',
  'nature/agriculture' => 'Природа/ Сельское хозяйство',
  'school_education' => 'Школьное Образование',
  'tourism/transportation' => 'Туризм/ Транспортировка',
  'all_fields' => 'Все области',
  'marketing_sales' => 'Маркетинг/Торговля',
  'other' => 'Другое',
);
