<?php

return array (
  'accomplishment' => 'Достижение',
  'computer_center' => 'Компьютерный центр',
  'no_certificate' => 'Сертификат',
  'other' => 'Другие',
  'participation' => 'Участие',
  'successful_accomplishment' => 'Успешное завершение',
  'technical_center' => 'Технический центр',
);
