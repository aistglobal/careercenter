<?php

return array (
  'active_url' => ':attribute недействителенный URL.',
  'after' => ':attribute должна быть дата после :date.',
  'alpha' => ':attribute может содержать только буквы.',
  'alpha_dash' => ':attribute может содержать только буквы, цифры, тире и штрихи.',
  'alpha_num' => ':attribute может содержать только буквы и цифры.',
  'confirmed' => ':attribute подтвержедие не совпадает',
  'date' => ':attribute не является действительной датой',
  'email' => ':attribute должен быть действительным адресом электронной почты',
  'file' => ':attribute должен быть файлом',
  'accepted' => ':attribute должен быть принят.',
  'after_or_equal' => ':attribute должен быть днем позже, либо равным.',
  'array' => ':attribute должен быть массивом.',
  'before' => ':attribute дложен быть датой до :date.',
  'before_or_equal' => ':attribute должен быть датой до, либо равной :date.',
  'between' => 
  array (
    'array' => ':attribute должен быть между :min и :max единиц.',
    'file' => ':attribute должен быть между :min и :max килобайт.',
    'numeric' => ':attribute должен быть между :min и :max.',
    'string' => ':attribute должен быть между :min и :max знаков.',
  ),
  'boolean' => ':attribute поле должно быть истинным или ложным.',
  'custom' => 
  array (
    'attribute-name' => 
    array (
      'rule-name' => 'Сгенерированое сообщение',
    ),
  ),
  'date_equals' => ':attribute должа быть дата равная :date.',
  'date_format' => ':attribute не соответствует формату :format.',
  'different' => ':attribute и :other должны быть разными.',
  'digits' => ':attribute должен быть :digits цифрами.',
  'digits_between' => ':attribute должен быть между :min и :max цифрами.',
  'dimensions' => ':attribute имеет неправильный рамер изображения.',
  'distinct' => ':attribute поле имеет повторяющиеся значения.',
  'ends_with' => ':attribute должен заканчиваться одними из following: :values',
  'exists' => 'Выбранный :attribute неправильный.',
  'filled' => ':attribute поле должно иметь значение.',
  'gt' => 
  array (
    'array' => ':attribute должно иметь более чем :value значений.',
    'file' => ':attribute должен быть больше чем :value килобайт.',
    'numeric' => ':attribute должен быть больше, чем :value.',
    'string' => ':attribute должен быть больше чем :value символов.',
  ),
  'gte' => 
  array (
    'array' => ':attribute должен иметь must have :value единиц или больше.',
    'file' => ':attribute должен буть больше либо равен :value килобайт.',
    'numeric' => ':attribute должен буть больше либо равен :value.',
    'string' => ':attribute должен буть больше либо равен :value символам.',
  ),
  'image' => ':attribute должен быть изображением.',
  'in' => 'Выбранный :attribute неправильный.',
  'in_array' => ':attribute поле не сушествует в :other.',
  'integer' => ':attribute должен быть целым числом.',
  'ip' => ':attribute должен быть действующим IP адресом.',
  'ipv4' => ':attribute должен быть действующим IPv4  адресом.',
  'ipv6' => ':attribute должен быть действующим IPv6   адресом.',
  'json' => ':attribute должен быть действующей JSON строкой.',
  'lt' => 
  array (
    'array' => ':attribute должен иметь меньше чем :value единиц.',
    'file' => ':attribute должен буть меньше :value килобайт.',
    'numeric' => ':attribute должен быть меньше :value.',
    'string' => ':attribute должен быть меньше :value символов.',
  ),
  'lte' => 
  array (
    'array' => ':attribute должен иметь не больше :value символов.',
    'file' => ':attribute должен быть меньше либо равным
 :value килобайт.',
    'numeric' => ':attribute должен быть меньше либо равным :value.',
    'string' => ':attribute должен быть меньше либо равным :value символам.',
  ),
  'max' => 
  array (
    'array' => ':attribute не должен превышать :max единиц.',
    'file' => ':attribute не может быть больше :max килобайт.',
    'numeric' => ':attribute не может быть больше :max.',
    'string' => ':attribute не может быть больше :max символов.',
  ),
  'mimes' => ':attribute должен быть файлом type: :values.',
  'mimetypes' => ':attribute должен быть файлом type: :values.',
  'min' => 
  array (
    'array' => ':attribute должен иметь  :min единиц.',
    'file' => ':attribute должен быть 
 :min килобайт.',
    'numeric' => ':attribute должен быть :min.',
    'string' => ':attribute должен быть  :min символов.',
  ),
  'not_in' => ':attribute не деиствительный.',
  'not_regex' => ':attribute формат не действительный.',
  'numeric' => ':attribute должен быть номером.',
  'present' => ':attribute поле должно присутствовать.',
  'regex' => ':attribute формат не действительный.',
  'required' => ':attribute поле обязательно.',
  'required_if' => ':attribute поле обязательно когда :other :value.',
  'required_unless' => ':attribute поле обязательно до тех пор :other в :values.',
  'required_with' => ':attribute поле обязательно когда
:values присутствуют.',
  'required_with_all' => ':attribute поле обязательно когда
 :values присутствуют.',
  'required_without' => ':attribute поле обязательно когда
:values не присутствуют.',
  'required_without_all' => ':attribute поле обязательно когда ни один из
:values присутствуют.',
  'same' => ':attribute и :other должны   совпадать.',
  'size' => 
  array (
    'array' => ':attribute должен содержать :size единиц.',
    'file' => ':attribute должен быть :size килобайт.',
    'numeric' => ':attribute должен быть :size.',
    'string' => ':attribute должен быть :size символов.',
  ),
  'starts_with' => ':attribute должен начинаться одним из  
following: :values',
  'string' => ':attribute должен быть строкой.',
  'timezone' => ':attribute должен быть действительной зоной.',
  'unique' => ':attribute 
уже занят.',
  'uploaded' => ':attribute загрузка не удалась.',
  'url' => ':attribute формат не действительный.',
  'uuid' => ':attribute должен быть действительным UUID.',
);
