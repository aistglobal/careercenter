<?php

return array (
  'job_opp' => 'Вакансии',
  'competition' => 'Конкурсы',
  'education' => 'Образовательные Возможности',
  'event' => 'События',
  'fellowship' => 'Образовательно-Трудовые Программы',
  'internship' => 'Стажировка',
  'news' => 'Новости',
  'publication' => 'Публикации',
  'scholarship' => 'Стипендии',
  'training' => 'Переподготовки',
  'volunteering' => 'Волонтерские Вакансии',
  'job opportunity' => 'Вакансии',
);
