<?php

return array (
  'agree' => 'Согласен(а)',
  'terms' => 'Условия пользования',
  'confirm_pass' => 'Подтвердить пароль',
  'next' => 'Далее',
  'password' => 'Пароль',
  'registration' => 'Регистрация',
  'user_category' => 'Категория пользователя',
  'username' => 'Имя пользователя',
  'new_password' => 'Новый пароль',
  'role' => 'Роль',
);
