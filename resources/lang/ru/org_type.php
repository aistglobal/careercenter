<?php

return array (
  'direct_employer' => 'Прямой работадатель',
  'staffing_agency' => 'Кадровое агетнство',
  'educational_institution' => 'Учебное учреждение',
  'library' => 'Библиотека',
  'other' => 'Другое',
);
