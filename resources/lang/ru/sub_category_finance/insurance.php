<?php

return array (
  'finance' => 'Финансы',
  'business' => 'Бизнес',
  'auditing' => 'Аудит',
  'economics' => 'Экономика',
  'accounting' => 'Бухгалтерия',
  'administration' => 'Администрация',
  'management' => 'Управление',
  'insurance_brokerage' => 'Страхование/Риэлторские услуги',
);
