<?php

return array (
  'pharmacy' => 'Фармацевтика',
  'medical_consultancy' => 'Медицинская  консультация',
  'lab_supervising' => 'Надзор за лабораторией',
  'social_science' => 'Социология',
  'surgery' => 'Хирургия',
  'dentistry' => 'Стоматология',
  'therapeutics' => 'Терапия',
  'doctor' => 'Доктор',
  'doctor_cosmetologist' => 'Доктор косметолог',
  'gynecology' => 'Гинекология',
  'pediatrics' => 'Педиатрия',
  'cardiology' => 'Кардиология',
  'nursing' => 'Ухода за больными',
);
