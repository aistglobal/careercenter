<?php

return array (
  'engineering' => 'Инженерия',
  'architecture' => 'Архитектура',
  'construction' => 'Строительство',
  'manufacturing' => 'Производство',
  'real_estate_brokerage' => 'Недвижимость/брокерство',
  'agent' => 'Агент',
);
