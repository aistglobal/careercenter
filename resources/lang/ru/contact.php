<?php

return array (
  'contact_us' => 'Контакты',
  'description' => 'Свяжитесь с нами',
  'email' => 'Эл. почта',
  'message' => 'Письмо',
  'name' => 'Имя',
  'send' => 'Отправить',
  'mail_success' => 'Ваше письмо было успешно отправлено',
  'thank_you' => 'Спасибо!',
  'mail' => 'mailbox@careerhouse.com',
  'telephone' => '+374 10 519 062',
  'website' => 'www careerhouse.com',
  'address' => 'Абовян 25, Армения, 0009, Ереван',
);
