<?php

return array (
  'announcements' => 'Объявления',
  'my_profile' => 'Мой Профиль',
  'post_announcement' => 'Разместить Вакансию',
  'recruitment' => 'Трудоустройство',
  'resumes' => 'Резюме',
  'blogs' => 'Статьи',
  'reviews' => 'Отзывы',
  'users' => 'Пользователи',
  'category' => 'Категория',
  'pages' => 'Страницы',
  'resources' => 'Ресурсы',
  'translations' => 'Переводы',
  'subscribtions' => 'Подписка',
  'appliers' => 'Заявители',
  'resource_category' => 'Категории ресурсов',
);
