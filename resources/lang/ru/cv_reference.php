<?php

return array (
  'days' => 'Дни',
  'hours' => 'Часы',
  'months' => 'Месяцы',
  'my_colleague' => 'Мой коллега',
  'my_company_head' => 'Руководитель компании',
  'my_department_director' => 'Директор моего департамента',
  'my_direct_supervisor' => 'Непосредственный начальник',
  'my_teacher/instructor' => 'Мой учитель/инструктор',
  'weeks' => 'Недели',
  'years' => 'Годы',
);
