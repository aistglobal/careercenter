<?php

return array (
  'title' => 'Название',
  'content' => 'Контент',
  'dont_forget_to_share' => 'Поделиться',
  'originally_published' => 'Первоначально опубликованно',
  'read_more' => 'Читать',
  'related_articles' => 'Похожие статьи',
  'short_description' => 'Краткое описание',
  'updated' => 'Обнавленно',
  'written_by' => 'Автор',
  'meta_description' => 'Мета Описание',
  'meta_keywords' => 'Мета ключевые слова',
  'meta_title' => 'Мета Заголовок',
  'create' => 'Создать',
  'save' => 'Сохранить',
  'upload_image' => 'Upload Image',
);
