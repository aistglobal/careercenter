<?php

return array (
  'password' => 'Пароль',
  'register' => 'Регистрация',
  'login' => 'Вход',
  'logout' => 'Выход',
  'my_profile' => 'Мой профиль',
  'remember' => 'Запомнить',
  'username_or_email' => 'Имя пользователя или Эл. почта',
  'advanced' => 'Расширенный',
  'we_send_yo_email' => 'Соответствующие инструкции были высланы по эл. почте',
  'forgot_password' => 'Забыли пароль?',
);
