<?php

return array (
  'latest_articles' => 'Последние Статьи',
  'read_more' => 'Читать',
  'what_people_say' => 'Отзывы',
  'english' => 'Английский',
  'russian' => 'Русский',
  'announcement_description' => 'Размещайте вакансии! Мы поможем Вам найти талантливых работников!',
  'create_now' => 'Создать сейчас',
  'education' => 'Образование',
  'education_description' => 'Мы предлагаем разнообразные курсы',
  'job_description' => 'Мы поможем Вам найти работу мечты!',
  'job_for_everyone' => 'Работа Для Каждого',
  'job_title_skill' => 'Профессия/Должность/Название',
  'need_resume' => 'Нужно Резюме?',
  'post_announcement' => 'Разместить Вакансию',
  'resume_description' => 'Простые шаги по созданию резюме',
  'search_job' => 'Искать работу',
  'create_resume' => 'Создать Резюме',
);
