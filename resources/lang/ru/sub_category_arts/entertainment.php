<?php

return array (
  'styling' => 'Стайлинг',
  'interior_and_exterior_decoration' => 'Украшение интерьера и экстерьера',
  'fashion_design' => 'Дизайн одежды',
  'design' => 'Дизайн',
  'painting' => 'Живопись',
  'music' => 'Музыка',
  'acting' => 'Актерство',
  'make-up' => 'Макияж',
  'manicure' => 'Маникюр',
  'hair_styling' => 'Стилист парикмахер',
  'production' => 'Продакшн',
  'casting' => 'Кастинг',
  'operating' => 'Операторство',
  'cosmetology' => 'Косметология',
);
