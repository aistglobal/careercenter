<?php

return array (
  'journalism' => 'Журналистика',
  'TV_Reporting' => 'Телевизионный репортаж',
  'correspondence' => 'Корреспонденция',
  'editing' => 'Редактирование/монтаж',
  'photography' => 'Фотография',
  'graphics_editing' => 'Графическое редактирование',
);
