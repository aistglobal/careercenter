<?php

return array (
  'accomplished' => 'Достигнутый',
  'application_date' => 'Дата заявки',
  'cover_letter' => 'Сопроводительное письмо',
  'details' => 'Детали',
  'draft' => 'Черновик',
  'in_process' => 'В процессе',
  'job_title' => 'Должность',
  'name' => 'Имя',
  'pending' => 'На рассмотрении',
  'refused' => 'Отказано',
  'rejected' => 'Отклонено',
  'show' => 'Показать',
  'applier_success' => 'Ваша заявка была подана',
  'download_cv' => 'Загрузить резюме',
  'status' => 'Статус',
  'thank_you' => 'Спасибо!',
  'view_applicants' => 'Рассматреть кандидатов',
);
