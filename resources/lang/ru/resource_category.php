<?php

return array(
    'educational' => 'Образовательный',
    'employment' => 'Трудоустройство',
    'for_organizations' => 'Найм на работу',
    'other' => 'Другое',
    'category' => 'Категория',
    'create' => 'Создать',
    'save' => 'Сохранить',
);
