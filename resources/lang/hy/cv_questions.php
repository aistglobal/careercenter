<?php

return array (
  'present_employer' => 'Ներկա գործատու',
  'make_inquiries' => 'Հավանական գործատուն կարո՞ղ է Ձեզ հարցեր տալ',
  'previous_employer' => 'Նախկին գործատու',
  'arrested_details' => 'Մանրամասներ',
  'government' => 'Այժմ կամ երբևէ եղե՞լ եք կառավարության քաղաքացիական ծառայող։ Եթե այո, մանրամասնե՛ք',
  'arrested' => 'Դուք երբեւէ ձերբակալվել, մեղադրվել կամ կանչվե՞լ եք դատարան որպես քրեական դատավարության մեղադրյալ կամ դատապարտվել, տուգանվել, ազատազրկվել որևէ օրենքի խախտման  համար (բացառությամբ ճանապարհային երթևեկության աննշան խախտումների)',
);
