<?php

return array (
  'acting' => 'Դերասանական արվեստ',
  'casting' => 'Քասթինգ',
  'cosmetology' => 'Կոսմետոլոգիա',
  'design' => 'Դիզայն',
  'fashion_design' => 'Նորաձեւության դիզայն',
  'interior_and_exterior_decoration' => 'Ինտերիերի եւ էքստերիերի ձեւավորում',
  'hair_styling' => 'Մազերի հարդարում',
  'manicure' => 'Մատնահարդարում',
  'make-up' => 'Դիմահարդարում',
  'music' => 'Երաժշտություն',
  'operating' => 'Օպերատրություն',
  'painting' => 'Նկարչություն',
  'production' => 'Փրոդաքշն',
  'styling' => 'Մոդելավորում',
);
