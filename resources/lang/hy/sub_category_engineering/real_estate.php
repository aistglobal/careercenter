<?php

return array (
  'agent' => 'Գործակալ',
  'architecture' => 'Ճարտարապետություն',
  'construction' => 'Շինարարություն',
  'engineering' => 'Ճարտարագիտություն',
  'manufacturing' => 'Արտադրություն',
  'real_estate_brokerage' => 'Անշարժ գույքի/բրոկերային ծառայություններ',
);
