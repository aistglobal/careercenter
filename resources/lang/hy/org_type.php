<?php

return array (
  'library' => 'Գրադարան',
  'direct_employer' => 'Գործատու',
  'educational_institution' => 'Կրթական հաստատություն',
  'other' => 'Այլ',
  'staffing_agency' => 'Աշխատանքի տեղավորման գործակալություն',
);
