<?php

return array (
  'active_url' => ':attribute հղումը վավեր չէ.',
  'after' => ':attribute պետք լինի ամսաթիվ :date հետո.',
  'alpha' => ':attribute կարող է պարունակել միայն տառեր',
  'alpha_dash' => ':attribute կարող է պարունակել միայն տառեր, թվեր, գծիկներ',
  'alpha_num' => ':attribute կարող է պարունակել միայն տառեր և թվեր',
  'confirmed' => ':attribute հաստատումը չի համապատասխանում',
  'date' => ':attribute վավեր ամսաթիվ չէ',
  'email' => ':attribute պետք է լինի վավեր էլ․ հասցե',
  'file' => ':attribute պետք է լինի ֆայլ․',
  'numeric' => ':attribute պետք է լինի թիվ.',
  'string' => ':attribute պետք է լինի տող.',
  'url' => ':attribute ձևաչափը անթույլատրելի է.',
  'required' => ':attribute դաշտը պարտադիր է.',
  'after_or_equal' => ':attribute ամսաթիվը պետք է լինի :date կամ դրանից հետո․',
  'accepted' => ':attribute պետք է ընդունվի․',
  'before' => ':attribute ամսաթիվը պետք է լինի :date առաջ.',
  'array' => ':attribute պետք է լինի ցուցակ.',
  'before_or_equal' => ':attribute պետք է լինի :date կամ դրանից առաջ.',
  'between' => 
  array (
    'array' => ':attribute պետք է պարունակի միավորներ :min և :max միջև.',
    'file' => ':attribute պետք է լինի :min և :max կիլոբայթի սահմաններում.',
    'numeric' => ':attribute պետք է լինի :min և :max միջև.',
    'string' => ':attribute պետք է պարունակի նիշեր :min և :max սահմաններում.',
  ),
  'boolean' => ':attribute դաշտը պետք է լինի ճիշտ կամ սխալ.',
  'custom' => 
  array (
    'attribute-name' => 
    array (
      'rule-name' => 'Անհատական հաղորդագրություն',
    ),
  ),
  'date_equals' => ':attribute պետք է լինի :date ամսաթիվ.',
  'date_format' => ':attribute չի համապատասխանում :format ֆորմատին.',
  'different' => ':attribute և :other պետք է տարբեր լինեն.',
  'digits' => ':attribute պետք է լինի :digits թվանշան.',
  'digits_between' => ':attribute պետք է լինի :min և :max թվանշանների սահմաններում.',
  'dimensions' => ':attribute ունի նկարի անթույլատրելի չափեր.',
  'distinct' => ':attribute դաշտը պարունակում է կրկնվող հատված.',
  'ends_with' => ':attribute պետք է ավարտվի following: :values արժեքներից որևէ մեկով',
  'exists' => 'Ընտրված :attribute անթույլատրելի է.',
  'filled' => ':attribute դաշտը պետք է արժեք ունենա․',
  'gt' => 
  array (
    'array' => ':attribute պետք է պարունակի ավելի շատ միավորներ քան :value.',
    'file' => ':attribute պետք է լինի ավելի մեծ չափի քան :value կիլոբայթը.',
    'numeric' => ':attribute պետք է լինի ավելի մեծ քան :value.',
    'string' => ':attribute պետք է պարունակի ավելի շատ :value նիշ.',
  ),
  'gte' => 
  array (
    'array' => ':attribute պետք է պարունակի :value կամ ավելի շատ միավոր.',
    'file' => ':attribute պետք է պարունակի :value կամ ավելի շատ կիլոբայթեր.',
    'numeric' => ':attribute պետք է լինի :value կամ ավելի մեծ.',
    'string' => ':attribute պետք է լինի :value նիշ կամ ավելի շատ.',
  ),
  'image' => ':attribute պետք է լինի նկար.',
  'in' => 'Ընտրված :attribute անթույլատրելի է.',
  'in_array' => ':attribute դաշտը չկա :other.',
  'integer' => ':attribute պետք է լինի ամբողջ թիվ.',
  'ip' => ':attribute պետք է լինի վավեր IP հասցե.',
  'ipv4' => ':attribute պետք է լինի վավեր IPv4 հասցե.',
  'ipv6' => ':attribute պետք է լինի վավեր IPv6 հասցե.',
  'json' => ':attribute պետք է լինի վավեր JSON տող.',
  'lt' => 
  array (
    'array' => ':attribute պետք է պարունակի ավելի քիչ միավոր քան :value.',
    'file' => ':attribute պետք է լինի ավելի քիչ քան :value կիլոբայթ.',
    'numeric' => ':attribute պետք է լինի ավելի քիչ քան :value.',
    'string' => ':attribute պետք է պարունակի ավելի քիչ նիշ քան :value.',
  ),
  'lte' => 
  array (
    'array' => ':attribute չպետք է պարունակի ավելի շատ միավոր քան :value.',
    'file' => ':attribute պետք է պարունակի :value կիլոբայթ կամ ավելի քիչ.',
    'numeric' => ':attribute պետք է լինի :value կամ ավելի քիչ.',
    'string' => ':attribute պետք է պարունակի :value կամ ավելի քիչ նիշ.',
  ),
  'max' => 
  array (
    'array' => ':attribute չպետք է պարունակի ավելի քան  :max միավոր.',
    'file' => ':attribute չպետք է պարունակի ավելի քան :max կիլոբայթ.',
    'numeric' => ':attribute չպետք է լինի ավելի մեծ քան :max.',
    'string' => ':attribute չպետք է պարունակի ավելի շատ քան :max նիշ.',
  ),
  'mimes' => ':attribute պետք է լինի type: :values ֆայլ.',
  'mimetypes' => ':attribute պետք է լինի type: :values ֆայլ.',
  'min' => 
  array (
    'array' => ':attribute պետք է պարունակի առնվազն :min միավոր.',
    'file' => ':attribute պետք է լինի առնվազն :min կիլոբայթ.',
    'numeric' => ':attribute պետք է լինի առնվազն :min.',
    'string' => ':attribute պետք է պարունակի առնվազն :min նիշ.',
  ),
  'not_in' => 'Ընտրված :attribute անթույլատրելի է.',
  'not_regex' => ':attribute ֆորմատն անթույլատրելի է․',
  'present' => ':attribute դաշտը պետք է լինի.',
  'regex' => ':attribute ֆորմատն անթույլատրելի է․',
  'required_if' => ':attribute դաշտը պարտադիր է, երբ :other :value է.',
  'required_unless' => ':attribute դաշտը պարտադիր է, քանի դեռ :other :values մեջ է.',
  'required_with' => ':attribute դաշտը պարտադիր է, երբ :values առկա է.',
  'required_with_all' => ':attribute դաշտը պարտադիր է, երբ :values առկա են.',
  'required_without' => ':attribute դաշտը պարտադիր է, երբ :values առկա չէ.',
  'required_without_all' => ':attribute դաշտը պարտադիր է, երբ ոչ մի :values առկա չեն.',
  'same' => ':attribute և :other պետք է համապատասխանեն.',
  'size' => 
  array (
    'array' => ':attribute պետք է պարունակի :size միավորներ.',
    'file' => ':attribute պետք է լինի :size կիլոբայթ.',
    'numeric' => ':attribute պետք է լինի :size.',
    'string' => ':attribute պետք է լինի :size նիշ.',
  ),
  'starts_with' => ':attribute պետք է սկսվի following: :values արժեքներից որևէ մեկով',
  'timezone' => ':attribute պետք է լինի թույլատրելի գոտի.',
  'unique' => ':attribute արդեն օգտագորրծվել է.',
  'uploaded' => ':attribute վերբեռնումը ձախողվեց.',
  'uuid' => ':attribute պետք է լինի վավեր UUID.',
);
