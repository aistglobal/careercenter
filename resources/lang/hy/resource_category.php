<?php

return array(
    'educational' => 'Կրթական',
    'employment' => 'Աշխատանք',
    'for_organizations' => 'Աշխատանքի ընդունում',
    'other' => 'Այլ',
    'category' => 'Կատեգորիա',
    'create' => 'Ստեղծել',
    'save' => 'Պահպանել',
);
