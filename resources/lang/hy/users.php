<?php

return array (
  'role' => 'Դեր',
  'username' => 'Օգտանուն',
  'admin' => 'Ադմին',
  'head_moderator' => 'Ղեկավար մոդերատոր',
  'moderator' => 'Մոդերատոր',
  'organization' => 'Կազմակերպություն',
  'pro_organization' => 'Pro կազմակերպություն',
  'user' => 'Անհատ օգտվող',
  'email' => 'Էլ. փոստ',
  'first_name' => 'Անուն',
  'last_name' => 'Ազգանուն',
  'status' => 'Կարգավիճակ',
  'profile_success_updated' => 'Պրոֆիլը հաջողությամբ թարմացվել է',
  'joined' => 'Միացել է',
  'full_name' => 'Անուն Ազգանուն',
);
