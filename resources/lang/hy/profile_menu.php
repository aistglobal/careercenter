<?php

return array (
  'announcements' => 'Հայտարարություններ',
  'my_profile' => 'Իմ Էջը',
  'post_announcement' => 'Տեղադրել Հայտարարություն',
  'resumes' => 'Ռեզյումե',
  'recruitment' => 'Աշխատանքի Ընդունում',
  'blogs' => 'Հոդվածներ',
  'reviews' => 'Կարծիքներ',
  'users' => 'Օգտատերեր',
  'category' => 'Կատեգորիա',
  'pages' => 'Էջեր',
  'resources' => 'Ռեսուրսներ',
  'translations' => 'Թարգմանություններ',
  'subscribtions' => 'Բաժանորդագրություն',
  'appliers' => 'Դիմորդներ',
  'resource_category' => 'Ռեսուրսների կատեգորիա',
);
