<?php

return array (
  'C/C++/C#_development' => 'C/C++/C# ծրագրավորում',
  'ASP' => 
  array (
    'NET_development' => 'ASP.NET ծրագրավորում',
  ),
  'PHP/MySQL_development' => 'PHP/MySQL ծրագրավորում',
  'java_development' => 'Java ծրագրավորում',
  'computer_graphics_and_animation' => 'Համակարգչային գրաֆիկա և անիմացիա',
  'perl_programming' => 'Perl ծրագրավորում',
  'general_pc_usage' => 'Համակարգչի ընդհանուր օգտագործում',
  'it_management' => 'ՏՏ մենեջմենթ',
  'system_administration' => 'Ցանցային ադմինիստրացիա',
  'software_development' => 'Ծրագրային ապահովում',
  'system_analysis' => 'Համակարգային վերլուծություն',
  'software_engineering' => 'Ծրագրային ապահովման ճարտարագիտություն',
  'web_design' => 'Վեբ դիզայն',
  'web_site_moderator' => 'Կայքի մոդերատոր',
  'wireless_engineering' => 'Անլար սարքավորումներ',
);
