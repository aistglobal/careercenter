<?php

return array (
  'cardiology' => 'Սրտաբանություն',
  'dentistry' => 'Ստոմատոլոգիա',
  'doctor' => 'Բժիշկ',
  'doctor_cosmetologist' => 'Բժիշկ կոսմետոլոգ',
  'gynecology' => 'Գինեկոլոգիա',
  'medical_consultancy' => 'Բժշկական խորհրդատվություն',
  'lab_supervising' => 'Լաբորատոր հսկողություն',
  'pediatrics' => 'Մանկաբուժություն',
  'nursing' => 'Խնամք',
  'social_science' => 'Հասարակական գիտություններ',
  'surgery' => 'Վիրաբուժություն',
  'therapeutics' => 'Թերապիա',
  'pharmacy' => 'Դեղագործություն',
);
