<?php

return array (
  'announcements' => 'Հայտարարություններ',
  'blog' => 'Բլոգ',
  'post_announcement' => 'Տեղադրել Հայտարարություն',
  'resources' => 'Միջոցներ',
  'resume_builder' => 'Ստեղծի՛ր Ռեզյումե',
  'find_jobs' => 'Գտնել Աշխատանք',
  'for_employers' => 'Գործատուների Համար',
  'for_job_seekers' => 'Գործ Փնտրողների Համար',
  'help' => 'Օգնություն',
  'helpful_resources' => 'Օգտակար Հղումներ',
  'monthly_visits' => 'Ամսեկան այցելություն',
  'post_jobs' => 'Տեղադրել Աշխատանք',
  'pricing' => 'Գնացուցակ',
  'privacy_policy' => 'Գաղտնիության Քաղաքականություն',
  'salary' => 'Աշխատավարձ',
  'search_resumes' => 'Փնտրել Ռեզյումե',
  'stay_connected' => 'Մնալ Կապի Մեջ',
  'subscribe' => 'Բաժանորդագրվել',
  'terms_of_conditions' => 'Կայքից Օգտվելու Կանոնները',
  'upload_resume' => 'Ներբեռնել Ռեզյումե',
  'contact_us' => 'Կապ',
  'about' => 'Մեր Մասին',
  'all' => 'Առկա',
  'today' => 'Առկա Այսօր',
  'my-all' => 'Իմ ամբողջը',
  'testimonial' => 'Կարծիքներ',
  'all-draft' => 'Սևագիր',
  'my-draft' => 'Իմ  Սևագիր',
  'all-expired' => 'Ժամկետանց',
  'my-expired' => 'Իմ Ժամկետանց',
  'all-posted' => 'Հրապարակված',
  'my-posted' => 'Իմ Հրապարակված',
  'all-pending' => 'Ընթացիկ',
  'my-pending' => 'Իմ Ընթացիկ',
  'all-rejected' => 'Մերժված',
  'resource_educational' => 'Կրթական',
  'resource_other' => 'Այլ',
  'resource_employment' => 'Աշխատանք',
  'resource_for_organizations' => 'Աշխատանքի ընդունում',
    'posting_rules' => 'Փակցնելու կանոնները',
  'apply_job' => 'Դիմել աշխատանքի համար',
  'my-rejected' => 'Իմ մերժված դիմումները',
);
