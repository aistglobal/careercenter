<?php

return array (
  'benevolent_union' => 'Բարեգործական միություն',
  'charity' => 'Բարեգործական հիմնադրամ',
  'cjsc' => 'ՓԲԸ',
  'foundation' => 'Հիմնադրամ',
  'fund' => 'Ֆոնդ',
  'general_benevolent_union' => 'Ընդհանուր բարեգործական միություն',
  'government' => 'Կառավարություն',
  'humanitar_organization' => 'Մարդասիրական կազմակերպություն',
  'international_ngo' => 'Միջազգային ՀԿ',
  'llc' => 'ՍՊԸ',
  'local_ngo' => 'Տեղական ՀԿ',
  'ltd' => 'ՍՊԸ',
  'ojsc' => 'ԲԲԸ',
  'other' => 'Այլ',
  'private' => 'Անհատական գործունեություն',
);
