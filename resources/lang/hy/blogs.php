<?php

return array (
  'title' => 'Վերնագիր',
  'content' => 'Բովանդակություն',
  'dont_forget_to_share' => 'Կիսվել',
  'originally_published' => 'Առաջին անգամ հրապարակված',
  'read_more' => 'Կարդալ ավելին',
  'related_articles' => 'Նմանատիպ հրապարակումներ',
  'short_description' => 'Կարճ նկարագիր',
  'updated' => 'Թարմացված է',
  'written_by' => 'Հեղինակ',
  'meta_description' => 'Մետա նկարագրություն',
  'meta_keywords' => 'Մետա հիմնաբառեր',
  'meta_title' => 'Մետա Վերնագիր',
  'create' => 'Ստեղծել',
  'save' => 'Պահպանել',
  'upload_image' => 'Upload Image',
);
