<?php

return array (
  'computer_center' => 'Համակարգչային կենտրոն',
  'no_certificate' => 'Վկայական չկա',
  'accomplishment' => 'Ձեռքբերում',
  'other' => 'Այլ',
  'participation' => 'Մասնակցություն',
  'technical_center' => 'Տեխնիկական կենտրոն',
  'successful_accomplishment' => 'Հաջողակ ձեռքբերում',
);
