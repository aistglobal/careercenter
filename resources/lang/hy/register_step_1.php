<?php

return array (
  'username' => 'Օգտվողի անունը',
  'password' => 'Ծածկագիր',
  'confirm_pass' => 'Հաստատել Ծածկագիրը',
  'user_category' => 'Օգտվողի անվանակարգը',
  'registration' => 'Գրանցում',
  'agree' => 'Համաձայն եմ',
  'terms' => 'Օգտագործման պայմաններին',
  'next' => 'Հաջորդը',
  'new_password' => 'Նոր ծածկագիր',
  'role' => 'Դեր',
);
