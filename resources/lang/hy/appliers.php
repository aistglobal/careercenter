<?php

return array (
  'accomplished' => 'Ավարտած',
  'details' => 'Մանրամասներ',
  'application_date' => 'Հայտի ներկայացման ամսաթիվ',
  'cover_letter' => 'Ուղեկցող նամակ',
  'draft' => 'Սևագիր',
  'in_process' => 'Ընթացքի մեջ',
  'job_title' => 'Հաստիքի անուն',
  'pending' => 'Սպասման մեջ',
  'name' => 'Անուն',
  'refused' => 'Մերժված',
  'rejected' => 'Հրաժարված',
  'show' => 'Ցուցադրել',
  'download_cv' => 'Ներբեռնել ռեզյումե',
  'applier_success' => 'Ձեր հայտը ներկայացվել է',
  'thank_you' => 'Շնորհակալություն',
  'view_applicants' => 'Դիտել դիմորդներին',
  'status' => 'Կարգավիճակ',
);
