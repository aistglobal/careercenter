<?php

return array (
  'contact_us' => 'Կապ',
  'description' => 'Կապնվեք մեզ հետ',
  'email' => 'Էլ. հասցե',
  'message' => 'Նամակ',
  'name' => 'Անուն',
  'send' => 'Ուղարկել',
  'mail_success' => 'Ձեր էլ․ նամակը հաջողությամբ ուղարկվել է',
  'thank_you' => 'Շնորհակալություն',
  'mail' => 'mailbox@careerhouse.com',
  'telephone' => '+374 10 519 062',
  'website' => 'www careerhouse.com',
  'address' => 'Աբովյան փող., 25 շենք, Հայաստան, 0009, Երևան',
);
