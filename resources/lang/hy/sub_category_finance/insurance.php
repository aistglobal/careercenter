<?php

return array (
  'accounting' => 'Հաշվապահություն',
  'administration' => 'Կառավարում',
  'auditing' => 'Աուդիտ',
  'business' => 'Բիզնես',
  'economics' => 'Տնտեսություն',
  'finance' => 'Ֆինանսներ',
  'insurance_brokerage' => 'Ապահովագրություն/բրոկերային ծառայություններ',
  'management' => 'Մենեջմենթ',
);
