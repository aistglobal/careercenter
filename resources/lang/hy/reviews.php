<?php

return array(
    'name' => 'Անուն',
    'profession' => 'Մասնագիտություն',
    'rate' => 'Գնահատական',
    'review' => 'Կարծիք',
    'reviews' => 'Կարծիքներ',
    'create' => 'Ստեղծել',
    'save' => 'Պահպանել',
);
