<?php

return array (
  'advertising/pr' => 'Գովազդ/Արտաքին կապեր',
  'arts/entertainment' => 'Արվեստ / Շոու',
  'diplomacy' => 'Դիվանագիտություն',
  'engineering/real_estate' => 'Ճարտարագիտություն / Անշարժ գույք',
  'finance/insurance' => 'Ֆինանսներ / Ապահովագրություն',
  'health_care/chemistry' => 'Առողջապահություն / Քիմիա',
  'human_resources' => 'Մարդկային Ռեսուրսներ',
  'it' => 'Տեղեկատվական Տեխնոլոգիաներ',
  'journalism/publishing' => 'Լրագրություն / Հրատարակչություն',
  'law/legal' => 'Օրենք / Իրավաբանություն',
  'linguistics' => 'Լեզվաբանություն',
  'nature/agriculture' => 'Բնություն / Գյուղատնտեսություն',
  'tourism/transportation' => 'Տուրիզմ / Փոխադրումներ',
  'school_education' => 'Դպրոցական կրթություն',
  'all_fields' => 'Բոլոր ոլորտները',
  'marketing_sales' => 'Մարքեթինգ/Վաճառք',
  'other' => 'Այլ',
);
