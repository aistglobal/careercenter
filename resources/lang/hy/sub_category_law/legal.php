<?php

return array (
  'advocacy' => 'Փաստաբանություն',
  'body_guard' => 'Թիկնապահ',
  'detective' => 'Դետեկտիվ',
  'investigation' => 'Հետաքննություն',
  'jurisprudence' => 'Իրավաբանություն',
  'law' => 'Օրենք',
  'police_work' => 'Ոստիկանություն',
  'military_officer' => 'Զինվորական սպա',
  'procurator' => 'Դատախազ',
  'security_service' => 'Անվտանգության ծառայություն',
);
