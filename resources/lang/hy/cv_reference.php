<?php

return array (
  'days' => 'Օրեր',
  'hours' => 'Ժամեր',
  'months' => 'Ամիսներ',
  'my_colleague' => 'Իմ գործընկեր',
  'my_company_head' => 'Իմ ընկերությունը ղեկավար',
  'my_department_director' => 'Իմ բաժնի ղեկավար',
  'my_direct_supervisor' => 'Իմ անմիջական ղեկավար',
  'my_teacher/instructor' => 'Իմ ուսուցիչ',
  'weeks' => 'Շաբաթներ',
  'years' => 'Տարիներ',
);
