<?php

return array (
  'username_or_email' => 'Օգտվողի անունը կամ էլ-հասցեն',
  'password' => 'Գաղտնաբառ',
  'login' => 'Մուտք',
  'remember' => 'Հիշել',
  'register' => 'Գրանցում',
  'logout' => 'Ելք',
  'my_profile' => 'Իմ էջը',
  'advanced' => 'Ընդլայնված',
  'we_send_yo_email' => 'Գաղտնաբառի վերականգնման ցուցումներն ուղարկվել են Ձեր էլ․ հասցեին',
  'forgot_password' => 'Մոռացե՞լ եք գաղտնաբառը',
);
