<?php

return array (
  '1-2_years' => '1-2 տարի',
  '1-3_months' => '1-3 ամիս',
  '2-3_years' => '2-3 տարի',
  '3-5_years' => '3-5 տար',
  '4-6_months' => '4-6 ամիս',
  '7-12_months' => '7-12 ամիս',
  'lsq_Fair' => 'Միջին',
  'lsq_Fluent' => 'Վարժ',
  'lsq_Poor' => 'Վատ',
  'lsr_Good' => 'Լավ',
  'over_10_years' => 'Ավելի քան 10 տարի',
  'over_5_years' => 'Ավելի քան 5 տարի',
);
