<?php

return array (
  'amd' => 'Դրամ',
  'daily' => 'Օրեկան',
  'euro' => 'Եվրո',
  'hourly' => 'Ժամյա',
  'monthly' => 'Ամսեկան',
  'rur' => 'Ռուբլի',
  'usd' => 'Դոլար',
  'weekly' => 'Շաբաթական',
  'yearly' => 'Տարեկան',
);
