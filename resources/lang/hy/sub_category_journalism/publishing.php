<?php

return array (
  'TV_Reporting' => 'Հեռուստատեսային ռեպորտաժներ',
  'correspondence' => 'Խմբագրում',
  'editing' => 'Խմբագրություն/մոնտաժ',
  'journalism' => 'Լրագրություն',
  'photography' => 'Լուսանկարչություն',
  'graphics_editing' => 'Գրաֆիկայի խմբագրում',
);
