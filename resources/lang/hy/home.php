<?php

return array (
  'latest_articles' => 'Վերջին Հոդվածներ',
  'read_more' => 'Կարդալ ավելին',
  'what_people_say' => 'Կարծիքներ',
  'announcement_description' => 'Մենք կօգնենք Ձեզ գտնել կատարյալ թեկնածուներ',
  'create_now' => 'Ստեղծել հիմա',
  'education' => 'Ուսում',
  'education_description' => 'Մենք առաջարկում ենք  բազմատեսակ դասընթացներ',
  'job_description' => 'Գտեք Ձեր կատարյալ աշխատանքն այսօր',
  'job_for_everyone' => 'Աշխատանք Բոլորի Համար',
  'job_title_skill' => 'Աշխատանք / Վերնագիր',
  'need_resume' => 'Հարկավոր է Ռեզյումե',
  'post_announcement' => 'Փակցնել Հայտարարություն',
  'resume_description' => 'Պարզ քայլեր ռեզյումե ստեղծելու համար',
  'search_job' => 'Որոնել աշխատանք',
  'english' => 'Անգլերեն',
  'russian' => 'Ռուսերեն',
  'create_resume' => 'Ստեղծել Ռեզյումե',
);
