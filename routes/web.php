<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']],
    function () {

        Route::get('/', 'HomeController@index');
        //reset password
        Route::get('/reset-password/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
        Auth::routes();
        Route::get('/get/job', 'Frontend\JobsController@getJob');
        Route::get('/register', 'RegistrationController@registerForm')->name('user.register_form_step_1');
        Route::get('/register/organization', 'RegistrationController@registerOrganizationForm')->name('user.register_form_step_2_organization')->middleware('auth');
        Route::get('/register/individual', 'RegistrationController@registerIndividualForm')->name('user.register_form_step_2_individual')->middleware('auth');
        Route::post('/register-final/{id}', 'RegistrationController@register_final')->name('user.register_final');
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/jobs/{slug?}/{type?}/{view?}', 'Frontend\JobsController@index')->name('jobs');
        Route::post('/job/{id}/clone', ['as' => 'jobs.clone', 'uses' => 'JobController@clone']);
        Route::get('/blog', 'Frontend\BlogsController@index')->name('blog');
        Route::get('/blog/{slug}', 'Frontend\BlogsController@show')->name('blog-single');
        Route::get('/review', 'Frontend\ReviewsController@index')->name('review');
        Route::get('/links', 'Frontend\ResourceController@index')->name('resource.index');
        Route::get('/about', 'HomeController@about')->name('about');
        Route::get('/contact', 'HomeController@contact')->name('contact');
        Route::get('/company/{slug}', 'HomeController@companySingle')->name('companySingle');


        //my-profile
        Route::group(['prefix' => 'profile', 'middleware' => ['auth', 'permission:profile', 'closedUser']], function () {
            Route::resource('permission', 'PermissionController')->middleware(['permission:give_permissions']);;
            Route::post('/permissions/update', ['as' => 'permissionsUpdate', 'uses' => 'PermissionController@permissionsUpdate']);
            Route::get('/make/announcement/{type}/{job_id?}', 'JobController@create')->name('make-job')->middleware(['permission:create_announcement']);
            Route::get('/announcements/{id}/edit/{translate?}', 'JobController@edit')->name('announcements-edit')->middleware(['permission:edit_announcement']);
//

            Route::resource('announcements', 'JobController')->except(['edit'])->middleware(['permission:create_announcement|delete_announcement']);
            Route::resource('resource_category', 'ResourceCategoryController')->middleware(['permission:resourceCategory_resources']);

            Route::post('/change/status/{id}', 'JobController@changeStatus')->name('status-change');
            Route::post('/after/preview/{id}', 'JobController@afterPreview')->name('after-preview');
            Route::post('/change/resource/status/{id}', 'ResourceController@changeStatus')->name('status-change-resource');
            Route::post('/change/blog/status/{id}', 'BlogController@changeStatus')->name('status-change-blog');
            Route::resource('reviews', 'ReviewController')->middleware(['permission:create_review|edit_review|delete_review']);
            Route::resource('blogs', 'BlogController')->middleware(['permission:create_article|edit_article|delete_article']);
            Route::resource('users', 'UserController')->middleware(['permission:create_user|edit_user|delete_user|close_user']);
            Route::resource('resources', 'ResourceController')->middleware(['permission:create_resource|delete_resource|edit_resource']);
            Route::resource('appliers', 'AppliersController');
            Route::get('/appliers/show/details/applier/{id}', ['as' => 'appliers.show-details', 'uses' => 'AppliersController@showDetails']);
            Route::post('/change/applier/status/{id}', 'AppliersController@changeStatus')->name('applier-status-change');
            Route::post('/user/change/status/{id}', 'UserController@changeStatus')->name('user-status-change');
            Route::post('/invoice/change/status/{id}', 'InvoiceController@changeStatus')->name('invoice-status-change');

            Route::resource('resume', 'CvController')->except(['create'])->middleware(['permission:create_resume|edit_resume|delete_resume']);
            Route::get('/create/resume/{id?}', ['as' => 'resume.create', 'uses' => 'CvController@create', 'middleware' => ['permission:create_resume|edit_resume']]);

            Route::resource('subscribtion', 'SubscribeController')->middleware(['permission:subscribtions']);
            Route::resource('pages', 'PageController')->middleware(['permission:create_pages|edit_pages|delete_pages']);
            Route::get('/register/organization/{id}', 'UserController@registerOrganizationForm')->name('profile.register_form_step_2_organization');
            Route::get('/register/individual/{id}', 'UserController@registerIndividualForm')->name('profile.register_form_step_2_individual');
            Route::post('/user/register/step-1', 'UserController@registerStep1')->name('registerUserStep1');
            Route::post('/user/register/step-2/{id}', 'UserController@registerStep2')->name('registerUserStep2');
            Route::patch('/change_password/{id}', 'UserController@changePassword')->name('changeUserPassword');
            Route::patch('/update_bankDetails/{id}', 'UserController@bankDetails')->name('bankDetails')->middleware(['permission:changeBankDetails_user']);;
            Route::get('/register/organization/{id}', 'UserController@registerOrganizationForm')->name('profile.register_form_step_2_organization');
            Route::get('/users/permissions/{id}', 'UserController@editPermissions')->name('user-permission');
            Route::patch('/permissions/update/{id}', 'UserController@updatePermissions')->name('user-permission-update');
            Route::patch('/connect', 'UserController@connectToOrganization')->name('connect_organization');
            Route::get('/', 'ProfileController@index')->name('profile');
            Route::post('/image', 'ProfileController@profileImage')->name('profile-image');
            Route::get('/resume-pdf/{id}', 'CvController@resumePdf')->name('resumePDF');
            Route::get('/invoice-pdf/{id}', 'InvoiceController@invoicePdf')->name('invoicePDF');
            Route::get('/login/users/{id}/{origin?}', 'UserController@loginUser')->name('usersLogin');
            Route::resource('service_category', 'ServiceCategoryController')->middleware(['permission:serviceCategory_service']);
            Route::resource('services', 'ServiceController')->middleware(['permission:create_service|edit_service|delete_service|all_service']);


            Route::resource('invoice', 'InvoiceController')->middleware(['permission:create_invoice|edit_invoice|delete_invoice|all_invoice']);
            Route::get('/create/invoice', 'InvoiceController@createInvoiceStep1')->name('createInvoiceStep1')->middleware(['permission:create_invoice|edit_invoice|delete_invoice|all_invoice']);
            Route::get('/search/resume', 'SearchResumeController@index')->name('searchResume');


        });
        Route::group(['middleware' => ['auth', 'closedUser']], function () {
            Route::post('/apply', 'Frontend\JobsController@apply')->name('apply');
            Route::get('/apply/job/{slug}', 'Frontend\JobsController@applyJob')->name('applyJob');
            Route::post('/get_language', 'CvController@getLanguage');
            Route::post('/cv_language', 'CvController@language');
            Route::post('/cv_visa', 'CvController@visa');
            Route::post('/get_visa', 'CvController@getVisa');
            Route::post('/remove/attachment', 'JobController@removeAttachment');
            Route::post('/get/sub_category', 'ResourceController@getSubcategory')->name('getSubCategory');
        });

        Route::get('/404', function () {
            return view('404');
        });


        Route::get('/cv/{id}', 'CvController@showCv')->name('showCv');
        Route::get('/{page}', 'Frontend\PageController@index')->name('pages');


    });

Route::get('/translations', ['as' => 'translation_manager', 'uses' => '\Barryvdh\TranslationManager\Controller@getIndex'])->middleware(['permission:translate', 'auth']);
Route::get('/translations/view/{group?}', ['as' => 'translation_group', 'uses' => '\Barryvdh\TranslationManager\Controller@getView'])->middleware(['permission:translate', 'auth']);


Route::get('/search/translations/key', [
    'as' => 'translations.search',
    'middleware' => ['permission:translate', 'auth'],
    'uses' => 'TranslationController@searchKey'
]);

Route::post('/translations/add-description', [
    'as' => 'translations.description',
    'middleware' => ['permission:translate', 'auth'],
    'uses' => 'TranslationController@addDescription'
]);


Route::post('/register', 'RegistrationController@register')->name('user.register');

Route::patch('/user_details/{id}', 'ProfileController@update')->name('profile.user_details');
Route::patch('/change_password/{id}', 'ProfileController@changePassword')->name('changePassword');
Route::patch('/change/user/password/{id}', 'UserController@changePassword')->name('changeUserPassword');


Route::post('/cv_contacts', 'CvController@contacts');
Route::post('/cv_personal', 'CvController@personal');
Route::post('/cv_objective', 'CvController@objective');
Route::post('/cv_computer_skills', 'CvController@computer_skills');
Route::post('/cv_skills', 'CvController@skills');
Route::post('/cv_volunteering', 'CvController@volunteering');
Route::post('/cv_image', 'CvController@image')->name('cv-image');
//Experience
Route::post('/cv_experience', 'CvController@experience');
Route::post('/get_experience', 'CvController@getExperience');
Route::post('/delete_experience', 'CvController@deleteExperience');
//Education
Route::post('/cv_education', 'CvController@education');
Route::post('/get_education', 'CvController@getEducation');
Route::post('/delete_education', 'CvController@deleteEducation');
//Trainings
Route::post('/cv_training', 'CvController@training');
Route::post('/get_training', 'CvController@getTraining');
Route::post('/delete_training', 'CvController@deleteTraining');

//Languages


Route::post('/delete_language', 'CvController@deleteLanguage');


//Visa

Route::post('/delete_visa', 'CvController@deleteVisa');

//Availability
Route::post('/cv_availability', 'CvController@availability');

//References
Route::post('/cv_reference', 'CvController@reference');
Route::post('/get_reference', 'CvController@getReference');
Route::post('/delete_reference', 'CvController@deleteReference');

//Questions
Route::post('/cv_questions', 'CvController@questions');

//Cv Attachments

Route::post('/cv_attachments', 'CvController@attachments');
Route::post('/cv/attachments', 'CvController@getAttachments');


Route::delete('/destroy/job/{id}', 'JobController@jobDestroy')->name('job-destroy');

Route::post('/job/{id}/submit', ['as' => 'jobs.submit', 'uses' => 'JobController@submit']);


//preview Cv

Route::post('/preview/cv', 'CvController@previewCv');

//get attachments
Route::post('/job/attachments', 'JobController@attachments');
//contact-us
Route::post('/contact-us', 'HomeController@contactUs')->name('contact-us');
//subscribe
Route::post('/subscribe-us', 'HomeController@subscribe')->name('subscribe');


//get company info from profile
Route::post('/get/company', 'JobController@getCompany');

//get bank details for invoice
Route::post('/get/bankDetails', 'InvoiceController@getBankDetails');

//get companies for attach company to users
Route::post('/get/organizations', 'UserController@getOrganizations');

//create Invoice
Route::post('/create-invoice', 'InvoiceController@createInvoice')->name('createInvoice');

//get service amount
Route::post('/get/service-amount', 'InvoiceController@getServiceAmount');

//save Invoice
Route::post('/save-invoice/{id}', 'InvoiceController@saveInvoice')->name('saveInvoice');
















