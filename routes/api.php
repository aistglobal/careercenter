<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::post('/upload/image', ['as' => 'uploadImage', 'uses' => 'Api\CkeditorController@uploadImage']);
Route::prefix('v1')->group(function () {
    Route::group(['prefix' => LaravelLocalization::setLocale(request()->segment(3)),
        'middleware' => ['localizationRedirect']],
        function () {
            Route::group(['namespace' => 'Api'], function () {
                Route::prefix('auth')->group(function () {
                    // Below mention routes are public, user can access those without any restriction.
                    // Create New User
                    Route::post('register', 'AuthController@register');
                    // Login User
                    Route::post('login', 'AuthController@login');

                    // Refresh the JWT Token
                    Route::get('refresh', 'AuthController@refresh');

                    // Send reset password mail
                    Route::post('reset/token', 'AuthController@resetPasswordForApp');

                    // handle reset password form process
//            Route::post('reset/token', 'AuthController@callResetPassword');
                    Route::get('reset/password/{reset_token}', 'AuthController@checkToken');
                    Route::post('reset/password', 'AuthController@resetPassword');

                    // Below mention routes are available only for the authenticated users.
                    Route::middleware('JWTAuth:api')->group(function () {
                        // Get user info
                        Route::get('user', 'AuthController@user');
                        // Logout user from application
                        Route::post('logout', 'AuthController@logout');
                    });
                });
                Route::middleware('JWTAuth:api')->group(function () {
                    Route::get('jobs', 'JobController@getAll');
                    Route::get('job/{slug}', 'JobController@getBySlug');
                    Route::post('apply', 'JobController@apply');
                    Route::get('types', 'JobController@getJobTypes');
                    Route::get('categories', 'JobController@getCategories');
                    Route::get('bookmarks', 'JobController@getUserBookmarks');
                    Route::post('image', 'ProfileController@profileImage');
                    Route::patch('/user_details/{id}', 'ProfileController@update');
                    Route::patch('/change_password/{id}', 'ProfileController@changePassword');
                    Route::get('/history', 'JobController@getHistory');
                    Route::get('/get-applicants', 'JobController@getApplicants');
                    Route::group(['prefix' => 'bookmark'], function () {
                        Route::post('add', 'JobController@addBookmark');
                        Route::post('remove', 'JobController@removeBookmark');
                    });
                    Route::group(['prefix' => 'notification'], function () {
                        Route::post('create', 'ProfileController@setPushNotificationToken');
                        Route::post('delete', 'ProfileController@removePushNotificationToken');
                        Route::get('get/{user_id}', 'ProfileController@getUserNotifications');
                    });
                });
                Route::get('/blog', 'BlogController@getAll');
                Route::get('/blog/{slug}', 'BlogController@getBySlug');
                Route::post('/contact', 'AuthController@contactUs');
                Route::get('/countries', 'JobController@getAllCountries');
                Route::get('/test', function (Request $request, \App\Services\PushNotificationService $pushNotificationService) {
                    $pushNotificationService->sendNotifications("test", "test", [], [$request->token]);
                });

            });
        });
});